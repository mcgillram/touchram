package ca.mcgill.sel.operationmodel.language.controller;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.operationmodel.Actor;
import ca.mcgill.sel.operationmodel.Classifier;
import ca.mcgill.sel.operationmodel.Message;
import ca.mcgill.sel.operationmodel.OmFactory;
import ca.mcgill.sel.operationmodel.OmPackage;
import ca.mcgill.sel.operationmodel.OperationSchema;

/**
 * The controller for {@link EnvironmentModel}
 * @author hyacinthali
 *
 */
public class OperationModelController extends BaseController{
	
	 /**
     * Creates a new instance of {@link OperationModelController}.
     */
    protected OperationModelController() {
        // Prevent anyone outside this package to instantiate.
    }
 
    /**
     * Creates new actor in operation schema.
     * @param owner - the container {@link OperationSchema} of the actor
     * @param name - name of the actor
     */
    public void createActor(OperationSchema owner, String name) {
    	
    	Actor newActor = OmFactory.eINSTANCE.createActor();
        newActor.setName(name);

        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);

        // Create commands.
        Command addActorCommand = AddCommand.create(editingDomain, owner, OmPackage.Literals.OPERATION_SCHEMA__ACTORS,
                newActor);
        
        // Create compound command.
        CompoundCommand compoundCommand = new CompoundCommand();
        compoundCommand.append(addActorCommand);
        
        doExecute(editingDomain, compoundCommand);
    	
    }
    
    /**
     * Creates new classifier in operation schema.
     * @param owner - the container {@link OperationSchema} of the classifier
     * @param name - name of the classifier
     */
    public void createClass(OperationSchema owner, String name) {
    	
    	Classifier newClassifier = OmFactory.eINSTANCE.createClassifier();
    	newClassifier.setName(name);

        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);

        // Create commands.
        Command addActorCommand = AddCommand.create(editingDomain, owner, OmPackage.Literals.OPERATION_SCHEMA__SCOPE,
        		newClassifier);
        
        // Create compound command.
        CompoundCommand compoundCommand = new CompoundCommand();
        compoundCommand.append(addActorCommand);
        
        doExecute(editingDomain, compoundCommand);
    	
    }
    

    /**
     * Creates a message in an operation schema.
     * @param em - the operation schema
     * @param actor - the actor that references the new message, if the message is an output message
     * @param name - name of the message
     */
    public void createMessage(OperationSchema os, Actor actor, String name, boolean inputMessage) {
    	
    	// create message
    	Message message = OmFactory.eINSTANCE.createMessage();
    	if (inputMessage) {
    		os.setInputMessage(message);
    	} else {
    		actor.getOutputMessages().add(message);
    	}
    	
    	EditingDomain editingDomain = EMFEditUtil.getEditingDomain(os);
    	
    	// Create commands.
        Command addMessageCommand = AddCommand.create(editingDomain, os, 
        		OmPackage.Literals.OPERATION_SCHEMA__MESSAGES, message);
        
        // Create compound command.
        CompoundCommand compoundCommand = new CompoundCommand();
        compoundCommand.append(addMessageCommand);
        
        doExecute(editingDomain, compoundCommand);
    	
    }
    
    /**
     * Removes the given actor
     * @param actor - the actor to be removed
     */
    public void removeActor(Actor actor) {
    	OperationSchema os = (OperationSchema) actor.eContainer();
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(os);

        CompoundCommand compoundCommand = new CompoundCommand();
       
        // Create command for removing actor.
        Command removeActorCommand = RemoveCommand.create(editingDomain, actor);
        compoundCommand.append(removeActorCommand);

        doExecute(editingDomain, compoundCommand);
    }
    
    /**
     * Removes the given message.
     *
     * @param message - the message to be removed
     */
    public void removeMessage(Message message) {
        OperationSchema os = (OperationSchema) message.eContainer();
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(os);
        CompoundCommand compoundCommand = new CompoundCommand();

        compoundCommand.append(RemoveCommand.create(editingDomain, message));

        doExecute(editingDomain, compoundCommand);
    }
    
    /**
     * Removes the given classifier
     * @param actor - the actor to be removed
     */
    public void removeClassifer(Classifier classifer) {
    	OperationSchema os = (OperationSchema) classifer.eContainer();
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(os);

        CompoundCommand compoundCommand = new CompoundCommand();
       
        // Create command for removing classifier.
        Command removeActorCommand = RemoveCommand.create(editingDomain, classifer);
        compoundCommand.append(removeActorCommand);

        doExecute(editingDomain, compoundCommand);
    }

}
