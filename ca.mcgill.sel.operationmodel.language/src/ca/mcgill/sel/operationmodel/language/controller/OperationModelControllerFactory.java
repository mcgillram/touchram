package ca.mcgill.sel.operationmodel.language.controller;

/**
 * A factory to obtain controllers for operation model
 * 
 * @author hyacinthali
 *
 */
public class OperationModelControllerFactory {
	
	/**
     * The singleton instance of this factory.
     */
    public static final OperationModelControllerFactory INSTANCE = new OperationModelControllerFactory();
    
    private OperationModelController operationModelController ;
        
    /**
     * Creates a new instance of {@link OperationModelControllerFactory}.
     */
    private OperationModelControllerFactory() {
        
    }

	/**
	 * Returns the controller for {@link OperationModelController}.
	 * 
	 * @return the operationModelController
	 */
	public OperationModelController getOperationModelController() {
		if (operationModelController == null) {
			operationModelController = new OperationModelController();
        }
		return operationModelController;
	}
    
    

}
