package ca.mcgill.sel.core.perspective.domain.design.usecase.models;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.eclipse.emf.common.util.URI;

import ca.mcgill.sel.commons.ResourceUtil;
import ca.mcgill.sel.commons.emf.util.AdapterFactoryRegistry;
import ca.mcgill.sel.commons.emf.util.ResourceManager;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREExternalLanguage;
import ca.mcgill.sel.core.CORELanguageElementMapping;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.provider.CoreItemProviderAdapterFactory;
import ca.mcgill.sel.core.util.CoreResourceFactoryImpl;
import ca.mcgill.sel.ram.ui.utils.ResourceUtils;

public class DomainDesignUseCasePerspectiveDesign {
    
 public static void main(String[] args) {
        
        // Initialize ResourceManager
        ResourceManager.initialize();
    
        // Initialize CORE packages.
        CorePackage.eINSTANCE.eClass();
    
        // Register resource factories
        ResourceManager.registerExtensionFactory("core", new CoreResourceFactoryImpl());
    
        // Initialize adapter factories
        AdapterFactoryRegistry.INSTANCE.addAdapterFactory(CoreItemProviderAdapterFactory.class);
        
        ResourceUtils.loadLibraries();
       
        // create a perspective and its features, if any
        COREPerspective perspective = DomainDesignUseCasePerspective.createPerspective();

        // create external languages, if any
//        COREExternalLanguage classDiagram = ClassDiagramLanguage.createLanguage();
//        perspective.getLanguages().put("Domain_Model", classDiagram);
//        perspective.getLanguages().put("Design_Model", classDiagram);
//        
//        COREExternalLanguage useCaseDiagram = UseCaseLanguage.createLanguage();
//        perspective.getLanguages().put("Use_Case", useCaseDiagram);
        
        // Add existing external languages, if any
        List<String> languages = ResourceUtil.getResourceListing("models/testlanguages/", ".core");
        if (languages != null) {
            for (String l : languages) {
        
                // load existing languages
                URI fileURI = URI.createURI(l);
                COREConcern languageConcern = (COREConcern) ResourceManager.loadModel(fileURI);
                for (COREArtefact a : languageConcern.getArtefacts()) {
                    if (a instanceof COREExternalLanguage) {
                        COREExternalLanguage existingLanguage = (COREExternalLanguage) a;
                        if (existingLanguage.getName().equals("ClassDiagramLanguage_Gen")) {
                            perspective.getLanguages().put("Domain_Model", existingLanguage);
                            perspective.getLanguages().put("Design_Model", existingLanguage);
                        } else if (existingLanguage.getName().equals("Use Case Diagram")) {
                            perspective.getLanguages().put("Use_Case", existingLanguage);
                        }
                    }
                }
            }
        }
        
        
        COREConcern perspectiveConcern = perspective.getCoreConcern();
        
        String fileName = "/Users/hyacinthali/TouchCORE/touchram/ca.mcgill.sel.ram/resources/models/testperspectives/"
           + perspective.getName();
        
        try {
            String filePath = new File("").getAbsolutePath();
            filePath = filePath.replace("ca.mcgill.sel.core.perspective.design", 
                    "ca.mcgill.sel.ram/resources/models/testperspectives/");
            filePath = filePath + perspective.getName();
            ResourceManager.saveModel(perspectiveConcern, filePath.concat("." + "core"));
        } catch (IOException e) {
            // Shouldn't happen.
            e.printStackTrace();
        } 
   }
    
    public static int getNextMappingId(COREPerspective perspective) {
        
        int idNumber = 0;
        for (CORELanguageElementMapping lem : perspective.getMappings()) {
            if (lem.getIdentifier() > idNumber) {
                idNumber = lem.getIdentifier();
            }
        }
        return idNumber + 1;
      }

}
