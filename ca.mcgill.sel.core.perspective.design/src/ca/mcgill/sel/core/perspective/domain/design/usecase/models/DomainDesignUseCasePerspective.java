package ca.mcgill.sel.core.perspective.domain.design.usecase.models;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EReference;

import ca.mcgill.sel.classdiagram.CdmPackage;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.CORELanguageElementMapping;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.COREPerspectiveAction;
import ca.mcgill.sel.core.CORERelationship;
import ca.mcgill.sel.core.Cardinality;
import ca.mcgill.sel.core.CoreFactory;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.IntraLanguageMapping;
import ca.mcgill.sel.core.MappingEnd;
import ca.mcgill.sel.core.perspective.domain.design.models.PerspectiveDesign;
import ca.mcgill.sel.core.util.COREModelUtil;
import ca.mcgill.sel.usecasediagram.UcdmPackage;
import ca.mcgill.sel.ram.RamPackage;

public class DomainDesignUseCasePerspective {
    
    public static COREPerspective createPerspective() {
        
        // create perspective concern
        COREConcern perspectiveConcern = COREModelUtil.createConcern("Domain_Design_UseCase");
    
        COREPerspective perspective = CoreFactory.eINSTANCE.createCOREPerspective();
        perspective.setName("Domain_Design_UseCase");
    
       // create perspective actions
        createPerspectiveAction(perspective);

        // create perspective mappings
        createPerspectiveMappings(perspective);
        
        createNavigationMappings(perspective);
        
        perspectiveConcern.getArtefacts().add(perspective);
        
        return perspective;
    }
    
    private static void createPerspectiveAction(COREPerspective perspective) {
        // create perspective actions
        
        COREPerspectiveAction pAction = CoreFactory.eINSTANCE.createCORERedefineAction();
        pAction.setName("ClassDiagram.Class.add"); 
        pAction.setForRole("Domain_Model");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCORERedefineAction();
        pAction.setName("ClassDiagram.Class.edit"); 
        pAction.setForRole("Domain_Model");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCORERedefineAction();
        pAction.setName("ClassDiagram.Class.delete"); 
        pAction.setForRole("Domain_Model");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCOREReexposeAction();
        pAction.setName("ClassDiagram.Note.add"); 
        pAction.setForRole("Domain_Model");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCOREReexposeAction();
        pAction.setName("ClassDiagram.Note.edit"); 
        pAction.setForRole("Domain_Model");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCOREReexposeAction();
        pAction.setName("ClassDiagram.Note.delete"); 
        pAction.setForRole("Domain_Model");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCOREReexposeAction();
        pAction.setName("ClassDiagram.Association.add"); 
        pAction.setForRole("Domain_Model");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCOREReexposeAction();
        pAction.setName("ClassDiagram.Association.edit"); 
        pAction.setForRole("Domain_Model");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCOREReexposeAction();
        pAction.setName("ClassDiagram.Association.delete"); 
        pAction.setForRole("Domain_Model");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCOREReexposeAction();
        pAction.setName("ClassDiagram.NaryAssociation.add"); 
        pAction.setForRole("Domain_Model");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCOREReexposeAction();
        pAction.setName("ClassDiagram.NaryAssociation.delete"); 
        pAction.setForRole("Domain_Model");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCORERedefineAction();
        pAction.setName("ClassDiagram.Classifier.Attribute.add"); 
        pAction.setForRole("Domain_Model");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCORERedefineAction();
        pAction.setName("ClassDiagram.Classifier.Attribute.edit"); 
        pAction.setForRole("Domain_Model");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCORERedefineAction();
        pAction.setName("ClassDiagram.Classifier.Attribute.delete"); 
        pAction.setForRole("Domain_Model");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCOREReexposeAction();
        pAction.setName("ClassDiagram.CDEnum.add"); 
        pAction.setForRole("Domain_Model");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCOREReexposeAction();
        pAction.setName("ClassDiagram.CDEnum.edit"); 
        pAction.setForRole("Domain_Model");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCOREReexposeAction();
        pAction.setName("ClassDiagram.CDEnum.delete"); 
        pAction.setForRole("Domain_Model");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCOREReexposeAction();
        pAction.setName("ClassDiagram.StructuralFeature.static.edit"); 
        pAction.setForRole("Domain_Model");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCOREReexposeAction();
        pAction.setName("ClassDiagram.VisibilityType.edit"); 
        pAction.setForRole("Domain_Model");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCORERedefineAction();
        pAction.setName("ClassDiagram.Class.add"); 
        pAction.setForRole("Design_Model");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCORERedefineAction();
        pAction.setName("ClassDiagram.Class.edit"); 
        pAction.setForRole("Design_Model");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCORERedefineAction();
        pAction.setName("ClassDiagram.Class.delete"); 
        pAction.setForRole("Design_Model");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCOREReexposeAction();
        pAction.setName("ClassDiagram.Note.add"); 
        pAction.setForRole("Design_Model");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCOREReexposeAction();
        pAction.setName("ClassDiagram.Note.edit"); 
        pAction.setForRole("Design_Model");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCOREReexposeAction();
        pAction.setName("ClassDiagram.Note.delete"); 
        pAction.setForRole("Design_Model");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCOREReexposeAction();
        pAction.setName("ClassDiagram.Association.add"); 
        pAction.setForRole("Design_Model");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCOREReexposeAction();
        pAction.setName("ClassDiagram.Association.edit"); 
        pAction.setForRole("Design_Model");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCOREReexposeAction();
        pAction.setName("ClassDiagram.Association.delete"); 
        pAction.setForRole("Design_Model");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCOREReexposeAction();
        pAction.setName("ClassDiagram.NaryAssociation.add"); 
        pAction.setForRole("Design_Model");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCOREReexposeAction();
        pAction.setName("ClassDiagram.NaryAssociation.delete"); 
        pAction.setForRole("Design_Model");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCORERedefineAction();
        pAction.setName("ClassDiagram.Classifier.Attribute.add"); 
        pAction.setForRole("Design_Model");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCORERedefineAction();
        pAction.setName("ClassDiagram.Classifier.Attribute.edit"); 
        pAction.setForRole("Design_Model");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCORERedefineAction();
        pAction.setName("ClassDiagram.Classifier.Attribute.delete"); 
        pAction.setForRole("Design_Model");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCOREReexposeAction();
        pAction.setName("ClassDiagram.CDEnum.add"); 
        pAction.setForRole("Design_Model");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCOREReexposeAction();
        pAction.setName("ClassDiagram.CDEnum.edit"); 
        pAction.setForRole("Design_Model");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCOREReexposeAction();
        pAction.setName("ClassDiagram.CDEnum.delete"); 
        pAction.setForRole("Design_Model");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCOREReexposeAction();
        pAction.setName("ClassDiagram.StructuralFeature.static.edit"); 
        pAction.setForRole("Design_Model");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCOREReexposeAction();
        pAction.setName("ClassDiagram.ImplementationClass.add"); 
        pAction.setForRole("Design_Model");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCOREReexposeAction();
        pAction.setName("ClassDiagram.ImplementationClass.edit"); 
        pAction.setForRole("Design_Model");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCOREReexposeAction();
        pAction.setName("ClassDiagram.ImplementationClass.delete"); 
        pAction.setForRole("Design_Model");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCOREReexposeAction();
        pAction.setName("ClassDiagram.Classifier.Operation.add"); 
        pAction.setForRole("Design_Model");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCOREReexposeAction();
        pAction.setName("ClassDiagram.Classifier.Operation.edit"); 
        pAction.setForRole("Design_Model");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCOREReexposeAction();
        pAction.setName("ClassDiagram.Classifier.Operation.delete"); 
        pAction.setForRole("Design_Model");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCOREReexposeAction();
        pAction.setName("ClassDiagram.Classifier.Operation.Parameter.add"); 
        pAction.setForRole("Design_Model");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCOREReexposeAction();
        pAction.setName("ClassDiagram.Classifier.Operation.Parameter.edit"); 
        pAction.setForRole("Design_Model");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCOREReexposeAction();
        pAction.setName("ClassDiagram.Classifier.Operation.Parameter.delete"); 
        pAction.setForRole("Design_Model");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCOREReexposeAction();
        pAction.setName("ClassDiagram.VisibilityType.edit"); 
        pAction.setForRole("Design_Model");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCORERedefineAction();
        pAction.setName("UseCaseDiagram.Actor.add"); 
        pAction.setForRole("Use_Case");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCORERedefineAction();
        pAction.setName("UseCaseDiagram.Actor.edit"); 
        pAction.setForRole("Use_Case");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCORERedefineAction();
        pAction.setName("UseCaseDiagram.Actor.delete"); 
        pAction.setForRole("Use_Case");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCOREReexposeAction();
        pAction.setName("UseCaseDiagram.Note.add"); 
        pAction.setForRole("Use_Case");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCOREReexposeAction();
        pAction.setName("UseCaseDiagram.Note.edit"); 
        pAction.setForRole("Use_Case");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCOREReexposeAction();
        pAction.setName("UseCaseDiagram.Note.delete"); 
        pAction.setForRole("Use_Case");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCOREReexposeAction();
        pAction.setName("UseCaseDiagram.PrimaryLink.add"); 
        pAction.setForRole("Use_Case");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCOREReexposeAction();
        pAction.setName("UseCaseDiagram.PrimaryLink.edit"); 
        pAction.setForRole("Use_Case");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCOREReexposeAction();
        pAction.setName("UseCaseDiagram.PrimaryLink.delete"); 
        pAction.setForRole("Use_Case");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCORERedefineAction();
        pAction.setName("UseCaseDiagram.SecondaryLink.add"); 
        pAction.setForRole("Use_Case");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCORERedefineAction();
        pAction.setName("UseCaseDiagram.SecondaryLink.edit"); 
        pAction.setForRole("Use_Case");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCORERedefineAction();
        pAction.setName("UseCaseDiagram.SecondaryLink.delete"); 
        pAction.setForRole("Use_Case");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCOREReexposeAction();
        pAction.setName("UseCaseDiagram.UseCase.add"); 
        pAction.setForRole("Use_Case");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCOREReexposeAction();
        pAction.setName("UseCaseDiagram.UseCase.edit"); 
        pAction.setForRole("Use_Case");   
        perspective.getActions().add(pAction); 
        
        pAction = CoreFactory.eINSTANCE.createCOREReexposeAction();
        pAction.setName("UseCaseDiagram.UseCase.delete"); 
        pAction.setForRole("Use_Case");   
        perspective.getActions().add(pAction); 
    }


    private static void createPerspectiveMappings(COREPerspective perspective) {
        
        CORELanguageElementMapping mappingType = null;
        MappingEnd fromMappingEnd = null;
        MappingEnd toMappingEnd = null;
        
        // language element mapping between a domain diagram metaclass and a use case diagram metaclass.
        {
            mappingType = CoreFactory.eINSTANCE.createCORELanguageElementMapping();
            mappingType.setIdentifier(PerspectiveDesign.getNextMappingId(perspective));
            mappingType.setRelationship(CORERelationship.EQUALITY);
        
            fromMappingEnd = CoreFactory.eINSTANCE.createMappingEnd(); 
            fromMappingEnd.setCardinality(Cardinality.COMPULSORY);
            fromMappingEnd.setRoleName("Domain_Model");
            fromMappingEnd.setLanguageElement(CdmPackage.eINSTANCE.getClassDiagram());
                     
            toMappingEnd = CoreFactory.eINSTANCE.createMappingEnd();
            toMappingEnd.setCardinality(Cardinality.COMPULSORY);
            toMappingEnd.setRoleName("Use_Case");
            toMappingEnd.setLanguageElement(UcdmPackage.eINSTANCE.getUseCaseDiagram());
        
            mappingType.getMappingEnds().add(fromMappingEnd);
            mappingType.getMappingEnds().add(toMappingEnd);
            perspective.getMappings().add(mappingType);
        }
        
        // language element mapping between a domain diagram metaclass and a design diagram metaclass.
        {
            mappingType = CoreFactory.eINSTANCE.createCORELanguageElementMapping();
            mappingType.setIdentifier(PerspectiveDesign.getNextMappingId(perspective));
            mappingType.setRelationship(CORERelationship.EQUALITY);
        
            fromMappingEnd = CoreFactory.eINSTANCE.createMappingEnd(); 
            fromMappingEnd.setCardinality(Cardinality.COMPULSORY);
            fromMappingEnd.setRoleName("Domain_Model");
            fromMappingEnd.setLanguageElement(CdmPackage.eINSTANCE.getClassDiagram());
                     
            toMappingEnd = CoreFactory.eINSTANCE.createMappingEnd();
            toMappingEnd.setCardinality(Cardinality.COMPULSORY);
            toMappingEnd.setRoleName("Design_Model");
            toMappingEnd.setLanguageElement(CdmPackage.eINSTANCE.getClassDiagram());
        
            mappingType.getMappingEnds().add(fromMappingEnd);
            mappingType.getMappingEnds().add(toMappingEnd);
            perspective.getMappings().add(mappingType);
        }
        
        // language element mapping between a domain model Class metaclass and an Use Case Model Actor metaclass.
        {
            mappingType = CoreFactory.eINSTANCE.createCORELanguageElementMapping();
            mappingType.setIdentifier(PerspectiveDesign.getNextMappingId(perspective));
            mappingType.setRelationship(CORERelationship.EQUALITY);
        
            fromMappingEnd = CoreFactory.eINSTANCE.createMappingEnd(); 
            fromMappingEnd.setCardinality(Cardinality.COMPULSORY);
            fromMappingEnd.setRoleName("Domain_Model");
            fromMappingEnd.setLanguageElement(CdmPackage.eINSTANCE.getClass_());
                     
            toMappingEnd = CoreFactory.eINSTANCE.createMappingEnd();
            toMappingEnd.setCardinality(Cardinality.OPTIONAL);
            toMappingEnd.setRoleName("Use_Case");
            toMappingEnd.setLanguageElement(UcdmPackage.eINSTANCE.getActor());
        
            mappingType.getMappingEnds().add(fromMappingEnd);
            mappingType.getMappingEnds().add(toMappingEnd);
            perspective.getMappings().add(mappingType); 
        }
        
        
        // language element mapping between a domain model Class metaclass and a design Model Class metaclass.
        {
            mappingType = CoreFactory.eINSTANCE.createCORELanguageElementMapping();
            mappingType.setIdentifier(PerspectiveDesign.getNextMappingId(perspective));
            mappingType.setRelationship(CORERelationship.EQUALITY);
        
            fromMappingEnd = CoreFactory.eINSTANCE.createMappingEnd(); 
            fromMappingEnd.setCardinality(Cardinality.COMPULSORY);
            fromMappingEnd.setRoleName("Domain_Model");
            fromMappingEnd.setLanguageElement(CdmPackage.eINSTANCE.getClass_());
                     
            toMappingEnd = CoreFactory.eINSTANCE.createMappingEnd();
            toMappingEnd.setCardinality(Cardinality.OPTIONAL);
            toMappingEnd.setRoleName("Design_Model");
            toMappingEnd.setLanguageElement(CdmPackage.eINSTANCE.getClass_());
        
            mappingType.getMappingEnds().add(fromMappingEnd);
            mappingType.getMappingEnds().add(toMappingEnd);
            perspective.getMappings().add(mappingType); 
        }
        
        // language element mapping between a domain model Attribute metaclass and a design Model Attribute metaclass.
        {
            mappingType = CoreFactory.eINSTANCE.createCORELanguageElementMapping();
            mappingType.setIdentifier(PerspectiveDesign.getNextMappingId(perspective));
            mappingType.setRelationship(CORERelationship.EQUALITY);
        
            fromMappingEnd = CoreFactory.eINSTANCE.createMappingEnd(); 
            fromMappingEnd.setCardinality(Cardinality.COMPULSORY);
            fromMappingEnd.setRoleName("Domain_Model");
            fromMappingEnd.setLanguageElement(CdmPackage.eINSTANCE.getAttribute());
                     
            toMappingEnd = CoreFactory.eINSTANCE.createMappingEnd();
            toMappingEnd.setCardinality(Cardinality.OPTIONAL);
            toMappingEnd.setRoleName("Design_Model");
            toMappingEnd.setLanguageElement(CdmPackage.eINSTANCE.getAttribute());
        
            mappingType.getMappingEnds().add(fromMappingEnd);
            mappingType.getMappingEnds().add(toMappingEnd);
            perspective.getMappings().add(mappingType); 
        }
       
    }
    
    private static void createNavigationMappings(COREPerspective perspective) {
//        createBaseFeatureModelMappings(perspective);
//        
//        createBaseImpactModelMappings(perspective);
        
        createBaseAspectMappings(perspective);
        
        createBaseClassDiagramMappings(perspective);
    }
    
    
    private static void createBaseAspectMappings(COREPerspective perspective) {
        IntraLanguageMapping aspectClassifierMapping = CoreFactory.eINSTANCE.createIntraLanguageMapping();
        IntraLanguageMapping structuralViewClassifierMapping = CoreFactory.eINSTANCE.createIntraLanguageMapping();
        IntraLanguageMapping classOperationsMapping = CoreFactory.eINSTANCE.createIntraLanguageMapping();
        IntraLanguageMapping classSuperClassMapping = CoreFactory.eINSTANCE.createIntraLanguageMapping();
        IntraLanguageMapping aspectInteractionMapping = CoreFactory.eINSTANCE.createIntraLanguageMapping();
        IntraLanguageMapping messageViewInteractionMapping = CoreFactory.eINSTANCE.createIntraLanguageMapping();
        IntraLanguageMapping interactionLifelineMapping = CoreFactory.eINSTANCE.createIntraLanguageMapping();
        IntraLanguageMapping interactionMessageMapping = CoreFactory.eINSTANCE.createIntraLanguageMapping();
        
        EClass aspectEClass = RamPackage.eINSTANCE.getAspect();
        EClass classifierEClass = RamPackage.eINSTANCE.getClassifier();
        EClass interactionEClass = RamPackage.eINSTANCE.getInteraction();
        EClass structrualViewEClass = RamPackage.eINSTANCE.getStructuralView();
        EClass messageViewEClass = RamPackage.eINSTANCE.getMessageView();
        
        EReference aspectStructuralView = RamPackage.eINSTANCE.getAspect_StructuralView();
        EReference structuralViewClass = RamPackage.eINSTANCE.getStructuralView_Classes();
        EReference classOperations = RamPackage.eINSTANCE.getClassifier_Operations();
        EReference classSuperClasses = RamPackage.eINSTANCE.getClassifier_SuperTypes();
        EReference aspectMessageView = RamPackage.eINSTANCE.getAspect_MessageViews();
        EReference messageViewInteraction = RamPackage.eINSTANCE.getMessageView_Specification();
        EReference interactionLifeline = RamPackage.eINSTANCE.getInteraction_Lifelines();
        EReference interactionMessage = RamPackage.eINSTANCE.getInteraction_Messages();
        
        aspectClassifierMapping.setFrom(aspectEClass);
        aspectClassifierMapping.getHops().add(aspectStructuralView);
        aspectClassifierMapping.getHops().add(structuralViewClass);
        
        structuralViewClassifierMapping.setFrom(structrualViewEClass);
        structuralViewClassifierMapping.getHops().add(structuralViewClass);
        
        classOperationsMapping.setFrom(classifierEClass);
        classOperationsMapping.getHops().add(classOperations);
        
        classSuperClassMapping.setFrom(classifierEClass);
        classSuperClassMapping.getHops().add(classSuperClasses);
        classSuperClassMapping.setClosure(true);
        
        aspectInteractionMapping.setFrom(aspectEClass);
        aspectInteractionMapping.getHops().add(aspectMessageView);
        aspectInteractionMapping.getHops().add(messageViewInteraction);
        
        messageViewInteractionMapping.setFrom(messageViewEClass);
        messageViewInteractionMapping.getHops().add(messageViewInteraction);
        
        interactionLifelineMapping.setFrom(interactionEClass);
        interactionLifelineMapping.getHops().add(interactionLifeline);
        
        interactionMessageMapping.setFrom(interactionEClass);
        interactionMessageMapping.getHops().add(interactionMessage);
        
        List<IntraLanguageMapping> aspectILM = new ArrayList<IntraLanguageMapping>();
        aspectILM.add(aspectClassifierMapping);
        aspectILM.add(classOperationsMapping);
        aspectILM.add(classSuperClassMapping);
        aspectILM.add(aspectInteractionMapping);
        aspectILM.add(interactionLifelineMapping);
        aspectILM.add(interactionMessageMapping);
        
        List<IntraLanguageMapping> structuralViewILM = new ArrayList<IntraLanguageMapping>();
        structuralViewILM.add(structuralViewClassifierMapping);
        structuralViewILM.add(classOperationsMapping);
        structuralViewILM.add(classSuperClassMapping);
        
        List<IntraLanguageMapping> messageViewILM = new ArrayList<IntraLanguageMapping>();
        messageViewILM.add(messageViewInteractionMapping);
        messageViewILM.add(interactionLifelineMapping);
        messageViewILM.add(interactionMessageMapping);
                
        perspective.getNavigationMappings().addAll(aspectILM);
        perspective.getNavigationMappings().add(structuralViewClassifierMapping);
        perspective.getNavigationMappings().add(messageViewInteractionMapping);

    }
    
    private static void createBaseClassDiagramMappings(COREPerspective perspective) {
        IntraLanguageMapping cdClassifierMapping = CoreFactory.eINSTANCE.createIntraLanguageMapping();
        IntraLanguageMapping classifierSuperClassMapping = CoreFactory.eINSTANCE.createIntraLanguageMapping();
        IntraLanguageMapping classifierOperationsMapping = CoreFactory.eINSTANCE.createIntraLanguageMapping();
        
        EClass classDiagramEClass = CdmPackage.eINSTANCE.getClassDiagram();
        EClass classifierEClass = CdmPackage.eINSTANCE.getClassifier();
        
        EReference classDiagramClassifier = CdmPackage.eINSTANCE.getClassDiagram_Classes();
        EReference classifierSuperClass = CdmPackage.eINSTANCE.getClassifier_SuperTypes();
        EReference classifierOperations = CdmPackage.eINSTANCE.getClassifier_Operations();
        
        cdClassifierMapping.setFrom(classDiagramEClass);
        cdClassifierMapping.getHops().add(classDiagramClassifier);
        classifierSuperClassMapping.setFrom(classifierEClass);
        classifierSuperClassMapping.getHops().add(classifierSuperClass);
        classifierOperationsMapping.setFrom(classifierEClass);
        classifierOperationsMapping.getHops().add(classifierOperations);
        
        List<IntraLanguageMapping> classDiagramILM = new ArrayList<IntraLanguageMapping>();
        classDiagramILM.add(cdClassifierMapping);
        classDiagramILM.add(classifierSuperClassMapping);
        classDiagramILM.add(classifierOperationsMapping);
        
        
        perspective.getNavigationMappings().addAll(classDiagramILM);
    }
}
