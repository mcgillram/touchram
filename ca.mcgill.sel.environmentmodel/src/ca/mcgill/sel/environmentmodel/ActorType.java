/**
 */
package ca.mcgill.sel.environmentmodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Actor Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.mcgill.sel.environmentmodel.EmPackage#getActorType()
 * @model
 * @generated
 */
public interface ActorType extends NamedElement {
} // ActorType
