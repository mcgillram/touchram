/**
 */
package ca.mcgill.sel.environmentmodel;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see ca.mcgill.sel.environmentmodel.EmPackage
 * @generated
 */
public interface EmFactory extends EFactory {
	/**
     * The singleton instance of the factory.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	EmFactory eINSTANCE = ca.mcgill.sel.environmentmodel.impl.EmFactoryImpl.init();

	/**
     * Returns a new object of class '<em>Environment Model</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Environment Model</em>'.
     * @generated
     */
	EnvironmentModel createEnvironmentModel();

	/**
     * Returns a new object of class '<em>Time Triggered Event</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Time Triggered Event</em>'.
     * @generated
     */
	TimeTriggeredEvent createTimeTriggeredEvent();

	/**
     * Returns a new object of class '<em>Actor Actor Communication</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Actor Actor Communication</em>'.
     * @generated
     */
	ActorActorCommunication createActorActorCommunication();

	/**
     * Returns a new object of class '<em>Actor</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Actor</em>'.
     * @generated
     */
	Actor createActor();

	/**
     * Returns a new object of class '<em>Message</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Message</em>'.
     * @generated
     */
	Message createMessage();

	/**
     * Returns a new object of class '<em>Parameter</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Parameter</em>'.
     * @generated
     */
	Parameter createParameter();

	/**
     * Returns a new object of class '<em>Actor Type</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Actor Type</em>'.
     * @generated
     */
	ActorType createActorType();

	/**
     * Returns a new object of class '<em>Message Type</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Message Type</em>'.
     * @generated
     */
	MessageType createMessageType();

	/**
     * Returns a new object of class '<em>Parameter Type</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Parameter Type</em>'.
     * @generated
     */
	ParameterType createParameterType();

	/**
     * Returns a new object of class '<em>Layout</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Layout</em>'.
     * @generated
     */
	Layout createLayout();

	/**
     * Returns a new object of class '<em>Layout Element</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Layout Element</em>'.
     * @generated
     */
	LayoutElement createLayoutElement();

	/**
     * Returns a new object of class '<em>Note</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Note</em>'.
     * @generated
     */
    Note createNote();

    /**
     * Returns the package supported by this factory.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the package supported by this factory.
     * @generated
     */
	EmPackage getEmPackage();

} //EmFactory
