/**
 */
package ca.mcgill.sel.environmentmodel.impl;

import ca.mcgill.sel.environmentmodel.Actor;
import ca.mcgill.sel.environmentmodel.ActorActorCommunication;
import ca.mcgill.sel.environmentmodel.EmPackage;
import java.util.Collection;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Actor Actor Communication</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.environmentmodel.impl.ActorActorCommunicationImpl#getParticipants <em>Participants</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ActorActorCommunicationImpl extends NamedElementImpl implements ActorActorCommunication {
	/**
     * The cached value of the '{@link #getParticipants() <em>Participants</em>}' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getParticipants()
     * @generated
     * @ordered
     */
	protected EList<Actor> participants;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected ActorActorCommunicationImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return EmPackage.Literals.ACTOR_ACTOR_COMMUNICATION;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public EList<Actor> getParticipants() {
        if (participants == null) {
            participants = new EObjectResolvingEList<Actor>(Actor.class, this, EmPackage.ACTOR_ACTOR_COMMUNICATION__PARTICIPANTS);
        }
        return participants;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case EmPackage.ACTOR_ACTOR_COMMUNICATION__PARTICIPANTS:
                return getParticipants();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case EmPackage.ACTOR_ACTOR_COMMUNICATION__PARTICIPANTS:
                getParticipants().clear();
                getParticipants().addAll((Collection<? extends Actor>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID) {
            case EmPackage.ACTOR_ACTOR_COMMUNICATION__PARTICIPANTS:
                getParticipants().clear();
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID) {
            case EmPackage.ACTOR_ACTOR_COMMUNICATION__PARTICIPANTS:
                return participants != null && !participants.isEmpty();
        }
        return super.eIsSet(featureID);
    }

} //ActorActorCommunicationImpl
