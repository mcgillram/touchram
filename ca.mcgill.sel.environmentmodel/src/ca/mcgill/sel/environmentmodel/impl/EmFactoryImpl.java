/**
 */
package ca.mcgill.sel.environmentmodel.impl;

import ca.mcgill.sel.environmentmodel.*;
import java.util.Map;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class EmFactoryImpl extends EFactoryImpl implements EmFactory {
	/**
     * Creates the default factory implementation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public static EmFactory init() {
        try {
            EmFactory theEmFactory = (EmFactory)EPackage.Registry.INSTANCE.getEFactory(EmPackage.eNS_URI);
            if (theEmFactory != null) {
                return theEmFactory;
            }
        }
        catch (Exception exception) {
            EcorePlugin.INSTANCE.log(exception);
        }
        return new EmFactoryImpl();
    }

	/**
     * Creates an instance of the factory.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public EmFactoryImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EObject create(EClass eClass) {
        switch (eClass.getClassifierID()) {
            case EmPackage.ENVIRONMENT_MODEL: return createEnvironmentModel();
            case EmPackage.TIME_TRIGGERED_EVENT: return createTimeTriggeredEvent();
            case EmPackage.ACTOR_ACTOR_COMMUNICATION: return createActorActorCommunication();
            case EmPackage.ACTOR: return createActor();
            case EmPackage.MESSAGE: return createMessage();
            case EmPackage.PARAMETER: return createParameter();
            case EmPackage.ACTOR_TYPE: return createActorType();
            case EmPackage.MESSAGE_TYPE: return createMessageType();
            case EmPackage.PARAMETER_TYPE: return createParameterType();
            case EmPackage.CONTAINER_MAP: return (EObject)createContainerMap();
            case EmPackage.LAYOUT: return createLayout();
            case EmPackage.ELEMENT_MAP: return (EObject)createElementMap();
            case EmPackage.LAYOUT_ELEMENT: return createLayoutElement();
            case EmPackage.NOTE: return createNote();
            default:
                throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
        }
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
        switch (eDataType.getClassifierID()) {
            case EmPackage.MESSAGE_DIRECTION:
                return createMessageDirectionFromString(eDataType, initialValue);
            default:
                throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
        }
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
        switch (eDataType.getClassifierID()) {
            case EmPackage.MESSAGE_DIRECTION:
                return convertMessageDirectionToString(eDataType, instanceValue);
            default:
                throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
        }
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public EnvironmentModel createEnvironmentModel() {
        EnvironmentModelImpl environmentModel = new EnvironmentModelImpl();
        return environmentModel;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public TimeTriggeredEvent createTimeTriggeredEvent() {
        TimeTriggeredEventImpl timeTriggeredEvent = new TimeTriggeredEventImpl();
        return timeTriggeredEvent;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public ActorActorCommunication createActorActorCommunication() {
        ActorActorCommunicationImpl actorActorCommunication = new ActorActorCommunicationImpl();
        return actorActorCommunication;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public Actor createActor() {
        ActorImpl actor = new ActorImpl();
        return actor;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public Message createMessage() {
        MessageImpl message = new MessageImpl();
        return message;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public Parameter createParameter() {
        ParameterImpl parameter = new ParameterImpl();
        return parameter;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public ActorType createActorType() {
        ActorTypeImpl actorType = new ActorTypeImpl();
        return actorType;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public MessageType createMessageType() {
        MessageTypeImpl messageType = new MessageTypeImpl();
        return messageType;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public ParameterType createParameterType() {
        ParameterTypeImpl parameterType = new ParameterTypeImpl();
        return parameterType;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public Map.Entry<EObject, EMap<EObject, LayoutElement>> createContainerMap() {
        ContainerMapImpl containerMap = new ContainerMapImpl();
        return containerMap;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public Layout createLayout() {
        LayoutImpl layout = new LayoutImpl();
        return layout;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public Map.Entry<EObject, LayoutElement> createElementMap() {
        ElementMapImpl elementMap = new ElementMapImpl();
        return elementMap;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public LayoutElement createLayoutElement() {
        LayoutElementImpl layoutElement = new LayoutElementImpl();
        return layoutElement;
    }

	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public Note createNote() {
        NoteImpl note = new NoteImpl();
        return note;
    }

    /**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public MessageDirection createMessageDirectionFromString(EDataType eDataType, String initialValue) {
        MessageDirection result = MessageDirection.get(initialValue);
        if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
        return result;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public String convertMessageDirectionToString(EDataType eDataType, Object instanceValue) {
        return instanceValue == null ? null : instanceValue.toString();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public EmPackage getEmPackage() {
        return (EmPackage)getEPackage();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @deprecated
     * @generated
     */
	@Deprecated
	public static EmPackage getPackage() {
        return EmPackage.eINSTANCE;
    }

} //EmFactoryImpl
