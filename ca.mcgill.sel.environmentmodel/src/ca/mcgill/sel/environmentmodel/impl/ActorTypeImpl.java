/**
 */
package ca.mcgill.sel.environmentmodel.impl;

import ca.mcgill.sel.environmentmodel.ActorType;
import ca.mcgill.sel.environmentmodel.EmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Actor Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ActorTypeImpl extends NamedElementImpl implements ActorType {
	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected ActorTypeImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return EmPackage.Literals.ACTOR_TYPE;
    }

} //ActorTypeImpl
