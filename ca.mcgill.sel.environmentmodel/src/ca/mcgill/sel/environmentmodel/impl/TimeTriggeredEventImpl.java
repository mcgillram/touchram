/**
 */
package ca.mcgill.sel.environmentmodel.impl;

import ca.mcgill.sel.environmentmodel.EmPackage;
import ca.mcgill.sel.environmentmodel.TimeTriggeredEvent;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Time Triggered Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TimeTriggeredEventImpl extends NamedElementImpl implements TimeTriggeredEvent {
	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected TimeTriggeredEventImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return EmPackage.Literals.TIME_TRIGGERED_EVENT;
    }

} //TimeTriggeredEventImpl
