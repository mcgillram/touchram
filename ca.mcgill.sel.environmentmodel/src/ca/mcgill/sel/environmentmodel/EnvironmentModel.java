/**
 */
package ca.mcgill.sel.environmentmodel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Environment Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.environmentmodel.EnvironmentModel#getActors <em>Actors</em>}</li>
 *   <li>{@link ca.mcgill.sel.environmentmodel.EnvironmentModel#getCommunications <em>Communications</em>}</li>
 *   <li>{@link ca.mcgill.sel.environmentmodel.EnvironmentModel#getSystemName <em>System Name</em>}</li>
 *   <li>{@link ca.mcgill.sel.environmentmodel.EnvironmentModel#getTimeTriggeredEvents <em>Time Triggered Events</em>}</li>
 *   <li>{@link ca.mcgill.sel.environmentmodel.EnvironmentModel#getActorTypes <em>Actor Types</em>}</li>
 *   <li>{@link ca.mcgill.sel.environmentmodel.EnvironmentModel#getParametertype <em>Parametertype</em>}</li>
 *   <li>{@link ca.mcgill.sel.environmentmodel.EnvironmentModel#getMessageTypes <em>Message Types</em>}</li>
 *   <li>{@link ca.mcgill.sel.environmentmodel.EnvironmentModel#getLayout <em>Layout</em>}</li>
 *   <li>{@link ca.mcgill.sel.environmentmodel.EnvironmentModel#getNotes <em>Notes</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.environmentmodel.EmPackage#getEnvironmentModel()
 * @model
 * @generated
 */
public interface EnvironmentModel extends NamedElement {
	/**
     * Returns the value of the '<em><b>Actors</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.environmentmodel.Actor}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Actors</em>' containment reference list.
     * @see ca.mcgill.sel.environmentmodel.EmPackage#getEnvironmentModel_Actors()
     * @model containment="true"
     * @generated
     */
	EList<Actor> getActors();

	/**
     * Returns the value of the '<em><b>Communications</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.environmentmodel.ActorActorCommunication}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Communications</em>' containment reference list.
     * @see ca.mcgill.sel.environmentmodel.EmPackage#getEnvironmentModel_Communications()
     * @model containment="true"
     * @generated
     */
	EList<ActorActorCommunication> getCommunications();

	/**
     * Returns the value of the '<em><b>System Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>System Name</em>' attribute.
     * @see #setSystemName(String)
     * @see ca.mcgill.sel.environmentmodel.EmPackage#getEnvironmentModel_SystemName()
     * @model
     * @generated
     */
	String getSystemName();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.environmentmodel.EnvironmentModel#getSystemName <em>System Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>System Name</em>' attribute.
     * @see #getSystemName()
     * @generated
     */
	void setSystemName(String value);

	/**
     * Returns the value of the '<em><b>Time Triggered Events</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.environmentmodel.TimeTriggeredEvent}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Time Triggered Events</em>' containment reference list.
     * @see ca.mcgill.sel.environmentmodel.EmPackage#getEnvironmentModel_TimeTriggeredEvents()
     * @model containment="true"
     * @generated
     */
	EList<TimeTriggeredEvent> getTimeTriggeredEvents();

	/**
     * Returns the value of the '<em><b>Actor Types</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.environmentmodel.ActorType}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Actor Types</em>' containment reference list.
     * @see ca.mcgill.sel.environmentmodel.EmPackage#getEnvironmentModel_ActorTypes()
     * @model containment="true"
     * @generated
     */
	EList<ActorType> getActorTypes();

	/**
     * Returns the value of the '<em><b>Parametertype</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.environmentmodel.ParameterType}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Parametertype</em>' containment reference list.
     * @see ca.mcgill.sel.environmentmodel.EmPackage#getEnvironmentModel_Parametertype()
     * @model containment="true"
     * @generated
     */
	EList<ParameterType> getParametertype();

	/**
     * Returns the value of the '<em><b>Message Types</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.environmentmodel.MessageType}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Message Types</em>' containment reference list.
     * @see ca.mcgill.sel.environmentmodel.EmPackage#getEnvironmentModel_MessageTypes()
     * @model containment="true"
     * @generated
     */
	EList<MessageType> getMessageTypes();

	/**
     * Returns the value of the '<em><b>Layout</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Layout</em>' containment reference.
     * @see #setLayout(Layout)
     * @see ca.mcgill.sel.environmentmodel.EmPackage#getEnvironmentModel_Layout()
     * @model containment="true"
     * @generated
     */
	Layout getLayout();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.environmentmodel.EnvironmentModel#getLayout <em>Layout</em>}' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Layout</em>' containment reference.
     * @see #getLayout()
     * @generated
     */
	void setLayout(Layout value);

    /**
     * Returns the value of the '<em><b>Notes</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.environmentmodel.Note}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Notes</em>' containment reference list.
     * @see ca.mcgill.sel.environmentmodel.EmPackage#getEnvironmentModel_Notes()
     * @model containment="true"
     * @generated
     */
    EList<Note> getNotes();

} // EnvironmentModel
