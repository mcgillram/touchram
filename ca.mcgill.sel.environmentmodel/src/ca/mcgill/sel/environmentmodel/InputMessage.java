/**
 */
package ca.mcgill.sel.environmentmodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Input Message</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.mcgill.sel.environmentmodel.EmPackage#getInputMessage()
 * @model
 * @generated
 */
public interface InputMessage extends Message {
} // InputMessage
