/**
 */
package ca.mcgill.sel.environmentmodel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Message Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.environmentmodel.MessageType#getParameters <em>Parameters</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.environmentmodel.EmPackage#getMessageType()
 * @model
 * @generated
 */
public interface MessageType extends NamedElement {
	/**
     * Returns the value of the '<em><b>Parameters</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.environmentmodel.Parameter}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Parameters</em>' containment reference list.
     * @see ca.mcgill.sel.environmentmodel.EmPackage#getMessageType_Parameters()
     * @model containment="true"
     * @generated
     */
	EList<Parameter> getParameters();

} // MessageType
