/**
 */
package ca.mcgill.sel.environmentmodel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Note</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.environmentmodel.Note#getContent <em>Content</em>}</li>
 *   <li>{@link ca.mcgill.sel.environmentmodel.Note#getAnnotatedElements <em>Annotated Elements</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.environmentmodel.EmPackage#getNote()
 * @model
 * @generated
 */
public interface Note extends EObject {
    /**
     * Returns the value of the '<em><b>Content</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Content</em>' attribute.
     * @see #setContent(String)
     * @see ca.mcgill.sel.environmentmodel.EmPackage#getNote_Content()
     * @model
     * @generated
     */
    String getContent();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.environmentmodel.Note#getContent <em>Content</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Content</em>' attribute.
     * @see #getContent()
     * @generated
     */
    void setContent(String value);

    /**
     * Returns the value of the '<em><b>Annotated Elements</b></em>' reference list.
     * The list contents are of type {@link ca.mcgill.sel.environmentmodel.NamedElement}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Annotated Elements</em>' reference list.
     * @see ca.mcgill.sel.environmentmodel.EmPackage#getNote_AnnotatedElements()
     * @model
     * @generated
     */
    EList<NamedElement> getAnnotatedElements();

} // Note
