/**
 */
package ca.mcgill.sel.environmentmodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>System Box</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.environmentmodel.SystemBox#getTimetriggeredevent <em>Timetriggeredevent</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.environmentmodel.EmPackage#getSystemBox()
 * @model
 * @generated
 */
public interface SystemBox extends Entity {
	/**
	 * Returns the value of the '<em><b>Timetriggeredevent</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Timetriggeredevent</em>' containment reference.
	 * @see #setTimetriggeredevent(TimeTriggeredEvent)
	 * @see ca.mcgill.sel.environmentmodel.EmPackage#getSystemBox_Timetriggeredevent()
	 * @model containment="true"
	 * @generated
	 */
	TimeTriggeredEvent getTimetriggeredevent();

	/**
	 * Sets the value of the '{@link ca.mcgill.sel.environmentmodel.SystemBox#getTimetriggeredevent <em>Timetriggeredevent</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Timetriggeredevent</em>' containment reference.
	 * @see #getTimetriggeredevent()
	 * @generated
	 */
	void setTimetriggeredevent(TimeTriggeredEvent value);

} // SystemBox
