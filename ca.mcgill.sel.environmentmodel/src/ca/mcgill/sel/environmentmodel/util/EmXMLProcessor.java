/**
 */
package ca.mcgill.sel.environmentmodel.util;

import ca.mcgill.sel.environmentmodel.EmPackage;

import java.util.Map;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.resource.Resource;

import org.eclipse.emf.ecore.xmi.util.XMLProcessor;

/**
 * This class contains helper methods to serialize and deserialize XML documents
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class EmXMLProcessor extends XMLProcessor {

	/**
     * Public constructor to instantiate the helper.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public EmXMLProcessor() {
        super((EPackage.Registry.INSTANCE));
        EmPackage.eINSTANCE.eClass();
    }
	
	/**
     * Register for "*" and "xml" file extensions the EmResourceFactoryImpl factory.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected Map<String, Resource.Factory> getRegistrations() {
        if (registrations == null) {
            super.getRegistrations();
            registrations.put(XML_EXTENSION, new EmResourceFactoryImpl());
            registrations.put(STAR_EXTENSION, new EmResourceFactoryImpl());
        }
        return registrations;
    }

} //EmXMLProcessor
