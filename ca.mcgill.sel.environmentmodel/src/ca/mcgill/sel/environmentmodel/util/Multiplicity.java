package ca.mcgill.sel.environmentmodel.util;

/**
 * A class representing a multiplicity.
 * Contains a lower and upper bound.
 * TODO: move somewhere common
 * @author rlanguay
 *
 */

public class Multiplicity {
    private int lowerBound;
    private int upperBound;   

    public Multiplicity() {
    }    

    public Multiplicity(int lowerBound, int upperBound) {
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
    }    

    public int getLowerBound() {
        return lowerBound;
    }

    public void setLowerBound(int lowerBound) {
        this.lowerBound = lowerBound;
    }

    public int getUpperBound() {
        return upperBound;
    }

    public void setUpperBound(int upperBound) {
        this.upperBound = upperBound;
    }
}