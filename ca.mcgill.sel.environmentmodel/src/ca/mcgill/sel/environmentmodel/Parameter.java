/**
 */
package ca.mcgill.sel.environmentmodel;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parameter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.environmentmodel.Parameter#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.environmentmodel.EmPackage#getParameter()
 * @model
 * @generated
 */
public interface Parameter extends NamedElement {
	/**
     * Returns the value of the '<em><b>Type</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Type</em>' reference.
     * @see #setType(ParameterType)
     * @see ca.mcgill.sel.environmentmodel.EmPackage#getParameter_Type()
     * @model required="true"
     * @generated
     */
	ParameterType getType();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.environmentmodel.Parameter#getType <em>Type</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Type</em>' reference.
     * @see #getType()
     * @generated
     */
	void setType(ParameterType value);

} // Parameter
