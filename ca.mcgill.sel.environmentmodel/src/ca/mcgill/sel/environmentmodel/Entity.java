/**
 */
package ca.mcgill.sel.environmentmodel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Entity</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.environmentmodel.Entity#getCommunicationEnds <em>Communication Ends</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.environmentmodel.EmPackage#getEntity()
 * @model abstract="true"
 * @generated
 */
public interface Entity extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Communication Ends</b></em>' containment reference list.
	 * The list contents are of type {@link ca.mcgill.sel.environmentmodel.CommunicationEnd}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Communication Ends</em>' containment reference list.
	 * @see ca.mcgill.sel.environmentmodel.EmPackage#getEntity_CommunicationEnds()
	 * @model containment="true"
	 * @generated
	 */
	EList<CommunicationEnd> getCommunicationEnds();

} // Entity
