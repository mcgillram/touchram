/**
 */
package ca.mcgill.sel.environmentmodel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Communication</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.environmentmodel.Communication#getCommunicationEnds <em>Communication Ends</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.environmentmodel.EmPackage#getCommunication()
 * @model abstract="true"
 * @generated
 */
public interface Communication extends EObject {
	/**
	 * Returns the value of the '<em><b>Communication Ends</b></em>' reference list.
	 * The list contents are of type {@link ca.mcgill.sel.environmentmodel.CommunicationEnd}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Communication Ends</em>' reference list.
	 * @see ca.mcgill.sel.environmentmodel.EmPackage#getCommunication_CommunicationEnds()
	 * @model lower="2" upper="2"
	 * @generated
	 */
	EList<CommunicationEnd> getCommunicationEnds();

} // Communication
