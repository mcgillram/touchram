/**
 */
package ca.mcgill.sel.environmentmodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Output Message</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.mcgill.sel.environmentmodel.EmPackage#getOutputMessage()
 * @model
 * @generated
 */
public interface OutputMessage extends Message {
} // OutputMessage
