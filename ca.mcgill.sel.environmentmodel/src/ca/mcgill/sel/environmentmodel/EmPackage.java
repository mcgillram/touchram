/**
 */
package ca.mcgill.sel.environmentmodel;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see ca.mcgill.sel.environmentmodel.EmFactory
 * @model kind="package"
 * @generated
 */
public interface EmPackage extends EPackage {
	/**
     * The package name.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	String eNAME = "environmentmodel";

	/**
     * The package namespace URI.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	String eNS_URI = "http://cs.mcgill.ca/sel/em/1.0";

	/**
     * The package namespace name.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	String eNS_PREFIX = "environmentmodel";

	/**
     * The singleton instance of the package.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	EmPackage eINSTANCE = ca.mcgill.sel.environmentmodel.impl.EmPackageImpl.init();

	/**
     * The meta object id for the '{@link ca.mcgill.sel.environmentmodel.impl.EnvironmentModelImpl <em>Environment Model</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.environmentmodel.impl.EnvironmentModelImpl
     * @see ca.mcgill.sel.environmentmodel.impl.EmPackageImpl#getEnvironmentModel()
     * @generated
     */
	int ENVIRONMENT_MODEL = 0;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.environmentmodel.impl.NamedElementImpl <em>Named Element</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.environmentmodel.impl.NamedElementImpl
     * @see ca.mcgill.sel.environmentmodel.impl.EmPackageImpl#getNamedElement()
     * @generated
     */
	int NAMED_ELEMENT = 4;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int NAMED_ELEMENT__NAME = 0;

	/**
     * The number of structural features of the '<em>Named Element</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int NAMED_ELEMENT_FEATURE_COUNT = 1;

	/**
     * The number of operations of the '<em>Named Element</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int NAMED_ELEMENT_OPERATION_COUNT = 0;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ENVIRONMENT_MODEL__NAME = NAMED_ELEMENT__NAME;

	/**
     * The feature id for the '<em><b>Actors</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ENVIRONMENT_MODEL__ACTORS = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>Communications</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ENVIRONMENT_MODEL__COMMUNICATIONS = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
     * The feature id for the '<em><b>System Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ENVIRONMENT_MODEL__SYSTEM_NAME = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
     * The feature id for the '<em><b>Time Triggered Events</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ENVIRONMENT_MODEL__TIME_TRIGGERED_EVENTS = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
     * The feature id for the '<em><b>Actor Types</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ENVIRONMENT_MODEL__ACTOR_TYPES = NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
     * The feature id for the '<em><b>Parametertype</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ENVIRONMENT_MODEL__PARAMETERTYPE = NAMED_ELEMENT_FEATURE_COUNT + 5;

	/**
     * The feature id for the '<em><b>Message Types</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ENVIRONMENT_MODEL__MESSAGE_TYPES = NAMED_ELEMENT_FEATURE_COUNT + 6;

	/**
     * The feature id for the '<em><b>Layout</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ENVIRONMENT_MODEL__LAYOUT = NAMED_ELEMENT_FEATURE_COUNT + 7;

	/**
     * The feature id for the '<em><b>Notes</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ENVIRONMENT_MODEL__NOTES = NAMED_ELEMENT_FEATURE_COUNT + 8;

    /**
     * The number of structural features of the '<em>Environment Model</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ENVIRONMENT_MODEL_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 9;

	/**
     * The number of operations of the '<em>Environment Model</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ENVIRONMENT_MODEL_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.environmentmodel.impl.TimeTriggeredEventImpl <em>Time Triggered Event</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.environmentmodel.impl.TimeTriggeredEventImpl
     * @see ca.mcgill.sel.environmentmodel.impl.EmPackageImpl#getTimeTriggeredEvent()
     * @generated
     */
	int TIME_TRIGGERED_EVENT = 1;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int TIME_TRIGGERED_EVENT__NAME = NAMED_ELEMENT__NAME;

	/**
     * The number of structural features of the '<em>Time Triggered Event</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int TIME_TRIGGERED_EVENT_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
     * The number of operations of the '<em>Time Triggered Event</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int TIME_TRIGGERED_EVENT_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.environmentmodel.impl.ActorActorCommunicationImpl <em>Actor Actor Communication</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.environmentmodel.impl.ActorActorCommunicationImpl
     * @see ca.mcgill.sel.environmentmodel.impl.EmPackageImpl#getActorActorCommunication()
     * @generated
     */
	int ACTOR_ACTOR_COMMUNICATION = 2;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTOR_ACTOR_COMMUNICATION__NAME = NAMED_ELEMENT__NAME;

	/**
     * The feature id for the '<em><b>Participants</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTOR_ACTOR_COMMUNICATION__PARTICIPANTS = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
     * The number of structural features of the '<em>Actor Actor Communication</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTOR_ACTOR_COMMUNICATION_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
     * The number of operations of the '<em>Actor Actor Communication</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTOR_ACTOR_COMMUNICATION_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.environmentmodel.impl.ActorImpl <em>Actor</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.environmentmodel.impl.ActorImpl
     * @see ca.mcgill.sel.environmentmodel.impl.EmPackageImpl#getActor()
     * @generated
     */
	int ACTOR = 3;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTOR__NAME = NAMED_ELEMENT__NAME;

	/**
     * The feature id for the '<em><b>Actor Lower Bound</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTOR__ACTOR_LOWER_BOUND = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>Actor Upper Bound</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTOR__ACTOR_UPPER_BOUND = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
     * The feature id for the '<em><b>Actor Type</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTOR__ACTOR_TYPE = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
     * The feature id for the '<em><b>Communication Lower Bound</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTOR__COMMUNICATION_LOWER_BOUND = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
     * The feature id for the '<em><b>Communication Upper Bound</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTOR__COMMUNICATION_UPPER_BOUND = NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
     * The feature id for the '<em><b>Messages</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTOR__MESSAGES = NAMED_ELEMENT_FEATURE_COUNT + 5;

	/**
     * The number of structural features of the '<em>Actor</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTOR_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 6;

	/**
     * The number of operations of the '<em>Actor</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTOR_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.environmentmodel.impl.MessageImpl <em>Message</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.environmentmodel.impl.MessageImpl
     * @see ca.mcgill.sel.environmentmodel.impl.EmPackageImpl#getMessage()
     * @generated
     */
	int MESSAGE = 5;

	/**
     * The feature id for the '<em><b>Message Type</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MESSAGE__MESSAGE_TYPE = 0;

	/**
     * The feature id for the '<em><b>Message Direction</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MESSAGE__MESSAGE_DIRECTION = 1;

	/**
     * The number of structural features of the '<em>Message</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MESSAGE_FEATURE_COUNT = 2;

	/**
     * The number of operations of the '<em>Message</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MESSAGE_OPERATION_COUNT = 0;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.environmentmodel.impl.ParameterImpl <em>Parameter</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.environmentmodel.impl.ParameterImpl
     * @see ca.mcgill.sel.environmentmodel.impl.EmPackageImpl#getParameter()
     * @generated
     */
	int PARAMETER = 6;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PARAMETER__NAME = NAMED_ELEMENT__NAME;

	/**
     * The feature id for the '<em><b>Type</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PARAMETER__TYPE = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
     * The number of structural features of the '<em>Parameter</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PARAMETER_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
     * The number of operations of the '<em>Parameter</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PARAMETER_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;


	/**
     * The meta object id for the '{@link ca.mcgill.sel.environmentmodel.impl.ActorTypeImpl <em>Actor Type</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.environmentmodel.impl.ActorTypeImpl
     * @see ca.mcgill.sel.environmentmodel.impl.EmPackageImpl#getActorType()
     * @generated
     */
	int ACTOR_TYPE = 7;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTOR_TYPE__NAME = NAMED_ELEMENT__NAME;

	/**
     * The number of structural features of the '<em>Actor Type</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTOR_TYPE_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
     * The number of operations of the '<em>Actor Type</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTOR_TYPE_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.environmentmodel.impl.MessageTypeImpl <em>Message Type</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.environmentmodel.impl.MessageTypeImpl
     * @see ca.mcgill.sel.environmentmodel.impl.EmPackageImpl#getMessageType()
     * @generated
     */
	int MESSAGE_TYPE = 8;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MESSAGE_TYPE__NAME = NAMED_ELEMENT__NAME;

	/**
     * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MESSAGE_TYPE__PARAMETERS = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
     * The number of structural features of the '<em>Message Type</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MESSAGE_TYPE_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
     * The number of operations of the '<em>Message Type</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MESSAGE_TYPE_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.environmentmodel.impl.ParameterTypeImpl <em>Parameter Type</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.environmentmodel.impl.ParameterTypeImpl
     * @see ca.mcgill.sel.environmentmodel.impl.EmPackageImpl#getParameterType()
     * @generated
     */
	int PARAMETER_TYPE = 9;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PARAMETER_TYPE__NAME = NAMED_ELEMENT__NAME;

	/**
     * The number of structural features of the '<em>Parameter Type</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PARAMETER_TYPE_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
     * The number of operations of the '<em>Parameter Type</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PARAMETER_TYPE_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.environmentmodel.impl.ContainerMapImpl <em>Container Map</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.environmentmodel.impl.ContainerMapImpl
     * @see ca.mcgill.sel.environmentmodel.impl.EmPackageImpl#getContainerMap()
     * @generated
     */
	int CONTAINER_MAP = 10;

	/**
     * The feature id for the '<em><b>Key</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CONTAINER_MAP__KEY = 0;

	/**
     * The feature id for the '<em><b>Value</b></em>' map.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CONTAINER_MAP__VALUE = 1;

	/**
     * The number of structural features of the '<em>Container Map</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CONTAINER_MAP_FEATURE_COUNT = 2;

	/**
     * The number of operations of the '<em>Container Map</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CONTAINER_MAP_OPERATION_COUNT = 0;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.environmentmodel.impl.LayoutImpl <em>Layout</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.environmentmodel.impl.LayoutImpl
     * @see ca.mcgill.sel.environmentmodel.impl.EmPackageImpl#getLayout()
     * @generated
     */
	int LAYOUT = 11;

	/**
     * The feature id for the '<em><b>Containers</b></em>' map.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int LAYOUT__CONTAINERS = 0;

	/**
     * The number of structural features of the '<em>Layout</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int LAYOUT_FEATURE_COUNT = 1;

	/**
     * The number of operations of the '<em>Layout</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int LAYOUT_OPERATION_COUNT = 0;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.environmentmodel.impl.ElementMapImpl <em>Element Map</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.environmentmodel.impl.ElementMapImpl
     * @see ca.mcgill.sel.environmentmodel.impl.EmPackageImpl#getElementMap()
     * @generated
     */
	int ELEMENT_MAP = 12;

	/**
     * The feature id for the '<em><b>Key</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ELEMENT_MAP__KEY = 0;

	/**
     * The feature id for the '<em><b>Value</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ELEMENT_MAP__VALUE = 1;

	/**
     * The number of structural features of the '<em>Element Map</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ELEMENT_MAP_FEATURE_COUNT = 2;

	/**
     * The number of operations of the '<em>Element Map</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ELEMENT_MAP_OPERATION_COUNT = 0;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.environmentmodel.impl.LayoutElementImpl <em>Layout Element</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.environmentmodel.impl.LayoutElementImpl
     * @see ca.mcgill.sel.environmentmodel.impl.EmPackageImpl#getLayoutElement()
     * @generated
     */
	int LAYOUT_ELEMENT = 13;

	/**
     * The feature id for the '<em><b>X</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int LAYOUT_ELEMENT__X = 0;

	/**
     * The feature id for the '<em><b>Y</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int LAYOUT_ELEMENT__Y = 1;

	/**
     * The number of structural features of the '<em>Layout Element</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int LAYOUT_ELEMENT_FEATURE_COUNT = 2;

	/**
     * The number of operations of the '<em>Layout Element</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int LAYOUT_ELEMENT_OPERATION_COUNT = 0;

	/**
     * The meta object id for the '{@link ca.mcgill.sel.environmentmodel.impl.NoteImpl <em>Note</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.environmentmodel.impl.NoteImpl
     * @see ca.mcgill.sel.environmentmodel.impl.EmPackageImpl#getNote()
     * @generated
     */
    int NOTE = 14;

    /**
     * The feature id for the '<em><b>Content</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NOTE__CONTENT = 0;

    /**
     * The feature id for the '<em><b>Annotated Elements</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NOTE__ANNOTATED_ELEMENTS = 1;

    /**
     * The number of structural features of the '<em>Note</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NOTE_FEATURE_COUNT = 2;

    /**
     * The number of operations of the '<em>Note</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NOTE_OPERATION_COUNT = 0;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.environmentmodel.MessageDirection <em>Message Direction</em>}' enum.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see ca.mcgill.sel.environmentmodel.MessageDirection
     * @see ca.mcgill.sel.environmentmodel.impl.EmPackageImpl#getMessageDirection()
     * @generated
     */
	int MESSAGE_DIRECTION = 15;


	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.environmentmodel.EnvironmentModel <em>Environment Model</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Environment Model</em>'.
     * @see ca.mcgill.sel.environmentmodel.EnvironmentModel
     * @generated
     */
	EClass getEnvironmentModel();

	/**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.environmentmodel.EnvironmentModel#getActors <em>Actors</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Actors</em>'.
     * @see ca.mcgill.sel.environmentmodel.EnvironmentModel#getActors()
     * @see #getEnvironmentModel()
     * @generated
     */
	EReference getEnvironmentModel_Actors();

	/**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.environmentmodel.EnvironmentModel#getCommunications <em>Communications</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Communications</em>'.
     * @see ca.mcgill.sel.environmentmodel.EnvironmentModel#getCommunications()
     * @see #getEnvironmentModel()
     * @generated
     */
	EReference getEnvironmentModel_Communications();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.environmentmodel.EnvironmentModel#getSystemName <em>System Name</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>System Name</em>'.
     * @see ca.mcgill.sel.environmentmodel.EnvironmentModel#getSystemName()
     * @see #getEnvironmentModel()
     * @generated
     */
	EAttribute getEnvironmentModel_SystemName();

	/**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.environmentmodel.EnvironmentModel#getTimeTriggeredEvents <em>Time Triggered Events</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Time Triggered Events</em>'.
     * @see ca.mcgill.sel.environmentmodel.EnvironmentModel#getTimeTriggeredEvents()
     * @see #getEnvironmentModel()
     * @generated
     */
	EReference getEnvironmentModel_TimeTriggeredEvents();

	/**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.environmentmodel.EnvironmentModel#getActorTypes <em>Actor Types</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Actor Types</em>'.
     * @see ca.mcgill.sel.environmentmodel.EnvironmentModel#getActorTypes()
     * @see #getEnvironmentModel()
     * @generated
     */
	EReference getEnvironmentModel_ActorTypes();

	/**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.environmentmodel.EnvironmentModel#getParametertype <em>Parametertype</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Parametertype</em>'.
     * @see ca.mcgill.sel.environmentmodel.EnvironmentModel#getParametertype()
     * @see #getEnvironmentModel()
     * @generated
     */
	EReference getEnvironmentModel_Parametertype();

	/**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.environmentmodel.EnvironmentModel#getMessageTypes <em>Message Types</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Message Types</em>'.
     * @see ca.mcgill.sel.environmentmodel.EnvironmentModel#getMessageTypes()
     * @see #getEnvironmentModel()
     * @generated
     */
	EReference getEnvironmentModel_MessageTypes();

	/**
     * Returns the meta object for the containment reference '{@link ca.mcgill.sel.environmentmodel.EnvironmentModel#getLayout <em>Layout</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference '<em>Layout</em>'.
     * @see ca.mcgill.sel.environmentmodel.EnvironmentModel#getLayout()
     * @see #getEnvironmentModel()
     * @generated
     */
	EReference getEnvironmentModel_Layout();

	/**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.environmentmodel.EnvironmentModel#getNotes <em>Notes</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Notes</em>'.
     * @see ca.mcgill.sel.environmentmodel.EnvironmentModel#getNotes()
     * @see #getEnvironmentModel()
     * @generated
     */
    EReference getEnvironmentModel_Notes();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.environmentmodel.TimeTriggeredEvent <em>Time Triggered Event</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Time Triggered Event</em>'.
     * @see ca.mcgill.sel.environmentmodel.TimeTriggeredEvent
     * @generated
     */
	EClass getTimeTriggeredEvent();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.environmentmodel.ActorActorCommunication <em>Actor Actor Communication</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Actor Actor Communication</em>'.
     * @see ca.mcgill.sel.environmentmodel.ActorActorCommunication
     * @generated
     */
	EClass getActorActorCommunication();

	/**
     * Returns the meta object for the reference list '{@link ca.mcgill.sel.environmentmodel.ActorActorCommunication#getParticipants <em>Participants</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Participants</em>'.
     * @see ca.mcgill.sel.environmentmodel.ActorActorCommunication#getParticipants()
     * @see #getActorActorCommunication()
     * @generated
     */
	EReference getActorActorCommunication_Participants();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.environmentmodel.Actor <em>Actor</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Actor</em>'.
     * @see ca.mcgill.sel.environmentmodel.Actor
     * @generated
     */
	EClass getActor();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.environmentmodel.Actor#getActorLowerBound <em>Actor Lower Bound</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Actor Lower Bound</em>'.
     * @see ca.mcgill.sel.environmentmodel.Actor#getActorLowerBound()
     * @see #getActor()
     * @generated
     */
	EAttribute getActor_ActorLowerBound();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.environmentmodel.Actor#getActorUpperBound <em>Actor Upper Bound</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Actor Upper Bound</em>'.
     * @see ca.mcgill.sel.environmentmodel.Actor#getActorUpperBound()
     * @see #getActor()
     * @generated
     */
	EAttribute getActor_ActorUpperBound();

	/**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.environmentmodel.Actor#getActorType <em>Actor Type</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Actor Type</em>'.
     * @see ca.mcgill.sel.environmentmodel.Actor#getActorType()
     * @see #getActor()
     * @generated
     */
	EReference getActor_ActorType();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.environmentmodel.Actor#getCommunicationLowerBound <em>Communication Lower Bound</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Communication Lower Bound</em>'.
     * @see ca.mcgill.sel.environmentmodel.Actor#getCommunicationLowerBound()
     * @see #getActor()
     * @generated
     */
	EAttribute getActor_CommunicationLowerBound();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.environmentmodel.Actor#getCommunicationUpperBound <em>Communication Upper Bound</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Communication Upper Bound</em>'.
     * @see ca.mcgill.sel.environmentmodel.Actor#getCommunicationUpperBound()
     * @see #getActor()
     * @generated
     */
	EAttribute getActor_CommunicationUpperBound();

	/**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.environmentmodel.Actor#getMessages <em>Messages</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Messages</em>'.
     * @see ca.mcgill.sel.environmentmodel.Actor#getMessages()
     * @see #getActor()
     * @generated
     */
	EReference getActor_Messages();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.environmentmodel.NamedElement <em>Named Element</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Named Element</em>'.
     * @see ca.mcgill.sel.environmentmodel.NamedElement
     * @generated
     */
	EClass getNamedElement();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.environmentmodel.NamedElement#getName <em>Name</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see ca.mcgill.sel.environmentmodel.NamedElement#getName()
     * @see #getNamedElement()
     * @generated
     */
	EAttribute getNamedElement_Name();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.environmentmodel.Message <em>Message</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Message</em>'.
     * @see ca.mcgill.sel.environmentmodel.Message
     * @generated
     */
	EClass getMessage();

	/**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.environmentmodel.Message#getMessageType <em>Message Type</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Message Type</em>'.
     * @see ca.mcgill.sel.environmentmodel.Message#getMessageType()
     * @see #getMessage()
     * @generated
     */
	EReference getMessage_MessageType();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.environmentmodel.Message#getMessageDirection <em>Message Direction</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Message Direction</em>'.
     * @see ca.mcgill.sel.environmentmodel.Message#getMessageDirection()
     * @see #getMessage()
     * @generated
     */
	EAttribute getMessage_MessageDirection();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.environmentmodel.Parameter <em>Parameter</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Parameter</em>'.
     * @see ca.mcgill.sel.environmentmodel.Parameter
     * @generated
     */
	EClass getParameter();

	/**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.environmentmodel.Parameter#getType <em>Type</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Type</em>'.
     * @see ca.mcgill.sel.environmentmodel.Parameter#getType()
     * @see #getParameter()
     * @generated
     */
	EReference getParameter_Type();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.environmentmodel.ActorType <em>Actor Type</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Actor Type</em>'.
     * @see ca.mcgill.sel.environmentmodel.ActorType
     * @generated
     */
	EClass getActorType();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.environmentmodel.MessageType <em>Message Type</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Message Type</em>'.
     * @see ca.mcgill.sel.environmentmodel.MessageType
     * @generated
     */
	EClass getMessageType();

	/**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.environmentmodel.MessageType#getParameters <em>Parameters</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Parameters</em>'.
     * @see ca.mcgill.sel.environmentmodel.MessageType#getParameters()
     * @see #getMessageType()
     * @generated
     */
	EReference getMessageType_Parameters();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.environmentmodel.ParameterType <em>Parameter Type</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Parameter Type</em>'.
     * @see ca.mcgill.sel.environmentmodel.ParameterType
     * @generated
     */
	EClass getParameterType();

	/**
     * Returns the meta object for class '{@link java.util.Map.Entry <em>Container Map</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Container Map</em>'.
     * @see java.util.Map.Entry
     * @model keyType="org.eclipse.emf.ecore.EObject" keyRequired="true"
     *        valueMapType="ca.mcgill.sel.environmentmodel.ElementMap&lt;org.eclipse.emf.ecore.EObject, ca.mcgill.sel.environmentmodel.LayoutElement&gt;"
     * @generated
     */
	EClass getContainerMap();

	/**
     * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Key</em>'.
     * @see java.util.Map.Entry
     * @see #getContainerMap()
     * @generated
     */
	EReference getContainerMap_Key();

	/**
     * Returns the meta object for the map '{@link java.util.Map.Entry <em>Value</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the map '<em>Value</em>'.
     * @see java.util.Map.Entry
     * @see #getContainerMap()
     * @generated
     */
	EReference getContainerMap_Value();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.environmentmodel.Layout <em>Layout</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Layout</em>'.
     * @see ca.mcgill.sel.environmentmodel.Layout
     * @generated
     */
	EClass getLayout();

	/**
     * Returns the meta object for the map '{@link ca.mcgill.sel.environmentmodel.Layout#getContainers <em>Containers</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the map '<em>Containers</em>'.
     * @see ca.mcgill.sel.environmentmodel.Layout#getContainers()
     * @see #getLayout()
     * @generated
     */
	EReference getLayout_Containers();

	/**
     * Returns the meta object for class '{@link java.util.Map.Entry <em>Element Map</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Element Map</em>'.
     * @see java.util.Map.Entry
     * @model keyType="org.eclipse.emf.ecore.EObject" keyRequired="true"
     *        valueType="ca.mcgill.sel.environmentmodel.LayoutElement" valueRequired="true"
     * @generated
     */
	EClass getElementMap();

	/**
     * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Key</em>'.
     * @see java.util.Map.Entry
     * @see #getElementMap()
     * @generated
     */
	EReference getElementMap_Key();

	/**
     * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Value</em>'.
     * @see java.util.Map.Entry
     * @see #getElementMap()
     * @generated
     */
	EReference getElementMap_Value();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.environmentmodel.LayoutElement <em>Layout Element</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Layout Element</em>'.
     * @see ca.mcgill.sel.environmentmodel.LayoutElement
     * @generated
     */
	EClass getLayoutElement();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.environmentmodel.LayoutElement#getX <em>X</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>X</em>'.
     * @see ca.mcgill.sel.environmentmodel.LayoutElement#getX()
     * @see #getLayoutElement()
     * @generated
     */
	EAttribute getLayoutElement_X();

	/**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.environmentmodel.LayoutElement#getY <em>Y</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Y</em>'.
     * @see ca.mcgill.sel.environmentmodel.LayoutElement#getY()
     * @see #getLayoutElement()
     * @generated
     */
	EAttribute getLayoutElement_Y();

	/**
     * Returns the meta object for class '{@link ca.mcgill.sel.environmentmodel.Note <em>Note</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Note</em>'.
     * @see ca.mcgill.sel.environmentmodel.Note
     * @generated
     */
    EClass getNote();

    /**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.environmentmodel.Note#getContent <em>Content</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Content</em>'.
     * @see ca.mcgill.sel.environmentmodel.Note#getContent()
     * @see #getNote()
     * @generated
     */
    EAttribute getNote_Content();

    /**
     * Returns the meta object for the reference list '{@link ca.mcgill.sel.environmentmodel.Note#getAnnotatedElements <em>Annotated Elements</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Annotated Elements</em>'.
     * @see ca.mcgill.sel.environmentmodel.Note#getAnnotatedElements()
     * @see #getNote()
     * @generated
     */
    EReference getNote_AnnotatedElements();

    /**
     * Returns the meta object for enum '{@link ca.mcgill.sel.environmentmodel.MessageDirection <em>Message Direction</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for enum '<em>Message Direction</em>'.
     * @see ca.mcgill.sel.environmentmodel.MessageDirection
     * @generated
     */
	EEnum getMessageDirection();

	/**
     * Returns the factory that creates the instances of the model.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the factory that creates the instances of the model.
     * @generated
     */
	EmFactory getEmFactory();

	/**
     * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
     * @generated
     */
	interface Literals {
		/**
         * The meta object literal for the '{@link ca.mcgill.sel.environmentmodel.impl.EnvironmentModelImpl <em>Environment Model</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.environmentmodel.impl.EnvironmentModelImpl
         * @see ca.mcgill.sel.environmentmodel.impl.EmPackageImpl#getEnvironmentModel()
         * @generated
         */
		EClass ENVIRONMENT_MODEL = eINSTANCE.getEnvironmentModel();

		/**
         * The meta object literal for the '<em><b>Actors</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference ENVIRONMENT_MODEL__ACTORS = eINSTANCE.getEnvironmentModel_Actors();

		/**
         * The meta object literal for the '<em><b>Communications</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference ENVIRONMENT_MODEL__COMMUNICATIONS = eINSTANCE.getEnvironmentModel_Communications();

		/**
         * The meta object literal for the '<em><b>System Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute ENVIRONMENT_MODEL__SYSTEM_NAME = eINSTANCE.getEnvironmentModel_SystemName();

		/**
         * The meta object literal for the '<em><b>Time Triggered Events</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference ENVIRONMENT_MODEL__TIME_TRIGGERED_EVENTS = eINSTANCE.getEnvironmentModel_TimeTriggeredEvents();

		/**
         * The meta object literal for the '<em><b>Actor Types</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference ENVIRONMENT_MODEL__ACTOR_TYPES = eINSTANCE.getEnvironmentModel_ActorTypes();

		/**
         * The meta object literal for the '<em><b>Parametertype</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference ENVIRONMENT_MODEL__PARAMETERTYPE = eINSTANCE.getEnvironmentModel_Parametertype();

		/**
         * The meta object literal for the '<em><b>Message Types</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference ENVIRONMENT_MODEL__MESSAGE_TYPES = eINSTANCE.getEnvironmentModel_MessageTypes();

		/**
         * The meta object literal for the '<em><b>Layout</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference ENVIRONMENT_MODEL__LAYOUT = eINSTANCE.getEnvironmentModel_Layout();

		/**
         * The meta object literal for the '<em><b>Notes</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference ENVIRONMENT_MODEL__NOTES = eINSTANCE.getEnvironmentModel_Notes();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.environmentmodel.impl.TimeTriggeredEventImpl <em>Time Triggered Event</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.environmentmodel.impl.TimeTriggeredEventImpl
         * @see ca.mcgill.sel.environmentmodel.impl.EmPackageImpl#getTimeTriggeredEvent()
         * @generated
         */
		EClass TIME_TRIGGERED_EVENT = eINSTANCE.getTimeTriggeredEvent();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.environmentmodel.impl.ActorActorCommunicationImpl <em>Actor Actor Communication</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.environmentmodel.impl.ActorActorCommunicationImpl
         * @see ca.mcgill.sel.environmentmodel.impl.EmPackageImpl#getActorActorCommunication()
         * @generated
         */
		EClass ACTOR_ACTOR_COMMUNICATION = eINSTANCE.getActorActorCommunication();

		/**
         * The meta object literal for the '<em><b>Participants</b></em>' reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference ACTOR_ACTOR_COMMUNICATION__PARTICIPANTS = eINSTANCE.getActorActorCommunication_Participants();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.environmentmodel.impl.ActorImpl <em>Actor</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.environmentmodel.impl.ActorImpl
         * @see ca.mcgill.sel.environmentmodel.impl.EmPackageImpl#getActor()
         * @generated
         */
		EClass ACTOR = eINSTANCE.getActor();

		/**
         * The meta object literal for the '<em><b>Actor Lower Bound</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute ACTOR__ACTOR_LOWER_BOUND = eINSTANCE.getActor_ActorLowerBound();

		/**
         * The meta object literal for the '<em><b>Actor Upper Bound</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute ACTOR__ACTOR_UPPER_BOUND = eINSTANCE.getActor_ActorUpperBound();

		/**
         * The meta object literal for the '<em><b>Actor Type</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference ACTOR__ACTOR_TYPE = eINSTANCE.getActor_ActorType();

		/**
         * The meta object literal for the '<em><b>Communication Lower Bound</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute ACTOR__COMMUNICATION_LOWER_BOUND = eINSTANCE.getActor_CommunicationLowerBound();

		/**
         * The meta object literal for the '<em><b>Communication Upper Bound</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute ACTOR__COMMUNICATION_UPPER_BOUND = eINSTANCE.getActor_CommunicationUpperBound();

		/**
         * The meta object literal for the '<em><b>Messages</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference ACTOR__MESSAGES = eINSTANCE.getActor_Messages();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.environmentmodel.impl.NamedElementImpl <em>Named Element</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.environmentmodel.impl.NamedElementImpl
         * @see ca.mcgill.sel.environmentmodel.impl.EmPackageImpl#getNamedElement()
         * @generated
         */
		EClass NAMED_ELEMENT = eINSTANCE.getNamedElement();

		/**
         * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute NAMED_ELEMENT__NAME = eINSTANCE.getNamedElement_Name();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.environmentmodel.impl.MessageImpl <em>Message</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.environmentmodel.impl.MessageImpl
         * @see ca.mcgill.sel.environmentmodel.impl.EmPackageImpl#getMessage()
         * @generated
         */
		EClass MESSAGE = eINSTANCE.getMessage();

		/**
         * The meta object literal for the '<em><b>Message Type</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference MESSAGE__MESSAGE_TYPE = eINSTANCE.getMessage_MessageType();

		/**
         * The meta object literal for the '<em><b>Message Direction</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute MESSAGE__MESSAGE_DIRECTION = eINSTANCE.getMessage_MessageDirection();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.environmentmodel.impl.ParameterImpl <em>Parameter</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.environmentmodel.impl.ParameterImpl
         * @see ca.mcgill.sel.environmentmodel.impl.EmPackageImpl#getParameter()
         * @generated
         */
		EClass PARAMETER = eINSTANCE.getParameter();

		/**
         * The meta object literal for the '<em><b>Type</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference PARAMETER__TYPE = eINSTANCE.getParameter_Type();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.environmentmodel.impl.ActorTypeImpl <em>Actor Type</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.environmentmodel.impl.ActorTypeImpl
         * @see ca.mcgill.sel.environmentmodel.impl.EmPackageImpl#getActorType()
         * @generated
         */
		EClass ACTOR_TYPE = eINSTANCE.getActorType();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.environmentmodel.impl.MessageTypeImpl <em>Message Type</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.environmentmodel.impl.MessageTypeImpl
         * @see ca.mcgill.sel.environmentmodel.impl.EmPackageImpl#getMessageType()
         * @generated
         */
		EClass MESSAGE_TYPE = eINSTANCE.getMessageType();

		/**
         * The meta object literal for the '<em><b>Parameters</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference MESSAGE_TYPE__PARAMETERS = eINSTANCE.getMessageType_Parameters();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.environmentmodel.impl.ParameterTypeImpl <em>Parameter Type</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.environmentmodel.impl.ParameterTypeImpl
         * @see ca.mcgill.sel.environmentmodel.impl.EmPackageImpl#getParameterType()
         * @generated
         */
		EClass PARAMETER_TYPE = eINSTANCE.getParameterType();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.environmentmodel.impl.ContainerMapImpl <em>Container Map</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.environmentmodel.impl.ContainerMapImpl
         * @see ca.mcgill.sel.environmentmodel.impl.EmPackageImpl#getContainerMap()
         * @generated
         */
		EClass CONTAINER_MAP = eINSTANCE.getContainerMap();

		/**
         * The meta object literal for the '<em><b>Key</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CONTAINER_MAP__KEY = eINSTANCE.getContainerMap_Key();

		/**
         * The meta object literal for the '<em><b>Value</b></em>' map feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference CONTAINER_MAP__VALUE = eINSTANCE.getContainerMap_Value();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.environmentmodel.impl.LayoutImpl <em>Layout</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.environmentmodel.impl.LayoutImpl
         * @see ca.mcgill.sel.environmentmodel.impl.EmPackageImpl#getLayout()
         * @generated
         */
		EClass LAYOUT = eINSTANCE.getLayout();

		/**
         * The meta object literal for the '<em><b>Containers</b></em>' map feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference LAYOUT__CONTAINERS = eINSTANCE.getLayout_Containers();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.environmentmodel.impl.ElementMapImpl <em>Element Map</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.environmentmodel.impl.ElementMapImpl
         * @see ca.mcgill.sel.environmentmodel.impl.EmPackageImpl#getElementMap()
         * @generated
         */
		EClass ELEMENT_MAP = eINSTANCE.getElementMap();

		/**
         * The meta object literal for the '<em><b>Key</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference ELEMENT_MAP__KEY = eINSTANCE.getElementMap_Key();

		/**
         * The meta object literal for the '<em><b>Value</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference ELEMENT_MAP__VALUE = eINSTANCE.getElementMap_Value();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.environmentmodel.impl.LayoutElementImpl <em>Layout Element</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.environmentmodel.impl.LayoutElementImpl
         * @see ca.mcgill.sel.environmentmodel.impl.EmPackageImpl#getLayoutElement()
         * @generated
         */
		EClass LAYOUT_ELEMENT = eINSTANCE.getLayoutElement();

		/**
         * The meta object literal for the '<em><b>X</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute LAYOUT_ELEMENT__X = eINSTANCE.getLayoutElement_X();

		/**
         * The meta object literal for the '<em><b>Y</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute LAYOUT_ELEMENT__Y = eINSTANCE.getLayoutElement_Y();

		/**
         * The meta object literal for the '{@link ca.mcgill.sel.environmentmodel.impl.NoteImpl <em>Note</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.environmentmodel.impl.NoteImpl
         * @see ca.mcgill.sel.environmentmodel.impl.EmPackageImpl#getNote()
         * @generated
         */
        EClass NOTE = eINSTANCE.getNote();

        /**
         * The meta object literal for the '<em><b>Content</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute NOTE__CONTENT = eINSTANCE.getNote_Content();

        /**
         * The meta object literal for the '<em><b>Annotated Elements</b></em>' reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference NOTE__ANNOTATED_ELEMENTS = eINSTANCE.getNote_AnnotatedElements();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.environmentmodel.MessageDirection <em>Message Direction</em>}' enum.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see ca.mcgill.sel.environmentmodel.MessageDirection
         * @see ca.mcgill.sel.environmentmodel.impl.EmPackageImpl#getMessageDirection()
         * @generated
         */
		EEnum MESSAGE_DIRECTION = eINSTANCE.getMessageDirection();

	}

} //EmPackage
