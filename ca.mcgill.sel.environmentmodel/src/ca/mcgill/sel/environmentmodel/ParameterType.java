/**
 */
package ca.mcgill.sel.environmentmodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parameter Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.mcgill.sel.environmentmodel.EmPackage#getParameterType()
 * @model
 * @generated
 */
public interface ParameterType extends NamedElement {
} // ParameterType
