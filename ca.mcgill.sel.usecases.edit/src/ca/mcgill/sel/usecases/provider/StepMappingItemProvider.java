/**
 */
package ca.mcgill.sel.usecases.provider;

import ca.mcgill.sel.core.COREMapping;
import ca.mcgill.sel.core.CorePackage;

import ca.mcgill.sel.core.provider.COREMappingItemProvider;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.core.util.COREModelUtil;
import ca.mcgill.sel.usecases.Flow;
import ca.mcgill.sel.usecases.FlowMapping;
import ca.mcgill.sel.usecases.Step;
import ca.mcgill.sel.usecases.StepMapping;
import ca.mcgill.sel.usecases.UcFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;

/**
 * This is the item provider adapter for a {@link ca.mcgill.sel.usecases.StepMapping} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class StepMappingItemProvider extends COREMappingItemProvider {
    /**
     * This constructs an instance from a factory and a notifier.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public StepMappingItemProvider(AdapterFactory adapterFactory) {
        super(adapterFactory);
    }

	/**
     * This returns the property descriptors for the adapted class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
        if (itemPropertyDescriptors == null) {
            super.getPropertyDescriptors(object);

        }
        return itemPropertyDescriptors;
    }

	/**
     * This returns StepMapping.gif.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object getImage(Object object) {
        return overlayImage(object, getResourceLocator().getImage("full/obj16/StepMapping"));
    }

	/**
     * This returns the label text for the adapted class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String getText(Object object) {
        return getString("_UI_StepMapping_type");
    }


	/**
     * This handles model notifications by calling {@link #updateChildren} to update any cached
     * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void notifyChanged(Notification notification) {
        updateChildren(notification);
        super.notifyChanged(notification);
    }

	/**
     * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
     * that can be created under this object.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
        super.collectNewChildDescriptors(newChildDescriptors, object);

        newChildDescriptors.add
            (createChildParameter
                (CorePackage.Literals.CORE_MAPPING__MAPPINGS,
                 UcFactory.eINSTANCE.createActorMapping()));

        newChildDescriptors.add
            (createChildParameter
                (CorePackage.Literals.CORE_MAPPING__MAPPINGS,
                 UcFactory.eINSTANCE.createUseCaseMapping()));

        newChildDescriptors.add
            (createChildParameter
                (CorePackage.Literals.CORE_MAPPING__MAPPINGS,
                 UcFactory.eINSTANCE.createStepMapping()));

        newChildDescriptors.add
            (createChildParameter
                (CorePackage.Literals.CORE_MAPPING__MAPPINGS,
                 UcFactory.eINSTANCE.createFlowMapping()));
    }

	/**
     * Return the resource locator for this item provider's resources.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public ResourceLocator getResourceLocator() {
        return UseCasesEditPlugin.INSTANCE;
    }
    
    /**
     * This adds a property descriptor for the From feature.
     * <!-- begin-user-doc -->
     * 
     * @param object the object to add a property descriptor for
     *            <!-- end-user-doc -->
     * @generated NOT
     */
    @Override
    protected void addFromPropertyDescriptor(Object object) {
        itemPropertyDescriptors.add(
            // CHECKSTYLE:IGNORE AnonInnerLength: Okay here.
            new MappingFromItemPropertyDescriptor(
                    ((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
                    getResourceLocator(),
                    getString("_UI_CORELink_from_feature"),
                    getString("_UI_PropertyDescriptor_description", 
                                "_UI_CORELink_from_feature", "_UI_CORELink_type"),
                    CorePackage.Literals.CORE_LINK__FROM,
                    true,
                    false,
                    true,
                    null,
                    null,
                    null) {
        
                @Override
                public Collection<?> getChoiceOfValues(Object object) {
                    COREMapping<Step> stepMapping = (StepMapping) object;
                    
                    // Gather the use case mapping associated
                    COREMapping<Flow> flowMapping = (FlowMapping) stepMapping.eContainer();
                   
                    // Gather all the steps from all mapped elements
                    Collection<Step> stepResult = new ArrayList<>();
                    for (EObject flow : COREArtefactUtil.getAllMappedElements(flowMapping)) {
                        stepResult.addAll(((Flow) flow).getSteps());
                    }
                    
                    // Filter out all elements from step result that are mapped to another element.
                    COREModelUtil.filterMappedElements(stepResult);
                    
                    return stepResult;
                }
            });
    }
    
    /**
     * This adds a property descriptor for the To feature.
     * <!-- begin-user-doc -->
     * 
     * @param object the object to add a property descriptor for
     *            <!-- end-user-doc -->
     * @generated NOT
     */
    protected void addToPropertyDescriptor(Object object) {
        itemPropertyDescriptors.add(
            new ItemPropertyDescriptor(
                    ((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
                    getResourceLocator(),
                    getString("_UI_CORELink_to_feature"),
                    getString("_UI_PropertyDescriptor_description", "_UI_CORELink_to_feature", 
                                "_UI_CORELink_type"),
                    CorePackage.Literals.CORE_LINK__TO,
                    true,
                    false,
                    true,
                    null,
                    null,
                    null) {
                
                @Override
                public Collection<?> getChoiceOfValues(Object object) {
                    COREMapping<Step> stepMapping = (StepMapping) object;
                    
                    // Gather the use case mapping associated
                    COREMapping<Flow> flowMapping = (FlowMapping) stepMapping.eContainer();
                   
                    // Gather all the steps from all mapped elements
                    Collection<Step> stepResult = new ArrayList<>();
                    for (EObject flow : COREArtefactUtil.getAllMappedElements(flowMapping)) {
                        stepResult.addAll(((Flow) flow).getSteps());
                    }
                    
                    // Gather the R use case from the "to" relation
                    Flow rFlow = flowMapping.getTo();
                    stepResult.addAll(rFlow.getSteps());
                    
                    // Filter out all elements from step result that are mapped to another element.
                    COREModelUtil.filterMappedElements(stepResult);
                    
                    // Remove the previously chosen attribute in the step mapping from
                    stepResult.remove(stepMapping.getFrom());
                    
                    return stepResult;
                }
            });
    }

}
