/**
 */
package ca.mcgill.sel.usecases.provider;

import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREMapping;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.controller.COREControllerFactory;
import ca.mcgill.sel.core.controller.MappingController;
import ca.mcgill.sel.core.controller.util.COREReferenceUtil;
import ca.mcgill.sel.core.provider.COREMappingItemProvider;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.usecases.UcFactory;
import ca.mcgill.sel.usecases.UseCase;
import ca.mcgill.sel.usecases.UseCaseMapping;
import ca.mcgill.sel.usecases.util.UcInterfaceUtil;
import ca.mcgill.sel.usecases.util.UcModelUtil;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;

/**
 * This is the item provider adapter for a {@link ca.mcgill.sel.usecases.UseCaseMapping} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class UseCaseMappingItemProvider extends COREMappingItemProvider {
	/**
     * This constructs an instance from a factory and a notifier.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public UseCaseMappingItemProvider(AdapterFactory adapterFactory) {
        super(adapterFactory);
    }

	/**
     * This returns the property descriptors for the adapted class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
        if (itemPropertyDescriptors == null) {
            super.getPropertyDescriptors(object);

        }
        return itemPropertyDescriptors;
    }

	/**
     * This returns UseCaseMapping.gif.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object getImage(Object object) {
        return overlayImage(object, getResourceLocator().getImage("full/obj16/UseCaseMapping"));
    }

	/**
     * This returns the label text for the adapted class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String getText(Object object) {
        return getString("_UI_UseCaseMapping_type");
    }


	/**
     * This handles model notifications by calling {@link #updateChildren} to update any cached
     * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void notifyChanged(Notification notification) {
        updateChildren(notification);
        super.notifyChanged(notification);
    }

	/**
     * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
     * that can be created under this object.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
        super.collectNewChildDescriptors(newChildDescriptors, object);

        newChildDescriptors.add
            (createChildParameter
                (CorePackage.Literals.CORE_MAPPING__MAPPINGS,
                 UcFactory.eINSTANCE.createActorMapping()));

        newChildDescriptors.add
            (createChildParameter
                (CorePackage.Literals.CORE_MAPPING__MAPPINGS,
                 UcFactory.eINSTANCE.createUseCaseMapping()));

        newChildDescriptors.add
            (createChildParameter
                (CorePackage.Literals.CORE_MAPPING__MAPPINGS,
                 UcFactory.eINSTANCE.createStepMapping()));

        newChildDescriptors.add
            (createChildParameter
                (CorePackage.Literals.CORE_MAPPING__MAPPINGS,
                 UcFactory.eINSTANCE.createFlowMapping()));
    }

	/**
     * Return the resource locator for this item provider's resources.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public ResourceLocator getResourceLocator() {
        return UseCasesEditPlugin.INSTANCE;
    }
    
    /**
     * This adds a property descriptor for the To Element feature.
     * <!-- begin-user-doc -->
     * @param object the object to add a property descriptor for
     * <!-- end-user-doc -->
     * @generated NOT
     */
    protected void addToPropertyDescriptor(Object object) {
        itemPropertyDescriptors.add(
            // CHECKSTYLE:IGNORE AnonInnerLength: Okay here.
            new ItemPropertyDescriptor(
                ((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
                 getResourceLocator(),
                 getString("_UI_CORELink_to_feature"),
                 getString("_UI_PropertyDescriptor_description", 
                             "_UI_CORELink_to_feature", "_UI_CORELink_type"),
                 CorePackage.Literals.CORE_LINK__TO,
                 true,
                 false,
                 true,
                 null,
                 null,
                 null) {
                @Override
                public Collection<?> getChoiceOfValues(Object object) {
                    COREMapping<UseCase> useCaseMapping = (UseCaseMapping) object;
                    
                    // return nothing if the "from" mapping has not been established yet
                    if (useCaseMapping.getFrom() == null) {
                        return null;
                    }
                    
                    // Gather the current COREExternalArtefact
                    COREExternalArtefact currentArtefact = 
                            (COREExternalArtefact) useCaseMapping.eContainer().eContainer();
                    
                    // Gather all use cases
                    Collection<UseCase> useCaseResult = UcInterfaceUtil.getAvailableUseCases(currentArtefact);                  
                    return useCaseResult;
                }
                
                @Override
                public void setPropertyValue(Object object, Object value) {
                    
                    EditingDomain editingDomain = getEditingDomain(object);
                    CompoundCommand compoundCommand = new CompoundCommand();
                    UseCase eValue = (UseCase) value;

                    EObject localElement = ((COREExternalArtefact) (((UseCaseMapping) object).eContainer()
                            .eContainer())).getRootModelElement();
                    
                    UseCase localValue = COREReferenceUtil.localizeElement(editingDomain, compoundCommand,
                            localElement, eValue);                    
                    
                    compoundCommand.append(SetCommand.create(editingDomain, object,
                            CorePackage.Literals.CORE_LINK__TO, localValue));
                    
                    editingDomain.getCommandStack().execute(compoundCommand);                    
                }
            });
    }

    /**
     * This adds a property descriptor for the From Element feature.
     * <!-- begin-user-doc -->
     * @param object the object to add a property descriptor for
     * <!-- end-user-doc -->
     * @generated NOT
     */
    protected void addFromPropertyDescriptor(Object object) {
        itemPropertyDescriptors.add(
            // CHECKSTYLE:IGNORE AnonInnerLength: Okay here.
            new ItemPropertyDescriptor(
                ((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
                 getResourceLocator(),
                 // CHECKSTYLE:IGNORE MultipleStringLiterals: Outcome of code generator.
                 getString("_UI_CORELink_from_feature"),
                 getString("_UI_PropertyDescriptor_description", 
                             "_UI_CORELink_from_feature", "_UI_CORELink_type"),
                 CorePackage.Literals.CORE_LINK__FROM,
                 true,
                 false,
                 true,
                 null,
                 null,
                 null) {
                
                @Override
                public Collection<?> getChoiceOfValues(Object object) {
                    COREMapping<UseCase> useCaseMapping = (UseCaseMapping) object;
                    
                    // Gather the COREModelExtension of the use case mapping to gather its COREExternalArtefact
                    COREModelComposition modelExtension = (COREModelComposition) useCaseMapping.eContainer();
                    COREExternalArtefact artefact = (COREExternalArtefact) modelExtension.getSource();
                    
                    // Gather all external & internal use cases
                    Collection<UseCase> useCasesResult = UcInterfaceUtil.getAvailableUseCases(artefact);
                    useCasesResult.remove(useCaseMapping.getFrom());
                    
                    // Check for cardinalities restrictions
                    Collection<EObject> useCasesFiltered = COREArtefactUtil.filterPossibleMapping(
                            useCasesResult, artefact, useCaseMapping);
                    
                    return useCasesFiltered;
                }
                
                @Override
                public IItemLabelProvider getLabelProvider(Object object) {
                    // CHECKSTYLE:IGNORE AnonInnerLength: Okay here.
                    return new IItemLabelProvider() {
                        
                        @Override
                        public String getText(Object object) {
                            StringBuilder result = new StringBuilder();
                            result.append(itemDelegator.getText(object));
                            
                            if (object != null) {
                                @SuppressWarnings("unchecked")
                                COREMapping<UseCase> target = (COREMapping<UseCase>) getTarget();
                                result.append(UcModelUtil.getReferencedMappingsText(target, (UseCase) object));
                            }
                            
                            return result.toString();
                        }

                        @Override
                        public Object getImage(Object object) {
                            return itemDelegator.getImage(object);
                        }
                    };
                }
                
                @Override
                public void setPropertyValue(Object object, Object value) {
                    MappingController controller = COREControllerFactory.INSTANCE.getMappingController();
                    
                    @SuppressWarnings("unchecked")
                    COREMapping<UseCase> mapping = (COREMapping<UseCase>) object;
                    
                    controller.setFromMapping(mapping, (UseCase) value);
                }
                
            });
    }
}
