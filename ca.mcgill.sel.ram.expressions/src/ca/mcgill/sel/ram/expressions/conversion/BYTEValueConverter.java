package ca.mcgill.sel.ram.expressions.conversion;

import org.eclipse.xtext.conversion.ValueConverterException;
import org.eclipse.xtext.conversion.impl.AbstractLexerBasedConverter;
import org.eclipse.xtext.nodemodel.INode;

/**
 * ValueConverter for a LiteralByte.
 * 
 * @author Rohit Verma
 */
public class BYTEValueConverter extends AbstractLexerBasedConverter<Byte> {

    /**
     * Value of a LiteralByte.
     */
    private String intPart;
    
    /**
     * Extracts the integer part from the value of the LiteralByte, e.g., returns 1 if the string is "1b".
     * @param str the given input.
     * @param node the parsed node.
     * @return the new {@code int}.
     * @throws ValueConverterException if the given input is {@code null}, empty or does not
     *  represent an integer number.
     */
    @Override public Byte toValue(String str, INode node) throws ValueConverterException {
        if (str.isEmpty()) {
            throw new ValueConverterException("Couldn't convert empty string to byte.", node, null);
        }
        if (str.contains("b")) {
            String[] strArray = str.split("b");
            intPart = strArray[0];
        }
        try {
            return Byte.parseByte(intPart);
        } catch (NumberFormatException e) {
            throw parsingError(str, node, e);
        }
    }

    /**
     * Throws Exception if the ValueConverter failed to do its job.
     * @param str the given input.
     * @param node the parsed node.
     * @param cause of the exception
     * @return the new {@code int}.
     * @throws ValueConverterException if the given input is {@code null}, empty or does not
     *  represent an integer number.
     */
    private ValueConverterException parsingError(String str, INode node, Exception cause) {
        return new ValueConverterException("Couldn't convert '" + str + "' to byte.", node, cause);
    }
}