package ca.mcgill.sel.ram.expressions.typechecking

import ca.mcgill.sel.ram.ValueSpecification
import ca.mcgill.sel.ram.LiteralDouble
import ca.mcgill.sel.ram.LiteralLong
import ca.mcgill.sel.ram.Minus
import ca.mcgill.sel.ram.Comparison
import ca.mcgill.sel.ram.Conditional
import ca.mcgill.sel.ram.PreIncrementOrDecrement
import ca.mcgill.sel.ram.PostIncrementOrDecrement
import ca.mcgill.sel.ram.LiteralInteger
import ca.mcgill.sel.ram.LiteralBoolean
import ca.mcgill.sel.ram.LiteralFloat
import ca.mcgill.sel.ram.Ternary
import ca.mcgill.sel.ram.Binary
import ca.mcgill.sel.ram.Unary
import ca.mcgill.sel.ram.Equality
import ca.mcgill.sel.ram.expressions.typechecking.nodes.EqualityNode
import ca.mcgill.sel.ram.expressions.typechecking.nodes.ConditionalNode
import ca.mcgill.sel.ram.expressions.typechecking.TypeChecker.Types
import ca.mcgill.sel.ram.LiteralChar
import ca.mcgill.sel.ram.LiteralString
import ca.mcgill.sel.ram.LiteralNull
import ca.mcgill.sel.ram.StructuralFeatureValue
import ca.mcgill.sel.ram.ImplementationClass
import ca.mcgill.sel.ram.Class;
import ca.mcgill.sel.ram.MulDivMod
import ca.mcgill.sel.ram.expressions.typechecking.nodes.MulOrDivNode
import ca.mcgill.sel.ram.Plus
import ca.mcgill.sel.ram.expressions.typechecking.nodes.PlusNode
import ca.mcgill.sel.ram.And
import ca.mcgill.sel.ram.expressions.typechecking.nodes.AndNode
import ca.mcgill.sel.ram.expressions.typechecking.nodes.OrNode
import ca.mcgill.sel.ram.Or
import ca.mcgill.sel.ram.expressions.typechecking.nodes.MinusNode
import ca.mcgill.sel.ram.expressions.typechecking.nodes.ComparisonNode
import ca.mcgill.sel.ram.Shift
import ca.mcgill.sel.ram.expressions.typechecking.nodes.BitwiseShiftNode
import ca.mcgill.sel.ram.LogicalOperator
import ca.mcgill.sel.ram.Not
import ca.mcgill.sel.ram.expressions.typechecking.nodes.NotNode
import ca.mcgill.sel.ram.expressions.typechecking.nodes.IncrementDecrementNode
import ca.mcgill.sel.ram.Type
import ca.mcgill.sel.ram.RBoolean
import ca.mcgill.sel.ram.RInt
import ca.mcgill.sel.ram.RFloat
import ca.mcgill.sel.ram.RDouble
import ca.mcgill.sel.ram.RChar
import ca.mcgill.sel.ram.RString
import ca.mcgill.sel.ram.RLong
import ca.mcgill.sel.ram.RByte
import ca.mcgill.sel.ram.ParameterValue
import ca.mcgill.sel.ram.LiteralByte
import ca.mcgill.sel.ram.Classifier

class TypeUtil {
    
    // Opaque expression has to be typed before it's going to be used in here
    def static Types typeFor(ValueSpecification e) {
        switch (e) {
            Ternary: typeFor(e)
            Binary: typeFor(e)
            Unary: typeFor(e.expression)
            LiteralByte: Types.byteType
            LiteralBoolean: Types.booleanType
            LiteralInteger: Types.intType
            LiteralFloat: Types.floatType
            LiteralDouble: Types.doubleType
            LiteralLong: Types.longType
            LiteralChar: Types.charType
            LiteralNull: Types.nullType
            LiteralString: Types.stringType
            StructuralFeatureValue: Types.userDefinedType
            ImplementationClass: Types.userDefinedType
            Class: Types.userDefinedType
            ParameterValue: fromRAMDataTypetoCustomType(e.getParameter().getType()) 
        }
    }

    def static Types typeFor(Binary e){
        var left = e.left
        var right = e.right
        switch(e){
            Equality: EqualityNode.typeCheck(left, right)
            MulDivMod: MulOrDivNode.typeCheck(left, right)
            Plus: PlusNode.typeCheck(left, right)
            Minus: MinusNode.typeCheck(left, right)
            Comparison: ComparisonNode.typeCheck(left, right)
            And: AndNode.typeCheck(left, right)
            Or: OrNode.typeCheck(left, right)
            Shift: BitwiseShiftNode.typeCheck(left, right)
            LogicalOperator: BitwiseShiftNode.typeCheck(left, right) // has to change
        }
    }
    
    def static Types typeFor(Unary e){
        var exp = e.expression
        switch(e){
            Not: NotNode.typeCheck(exp)
            PostIncrementOrDecrement: IncrementDecrementNode.typeCheck(exp)
            PreIncrementOrDecrement:  IncrementDecrementNode.typeCheck(exp)
        }
    }
    
    def static Types typeFor(Ternary e){
        switch(e){
            Conditional: ConditionalNode.typeCheck(e.condition, e.expressionT, e.expressionF)
        }
    }
    
    def static Types fromRAMDataTypetoCustomType(Type type){
        switch(type){
            RBoolean: Types.booleanType
            RByte: Types.byteType
            RInt: Types.intType  
            RFloat: Types.floatType
            RDouble: Types.doubleType
            RLong: Types.longType
            RChar: Types.charType
            RString: Types.stringType
            Classifier: Types.userDefinedType
        }
        
    }
}