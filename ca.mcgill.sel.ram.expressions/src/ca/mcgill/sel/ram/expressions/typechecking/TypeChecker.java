package ca.mcgill.sel.ram.expressions.typechecking;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.ram.Class;
import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.ImplementationClass;
import ca.mcgill.sel.ram.ParameterValue;
import ca.mcgill.sel.ram.StructuralFeatureValue;
import ca.mcgill.sel.ram.Type;
import ca.mcgill.sel.ram.ValueSpecification;
import ca.mcgill.sel.ram.util.RAMInterfaceUtil;
//import ca.mcgill.sel.ram.expressions.typechecking.nodes.CastNode;

/**
 * A class to typecheck parsed models.
 * 
 * @author Rohit Verma
 */
public final class TypeChecker {
    /**
     * A list of Strings outlining any issues with typechecking.
     */
    private static List<String> issues = new ArrayList<String>();

    private static String cannotResolveTypeErrorTemplate = "Cannot resolve the type of %s";

    /**
     * Contains the different (data)types of nodes that can be expected.
     */
    public enum Types {
        /**
         * Value for datatype "int".
         */
        intType,
        /**
         * Value for datatype "String".
         */
        stringType,
        /**
         * Value for datatype "float".
         */
        floatType,
        /**
         * Value for datatype "double".
         */
        doubleType,
        /**
         * Value for datatype "char".
         */
        charType,
        /**
         * Value for datatype "byte".
         */
        byteType,
        /**
         * Value for datatype "long".
         */
        longType,
        /**
         * Value for datatype "boolean".
         */
        booleanType,
        /**
         * Value for datatype "null".
         */
        nullType,
        /**
         * Value for a User Defined datatype.
         */
        userDefinedType,
        /**
         * Value for a variable that references "int".
         */
        referenceIntType
    };

    /**
     * Creates a new instance.
     */
    private TypeChecker() {
    }

    /**
     * Generic method to typecheck a node -- redirects to the implementation for
     * that specific type of the node.
     * 
     * @param node
     *            : The node to be typechecked
     * @param expectedTypeRAM
     *            : Expected return type of the node (in the RAM model).
     * 
     * @return List of issues
     */
    public static List<String> typeCheckNode(ValueSpecification node, Type expectedTypeRAM) {

        Types returnType = null;
        Classifier userDefType = null;
        boolean hasIssues = false;

        Types expectedType = TypeUtil.fromRAMDataTypetoCustomType(expectedTypeRAM);

        TypeChecker.clearIssueList();
        returnType = TypeUtil.typeFor(node);

        if (returnType != null) {

            if (returnType == Types.userDefinedType) {
                if (node instanceof ParameterValue) {
                    userDefType = (Classifier) (((ParameterValue) node).getParameter()).getType();
                } else if (node instanceof StructuralFeatureValue) {
                    userDefType = (Classifier) (((StructuralFeatureValue) node).getValue()).getType();
                }

                if (userDefType instanceof Class || userDefType instanceof ImplementationClass) {
                    if (userDefType != expectedTypeRAM) {
                        Collection<Classifier> allSuperTypes = RAMInterfaceUtil.getAvailableSuperTypes(userDefType);
                        hasIssues = !allSuperTypes.contains(expectedTypeRAM);
                    }
                } // NOT VERY CLEAN
            } else if (!(returnType == expectedType
                    || (returnType == Types.nullType && expectedType == Types.userDefinedType)
                    || (expectedType == Types.referenceIntType && returnType == Types.intType))) {
                hasIssues = true;
            }
        } else {
            issues.add(String.format(cannotResolveTypeErrorTemplate, EMFEditUtil.getText(node)));
        }

        if (hasIssues) {
            issues.add("Expression has type: " + returnType.toString() + " when type: " + expectedTypeRAM.getName()
                    + " is expected");
        }

        return issues;
    }

    /**
     * Type checking node.
     * 
     * @param node
     *            node belonging to AST
     * @param expectedType
     *            type that AST should resolve to
     * @return
     *         issues that were found during type checking.
     */
    public static List<String> typeCheckNode(ValueSpecification node, Types expectedType) {

        Types returnType = TypeUtil.typeFor(node);

        TypeChecker.clearIssueList();

        if (returnType != null) {
            if (!(returnType == expectedType
                    || (expectedType == Types.referenceIntType && returnType == Types.intType))) {
                issues.add("Expression has type: " + returnType.toString() + " when type: " + expectedType.toString()
                        + " is expected");
            }
        } else {
            issues.add(String.format(cannotResolveTypeErrorTemplate, EMFEditUtil.getText(node)));
        }
        return issues;
    }

    /**
     * This method resolves the type of a node recursively.
     * 
     * @param node
     *            : The node whose type is to be resolved.
     * @return returnedType : Type that the node resolves to.
     */
    public static Types resolveType(ValueSpecification node) {

        Types returnedType = null;

        if (node instanceof StructuralFeatureValue) {
            returnedType = TypeUtil.fromRAMDataTypetoCustomType(((StructuralFeatureValue) node).getValue().getType());
        } else if (node instanceof ParameterValue) {
            returnedType = TypeUtil.fromRAMDataTypetoCustomType(((ParameterValue) node).getParameter().getType());
        } else if (node instanceof Class) {
            returnedType = Types.userDefinedType;
        } else if (node instanceof ImplementationClass) {
            returnedType = Types.userDefinedType;
        } else {
            returnedType = TypeUtil.typeFor(node);
        }

        if (returnedType == null) {
            issues.add(String.format(cannotResolveTypeErrorTemplate, EMFEditUtil.getText(node)));
        }

        return returnedType;
    }

    /**
     * Helper method -- to add a new String(issue) to the list.
     * 
     * @param issue
     *            : An issue found during typecasting.
     */
    public static void addIssue(String issue) {
        issues.add(issue);
    }

    /**
     * Helper method -- to get the list of issues..
     * 
     * @return issues : List of issues (Global in nature).
     */
    public static List<String> getIssueList() {
        return issues;
    }

    /**
     * Helper method -- to clear the issuelist. (Like a refresh).
     */
    public static void clearIssueList() {
        issues.clear();
    }
}