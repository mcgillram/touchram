package ca.mcgill.sel.ram.expressions.typechecking.nodes;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.ram.ValueSpecification;
import ca.mcgill.sel.ram.expressions.typechecking.TypeChecker;
import ca.mcgill.sel.ram.expressions.typechecking.TypeChecker.Types;

/**
 * Used to infer type of the increment or decrement expression.
 * 
 * @author maksim
 *
 */
public final class IncrementDecrementNode {

    /**
     * Private constructor to prevent unintended instantiation.
     */
    private IncrementDecrementNode() {
    }

    /**
     * This method type checks expression.
     * 
     * @param expression
     *            expression to evaluate
     * @return
     *         return type of the IncrementDecrement expression
     */
    public static Types typeCheck(ValueSpecification expression) {
        Types shouldReturnType = null;
        Types expType = TypeChecker.resolveType(expression);

        if (expType != null) {
            if (expType.equals(Types.referenceIntType)) {
                shouldReturnType = Types.referenceIntType;
            } else {
                TypeChecker.addIssue(EMFEditUtil.getText(expression) + " is not a valid type for the NOT operation ");
            }
        }
        return shouldReturnType;
    }
}
