package ca.mcgill.sel.ram.expressions.typechecking.nodes;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.ram.ValueSpecification;
import ca.mcgill.sel.ram.expressions.typechecking.TypeChecker;
import ca.mcgill.sel.ram.expressions.typechecking.TypeCheckingUtil;
import ca.mcgill.sel.ram.expressions.typechecking.TypeChecker.Types;

/**
 * Node that represents a minus operator in the CST/model.
 * 
 * @author Rohit Verma
 */
public final class MinusNode {

    /**
     * Private constructor to prevent unintended instantiation.
     */
    private MinusNode() {
    }

    /**
     * Method that implements the typechecking logic for the node representing "Minus" Operator.
     * 
     * @param left : left of MinusNode
     * @param right : right of MinusNode
     * 
     * @return The type that MinusNode should return
     */
    public static Types typeCheck(ValueSpecification left, ValueSpecification right) {

        Types shouldReturnType = null;

        // The binary - operator performs subtraction, producing the difference of two numeric operands.
        // Operands allowed on Minus Operator

        // Binary numeric promotion is performed on the operands.

        Types lType = TypeChecker.resolveType(left);
        Types rType = TypeChecker.resolveType(right);

        if (TypeCheckingUtil.MINUS_OPERANDS.contains(lType) && TypeCheckingUtil.MINUS_OPERANDS.contains(rType)) {
            if (lType.equals(rType)) {
                shouldReturnType = rType;
            } else {
                if (lType.equals(rType)) {
                    return rType;
                } else {
                    return TypeCheckingUtil.promoteToType(lType, rType);
                }
            }
        } else {
            if (TypeCheckingUtil.MINUS_OPERANDS.contains(lType)) {
                TypeChecker.addIssue(EMFEditUtil.getText(right)
                        + " is not a valid type for subtraction "
                        + EMFEditUtil.getText(left));
            } else {
                TypeChecker.addIssue(EMFEditUtil.getText(left)
                        + " is not a valid type for subtraction "
                        + EMFEditUtil.getText(right));
            }

        }

        return shouldReturnType;
    }
}
