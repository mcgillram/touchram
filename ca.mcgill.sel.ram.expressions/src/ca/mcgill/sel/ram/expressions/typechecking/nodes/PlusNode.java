package ca.mcgill.sel.ram.expressions.typechecking.nodes;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.ram.ValueSpecification;
import ca.mcgill.sel.ram.expressions.typechecking.TypeChecker;
import ca.mcgill.sel.ram.expressions.typechecking.TypeCheckingUtil;
import ca.mcgill.sel.ram.expressions.typechecking.TypeChecker.Types;

/**
 * A class that contains the typechecking logic for a node representing "Plus" Operator..
 * 
 * @author Rohit Verma
 */
public final class PlusNode {

    /**
     * Private constructor to prevent unintended instantiation.
     */
    private PlusNode() {
    }

    /**
     * Method that implements the typechecking logic for the node representing "Plus" Operator.
     * 
     * @param left : left of PlusNode
     * @param right : right of PlusNode
     * 
     * @return The type that PlusNode should return
     */
    public static Types typeCheck(ValueSpecification left, ValueSpecification right) {

        Types shouldReturnType = null;

        // The Plus operator performs addition when applied to two operands of numeric type,
        // producing the sum of the operands.

        // Binary numeric promotion is performed on the operands.
        // The type of an additive expression on numeric operands is the promoted type of its operands.

        Types lType = TypeChecker.resolveType(left);
        Types rType = TypeChecker.resolveType(right);

        if (TypeCheckingUtil.PLUS_OPERANDS.contains(lType) && TypeCheckingUtil.PLUS_OPERANDS.contains(rType)) {
            if (lType.equals(rType)) {
                return rType;
            } else {
                return TypeCheckingUtil.promoteToType(lType, rType);
            }
        } else {
            if (TypeCheckingUtil.PLUS_OPERANDS.contains(lType)) {
                TypeChecker.addIssue(EMFEditUtil.getText(right)
                        + " is not a valid type for addition "
                        + EMFEditUtil.getText(left));
            } else {
                TypeChecker.addIssue(EMFEditUtil.getText(left)
                        + " is not a valid type for addition"
                        + EMFEditUtil.getText(right));
            }

        }
        return shouldReturnType;
    }
}
