package ca.mcgill.sel.ram.expressions.typechecking.nodes;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.ram.ValueSpecification;
import ca.mcgill.sel.ram.expressions.typechecking.TypeChecker;
import ca.mcgill.sel.ram.expressions.typechecking.TypeCheckingUtil;
import ca.mcgill.sel.ram.expressions.typechecking.TypeChecker.Types;

/**
 * A class that contains the typechecking logic for a node representing "MulOrDiv" Operator..
 * 
 * @author Rohit Verma
 */
public final class MulOrDivNode {

    /**
     * Private constructor to prevent unintended instantiation.
     */
    private MulOrDivNode() {
    }

    /**
     * Method that implements the typechecking logic for the node representing "MulOrDiv" Operator.
     * 
     * @param left : left of MulOrDivNode
     * @param right : right of MulOrDivNode
     * 
     * @return The type that MulOrDivNode should return
     */
    public static Types typeCheck(ValueSpecification left, ValueSpecification right) {

        Types shouldReturnType = null;

        Types lType = TypeChecker.resolveType(left);
        Types rType = TypeChecker.resolveType(right);

        if (TypeCheckingUtil.MUL_DIV_MOD_OPERAND.contains(lType)
                && TypeCheckingUtil.MUL_DIV_MOD_OPERAND.contains(rType)) {

            if (lType != null && rType != null) {
                if (lType.equals(rType)) {
                    return rType;
                } else {
                    return TypeCheckingUtil.promoteToType(lType, rType);
                }
            }
        } else {
            if (TypeCheckingUtil.MUL_DIV_MOD_OPERAND.contains(lType)) {
                TypeChecker.addIssue(EMFEditUtil.getText(right)
                        + " is not a valid type for multiplication and division"
                        + EMFEditUtil.getText(left));
            } else {
                TypeChecker.addIssue(EMFEditUtil.getText(left)
                        + " is not a valid type for multiplication and division "
                        + EMFEditUtil.getText(right));
            }

        }
        return shouldReturnType;
    }
}