package ca.mcgill.sel.ram.expressions

import ca.mcgill.sel.ram.ValueSpecification
import org.eclipse.emf.ecore.EObject
import ca.mcgill.sel.ram.MessageView
import ca.mcgill.sel.ram.Ternary
import ca.mcgill.sel.ram.Binary
import ca.mcgill.sel.ram.Unary
import ca.mcgill.sel.ram.OpaqueExpression
import ca.mcgill.sel.ram.Aspect

class OpaqueExpressionUtil {
    /**
     * Method to replace OpaqueExpression nodes in the CST with StructuralFeature/ParameterValueMapping nodes.
     * It looks at the whole expression and has recursive calls to itself if the expression has other nodes
     * in addition to OpaqueExpression.
     * 
     * @param nodevs
     *            The AST/parsed model returned by the parser which might contain OpaqueExpression nodes
     * @param rootContainer
     *            rootContainer
     * @param currentTextView
     *            currentTextView
     * @param rootMessageView
     *            rootMessageView
     * @param utils
     *            utils
     * @return
     *         The AST/parsed model with no OpaqueExpression nodes
     */
    def static ValueSpecification replaceOpaqueExpressionsInCST(ValueSpecification exp, Aspect rootContainer,
        EObject currentTextView, MessageView rootMessageView, ParserUtil util) {
        switch (exp) {
            Ternary: {
                var nodeC = replaceOpaqueExpressionsInCST(exp.condition, rootContainer, currentTextView,
                    rootMessageView, util
                )
                if(nodeC !== null) exp.setCondition(nodeC)

                var nodeT =  replaceOpaqueExpressionsInCST(exp.expressionT, rootContainer,
                    currentTextView, rootMessageView,
                        util)
                if(nodeT !== null) exp.setExpressionT(nodeT)

                var nodeF = replaceOpaqueExpressionsInCST(exp.expressionF, rootContainer,
                    currentTextView, rootMessageView,
                        util)
                if(nodeF !== null) exp.setExpressionF(nodeF)
                return exp;
            }
            Binary: {
                var nodeL = replaceOpaqueExpressionsInCST(exp.left, rootContainer, currentTextView,
                    rootMessageView, util
                )
                if(nodeL !== null) exp.setLeft(nodeL)

                var nodeR = replaceOpaqueExpressionsInCST(exp.right, rootContainer, currentTextView,
                    rootMessageView, util
                )
                if(nodeR !== null) exp.setRight(nodeR)

                return exp;
            }
            Unary: {
                var node = replaceOpaqueExpressionsInCST(exp.expression, rootContainer, currentTextView,
                    rootMessageView, util
                )
                if(node !== null) exp.setExpression(node)
                return exp;
            }
            OpaqueExpression: {
                var node = ModelUtils.getActualType(exp, rootContainer, currentTextView, rootMessageView, util)
                if (node === null)
                    util.addIssue(exp.getBody() + ParserUtil.ERROR_MESSAGE)

                return node;
            }
            default:
                exp
        }
    }
    
    def static ValueSpecification checkOpaqueExpression(OpaqueExpression exp, Aspect rootContainer,
        EObject currentTextView, MessageView rootMessageView, ParserUtil util) {
            var node = ModelUtils.getActualType(exp, rootContainer, currentTextView, rootMessageView, util)
            if (node === null)
                util.addIssue(exp.getBody() + ParserUtil.ERROR_MESSAGE)
            return node;
    }
    
}
