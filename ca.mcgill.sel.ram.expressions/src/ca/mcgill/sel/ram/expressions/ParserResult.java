package ca.mcgill.sel.ram.expressions;

import java.util.List;

import ca.mcgill.sel.ram.ValueSpecification;

/**
 * A class that is used by the parser to return information to the calling method.
 *
 * @author Rohit Verma
 */
public class ParserResult {

    private ValueSpecification model;
    private List<String> errors;

    /**
     * A getter for the Model that is inferred from text passed by calling method.
     * 
     * @return
     *         model: The model object
     *
     */
    public ValueSpecification getModel() {
        return model;
    }

    /**
     * A setter for the Model that is inferred from text passed by calling method.
     * 
     * @param model
     *            The model object
     *
     */
    public void setModel(ValueSpecification model) {
        this.model = model;
    }

    /**
     * A getter for the errorList (that contains any error messages) obtained during parsing.
     * 
     * @return
     *         The list that contains error messages obtained during parsing.
     *
     */
    public List<String> getErrors() {
        return errors;
    }

    /**
     * A setter for the errorList (that contains any error messages) obtained during parsing.
     * 
     * @param errors
     *            The list that contains error messages obtained during parsing.
     *
     */
    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

}
