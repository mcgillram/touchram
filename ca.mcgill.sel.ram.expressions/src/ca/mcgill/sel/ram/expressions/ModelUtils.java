package ca.mcgill.sel.ram.expressions;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.AssignmentStatement;
import ca.mcgill.sel.ram.CombinedFragment;
import ca.mcgill.sel.ram.EnumLiteralValue;
import ca.mcgill.sel.ram.InteractionFragment;
import ca.mcgill.sel.ram.InteractionOperand;
import ca.mcgill.sel.ram.Lifeline;
import ca.mcgill.sel.ram.Message;
import ca.mcgill.sel.ram.MessageOccurrenceSpecification;
import ca.mcgill.sel.ram.MessageView;
import ca.mcgill.sel.ram.OpaqueExpression;
import ca.mcgill.sel.ram.Operation;
import ca.mcgill.sel.ram.Parameter;
import ca.mcgill.sel.ram.ParameterValue;
import ca.mcgill.sel.ram.ParameterValueMapping;
import ca.mcgill.sel.ram.REnum;
import ca.mcgill.sel.ram.REnumLiteral;
import ca.mcgill.sel.ram.RamFactory;
import ca.mcgill.sel.ram.StructuralFeature;
import ca.mcgill.sel.ram.StructuralFeatureValue;
import ca.mcgill.sel.ram.ValueSpecification;
import ca.mcgill.sel.ram.util.MessageViewUtil;
import ca.mcgill.sel.ram.util.RAMInterfaceUtil;

/**
 * Used for getting structural feature values and parameter values.
 * 
 * @author maksim
 *
 */
public final class ModelUtils {

    /**
     * Private method for utility class.
     */
    private ModelUtils() {

    }

    /**
     * Method to replace OpaqueExpression node in the CST with StructuralFeatureValue node.
     * 
     * @param opaqueExpression
     *            The OpaqueExpression node to be replaced.
     * @param rootContainer
     *            rootContainer
     * @param currentTextView
     *            currentTextView
     * @return
     *         The node that replaces OpaqueExpression (with a StructuralFeatureValue).
     */
    static ValueSpecification getStructuralFeatureValue(OpaqueExpression opaqueExpression, Aspect rootContainer,
            EObject currentTextView) {
        StructuralFeatureValue structuralFeatureValue = null;
        Message initialMessage = null;
        Lifeline lifeline = null;
        Collection<StructuralFeature> structuralFeaturesList = null;

        String userString = opaqueExpression.getBody();

        if (currentTextView instanceof ParameterValueMapping) {
            // Called from a ParameterValueMapping
            Message message = (Message) currentTextView.eContainer();

            MessageOccurrenceSpecification sendEvent = (MessageOccurrenceSpecification) message.getSendEvent();
            initialMessage = MessageViewUtil.findInitialMessage(sendEvent);

            if (sendEvent != null && sendEvent.getCovered().size() > 0) {
                lifeline = sendEvent.getCovered().get(0);
            }

            structuralFeaturesList = RAMInterfaceUtil.getStructuralFeatures(rootContainer, lifeline, initialMessage);

        } else if (currentTextView instanceof AssignmentStatement) {
            // Called from an AssignmentStatement
            initialMessage = MessageViewUtil.findInitialMessage((AssignmentStatement) currentTextView);

            if (((AssignmentStatement) currentTextView).getCovered().size() > 0) {
                lifeline = ((AssignmentStatement) currentTextView).getCovered().get(0);
            }
            structuralFeaturesList = RAMInterfaceUtil.getStructuralFeatures(rootContainer, lifeline, initialMessage);
        } else if (currentTextView instanceof InteractionOperand) {
            // Called from an interaction frame
            InteractionFragment fragment = (CombinedFragment) currentTextView.eContainer();
            initialMessage = MessageViewUtil.findInitialMessage(fragment);

            if (fragment.getCovered().size() > 0) {
                lifeline = fragment.getCovered().get(0);
            }
            structuralFeaturesList = RAMInterfaceUtil.getStructuralFeatures(rootContainer, lifeline, initialMessage);
        }

        if (structuralFeaturesList != null) {
            for (StructuralFeature structuralFeature : structuralFeaturesList) {
                if (structuralFeature.getName().equals(userString)) {
                    structuralFeatureValue =
                            RamFactory.eINSTANCE.createStructuralFeatureValue();
                    structuralFeatureValue.setValue(structuralFeature);
                }
            }
        }

        return structuralFeatureValue;
    }

    /**
     * Method to replace OpaqueExpression node in the CST with ParameterValueMapping node.
     * 
     * @param opaqueNode
     *            The OpaqueExpression node to be replaced
     * @param container
     *            container
     * @return
     *         The node that replaces OpaqueExpression (with a ParameterValue).
     */
    static ValueSpecification getParameterValue(OpaqueExpression opaqueNode, MessageView container) {

        ParameterValue parameterValue = null;

        // Search for Parameter
        Operation operationInAspect = container.getSpecifies();

        List<Parameter> parameters = operationInAspect.getParameters();
        for (int p = 0; p < parameters.size(); p++) {
            if (parameters.get(p).getName().equals(opaqueNode.getBody())) {
                parameterValue = RamFactory.eINSTANCE.createParameterValue();
                parameterValue.setParameter(parameters.get(p));
            }
        }
        return parameterValue;
    }

    /**
     * Method to replace OpaqueExpression nodes in the CST with StructuralFeature/ParameterValueMapping/EnumLiteralValue
     * nodes.
     * 
     * @param opaqueNode
     *            The OpaqueExpression node to be replaced
     * @param rootContainer
     *            rootContainer
     * @param currentTextView
     *            currentTextView
     * @param container
     *            container
     * @param util the {@link ParserUtil} to use
     * @return
     *         The node that replaces OpaqueExpression.
     *         Can be a StructuralFeature/ParameterValueMapping/EnumLiteralValue.
     */
    static ValueSpecification getActualType(OpaqueExpression opaqueNode, Aspect rootContainer, EObject currentTextView,
            MessageView container, ParserUtil util) {

        ValueSpecification newVS = null;

        // ENUMS
        if (opaqueNode.getBody().contains(".")) {
            String enumString = opaqueNode.getBody();
            String[] enumArray = enumString.split("\\.");

            String enumDef = enumArray[0];
            String enumLiteral = enumArray[1];

            List<REnum> enumList = (List<REnum>) RAMInterfaceUtil.getAvailableEnums(rootContainer);

            if (!enumList.isEmpty()) {
                for (REnum rEnum : enumList) {
                    if (enumDef.equals(rEnum.getName())) {
                        for (REnumLiteral literal : rEnum.getLiterals()) {
                            if (enumLiteral.equals(literal.getName())) {
                                newVS = getNewEnumLiteralValue(literal);
                            }
                        }
                    }
                }
            }
        } else {
            // PARAMETER
            newVS = getParameterValue(opaqueNode, container);

            if (newVS == null) {
                // STRUCTURALFEATURE
                newVS = ModelUtils.getStructuralFeatureValue(opaqueNode, rootContainer, currentTextView);
            }
        }

        if (newVS == null) {
            util.addIssue(opaqueNode.getBody() + ParserUtil.ERROR_MESSAGE);
            newVS = opaqueNode;
        }
        return newVS;
    }

    /**
     * Returns a new EnumLiteralValue object.
     * 
     * @param rEnumLiteral
     *            The EnumLiteral object that is contained in the EnumLiteralValue returned.
     * @return enumLiteralValue
     *         The returned EnumLiteralValue.
     */
    static ValueSpecification getNewEnumLiteralValue(REnumLiteral rEnumLiteral) {

        EnumLiteralValue enumLiteralValue =
                RamFactory.eINSTANCE.createEnumLiteralValue();
        enumLiteralValue.setValue(rEnumLiteral);
        return enumLiteralValue;
    }
}
