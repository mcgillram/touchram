/**
 */
package ca.mcgill.sel.restif.impl;

import ca.mcgill.sel.restif.RestifPackage;
import ca.mcgill.sel.restif.StaticFragment;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Static Fragment</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.restif.impl.StaticFragmentImpl#getInternalname <em>Internalname</em>}</li>
 * </ul>
 *
 * @generated
 */
public class StaticFragmentImpl extends PathFragmentImpl implements StaticFragment {
    /**
     * The default value of the '{@link #getInternalname() <em>Internalname</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getInternalname()
     * @generated
     * @ordered
     */
    protected static final String INTERNALNAME_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getInternalname() <em>Internalname</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getInternalname()
     * @generated
     * @ordered
     */
    protected String internalname = INTERNALNAME_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected StaticFragmentImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return RestifPackage.Literals.STATIC_FRAGMENT;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String getInternalname() {
        return internalname;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setInternalname(String newInternalname) {
        String oldInternalname = internalname;
        internalname = newInternalname;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, RestifPackage.STATIC_FRAGMENT__INTERNALNAME, oldInternalname, internalname));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case RestifPackage.STATIC_FRAGMENT__INTERNALNAME:
                return getInternalname();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case RestifPackage.STATIC_FRAGMENT__INTERNALNAME:
                setInternalname((String)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case RestifPackage.STATIC_FRAGMENT__INTERNALNAME:
                setInternalname(INTERNALNAME_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case RestifPackage.STATIC_FRAGMENT__INTERNALNAME:
                return INTERNALNAME_EDEFAULT == null ? internalname != null : !INTERNALNAME_EDEFAULT.equals(internalname);
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (internalname: ");
        result.append(internalname);
        result.append(')');
        return result.toString();
    }

} //StaticFragmentImpl
