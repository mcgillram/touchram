/**
 */
package ca.mcgill.sel.restif;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Static Fragment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.restif.StaticFragment#getInternalname <em>Internalname</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.restif.RestifPackage#getStaticFragment()
 * @model
 * @generated
 */
public interface StaticFragment extends PathFragment {
    /**
     * Returns the value of the '<em><b>Internalname</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Internalname</em>' attribute.
     * @see #setInternalname(String)
     * @see ca.mcgill.sel.restif.RestifPackage#getStaticFragment_Internalname()
     * @model
     * @generated
     */
    String getInternalname();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.restif.StaticFragment#getInternalname <em>Internalname</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Internalname</em>' attribute.
     * @see #getInternalname()
     * @generated
     */
    void setInternalname(String value);

} // StaticFragment
