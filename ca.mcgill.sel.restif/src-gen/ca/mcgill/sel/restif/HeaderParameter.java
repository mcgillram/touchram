/**
 */
package ca.mcgill.sel.restif;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Header Parameter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.restif.HeaderParameter#getHeaderField <em>Header Field</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.restif.RestifPackage#getHeaderParameter()
 * @model
 * @generated
 */
public interface HeaderParameter extends Parameter {
    /**
     * Returns the value of the '<em><b>Header Field</b></em>' attribute.
     * The default value is <code>"USERAGENT"</code>.
     * The literals are from the enumeration {@link ca.mcgill.sel.restif.HeaderField}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Header Field</em>' attribute.
     * @see ca.mcgill.sel.restif.HeaderField
     * @see #setHeaderField(HeaderField)
     * @see ca.mcgill.sel.restif.RestifPackage#getHeaderParameter_HeaderField()
     * @model default="USERAGENT"
     * @generated
     */
    HeaderField getHeaderField();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.restif.HeaderParameter#getHeaderField <em>Header Field</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Header Field</em>' attribute.
     * @see ca.mcgill.sel.restif.HeaderField
     * @see #getHeaderField()
     * @generated
     */
    void setHeaderField(HeaderField value);

} // HeaderParameter
