package ca.mcgill.sel.restif.util;

import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.core.controller.ModelUtil;
import ca.mcgill.sel.restif.RestIF;
import ca.mcgill.sel.restif.RestifFactory;
import ca.mcgill.sel.restif.StaticFragment;

public final class RifModelUtil implements ModelUtil {

    /**
     * Creates a new instance with default pretty printers.
     */
    public RifModelUtil() {
        
    }

	@Override
	public EObject createNewEmptyModel(String name) {
		
		// Someone should implement the creation of an empty RIF model here (essentially just a root node.)
		// It seems the language controller can be used for that.
		RestIF restIF = RestifFactory.eINSTANCE.createRestIF();
		restIF.setName(name);
		
        StaticFragment root = RestifFactory.eINSTANCE.createStaticFragment();
        root.setInternalname("/root");
        
        restIF.setRoot(root);
        
		return restIF;
	}
}
