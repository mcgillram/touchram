package ca.mcgill.sel.ram.generator;

/**
 * Flags for generation of use case details.
 * At least one option is required.
 * @author rlanguay
 *
 */
public enum UseCaseDetailGeneratorFlags {
    /**
     * Generates the details as a rich text document.
     * Headers will be in bold, and some additional formatting will also be applied.
     */
    RICH_TEXT,
    
    /**
     * Generates the details as a plain text document.
     */
    PLAIN_TEXT;
    
    /**
     * Although implemented as Generics, acceleo crashes when fed with non-string
     * arguments. We therefore convert all Enums to their string representation,
     * before handing them on to acceleo.
     */
    @Override
    public String toString() {
        return this.name();
    }
}
