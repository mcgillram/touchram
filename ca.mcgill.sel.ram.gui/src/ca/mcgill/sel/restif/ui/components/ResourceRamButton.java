package ca.mcgill.sel.restif.ui.components;

import ca.mcgill.sel.ram.ui.components.RamButton;
import ca.mcgill.sel.ram.ui.components.RamImageComponent;
import ca.mcgill.sel.restif.AccessMethod;
import ca.mcgill.sel.restif.MethodType;

/**
 * ResourceRamButton is used for the Icon Buttons for each {@link MethodType}.
 * This class is created to override the setIcon method due to different formatting.
 * 
 * @author Bowen
 */
public class ResourceRamButton extends RamButton {


    @SuppressWarnings("unused")
    private RamImageComponent icon;

    private boolean usesImage;
    private AccessMethod accessMethod;

    /**
     * Creates a new button with the given image representation and arc radius.
     * 
     * @param image - the image to be shown
     * @param arcRadius - the radius of the corners.
     */
    public ResourceRamButton(RamImageComponent image, int arcRadius) {
        super(image, arcRadius);
        this.usesImage = true;
    }
    
    /**
     * Replace the current icon by the one given in parameter.
     * 
     * @param img - the new icon for the button.
     */
    @Override
    public void setIcon(RamImageComponent img) {
        if (img != null) {
            if (usesImage) {
                removeAllChildren();
                setBuffers(0);
                this.addChild(img);
            }
        }
    }
    
    public AccessMethod getAccessMethod() {
        return accessMethod;
    }

    public void setAccessMethod(AccessMethod accessMethod) {
        this.accessMethod = accessMethod;
    }
}
