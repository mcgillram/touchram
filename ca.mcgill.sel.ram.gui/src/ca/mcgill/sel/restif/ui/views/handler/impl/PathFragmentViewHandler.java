package ca.mcgill.sel.restif.ui.views.handler.impl;

import java.util.LinkedHashMap;

import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;

import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.menu.RamLinkedMenu;
import ca.mcgill.sel.ram.ui.events.listeners.ActionListener;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.handler.ILinkedMenuListener;
import ca.mcgill.sel.restif.PathFragment;
import ca.mcgill.sel.restif.RestIF;
import ca.mcgill.sel.restif.language.controller.ControllerFactory;
import ca.mcgill.sel.restif.ui.views.BaseView;
import ca.mcgill.sel.restif.ui.views.PathFragmentView;
import ca.mcgill.sel.restif.ui.views.RestTreeView;
import ca.mcgill.sel.restif.ui.views.handler.IPathFragmentViewHandler;

/**
 * The handler of the {@link PathFragmentView}.
 * 
 * @author Bowen
 */
public class PathFragmentViewHandler extends BasePathFragmentViewHandler 
    implements IPathFragmentViewHandler, ILinkedMenuListener {

    private static final String ACTION_PATH_FRAGMENT_REMOVE = "view.pathfragment.remove";
    private static final String ACTION_PATH_FRAGMENT_SWITCH = "view.pathfragment.switch";
    
    @Override
    public void removeRepresented(BaseView<?> baseView) {
        PathFragmentView pathFragmentView = (PathFragmentView) baseView;
        RestTreeView restTreeView = pathFragmentView.getParentOfType(RestTreeView.class);
        RestIF restIF = restTreeView.getRestIF();
        
        PathFragment pathFragment = pathFragmentView.getPathFragment();
        PathFragment parent = restTreeView.getParent(pathFragment);
        LinkedHashMap<PathFragment, PathFragment> childrenToParentMap = 
                restTreeView.getChildrenToParentMap(pathFragment);   
        
        ControllerFactory.INSTANCE.getRestTreeController().removePathFragment(parent, 
                pathFragment, restIF, childrenToParentMap);
    }

    @Override
    public void actionPerformed(ActionListener.ActionEvent event) {
        String actionCommand = event.getActionCommand();
        RamRectangleComponent pressedButton = (RamRectangleComponent) event.getTarget();
        RamLinkedMenu linkedMenu = (RamLinkedMenu) pressedButton.getParentOfType(RamLinkedMenu.class);

        if (linkedMenu != null) {
            PathFragmentView pathFragmentView = (PathFragmentView) linkedMenu.getLinkedView();

            if (ACTION_PATH_FRAGMENT_REMOVE.equals(actionCommand)) {
                removeRepresented(pathFragmentView); 
            } else if (ACTION_PATH_FRAGMENT_SWITCH.equals(actionCommand)) {
                switchPathFragment(pathFragmentView);
            }
        }
    }

    @Override
    public void switchPathFragment(PathFragmentView pathFragmentView) {
        RestTreeView restTreeView = pathFragmentView.getParentOfType(RestTreeView.class);
        RestIF restIF = restTreeView.getRestIF();
        
        PathFragment pathFragment = pathFragmentView.getPathFragment();
        PathFragment parent = restTreeView.getParent(pathFragment);
        LinkedHashMap<PathFragment, PathFragment> childrenToParentMap = 
                restTreeView.getChildrenToParentMap(pathFragment);
                
        if (pathFragment == restIF.getRoot()) {
            ControllerFactory.INSTANCE.getRestTreeController().switchRootPathFragment(restIF, childrenToParentMap);
        } else {
            ControllerFactory.INSTANCE.getRestTreeController().switchPathFragment(parent, 
                    pathFragment, restIF, childrenToParentMap);
        }
    }

    @Override
    public void initMenu(RamLinkedMenu menu, TapAndHoldEvent event) {
        PathFragmentView pathFragmentView = (PathFragmentView) menu.getLinkedView();
        
        // Disable the remove button on the root.
        if (pathFragmentView.getParentOfType(RestTreeView.class)
                .getParent(pathFragmentView.getPathFragment()) != null) {
            menu.addAction(Strings.MENU_DELETE_PATH_FRAGMENT,
                    Icons.ICON_MENU_TRASH, ACTION_PATH_FRAGMENT_REMOVE, this, true);
        }
        
        menu.addAction(Strings.MENU_SWITCH, Icons.ICON_MENU_SWITCH, ACTION_PATH_FRAGMENT_SWITCH, this, true);
    }

}
