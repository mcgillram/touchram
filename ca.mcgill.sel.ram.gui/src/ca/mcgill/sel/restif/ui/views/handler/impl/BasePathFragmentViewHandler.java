package ca.mcgill.sel.restif.ui.views.handler.impl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.mt4j.components.MTComponent;
import org.mt4j.input.inputProcessors.MTGestureEvent;
import org.mt4j.input.inputProcessors.componentProcessors.dragProcessor.DragEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.menu.RamLinkedMenu;
import ca.mcgill.sel.ram.ui.events.DelayedDrag;
import ca.mcgill.sel.ram.ui.utils.GUIConstants;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.handler.BaseHandler;
import ca.mcgill.sel.ram.ui.views.handler.ILinkedMenuListener;
import ca.mcgill.sel.restif.PathFragment;
import ca.mcgill.sel.restif.language.controller.ControllerFactory;
import ca.mcgill.sel.restif.ui.views.BaseView;
import ca.mcgill.sel.restif.ui.views.PathFragmentView;
import ca.mcgill.sel.restif.ui.views.RestTreeView;
import ca.mcgill.sel.restif.ui.views.handler.IBaseViewHandler;

/**
 * A modified handler for the PathFragmentView.
 * Inspired by {@link ca.mcgill.sel.classdiagram.ui.views.handler.impl.BaseViewHandler} and 
 * {@link ca.mcgill.sel.ram.ui.views.feature.handler.impl.FeatureEditModeHandler}
 *
 * @author Bowen
 */
public abstract class BasePathFragmentViewHandler extends BaseHandler implements IBaseViewHandler, ILinkedMenuListener {

    /**
     * Name of the sub-menu with add actions.
     */
    protected static final String SUBMENU_ADD = "sub.add";

    private static final String ACTION_REPRESENTED_REMOVE = "view.represented.remove";
    
    private DelayedDrag dragAction = new DelayedDrag(GUIConstants.DELAYED_DRAG_MIN_DRAG_DISTANCE);

    @Override
    public boolean processDragEvent(DragEvent dragEvent) {
        PathFragmentView target = (PathFragmentView) dragEvent.getTarget();
        
        // Disable dragging of the root path fragment.
        if (!target.getPathFragment().equals(target.getRestTreeView().getRestIF().getRoot())) {
            switch (dragEvent.getId()) {
                case MTGestureEvent.GESTURE_STARTED:
                case MTGestureEvent.GESTURE_UPDATED:
                    dragAction.processGestureEvent(dragEvent);
                    for (MTComponent comp : target.getAllDragContainers()) {
                        dragEvent.setTarget(comp);
                        dragAction.processGestureEvent(dragEvent);
                    }
                    if (dragAction.wasDragPerformed()) {
                        updatePathFragmentColors(target.getRestTreeView().getAllPathFragmentViews(), target);
                    }
                    break;
                case MTGestureEvent.GESTURE_ENDED:
                    if (dragAction.wasDragPerformed()) {
                        setPositionUpdate(target);
                        // Reset highlight.
                        for (PathFragmentView pf : target.getRestTreeView().getAllPathFragmentViews()) {
                            pf.highlight(false);
                        }
                    }
            }
        }
        return true;
    }

    
    /**
     * This method calls the controller to execute an EMF command to set the given {@link pathFragmentView}'s position.
     * 
     * @param pathFragmentView - the given {@link pathFragmentView}
     */
    private static void setPositionUpdate(PathFragmentView pathFragmentView) {
        /*
         * First checks if a path fragment had to be added as a child of another feature.
         * This is horizontal movement, where intersection adds it as a child.
         */
        PathFragmentView target = getUnderlyingPathFragment(pathFragmentView.getRestTreeView()
                .getAllPathFragmentViews(), pathFragmentView);
        
        if (target != null && !target.equals(pathFragmentView.getParentPathFragmentView())) {
            LinkedHashMap<PathFragment, PathFragment> childrenToParentHashMap = 
                    pathFragmentView.getRestTreeView().getChildrenToParentMap(pathFragmentView.getPathFragment());
            
            ControllerFactory.INSTANCE.getRestTreeController().switchParent(pathFragmentView.getPathFragment(),
                    pathFragmentView.getParentPathFragmentView().getPathFragment(), target.getPathFragment(),
                    childrenToParentHashMap);
        } else {
            int newPosition = getPathFragmentPosition(pathFragmentView);
            
            if (newPosition == -1) {
                newPosition = pathFragmentView.getCurrentPosition();
            }
            
            ControllerFactory.INSTANCE.getRestTreeController().setPathFragmentPosition(
                    pathFragmentView.getRestTreeView().getRestIF(),
                    pathFragmentView.getPathFragment(), pathFragmentView.getParentPathFragmentView().getPathFragment(), 
                    newPosition);
        }
    }

    /**
     * Highlight the underlying {@link PathFragmentView} when dragging a path fragment.
     *
     * @param list - the set of features to examine
     * @param pathFragmentView - the dragged path fragment view
     */
    private static void updatePathFragmentColors(ArrayList<PathFragmentView> list, PathFragmentView pathFragmentView) {
        // Reset all highlights.
        for (PathFragmentView pf : list) {
            pf.highlight(false);
        }
        // Highlight underlying feature, if any.
        PathFragmentView underlyingPathFragmentView = getUnderlyingPathFragment(list, pathFragmentView);
        if (underlyingPathFragmentView != null) {
            underlyingPathFragmentView.highlight(true);
            pathFragmentView.highlight(true);
        } else {
            // Check if we are to be moved.
            PathFragmentView parentV = pathFragmentView.getParentPathFragmentView();
            int newPosition = getPathFragmentPosition(pathFragmentView);

            if (newPosition == -1) {
                return;
            }

            int offset = pathFragmentView.getCurrentPosition() - newPosition;
            int prevPosition;
            int nextPosition;
            // To the left.
            if (offset > 0) {
                prevPosition = newPosition - 1;
                nextPosition = newPosition;
            } else {
                prevPosition = newPosition;
                nextPosition = newPosition + 1;
            }
            if (prevPosition >= 0 && prevPosition < parentV.getChildrenView().size()) {
                PathFragmentView prevChild = parentV.getChildrenView().get(prevPosition);
                prevChild.highlight(true);
                parentV.highlight(true);
            }
            if (nextPosition >= 0 && nextPosition < parentV.getChildrenView().size()) {
                PathFragmentView nextChild = parentV.getChildrenView().get(nextPosition);
                nextChild.highlight(true);
                parentV.highlight(true);
            }
        }        
    }
    
    /**
     * Return the current position of a path fragment related to its sibling on the screen.
     * This is used when a path fragment is dragged to find out were it should be placed in the end.
     *
     * @param pathFragmentView - the {@link PathFragmentView}
     * @return the new position, or -1 if the position hasn't changed
     */
    private static int getPathFragmentPosition(PathFragmentView pathFragmentView) {
        int initialPosition = pathFragmentView.getCurrentPosition();
        
        // Get the siblings.
        List<PathFragmentView> siblings = new ArrayList<>();
        for (PathFragmentView pf : pathFragmentView.getSiblingViews()) {
            siblings.add(pf);
        }
        int newPosition = initialPosition;
        for (PathFragmentView sib : siblings) {
            if (initialPosition < sib.getCurrentPosition()) {
                // The feature is originally located left of its sibling.
                if (pathFragmentView.getX() > sib.getX() + sib.getWidth()) {
                    // The feature as now moved farther to the right of the sibling.
                    newPosition++;
                }
            } else {
                // The feature is originally located right of its sibling.
                if (pathFragmentView.getX() < sib.getX()) {
                    // The feature as now moved farther to the left of the sibling.
                    newPosition--;
                }
            }
        }
        if (newPosition != initialPosition) {
            return newPosition;
        }
        return -1;
    }

    /**
     * Get the {@link PathFragmentView} lying under the given one. 
     * Used during drag events to check where the gesture ended.
     *
     * @param list - The list of {@link PathFragmentView}s to examine
     * @param pathFragmentView - the dragged {@link PathFragmentView}
     * @return the pathFragmentView the gesture ended on, or null if none
     */
    private static PathFragmentView getUnderlyingPathFragment(ArrayList<PathFragmentView> list,
            PathFragmentView pathFragmentView) {
        list.remove(pathFragmentView);
        
        for (PathFragmentView pf : list) {
            if (pf.containsPointGlobal(new Vector3D(pathFragmentView.getX(), pathFragmentView.getY()))) {
                return pf;
            }
        }
        return null;
    }

    @Override
    public boolean processTapAndHoldEvent(TapAndHoldEvent tapAndHoldEvent) {
        if (tapAndHoldEvent.isHoldComplete()) {
            BaseView<?> target = (BaseView<?>) tapAndHoldEvent.getTarget();
            return target.getRestTreeView().getHandler()
                    .handleTapAndHold(target.getRestTreeView(), target);
        }
        return false;
    }

    @Override
    public boolean processTapEvent(TapEvent tapEvent) {
        // Always flip the selection of the view.
        if (tapEvent.isTapped() || tapEvent.isDoubleTap()) {
            BaseView<?> target = (BaseView<?>) tapEvent.getTarget();
            target.setSelect(!target.getIsSelected());

            // Pass it to the structural view handler to handle more advanced gestures.
            if (tapEvent.isDoubleTap()) {
                RestTreeView structuralDiagramView = target.getRestTreeView();
                return structuralDiagramView.getHandler().handleDoubleTap(structuralDiagramView, target);
            }
        }

        return false;
    }

    @Override
    public abstract void removeRepresented(BaseView<?> baseView);

    @Override
    public void actionPerformed(ActionEvent event) {
        String actionCommand = event.getActionCommand();
        RamRectangleComponent pressedButton = (RamRectangleComponent) event.getTarget();
        RamLinkedMenu linkedMenu = (RamLinkedMenu) pressedButton.getParentOfType(RamLinkedMenu.class);
        if (linkedMenu != null) {

            BaseView<?> represented = (BaseView<?>) linkedMenu.getLinkedView();

            if (ACTION_REPRESENTED_REMOVE.equals(actionCommand)) {
                removeRepresented(represented);
            }
        }
    }

    @Override
    public EObject getEobject(RamRectangleComponent rectangle) {
        return ((BaseView<?>) rectangle).getRepresented();
    }

    @Override
    public void initMenu(RamLinkedMenu menu, TapAndHoldEvent event) {
        menu.addSubMenu(1, SUBMENU_ADD);
        menu.addAction(Strings.MENU_DELETE, Icons.ICON_MENU_TRASH, ACTION_REPRESENTED_REMOVE, this, true);
    }

    @Override
    public List<EObject> getEObjectToListenForUpdateMenu(RamRectangleComponent rectangle) {
        BaseView<?> baseView = (BaseView<?>) rectangle;
        ArrayList<EObject> ret = new ArrayList<EObject>();
        ret.add(baseView.getRepresented());
        return ret;
    }

    @Override
    public void updateMenu(RamLinkedMenu menu, Notification notification) {
    }

    @Override
    public RamRectangleComponent getVisualLinkedComponent(RamRectangleComponent link) {
        return link;
    }

}
