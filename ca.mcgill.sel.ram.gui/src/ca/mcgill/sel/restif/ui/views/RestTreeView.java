package ca.mcgill.sel.restif.ui.views;

import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Set;

import org.abego.treelayout.NodeExtentProvider;
import org.abego.treelayout.TreeLayout;
import org.abego.treelayout.util.DefaultConfiguration;
import org.abego.treelayout.util.DefaultTreeForTreeLayout;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.edit.provider.INotifyChangedListener;
import org.mt4j.input.gestureAction.TapAndHoldVisualizer;
import org.mt4j.input.inputProcessors.IGestureEventListener;
import org.mt4j.input.inputProcessors.componentProcessors.panProcessor.PanProcessorTwoFingers;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldProcessor;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapProcessor;
import org.mt4j.input.inputProcessors.componentProcessors.zoomProcessor.ZoomProcessor;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamLineComponent;
import ca.mcgill.sel.ram.ui.events.MouseWheelProcessor;
import ca.mcgill.sel.ram.ui.events.RightClickDragProcessor;
import ca.mcgill.sel.ram.ui.events.UnistrokeProcessorLeftClick;
import ca.mcgill.sel.ram.ui.utils.GUIConstants;
import ca.mcgill.sel.ram.ui.utils.MathUtils;
import ca.mcgill.sel.ram.ui.utils.autolayout.DefaultNodeExtentProvider;
import ca.mcgill.sel.ram.ui.views.AbstractView;
import ca.mcgill.sel.restif.AccessMethod;
import ca.mcgill.sel.restif.MethodType;
import ca.mcgill.sel.restif.PathFragment;
import ca.mcgill.sel.restif.Resource;
import ca.mcgill.sel.restif.RestIF;
import ca.mcgill.sel.restif.RestifPackage;
//import ca.mcgill.sel.restif.impl.ContainerMapImpl;
//import ca.mcgill.sel.restif.impl.ElementMapImpl;
import ca.mcgill.sel.restif.impl.RestifFactoryImpl;
import ca.mcgill.sel.restif.language.controller.ControllerFactory;
import ca.mcgill.sel.restif.ui.components.ResourceRamButton;
import ca.mcgill.sel.restif.ui.views.handler.IRestTreeViewHandler;
import ca.mcgill.sel.restif.ui.views.handler.RestTreeHandlerFactory;

/**
 * The visual representation for a rest tree.
 * This class is inspired by {@link ClassDiagramView} and {@link FeatureDiagramView}.
 * 
 * @author Bowen
 */
public class RestTreeView extends AbstractView<IRestTreeViewHandler> implements INotifyChangedListener {
        
    // The initial level / depth to be shown based on screen resolution
    private static final float INITIAL_NUMBER_OF_LEVELS = 4;

    // The maximum distance between two features while being displayed.
    private static final float MAXIMUM_VERTICAL_DISTANCE = 100;

    // The minimum distance between two features while being displayed.
    private static final float INITIAL_HORIZONTAL_DISTANCE = 50;
    
    // Int value for when there is no specified index of a new path fragment view
    private static final int NO_SPECIFIED_INDEX = -1;
    
    /** Tree layout representing the diagram. */
    private static TreeLayout<PathFragmentView> treeLayout;
    
    /** Tree containing the features hierarchy. */
    private DefaultTreeForTreeLayout<PathFragmentView> tree;
    
    private RestIF restIF;

    private HashMap<PathFragment, PathFragmentView> pathFragmentToViewMap;
    private HashMap<Resource, ResourceView> resourceToViewMap;

    private float gapBetweenLevels;
    
    private Set<BaseView<?>> selectedElements;

    /**
     * Creates a new RestTreeView for the given rest tree. For every class inside the rest tree its own
     * view is created.
     * 
     * @param restIF - the rest tree
     * @param width - the width over which the elements can be displayed
     * @param height - the height over which the elements can be displayed
     */
    public RestTreeView(RestIF restIF, float width, float height) {
        super(width, height);

        this.restIF = restIF;
        this.pathFragmentToViewMap = new HashMap<>();
        this.resourceToViewMap = new HashMap<>();
        this.selectedElements = new HashSet<BaseView<?>>();
        
        // Compute the gap between the levels based on the height of the screen, divided by the desired value of user.
        this.gapBetweenLevels = RamApp.getActiveScene().getHeight() / INITIAL_NUMBER_OF_LEVELS;
        if (gapBetweenLevels >= MAXIMUM_VERTICAL_DISTANCE) {
            gapBetweenLevels = MAXIMUM_VERTICAL_DISTANCE;
        }
        
        // Create the views for all the path fragments.
        PathFragment root = restIF.getRoot();
        initPathFragmentViews(root);
        
        // Build the tree.
        populateTree();
        
        // Compute positions for the path fragments.
        computePathFragmentsPosition();

        // Add the views to the container. Also creates lines for relationships and initializes listeners.
        displayDiagram();
        
        EMFEditUtil.addListenerFor(restIF, this);
    }
    
    /**
     * Create the {@link PathFragmentView} for a given {@link PathFragment}.
     * 
     * @param pathFragment - The {@link PathFragment}
     */
    private void initPathFragmentViews(PathFragment pathFragment) {
        PathFragmentView pathFragmentView = pathFragmentToViewMap.get(pathFragment);
        // Create the path fragment only if the view doesn't already exist.
        if (pathFragmentView == null) {            
            pathFragmentView = addNewPathFragmentView(pathFragment, NO_SPECIFIED_INDEX);
        }
                
        // Create the children path fragments.
        for (PathFragment child : pathFragment.getChild()) {
            initPathFragmentViews(child);

        }
    }
    
    /**
     * Adds a new {@link PathFragmentView} for the given {@link PathFragment} to this view.
     * 
     * @param pathFragment - the {@link PathFragment} to add a view for
     * @param index - the index of the newly added {@link PathFragment}
     * @return the newly created {@link PathFragmentView}
     */
    private PathFragmentView addNewPathFragmentView(PathFragment pathFragment, int index) {
        Vector3D vector = new Vector3D(0.0f, 0.0f);

        PathFragmentView pathFragmentView = new PathFragmentView(this, pathFragment, vector);
        pathFragmentToViewMap.put(pathFragment, pathFragmentView);
        
        Resource resource = getResource(pathFragment);
        boolean resourceExistInRestIFModel = isExistInRestIFModel(resource);
        addNewResource(resource, pathFragmentView, resourceExistInRestIFModel);
        
        if (getParent(pathFragment) != null) {
            if (index == NO_SPECIFIED_INDEX) {
                pathFragmentToViewMap.get(getParent(pathFragment)).addChildPathFragmentView(pathFragmentView);
            } else {
                pathFragmentToViewMap.get(getParent(pathFragment)).addChildPathFragmentView(pathFragmentView, index);
            }
        }
        
        return pathFragmentView;
    }
    
    /**
     * Adds a new {@link ResourceView} for the given {@link Resource} to this view.
     * 
     * @param resource - the {@link Resource} to add a view for
     * @param parentView - the {@link PathFragment} this {@link Resource} represents
     * @param existInRestIFModel - whether or not the {@link Resource} already exists in the {@link RestIF model}
     */
    private void addNewResource(Resource resource, PathFragmentView parentView, boolean existInRestIFModel) {
        ResourceView resourceView = new ResourceView(this, resource, parentView, existInRestIFModel);
        resourceToViewMap.put(resource, resourceView);
        
        // Add the resource view as a child to the path fragment view.
        parentView.addChildView(resourceView);
    }
    
    /**
     * Populate the tree layout with the {@link PathFragmentView} we want to display.
     */
    private void populateTree() {
        PathFragmentView rootView = pathFragmentToViewMap.get(restIF.getRoot());
        
        // Create a tree with root feature as per the existing data structure in jar.
        tree = new DefaultTreeForTreeLayout<PathFragmentView>(rootView);
        
        // With retrospect to rootFeature, add all to the pathFragmentViews.
        recurseAndAdd(rootView);
        
        // Copy the horizontal minimum distance.
        double gapBetweenNodes = INITIAL_HORIZONTAL_DISTANCE;
        
        // A modelReuse is created according to the data structure provided in the jar, passing the above two values.
        DefaultConfiguration<PathFragmentView> configuration = new DefaultConfiguration
                <PathFragmentView>(gapBetweenLevels, gapBetweenNodes);
        
        // Create the NodeExtentProvider for TextInBox nodes.
        NodeExtentProvider<PathFragmentView> nodeExtentProvider = new DefaultNodeExtentProvider<PathFragmentView>();

        // Create the layout - Created by passing the tree, the modelReuse and the nodeExtentProvider.
        treeLayout = new TreeLayout<PathFragmentView>(tree, nodeExtentProvider, configuration);
    }
    
    /**
     * Function to recursively add all path fragments from RestIF model to layout data structure.
     * 
     * @param pathFragmentView - view to add to the tree
     */
    private void recurseAndAdd(PathFragmentView pathFragmentView) {
        for (PathFragmentView child : pathFragmentView.getChildrenView()) {
            tree.addChild(pathFragmentView, child);
            recurseAndAdd(child);
        }
    }
    
    /**
     * Function called to set the position of all the path fragments. In turn calls recursive functions
     * which further make the alignment perfect.
     */
    private void computePathFragmentsPosition() {
        PathFragmentView rootView = pathFragmentToViewMap.get(restIF.getRoot());
        recursiveCall(rootView, gapBetweenLevels, 0);

        // This is to determine at what percentage the path fragment is moved in relative spacing to width of screen.
        float offsetValue = rootView.getX() - (getWidth() / 2);
        offSet(rootView, offsetValue);
        
        // Since the path fragments' anchors are in upper left, we have to adjust the x coordinate to center the views.
        centerViews(rootView);
    }

    /**
     * Function called to adjust all the final positions of the path fragment. Recursive function.
     *  Calls all its children by itself.
     *
     * @param pathFragmentView - The path fragment to which the position is to be computed
     * @param offsetY - The gap between each level of the Rest Tree
     * @param position - THe horizontal position of the node
     */
    private void recursiveCall(PathFragmentView pathFragmentView, float offsetY, int position) {
        Rectangle2D.Double b1 = getBoundsOfNode(pathFragmentView);
        double x1 = b1.getCenterX();
        double y1 = ((position * offsetY) + ((position + 1) * offsetY)) / 2;

        pathFragmentView.setVector(new Vector3D((float) x1, (float) y1));
        
        pathFragmentView.setXposition((float) x1);
        pathFragmentView.setYposition((float) y1);
        
        for (PathFragmentView child : pathFragmentView.getChildrenView()) {
            recursiveCall(child, offsetY, position + 1);
        }
    }
    
    /**
     * Function to return back the data structure of Rectangle.
     * 
     * @param pathFragmentView - Pass the node from which the rectangle needs to be extracted
     * @return the rectangle corresponding to the node
     */
    private static Rectangle2D.Double getBoundsOfNode(PathFragmentView pathFragmentView) {        
        return treeLayout.getNodeBounds().get(pathFragmentView);
    }
    
    /**
     * Offsets position of a path fragment and its children on the X axis.
     * 
     * @param pathFragmentView - the path fragment to move
     * @param offsetValue - the offset to apply
     */
    private void offSet(PathFragmentView pathFragmentView, float offsetValue) {
        pathFragmentView.setXposition(pathFragmentView.getXposition() - offsetValue);
        
        pathFragmentView.setVector(new Vector3D(pathFragmentView.getXposition() 
                - offsetValue, pathFragmentView.getYposition()));
        
        for (PathFragmentView child : pathFragmentView.getChildrenView()) {
            offSet(child, offsetValue);
        }
    }
    
    /**
     * Centers the position of a path fragment and its children on the X axis.
     * 
     * @param pathFragmentView - the path fragment to move
     */
    private void centerViews(PathFragmentView pathFragmentView) {
        pathFragmentView.setXposition(pathFragmentView.getXposition() - pathFragmentView.getWidth() / 2);
        
        pathFragmentView.setVector(new Vector3D(pathFragmentView.getXposition(), pathFragmentView.getYposition()));
                
        for (PathFragmentView child : pathFragmentView.getChildrenView()) {
            centerViews(child);
        }        
    }
    
    /**
     * Display the whole {@link RestTreeView} according to user defined preferences.
     */
    private void displayDiagram() {
        displayPathFragment(tree.getRoot());
        setHandlers();
    }

    /**
     * Display the given {@link PathFragmentView} and its children.
     * Draws line to parent for the relationships.
     * 
     * @param pathFragmentView - The {@link PathFragmentView} to display
     */
    private void displayPathFragment(PathFragmentView pathFragmentView) {
        // Display pathFragmentView.
        containerLayer.addChild(pathFragmentView);
        
        for (PathFragmentView child : tree.getChildren(pathFragmentView)) {
            displayPathFragment(child);
        }

        // Draw line to parent.
        if (getParent(pathFragmentView.getPathFragment()) != null) {
            drawLine(pathFragmentView, pathFragmentToViewMap.get(getParent(pathFragmentView.getPathFragment())));
        }
    }
    
    /**
     * Function called to draw a line between parent and the child.
     * 
     * @param child - child path fragment
     * @param parent - parent path fragment
     */
    private void drawLine(PathFragmentView child, PathFragmentView parent) {
        // Set the x and y of the parent section of the line as the middle bottom point of the rectangle.
        float xParent = parent.getXposition() + parent.getWidth() / 2;
        float yParent = parent.getYposition() + parent.getHeight();

        // Set the x and y of the child section of the line as the middle top point of the rectangle.
        float xChild = child.getXposition() + child.getWidth() / 2;
        float yChild = child.getYposition();

        RamLineComponent line = new RamLineComponent(xParent, yParent, xChild, yChild);

        child.setLineToParent(line);
        containerLayer.addChild(line);
    }
    
    /**
     * Function called to set handlers for each {@link PathFragmentView} if they do not already have one.
     */
    private void setHandlers() {
        for (PathFragmentView pathFragmentView : pathFragmentToViewMap.values()) {
            if (pathFragmentView.getHandler() == null) {
                pathFragmentView.setHandler(RestTreeHandlerFactory.INSTANCE.getPathFragmentViewHandler());
            }
        }
    }
    
    /**
     * Returns the parent of a specific path fragment in the RestIF model.
     * 
     * @param pathFragment - The {@link PathFragment} we are searching for the parent of
     * @return the desired {@link PathFragment} parent, null if it does not exist
     */
    public PathFragment getParent(PathFragment pathFragment) {
        ArrayList<PathFragment> parents = new ArrayList<>(Arrays.asList(restIF.getRoot()));
        ArrayList<PathFragment> newParents;

        do {
            newParents = new ArrayList<>();
            for (PathFragment parent : parents) {
                EList<PathFragment> children = parent.getChild();

                if (children != null) {
                    newParents.addAll(children);

                    for (PathFragment child : parent.getChild()) {
                        if (child.equals(pathFragment)) {
                            return parent;
                        }
                    }
                }
            }
            parents.clear();
            parents.addAll(newParents);
        }
        while (parents.size() > 0);

        return null;
    }
    
    /**
     * Returns the parent of a specific deleted path fragment in the RestIF model.
     * 
     * @param deletedPathFragment - The {@link PathFragment} we are searching for the parent of
     * @return the desired {@link PathFragment} parent, null if it does not exist
     */
    public PathFragment getParentOfDeleted(PathFragment deletedPathFragment) {
        ArrayList<PathFragment> parents = new ArrayList<>(Arrays.asList(restIF.getRoot()));
        ArrayList<PathFragment> newParents;

        do {
            newParents = new ArrayList<>();
            for (PathFragment parent : parents) {
                ArrayList<PathFragment> children = new ArrayList<>();

                for (PathFragmentView view : pathFragmentToViewMap.get(parent).getChildrenView()) {
                    children.add(view.getPathFragment());

                    if (view.getPathFragment().equals(deletedPathFragment)) {
                        return parent;
                    }
                }

                newParents.addAll(children);
            }
            parents.clear();
            parents.addAll(newParents);
        }
        while (parents.size() > 0);

        return null;
    }
    
    /**
     * Returns whether or not a given resource exists in the RestIF model.
     * 
     * @param checkedResource - The {@link Resource} we are checking
     * @return boolean
     */
    private boolean isExistInRestIFModel(Resource checkedResource) {
        EList<Resource> resources = restIF.getResource();
        if (resources != null) {
            for (Resource resource : resources) {
                if (resource.equals(checkedResource)) {
                    return true;
                }
            }
        }
        return false;
    }
    
    /**
     * Redraw the diagram of PathFragments.
     */
    private void updateRestTreeDisplay() {
        containerLayer.removeAllChildren();
        populateTree();
        computePathFragmentsPosition();
        displayDiagram();        
    }
    
    /**
     * Returns the {@link Resource} linked to the given {@link PathFragment}.
     * If there does not exist one, we create a dummy {@link Resource} that does not exist in the RestIF model.
     * 
     * @param pathFragment - The {@link PathFragment} checked
     * @return the correlated or generated {@link Resource}
     */
    public Resource getResource(PathFragment pathFragment) {
        EList<Resource> resources = restIF.getResource();
        if (resources != null) {
            for (Resource resource : resources) {
                if (pathFragment.equals(resource.getEndpoint())) {
                    return resource;
                }
            }
        }
        Resource resource = RestifFactoryImpl.eINSTANCE.createResource();
        resource.setEndpoint(pathFragment);

        return resource;
    }
    
    /**
     * Removes the {@link PathFragmentView} associated to the {@link PathFragment} from this RestTreeView.
     * 
     * @param pathFragment - The {@link PathFragment} we wish to remove
     */
    private void removePathFragmentView(PathFragment pathFragment) {
        if (restIF.getRoot() != null) {
            pathFragmentToViewMap.get(getParentOfDeleted(pathFragment)).removeChildPathFragmentView(
                    pathFragmentToViewMap.get(pathFragment));
        }
        PathFragmentView pathFragmentView = pathFragmentToViewMap.remove(pathFragment);
        selectedElements.remove(pathFragmentView);
        if (pathFragmentView.getLineToParent() != null) {
            containerLayer.removeChild(pathFragmentView.getLineToParent());
        }
        removeChild(pathFragmentView);
        pathFragmentView.destroy();
    }
    

    /**
     * Returns a {@link LinkedHashMap} of every child (only children of the given node) to parent 
     * relationship of the given {@link PathFragment}.
     * 
     * @param pathFragment - The {@link PathFragment} given
     * @return a map of every child (only children of the given node) to parent relationship
     */    
    public LinkedHashMap<PathFragment, PathFragment> getChildrenToParentMap(PathFragment pathFragment) {
        ArrayList<PathFragment> parents = new ArrayList<>(Arrays.asList(pathFragment));
        ArrayList<PathFragment> newParents;
        LinkedHashMap<PathFragment, PathFragment> childrenMap = new LinkedHashMap<>();

        do {
            newParents = new ArrayList<>();
            for (PathFragment parent : parents) {
                EList<PathFragment> children = parent.getChild();

                if (children != null) {
                    newParents.addAll(children);

                    for (PathFragment child : parent.getChild()) {
                        childrenMap.put(child, parent);
                    }
                }
            }
            parents.clear();
            parents.addAll(newParents);
        } 

        while (parents.size() > 0);

        return childrenMap;
    }
    
    /**
     * Gets the {@link PathFragmentView} of the specified PathFragment.
     * 
     * @param pathFragment the PathFragment element for which we want to get the path fragment view
     * @return {@link PathFragmentView}
     */
    public PathFragmentView getPathFragmentViewOf(PathFragment pathFragment) {
        return pathFragmentToViewMap.get(pathFragment);
    }
    
    /**
     * Gets the {@link ResourceRamButton} of the specified AccessMethod.
     * 
     * @param accessMethod the AccessMethod element for which we want to get the resource ram button
     * @return {@link ResourceRamButton}
     */
    public ResourceRamButton getResourceRamButtonOf(AccessMethod accessMethod) {
        for (Resource resource : resourceToViewMap.keySet()) {
            for (AccessMethod currentAccessMethod : resource.getAccessmethod()) {
                if (currentAccessMethod.equals(accessMethod)) {
                    if (accessMethod.getType() == MethodType.GET) {
                        return resourceToViewMap.get(resource).getGetButton();
                    } else if (accessMethod.getType() == MethodType.PUT) {
                        return resourceToViewMap.get(resource).getPutButton();
                    } else if (accessMethod.getType() == MethodType.POST) {
                        return resourceToViewMap.get(resource).getPostButton();
                    // delete
                    } else {
                        return resourceToViewMap.get(resource).getDeleteButton();
                    }
                }
            }
        }
        
        return null;
    }
    
    /**
     * Returns the current {@link RestIF} model.
     * 
     * @return {@link RestIF}
     */
    public RestIF getRestIF() {
        return restIF;
    }
    
    /**
     * Get the PathFragmentView closest to the position in the diagram, if there is one.
     *
     * @param position - The position to check
     * @return the closest {@link PathFragmentView} or null if there is no feature at this position
     */
    public PathFragmentView liesAround(Vector3D position) {
        for (PathFragmentView pathFragmentView : pathFragmentToViewMap.values()) {
            if (MathUtils.pointIsInRectangle(position, pathFragmentView, GUIConstants.MARGIN_ELEMENT_DETECTION)) {
                return pathFragmentView;
            }
        }
        return null;
    }
    
    /**
     * Function called to destroy every {@link PathFragmentView} and reset the hash map.
     */
    private void resetPathFragmentViews() {
        for (PathFragmentView view : pathFragmentToViewMap.values()) {
            view.destroy();
        }
        
        pathFragmentToViewMap.clear();
    }
    
    @Override
    public void notifyChanged(Notification notification) {        
        if (notification.getFeature() == RestifPackage.Literals.PATH_FRAGMENT__CHILD) {
            PathFragment pathFragment = null;

            switch (notification.getEventType()) {
                case Notification.ADD:
                    int index = notification.getPosition();
                    addNewPathFragmentView((PathFragment) notification.getNewValue(), index);
                    updateRestTreeDisplay();
                    break;
                case Notification.REMOVE:
                    pathFragment = (PathFragment) notification.getOldValue();
                    removePathFragmentView(pathFragment);
                    // remove resource from resourceToViewMap
                    resourceToViewMap.remove(getResource(pathFragment));
                    updateRestTreeDisplay();
                    break;
                case Notification.MOVE:
                    // Reset the hash map to fix the new positions.
                    resetPathFragmentViews();
                    
                    // Recreate the views for all the path fragments.
                    PathFragment root = restIF.getRoot();
                    initPathFragmentViews(root);
                    
                    updateRestTreeDisplay();
                    break;
            }
        } else if (notification.getFeature() == RestifPackage.Literals.REST_IF__ROOT) {
            switch (notification.getEventType()) {
                case Notification.SET:                    
                    if (notification.getOldValue() != null) {
                        PathFragment root = (PathFragment) notification.getOldValue();
                        removePathFragmentView(root);
                    } else {
                        addNewPathFragmentView((PathFragment) notification.getNewValue(), NO_SPECIFIED_INDEX);
                        updateRestTreeDisplay();
                    }
                    break;
            }
        } else if (notification.getFeature() == RestifPackage.Literals.STATIC_FRAGMENT__INTERNALNAME) {
            updateRestTreeDisplay();
        } else if (notification.getFeature() == RestifPackage.Literals.DYNAMIC_FRAGMENT__PLACEHOLDER) {
            updateRestTreeDisplay();
        }
    }

    @Override
    protected void registerGestureListeners(IGestureEventListener listener) {
        super.registerGestureListeners(listener);

        addGestureListener(TapProcessor.class, listener);
        addGestureListener(TapAndHoldProcessor.class, listener);
        addGestureListener(TapAndHoldProcessor.class, new TapAndHoldVisualizer(RamApp.getApplication(), getParent()));
    }

    @Override
    protected void registerInputProcessors() {
        registerInputProcessor(new TapProcessor(RamApp.getApplication(), GUIConstants.TAP_MAX_FINGER_UP, false,
                GUIConstants.TAP_DOUBLE_TAP_TIME));
        registerInputProcessor(new TapAndHoldProcessor(RamApp.getApplication(), GUIConstants.TAP_AND_HOLD_DURATION));
        registerInputProcessor(new PanProcessorTwoFingers(RamApp.getApplication()));
        registerInputProcessor(new RightClickDragProcessor(RamApp.getApplication()));
        registerInputProcessor(new ZoomProcessor(RamApp.getApplication()));
        registerInputProcessor(new MouseWheelProcessor(RamApp.getApplication()));

        UnistrokeProcessorLeftClick up = new UnistrokeProcessorLeftClick(RamApp.getApplication());

        registerInputProcessor(up);
    }
    
    /**
     * Removes the given {@link BaseView} from the list of selected views.
     * 
     * @param baseView the {@link BaseView} to remove
     */
    public void elementDeselected(BaseView<?> baseView) {
        selectedElements.remove(baseView);
    }

    /**
     * Adds the given {@link BaseView} to the list of selected views.
     * 
     * @param baseView the baseView to add to the selection
     */
    public void elementSelected(BaseView<?> baseView) {
        selectedElements.add(baseView);
    }
    
    /**
     * Returns the list of selected views.
     * 
     * @return selectedElements
     */
    public Set<BaseView<?>> getSelectedElements() {
        return selectedElements;
    }

    /**
     * Deselects all currently selected classifiers.
     */
    public void deselect() {
        // Use separate set here, otherwise a concurrent modification occurs,
        // because the view notifies us that it was deselected, which triggers
        // the removal of the view from our set.
        for (BaseView<?> baseView : new HashSet<BaseView<?>>(selectedElements)) {
            baseView.setSelect(false);
        }

        selectedElements.clear();
    }
    
    /**
     * Returns every {@link PathFragmentView} in the hash map.
     * 
     * @return every {@link PathFragmentView} in the hash map
     */
    public ArrayList<PathFragmentView> getAllPathFragmentViews() {
        return new ArrayList<>(pathFragmentToViewMap.values());
    }
    
    /**
     * Returns the parent {@link PathFragmentView} of a given {@link PathFragmentView}.
     * 
     * @param pathFragmentView - the given {@link PathFragmentView}
     * @return the parent {@link PathFragmentView}
     */
    public PathFragmentView getParentPathFragmentViewOf(PathFragmentView pathFragmentView) {
        return pathFragmentToViewMap.get(getParent(pathFragmentView.getPathFragment()));
    }


    /**
     * Returns the position of a given {@link PathFragmentView} in terms of its siblings.
     * 
     * @param pathFragmentView - the given {@link PathFragmentView}
     * @return the position of the given {@link PathFragmentView}
     */
    public int getPositionOf(PathFragmentView pathFragmentView) {
        return pathFragmentToViewMap.get(getParent(pathFragmentView.getPathFragment()))
                .getChildrenView().indexOf(pathFragmentView);
    }

    /**
     * Returns the views of a given {@link PathFragmentView} under the same parent.
     * 
     * @param pathFragmentView - the given {@link PathFragmentView}
     * @return an array list of all sibling views
     */
    public ArrayList<PathFragmentView> getSiblingViews(PathFragmentView pathFragmentView) {
        ArrayList<PathFragmentView> siblingViews = new ArrayList<>();
        siblingViews.addAll(pathFragmentToViewMap.get(getParent(pathFragmentView.getPathFragment())).getChildrenView());
        siblingViews.remove(pathFragmentView);
        
        return siblingViews;
    }
}
