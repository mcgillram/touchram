package ca.mcgill.sel.restif.ui.views;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.edit.provider.INotifyChangedListener;
import org.mt4j.components.MTComponent;
import org.mt4j.util.MTColor;
import org.mt4j.util.math.Vector3D;
import ca.mcgill.sel.ram.ui.components.RamLineComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent.Alignment;
import ca.mcgill.sel.ram.ui.layouts.VerticalLayout;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.Fonts;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.restif.PathFragment;
import ca.mcgill.sel.restif.RestifPackage;
import ca.mcgill.sel.restif.ui.views.handler.IBaseViewHandler;
import ca.mcgill.sel.restif.ui.views.handler.RestTreeHandlerFactory;

/**
 * The visual representation for a {@link PathFragment}.
 * 
 * @author Bowen
 */
public class PathFragmentView extends BaseView<IBaseViewHandler> implements INotifyChangedListener {

    private static final float MINIMUM_WIDTH = 136.0f;

    // These variables are used to draw the line between parent and child.
    // We use separate variables because when a box is zoomed its x and y coordinates are incorrect for the line.
    private float xposition;
    private float yposition;
    
    private PathFragment pathFragment;
    private ArrayList<PathFragmentView> childrenView;
    private RamLineComponent lineToParent;
    
    /**
     * Creates a new {@link PathFragmentView} for the given {@link PathFragment}.
     * 
     * @param restTreeView - the parent view representing the {@link RestIF} model
     * @param pathFragment - the represented {@link PathFragment}
     * @param vector - the coordinates of where the {@link PathFragmentView} is to be placed
     */
    public PathFragmentView(RestTreeView restTreeView, PathFragment pathFragment, Vector3D vector) {
        super(restTreeView, pathFragment, vector);
                
        this.pathFragment = pathFragment;
        this.childrenView = new ArrayList<>();
        
        setMinimumWidth(MINIMUM_WIDTH);
        setAnchor(PositionAnchor.UPPER_LEFT);

        setLayout(new VerticalLayout());
        
        setNoFill(false);
        setFillColor(Colors.CLASS_VIEW_DEFAULT_FILL_COLOR);  
        setStrokeColor(Colors.DEFAULT_ELEMENT_STROKE_COLOR);
        setNoStroke(false);
        
        addNameField();
    }

    /**
     * Adds the {@link TextView} of the particular {@link PathFragmentVIew}.
     */
    private void addNameField() {
        TextView nameTextView = new TextView(pathFragment, RestifPackage.Literals.NAMED_ELEMENT__NAME);

        nameTextView.setFont(Fonts.DEFAULT_FONT_MEDIUM);
        nameTextView.setAlignment(Alignment.CENTER_ALIGN);
        nameTextView.setNewlineDisabled(true);
        nameTextView.setNoFill(true);
        nameTextView.setUniqueName(false);
        nameTextView.setPlaceholderText("Enter resource name:");
        nameTextView.setHandler(RestTreeHandlerFactory.INSTANCE.getPathFragmentNameHandler());
        
        setNameField(nameTextView);
    }
    
    /**
     * Adds a child {@link PathFragmentView} to its children array list to keep reference.
     * 
     * @param child - the given {@link PathFragmentView}
     */
    public void addChildPathFragmentView(PathFragmentView child) {
        childrenView.add(child);
    }
    
    /**
     * Adds a child {@link PathFragmentView} by given index to its children array list to keep reference.
     * 
     * @param child - the given {@link PathFragmentView}
     * @param index - the index of the newly added {@link PathFragment}
     */
    public void addChildPathFragmentView(PathFragmentView child, int index) {
        childrenView.add(index, child);
    }
    
    /**
     * Removes a child {@link PathFragmentView} from its children array list.
     * 
     * @param child - the given {@link PathFragmentView}
     */
    public void removeChildPathFragmentView(PathFragmentView child) {
        childrenView.remove(child);
    }

    /**
     * Adds an {@link ResourceView} as a child to have the resource rectangle within this {@link PathFragmentView}.
     * 
     * @param resourceView - the given {@link ResourceView}
     */
    public void addChildView(ResourceView resourceView) {
        addChild(resourceView);
    }

    /**
     * Returns the represented {@link PathFragment}.
     * 
     * @return pathFragment
     */
    public PathFragment getPathFragment() {
        return pathFragment;
    }

    /**
     * Displays the keyboard.
     */
    public void showKeyboard() {
        nameField.showKeyboard();
    }

    /**
     * Clear the name field of this view.
     */
    public void clearNameField() {
        ((TextView) nameField).clearText();
    }

    @Override
    public void notifyChanged(Notification notification) {                
        // When a child is added, removed, or moved a notification is sent on all path fragments,
        // thus we redirect the notification to RestTreeView to be handled.
        if (notification.getFeature() == RestifPackage.Literals.PATH_FRAGMENT__CHILD) {
            switch (notification.getEventType()) {                
                case Notification.ADD:
                    PathFragment parent = restTreeView.getParent((PathFragment) notification.getNewValue());

                    if (parent != null) {                    
                        if (parent.equals(pathFragment)) {                                   
                            restTreeView.notifyChanged(notification);
                        }
                    }
                    break;
                case Notification.REMOVE:   
                    if (childrenView.size() > 0) {
                        if (childrenView.contains(restTreeView.getPathFragmentViewOf(
                                (PathFragment) notification.getOldValue()))) {                                
                            restTreeView.notifyChanged(notification);
                        }
                    }
                    break;
                case Notification.MOVE:
                    if (childrenView.size() > 0) {
                        if (childrenView.contains(restTreeView.getPathFragmentViewOf(
                                (PathFragment) notification.getNewValue()))) {
                            restTreeView.notifyChanged(notification);
                        }
                    }
                    break;
            }
        } else if (notification.getFeature() == RestifPackage.Literals.STATIC_FRAGMENT__INTERNALNAME) {
            restTreeView.notifyChanged(notification);
        } else if (notification.getFeature() == RestifPackage.Literals.DYNAMIC_FRAGMENT__PLACEHOLDER) {
            restTreeView.notifyChanged(notification);
        }
    }
    
    @Override
    public void destroy() {
        this.destroyAllChildren();
        super.destroy();
    }
    
    /**
     * Returns the views of all of its children.
     * 
     * @return childrenView
     */
    public ArrayList<PathFragmentView> getChildrenView() {
        return childrenView;
    }

    /**
     * Returns the {@link RamLineComponent} to its parent.
     * 
     * @return lineToParent
     */
    public RamLineComponent getLineToParent() {
        return lineToParent;
    }

    /**
     * Sets the {@link RamLineComponent} to its parent.
     * 
     * @param lineToParent - the given {@link RamLineComponent}
     */
    public void setLineToParent(RamLineComponent lineToParent) {
        this.lineToParent = lineToParent;
    }

    /**
     * Recursive function which collects all the DragContainers of itself and its children.
     * 
     * @return the list of components to be dragged
     */
    public List<MTComponent> getAllDragContainers() {
        List<MTComponent> components = new LinkedList<MTComponent>();
        for (PathFragmentView child : childrenView) {
            if (child.lineToParent != null) {
                components.add(child.lineToParent);
            }
            components.addAll(child.getAllDragContainers());
            components.add(child);
        }
        return components;
    }
    
    /**
     * Highlight the feature.
     * 
     * @param highlight - whether we want to highlight the feature or not
     */
    public void highlight(boolean highlight) {
        MTColor color = highlight ? Colors.HIGHLIGHT_ELEMENT_STROKE_COLOR : Colors.DEFAULT_ELEMENT_STROKE_COLOR;
        this.setStrokeColor(color);
    }
    
    /**
     * Returns the parent {@link PathFragmentView} of this {@link PathFragmentView}.
     * 
     * @return the parent {@link PathFragmentView}
     */
    public PathFragmentView getParentPathFragmentView() {
        return this.getParentOfType(RestTreeView.class).getParentPathFragmentViewOf(this);
    }

    /**
     * Returns the position of this {@link PathFragmentView} in terms of its siblings.
     * 
     * @return the position of this {@link PathFragmentView}
     */
    public int getCurrentPosition() {
        return this.getParentOfType(RestTreeView.class).getPositionOf(this);
    }

    /**
     * Returns the views of this {@link PathFragmentView} under the same parent.
     * 
     * @return an array list of all sibling views
     */
    public ArrayList<PathFragmentView> getSiblingViews() {
        return this.getParentOfType(RestTreeView.class).getSiblingViews(this);
    }
    
    /**
     * Returns the x coordinate.
     * 
     * @return xposition
     */
    public float getXposition() {
        return xposition;
    }

    /**
     * Sets the x coordinate.
     * 
     * @param xposition - the given x position
     */
    public void setXposition(float xposition) {
        this.xposition = xposition;
    }

    /**
     * Returns the y coordinate.
     * 
     * @return xposition
     */
    public float getYposition() {
        return yposition;
    }

    /**
     * Sets the y coordinate.
     * 
     * @param yposition - the given y position
     */
    public void setYposition(float yposition) {
        this.yposition = yposition;
    }
}
