package ca.mcgill.sel.environmentmodel.ui.utils;

import ca.mcgill.sel.environmentmodel.ui.scenes.handler.impl.DisplayEnvironmentModelSceneHandler;
import ca.mcgill.sel.environmentmodel.ui.views.handler.IActorViewHandler;
import ca.mcgill.sel.environmentmodel.ui.views.handler.IBaseViewHandler;
import ca.mcgill.sel.environmentmodel.ui.views.handler.ICommunicationDiagramViewHandler;
import ca.mcgill.sel.environmentmodel.ui.views.handler.IMessageDetailViewHandler;
import ca.mcgill.sel.environmentmodel.ui.views.handler.IMessageInformationViewHandler;
import ca.mcgill.sel.environmentmodel.ui.views.handler.IMessageViewHandler;
import ca.mcgill.sel.environmentmodel.ui.views.handler.IParameterViewHandler;
import ca.mcgill.sel.environmentmodel.ui.views.handler.ISystemBoxViewHandler;
import ca.mcgill.sel.environmentmodel.ui.views.handler.ITimeTriggeredEventViewHandler;
import ca.mcgill.sel.environmentmodel.ui.views.handler.impl.ActorMultiplicityHandler;
import ca.mcgill.sel.environmentmodel.ui.views.handler.impl.ActorViewHandler;
import ca.mcgill.sel.environmentmodel.ui.views.handler.impl.CommunicationDiagramViewHandler;
import ca.mcgill.sel.environmentmodel.ui.views.handler.impl.CommunicationMultiplicityHandler;
import ca.mcgill.sel.environmentmodel.ui.views.handler.impl.MessageDetailViewHandler;
import ca.mcgill.sel.environmentmodel.ui.views.handler.impl.MessageInformationViewHandler;
import ca.mcgill.sel.environmentmodel.ui.views.handler.impl.MessageViewHandler;
import ca.mcgill.sel.environmentmodel.ui.views.handler.impl.NoteViewHandler;
import ca.mcgill.sel.environmentmodel.ui.views.handler.impl.ParameterViewHandler;
import ca.mcgill.sel.environmentmodel.ui.views.handler.impl.SystemBoxViewHandler;
import ca.mcgill.sel.environmentmodel.ui.views.handler.impl.TimeTriggeredEventViewHandler;
import ca.mcgill.sel.environmentmodel.ui.views.handler.impl.TypeNameHandler;
import ca.mcgill.sel.ram.ui.views.handler.IDisplaySceneHandler;
import ca.mcgill.sel.ram.ui.views.handler.ITextViewHandler;
import ca.mcgill.sel.ram.ui.views.handler.impl.TextViewHandler;

public final class EmHandlerFactory {
    /**
     * The singleton instance of the factory.
     */
    public static final EmHandlerFactory INSTANCE = new EmHandlerFactory();
    
    private IDisplaySceneHandler communicationDiagramDisplaySceneHandler;
    private ICommunicationDiagramViewHandler communicationDiagramViewHandler;
    private ISystemBoxViewHandler systemBoxViewHandler;
    private IActorViewHandler actorViewHandler;
    private ITextViewHandler noteContentHandler;
    private ITextViewHandler textViewHandler;
    private ITextViewHandler actorMultiplicityViewHandler;
    private ITextViewHandler actoCommunicationMultiplicityViewHandler;
    private ITimeTriggeredEventViewHandler timeTriggeredEventViewHandler;
    private IParameterViewHandler parameterViewHandler;
    private ITextViewHandler typeNameHandler;
    private IMessageInformationViewHandler messageInformationViewHandler;
    private IMessageDetailViewHandler messageDetailViewHandler;
    private IMessageViewHandler messageViewHandler;
    private NoteViewHandler noteViewHandler;
    
    private EmHandlerFactory() {      
    }
    
    /**
     * Gets the handler for the use case diagram scene.
     * @return The handler for the scene
     */
    public IDisplaySceneHandler getCommunicationDiagramDisplaySceneHandler() {
        if (this.communicationDiagramDisplaySceneHandler == null) {
            this.communicationDiagramDisplaySceneHandler = new DisplayEnvironmentModelSceneHandler();
        }
        
        return this.communicationDiagramDisplaySceneHandler;
    }
    
    /**
     * Gets the handler for the use case diagram view.
     * @return The handler for the view
     */
    public ICommunicationDiagramViewHandler getCommunicationDiagramViewHandler() {
        if (this.communicationDiagramViewHandler == null) {
            this.communicationDiagramViewHandler = new CommunicationDiagramViewHandler();
        }
        
        return this.communicationDiagramViewHandler;
    }
    
    /**
     * Gets the handler for the actor view.
     * @return The handler for the view
     */
    public IActorViewHandler getActorViewHandler() {
        if (this.actorViewHandler == null) {
            this.actorViewHandler = new ActorViewHandler();
        }
        
        return this.actorViewHandler;
    }
    
    /**
     * Gets the handler for the note content.
     * @return the handler
     */
    public ITextViewHandler getNoteContentHandler() {
        if (this.noteContentHandler == null) {
            this.noteContentHandler = new TextViewHandler();
        }
        
        return this.noteContentHandler;
    }
    
    /**
     * Gets a handler for generic text views.
     * @return the handler
     */
    public ITextViewHandler getTextViewHandler() {
        if (this.textViewHandler == null) {
            this.textViewHandler = new TextViewHandler();
        }
        
        return this.textViewHandler;
    }
    
    /**
     * Gets the actor multiplicity text view handler.
     * @return the handler
     */
    public ITextViewHandler getActorMultiplicityViewHandler() {
        if (this.actorMultiplicityViewHandler == null) {
            this.actorMultiplicityViewHandler = new ActorMultiplicityHandler();
        }
        
        return this.actorMultiplicityViewHandler;
    }
    
    public ITextViewHandler getActorCommunicationMultiplicityHandler() {
        if (this.actoCommunicationMultiplicityViewHandler == null) {
            this.actoCommunicationMultiplicityViewHandler = new CommunicationMultiplicityHandler();
        }
        
        return this.actoCommunicationMultiplicityViewHandler;
    }
    
    public ISystemBoxViewHandler getSystemBoxViewHandler() {
        if (this.systemBoxViewHandler == null) {
            this.systemBoxViewHandler = new SystemBoxViewHandler();
        }
        return this.systemBoxViewHandler;
    }
    
    public ITimeTriggeredEventViewHandler getTimeTriggeredEventViewHandler() {
        if (this.timeTriggeredEventViewHandler == null) {
            this.timeTriggeredEventViewHandler = new TimeTriggeredEventViewHandler();
        }
        return this.timeTriggeredEventViewHandler;
    }
 
    public IParameterViewHandler getParameterViewHandler() {
        if (this.parameterViewHandler == null) {
            this.parameterViewHandler = new ParameterViewHandler();
        }
        return this.parameterViewHandler;
    }    

    public ITextViewHandler getTypeNameHandler() {
        if (this.typeNameHandler == null) {
            this.typeNameHandler = new TypeNameHandler();
        }
        return this.typeNameHandler;
    }

    public IMessageInformationViewHandler getMessageInformationViewHandler() {
        if (this.messageInformationViewHandler == null) {
            this.messageInformationViewHandler = new MessageInformationViewHandler();
        }
        return this.messageInformationViewHandler;
    }

    public IMessageDetailViewHandler getMessageDetailViewHandler() {
        if (this.messageDetailViewHandler == null) {
            this.messageDetailViewHandler = new MessageDetailViewHandler();
        }
        return this.messageDetailViewHandler;
    }
    
    public IMessageViewHandler getMessageViewHandler() {
        if (this.messageViewHandler == null) {
            this.messageViewHandler = new MessageViewHandler();
        }
        return this.messageViewHandler;
    }

    public NoteViewHandler getNoteViewHandler() {
        if (this.noteViewHandler == null) {
            this.noteViewHandler = new NoteViewHandler();
        }
        return noteViewHandler;
    }
   
}
