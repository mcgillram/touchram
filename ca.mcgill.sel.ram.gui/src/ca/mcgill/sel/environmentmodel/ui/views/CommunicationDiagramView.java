package ca.mcgill.sel.environmentmodel.ui.views;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.edit.provider.INotifyChangedListener;
import org.mt4j.components.TransformSpace;
import org.mt4j.input.gestureAction.TapAndHoldVisualizer;
import org.mt4j.input.inputProcessors.IGestureEventListener;
import org.mt4j.input.inputProcessors.componentProcessors.panProcessor.PanProcessorTwoFingers;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldProcessor;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapProcessor;
import org.mt4j.input.inputProcessors.componentProcessors.unistrokeProcessor.UnistrokeUtils.Direction;
import org.mt4j.input.inputProcessors.componentProcessors.unistrokeProcessor.UnistrokeUtils.UnistrokeGesture;
import org.mt4j.input.inputProcessors.componentProcessors.zoomProcessor.ZoomProcessor;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.events.MouseWheelProcessor;
import ca.mcgill.sel.ram.ui.events.RightClickDragProcessor;
import ca.mcgill.sel.ram.ui.events.UnistrokeProcessorLeftClick;
import ca.mcgill.sel.ram.ui.utils.GUIConstants;
import ca.mcgill.sel.ram.ui.views.AbstractView;
import ca.mcgill.sel.ram.ui.views.RelationshipView;
import ca.mcgill.sel.environmentmodel.EnvironmentModel;
import ca.mcgill.sel.environmentmodel.LayoutElement;
import ca.mcgill.sel.environmentmodel.NamedElement;
import ca.mcgill.sel.environmentmodel.Note;
import ca.mcgill.sel.environmentmodel.TimeTriggeredEvent;
import ca.mcgill.sel.environmentmodel.Actor;
import ca.mcgill.sel.environmentmodel.ActorActorCommunication;
import ca.mcgill.sel.environmentmodel.EmFactory;
import ca.mcgill.sel.environmentmodel.EmPackage;
import ca.mcgill.sel.environmentmodel.impl.ContainerMapImpl;
import ca.mcgill.sel.environmentmodel.impl.ElementMapImpl;
import ca.mcgill.sel.environmentmodel.language.controller.EnvironmentModelControllerFactory;
import ca.mcgill.sel.environmentmodel.ui.utils.EmHandlerFactory;
import ca.mcgill.sel.environmentmodel.ui.views.handler.ICommunicationDiagramViewHandler;

public class CommunicationDiagramView extends AbstractView<ICommunicationDiagramViewHandler>
        implements INotifyChangedListener {
    
    private EnvironmentModel environmentModel;
    private ContainerMapImpl layout;
    
    private Set<BaseView<?>> selectedElements;
    
    private HashMap<Actor, ActorView> actorToViewMap;
    private HashMap<ActorActorCommunication, ActorActorCommunicationView> communicationToViewMap;
    private HashMap<Actor, ActorMessagesView> messageToViewMap;    
    private HashMap<TimeTriggeredEvent, TimeTriggeredEventView> eventToViewMap;   
    private HashMap<Note, NoteView> noteToViewMap;
    
    private List<AnnotationView> annotationViewList;
    
    private SystemBoxView systemBoxView;
        
    public CommunicationDiagramView(EnvironmentModel em, ContainerMapImpl layout, float width, float height) {
        super(width, height);
        
        this.environmentModel = em;
        this.layout = layout;
        
        this.actorToViewMap = new HashMap<Actor, ActorView>();
        this.communicationToViewMap = new HashMap<ActorActorCommunication, ActorActorCommunicationView>();
        this.eventToViewMap = new HashMap<TimeTriggeredEvent, TimeTriggeredEventView>();
        this.messageToViewMap = new HashMap<Actor, ActorMessagesView>();
        this.noteToViewMap = new HashMap<Note, NoteView>();
        
        this.annotationViewList = new ArrayList<AnnotationView>();
        
        buildAndLayout(width, height);

        EMFEditUtil.addListenerFor(em, this);

        // register to the ContainerMap to receive adds/removes of ElementMaps
        EMFEditUtil.addListenerFor(this.layout, this);
        
        this.selectedElements = new HashSet<BaseView<?>>();
                
    }

    @Override
    public void notifyChanged(Notification notification) {

        // The map as a whole can be changed
        if (notification.getFeature() == EmPackage.Literals.CONTAINER_MAP__VALUE) {
            if (notification.getEventType() == Notification.ADD) {
                ElementMapImpl elementMap = (ElementMapImpl) notification.getNewValue();
                if (elementMap.getKey() instanceof Actor) {
                    actorToViewMap.get(elementMap.getKey()).setLayoutElement(elementMap.getValue());
                } else if (elementMap.getKey() instanceof Note) {
                    noteToViewMap.get(elementMap.getKey()).setLayoutElement(elementMap.getValue());
                }
            }
        } else if (notification.getFeature() == EmPackage.Literals.ENVIRONMENT_MODEL__ACTORS) {
            Actor actor = null;
            switch (notification.getEventType()) {
                case Notification.ADD:
                    actor = (Actor) notification.getNewValue();
                    addActorView(actor, layout.getValue().get(actor));
                    break;
                case Notification.REMOVE:
                    actor = (Actor) notification.getOldValue();
                    deleteActorView(actor);
                    break;
            }
        } else if (notification.getFeature() == EmPackage.Literals.ENVIRONMENT_MODEL__NOTES) {
            Note note = null;
            switch (notification.getEventType()) {
                case Notification.ADD:
                    note = (Note) notification.getNewValue();
                    addNoteView(note, layout.getValue().get(note));
                    for (NamedElement annotated : note.getAnnotatedElements()) {
                        addAnnotationView(note, annotated);
                    }
                    break;
                case Notification.REMOVE:
                    note = (Note) notification.getOldValue();
                    deleteNoteView(note);
                    break;
            }
        
        } else if (notification.getFeature() == EmPackage.Literals.ENVIRONMENT_MODEL__COMMUNICATIONS) {
            ActorActorCommunication communication = null;
            switch (notification.getEventType()) {
                case Notification.REMOVE:
                    communication = (ActorActorCommunication) notification.getOldValue();
                    removeActorActorCommunicationView(communication);
                    break;
                case Notification.ADD:
                    communication = (ActorActorCommunication) notification.getNewValue();
                    addActorActorCommunicationView(communication);             
                    break;
            }
       
        }      
    }
    
    @Override
    protected void registerGestureListeners(IGestureEventListener listener) {
        super.registerGestureListeners(listener);

        addGestureListener(TapProcessor.class, listener);
        addGestureListener(TapAndHoldProcessor.class, listener);
        addGestureListener(TapAndHoldProcessor.class, new TapAndHoldVisualizer(RamApp.getApplication(), getParent()));
    }

    @Override
    protected void registerInputProcessors() {
        registerInputProcessor(new TapProcessor(RamApp.getApplication(), GUIConstants.TAP_MAX_FINGER_UP, false,
                GUIConstants.TAP_DOUBLE_TAP_TIME));
        registerInputProcessor(new TapAndHoldProcessor(RamApp.getApplication(), GUIConstants.TAP_AND_HOLD_DURATION));
        registerInputProcessor(new PanProcessorTwoFingers(RamApp.getApplication()));
        registerInputProcessor(new RightClickDragProcessor(RamApp.getApplication()));
        registerInputProcessor(new ZoomProcessor(RamApp.getApplication()));
        registerInputProcessor(new MouseWheelProcessor(RamApp.getApplication()));

        UnistrokeProcessorLeftClick up = new UnistrokeProcessorLeftClick(RamApp.getApplication());
        up.addTemplate(UnistrokeGesture.CIRCLE, Direction.CLOCKWISE);
        up.addTemplate(UnistrokeGesture.CIRCLE, Direction.COUNTERCLOCKWISE);

        registerInputProcessor(up);
    }
    
    /**
     * Gets the environment model associated with the view.
     * @return The environment model
     */
    public EnvironmentModel getEnvironmentModel() {
        return this.environmentModel;
    }
    
    /**
     * Returns a collection of all base views.
     * This includes {@link ClassView}, {@link NoteView}, {@link ImplementationClassView} and {@link SystemBoxView}.
     * 
     * @return a collection of all base views
     */
    public Collection<BaseView<?>> getBaseViews() {
        Collection<BaseView<?>> baseViews = new HashSet<BaseView<?>>();
        baseViews.addAll(actorToViewMap.values());
        baseViews.add(systemBoxView);
        baseViews.addAll(noteToViewMap.values());
        return baseViews;
    }
    
    /**
     * Removes the given {@link BaseView} from the list of selected views.
     * 
     * @param baseView the {@link BaseView} to remove
     */
    protected void elementDeselected(BaseView<?> baseView) {
        selectedElements.remove(baseView);
    }

    /**
     * Adds the given {@link BaseView} to the list of selected views.
     * 
     * @param baseView the baseView to add to the selection
     */
    protected void elementSelected(BaseView<?> baseView) {
        selectedElements.add(baseView);
    }
    
    public Set<BaseView<?>> getSelectedElements() {
        return this.selectedElements;
    }
    
    public ActorView getActorView(Actor actor) {
        return this.actorToViewMap.get(actor);
    }
    
    public TimeTriggeredEventView getTimeTriggeredEventView(TimeTriggeredEvent event) {
        return this.eventToViewMap.get(event);
    }
    
    public SystemBoxView getSystemBoxView() {
        return this.systemBoxView;
    }
    
    
    /**
     * Deselects all currently selected classifiers.
     */
    public void deselect() {
        // Use separate set here, otherwise a concurrent modification occurs,
        // because the view notifies us that it was deselected, which triggers
        // the removal of the view from our set.
        for (BaseView<?> baseView : new HashSet<BaseView<?>>(selectedElements)) {
            baseView.setSelect(false);
        }

        selectedElements.clear();
    }
    
    @Override
    public void destroy() {
        // remove listeners
        EMFEditUtil.removeListenerFor(layout, this);
        EMFEditUtil.removeListenerFor(this.environmentModel, this);

        for (ActorActorCommunicationView view : communicationToViewMap.values()) {
            view.destroy();
        }
       
        // destroy basic views
        for (BaseView<?> view : getBaseViews()) {
            view.destroy();
        }
        
        // do rest
        super.destroy();
    }    
    
    /**
     * Removes a relationship view from the model.
     * @param relationship The relationship view.
     */
    public void removeRelationshipView(RelationshipView<?, ?> relationship) {
        if (relationship instanceof ActorActorCommunicationView) {
            communicationToViewMap.remove(relationship);     
        } else if (relationship instanceof AnnotationView) {
            annotationViewList.remove(relationship);      
        }
    }
    
    private void buildAndLayout(float width, float height) {
        
        Vector3D position = new Vector3D(100, 100);
        float maxX = width;
        float maxY = height;

        if (layout == null || layout.getValue().get(getEnvironmentModel()) == null) {
            initializeSystemBoxView();
        } else {
            addSystemBoxView(layout.getValue().get(getEnvironmentModel()));
        }                     
        
        for (Actor actor : environmentModel.getActors()) {
            LayoutElement layoutElement = layout == null ? null : layout.getValue().get(actor);
            addActorView(actor, layoutElement);
            if (actor.getMessages().size() > 0) {
                addActorMessagesView(actor);
            }
        }

        for (ActorActorCommunication communication : environmentModel.getCommunications()) {
            addActorActorCommunicationView(communication);
        }        
        
        for (Note note : environmentModel.getNotes()) {
            LayoutElement layoutElement = layout == null ? null : layout.getValue().get(note);
            addNoteView(note, layoutElement);
            NoteView noteView = noteToViewMap.get(note);

            position.setX(Math.max(position.getX(), layoutElement.getX() + noteView.getWidth()));
            position.setY(Math.max(position.getY(), layoutElement.getY() + noteView.getHeight()));

            maxX = Math.max(maxX, noteView.getX() + noteView.getWidth());
            maxY = Math.max(maxY, noteView.getY() + noteView.getHeight());
        }

        // Create annotation links.
        for (Note note : environmentModel.getNotes()) {
            for (NamedElement annotatedElement : note.getAnnotatedElements()) {
                addAnnotationView(note, annotatedElement);
            }
        }              
       
    }
    
    private void addActorView(Actor actor, LayoutElement layoutElement) {
        ActorView view = new ActorView(this, actor, layoutElement);
        actorToViewMap.put(actor, view);
        addChild(view);
        view.setHandler(EmHandlerFactory.INSTANCE.getActorViewHandler()); 
    }
    
    private void deleteActorView(Actor actor) {
        ActorView actorView = this.actorToViewMap.remove(actor);        
        removeChild(actorView);
        actorView.destroy();   
    }  
       
    public void addActorActorCommunicationView(ActorActorCommunication communication) {

        Actor sender = communication.getParticipants().get(0);
        Actor receiver = communication.getParticipants().get(1);
        
        ActorActorCommunicationView communicationView = new ActorActorCommunicationView(communication, sender, 
                actorToViewMap.get(sender), receiver, actorToViewMap.get(receiver));

        communicationView.setBackgroundColor(this.getFillColor());
        communicationView.updateLines();
        communicationToViewMap.put(communication, communicationView);
        addChild(communicationView);
    }
    
    public void removeActorActorCommunicationView(ActorActorCommunication communication) {
        ActorActorCommunicationView communicationView = communicationToViewMap.get(communication);

        removeChild(communicationView);
        communicationToViewMap.remove(communication);
        communicationView.destroy();

    }
    
    public void initializeSystemBoxView() {
        
        float diagramHeight = this.getHeightXY(TransformSpace.RELATIVE_TO_PARENT);
        float diagramWidth = this.getWidthXY(TransformSpace.RELATIVE_TO_PARENT);
        float centerX = this.getPosition(TransformSpace.RELATIVE_TO_PARENT).x + diagramWidth / 2;
        float centerY = this.getPosition(TransformSpace.RELATIVE_TO_PARENT).x + diagramHeight / 2;

        LayoutElement layoutElement = EmFactory.eINSTANCE.createLayoutElement();
        layoutElement.setX(centerX - SystemBoxView.MINIMUM_WIDTH / 2);
        layoutElement.setY(centerY - SystemBoxView.MINIMUM_HIGHT / 2);

        EnvironmentModelControllerFactory.INSTANCE.getEnvironmentModelController()
            .createSystemBox(environmentModel, layoutElement);
       
        addSystemBoxView(layoutElement);
       
    }
    public void addSystemBoxView(LayoutElement layoutElement) {
        
        systemBoxView = new SystemBoxView(this, layoutElement);

        addChild(systemBoxView);
        systemBoxView.setHandler(EmHandlerFactory.INSTANCE.getSystemBoxViewHandler());
        
        systemBoxView.updateLayout();
       
    }
    
    public void addActorMessagesView(Actor actor) {
        ActorMessagesView messagesView = new ActorMessagesView(actor, actorToViewMap.get(actor), 
                getEnvironmentModel(), systemBoxView);
  
        messagesView.setBackgroundColor(this.getFillColor());      
        messagesView.updateLines();
        messageToViewMap.put(actor, messagesView);
        addChild(messagesView);
    } 

    public void removeActorMessagesView(Actor actor) {
        ActorMessagesView messagesView = messageToViewMap.get(actor);
        if (messagesView != null) {
            messagesView.destroy();
        }
        messageToViewMap.remove(actor);
    }
    
    /**
     * Add a note to the communication diagram view.
     * 
     * @param note that we want to add
     * @param layoutElement of the note view.
     */
    private void addNoteView(Note note, LayoutElement layoutElement) {
        // this view is registered as observer on the class (it is done in the class' constructor)
        NoteView noteView = new NoteView(this, note, layoutElement);

        noteToViewMap.put(note, noteView);

        addChild(noteView);

        noteView.setHandler(EmHandlerFactory.INSTANCE.getNoteViewHandler());
    }
    
    /**
     * Deletes some note from view.
     * 
     * @param note to be removed
     */
    private void deleteNoteView(final Note note) {
        RamApp.getApplication().invokeLater(new Runnable() {

            @Override
            public void run() {
                NoteView noteView = noteToViewMap.remove(note);
                removeChild(noteView);
                noteView.destroy();
            }
        });
    }
    
    /**
     * Adds an {@link AnnotationView} between the given note and class.
     * 
     * @param note the note
     * @param annotatedElement the annotated class
     */
    public void addAnnotationView(Note note, NamedElement annotatedElement) {
        BaseView<?> annotatedElementView = null;
        NoteView noteView = noteToViewMap.get(note);

        if (annotatedElement == environmentModel) {
            annotatedElementView = systemBoxView;
        } else if (actorToViewMap.containsKey(annotatedElement)) {
            annotatedElementView = actorToViewMap.get(annotatedElement);
//        } else if (communicationToViewMap.containsKey(annotatedElement)) {
//            annotatedElementView = communicationToViewMap.get(annotatedElement);
        } else if (annotatedElement == environmentModel) {
            annotatedElementView = systemBoxView;
        } else {
            return;
        }

        AnnotationView annotationView = new AnnotationView(noteView, annotatedElementView);

        annotationViewList.add(annotationView);

        addChild(annotationView);
        annotationView.updateLines();
    }
    
    /**
     * Removes the {@link AnnotationView} between the given note and class.
     * 
     * @param note the note
     * @param annotatedElement the annotated {@link NamedElement}
     */
    public void removeAnnotationView(Note note, NamedElement annotatedElement) {
        AnnotationView annotViewToBeDeleted = null;

        for (AnnotationView annotView : annotationViewList) {

            Note noteFrom = annotView.getNoteView().getNote();
            NamedElement annotatedElementTo = (NamedElement) annotView.getAnnotatedElementView().getRepresented();

            if (noteFrom == note && annotatedElementTo == annotatedElement) {
                annotViewToBeDeleted = annotView;
                break;
            }
        }

        if (annotViewToBeDeleted != null) {
            annotationViewList.remove(annotViewToBeDeleted);
            removeChild(annotViewToBeDeleted);
            annotViewToBeDeleted.destroy();
        }

    }
    
    /**
     * Returns {@link NoteView} for a specified {@link Note}.
     * 
     * @param note {@link Note} to retrieve view for
     * @return the {@link NoteView} for this note
     */
    public NoteView getNoteView(Note note) {
        if (noteToViewMap.containsKey(note)) {
            return noteToViewMap.get(note);
        }
        return null;
    }
}
