package ca.mcgill.sel.environmentmodel.ui.views.handler.impl;

import java.util.ArrayList;
import java.util.List;

import org.mt4j.components.TransformSpace;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.environmentmodel.language.controller.EnvironmentModelController;
import ca.mcgill.sel.environmentmodel.language.controller.EnvironmentModelControllerFactory;
import ca.mcgill.sel.environmentmodel.ui.views.ActorView;
import ca.mcgill.sel.environmentmodel.ui.views.BaseView;
import ca.mcgill.sel.environmentmodel.ui.views.handler.IActorViewHandler;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamSelectorComponent;
import ca.mcgill.sel.ram.ui.components.listeners.AbstractDefaultRamSelectorListener;
import ca.mcgill.sel.ram.ui.components.menu.RamLinkedMenu;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.environmentmodel.Actor;
import ca.mcgill.sel.environmentmodel.ActorActorCommunication;
import ca.mcgill.sel.environmentmodel.EnvironmentModel;
import ca.mcgill.sel.environmentmodel.MessageDirection;

public class ActorViewHandler extends BaseViewHandler implements IActorViewHandler {

    private static final String ACTION_ACTOR_DELETE_COMMUNICATION = "view.actor.communication.delete";   
    private static final String ACTION_ACTOR_INPUT_MESSAGE_ADD = "view.actor.inputmessage.add"; 
    private static final String ACTION_ACTOR_OUTPUT_MESSAGE_ADD = "view.actor.outputmessage.add"; 
    private static final String ACTION_ACTOR_NAME_SHOW = "view.actor.name.show"; 
    private static final String SUBMENU_CLASS_MORE = "sub.class.more";
        
    @Override
    public void removeRepresented(BaseView<?> baseView) {
        EnvironmentModelControllerFactory.INSTANCE.getEnvironmentModelController().removeActor(
                (Actor) baseView.getRepresented());
                        
    }
    
    @Override
    public void initMenu(RamLinkedMenu menu, TapAndHoldEvent event) {
        super.initMenu(menu, event);
        
        ActorView actorView = (ActorView) menu.getLinkedView();
        Actor actor = actorView.getActor();
        
        List<Actor> associatedActors = getAssociatedActors(actor);
        if (associatedActors.size() > 0) {
            menu.addAction(Strings.MENU_DELETE_COMMUNICATION,  Icons.ICON_MENU_CLEAR_SELECTION, 
                    ACTION_ACTOR_DELETE_COMMUNICATION, this, true);
        }
        
        menu.addAction(Strings.MENU_INPUT_MESSAGE_ADD,  Icons.ICON_MENU_ADD_INPUT_MESSAGE, 
                ACTION_ACTOR_INPUT_MESSAGE_ADD, this, true);
        menu.addAction(Strings.MENU_OUTPUT_MESSAGE_ADD,  Icons.ICON_MENU_ADD_OUTPUT_MESSAGE, 
                ACTION_ACTOR_OUTPUT_MESSAGE_ADD, this, true);
        
        menu.addSubMenu(1, SUBMENU_CLASS_MORE);   
        menu.addAction(Strings.MENU_SHOW_ACTOR_NAME, Strings.MENU_HIDE_ACTOR_NAME, Icons.ICON_MENU_SHOW,
                Icons.ICON_MENU_HIDE,
                ACTION_ACTOR_NAME_SHOW, this, SUBMENU_CLASS_MORE, true, actorView.nameShowed());
    }
    
    @Override
    public void actionPerformed(ActionEvent event) {
        String actionCommand = event.getActionCommand();
        RamRectangleComponent pressedButton = (RamRectangleComponent) event.getTarget();
        RamLinkedMenu linkedMenu = (RamLinkedMenu) pressedButton.getParentOfType(RamLinkedMenu.class);
        ActorView actorView = (ActorView) linkedMenu.getLinkedView();
        Actor actor = actorView.getActor();
        EnvironmentModel em = (EnvironmentModel) actor.eContainer();
        
        if (ACTION_ACTOR_DELETE_COMMUNICATION.equals(actionCommand)) {
            Vector3D position = linkedMenu.getPosition(TransformSpace.GLOBAL); 
            linkedMenu.destroy();
            deleteActorActorCommunication(actor, position);
        } else if (ACTION_ACTOR_INPUT_MESSAGE_ADD.equals(actionCommand)) {
            createMessage(em, actor, MessageDirection.INPUT);
        } else if (ACTION_ACTOR_OUTPUT_MESSAGE_ADD.equals(actionCommand)) {
            createMessage(em, actor, MessageDirection.OUTPUT);
        } else if (ACTION_ACTOR_NAME_SHOW.equals(actionCommand)) {
            actorView.showName();
            updateButtons(linkedMenu);
        } else {
            super.actionPerformed(event);
        }
    }
    
    /**
     * Updates buttons inside the menu.
     * 
     * @param menu - the menu which contains buttons.
     */
    private static void updateButtons(RamLinkedMenu menu) {
        ActorView actorView = (ActorView) menu.getLinkedView();

        menu.toggleAction(actorView.nameShowed(), ACTION_ACTOR_NAME_SHOW);

    }
    
    @SuppressWarnings("static-method")
    private void deleteActorActorCommunication(Actor actor, Vector3D menuPosition) {
        
        RamSelectorComponent<Actor> selector = 
                new RamSelectorComponent<Actor>(getAssociatedActors(actor));
        
        RamApp.getActiveScene().addComponent(selector, menuPosition);
        
        selector.registerListener(new AbstractDefaultRamSelectorListener<Actor>() {
           
            @Override
            public void elementSelected(RamSelectorComponent<Actor> selector, Actor element) {
                EnvironmentModelController controller = EnvironmentModelControllerFactory.INSTANCE
                        .getEnvironmentModelController();
                controller.removeActorActorCommunication(actor, element);
                selector.destroy();
                
            }
        });        
    }
    
    @SuppressWarnings("static-method")
    private void createMessage(EnvironmentModel em, Actor actor, MessageDirection direction) {
//        Set<String> names = new HashSet<String>();
//        for (MessageType messageType : em.getMessageTypes()) {
//            names.add(messageType.getName());
//        }      
//        String name = StringUtil.createUniqueName(Strings.DEFAULT_MESSAGE_TYPE_NAME, names);
        
        EnvironmentModelController controller = EnvironmentModelControllerFactory.INSTANCE
                .getEnvironmentModelController();
        controller.createMessage(em, actor, null, direction);
    }

    public static List<Actor> getAssociatedActors(Actor actor) {
        List<Actor> actors = new ArrayList<Actor>();
        EnvironmentModel em = (EnvironmentModel) actor.eContainer();
        
        for (ActorActorCommunication communication : em.getCommunications()) {          
            if (communication.getParticipants().get(0) == actor) {
                actors.add(communication.getParticipants().get(1));
            } else if (communication.getParticipants().get(1) == actor) {
                actors.add(communication.getParticipants().get(0));
            }
        }        
        return actors;
    }

}
