package ca.mcgill.sel.environmentmodel.ui.views.handler.impl;

import org.eclipse.emf.ecore.util.EcoreUtil;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;

import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.impl.TextViewHandler;
import ca.mcgill.sel.ram.util.MetamodelRegex;
import ca.mcgill.sel.environmentmodel.Actor;
import ca.mcgill.sel.environmentmodel.EnvironmentModel;
import ca.mcgill.sel.environmentmodel.language.controller.ActorController;
import ca.mcgill.sel.environmentmodel.language.controller.EnvironmentModelControllerFactory;
import ca.mcgill.sel.environmentmodel.ui.utils.EnvironmentModelUtils;
import ca.mcgill.sel.environmentmodel.util.Multiplicity;


public class ActorMultiplicityHandler extends TextViewHandler {

    @Override
    public void keyboardOpened(TextView textView) {
        // do nothing
    }

    @Override
    public boolean processTapEvent(TapEvent tapEvent) {
        if (tapEvent.isDoubleTap()) {
            TextView target = (TextView) tapEvent.getTarget();

            target.showKeyboard();
            return true;
        }

        return true;
    }
    
    @Override
    public boolean shouldDismissKeyboard(TextView textView) {
        String text = textView.getText();

        if (!text.matches(MetamodelRegex.REGEX_MULTIPLICITY)) {
            return false;
        }

        return setMultiplicity(text, (Actor) textView.getData());
    }
    
    @SuppressWarnings("static-method")
    private boolean setMultiplicity(String text, Actor actor) {
        Multiplicity multiplicity = EnvironmentModelUtils.parseMultiplicity(text);
        ActorController controller = EnvironmentModelControllerFactory.INSTANCE.getActorController();
        EnvironmentModel em = (EnvironmentModel) EcoreUtil.getRootContainer(actor);
        if (multiplicity != null) {
            controller.setMultiplicity(em, actor, multiplicity);
        }
        return true;
    }
}
