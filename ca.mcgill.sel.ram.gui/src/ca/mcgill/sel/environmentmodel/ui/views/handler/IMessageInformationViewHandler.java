package ca.mcgill.sel.environmentmodel.ui.views.handler;

import ca.mcgill.sel.ram.ui.events.listeners.ActionListener;

public interface IMessageInformationViewHandler extends IBaseViewHandler, ActionListener {

}
