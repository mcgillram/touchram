package ca.mcgill.sel.environmentmodel.ui.views;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.edit.provider.INotifyChangedListener;
import org.mt4j.components.MTComponent;
import org.mt4j.components.PickResult;
import org.mt4j.components.PickResult.PickEntry;
import org.mt4j.input.inputProcessors.MTGestureEvent;
import org.mt4j.input.inputProcessors.componentProcessors.dragProcessor.DragEvent;
import org.mt4j.input.inputProcessors.componentProcessors.scaleProcessor.ScaleEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;
import org.mt4j.input.inputProcessors.componentProcessors.unistrokeProcessor.UnistrokeEvent;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.CORECIElement;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.environmentmodel.EmPackage;
import ca.mcgill.sel.environmentmodel.EnvironmentModel;
import ca.mcgill.sel.environmentmodel.Message;
import ca.mcgill.sel.environmentmodel.MessageType;
import ca.mcgill.sel.environmentmodel.Parameter;
import ca.mcgill.sel.environmentmodel.ui.utils.EmHandlerFactory;
import ca.mcgill.sel.environmentmodel.ui.views.handler.IMessageViewHandler;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.ContainerComponent;
import ca.mcgill.sel.ram.ui.components.RamButton;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.ram.ui.events.WheelEvent;
import ca.mcgill.sel.ram.ui.layouts.HorizontalLayoutVerticallyCentered;
import ca.mcgill.sel.ram.ui.utils.GraphicalUpdater;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.IHandled;

/**
 * A horizontal list of texts that represent an {@link Message}. They update according to the changes in the model.
 *
 * @author qiutan
 */
public class MessageView extends ContainerComponent<IMessageViewHandler> implements INotifyChangedListener {
    //public class MessageView extends RamRectangleComponent implements INotifyChangedListener,
    //        IHandled<IMessageViewHandler> {

    /**
     * The buffer on the bottom (south) for each child).
     */
    public static final int BUFFER_BOTTOM = 0;

    /**
     * The textual representation of a parameter delimiter in an messages signature.
     */
    private static final String PARAMETER_DELIMITER = ",";

    private static final String BRACKET_OPEN = "(";
    private static final String BRACKET_CLOSE = ")";

    /**
     * The field for mapping cardinalities.
     */
    protected TextView mappingCardinalityField;

    private TextView nameField;

    private RamRectangleComponent parameterContainer;
    private RamTextComponent openBracketComponent;
    private RamTextComponent closeBracketComponent;

    private Message message;
    private MessageType messageType;
    private Map<Parameter, ParameterView> parameters;

    private GraphicalUpdater graphicalUpdater;

    private boolean isMutable;

    private COREArtefact artefact;

    /**
     * Creates an message view.
     *
     * @param message The message to display.
     * @param mutable Allow the message view to be editable.
     */
    public MessageView(Message message, boolean mutable) {
        this.message = message;
        this.messageType = message.getMessageType();
        isMutable = mutable;  

        openBracketComponent = new RamTextComponent(BRACKET_OPEN);
        openBracketComponent.setBufferSize(Cardinal.SOUTH, BUFFER_BOTTOM);
        openBracketComponent.setBufferSize(Cardinal.EAST, 1);
        openBracketComponent.setBufferSize(Cardinal.WEST, 1);

        closeBracketComponent = new RamTextComponent(BRACKET_CLOSE);
        closeBracketComponent.setBufferSize(Cardinal.SOUTH, BUFFER_BOTTOM);
        closeBracketComponent.setBufferSize(Cardinal.WEST, 1);

        build();

        EMFEditUtil.addListenerFor(message, this);
        EMFEditUtil.addListenerFor(message.getMessageType(), this);
        EnvironmentModel em = EMFModelUtil.getRootContainerOfType(message.getMessageType(), 
                EmPackage.Literals.ENVIRONMENT_MODEL);
        graphicalUpdater = RamApp.getApplication().getGraphicalUpdaterForModel(em);
        graphicalUpdater.addGUListener(message, this);

        // Add a listener to listen to any partiality change in the core metamodel
        artefact = COREArtefactUtil.getReferencingExternalArtefact(message);
        EMFEditUtil.addListenerFor(artefact, this);

        updateMappingCardinality();

        updateStyle();
    }

    /**
     * Creates an editable message view.
     *
     * @param message The message to display.
     */
    public MessageView(Message message) {
        this(message, true);
    }

    public void showParameters() {
        if (containsChild(parameterContainer)) {
            removeChild(parameterContainer);
        } else {
            addChild(parameterContainer);
            setLayout(new HorizontalLayoutVerticallyCentered(0));
        }
    }

    /**
     * Returns true if the message can be edited.
     * 
     * @return true if the message can be edited
     */
    public boolean isMutable() {
        return isMutable;
    }

    /**
     * Adds a parameter view for the given parameter at the given index.
     *
     * @param parameter the {@link Parameter} to add a view for
     * @param index the index to add the parameter at
     */
    private void addParameter(Parameter parameter, int index) {
        ParameterView parameterView = new ParameterView(parameter, isMutable);

        parameterView.setHandler(EmHandlerFactory.INSTANCE.getParameterViewHandler());

        parameters.put(parameter, parameterView);
        parameterContainer.addChild(index, parameterView);
    }

    /**
     * Adds a parameter delimiter at the given index.
     *
     * @param index the index the delimiter to add at
     */
    public void addDelimiter(int index) {
        RamTextComponent delimiter = new RamTextComponent(PARAMETER_DELIMITER);
        delimiter.setBufferSize(Cardinal.SOUTH, BUFFER_BOTTOM);
        delimiter.setBufferSize(Cardinal.WEST, 0);
        delimiter.setBufferSize(Cardinal.EAST, 2);
        parameterContainer.addChild(index, delimiter);
    }

    public void build() {
        parameters = new HashMap<Parameter, ParameterView>();          

        nameField = new TextView(message, EmPackage.Literals.MESSAGE__MESSAGE_TYPE);   
        nameField.setPlaceholderText(Strings.PH_ENTER_MESSAGE);
        nameField.setBufferSize(Cardinal.SOUTH, BUFFER_BOTTOM);
        nameField.setBufferSize(Cardinal.WEST, 1);
        nameField.setBufferSize(Cardinal.EAST, 1);
        nameField.setUniqueName(true);
        addChild(nameField);

        if (isMutable) {         
            nameField.setHandler(EmHandlerFactory.INSTANCE.getTypeNameHandler());
        }

        parameterContainer = new RamRectangleComponent();
        parameterContainer.setLayout(new HorizontalLayoutVerticallyCentered(0));        

        parameterContainer.addChild(openBracketComponent);

        for (Parameter parameter : message.getMessageType().getParameters()) {

            addParameter(parameter, parameterContainer.getChildCount());

            boolean lastParameter = message.getMessageType().getParameters().indexOf(parameter) 
                    == message.getMessageType().getParameters().size() - 1;
            if (!lastParameter) {
                addDelimiter(getChildCount());
            }
        }

        parameterContainer.addChild(closeBracketComponent);

        addChild(parameterContainer);

        setLayout(new HorizontalLayoutVerticallyCentered(0));

    }

    public void rebuild() {
        removeAllChildren();
        build();

        EMFEditUtil.removeListenerFor(messageType, this);
        this.messageType = message.getMessageType();
        EMFEditUtil.addListenerFor(messageType, this);
    }

    @Override
    public void destroy() {
        super.destroy();

        graphicalUpdater.removeGUListener(message, this);

        EMFEditUtil.removeListenerFor(message, this);
        EMFEditUtil.removeListenerFor(message.getMessageType(), this);

        if (artefact != null) {
            EMFEditUtil.removeListenerFor(artefact, this);
        }
    }


    @Override
    public IMessageViewHandler getHandler() {
        return handler;
    }

    /**
     * Returns the view parameter for the given parameter.
     *
     * @param parameter the parameter
     * @param position the position in the model
     * @return the view index for the parameter view
     */
    private int getIndexForParameter(Parameter parameter, int position) {
        int index = getChildIndexOf(openBracketComponent) + 1;

        if (position > 0) {
            Parameter previous = ((MessageType) parameter.eContainer()).getParameters().get(position - 1);
            ParameterView visualPrevious = parameters.get(previous);
            index = visualPrevious.getParent().getChildIndexOf(visualPrevious) + 1;
        }

        return index;
    }

    /**
     * Returns the {@link Message} that this view represents.
     *
     * @return the {@link Message} represented by this view
     */
    public Message getMessage() {
        return message;
    }

    @Override
    public void notifyChanged(Notification notification) {
        if (notification.getNotifier() == message.getMessageType()) {
            if (notification.getFeature() == EmPackage.Literals.MESSAGE_TYPE__PARAMETERS) {
                switch (notification.getEventType()) {
                    case Notification.ADD:
                        Parameter parameter = (Parameter) notification.getNewValue();
                        // figure out the index where the parameter has to be added
//                        int visualIndex = getIndexForParameter(parameter, notification.getPosition());
//                        System.out.println("!!!visualIndex = "+visualIndex);
//                        if (!parameters.isEmpty()) {
//                            // if it is the first parameter we need to add the delimiter after
//                            if (notification.getPosition() == 0) {
//                                addDelimiter(visualIndex);
//                            } else {
//                                // otherwise we need it before the parameter view
//                                addDelimiter(visualIndex++);
//                            }
//                        }
                        int visualIndex = parameterContainer.getChildCount() - 1;
                        
                        if (!parameters.isEmpty()) {
                            addDelimiter(visualIndex++);
                        }
                        addParameter(parameter, visualIndex);
                        break;
                    case Notification.REMOVE:
                        parameter = (Parameter) notification.getOldValue();
                        removeParameter(parameter);
                        break;
                }
            }
        } else if (notification.getNotifier() == artefact) {
            if (notification.getFeature() == CorePackage.Literals.CORE_ARTEFACT__CI_ELEMENTS) {
                CORECIElement ciElement = (CORECIElement) notification.getNewValue();
                if (ciElement == null) {
                    CORECIElement ciOldElement = (CORECIElement) notification.getOldValue();
                    if (ciOldElement.getModelElement() == message) {
                        updateMappingCardinality();
                    }
                } else if (ciElement.getModelElement() == message) {
                    updateMappingCardinality();
                }
            } 
        } 
    }

    /**
     * Removes the view of the given parameter.
     *
     * @param parameter the parameter to remove the view for
     */
    private void removeParameter(final Parameter parameter) {
        if (parameters.containsKey(parameter)) {
            RamApp.getApplication().invokeLater(new Runnable() {

                @Override
                public void run() {
                    ParameterView parameterView = parameters.remove(parameter);

                    int index = parameterContainer.getChildIndexOf(parameterView);
                    parameterContainer.removeChild(parameterView);

                    if (!parameters.isEmpty()) {
                        // also remove the delimiter after if there is one
                        if (parameterContainer.getChildByIndex(index) != closeBracketComponent) {
                            parameterContainer.removeChild(index);
                        } else {
                            // and the last delimiter
                            parameterContainer.removeChild(index - 1);
                        }
                    }

                    parameterView.destroy();
                }
            });
        }
    }

    /**
     * Update the mappingCardinalityField and display it if it wasn't already there.
     * If the {@link CORECIElement} associated do no longer exists, we just remove the mappingCardinalityField.
     */
    private void updateMappingCardinality() {
        //        TODO Not used in Class Diagrams
        //        CORECIElement ciElement = COREArtefactUtil.getCIElementFor(message);
        //
        //        if (mappingCardinalityField == null && ciElement != null) {
        //
        //            mappingCardinalityField = new MappingCardinalityTextView(ciElement);
        //            mappingCardinalityField.setBufferSize(Cardinal.SOUTH, 0);
        //            mappingCardinalityField.setBufferSize(Cardinal.WEST, 0);
        //            mappingCardinalityField.setPlaceholderText(Strings.PH_CARDINALITY);
        //            mappingCardinalityField.setFont(Fonts.FONT_CLASS_NAME_REFERENCE);
        //            addChild(mappingCardinalityField);
        //
        //            mappingCardinalityField.setHandler(ClassDiagramHandlerFactory.INSTANCE.getMappingCardinalityHandler());
        //        } else if (ciElement == null && mappingCardinalityField != null) {
        //            removeChild(mappingCardinalityField);
        //            mappingCardinalityField = null;
        //        }
    }


    /**
     * Returns the name field component.
     * 
     * @return the name field component.
     */
    public TextView getNameField() {
        return nameField;
    }

    public RamRectangleComponent getParameterContainer() {
        return parameterContainer;
    }

    @Override
    public boolean processGestureEvent(MTGestureEvent gestureEvent) {
        Vector3D pickPoint = null;
        boolean stopProcessing = false;

        if (gestureEvent instanceof TapEvent) {
            // prevent a tap event from being processed when a tap and hold event was processed before
            if (tapAndHoldPerformed) {
                tapAndHoldPerformed = false;
                stopProcessing = true;
            }

            pickPoint = ((TapEvent) gestureEvent).getLocationOnScreen();
        } else if (gestureEvent instanceof DragEvent) {
            pickPoint = ((DragEvent) gestureEvent).getFrom();
        } else if (gestureEvent instanceof TapAndHoldEvent) {
            TapAndHoldEvent tapAndHoldEvent = (TapAndHoldEvent) gestureEvent;
            // used for workaround to prevent tap event from being executed at the same time
            switch (tapAndHoldEvent.getId()) {
                case MTGestureEvent.GESTURE_ENDED:
                    if (tapAndHoldEvent.isHoldComplete()) {
                        tapAndHoldPerformed = true;
                    }
                    break;
            }

            pickPoint = tapAndHoldEvent.getLocationOnScreen();
        } else if (gestureEvent instanceof ScaleEvent) {
            pickPoint = ((ScaleEvent) gestureEvent).getScalingPoint();
        } else if (gestureEvent instanceof WheelEvent) {
            pickPoint = ((WheelEvent) gestureEvent).getLocationOnScreen();
        } else if (gestureEvent instanceof UnistrokeEvent) {
            pickPoint = ((UnistrokeEvent) gestureEvent).getCursor().getStartPosition();
            if (pickPoint == null) {
                stopProcessing = true;
            }
        } else {
            stopProcessing = true;
        }

        if (!stopProcessing) {
            PickResult pickResults = pick(pickPoint.getX(), pickPoint.getY(), false);

            for (final PickEntry pick : pickResults.getPickList()) {

                final MTComponent pickComponent = pick.hitObj;
                if (pickComponent != inputOverlay) {                  
                    gestureEvent.setTarget(pickComponent);
                    if (!(pickComponent == nameField && gestureEvent instanceof TapAndHoldEvent)) {
                        if (pickComponent instanceof IHandled) {
                            // see if the component wants to handle the event
                            IHandled<?> handledComponent = (IHandled<?>) pickComponent;

                            if (handledComponent.getHandler() != null
                                    && handledComponent.getHandler().processGestureEvent(gestureEvent)) {
                                return false;
                            }
                        } else if (pickComponent instanceof RamButton) {
                            if (pickComponent.processGestureEvent(gestureEvent)) {
                                return false;
                            }
                        }
                    }

                }
            }
        }
        return false;
    }

    public boolean parametersShowed() {
        return containsChild(parameterContainer);
    }

}
