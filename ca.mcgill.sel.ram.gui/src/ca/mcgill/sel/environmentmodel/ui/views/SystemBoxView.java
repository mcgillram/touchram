package ca.mcgill.sel.environmentmodel.ui.views;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.mt4j.components.TransformSpace;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.environmentmodel.EmPackage;
import ca.mcgill.sel.environmentmodel.EnvironmentModel;
import ca.mcgill.sel.environmentmodel.LayoutElement;
import ca.mcgill.sel.environmentmodel.NamedElement;
import ca.mcgill.sel.environmentmodel.TimeTriggeredEvent;
import ca.mcgill.sel.environmentmodel.ui.utils.EmHandlerFactory;
import ca.mcgill.sel.environmentmodel.ui.views.handler.ISystemBoxViewHandler;
import ca.mcgill.sel.classdiagram.ui.views.AssociationView;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent.Cardinal;
import ca.mcgill.sel.ram.ui.components.RamTextComponent.Alignment;
import ca.mcgill.sel.ram.ui.layouts.HorizontalLayoutAllCentered;
import ca.mcgill.sel.ram.ui.layouts.VerticalLayout;
import ca.mcgill.sel.ram.ui.utils.Fonts;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.RamEnd;
import ca.mcgill.sel.ram.ui.views.RamEnd.Position;
import ca.mcgill.sel.ram.ui.views.RelationshipView;
import ca.mcgill.sel.ram.ui.views.TextView;


/**
 * This view acts as a super class for all views that represent a sub-class of {@link Classifier}. It contains the basic
 * structure of a class and supports events and relationships. Sub-classes may override the build methods in order
 * to change the visual appearance.
 *
 * @author qiutan
 */
public class SystemBoxView extends LinkableView<ISystemBoxViewHandler> {

    /**
     * Bottom buffer for attribute and event container in order to prevent a static attribute or event to be not
     * visible.
     */
    protected static final float BUFFER_CONTAINER_BOTTOM = 4f;

    /**
     * The minimum width of the view.
     */
    protected static final float MINIMUM_WIDTH = 250f;
    /**
     * The minimum height of the view.
     */
    protected static final float MINIMUM_HIGHT = 50f;

    /**
     * The default icon size.
     */
    protected static final int ICON_SIZE = Fonts.DEFAULT_FONT.getFontAbsoluteHeight() + 2
            * RamTextComponent.DEFAULT_BUFFER_SIZE;

    /**
     * The buffer on the bottom (south) for each child).
     */
    protected static final int BUFFER_BOTTOM = 0;
    
    private static final float VERTICAL_SLOT_SPACE = 70;
    private static final float HORIZONTAL_SLOT_SPACE = 50;
    
    /**
     * The view container for system name and the spacer.
     */
    protected RamRectangleComponent systemNameAndSpacerContainer;

    /**
     * The view container for events.
     */
    protected RamRectangleComponent eventsContainer;   
   
    /**
     * The spacer.
     */
    protected RamRectangleComponent systemBoxViewSpacer;

    /**
     * The minimum number of vertical slots needed so that the text on the association
     * ends coming into this class that are navigable are nicely readable.
     */
    protected int previousVerticalSlotsNeeded;

    /**
     * The minimum number of horizontal slots needed so that the text on the association
     * ends coming into this class that are navigable are nicely readable.
     */
    protected int previousHorizontalSlotsNeeded;

    /**
     * The map of events to their views.
     */
    protected Map<TimeTriggeredEvent, TimeTriggeredEventView> events;

//    private EnvironmentModel em;


    /**
     * Creates a new {@link SystemBoxView} for a given communication diagram and layout element.
     *
     * @param structuralDiagramView the {@link CommunicationDiagramView} that is the parent of this view
     * @param layoutElement the {@link LayoutElement} that contains the layout information for this classifier
     */
    protected SystemBoxView(CommunicationDiagramView structuralDiagramView, LayoutElement layoutElement) {
        super(structuralDiagramView, structuralDiagramView.getEnvironmentModel(), layoutElement);
        
        setEnabled(true);
        setNoFill(false);
        setNoStroke(false);
        
        // build components
        build();
        
        initializeSystemBox(structuralDiagramView.getEnvironmentModel());

    }

    /**
     * Adds a new event view for the given event to this view at the given index. The index is an index inside
     * the {@link #eventsContainer}.
     *
     * @param index the index inside the events container where this event should be added to
     * @param event the event to add
     */
    protected void addTimeTriggeredEventField(int index, TimeTriggeredEvent event) {
        // Ensure that event field is added at the end of the events container and not at an
        // index out of bound , there might be more events in the class than in the
        // events container.
        
        int cleanedIndex = Math.min(index, eventsContainer.getChildCount());
        
        TimeTriggeredEventView eventView = new TimeTriggeredEventView(this, event);
        events.put(event, eventView);

        eventView.setHandler(EmHandlerFactory.INSTANCE.getTimeTriggeredEventViewHandler());
        
        eventsContainer.addChild(cleanedIndex, eventView);
        eventView.setWidthXYRelativeToParent(MINIMUM_WIDTH);
    }

    @Override
    public void addRelationshipEndAtPosition(RamEnd<? extends EObject, ? extends RamRectangleComponent> end) {
        super.addRelationshipEndAtPosition(end);
        updateSpacerSize();
    }    
    
    /**
     * Builds this view for the given classifier. Builds the name, events and events.
     *
     * @param classifier the classifier for this view
     */
    protected void build() {
        setMinimumWidth(MINIMUM_WIDTH);

        buildSystemNameAndSpacerContainer();
        
        buildTimeTriggeredEventsContainer();

    }

    /**
     * Builds the name field for the given classifier.
     *
     * @param classifier the classifier for this view
     */
    protected void buildSystemNameAndSpacerContainer() {       
        systemNameAndSpacerContainer = new RamRectangleComponent(new VerticalLayout());
        addChild(systemNameAndSpacerContainer);
        
        TextView systemNameField = new TextView(this.represented, EmPackage.Literals.ENVIRONMENT_MODEL__SYSTEM_NAME);
        systemNameField.setUniqueName(false);
        systemNameField.setAlignment(Alignment.CENTER_ALIGN);
        systemNameField.setPlaceholderText(Strings.PH_ENTER_CLASS_NAME);
        systemNameField.setHandler(EmHandlerFactory.INSTANCE.getTextViewHandler());
        systemNameAndSpacerContainer.addChild(systemNameField);
        
        systemBoxViewSpacer = new RamRectangleComponent();
        systemBoxViewSpacer.setNoFill(true);
        systemBoxViewSpacer.setLayout(new VerticalLayout());
        systemBoxViewSpacer.setAutoMinimizes(false);
        systemBoxViewSpacer.setMinimumHeight(MINIMUM_HIGHT);
        systemNameAndSpacerContainer.addChild(systemBoxViewSpacer);
    }

    /**
     * Builds the empty events container.
     */
    protected void buildTimeTriggeredEventsContainer() {

        events = new HashMap<TimeTriggeredEvent, TimeTriggeredEventView>();
        eventsContainer = new RamRectangleComponent(new VerticalLayout());
        eventsContainer.setNoStroke(false);
        eventsContainer.setMinimumHeight(ICON_SIZE);
        eventsContainer.setBufferSize(Cardinal.SOUTH, BUFFER_CONTAINER_BOTTOM);
        
        addChild(eventsContainer);
        RamTextComponent tagField = new RamTextComponent(Strings.TAG_TIME_TRIGGER);
        tagField.setAlignment(Alignment.CENTER_ALIGN);
        tagField.setBufferSize(Cardinal.SOUTH, 0);
        eventsContainer.addChild(tagField);       
    }

    @Override
    public void destroy() {        
        destroyRelationships();
        this.destroyAllChildren();
        super.destroy();
    }

//    /**
//     * Returns the classifier represented by this view.
//     *
//     * @return the {@link Classifier} represented by this view
//     */
//    @Override
//    public EnvironmentModel getEnvironmentModel() {
//        return represented;
//    }

    /**
     * Returns the component containing all {@link TimeTriggeredEventView}s.
     *
     * @return the {@link RamRectangleComponent} containing all {@link TimeTriggeredEventView}s
     */
    public RamRectangleComponent getTimeTriggeredEventsContainer() {
        return eventsContainer;
    }

    /**
     * Returns the {@link ClassDiagramView} which contains this view.
     *
     * @return the {@link ClassDiagramView} (parent of this view)
     */
    @Override
    public CommunicationDiagramView getCommunicationDiagramView() {
        return communicationDiagramView;
    }

    /**
     * Initializes the view for the given environment model.
     *
     * @param em the Environment Model represented by this view
     */
    protected void initializeSystemBox(EnvironmentModel em) {      
        for (TimeTriggeredEvent event : em.getTimeTriggeredEvents()) {
            addTimeTriggeredEventField(eventsContainer.getChildCount(), event);
        }
    }

    @Override
    public void notifyChanged(Notification notification) {
        super.notifyChanged(notification);
        EnvironmentModel em = (EnvironmentModel) represented;
        if (notification.getNotifier() == em) {
            if (notification.getFeature() == EmPackage.Literals.ENVIRONMENT_MODEL__TIME_TRIGGERED_EVENTS) {
                switch (notification.getEventType()) {
                    case Notification.REMOVE:
                        TimeTriggeredEvent event = (TimeTriggeredEvent) notification.getOldValue();
                        removeTimeTriggeredEvent(event);
                        break;
                    case Notification.ADD:
                        event = (TimeTriggeredEvent) notification.getNewValue();
                        addTimeTriggeredEventField(notification.getPosition() + 1, event);
                        break;
                    case Notification.MOVE:
                        event = (TimeTriggeredEvent) notification.getNewValue();
                        setTimeTriggeredEventPosition(event, notification.getPosition());
                        break;    
                }
            }
        }
    }
   

    /**
     * Removes the view of the given event from this view.
     *
     * @param event the event to remove
     */
    protected void removeTimeTriggeredEvent(final TimeTriggeredEvent event) {
        if (events.containsKey(event)) {
            RamApp.getApplication().invokeLater(new Runnable() {

                @Override
                public void run() {
                    TimeTriggeredEventView eventView = events.remove(event);

                    eventsContainer.removeChild(eventView);
                    eventView.destroy();
                }
            });
        }
    }
    
    public void setTimeTriggeredEventPosition(TimeTriggeredEvent event, int index) {
        RamApp.getApplication().invokeLater(new Runnable() {
            @Override
            public void run() {
                TimeTriggeredEventView eventView = events.get(event);

                eventsContainer.removeChild(eventView);
                eventsContainer.addChild(index, eventView);
            }
        });
    }

    @Override
    public void removeRelationshipEnd(RamEnd<? extends NamedElement, ? extends RamRectangleComponent> end) {
        super.removeRelationshipEnd(end);
        updateSpacerSize();
    }

    @Override
    public void scale(float x, float y, float z, Vector3D scalingPoint, TransformSpace transformSpace) {
        super.scale(x, y, z, scalingPoint, transformSpace);
    }


    /**
     * Calculates the new size of the spacer and sets the size in order to ensure that the association
     * end role names and multiplicities do not overlap.
     */
    void updateSpacerSize() {
        List<RamEnd<?, ?>> leftList = relationshipEndByPosition.get(Position.LEFT);
        List<RamEnd<?, ?>> rightList = relationshipEndByPosition.get(Position.RIGHT);   
        
        int leftSlots = 0;
        if (leftList.size() > 1) {
            leftSlots = calculateNumberOfSlots(leftList, Position.LEFT);
        }
        int rightSlots = 0;
        if (rightList.size() > 1) {
            rightSlots = calculateNumberOfSlots(rightList, Position.RIGHT);
        }
    
        int verticalSlotsNeeded = Math.max(leftSlots, rightSlots);
        boolean sizeChanged = false;
        
        if (verticalSlotsNeeded != previousVerticalSlotsNeeded 
                && systemBoxViewSpacer != null && eventsContainer != null) {
            previousVerticalSlotsNeeded = verticalSlotsNeeded;
            float baseHeight = nameContainer.getHeight() + eventsContainer.getHeight()
                + eventsContainer.getBufferSize(Cardinal.SOUTH)
                + eventsContainer.getBufferSize(Cardinal.NORTH);
            float desiredHeight = verticalSlotsNeeded * VERTICAL_SLOT_SPACE;
            float newHeight = 0;
            
            if (verticalSlotsNeeded > 0) {
                newHeight = desiredHeight - baseHeight;
            }

            systemBoxViewSpacer.setMinimumHeight(Math.max(newHeight, MINIMUM_HIGHT));
            sizeChanged = true;
        }
        
        List<RamEnd<?, ?>> topList = relationshipEndByPosition.get(Position.TOP);
        List<RamEnd<?, ?>> bottomList = relationshipEndByPosition.get(Position.BOTTOM);   
        
        int topSlots = 0;
        if (topList.size() > 1) {
            topSlots = calculateNumberOfSlots(topList, Position.TOP);
        }
        
        int bottomSlots = 0;
        if (bottomList.size() > 1) {
            bottomSlots = calculateNumberOfSlots(bottomList, Position.BOTTOM);
        }
    
        int horizontalSlotsNeeded = Math.max(topSlots, bottomSlots);
        
        if (horizontalSlotsNeeded != previousHorizontalSlotsNeeded && systemBoxViewSpacer != null) {
            previousHorizontalSlotsNeeded = horizontalSlotsNeeded;
            float newWidth = horizontalSlotsNeeded * HORIZONTAL_SLOT_SPACE;
            systemBoxViewSpacer.setMinimumWidth(Math.max(newWidth, MINIMUM_WIDTH));
            sizeChanged = true;
        }
        
        if (sizeChanged && systemBoxViewSpacer != null) {
            systemBoxViewSpacer.updateParent();
        }
    }
    
    @Override
    public void setSizeLocal(float width, float height) {
        super.setSizeLocal(width, height);
        updateRelationships();
    }

    @Override
    public void translate(Vector3D dirVect) {
        super.translate(dirVect);
        updateRelationships();
    }

//    /**
//     * Updates the style of this view depending on whether the classifier is a reference or not.
//     */
//    @Override
//    protected void updateStyle() {
//        Classifier classifier = (Classifier) represented;
//        if (COREModelUtil.isReference(classifier)) {
//            setFillColor(Colors.CLASS_VIEW_REFERENCE_FILL_COLOR);
//            setStrokeColor(Colors.CLASS_VIEW_REFERENCE_STROKE_COLOR);
//            visibilityField.setFont(Fonts.REFERENCE_FONT);
//        } else {
//            setFillColor(Colors.CLASS_VIEW_DEFAULT_FILL_COLOR);
//            setStrokeColor(Colors.CLASS_VIEW_DEFAULT_STROKE_COLOR);
//            visibilityField.setFont(Fonts.DEFAULT_FONT);
//        }
//        
//        setNameItalic(classifier.isAbstract());
//    }

}
