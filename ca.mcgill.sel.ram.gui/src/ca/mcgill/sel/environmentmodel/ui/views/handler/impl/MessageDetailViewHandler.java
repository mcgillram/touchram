package ca.mcgill.sel.environmentmodel.ui.views.handler.impl;

import org.mt4j.components.MTComponent;
import org.mt4j.input.gestureAction.DefaultScaleAction;
import org.mt4j.input.inputProcessors.componentProcessors.scaleProcessor.ScaleEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;
import org.mt4j.input.inputProcessors.componentProcessors.unistrokeProcessor.UnistrokeEvent;
import org.mt4j.input.inputProcessors.componentProcessors.unistrokeProcessor.UnistrokeUtils.UnistrokeGesture;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.environmentmodel.ui.views.MessageDetailView;
import ca.mcgill.sel.environmentmodel.ui.views.handler.IMessageDetailViewHandler;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.events.WheelEvent;
import ca.mcgill.sel.ram.ui.views.AbstractView;
import ca.mcgill.sel.ram.ui.views.handler.impl.AbstractViewHandler;

public class MessageDetailViewHandler extends AbstractViewHandler 
        implements IMessageDetailViewHandler {

    @Override
    public boolean processTapEvent(TapEvent tapEvent) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean processTapAndHoldEvent(TapAndHoldEvent tapAndHoldEvent) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void handleUnistrokeGesture(AbstractView<?> target, UnistrokeGesture gesture, Vector3D startPosition,
            UnistrokeEvent event) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    protected boolean processWheelEvent(WheelEvent wheelEvent) {
        if (wheelEvent.getTarget() instanceof AbstractView) {
            return super.processWheelEvent(wheelEvent);
        } else if (wheelEvent.getTarget() instanceof RamRectangleComponent) {
            // If the target has a parent of this type, then we want to handle the event.
            RamRectangleComponent target = (RamRectangleComponent) wheelEvent.getTarget();
            MessageDetailView detailView = target.getParentOfType(MessageDetailView.class);
            if (detailView != null) {
                AbstractViewHandler handler = (AbstractViewHandler) detailView.getHandler();
                if (this == handler) {
                    MTComponent containerLayer = detailView.getContainerLayer();

                    ScaleEvent se = wheelEvent.asScaleEvent(target);
                    DefaultScaleAction defaultScaleAction = new DefaultScaleAction(containerLayer);
                    defaultScaleAction.processGestureEvent(se);

                    return true;
                }
            }
                
        }
        return false;
    }
}
