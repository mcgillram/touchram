package ca.mcgill.sel.environmentmodel.ui.views.handler.impl;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.mt4j.components.TransformSpace;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.environmentmodel.Actor;
import ca.mcgill.sel.environmentmodel.EnvironmentModel;
import ca.mcgill.sel.environmentmodel.Message;
import ca.mcgill.sel.environmentmodel.Parameter;
import ca.mcgill.sel.environmentmodel.language.controller.EnvironmentModelController;
import ca.mcgill.sel.environmentmodel.language.controller.EnvironmentModelControllerFactory;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamSelectorComponent;
import ca.mcgill.sel.ram.ui.components.listeners.AbstractDefaultRamSelectorListener;
import ca.mcgill.sel.ram.ui.components.menu.RamLinkedMenu;
import ca.mcgill.sel.ram.ui.events.listeners.ActionListener;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.SelectorView;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.ILinkedMenuListener;
import ca.mcgill.sel.ram.ui.views.handler.impl.TextViewHandler;


/**
 * The handler for a typed element.
 * 
 * @author qiutan
 */
public class TypeNameHandler extends TextViewHandler implements ILinkedMenuListener {

    protected static final String ACTION_TYPE_SET = "view.type.set";
   
    @Override
    public void keyboardOpened(TextView textView) {
        // do nothing
    }

    @Override
    public boolean processTapEvent(TapEvent tapEvent) {
        if (tapEvent.isDoubleTap()) {
            TextView target = (TextView) tapEvent.getTarget();

            target.showKeyboard();
            return true;
        }

        return true;
    }

    @Override
    public boolean shouldDismissKeyboard(TextView textView) {
        EObject data = textView.getData();
        // Text editing is only available for attributes.
        EStructuralFeature feature = (EStructuralFeature) textView.getFeature();
        String text = textView.getText();

        // Replaces the text by the old name if the current text is empty
        if (text.isEmpty()) {
            text = EMFEditUtil.getFeatureText(data, feature);
            textView.setText(text);
        }

        // Checks if the name has changed
        String oldText = EMFEditUtil.getFeatureText(data, feature);
        boolean nameChanged = (oldText.equalsIgnoreCase(text)) ? false : true;
        
        if (nameChanged) {
            if (data instanceof Message) {
                ((Message) data).getMessageType().setName(text);
            } else if (data instanceof Actor) {
                ((Actor) data).getActorType().setName(text);
            } else if (data instanceof Parameter) {
                ((Parameter) data).getType().setName(text);
            }
        }
        
        return true;

//        if (textView.isUniqueName() && nameChanged) {
//            EObject container = feature.eContainer();
//            EStructuralFeature containingFeature = data.eContainingFeature();
//            
//            if (!RAMModelUtil.isUnique(container, containingFeature, text, data)) {
//                // TODO: show an error message?
//                return false;
//            }
//        }
//
//        // Convert the value to the type of the feature.
//        try {
//            Object newValue = EcoreUtil.createFromString(feature.getEAttributeType(), text);
//            setValue(data, feature, newValue);
//
//            return true;
//        } catch (IllegalArgumentException e) {
//            // The use did not give a correct format. For example, if the Type is Integer, and the user say "foo", A
//            // IllegalArgument Exception will be thrown.
//            // In that case, we won't dismiss the keyboard.
//
//            // TODO Show an error message to the user. Maybe put that in RamKeyboard>dimissKeyboard(boolean success)
//            // when dismiss boolean is false.
//            return false;
//        }
    }

    @Override
    public void initMenu(RamLinkedMenu menu, TapAndHoldEvent event) {
     
        menu.addAction(Strings.MENU_ATTRIBUTE_SETTER, Icons.ICON_MENU_SWITCH, ACTION_TYPE_SET, this, true);
                 
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        String actionCommand = event.getActionCommand();
        RamRectangleComponent pressedButton = (RamRectangleComponent) event.getTarget();
        RamLinkedMenu linkedMenu = (RamLinkedMenu) pressedButton.getParentOfType(RamLinkedMenu.class);
        if (linkedMenu != null) {
            TextView target = (TextView) linkedMenu.getLinkedView();
            
            if (ACTION_TYPE_SET.equals(actionCommand)) {
                RamSelectorComponent<Object> selector = new SelectorView(target.getData(), target.getFeature());
                
                RamApp.getActiveScene().addComponent(selector, linkedMenu.getPosition(TransformSpace.GLOBAL));
                // TODO: mschoettle: this could be done by the selector itself. but does it make sense?
                selector.registerListener(new AbstractDefaultRamSelectorListener<Object>() {
                    @Override
                    public void elementSelected(RamSelectorComponent<Object> selector, Object element) {
                        setValue(target.getData(), target.getFeature(), element);
                        target.updateText();
                    }
                });
                selector.addPlusButton(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent event) {
                        EnvironmentModelController controller = EnvironmentModelControllerFactory.INSTANCE
                                .getEnvironmentModelController();
                       
                        if (target.getData() instanceof Actor) {
                            Actor actor = (Actor) target.getData();
                            EnvironmentModel em = (EnvironmentModel) target.getData().eContainer();  
                            actor.setActorType(controller.createActorType(em, null));
                        } else if (target.getData() instanceof Parameter) {
                            Parameter parameter = (Parameter) target.getData();
                            EnvironmentModel em = (EnvironmentModel) (parameter.getType().eContainer());
                            parameter.setType(controller.createParameterType(em, em.getParametertype().size(), null));
                        } 
                        target.showKeyboard();
                        target.updateText();
                        selector.destroy();
                    }
                    
                });
            }
           
        }

    }

    @Override
    public EObject getEobject(RamRectangleComponent rectangle) {
        return ((TextView) rectangle).getData();
    }

    @Override
    public List<EObject> getEObjectToListenForUpdateMenu(RamRectangleComponent rectangle) {
        ArrayList<EObject> ret = new ArrayList<EObject>();
        ret.add(((TextView) rectangle).getData());
        return ret;
    }

    @Override
    public void updateMenu(RamLinkedMenu menu, Notification notification) {
    }

    @Override
    public RamRectangleComponent getVisualLinkedComponent(RamRectangleComponent link) {
        return link;
    }
}
