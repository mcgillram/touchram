package ca.mcgill.sel.environmentmodel.ui.views.handler.impl;

import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;

import ca.mcgill.sel.environmentmodel.Note;
import ca.mcgill.sel.environmentmodel.language.controller.EnvironmentModelController;
import ca.mcgill.sel.environmentmodel.language.controller.EnvironmentModelControllerFactory;
import ca.mcgill.sel.environmentmodel.ui.views.BaseView;
import ca.mcgill.sel.environmentmodel.ui.views.NoteView;
import ca.mcgill.sel.environmentmodel.ui.views.handler.INoteViewHandler;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.menu.RamLinkedMenu;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;

public class NoteViewHandler extends BaseViewHandler implements INoteViewHandler {

    private static final String ACTION_DELETE_ANNOTATIONS = "view.note.annotations.delete";
    
    @Override
    public void removeRepresented(BaseView<?> baseView) {
        EnvironmentModelControllerFactory.INSTANCE.getEnvironmentModelController().removeNote(
                (Note) baseView.getRepresented());                
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        String actionCommand = event.getActionCommand();
        RamRectangleComponent pressedButton = (RamRectangleComponent) event.getTarget();
        RamLinkedMenu linkedMenu = (RamLinkedMenu) pressedButton.getParentOfType(RamLinkedMenu.class);
        if (linkedMenu != null) {

            NoteView noteView = (NoteView) linkedMenu.getLinkedView();

            if (ACTION_DELETE_ANNOTATIONS.equals(actionCommand)) {
                EnvironmentModelController controller = 
                        EnvironmentModelControllerFactory.INSTANCE.getEnvironmentModelController();
                Note note = noteView.getNote();
                
                controller.removeAnnotations(note);
            }
        }
        super.actionPerformed(event);
    }
    
    @Override
    public void initMenu(RamLinkedMenu menu, TapAndHoldEvent event) {
        super.initMenu(menu, event);
        menu.addAction(Strings.MENU_DELETE_ANNOTATIONS, Icons.ICON_MENU_CLEAR_SELECTION,
                ACTION_DELETE_ANNOTATIONS, this, SUBMENU_ADD, true);
    }
}

