package ca.mcgill.sel.environmentmodel.ui.views;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.edit.provider.INotifyChangedListener;
import org.mt4j.util.MTColor;
import org.mt4j.util.font.IFont;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.core.util.COREModelUtil;
import ca.mcgill.sel.environmentmodel.EmPackage;
import ca.mcgill.sel.environmentmodel.EnvironmentModel;
import ca.mcgill.sel.environmentmodel.LayoutElement;
import ca.mcgill.sel.environmentmodel.TimeTriggeredEvent;
import ca.mcgill.sel.environmentmodel.ui.utils.EmHandlerFactory;
import ca.mcgill.sel.environmentmodel.ui.views.handler.ITimeTriggeredEventViewHandler;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent.Cardinal;
import ca.mcgill.sel.ram.ui.components.RamTextComponent.Alignment;
import ca.mcgill.sel.ram.ui.layouts.HorizontalLayoutAllCentered;
import ca.mcgill.sel.ram.ui.layouts.HorizontalLayoutVerticallyCentered;
import ca.mcgill.sel.ram.ui.layouts.VerticalLayout;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.Fonts;
import ca.mcgill.sel.ram.ui.utils.GraphicalUpdater;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.IHandled;


/**
 * This view draws the representation of a CDEnumLiteral onto a {@link SystemBoxView}. It only contains a name field.
 * 
 * @author Franz
 * @author yhattab
 * 
 */
//public class TimeTriggeredEventView extends RamRectangleComponent implements INotifyChangedListener,
//    IHandled<ITimeTriggeredEventViewHandler> {
public class TimeTriggeredEventView extends RamRectangleComponent implements IHandled<ITimeTriggeredEventViewHandler> {

    /**
     * The buffer on the bottom (south) for each child).
     */
    public static final int BUFFER_BOTTOM = 0;
    
    private TimeTriggeredEvent represented;
    private ITimeTriggeredEventViewHandler handler;
    private GraphicalUpdater graphicalUpdater;


    private TextView nameField;    
   
    protected TimeTriggeredEventView(SystemBoxView systemBoxView, TimeTriggeredEvent represented) {
        this.represented = represented;
        
        nameField = new TextView(represented, EmPackage.Literals.NAMED_ELEMENT__NAME);
        nameField.setBufferSize(Cardinal.SOUTH, 0);

        addChild(nameField);

        setLayout(new HorizontalLayoutAllCentered());

        EnvironmentModel em = EMFModelUtil.getRootContainerOfType(represented, EmPackage.Literals.ENVIRONMENT_MODEL);
        graphicalUpdater = RamApp.getApplication().getGraphicalUpdaterForModel(em);
        graphicalUpdater.addGUListener(represented, this);

        
        nameField.setHandler(EmHandlerFactory.INSTANCE.getTextViewHandler());
    }
    
    /**
     * Getter for {@link REnumLiteral} associated to this view.
     * 
     * @return {@link REnumLiteral} associated to this view.
     */
    public TimeTriggeredEvent getEvent() {
        return represented;
    }

    /**
     * Returns the name field view.
     * 
     * @return the name field view
     */
    public TextView getNameField() {
        return nameField;
    }

    @Override
    public ITimeTriggeredEventViewHandler getHandler() {
        return handler;
    }

    @Override
    public void setHandler(ITimeTriggeredEventViewHandler handler) {
        this.handler = handler;
    
    }

}
