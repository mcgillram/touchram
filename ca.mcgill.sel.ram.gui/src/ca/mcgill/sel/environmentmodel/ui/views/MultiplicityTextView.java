package ca.mcgill.sel.environmentmodel.ui.views;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import ca.mcgill.sel.environmentmodel.Actor;
import ca.mcgill.sel.environmentmodel.EmPackage;
import ca.mcgill.sel.environmentmodel.ui.utils.EnvironmentModelUtils;
import ca.mcgill.sel.ram.ui.views.TextView;


public class MultiplicityTextView extends TextView {

    public MultiplicityTextView(EObject data) {
        super(data, null, false);
    }
    
    public MultiplicityTextView(EObject data, EStructuralFeature feature) {
        super(data, feature, false);
    }
    
    @Override
    protected String getModelText() {
        EObject data = getData();
        EStructuralFeature feature = getFeature();
        if (data instanceof Actor) {
            if (feature == null) {
                return EnvironmentModelUtils.getActorMultiplicity((Actor) data);
            } else if (feature == EmPackage.Literals.ACTOR__MESSAGES) {
                return EnvironmentModelUtils.getActorCommunicationMultiplicity((Actor) data);
            } else {
                return super.getModelText();
            }
        } else {
            return super.getModelText();
        }
    }

    @Override
    public void notifyChanged(Notification notification) {
        if (notification.getNotifier() == getData()) {
            updateText();
        }
    }

    @Override
    public void showKeyboard() {
        super.showKeyboard(this);
        getKeyboard().setSymbolsState(true);
    }

}
