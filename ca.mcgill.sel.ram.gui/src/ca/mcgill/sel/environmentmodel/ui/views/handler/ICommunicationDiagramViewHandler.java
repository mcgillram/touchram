package ca.mcgill.sel.environmentmodel.ui.views.handler;

import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.environmentmodel.ui.views.BaseView;
import ca.mcgill.sel.environmentmodel.ui.views.CommunicationDiagramView;
import ca.mcgill.sel.ram.ui.events.listeners.ITapAndHoldListener;
import ca.mcgill.sel.ram.ui.events.listeners.ITapListener;
import ca.mcgill.sel.ram.ui.views.handler.IAbstractViewHandler;

public interface ICommunicationDiagramViewHandler extends IAbstractViewHandler, ITapListener, ITapAndHoldListener {

    boolean handleTapAndHold(CommunicationDiagramView CommunicationDiagramView, BaseView<?> target);

    boolean handleDoubleTap(CommunicationDiagramView structuralDiagramView, BaseView<?> target);
    
    void dragAllSelected(CommunicationDiagramView svd, Vector3D directionVector);

}
