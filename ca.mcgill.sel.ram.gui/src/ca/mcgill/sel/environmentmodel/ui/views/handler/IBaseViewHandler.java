package ca.mcgill.sel.environmentmodel.ui.views.handler;

import ca.mcgill.sel.environmentmodel.ui.views.BaseView;
import ca.mcgill.sel.ram.ui.events.listeners.IDragListener;
import ca.mcgill.sel.ram.ui.events.listeners.ITapAndHoldListener;
import ca.mcgill.sel.ram.ui.events.listeners.ITapListener;

/**
 * The handler of the {@link BaseView}.
 * 
 * @author Franz
 * 
 */
public interface IBaseViewHandler extends IDragListener, ITapAndHoldListener, ITapListener {

    /**
     * Handles the removal of what the view represents.
     * 
     * @param baseView the BaseView representing what's to be removed
     */
    void removeRepresented(BaseView<?> baseView);

}
