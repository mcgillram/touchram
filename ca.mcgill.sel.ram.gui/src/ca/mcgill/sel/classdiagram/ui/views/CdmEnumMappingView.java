package ca.mcgill.sel.classdiagram.ui.views;

import ca.mcgill.sel.classdiagram.CDEnumMapping;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.ram.ui.components.RamButton;
import ca.mcgill.sel.ram.ui.components.RamImageComponent;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent.Alignment;
import ca.mcgill.sel.ram.ui.events.listeners.ActionListener;
import ca.mcgill.sel.ram.ui.layouts.HorizontalLayoutVerticallyCentered;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.Fonts;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.HandlerFactory;

/**
 * This view shows the enum mapping such as EnumA-->EnumB in a mapping
 * along with the buttons : Enum Literal Mapping adding button,
 * mapping delete button.
 * 
 * @author joerg
 */
public class CdmEnumMappingView extends RamRectangleComponent implements ActionListener {

    private static final String ACTION_ENUM_LITERAL_MAPPING_ADD = "view.enumLiteralMapping.add";
    private static final String ACTION_ENUM_MAPPING_DELETE = "view.enumMapping.delete";

    private static final float ICON_SIZE = Fonts.FONTSIZE_COMPOSITION + 2;

    private CDEnumMapping myCDEnumMapping;

    /**
     * Button to add Enum Literal Mapping.
     */
    private RamButton buttonEnumLiteralMappingAdd;

    /**
     * Button to delete Enum Mapping.
     */
    private RamButton buttonEnumMappingDelete;

    /**
     * EnumMapping from element.
     */
    private TextView textEnumFromElement;

    /**
     * EnumMapping to element.
     */
    private TextView textEnumToElement;

    /**
     * Image for an arrow between mapping elements.
     */
    private RamImageComponent arrow;

    /**
     * Creates a new enum mapping view for the given enum mapping.
     *
     * @param containerView - the container this view is contained in
     * @param enumMapping - the {@link EnumMapping} to create a view for
     */
    public CdmEnumMappingView(CdmEnumMappingContainerView containerView, CDEnumMapping enumMapping) {
        setNoStroke(true);
        setNoFill(false);
        setFillColor(Colors.CLASS_MAPPING_VIEW_FILL_COLOR);
        setBuffers(0);
        myCDEnumMapping = enumMapping;

        // Add delete button

        RamImageComponent deleteEnumMappingImage = new RamImageComponent(Icons.ICON_DELETE,
                Colors.ICON_DELETE_COLOR);
        deleteEnumMappingImage.setMinimumSize(ICON_SIZE, ICON_SIZE);
        deleteEnumMappingImage.setMaximumSize(ICON_SIZE, ICON_SIZE);
        buttonEnumMappingDelete = new RamButton(deleteEnumMappingImage);
        buttonEnumMappingDelete.setActionCommand(ACTION_ENUM_MAPPING_DELETE);
        buttonEnumMappingDelete.addActionListener(this);
        addChild(buttonEnumMappingDelete);

        textEnumFromElement = new TextView(myCDEnumMapping, CorePackage.Literals.CORE_LINK__FROM);
        textEnumFromElement.setHandler(HandlerFactory.INSTANCE.getTextViewHandler());
        textEnumFromElement.setFont(Fonts.FONT_COMPOSITION);
        textEnumFromElement.setBufferSize(Cardinal.SOUTH, 0);
        textEnumFromElement.setBufferSize(Cardinal.EAST, 0);
        textEnumFromElement.setBufferSize(Cardinal.WEST, 0);
        textEnumFromElement.setAlignment(Alignment.CENTER_ALIGN);
        textEnumFromElement.setPlaceholderText(Strings.PH_SELECT_ENUM);
        textEnumFromElement.setAutoMinimizes(true);
        this.addChild(textEnumFromElement);

        arrow = new RamImageComponent(Icons.ICON_ARROW_RIGHT, Colors.ICON_ARROW_COLOR);
        arrow.setMinimumSize(ICON_SIZE, ICON_SIZE);
        arrow.setMaximumSize(ICON_SIZE, ICON_SIZE);
        this.addChild(arrow);

        textEnumToElement = new TextView(myCDEnumMapping, CorePackage.Literals.CORE_LINK__TO);
        textEnumToElement.setFont(Fonts.FONT_COMPOSITION);
        textEnumToElement.setBufferSize(Cardinal.SOUTH, 0);
        textEnumToElement.setBufferSize(Cardinal.EAST, 0);
        textEnumToElement.setBufferSize(Cardinal.WEST, 0);
        textEnumToElement.setAlignment(Alignment.CENTER_ALIGN);
        textEnumToElement.setPlaceholderText(Strings.PH_SELECT_ENUM);
        textEnumToElement.setAutoMinimizes(true);
        textEnumToElement.setHandler(HandlerFactory.INSTANCE.getTextViewHandler());
        this.addChild(textEnumToElement);

        // Add buttons for adding enum literal mapping,
        // and a button for enum mapping deleting
        RamImageComponent enumLiteralMappingAddImage = new RamImageComponent(Icons.ICON_ENUM_LITERAL_MAPPING_ADD,
                Colors.ICON_STRUCT_DEFAULT_COLOR);
        enumLiteralMappingAddImage.setMinimumSize(ICON_SIZE, ICON_SIZE);
        enumLiteralMappingAddImage.setMaximumSize(ICON_SIZE, ICON_SIZE);
        buttonEnumLiteralMappingAdd = new RamButton(enumLiteralMappingAddImage);
        buttonEnumLiteralMappingAdd.setActionCommand(ACTION_ENUM_LITERAL_MAPPING_ADD);
        buttonEnumLiteralMappingAdd.addActionListener(this);
        addChild(buttonEnumLiteralMappingAdd);

        setLayout(new HorizontalLayoutVerticallyCentered(Fonts.FONTSIZE_COMPOSITION / 5));

    }

    @Override
    public void actionPerformed(ActionEvent event) {
        String actionCommand = event.getActionCommand();

        if (ACTION_ENUM_LITERAL_MAPPING_ADD.equals(actionCommand)) {
            HandlerFactory.INSTANCE.getCdmMappingContainerViewHandler().addEnumLiteralMapping(myCDEnumMapping);
        } else if (ACTION_ENUM_MAPPING_DELETE.equals(actionCommand)) {
            HandlerFactory.INSTANCE.getCdmMappingContainerViewHandler().deleteEnumMapping(myCDEnumMapping);
        }
    }

}
