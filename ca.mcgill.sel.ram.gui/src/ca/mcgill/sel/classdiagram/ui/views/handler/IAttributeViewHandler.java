package ca.mcgill.sel.classdiagram.ui.views.handler;

import org.mt4j.input.inputProcessors.IGestureEventListener;

import ca.mcgill.sel.classdiagram.ui.views.AttributeView;

/**
 * This interface is implemented by something that can handle events for an {@link AttributeView}.
 * 
 * @author mschoettle
 * @author yhattab
 */
public interface IAttributeViewHandler extends IGestureEventListener {

    /**
     * Handles the removal of an attribute.
     * 
     * @param attributeView - the affected {@link AttributeView}
     */
    void removeAttribute(AttributeView attributeView);

    /**
     * Handles the set of attribute to static.
     * 
     * @param attributeView - the affected {@link AttributeView}
     */
    void setAttributeStatic(AttributeView attributeView);

    /**
     * Handles the generation of getter.
     * 
     * @param attributeView - the affected {@link AttributeView}
     */
    void generateGetter(AttributeView attributeView);

    /**
     * Handles the generation of setter.
     * 
     * @param attributeView - the affected {@link AttributeView}
     */
    void generateSetter(AttributeView attributeView);
}
