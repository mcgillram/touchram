package ca.mcgill.sel.classdiagram.ui.views;

import ca.mcgill.sel.classdiagram.CDClassifierMapping;
import ca.mcgill.sel.classdiagram.CDEnumMapping;
import ca.mcgill.sel.classdiagram.ui.views.handler.ICdmCompositionViewHandler;
import ca.mcgill.sel.core.COREMapping;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.views.CompositionContainerView;
import ca.mcgill.sel.ram.ui.views.CompositionTitleView;
import ca.mcgill.sel.ram.ui.views.CompositionView;
import ca.mcgill.sel.ram.ui.views.handler.HandlerFactory;

/**
 * This view subclasses CompositionView and provides implementations for all the Cdm-specific mappings.  
 * 
 * @author joerg
 */
public class CdmCompositionView extends CompositionView<ICdmCompositionViewHandler> {
    
    /**
     * Creates a new composition view.
     * 
     * @param modelComposition the model composition to create a view for
     * @param reuse the reuse the composition is for
     * @param containerView  the composition container view that contains this composition view. 
     */
    public CdmCompositionView(COREModelComposition modelComposition, COREReuse reuse, 
                                CompositionContainerView containerView) {
        super(modelComposition, reuse, containerView);
        setHandler(HandlerFactory.INSTANCE.getCdmCompositionViewHandler());
    }
    

    @Override
    public CompositionTitleView getCompositionTitleView(CompositionContainerView container,
            CompositionView<ICdmCompositionViewHandler> compositionView, COREReuse reuse, boolean detailedViewOn) {
        return new CdmCompositionTitleView(container, compositionView, reuse, detailedViewOn);
    }

    @Override
    public RamRectangleComponent getMappingContainerView(COREMapping<?> newMapping) {
        RamRectangleComponent mappingContainerView;
        if (newMapping instanceof CDClassifierMapping) {
            mappingContainerView = new CdmClassifierMappingContainerView((CDClassifierMapping) newMapping);
        } else {
            mappingContainerView = new CdmEnumMappingContainerView((CDEnumMapping) newMapping);
        }
        return mappingContainerView;
    }
    
}
