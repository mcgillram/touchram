package ca.mcgill.sel.classdiagram.ui.views;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.mt4j.components.MTComponent;

import ca.mcgill.sel.classdiagram.CDAttributeMapping;
import ca.mcgill.sel.classdiagram.CDClassifierMapping;
import ca.mcgill.sel.classdiagram.CDOperationMapping;
import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.COREMapping;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.ram.AttributeMapping;
import ca.mcgill.sel.ram.OperationMapping;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamRoundedRectangleComponent;
import ca.mcgill.sel.ram.ui.components.listeners.UINotifyChangedListener;
import ca.mcgill.sel.ram.ui.layouts.VerticalLayout;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.views.structural.AttributeMappingView;
import ca.mcgill.sel.ram.ui.views.structural.OperationMappingContainerView;

/**
 * This view visualizes a CDClassifierMapping and contains all mapping related container views
 * for a CD classifier mapping, i.e., attribute mappings and operation mappings.
 * 
 * @author joerg
 */
public class CdmClassifierMappingContainerView extends RamRoundedRectangleComponent implements UINotifyChangedListener {
    // It is useful to keep the child as a reference for hiding/showing purposes
    private CdmClassifierMappingView myCDClassifierMappingView;

    // Classifier Mapping information
    private CDClassifierMapping myCDClassifierMapping;

    // All the attribute mapping related views will be in this view:
    private RamRectangleComponent hideableAttributeContainer;

    // All the operation mapping related views will be in this view:
    private RamRectangleComponent hideableOperationContainer;    
    
    /**
     * Creates a new mapping container view.
     * 
     * @param cdClassifierMapping {@link CDClassifierMapping} which we want to create a
     *            {@link CdmClassifierMappingContainerView} for.
     */
    public CdmClassifierMappingContainerView(CDClassifierMapping cdClassifierMapping) {
        super(0);
        setNoStroke(false);
        setStrokeWeight(3);
        setNoFill(false);
        setFillColor(Colors.MAPPING_CONTAINER_VIEW_FILL_COLOR);

        myCDClassifierMappingView = new CdmClassifierMappingView(cdClassifierMapping);
        this.addChild(myCDClassifierMappingView);

        myCDClassifierMapping = cdClassifierMapping;

        // Container for the Attributes
        hideableAttributeContainer = new RamRectangleComponent();
        hideableAttributeContainer.setLayout(new VerticalLayout(0));
        hideableAttributeContainer.setFillColor(Colors.ATTRIBUTE_MAPPING_VIEW_FILL_COLOR);
        hideableAttributeContainer.setNoFill(false);
        hideableAttributeContainer.setBufferSize(Cardinal.EAST, 0);

        // Container for the Operation
        hideableOperationContainer = new RamRectangleComponent();
        hideableOperationContainer.setLayout(new VerticalLayout(0));
        hideableOperationContainer.setFillColor(Colors.OPERATION_MAPPING_VIEW_FILL_COLOR);
        hideableOperationContainer.setNoFill(false);
        hideableOperationContainer.setBufferSize(Cardinal.EAST, 0);
        
        this.addChild(hideableAttributeContainer);
        this.addChild(hideableOperationContainer);

        setLayout(new VerticalLayout());
        setBuffers(0);
        
        // Add all the operation and attribute mappings related to this classifier mapping.
        addAllOperationMappings();
        addAllAttributeMappings();

        EMFEditUtil.addListenerFor(myCDClassifierMapping, this);
    }

    /**
     * Adds all the attribute mappings which belongs to the classifier mapping.
     */
    private void addAllAttributeMappings() {
        EList<COREMapping<?>> coreMappings = myCDClassifierMapping.getMappings();
        for (COREMapping<?> mapping : coreMappings) {
            if (mapping instanceof CDAttributeMapping) {
                addAttributeMappingView((CDAttributeMapping) mapping);
            }
        }
    }

    /**
     * Adds all the operation mappings which belongs to the classifier mapping.
     * The operation are contained in a {@link OperationMappingContainerView}
     */
    private void addAllOperationMappings() {
        EList<COREMapping<?>> coreMappings = myCDClassifierMapping.getMappings();
        for (COREMapping<?> mapping : coreMappings) {
            if (mapping instanceof CDOperationMapping) {
                addOperationMappingContainerView((CDOperationMapping) mapping);
            }
        }
    }
    
    /**
     * Adds a {@link CdmAttributeMappingView} to the mapping container view (inside of hideableAttributeContainer).
     * 
     * @param newAttributeMapping the {@link AttributeMapping} to add a view for
     */
    private void addAttributeMappingView(CDAttributeMapping newAttributeMapping) {
        CdmAttributeMappingView attributeMappingView = new CdmAttributeMappingView(newAttributeMapping);
        hideableAttributeContainer.addChild(attributeMappingView);
    }

    /**
     * Adds an {@link OperationMappingContainerView} to the mapping container view.
     * 
     * @param newOperationMapping the {@link OperationMapping} to add a container for
     */
    private void addOperationMappingContainerView(CDOperationMapping newOperationMapping) {
        CdmOperationMappingContainerView operationMappingContainerView = 
                new CdmOperationMappingContainerView(newOperationMapping);
        hideableOperationContainer.addChild(operationMappingContainerView);
    }

    /**
     * Deletes an {@link AttributeMappingView} from the mapping container view.
     * 
     * @param deletedAttributeMapping the {@link CDAttributeMapping} to remove the view for
     */
    private void deleteAttributeMappingView(CDAttributeMapping deletedAttributeMapping) {
        MTComponent[] attributeMappingViews = hideableAttributeContainer.getChildren();
        for (MTComponent view : attributeMappingViews) {
            if (view instanceof CdmAttributeMappingView) {
                CdmAttributeMappingView attributeMappingView = (CdmAttributeMappingView) view;
                if (attributeMappingView.getAttributeMapping() == deletedAttributeMapping) {
                    hideableAttributeContainer.removeChild(attributeMappingView);
                }
            }
        }
    }

    /**
     * Deletes an {@link OperationMappingContainerView} from the mapping container view.
     * 
     * @param deletedOperationMapping the {@link OperationMapping} to remove the container for
     */
    private void deleteOperationMappingContainerView(CDOperationMapping deletedOperationMapping) {
        MTComponent[] operationMappingContainerViews = hideableOperationContainer.getChildren();
        for (MTComponent view : operationMappingContainerViews) {
            if (view instanceof CdmOperationMappingContainerView) {
                CdmOperationMappingContainerView operationMappingContainerView =
                        (CdmOperationMappingContainerView) view;
                if (operationMappingContainerView.getOperationMapping() == deletedOperationMapping) {
                    hideableOperationContainer.removeChild(operationMappingContainerView);
                }
            }
        }
    }

    @Override
    public void destroy() {
        EMFEditUtil.removeListenerFor(myCDClassifierMapping, this);
        super.destroy();
    }

    /**
     * Getter for ClassifierMapping information of the view.
     * 
     * @return {@link CDClassifierMapping}
     */
    public CDClassifierMapping getClassifierMapping() {
        return myCDClassifierMapping;
    }

    @Override
    public void handleNotification(Notification notification) {
        if (notification.getNotifier() == myCDClassifierMapping) {
            if (notification.getFeature() == CorePackage.Literals.CORE_MAPPING__MAPPINGS) {
                switch (notification.getEventType()) {
                    case Notification.ADD:
                        COREMapping<?> newMapping = (COREMapping<?>) notification.getNewValue();
                        
                        if (newMapping instanceof CDAttributeMapping) {
                            CDAttributeMapping newAttributeMapping = (CDAttributeMapping) newMapping;
                            addAttributeMappingView(newAttributeMapping);
                        } else if (newMapping instanceof CDOperationMapping) {
                            CDOperationMapping newOperationMapping = (CDOperationMapping) newMapping;
                            addOperationMappingContainerView(newOperationMapping);                          
                        }
                        break;

                    case Notification.REMOVE:
                        COREMapping<?> oldMapping = (COREMapping<?>) notification.getOldValue();

                        if (oldMapping instanceof CDAttributeMapping) {
                            CDAttributeMapping oldAttributeMapping = (CDAttributeMapping) oldMapping;
                            deleteAttributeMappingView(oldAttributeMapping);
                        } else if (oldMapping instanceof CDOperationMapping) {
                            CDOperationMapping oldOperationMapping = (CDOperationMapping) oldMapping;
                            deleteOperationMappingContainerView(oldOperationMapping);
                        }
                        break;
                }
            }
        }
    }
}
