package ca.mcgill.sel.classdiagram.ui.views;

import ca.mcgill.sel.classdiagram.AssociationEnd;
import ca.mcgill.sel.classdiagram.CdmPackage;
import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.ram.ui.utils.Fonts;
import ca.mcgill.sel.ram.ui.views.TextView;

/**
 * View to show and Association End Ordering and uniqueness properties.
 * @author yhattab
 *
 */
public class AssociationPropertyView extends TextView {

    private AssociationEnd associationEnd;
    
    /** 
     * Builds view to show properties of association like ordered and unique.
     * @param associationEnd Association End to which the qualifier belongs
     */
    public AssociationPropertyView(AssociationEnd associationEnd) {
        super(associationEnd, CdmPackage.Literals.ASSOCIATION_END__ORDERED);
        
        this.associationEnd = associationEnd;

        setNoStroke(true);
        setNoFill(true);
        setPlaceholderText("{}");
        setPlaceholderFont(Fonts.BACKGROUND_COLOR_PLACEHOLDER_FONT);
        setBufferSize(Cardinal.SOUTH, 0);
        
        updateText();
        
        EMFEditUtil.addListenerFor(associationEnd, this);
    }

    @Override
    protected void updateTextView(Object oldValue, Object newValue) {
        super.updateTextView(oldValue, newValue);
        if (this.getParent() instanceof AssociationView) {
            ((AssociationView) this.getParent()).updateLines();
        }
    }
    
    /**
     * Returns the association end this view is attached to.
     * @return {@link AssociationEnd} object whose properties this view represents
     */
    public AssociationEnd getAssociationEnd() {
        return associationEnd;
    }

    @Override
    protected String getModelText() {
        String modelText = "";
        if (associationEnd != null) {
            //only return text if it's not default values (unordered and unique)
            if (associationEnd.isOrdered()) {
                if (associationEnd.isUnique()) {
                    modelText = "{ordered}";
                } else {
                    modelText = "{ordered, not unique}";
                }
            } else {
                if (!associationEnd.isUnique()) {
                    modelText = "{not unique}";
                }
            }
        }

        return  modelText;
    }
}
