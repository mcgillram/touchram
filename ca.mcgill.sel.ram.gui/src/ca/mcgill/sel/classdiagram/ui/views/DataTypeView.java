package ca.mcgill.sel.classdiagram.ui.views;


import ca.mcgill.sel.classdiagram.Class;
import ca.mcgill.sel.classdiagram.Classifier;
import ca.mcgill.sel.classdiagram.LayoutElement;

import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent.Alignment;
import ca.mcgill.sel.ram.ui.utils.Fonts;
import ca.mcgill.sel.ram.ui.utils.Strings;


/**
 * This view draws the representation of a data type onto a {@link ClassDiagramView}. 
 * It contains a name field, and two lists for methods and attributes. 
 * ClassViews are inspectable and create a {@link ClassInspectorView} when click on.
 *
 * @author Alec Harmon
 * @author yhattab
 */
public class DataTypeView extends ClassView {

    /**
     * Creates a new view representing the given class.
     *
     * @param cdv the {@link ClassDiagramView} that owns this view
     * @param clazz The RAM class to display.
     * @param layoutElement The position at which to display it.
     */
    public DataTypeView(ClassDiagramView cdv, Class clazz, LayoutElement layoutElement) {
        super(cdv, clazz, layoutElement);

        addDataTypeTag();
    }
    
    /**
     * Adds the DataType tag.
     */
    protected void addDataTypeTag() {
        // Create the Text field
        RamTextComponent dataTypeTagField = new RamTextComponent();
        dataTypeTagField.setFont(Fonts.FONT_ENUM_TAG);
        dataTypeTagField.setAlignment(Alignment.CENTER_ALIGN);
        dataTypeTagField.setText(Strings.TAG_DATATYPE);
        dataTypeTagField.setNoStroke(true);
        dataTypeTagField.setBufferSize(Cardinal.SOUTH, 0);
        setTagField(dataTypeTagField);
    }
    
    @Override
    protected void buildNameField(Classifier classifier) {
        super.buildNameField(classifier);
        
        nameField.setPlaceholderText(Strings.PH_ENTER_DATATYPE_NAME);
    }
   
}
