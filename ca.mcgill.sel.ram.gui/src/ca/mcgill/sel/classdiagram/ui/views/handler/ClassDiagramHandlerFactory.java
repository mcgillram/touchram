package ca.mcgill.sel.classdiagram.ui.views.handler;

import ca.mcgill.sel.classdiagram.ui.scenes.handler.impl.DisplayClassDiagramSceneHandler;
import ca.mcgill.sel.classdiagram.ui.views.handler.impl.AssociationMultiplicityHandler;
import ca.mcgill.sel.classdiagram.ui.views.handler.impl.AssociationPropertyHandler;
import ca.mcgill.sel.classdiagram.ui.views.handler.impl.AssociationRoleNameHandler;
import ca.mcgill.sel.classdiagram.ui.views.handler.impl.AssociationViewHandler;
import ca.mcgill.sel.classdiagram.ui.views.handler.impl.AttributeViewHandler;
import ca.mcgill.sel.classdiagram.ui.views.handler.impl.ClassDiagramViewHandler;
import ca.mcgill.sel.classdiagram.ui.views.handler.impl.ClassViewHandler;
import ca.mcgill.sel.classdiagram.ui.views.handler.impl.ClassifierVisibilityViewHandler;
import ca.mcgill.sel.classdiagram.ui.views.handler.impl.EnumLiteralViewHandler;
import ca.mcgill.sel.classdiagram.ui.views.handler.impl.EnumNameHandler;
import ca.mcgill.sel.classdiagram.ui.views.handler.impl.EnumViewHandler;
import ca.mcgill.sel.classdiagram.ui.views.handler.impl.ImplementationClassViewHandler;
import ca.mcgill.sel.classdiagram.ui.views.handler.impl.InheritanceViewHandler;
import ca.mcgill.sel.classdiagram.ui.views.handler.impl.NaryAssociationNodeViewHandler;
import ca.mcgill.sel.classdiagram.ui.views.handler.impl.NoteViewHandler;
import ca.mcgill.sel.classdiagram.ui.views.handler.impl.OperationNameHandler;
import ca.mcgill.sel.classdiagram.ui.views.handler.impl.OperationViewHandler;
import ca.mcgill.sel.classdiagram.ui.views.handler.impl.OperationVisibilityViewHandler;
import ca.mcgill.sel.classdiagram.ui.views.handler.impl.ParameterViewHandler;
import ca.mcgill.sel.ram.ui.views.handler.IDisplaySceneHandler;
import ca.mcgill.sel.ram.ui.views.handler.ITextViewHandler;
import ca.mcgill.sel.ram.ui.views.handler.impl.TextViewHandler;
import ca.mcgill.sel.ram.ui.views.handler.impl.ValidatingTextViewHandler;
import ca.mcgill.sel.ram.ui.views.structural.handler.IAttributeNameHandler;
import ca.mcgill.sel.ram.ui.views.structural.handler.impl.AttributeNameHandler;
import ca.mcgill.sel.ram.ui.views.structural.handler.impl.ClassNameHandler;
import ca.mcgill.sel.ram.ui.views.structural.handler.impl.EnumLiteralNameHandler;
import ca.mcgill.sel.ram.util.MetamodelRegex;

/**
 * A Class Diagrams specific factory to obtain the handlers.
 * @author yhattab
 *
 */
public final class ClassDiagramHandlerFactory {
    
    /**
     * The singleton instance of the factory.
     */
    public static final ClassDiagramHandlerFactory INSTANCE = new ClassDiagramHandlerFactory();
    
    private IDisplaySceneHandler displayClassDiagramSceneHandler;
    private IClassDiagramViewHandler classDiagramViewHandler;
    private ITextViewHandler associationMultiplicityHandler;
    private ITextViewHandler associationRoleNameHandler;
    private ITextViewHandler textViewHandler;
    private ITextViewHandler noteContentHandler;
    private IRelationshipViewHandler associationViewHandler;
    private IAttributeNameHandler attributeNameHandler;

    private IAttributeViewHandler attributeViewHandler;

    private IClassViewHandler classViewHandler;

    private ITextViewHandler classifierVisibilityHandler;

    private ITextViewHandler classNameHandler;

    private IEnumLiteralViewHandler enumLiteralViewHandler;

    private ITextViewHandler enumLiteralNameHandler;

    private ITextViewHandler enumNameHandler;

    private IEnumViewHandler enumViewHandler;

    private ITextViewHandler implementationEnumNameHandler;

    private IClassifierViewHandler implementationClassViewHandler;

    private IRelationshipViewHandler inheritanceViewHandler;

    private ITextViewHandler operationNameHandler;

    private IOperationViewHandler operationViewHandler;

    private ITextViewHandler operationVisibilityViewHandler;

    private IParameterViewHandler parameterViewHandler;

    private ITextViewHandler parameterNameHandler;

    private IBaseViewHandler noteViewHandler;

    private INaryAssociationNodeViewHandler associationNodeViewHandler;

    private ITextViewHandler associationPropertyHandler;
    
    
    /**
     * Create a new instance.
     */
    private ClassDiagramHandlerFactory() {
        
    }
    
    /**
     * Returns the default handler for a display class diagram scene.
     * 
     * @return the default {@link IDisplaySceneHandler}
     */
    public IDisplaySceneHandler getDisplayClassDiagramSceneHandler() {
        if (displayClassDiagramSceneHandler == null) {
            displayClassDiagramSceneHandler = new DisplayClassDiagramSceneHandler();
        }

        return displayClassDiagramSceneHandler;
    }
    
    /**
     * Returns the default handler for a ClassDiagramView.
     * 
     * @return the default {@link IClassDiagramViewHandler}
     */
    public IClassDiagramViewHandler getClassDiagramViewHandler() {
        if (classDiagramViewHandler == null) {
            classDiagramViewHandler = new ClassDiagramViewHandler();
        }
        
        return classDiagramViewHandler;
    }

    /**
     * Returns the default handler for a AssociationMultiplicity.
     * 
     * @return the default {@link ITextViewHandler}
     */
    public ITextViewHandler getAssociationMultiplicityHandler() {
        if (associationMultiplicityHandler == null) {
            associationMultiplicityHandler = new AssociationMultiplicityHandler();
        }
        
        return associationMultiplicityHandler;
    }

    /**
     * Returns the default handler for a AssociationRoleName.
     * 
     * @return the default {@link ITextViewHandler}
     */
    public ITextViewHandler getAssociationRoleNameHandler() {
        if (associationRoleNameHandler == null) {
            associationRoleNameHandler = new AssociationRoleNameHandler();
        }
        
        return associationRoleNameHandler;
    }

    /**
     * Returns the default handler for a AssociationView.
     * 
     * @return the default {@link IRelationshipViewHandler}
     */
    public IRelationshipViewHandler getAssociationViewHandler() {
        if (associationViewHandler == null) {
            associationViewHandler = new AssociationViewHandler();
        }
        
        return associationViewHandler;
    }
    
    /**
     * Returns the default handler for a text view.
     * 
     * @return the default {@link ITextViewHandler}
     */
    public ITextViewHandler getTextViewHandler() {
        if (textViewHandler == null) {
            textViewHandler = new TextViewHandler();
        }

        return textViewHandler;
    }
    
    /**
     * Returns the default handler for a note content.
     * 
     * @return the default {@link ITextViewHandler}
     */
    public ITextViewHandler getNoteContentHandler() {
        if (noteContentHandler == null) {
            noteContentHandler = new TextViewHandler();
        }

        return noteContentHandler;
    }

    /**
     * Returns the default handler for an attribute name text view.
     * 
     * @return the default {@link IAttributeNameHandler}
     */
    public IAttributeNameHandler getAttributeNameHandler() {
        if (attributeNameHandler == null) {
            attributeNameHandler = new AttributeNameHandler();
        }

        return attributeNameHandler;
    }

    /**
     * Returns the default handler for a Classifier visibility.
     * 
     * @return the default {@link ITextViewHandler}
     */
    public ITextViewHandler getClassifierVisibilityHandler() {
        if (classifierVisibilityHandler == null) {
            classifierVisibilityHandler = new ClassifierVisibilityViewHandler();
        }
        
        return classifierVisibilityHandler;
    }
    
    /**
     * Returns the default handler for a Class view.
     * 
     * @return the default {@link IAttributeNameHandler}
     */
    public IClassViewHandler getClassViewHandler() {
        if (classViewHandler == null) {
            classViewHandler = new ClassViewHandler();
        }
        return classViewHandler;
    }

    /**
     * Returns the default handler for a Class name.
     * 
     * @return the default {@link IAttributeNameHandler}
     */
    public ITextViewHandler getClassNameHandler() {
        if (classNameHandler == null) {
            classNameHandler = new ClassNameHandler();
        }
        return classNameHandler;
    }
 
    /**
     * Returns the default handler for an Enum Literal name.
     * 
     * @return the default {@link ITextViewHandler}
     */
    public ITextViewHandler getEnumLiteralNameHandler() {
        if (enumLiteralNameHandler == null) {
            enumLiteralNameHandler = new EnumLiteralNameHandler();
        }
        return enumLiteralNameHandler;
    }

    /**
     * Returns the default handler for an Enum Literal view.
     * 
     * @return the default {@link IEnumLiteralViewHandler}
     */
    public IEnumLiteralViewHandler getEnumLiteralViewHandler() {
        if (enumLiteralViewHandler == null) {
            enumLiteralViewHandler = new EnumLiteralViewHandler();
        }
        return enumLiteralViewHandler;
    }

    /**
     * Returns the default handler for an Enum name.
     * 
     * @return the default {@link ITextViewHandler}
     */
    public ITextViewHandler getEnumNameHandler() {
        if (enumNameHandler == null) {
            enumNameHandler = new EnumNameHandler(false);
        }
        return enumNameHandler;
    }

    /**
     * Returns the default handler for an Enum View.
     * 
     * @return the default {@link IEnumViewHandler}
     */
    public IEnumViewHandler getEnumViewHandler() {
        if (enumViewHandler == null) {
            enumViewHandler = new EnumViewHandler();
        }
        return enumViewHandler;
    }
    
    /**
     * Returns the default handler for a Note View.
     * 
     * @return the default {@link INoteViewHandler}
     */
    public IBaseViewHandler getNoteViewHandler() {
        if (noteViewHandler == null) {
            noteViewHandler = new NoteViewHandler();
        }
        return noteViewHandler;
    }

    /**
     * Returns the default handler for an Implementation Enum name.
     * 
     * @return the default {@link ITextViewHandler}
     */
    public ITextViewHandler getImplementationEnumNameHandler() {
        if (implementationEnumNameHandler == null) {
            implementationEnumNameHandler = new EnumNameHandler(true);
        }
        return implementationEnumNameHandler;
    }
    
    /**
     * Returns the default handler for an Implementation Class view.
     * 
     * @return the default {@link IClassifierViewHandler}
     */
    public IClassifierViewHandler getImplementationClassViewHandler() {
        if (implementationClassViewHandler == null) {
            implementationClassViewHandler = new ImplementationClassViewHandler();
        }
        return implementationClassViewHandler;
    }
    
    /**
     * Returns the default handler for a Nary Association node view.
     * 
     * @return the default {@link INaryAssociationNodeViewHandler}
     */
    public INaryAssociationNodeViewHandler getNaryAssociationNodeViewHandler() {
        if (associationNodeViewHandler == null) {
            associationNodeViewHandler = new NaryAssociationNodeViewHandler();
        }
        return associationNodeViewHandler;
    }
    
    /**
     * Returns the default handler for an Inheritance view.
     * 
     * @return the default {@link IRelationshipViewHandler}
     */
    public IRelationshipViewHandler getInheritanceViewHandler() {
        if (inheritanceViewHandler == null) {
            inheritanceViewHandler = new InheritanceViewHandler();
        }
        return inheritanceViewHandler;
    }

    /**
     * Returns the default handler for an Operation view.
     * 
     * @return the default {@link IOperationViewHandler}
     */
    public IOperationViewHandler getOperationViewHandler() {
        if (operationViewHandler == null) {
            operationViewHandler = new OperationViewHandler();
        }
        return operationViewHandler;
    }

    /**
     * Returns the default handler for an Operation Visibility View.
     * 
     * @return the default {@link ITextViewHandler}
     */
    public ITextViewHandler getOperationVisibilityViewHandler() {
        if (operationVisibilityViewHandler == null) {
            operationVisibilityViewHandler = new OperationVisibilityViewHandler();
        }
        return operationVisibilityViewHandler;
    }

    /**
     * Returns the default handler for an Operation Name.
     * 
     * @return the default {@link ITextViewHandler}
     */
    public ITextViewHandler getOperationNameHandler() {
        if (operationNameHandler == null) {
            operationNameHandler = new OperationNameHandler();
        }
        return operationNameHandler;
    }    

    /**
     * Returns the default handler for an attribute view.
     * 
     * @return the default {@link IAttributeViewHandler}
     */
    public IAttributeViewHandler getAttributeViewHandler() {
        if (attributeViewHandler == null) {
            attributeViewHandler = new AttributeViewHandler();
        }

        return attributeViewHandler;
    }

    /**
     * Returns the default handler for a Parameter view.
     * 
     * @return the default {@link IParameterViewHandler}
     */
    public IParameterViewHandler getParameterViewHandler() {
        if (parameterViewHandler == null) {
            parameterViewHandler = new ParameterViewHandler();
        }

        return parameterViewHandler;
    }

    /**
     * Returns the default handler for a Parameter Name.
     * 
     * @return the default {@link ITextViewHandler}
     */
    public ITextViewHandler getParameterNameHandler() {
        if (parameterNameHandler == null) {
            parameterNameHandler = new ValidatingTextViewHandler(MetamodelRegex.REGEX_TYPE_NAME);
        }

        return parameterNameHandler;
    }
  
    /**
     * Returns the default handler for an Association Property (Uniqueness and Ordering).
     * 
     * @return the default {@link ITextViewHandler}
     */
    public ITextViewHandler getAssociationPropertyHandler() {
        if (associationPropertyHandler == null) {
            associationPropertyHandler = new AssociationPropertyHandler();
        }

        return associationPropertyHandler;
    }

    
}
