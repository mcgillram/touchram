package ca.mcgill.sel.classdiagram.ui.views.handler.impl;

import ca.mcgill.sel.classdiagram.CDAttributeMapping;
import ca.mcgill.sel.classdiagram.CDClassifierMapping;
import ca.mcgill.sel.classdiagram.CDEnumLiteralMapping;
import ca.mcgill.sel.classdiagram.CDEnumMapping;
import ca.mcgill.sel.classdiagram.CDOperationMapping;
import ca.mcgill.sel.classdiagram.CDParameterMapping;
import ca.mcgill.sel.classdiagram.ui.views.handler.ICdmMappingContainerViewHandler;
import ca.mcgill.sel.classdiagram.language.controller.ControllerFactory;
import ca.mcgill.sel.classdiagram.language.controller.CdmMappingsController;

/**
 * Default handler for {@link ca.mcgill.sel.ram.ui.views.structural.ClassifierMappingContainerView}.
 * 
 * @author eyildirim
 */
public class CdmMappingContainerViewHandler implements ICdmMappingContainerViewHandler {
    
    @Override
    public void addAttributeMapping(CDClassifierMapping classifierMapping) {
        if (classifierMapping.getFrom() != null && classifierMapping.getTo() != null) {
            
            CdmMappingsController mappingsController = ControllerFactory.INSTANCE.getCdmMappingsController();
            mappingsController.createAttributeMapping(classifierMapping);
        }
        
    }
    
    @Override
    public void addEnumLiteralMapping(CDEnumMapping enumMapping) {
        if (enumMapping.getFrom() != null && enumMapping.getTo() != null) {
            
            CdmMappingsController mappingsController = ControllerFactory.INSTANCE.getCdmMappingsController();
            mappingsController.createEnumLiteralMapping(enumMapping);
        }
        
    }
    
    @Override
    public void addOperationMapping(CDClassifierMapping classifierMapping) {
        if (classifierMapping.getFrom() != null && classifierMapping.getTo() != null) {
            CdmMappingsController mappingsController = ControllerFactory.INSTANCE.getCdmMappingsController();
            mappingsController.createOperationMapping(classifierMapping);
        }
    }
    
    @Override
    public void addParameterMapping(CDOperationMapping operationMapping) {
        if (operationMapping.getFrom() != null && operationMapping.getTo() != null) {
            CdmMappingsController mappingsController = ControllerFactory.INSTANCE.getCdmMappingsController();
            mappingsController.createParameterMapping(operationMapping);
        }
    }
    
    @Override
    public void deleteAttributeMapping(CDAttributeMapping attributeMapping) {
        CdmMappingsController mappingsController = ControllerFactory.INSTANCE.getCdmMappingsController();
        mappingsController.deleteMapping(attributeMapping);
    }
    
    @Override
    public void deleteEnumLiteralMapping(CDEnumLiteralMapping enumLiteralMapping) {
        CdmMappingsController mappingsController = ControllerFactory.INSTANCE.getCdmMappingsController();
        mappingsController.deleteMapping(enumLiteralMapping);
    }
    
    @Override
    public void deleteClassifierMapping(CDClassifierMapping classifierMapping) {
        CdmMappingsController mappingsController = ControllerFactory.INSTANCE.getCdmMappingsController();
        mappingsController.deleteMapping(classifierMapping);
    }
    
    @Override
    public void deleteEnumMapping(CDEnumMapping enumMapping) {
        CdmMappingsController mappingsController = ControllerFactory.INSTANCE.getCdmMappingsController();
        mappingsController.deleteMapping(enumMapping);
    }

    @Override
    public void deleteOperationMapping(CDOperationMapping operationMapping) {
        CdmMappingsController mappingsController = ControllerFactory.INSTANCE.getCdmMappingsController();
        mappingsController.deleteMapping(operationMapping);
    }

    @Override
    public void deleteParameterMapping(CDParameterMapping parameterMapping) {
        CdmMappingsController mappingsController = ControllerFactory.INSTANCE.getCdmMappingsController();
        mappingsController.deleteMapping(parameterMapping);
    }

}
