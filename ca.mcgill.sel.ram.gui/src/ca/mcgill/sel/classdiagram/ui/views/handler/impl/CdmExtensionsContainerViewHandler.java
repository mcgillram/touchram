package ca.mcgill.sel.classdiagram.ui.views.handler.impl;

import java.io.File;

import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.COREModelExtension;
import ca.mcgill.sel.core.controller.COREControllerFactory;
import ca.mcgill.sel.core.controller.ReuseController;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.core.util.COREModelUtil;
import ca.mcgill.sel.classdiagram.ClassDiagram;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamPopup.PopupType;
import ca.mcgill.sel.ram.ui.components.browser.GenericFileBrowser;
import ca.mcgill.sel.ram.ui.components.browser.interfaces.FileBrowserListener;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.structural.CompositionSplitEditingView;
import ca.mcgill.sel.ram.ui.views.structural.handler.ICompositionContainerViewHandler;

/**
 * Handles events for a {@link ca.mcgill.sel.ram.ui.views.CompositionContainerView} which is showing the
 * list of compositions in a class diagram.
 * 
 * @author joerg
 */
public class CdmExtensionsContainerViewHandler implements ICompositionContainerViewHandler {
    
    @Override
    public void deleteModelComposition(COREModelComposition modelComposition) {
        // Disallow deleting the COREModelComposition if split view is enabled.
        boolean splitModeEnabled = false;
        // TODO: Used to be RamApp.getActiveAspectScene().getCurrentView() instanceof CompositionSplitEditingView;
        
        if (!splitModeEnabled) {
            COREControllerFactory.INSTANCE.getReuseController()
                .removeModelExtension((COREModelExtension) modelComposition);
        } else {
            RamApp.getActiveAspectScene().displayPopup(Strings.POPUP_CANT_DELETE_INST_EDIT);
        }
    }

    @Override
    public void loadBrowser(final COREArtefact mainModel) {

        // Ask the user to load a class diagram
        GenericFileBrowser.loadModel("cdm", new FileBrowserListener() {

            @Override
            public void modelLoaded(final EObject model) {
                extendAspect(mainModel, (ClassDiagram) model);
            }

            @Override
            public void modelSaved(File file) {
            }
        });
    }
    

    @Override
    public void tailorExistingReuse(COREArtefact artefact) {
        // not implemented
        
    }

    /**
     * Create an extend relationship between extendedModel and extendingModel.
     * Does nothing if the creation is not valid.
     *
     * @param extendingArtefact - The aspect that extends
     * @param cdmToExtend - The class diagram to extend.
     */
    private static void extendAspect(final COREArtefact extendingArtefact, final ClassDiagram cdmToExtend) {
        RamApp.getApplication().invokeLater(new Runnable() {
            @SuppressWarnings("unlikely-arg-type")
            @Override
            public void run() {
                String errorMessage = null;
                ClassDiagram extendingCdm = (ClassDiagram)
                        ((COREExternalArtefact) extendingArtefact).getRootModelElement();
                // Look for error cases.
                if (cdmToExtend.equals(extendingCdm)) {
                    errorMessage = Strings.POPUP_ERROR_SELF_EXTENDS;
                } else if (COREModelUtil.collectExtendedModels(cdmToExtend).contains(extendingCdm)) {
                    errorMessage = Strings.POPUP_ERROR_CYCLIC_EXTENDS;
                }
                // Check if the extendingCdm is already extended.
                for (COREModelComposition composition : extendingArtefact.getModelExtensions()) {
                    if (composition.getSource() == cdmToExtend) {
                        errorMessage = Strings.POPUP_ERROR_SAME_EXTENDS;
                        break;
                    }
                }
                if (errorMessage != null) {
                    RamApp.getActiveScene().displayPopup(errorMessage, PopupType.ERROR);
                    return;
                }
                // We can create the composition.
                ReuseController controller = COREControllerFactory.INSTANCE.getReuseController();
                controller.createModelExtension(extendingArtefact,
                        COREArtefactUtil.getReferencingExternalArtefact(cdmToExtend));
            }
        });
    }


}
