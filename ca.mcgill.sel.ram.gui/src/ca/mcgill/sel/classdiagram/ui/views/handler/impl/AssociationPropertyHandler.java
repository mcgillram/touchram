package ca.mcgill.sel.classdiagram.ui.views.handler.impl;

import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;

import ca.mcgill.sel.classdiagram.AssociationEnd;
import ca.mcgill.sel.classdiagram.language.controller.ControllerFactory;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.perspective.COREPerspectiveUtil;
import ca.mcgill.sel.core.perspective.PerspectiveName;
import ca.mcgill.sel.ram.ui.perspective.controller.PerspectiveControllerFactory;
import ca.mcgill.sel.ram.ui.perspective.controller.ReexposedClassDiagramAction;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.impl.TextViewHandler;

/**
 * The default handler for a Qualifier Text View. It allows selecting another qualifier type
 * 
 * @author yhattab
 * */

public class AssociationPropertyHandler extends TextViewHandler {

    @Override
    public boolean processTapEvent(TapEvent tapEvent) {
        if (tapEvent.isDoubleTap()) {
            TextView view = (TextView) tapEvent.getTarget();
            AssociationEnd associationEnd = (AssociationEnd) view.getData();
            
            //toggle 4 different combinations of ordering and uniqueness properties
            if (associationEnd != null) {
                if (associationEnd.isOrdered()) {
                    if (associationEnd.isUnique()) {
                        ControllerFactory.INSTANCE.getAssociationController().switchUniqueness(associationEnd);
                        
                    } else {
                        ControllerFactory.INSTANCE.getAssociationController().switchOrdering(associationEnd);
                        
                    }
                } else {
                    if (associationEnd.isUnique()) {
                        ControllerFactory.INSTANCE.getAssociationController().switchOrdering(associationEnd);
                    } else {
                        ControllerFactory.INSTANCE.getAssociationController().switchUniqueness(associationEnd); 
                    }
                }
            }
            
            return true;
        }

        return false;
    }
}
