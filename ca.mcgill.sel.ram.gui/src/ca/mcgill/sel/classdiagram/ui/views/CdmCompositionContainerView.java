package ca.mcgill.sel.classdiagram.ui.views;

import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.ram.ui.views.CompositionContainerView;
import ca.mcgill.sel.ram.ui.views.CompositionView;
import ca.mcgill.sel.ram.ui.views.handler.HandlerFactory;

/**
 * This subclass of CompositionContainerView is used for creating CdmCompositionViews inside the CompositionPanel
 * when displaying a class diagram.
 *
 * @author joerg
 */
public class CdmCompositionContainerView extends CompositionContainerView {

    /**
     * Constant representing the "add CDClassifier mapping" action.
     */
    public static final String ACTION_CLASSIFIER_MAPPING_ADD = "mapping.cdclassifier.add";
    
    /**
     * Constant representing the "add CDEnum mapping" action.
     */
    public static final String ACTION_ENUM_MAPPING_ADD = "mapping.cdenum.add";        
    
    public CdmCompositionContainerView(COREArtefact currentArtefact, boolean isModelReuseContainer, String title) {
        super(currentArtefact, isModelReuseContainer, title);
    }

    /**
     * This method must return the appropriate CompositionView depending on the language.
     * @param composition the composition
     * @param reuse whether or not this is a reuse
     * @return the CompositionView corresponding to the language
     */
    @Override
    public CompositionView createCompositionView(COREModelComposition composition, COREReuse reuse) {
        return new CdmCompositionView(composition, reuse, this);
    }

    @Override
    public void customizableActionPerformed(ActionEvent event, EObject mappingContainer) {
        String actionCommand = event.getActionCommand();
    
        if (ACTION_CLASSIFIER_MAPPING_ADD.equals(actionCommand)) {
            HandlerFactory.INSTANCE.getCdmCompositionViewHandler()
                .addClassifierMapping((COREModelComposition) mappingContainer);
        } else if (ACTION_ENUM_MAPPING_ADD.equals(actionCommand)) {
            HandlerFactory.INSTANCE.getCdmCompositionViewHandler()
                .addEnumMapping((COREModelComposition) mappingContainer);
        }        
    }
    
    
}
