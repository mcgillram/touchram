package ca.mcgill.sel.usecases.ui.views;

import org.eclipse.emf.common.notify.Notification;
import org.mt4j.util.MTColor;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.ram.ui.components.RamRoundedRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent.Alignment;
import ca.mcgill.sel.ram.ui.layouts.HorizontalLayout;
import ca.mcgill.sel.ram.ui.layouts.VerticalLayout;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.Fonts;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.usecases.Actor;
import ca.mcgill.sel.usecases.LayoutElement;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.UseCase;
import ca.mcgill.sel.usecases.ui.utils.UseCaseModelHandlerFactory;
import ca.mcgill.sel.usecases.ui.views.ActorUseCaseAssociationView.AssociationType;
import ca.mcgill.sel.usecases.ui.views.handler.IUseCaseViewHandler;
import ca.mcgill.sel.usecases.util.UseCaseAssociationType;

public class UseCaseView extends LinkableView<IUseCaseViewHandler> {

    private static final float MINIMUM_WIDTH = 80.0f;
    private static final float MAXIMUM_WIDTH = 120.0f;
    private static final int ELLIPSE_ARC_RADIUS = 30;
    private static final float ELLIPSE_VERTICAL_BUFFER = 10.0f;
    private static final float ELLIPSE_HORIZONTAL_BUFFER = 5.0f;
    
    private RamRoundedRectangleComponent ellipse;
    private TextView visibilityField;
    
    private COREArtefact artefact;
    
    protected UseCaseView(UseCaseDiagramView useCaseDiagramView, UseCase represented, LayoutElement layoutElement) {
        super(useCaseDiagramView, represented, layoutElement);

        setMinimumWidth(MINIMUM_WIDTH);
        setNoFill(true);
        setNoStroke(true);
        setLayout(new VerticalLayout());
        
        ellipse = new RamRoundedRectangleComponent(ELLIPSE_ARC_RADIUS);
        ellipse.setMinimumWidth(MINIMUM_WIDTH);
        ellipse.setMaximumWidth(MAXIMUM_WIDTH);
        ellipse.setNoStroke(false);
        ellipse.setNoFill(false);
        ellipse.setFillColor(Colors.CLASS_VIEW_DEFAULT_FILL_COLOR);
        ellipse.setStrokeColor(Colors.DEFAULT_ELEMENT_STROKE_COLOR);
        ellipse.setEnabled(true);
        ellipse.setPickable(true);
        ellipse.setLayout(new HorizontalLayout());
        ellipse.setBufferSize(Cardinal.NORTH, ELLIPSE_VERTICAL_BUFFER);
        ellipse.setBufferSize(Cardinal.SOUTH, ELLIPSE_VERTICAL_BUFFER);
        ellipse.setBufferSize(Cardinal.EAST, ELLIPSE_HORIZONTAL_BUFFER);
        ellipse.setBufferSize(Cardinal.WEST, ELLIPSE_HORIZONTAL_BUFFER);
        addChild(ellipse);

        addNameField();
        nameField.setUnderlined(represented.getMainSuccessScenario() == null);
        
        // translate the class based on the meta-model
        if (layoutElement != null) {
            setLayoutElement(layoutElement);
        }
        
        artefact = COREArtefactUtil.getReferencingExternalArtefact(represented);
        EMFEditUtil.addListenerFor(artefact, this);
    }
    
    public UseCase getUseCase() {
        return (UseCase) this.represented;
    }

    @Override
    public void destroy() {        
        destroyRelationships();
        this.destroyAllChildren();
        super.destroy();
    }
    
    @Override
    public void notifyChanged(Notification notification) {
        super.notifyChanged(notification);
        UseCase useCase = this.getUseCase();
        
        if (notification.getNotifier() == useCase) {            
            if (notification.getFeature() == UcPackage.Literals.USE_CASE__PRIMARY_ACTORS
                    || notification.getFeature() == UcPackage.Literals.USE_CASE__SECONDARY_ACTORS
                    || notification.getFeature() == UcPackage.Literals.USE_CASE__FACILITATOR_ACTORS) {
                AssociationType associationType;
                if (notification.getFeature() == UcPackage.Literals.USE_CASE__PRIMARY_ACTORS) {
                    associationType = AssociationType.PRIMARY; 
                } else if (notification.getFeature() == UcPackage.Literals.USE_CASE__SECONDARY_ACTORS) {
                    associationType = AssociationType.SECONDARY;
                } else {
                    associationType = AssociationType.FACILITATOR;
                }
                
                switch (notification.getEventType()) {
                    case Notification.ADD:
                        Actor newActor = (Actor) notification.getNewValue();
                        useCaseDiagramView.addAssociationView(useCase, newActor, associationType);
                        break;
                    
                    case Notification.SET:
                        Actor setActor = (Actor) notification.getNewValue();
                        if (setActor == null) {
                            Actor unsetActor = (Actor) notification.getOldValue();
                            useCaseDiagramView.removeAssociationView(useCase, unsetActor);
                        } else {
                            useCaseDiagramView.addAssociationView(useCase, setActor, associationType);
                        }
                    case Notification.REMOVE:
                        Actor oldActor = (Actor) notification.getOldValue();
                        useCaseDiagramView.removeAssociationView(useCase, oldActor);
                        break;
                        
                }
            } else if (notification.getFeature() == UcPackage.Literals.USE_CASE__GENERALIZATION) {
                UseCase oldValue = (UseCase) notification.getOldValue();
                UseCase newValue = (UseCase) notification.getNewValue();
                
                if (oldValue != null) {
                    useCaseDiagramView.removeInheritanceAssociationView(useCase, oldValue);
                }
                
                if (newValue != null) {
                    useCaseDiagramView.addInheritanceAssociationView(useCase, newValue);
                }
            } else if (notification.getFeature() == UcPackage.Literals.USE_CASE__INCLUDED_USE_CASES) {
                switch (notification.getEventType()) {
                    case Notification.ADD:
                        UseCase includedUseCase = (UseCase) notification.getNewValue();
                        useCaseDiagramView.addUseCaseAssociationView(
                                useCase, includedUseCase, UseCaseAssociationType.INCLUDE);
                        break;
                    case Notification.REMOVE:
                        UseCase removedUseCase = (UseCase) notification.getOldValue();
                        useCaseDiagramView.removeUseCaseAssociationView(
                                useCase, removedUseCase, UseCaseAssociationType.INCLUDE);
                        break;  
                } 
            } else if (notification.getFeature() == UcPackage.Literals.NAMED_ELEMENT__NAME) {
                // Changing the name can also change the width
                useCaseDiagramView.resizeSystemBoundary();
            } else if (notification.getFeature() == UcPackage.Literals.USE_CASE__ABSTRACT) {
                boolean italic = notification.getNewBooleanValue();
                setNameItalic(italic);
            } else if (notification.getFeature() == UcPackage.Literals.USE_CASE__MAIN_SUCCESS_SCENARIO) {
                nameField.setUnderlined(notification.getNewValue() == null);
            }
        } else if (notification.getNotifier() == layoutElement) {
            useCaseDiagramView.resizeSystemBoundary();
        }        
    }
    
    /**
     * Set the class name in italic.
     *
     * @param italic true if you want to set the class name in italic. false otherwise.
     */
    private void setNameItalic(boolean italic) {
        if (italic) {
            nameField.setFont(Fonts.FONT_CLASS_NAME_ITALIC);                
        } else {
            nameField.setFont(Fonts.FONT_CLASS_NAME);
        }
    }
    
    @Override
    public void setStrokeColor(MTColor color) {
        if (ellipse != null) {
            ellipse.setStrokeColor(color);    
        }        
    }
    
    @Override
    public MTColor getStrokeColor() {
        if (ellipse != null) {
            return ellipse.getStrokeColor();
        } else {
            return super.getStrokeColor();
        }
    }
    
    @Override
    public void setFillColor(MTColor color) {
        if (ellipse != null) {
            ellipse.setFillColor(color);
        }
    }
    
    @Override
    public MTColor getFillColor() {
        if (ellipse != null) {
            return ellipse.getFillColor();
        } else {
            return super.getFillColor();
        }
    }
    
    private void addNameField() {
        // Add the visibility field to the view
        visibilityField = new TextView(getUseCase(), UcPackage.Literals.MAPPABLE_ELEMENT__VISIBILITY);
        visibilityField.setFont(Fonts.DEFAULT_FONT_MEDIUM);
        visibilityField.setNoFill(true);
        //ellipse.addChild(visibilityField);        
        //((TextView) visibilityField).setHandler(UseCaseModelHandlerFactory.INSTANCE.getVisibilityViewHandler());
        
        // Add the name field to base view
        nameField = new TextView(getUseCase(), UcPackage.Literals.NAMED_ELEMENT__NAME);
        nameField.setFont(Fonts.DEFAULT_FONT_MEDIUM);
        setNameItalic(((UseCase) this.represented).isAbstract());
        nameField.setAlignment(Alignment.CENTER_ALIGN);
        nameField.setPlaceholderText(Strings.PH_ENTER_USE_CASE_NAME);
        nameField.setNewlineDisabled(true);
        nameField.setNoFill(true);
        ((TextView) nameField).setUniqueName(true);
        
        ellipse.addChild(nameField);
        
        ((TextView) nameField).setHandler(UseCaseModelHandlerFactory.INSTANCE.getTextViewHandler());
    }
}
