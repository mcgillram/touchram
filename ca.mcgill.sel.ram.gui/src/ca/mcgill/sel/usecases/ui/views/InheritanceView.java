package ca.mcgill.sel.usecases.ui.views;

import org.eclipse.emf.ecore.EObject;
import org.mt4j.components.TransformSpace;
import org.mt4j.components.visibleComponents.shapes.MTPolygon;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.utils.GraphicalUpdater;
import ca.mcgill.sel.ram.ui.views.IRelationshipEndView;
import ca.mcgill.sel.ram.ui.views.RamEnd;
import ca.mcgill.sel.ram.ui.views.RelationshipView;
import ca.mcgill.sel.ram.ui.views.RamEnd.Position;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.UseCaseModel;

public class InheritanceView extends RelationshipView<EObject, BaseView<?>> {    
    /**
     * Distance from the end of the super class to the point where the next line is drawn (orthogonal), also referred to
     * as the "center".
     */
    private static final float DISTANCE_INHERITANCE = 40.0f;
    
    private GraphicalUpdater graphicalUpdater;
    
    public InheritanceView(EObject from, BaseView<?> fromView, EObject to, BaseView<?> toView) {
        super(from, fromView, to, toView);
        this.lineStyle = LineStyle.SOLID;

        UseCaseModel ucd = EMFModelUtil.getRootContainerOfType(from, UcPackage.Literals.USE_CASE_MODEL);
        graphicalUpdater = RamApp.getApplication().getGraphicalUpdaterForModel(ucd);
        graphicalUpdater.addGUListener(from, this);
    }
    
    @Override
    protected void update() {
        drawAllLines();        
        drawEnd(getToEnd());
    }
    
    /**
     * Draws the line between the center of the note and the center of the annotated element (not using the CDEnds).
     */
    @Override
    protected void drawAllLines() {
        float fromX = fromEnd.getLocation().getX();
        float fromY = fromEnd.getLocation().getY();
        Position positionFrom = fromEnd.getPosition();
        float toX = toEnd.getLocation().getX();
        float toY = toEnd.getLocation().getY();
        Position positionTo = toEnd.getPosition();
        
        // if the x or y values are the same they are just directly next to each other or above each other
        // only one line necessary
        // since the values are floats a comparison needs to be done using an epsilon
        if (Math.abs(fromX - toX) < EPSILON || Math.abs(fromY - toY) < EPSILON) {
            drawLine(fromX, fromY, null, toX, toY);
        } else {
            // "to" is the superclass, "from" is the subclass
            // the center should be closer to the super class
            float centerX = toX + (fromX - toX > 0 ? DISTANCE_INHERITANCE : -DISTANCE_INHERITANCE);
            float centerY = toY + (fromY - toY > 0 ? DISTANCE_INHERITANCE : -DISTANCE_INHERITANCE);
            drawLine(fromX, fromY, positionFrom, centerX, centerY);
            drawLine(toX, toY, positionTo, centerX, centerY);
            // revert the position according to the current one
            // since drawLine does the same for TOP and BOTTOM it is not necessary to distinguish between the two cases
            // if the classes are next to each other the y value stays the same and the x center is used
            if (positionFrom == Position.RIGHT || positionFrom == Position.LEFT) {
                drawLine(centerX, fromY, Position.TOP, centerX, toY);
                // otherwise x stays the same and the y center is used
            } else {
                drawLine(fromX, centerY, Position.LEFT, toX, centerY);
            }
        }
    }
    
    @Override
    protected void updateRelationshipEnds(RamRectangleComponent viewFrom, RamRectangleComponent viewTo,
            Position fromEndPosition, Position toEndPosition) {
     // now we set the positions.
        if (viewFrom instanceof LinkableView) {
            // process from end
            LinkableView<?> classViewFrom = (LinkableView<?>) viewFrom;
            LinkableView<?> classViewTo = (LinkableView<?>) viewTo;

            // if previous and current positions are different
            // also if fromEnd.getPosition() is null
            if (fromEndPosition != fromEnd.getPosition()) {
                classViewFrom.moveRelationshipEnd(fromEnd, fromEndPosition);
                classViewFrom.updateLayout();
            } else {
                // if position is the same reset the positions
                // for all ends on the same edge of this view.
                classViewFrom.setCorrectPosition(fromEnd);
            }

            // process to end
            if (toEndPosition != toEnd.getPosition()) {
                classViewTo.moveRelationshipEnd(toEnd, toEndPosition);
                classViewFrom.moveRelationshipEnd(fromEnd, fromEndPosition);
                classViewTo.updateLayout();
            } else {
                classViewTo.setCorrectPosition(toEnd);
            }
        } else if (viewFrom instanceof RamRectangleComponent) {
            // TODO: This is what everyone should do (see issue #109).
            IRelationshipEndView fromRelationshipView = null;
            IRelationshipEndView toRelationshipView = null;
            if (fromEnd.getComponentView() instanceof IRelationshipEndView) {
                fromRelationshipView = (IRelationshipEndView) fromEnd.getComponentView();
            }
            if (toEnd.getComponentView() instanceof IRelationshipEndView) {
                toRelationshipView = (IRelationshipEndView) toEnd.getComponentView();
            }
            // End has to be moved somewhere else.
            if (fromEndPosition != fromEnd.getPosition()) {
                Position oldPosition = fromEnd.getPosition();
                fromEnd.setPosition(fromEndPosition);
                if (fromRelationshipView != null) {
                    fromRelationshipView.moveRelationshipEnd(fromEnd, oldPosition, fromEndPosition);
                }
            } else {
                if (fromRelationshipView != null) {
                    fromRelationshipView.updateRelationshipEnd(fromEnd);
                }
            }

            if (toEndPosition != toEnd.getPosition()) {
                Position oldPosition = toEnd.getPosition();
                toEnd.setPosition(toEndPosition);
                if (toRelationshipView != null) {
                    toRelationshipView.moveRelationshipEnd(toEnd, oldPosition, toEndPosition);
                }
                // The from end might have to be fixed depending on what happened to the to end.
                if (fromRelationshipView != null) {
                    fromRelationshipView.updateRelationshipEnd(fromEnd);
                }
            } else {
                if (toRelationshipView != null) {
                    toRelationshipView.updateRelationshipEnd(toEnd);
                }
            }
        }
    }
    
    /**
     * Updates the line layout for the view.
     */
    @Override
    public void updateLines() {
        RamRectangleComponent viewFrom = fromEnd.getComponentView();
        RamRectangleComponent viewTo = toEnd.getComponentView();

        // get the top left corner coordinates of the classes for all calculations
        Vector3D topLeftFrom = viewFrom.getPosition(TransformSpace.GLOBAL);
        Vector3D topLeftTo = viewTo.getPosition(TransformSpace.GLOBAL);

        float heightFrom = viewFrom.getHeightXY(TransformSpace.GLOBAL);
        float widthFrom = viewFrom.getWidthXY(TransformSpace.GLOBAL);
        float heightTo = viewTo.getHeightXY(TransformSpace.GLOBAL);
        float widthTo = viewTo.getWidthXY(TransformSpace.GLOBAL);

        // calculate the outer coordinates and center of each class
        float leftXFrom = topLeftFrom.getX();
        float rightXFrom = topLeftFrom.getX() + widthFrom;
        float topYFrom = topLeftFrom.getY();
        float bottomYFrom = topLeftFrom.getY() + heightFrom;
        float leftXTo = topLeftTo.getX();
        float rightXTo = topLeftTo.getX() + widthTo;
        float topYTo = topLeftTo.getY();
        float bottomYTo = topLeftTo.getY() + heightTo;

        Position fromEndPosition = null;
        Position toEndPosition = null;

        // it is an inheritance relationship
        // "to" is the superclass, "from" is the subclass
        if (topYTo > bottomYFrom) {
            fromEndPosition = Position.BOTTOM;
            toEndPosition = Position.TOP;
        } else if (bottomYTo < topYFrom) {
            fromEndPosition = Position.TOP;
            toEndPosition = Position.BOTTOM;
        } else if (rightXFrom < leftXTo) {
            fromEndPosition = Position.RIGHT;
            toEndPosition = Position.LEFT;
        } else if (leftXFrom > rightXTo) {
            fromEndPosition = Position.LEFT;
            toEndPosition = Position.RIGHT;
        } else {
            fromEndPosition = Position.OFFSCREEN;
            toEndPosition = Position.OFFSCREEN;
        }
        
        updateRelationshipEnds(viewFrom, viewTo, fromEndPosition, toEndPosition);
    }
    
    
    /**
     * Draws the visualizing of an end of a message for the given end.
     * A synchronous call ends with a closed (filled) arrow, whereas a reply message has an open arrow.
     * 
     * @param end the end of a message to visualize
     */
    private void drawEnd(RamEnd<EObject, BaseView<?>> end) {
        float x = end.getLocation().getX();
        float y = end.getLocation().getY();

        MTPolygon arrowPolygon = new InheritancePolygon(x, y, drawColor);
                       
        rotateShape(arrowPolygon, x, y, end.getPosition());
        
        addChild(arrowPolygon);
    }
}