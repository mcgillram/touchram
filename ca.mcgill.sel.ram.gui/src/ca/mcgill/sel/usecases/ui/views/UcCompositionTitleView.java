package ca.mcgill.sel.usecases.ui.views;

import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.ram.ui.components.RamButton;
import ca.mcgill.sel.ram.ui.components.RamImageComponent;
import ca.mcgill.sel.ram.ui.events.listeners.ActionListener;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.views.CompositionContainerView;
import ca.mcgill.sel.ram.ui.views.CompositionTitleView;
import ca.mcgill.sel.ram.ui.views.CompositionView;

public class UcCompositionTitleView extends CompositionTitleView implements ActionListener {
    private RamButton buttonAddActorMapping;
    private RamButton buttonAddUseCaseMapping;

    /**
     * Creates a new title view.
     * 
     * @param container the composition container view that contains all the compositions
     * @param compositionView the {@link CompositionView} the title view is for
     * @param reuse - the {@link COREReuse} the composition is for.
     * @param detailedViewOn whether the detailed view should be enabled
     */
    public UcCompositionTitleView(CompositionContainerView container,
            CompositionView<?> compositionView, COREReuse reuse, boolean detailedViewOn) {
        super(container, compositionView, reuse, detailedViewOn);
    }
    
    @Override
    protected void destroyComponent() {
        super.destroyComponent();
        buttonAddActorMapping.destroy();
        buttonAddUseCaseMapping.destroy();
    }
   
    /**
     * Hides all buttons and shows the expand button.
     */
    @Override
    public void hideButtons() {
        super.hideButtons();
        this.removeChild(buttonAddActorMapping);
        this.removeChild(buttonAddUseCaseMapping);
    }
    
    /**
     * Shows all buttons including a collapse button.
     */
    @Override
    public void showButtons() {
        super.showButtons();        
        this.addChild(buttonAddActorMapping);
        this.addChild(buttonAddUseCaseMapping);
    }
    
    @Override
    public void addCustomButtons(boolean detailedViewOn) {
        RamImageComponent addActorImage = new RamImageComponent(Icons.ICON_ACTOR_MAPPING_ADD,
                Colors.ICON_STRUCT_DEFAULT_COLOR);
        addActorImage.setMinimumSize(ICON_SIZE, ICON_SIZE);
        addActorImage.setMaximumSize(ICON_SIZE, ICON_SIZE);
        buttonAddActorMapping = new RamButton(addActorImage);
        buttonAddActorMapping.setActionCommand(UcCompositionContainerView.ACTION_ACTOR_MAPPING_ADD);
        buttonAddActorMapping.addActionListener(this);
        if (detailedViewOn) {
            addChild(buttonAddActorMapping);
        }

        RamImageComponent addUseCaseImage = new RamImageComponent(Icons.ICON_USE_CASE_MAPPING_ADD,
                Colors.ICON_STRUCT_DEFAULT_COLOR);
        addUseCaseImage.setMinimumSize(ICON_SIZE, ICON_SIZE);
        addUseCaseImage.setMaximumSize(ICON_SIZE, ICON_SIZE);
        buttonAddUseCaseMapping = new RamButton(addUseCaseImage);
        buttonAddUseCaseMapping.setActionCommand(UcCompositionContainerView.ACTION_USE_CASE_MAPPING_ADD);
        buttonAddUseCaseMapping.addActionListener(this);
        if (detailedViewOn) {
            addChild(buttonAddUseCaseMapping);
        }        
    }

}
