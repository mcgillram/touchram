package ca.mcgill.sel.usecases.ui.views;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.mt4j.components.MTComponent;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.COREMapping;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamRoundedRectangleComponent;
import ca.mcgill.sel.ram.ui.components.listeners.UINotifyChangedListener;
import ca.mcgill.sel.ram.ui.layouts.VerticalLayout;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.usecases.FlowMapping;
import ca.mcgill.sel.usecases.UseCaseMapping;

public class UseCaseMappingContainerView extends RamRoundedRectangleComponent implements UINotifyChangedListener {
    private UseCaseMappingView useCaseMappingView;
    
    private UseCaseMapping useCaseMapping;
    
    private RamRectangleComponent hideableFlowContainer;
    
    public UseCaseMappingContainerView(UseCaseMapping useCaseMapping) {
        super(0);
        setNoStroke(false);
        setStrokeWeight(3);
        setNoFill(false);
        setFillColor(Colors.MAPPING_CONTAINER_VIEW_FILL_COLOR);

        useCaseMappingView = new UseCaseMappingView(useCaseMapping);
        this.addChild(useCaseMappingView);

        this.useCaseMapping = useCaseMapping;
        
        // Container for the steps
        hideableFlowContainer = new RamRectangleComponent();
        hideableFlowContainer.setLayout(new VerticalLayout(0));
        hideableFlowContainer.setFillColor(Colors.STEP_MAPPING_VIEW_FILL_COLOR);
        hideableFlowContainer.setNoFill(false);
        hideableFlowContainer.setBufferSize(Cardinal.EAST, 0);
        
        this.addChild(hideableFlowContainer);
        
        setLayout(new VerticalLayout());
        setBuffers(0);
        
        addAllFlowMappings();
        
        EMFEditUtil.addListenerFor(useCaseMapping, this);
    }
    
    @Override
    public void destroy() {
        EMFEditUtil.removeListenerFor(useCaseMapping, this);
        super.destroy();
    }

    /**
     * Getter for UseCaseMapping information of the view.
     * 
     * @return {@link UseCaseMapping}
     */
    public UseCaseMapping getUseCaseMapping() {
        return useCaseMapping;
    }
    
    private void addAllFlowMappings() {
        EList<COREMapping<?>> coreMappings = useCaseMapping.getMappings();
        for (COREMapping<?> mapping : coreMappings) {
            if (mapping instanceof FlowMapping) {
                addFlowMappingView((FlowMapping) mapping);
            }
        }
    }
    
    private void addFlowMappingView(FlowMapping mapping) {
        FlowMappingContainerView flowMappingView = new FlowMappingContainerView(mapping);
        hideableFlowContainer.addChild(flowMappingView);
    }
    
    private void deleteFlowMappingView(FlowMapping mapping) {
        MTComponent[] flowMappingContainerViews = hideableFlowContainer.getChildren();
        for (MTComponent view : flowMappingContainerViews) {
            if (view instanceof FlowMappingContainerView) {
                FlowMappingContainerView flowMappingContainerView = (FlowMappingContainerView) view;
                if (flowMappingContainerView.getFlowMapping() == mapping) {
                    hideableFlowContainer.removeChild(flowMappingContainerView);
                }
            }
        }
    }
    
    @Override
    public void handleNotification(Notification notification) {
        if (notification.getNotifier() == useCaseMapping) {
            if (notification.getFeature() == CorePackage.Literals.CORE_MAPPING__MAPPINGS) {
                switch (notification.getEventType()) {
                    case Notification.ADD:
                        COREMapping<?> newMapping = (COREMapping<?>) notification.getNewValue();
                        
                        if (newMapping instanceof FlowMapping) {
                            FlowMapping newFlowMapping = (FlowMapping) newMapping;
                            addFlowMappingView(newFlowMapping);
                        }
                        
                        break;

                    case Notification.REMOVE:
                        COREMapping<?> oldMapping = (COREMapping<?>) notification.getOldValue();

                        if (oldMapping instanceof FlowMapping) {
                            FlowMapping oldFlowMapping = (FlowMapping) oldMapping;
                            deleteFlowMappingView(oldFlowMapping);
                        }
                        break;
                }
            }
        }
    }

}
