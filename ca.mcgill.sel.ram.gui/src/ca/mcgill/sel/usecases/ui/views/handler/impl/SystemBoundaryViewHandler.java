package ca.mcgill.sel.usecases.ui.views.handler.impl;

import org.mt4j.input.inputProcessors.componentProcessors.dragProcessor.DragEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;
import org.mt4j.input.inputProcessors.componentProcessors.unistrokeProcessor.UnistrokeEvent;
import org.mt4j.input.inputProcessors.componentProcessors.unistrokeProcessor.UnistrokeUtils.UnistrokeGesture;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.ram.ui.events.WheelEvent;
import ca.mcgill.sel.ram.ui.views.AbstractView;
import ca.mcgill.sel.ram.ui.views.handler.impl.AbstractViewHandler;
import ca.mcgill.sel.usecases.ui.views.SystemBoundaryView;
import ca.mcgill.sel.usecases.ui.views.UseCaseDiagramView;
import ca.mcgill.sel.usecases.ui.views.handler.ISystemBoundaryViewHandler;

/**
 * This handler's job is just to forward events to the diagram view (its parent).
 * @author rlanguay
 *
 */
public class SystemBoundaryViewHandler extends AbstractViewHandler 
    implements ISystemBoundaryViewHandler {

    @Override
    public boolean processTapEvent(TapEvent tapEvent) {
        final SystemBoundaryView target = (SystemBoundaryView) tapEvent.getTarget();
        final UseCaseDiagramView diagramView = target.getParentOfType(UseCaseDiagramView.class);
        tapEvent.setTarget(diagramView);
        return diagramView.getHandler().processTapEvent(tapEvent);
    }

    @Override
    public boolean processTapAndHoldEvent(TapAndHoldEvent tapAndHoldEvent) {
        final SystemBoundaryView target = (SystemBoundaryView) tapAndHoldEvent.getTarget();
        final UseCaseDiagramView diagramView = target.getParentOfType(UseCaseDiagramView.class);
        tapAndHoldEvent.setTarget(diagramView);
        return diagramView.getHandler().processTapAndHoldEvent(tapAndHoldEvent);
    }

    @Override
    public void handleUnistrokeGesture(AbstractView<?> target, UnistrokeGesture gesture, Vector3D startPosition,
            UnistrokeEvent event) {
        final UseCaseDiagramView diagramView = target.getParentOfType(UseCaseDiagramView.class);
        ((UseCaseDiagramViewHandler) diagramView.getHandler())
            .handleUnistrokeGesture(diagramView, gesture, startPosition, event);
    }
    
    @Override
    public boolean processUnistrokeEvent(UnistrokeEvent uniStrokeEvent) {
        SystemBoundaryView target = (SystemBoundaryView) uniStrokeEvent.getTarget();
        final UseCaseDiagramView diagramView = target.getParentOfType(UseCaseDiagramView.class);
        uniStrokeEvent.setTarget(diagramView);
        return diagramView.getHandler().processUnistrokeEvent(uniStrokeEvent);
    }
    
    @Override
    protected boolean processWheelEvent(WheelEvent wheelEvent) {
        if (wheelEvent.getTarget() instanceof SystemBoundaryView) {
            SystemBoundaryView target = (SystemBoundaryView) wheelEvent.getTarget();
            final UseCaseDiagramView diagramView = target.getParentOfType(UseCaseDiagramView.class);
            wheelEvent.setTarget(diagramView);
            return diagramView.getHandler().processGestureEvent(wheelEvent);
        }
        
        return super.processWheelEvent(wheelEvent);
    }
    
    @Override
    public boolean processDragEvent(DragEvent dragEvent) {
        SystemBoundaryView target = (SystemBoundaryView) dragEvent.getTarget();
        final UseCaseDiagramView diagramView = target.getParentOfType(UseCaseDiagramView.class);
        dragEvent.setTarget(diagramView);
        return diagramView.getHandler().processDragEvent(dragEvent);
    }

}
