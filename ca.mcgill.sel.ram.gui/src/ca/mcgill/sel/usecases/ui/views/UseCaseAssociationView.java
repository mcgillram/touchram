package ca.mcgill.sel.usecases.ui.views;

import org.mt4j.components.TransformSpace;
import org.mt4j.components.visibleComponents.shapes.MTPolygon;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.ram.ui.utils.GraphicalUpdater;
import ca.mcgill.sel.ram.ui.views.IRelationshipEndView;
import ca.mcgill.sel.ram.ui.views.RamEnd;
import ca.mcgill.sel.ram.ui.views.RamEnd.Position;
import ca.mcgill.sel.ram.ui.views.RelationshipView;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.UseCase;
import ca.mcgill.sel.usecases.UseCaseModel;
import ca.mcgill.sel.usecases.util.UseCaseAssociationType;
import processing.core.PGraphics;

public class UseCaseAssociationView extends RelationshipView<UseCase, UseCaseView> {

    private static final int SCALING_CONSTANT = 180;
    private static final int TEXT_OFFSET_FROM_ACTOR = 10;
    private static final float ARROWHEAD_OFFSET_FROM_USECASE = 8.0f;
    private static final float TEXT_OFFSET = 50.0f;
    
    private static final String INCLUDE_TEXT = "<<include>>";
    private static final String EXTEND_TEXT = "<<extend>>";
    
    private Position lastFromPosition;

    private GraphicalUpdater graphicalUpdater;
    private boolean shouldUpdate;
    private UseCaseAssociationType type;
    
    // visual elements
    private RamTextComponent textToDisplay;
    private MTPolygon arrowPolygon;
    private float currentTextRotation;
    private float currentArrowRotation;
   
    public UseCaseAssociationView(UseCase from, UseCaseView fromView, UseCase to, UseCaseView toView, 
            UseCaseAssociationType type) {
        super(from, fromView, to, toView);
        this.shouldUpdate = true;
        this.type = type;
        
        this.lineStyle = LineStyle.DASHED;                

        UseCaseModel ucd = EMFModelUtil.getRootContainerOfType(from, UcPackage.Literals.USE_CASE_MODEL);
        graphicalUpdater = RamApp.getApplication().getGraphicalUpdaterForModel(ucd);
        graphicalUpdater.addGUListener(from, this);
    }
    
    public UseCaseAssociationType getType() {
        return this.type;
    }

    @Override
    protected void update() {
        drawAllLines();
        createTextIfNecessary();
        moveTexts(getFromEnd(), textToDisplay, lastFromPosition);
        rotateTextFree();    
        
        lastFromPosition = getToEnd().getPosition();
        drawEnd(getToEnd());
    }
    
    /**
     * Draws the line between the center of the note and the center of the annotated element (not using the CDEnds).
     */
    @Override
    protected void drawAllLines() {
        float fromX = fromEnd.getLocation().getX();
        float fromY = fromEnd.getLocation().getY();
        float toX = toEnd.getLocation().getX();
        float toY = toEnd.getLocation().getY();

        drawLine(fromX, fromY, null, toX, toY);
    }
    
    @Override
    public void drawComponent(PGraphics g) {
        if (this.shouldUpdate) {
            prepareUpdate();
            this.shouldUpdate = false;
        }
        super.drawComponent(g);
    }
    
    @Override
    protected void updateRelationshipEnds(RamRectangleComponent viewFrom, RamRectangleComponent viewTo,
            Position fromEndPosition, Position toEndPosition) {
     // now we set the positions.
        if (viewFrom instanceof LinkableView) {
            // process from end
            LinkableView<?> useCaseViewFrom = (LinkableView<?>) viewFrom;
            LinkableView<?> useCaseViewTo = (LinkableView<?>) viewTo;

            // if previous and current positions are different
            // also if fromEnd.getPosition() is null
            if (fromEndPosition != fromEnd.getPosition()) {
                useCaseViewFrom.moveRelationshipEnd(fromEnd, fromEndPosition);
                useCaseViewFrom.updateLayout();
            } else {
                // if position is the same reset the positions
                // for all ends on the same edge of this view.
                useCaseViewFrom.setCorrectPosition(fromEnd);
            }

            // process to end
            if (toEndPosition != toEnd.getPosition()) {
                useCaseViewTo.moveRelationshipEnd(toEnd, toEndPosition);
                useCaseViewFrom.moveRelationshipEnd(fromEnd, fromEndPosition);
                useCaseViewTo.updateLayout();
            } else {
                useCaseViewTo.setCorrectPosition(toEnd);
            }
        } else if (viewFrom instanceof RamRectangleComponent) {
            IRelationshipEndView fromRelationshipView = null;
            IRelationshipEndView toRelationshipView = null;
            if (fromEnd.getComponentView() instanceof IRelationshipEndView) {
                fromRelationshipView = (IRelationshipEndView) fromEnd.getComponentView();
            }
            if (toEnd.getComponentView() instanceof IRelationshipEndView) {
                toRelationshipView = (IRelationshipEndView) toEnd.getComponentView();
            }
            // End has to be moved somewhere else.
            if (fromEndPosition != fromEnd.getPosition()) {
                Position oldPosition = fromEnd.getPosition();
                fromEnd.setPosition(fromEndPosition);
                if (fromRelationshipView != null) {
                    fromRelationshipView.moveRelationshipEnd(fromEnd, oldPosition, fromEndPosition);
                }
            } else {
                if (fromRelationshipView != null) {
                    fromRelationshipView.updateRelationshipEnd(fromEnd);
                }
            }

            if (toEndPosition != toEnd.getPosition()) {
                Position oldPosition = toEnd.getPosition();
                toEnd.setPosition(toEndPosition);
                if (toRelationshipView != null) {
                    toRelationshipView.moveRelationshipEnd(toEnd, oldPosition, toEndPosition);
                }
                // The from end might have to be fixed depending on what happened to the to end.
                if (fromRelationshipView != null) {
                    fromRelationshipView.updateRelationshipEnd(fromEnd);
                }
            } else {
                if (toRelationshipView != null) {
                    toRelationshipView.updateRelationshipEnd(toEnd);
                }
            }
        }
    }
    
    /**
     * Prepares the view for an upcoming update.
     */
    private void prepareUpdate() {
        update();
    }
    
    /**
     * Draws the visualizing of an end of a use case dependency.
     * 
     * @param end the end of the use case dependency to visualize
     */
    private void drawEnd(RamEnd<UseCase, UseCaseView> end) {
        float fromX = fromEnd.getLocation().getX();
        float fromY = fromEnd.getLocation().getY();
        float x = end.getLocation().getX();
        float y = end.getLocation().getY();

        if (arrowPolygon == null) {
            arrowPolygon = new ArrowPolygon(x, y, drawColor);
        }
        
        float angle = (float) Math.atan2(fromY - y, fromX - x);
        float angleToRotate = angle - currentArrowRotation;
        
        arrowPolygon.rotateZ(new Vector3D(x, y), (float) (angleToRotate * 180.0 / Math.PI));

        currentArrowRotation = angle;
        
        addChild(arrowPolygon);
        
        // displace arrowhead a little bit so it is nicely visible
        float xDisplacement = (float) (ARROWHEAD_OFFSET_FROM_USECASE * Math.cos(currentArrowRotation));
        float yDisplacement = (float) (ARROWHEAD_OFFSET_FROM_USECASE * Math.sin(currentArrowRotation));
        arrowPolygon.setPositionRelativeToParent(new Vector3D(x + xDisplacement, y + yDisplacement));
    }
    
    private void createTextIfNecessary() {
        if (textToDisplay == null) {
            textToDisplay = new RamTextComponent();
            if (type == UseCaseAssociationType.INCLUDE) {
                textToDisplay.setText(INCLUDE_TEXT);
            } else if (type == UseCaseAssociationType.EXTEND) {
                textToDisplay.setText(EXTEND_TEXT);
            }
            
            addChild(textToDisplay);
        }
    }
    
    /**
     * Moves the texts (multiplicity and role name) for the given end.
     *
     * @param ramEnd the end the texts to move for
     * @param roleName the view for the role name
     * @param lastPosition the last position the text was located at
     */
    private void moveTexts(RamEnd<UseCase, UseCaseView> ramEnd, RamTextComponent roleName,
            Position lastPosition) {
        RamEnd<UseCase, UseCaseView> opposite = ramEnd.getOpposite();
        moveRoleName(roleName, opposite.getLocation(), opposite.getPosition());        
    }

    /**
     * Rotates the association text depending on the position and into the given direction.
     */
    private void rotateTextFree() {
        float fromX = fromEnd.getLocation().getX();
        float fromY = fromEnd.getLocation().getY();
        float toX = toEnd.getLocation().getX();
        float toY = toEnd.getLocation().getY();

        float angle = (float) Math.atan2(fromY - toY, fromX - toX);
        
        if (angle < -Math.PI / 2) {
            angle = angle + (float) Math.PI;
        } else if (angle > Math.PI / 2) {
            angle = angle - (float) Math.PI;
        }
        float x = textToDisplay.getPosition(TransformSpace.RELATIVE_TO_PARENT).getX();
        float y = textToDisplay.getPosition(TransformSpace.RELATIVE_TO_PARENT).getY();
        
        float angleToRotate = angle - currentTextRotation;
        
        textToDisplay.rotateZ(new Vector3D(x, y), (float) (angleToRotate * SCALING_CONSTANT / Math.PI));
        
        currentTextRotation = angle;
    }

    /**
     * Moves the role name to the proper position.
     *
     * @param textView the view of the role name
     * @param currentPosition the current position of the role name
     * @param position the side of the association end on the attached actor view
     */
    @SuppressWarnings("static-method")
    private void moveRoleName(RamTextComponent textView, Vector3D currentPosition,
            Position position) {
        float x = currentPosition.getX();
        float y = currentPosition.getY();        

        float fromX = fromEnd.getLocation().getX();
        float fromY = fromEnd.getLocation().getY();
        float toX = toEnd.getLocation().getX();
        
        double angle = Math.atan2(fromY - y, fromX - x); 
        switch (position) {
            case BOTTOM:
                if (fromX - toX <= 0) {
                    textView.setAnchor(PositionAnchor.LOWER_RIGHT);
                } else {
                    textView.setAnchor(PositionAnchor.LOWER_LEFT);
                }
                x += TEXT_OFFSET * (float) Math.cos(angle) * Math.abs(Math.cos(angle));
                y += TEXT_OFFSET * (float) Math.sin(angle) * Math.abs(Math.cos(angle));
                break;
            case TOP:
                if (fromX - toX <= 0) {
                    textView.setAnchor(PositionAnchor.LOWER_RIGHT);
                } else {
                    textView.setAnchor(PositionAnchor.LOWER_LEFT);
                }
                break;
            case LEFT:
                textView.setAnchor(PositionAnchor.LOWER_RIGHT);
                x -= TEXT_OFFSET_FROM_ACTOR;
                break;
            case RIGHT:
                textView.setAnchor(PositionAnchor.LOWER_LEFT);
                x += TEXT_OFFSET_FROM_ACTOR;
                break;
        }

        textView.setPositionRelativeToParent(new Vector3D(x, y));
    }

    @Override
    public void updateLines() {
        RamRectangleComponent viewFrom = fromEnd.getComponentView();
        RamRectangleComponent viewTo = toEnd.getComponentView();

        // get the top left corner coordinates of the classes for all calculations
        Vector3D topLeftFrom = viewFrom.getPosition(TransformSpace.GLOBAL);
        Vector3D topLeftTo = viewTo.getPosition(TransformSpace.GLOBAL);

        float heightFrom = viewFrom.getHeightXY(TransformSpace.GLOBAL);
        float widthFrom = viewFrom.getWidthXY(TransformSpace.GLOBAL);
        float heightTo = viewTo.getHeightXY(TransformSpace.GLOBAL);
        float widthTo = viewTo.getWidthXY(TransformSpace.GLOBAL);

        // calculate the outer coordinates and center of each class
        float leftXFrom = topLeftFrom.getX();
        float rightXFrom = topLeftFrom.getX() + widthFrom;
        float topYFrom = topLeftFrom.getY();
        float bottomYFrom = topLeftFrom.getY() + heightFrom;
        float leftXTo = topLeftTo.getX();
        float rightXTo = topLeftTo.getX() + widthTo;
        float topYTo = topLeftTo.getY();
        float bottomYTo = topLeftTo.getY() + heightTo;

        Position fromEndPosition = null;
        Position toEndPosition = null;

        // depending on the positions of hte use cases,
        // determine the position of the ends
        if (rightXFrom < leftXTo) {
            // from use case is on the left side of to use case

            // from use case is above to use case
            if (bottomYFrom < topYTo) {
                fromEndPosition = Position.BOTTOM;
                toEndPosition = Position.TOP;
            } else if (bottomYTo < topYFrom) {
                // to use case is above from use case
                fromEndPosition = Position.TOP;
                toEndPosition = Position.BOTTOM;
            } else {
                // use cases are next to each other (neither above nor below)
                fromEndPosition = Position.RIGHT;
                toEndPosition = Position.LEFT;
            }
        } else if (rightXTo < leftXFrom) {
            // to use case is on the left side of from use case
            // from use case is above to use case
            if (bottomYFrom < topYTo) {
                fromEndPosition = Position.BOTTOM;
                toEndPosition = Position.TOP;
            } else if (bottomYTo < topYFrom) {
                // to use case is above from use case
                fromEndPosition = Position.TOP;
                toEndPosition = Position.BOTTOM;
            } else {
                // use cases are next each other (neither above nor below)
                fromEndPosition = Position.LEFT;
                toEndPosition = Position.RIGHT;
            }
        } else if (bottomYFrom < topYTo) {
            // use cases neither on the left nor on the right of each other
            // from use case is above to use case
            fromEndPosition = Position.BOTTOM;
            toEndPosition = Position.TOP;
        } else if (bottomYTo < topYFrom) {
            // use case neither on the left nor on the right of each other
            // to use case is above from use case
            fromEndPosition = Position.TOP;
            toEndPosition = Position.BOTTOM;
        } else {
            fromEndPosition = Position.OFFSCREEN;
            toEndPosition = Position.OFFSCREEN;
        }
        updateRelationshipEnds(viewFrom, viewTo, fromEndPosition, toEndPosition);
    }
    
}
