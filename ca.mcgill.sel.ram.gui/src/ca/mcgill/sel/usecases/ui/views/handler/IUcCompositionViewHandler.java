package ca.mcgill.sel.usecases.ui.views.handler;

import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.ram.ui.views.CompositionView;
import ca.mcgill.sel.ram.ui.views.handler.ICompositionViewHandler;

public interface IUcCompositionViewHandler extends ICompositionViewHandler {
    /**
     * Used to add a new actor mapping.
     * @param composition The composition.
     */
    void addActorMapping(COREModelComposition composition);
    
    /**
     * Used to add a new use case mapping.
     * @param composition The composition.
     */
    void addUseCaseMapping(COREModelComposition composition);
    
    /**
     * Loads the extended or reused use case model and displays it.
     * 
     * @param myCompositionView the composition
     */
    void showExternalModelOfComposition(CompositionView<IUcCompositionViewHandler> myCompositionView);
}
