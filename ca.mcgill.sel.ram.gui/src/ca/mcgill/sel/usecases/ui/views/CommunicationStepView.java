package ca.mcgill.sel.usecases.ui.views;

import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.usecases.Step;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.ui.utils.UcModelUtils;
import ca.mcgill.sel.usecases.ui.utils.UseCaseModelHandlerFactory;

public class CommunicationStepView extends StepView {   
    private ActorReferenceTextView messageField;
    private TextView directionField;
    
    protected CommunicationStepView(UseCaseDiagramView useCaseDiagramView, Step represented, boolean readonly) {
        super(useCaseDiagramView, represented, readonly);   
    }

    @Override
    protected void buildStepView() {        
        messageField = new ActorReferenceTextView(getStep().getStepDescription(), true);
        
        messageField.setPlaceholderText(Strings.PH_ENTER_MESSAGE);
        messageField.setMaximumWidth(UcModelUtils.getMaxComponentWidth());
        messageField.setBufferSize(Cardinal.WEST, 0);
        messageField.enableKeyboard(!readonly);
        
        addChild(messageField);
       
        messageField.setHandler(UseCaseModelHandlerFactory.INSTANCE.getActorReferenceTextViewHandler());
        
        directionField = new TextView(represented, UcPackage.Literals.COMMUNICATION_STEP__DIRECTION);  
        addChild(directionField);
        if (!readonly) {
            directionField.setHandler(UseCaseModelHandlerFactory.INSTANCE.getTextViewHandler());    
        }        
    }
    
    @Override
    public void destroy() {
        messageField.destroy();
        directionField.destroy();
        super.destroy();
    }
}
