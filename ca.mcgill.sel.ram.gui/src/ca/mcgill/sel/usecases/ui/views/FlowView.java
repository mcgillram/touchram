package ca.mcgill.sel.usecases.ui.views;

import java.util.HashMap;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.edit.provider.INotifyChangedListener;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamButton;
import ca.mcgill.sel.ram.ui.components.RamImageComponent;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.ram.ui.layouts.HorizontalLayout;
import ca.mcgill.sel.ram.ui.layouts.VerticalLayout;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.Fonts;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.usecases.AnythingStep;
import ca.mcgill.sel.usecases.CommunicationStep;
import ca.mcgill.sel.usecases.Condition;
import ca.mcgill.sel.usecases.ContextStep;
import ca.mcgill.sel.usecases.Flow;
import ca.mcgill.sel.usecases.Step;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.UseCase;
import ca.mcgill.sel.usecases.UseCaseReferenceStep;
import ca.mcgill.sel.usecases.ui.utils.UcModelUtils;
import ca.mcgill.sel.usecases.ui.utils.UseCaseModelHandlerFactory;
import ca.mcgill.sel.usecases.ui.views.handler.IFlowViewHandler;
import ca.mcgill.sel.usecases.util.AlternateFlowComparator;
import ca.mcgill.sel.usecases.util.UcModelUtil;
import ca.mcgill.sel.usecases.util.UseCaseAssociationType;
import ca.mcgill.sel.usecases.util.UseCaseTextUtils;

public class FlowView extends BaseView<IFlowViewHandler> 
    implements INotifyChangedListener {   
    /**
     * The event to add a new step to a flow.
     */
    public static final String ACTION_STEP_ADD = "usecase.flow.steps.add";
    
    /**
     * The event to delete a flow.
     */
    public static final String ACTION_FLOW_DELETE = "usecase.flow.delete";
    
    /**
     * The event to enable the postcondition field for a flow.
     */
    public static final String ACTION_FLOW_ENABLE_POSTCONDITION = "usecase.flow.postcondition.enabled";
    
    /**
     * The event to enable the extensions section for a flow.
     */
    public static final String ACTION_FLOW_ENABLE_EXTENSIONS = "usecase.flow.extensions.enabled";
    
    /**
     * The event to add an extension.
     */
    public static final String ACTION_ALTERNATE_FLOW_ADD = "usecase.extensions.add";

    private static final int FLOW_PADDING = 15;
       
    private Flow flow;
    private int depth;
    
    private RamRectangleComponent headerContainer;
    private RamTextComponent stepNumberLabel;
    private TextView headerLabel;
    
    private RamRectangleComponent buttonsContainer;
    private RamButton addStepButton;
    private RamButton deleteFlowButton;
    private RamButton enablePostconditionButton;
    private RamButton enableExtensionsButton;

    private RamRectangleComponent stepsContainer;
    private RamRectangleComponent alternateFlowsContainer;

    private RamRectangleComponent conclusionContainer;
    private TextView conclusionField;
    
    private RamRectangleComponent postconditionContainer;
    private RamTextComponent postconditionLabel;
    private TextView postconditionField;
    
    private RamRectangleComponent extensionsHeaderContainer;
    private RamTextComponent extensionsHeaderLabel;
    private RamButton addExtensionButton;
    private RamRectangleComponent flowsContainer;
    
    private HashMap<Step, StepView> stepToViewMap;
    private HashMap<Flow, FlowView> alternateFlowToViewMap;
    
    private AlternateFlowComparator comparator;
    private SortedSet<Flow> sortedAlternateFlows;
    
    private boolean readonly;
    
    public FlowView(UseCaseDiagramView diagramView, UseCase represented, 
            Flow flow, int depth) {
        super(diagramView, represented, null);
        
        this.flow = flow;
        this.depth = depth;
        this.readonly = represented.isAbstract();
        this.stepToViewMap = new HashMap<Step, StepView>();
        this.alternateFlowToViewMap = new HashMap<Flow, FlowView>();
        this.comparator = new AlternateFlowComparator(flow);
        this.sortedAlternateFlows = new TreeSet<Flow>(comparator);

        setNoStroke(true);
        setNoFill(true);        
        
        setMinimumWidth(UcModelUtils.getMaxComponentWidth());
        setLayout(new VerticalLayout());
        
        
        EMFEditUtil.addListenerFor(flow, this);
        if (flow.getConclusionStep() != null) {
            EMFEditUtil.addListenerFor(flow.getConclusionStep(), this);    
        }
        
        build();
    }
    
    public UseCase getUseCase() {
        return (UseCase) this.represented;
    }
    
    public Flow getFlow() {
        return this.flow;
    }
    
    @Override
    public void notifyChanged(Notification notification) {
        if (notification.getNotifier() == flow) {
            if (notification.getFeature() == UcPackage.Literals.FLOW__STEPS) {
                Step step = null;
                switch (notification.getEventType()) {
                    case Notification.ADD:
                        step = (Step) notification.getNewValue();
                        addStep(step, notification.getPosition());
                        
                        if (step instanceof UseCaseReferenceStep) {
                            UseCaseReferenceStep refStep = (UseCaseReferenceStep) step;
                            UseCaseAssociationType associationType = UseCaseAssociationType.INCLUDE;
                            useCaseDiagramView.addUseCaseAssociationView(
                                    getUseCase(), refStep.getUsecase(), associationType);
                        }
                        break;
                    case Notification.REMOVE:
                        step = (Step) notification.getOldValue();
                        removeStep(step);
                        break;
                    case Notification.MOVE:
                        step = (Step) notification.getNewValue();
                        setStepPosition(step, notification.getPosition());
                        break;
                }
                
                resetIndexText();                    
                updateAlternateFlowOrder();
            } else if (notification.getFeature() == UcPackage.Literals.FLOW__REFERENCED_STEPS) {
                resetIndexText();
            } else if (notification.getFeature() == UcPackage.Literals.FLOW__ALTERNATE_FLOWS) {
                Flow f = null;
                switch (notification.getEventType()) {
                    case Notification.ADD:
                        f = (Flow) notification.getNewValue();
                        sortedAlternateFlows.add(f);
                        addAlternateFlow(f, notification.getPosition());
                        break;
                    case Notification.REMOVE:
                        f = (Flow) notification.getOldValue();
                        removeAlternateFlow(f);
                        break;
                    case Notification.MOVE:
                        f = (Flow) notification.getNewValue();
                        setFlowPosition(f, notification.getPosition());
                        break;
                }
                
                // The lettering may have changed in any sub flows
                for (FlowView view : alternateFlowToViewMap.values()) {
                    view.resetIndexText();
                }
            } else if (notification.getFeature() == UcPackage.Literals.FLOW__CONCLUSION_TYPE
                    || notification.getFeature() == UcPackage.Literals.FLOW__CONCLUSION_STEP) {
                conclusionField.setText(UseCaseTextUtils.getConclusionTypeText(flow));
            } else if (notification.getFeature() == UcPackage.Literals.FLOW__POST_CONDITION) {
                Condition newValue = (Condition) notification.getNewValue();
                if (newValue != null) {
                    enablePostcondition();
                } else {
                    if (alternateFlowsContainer != null) {
                        alternateFlowsContainer.removeChild(postconditionContainer);
                    } else {
                        removeChild(postconditionContainer);    
                    }
                    buttonsContainer.addChild(enablePostconditionButton);
                }                
            }
        }
    }    
    
    public void resetIndexText() {
        updateStepLabelNumbers();
        if (!flow.isMainSuccessScenario()) {
            stepNumberLabel.setText(UseCaseTextUtils.getFlowStepNumberText(flow));
            conclusionField.setText(UseCaseTextUtils.getConclusionTypeText(flow));    
        }        
        
        for (FlowView view : alternateFlowToViewMap.values()) {
            view.resetIndexText();            
        }        
    }    
    
    public void enableExtensions() {
        buildExtensions();
        buttonsContainer.removeChild(enableExtensionsButton);
    }    
    
    private void updateAlternateFlowOrder() {
        TreeSet<Flow> newSortOrder = new TreeSet<Flow>(comparator);
        newSortOrder.addAll(flow.getAlternateFlows());
        
        int position = 0;
        // Iterating over the elements in the set
        Iterator<Flow> it = newSortOrder.iterator();
        for (Flow f : sortedAlternateFlows) {
            Flow currentFlow = it.next();
            if (currentFlow == f) {
                // The ordering hasn't changed yet
                position++;
                continue;
            } else {
                // All remaining flows have to be changed
                setFlowPosition(f, position);
                while (it.hasNext()) {
                    Flow element = (Flow) it.next();
                    setFlowPosition(element, position);
                    position++;
                }
                break;
            }
        }        
        
        sortedAlternateFlows = newSortOrder;
    }
    
    
    private void enablePostcondition() {
        buildPostconditionField();
        buttonsContainer.removeChild(enablePostconditionButton);
    }
    
    private void build() {       
        boolean extensionsEnabled = flow.isMainSuccessScenario() 
                || (flow.getAlternateFlows() != null && flow.getAlternateFlows().size() > 0);
        boolean postConditionEnabled = flow.getPostCondition() != null;
        
        headerContainer = new RamRectangleComponent(new HorizontalLayout());
        headerContainer.setNoFill(true);
        headerContainer.setNoStroke(true); 
        
        if (flow.isMainSuccessScenario() || readonly) {
            headerLabel = new TextView(flow, UcPackage.Literals.NAMED_ELEMENT__NAME);
            headerLabel.setPlaceholderText(Strings.PH_ENTER_USE_CASE_NAME);
            headerLabel.setFont(Fonts.FONT_CLASS_NAME_ITALIC);
            headerLabel.enableKeyboard(false);
            headerLabel.setMaximumWidth(UcModelUtils.getMaxComponentWidth());
            headerContainer.addChild(headerLabel); 
        } else {
            stepNumberLabel = new RamTextComponent();
            
            String stepNumberLabelText = UseCaseTextUtils.getFlowStepNumberText(flow);
            if (stepNumberLabelText != null) {
                stepNumberLabel.setText(stepNumberLabelText);
                headerContainer.addChild(stepNumberLabel);
            }
            
            Step preconditionStep = flow.getPreconditionStep();
            if (preconditionStep != null) {
                headerLabel = new ActorReferenceTextView(preconditionStep.getStepDescription());
                headerLabel.setPlaceholderText(Strings.PH_ENTER_PRECONDITION);
                headerLabel.setMaximumWidth(UcModelUtils.getMaxComponentWidth());
                headerLabel.setBufferSize(Cardinal.WEST, 0);
                headerContainer.addChild(headerLabel);
                headerLabel.setHandler(UseCaseModelHandlerFactory.INSTANCE.getActorReferenceTextViewHandler());
                
                if (preconditionStep instanceof CommunicationStep) {
                    TextView directionField = new TextView(preconditionStep, 
                            UcPackage.Literals.COMMUNICATION_STEP__DIRECTION);  
                    headerContainer.addChild(directionField);
                }
            }
        }       
        
        buttonsContainer = new RamRectangleComponent(new HorizontalLayout());
        buttonsContainer.setNoStroke(true);
        buttonsContainer.setNoFill(true);
        buttonsContainer.setBufferSize(Cardinal.NORTH, 5);
        buttonsContainer.setBufferSize(Cardinal.EAST, 5);
        
        if (!readonly) {
            RamImageComponent addImage = new RamImageComponent(Icons.ICON_ADD, Colors.ICON_STRUCT_DEFAULT_COLOR,
                    ICON_SIZE, ICON_SIZE);
            addStepButton = new RamButton(addImage);
            addStepButton.setActionCommand(ACTION_STEP_ADD);
            addStepButton.addActionListener(UseCaseModelHandlerFactory.INSTANCE.getFlowViewHandler());
           
            buttonsContainer.addChild(addStepButton);        
            if (!postConditionEnabled) {
                RamImageComponent enablePostconditionImage = new RamImageComponent(Icons.ICON_PARAMETER_MAPPING_ADD, 
                        Colors.ICON_STRUCT_DEFAULT_COLOR, ICON_SIZE, ICON_SIZE);
                enablePostconditionButton = new RamButton(enablePostconditionImage);
                enablePostconditionButton.setActionCommand(ACTION_FLOW_ENABLE_POSTCONDITION);
                enablePostconditionButton.addActionListener(UseCaseModelHandlerFactory.INSTANCE.getFlowViewHandler());
                
                buttonsContainer.addChild(enablePostconditionButton);
            }
            
            if (!extensionsEnabled) {
                RamImageComponent enableExtensionsImage = new RamImageComponent(Icons.ICON_ENUM_MAPPING_ADD, 
                        Colors.ICON_STRUCT_DEFAULT_COLOR, ICON_SIZE, ICON_SIZE);
                enableExtensionsButton = new RamButton(enableExtensionsImage);
                enableExtensionsButton.setActionCommand(ACTION_FLOW_ENABLE_EXTENSIONS);
                enableExtensionsButton.addActionListener(UseCaseModelHandlerFactory.INSTANCE.getFlowViewHandler());
                
                buttonsContainer.addChild(enableExtensionsButton);
            }
            
            if (!flow.isMainSuccessScenario()) {
                RamImageComponent deleteImage = new RamImageComponent(Icons.ICON_DELETE, Colors.ICON_DELETE_COLOR,
                        ICON_SIZE, ICON_SIZE);
                deleteFlowButton = new RamButton(deleteImage);
                deleteFlowButton.setActionCommand(ACTION_FLOW_DELETE);
                deleteFlowButton.addActionListener(UseCaseModelHandlerFactory.INSTANCE.getFlowViewHandler());
                
                buttonsContainer.addChild(deleteFlowButton);
            }
        }        
        
        headerContainer.addChild(buttonsContainer);
        addChild(headerContainer);
        
        stepsContainer = new RamRectangleComponent(new VerticalLayout());
        stepsContainer.setNoFill(true);
        stepsContainer.setNoStroke(true);
        
        for (int i = 0; i < flow.getSteps().size(); i++) {
            Step step = flow.getSteps().get(i);
            addStep(step, i);
        }
        
        addChild(stepsContainer);
        
        if (!flow.isMainSuccessScenario() && !readonly) {
            conclusionContainer = new RamRectangleComponent(new HorizontalLayout());
            conclusionContainer.setNoFill(true);
            conclusionContainer.setNoStroke(true);
            
            conclusionField = new TextView(flow, UcPackage.Literals.FLOW__CONCLUSION_TYPE) {
                @Override
                public void handleNotification(Notification notification) {
                    // Do nothing; other places will handle this.
                }
            };
            
            conclusionField.setBufferSize(Cardinal.WEST, (depth + 1) * FLOW_PADDING);
            conclusionField.setText(UseCaseTextUtils.getConclusionTypeText(flow));
            conclusionContainer.addChild(conclusionField);
            conclusionField.setHandler(UseCaseModelHandlerFactory.INSTANCE.getFlowConclusionSelectorHandler());
            
            addChild(conclusionContainer);   
        }
        
        if (postConditionEnabled) {
            buildPostconditionField();
        }
        
        if (extensionsEnabled) {
            buildExtensions();    
        }        
    }
    
    private void buildExtensions() {
        alternateFlowsContainer = new RamRectangleComponent(new VerticalLayout());
        alternateFlowsContainer.setNoFill(true);
        alternateFlowsContainer.setNoStroke(true);
        
        extensionsHeaderContainer = new RamRectangleComponent(new HorizontalLayout());
        extensionsHeaderContainer.setNoFill(true);
        extensionsHeaderContainer.setNoStroke(true);
                
        extensionsHeaderLabel = new RamTextComponent(Fonts.FONT_CLASS_NAME_ITALIC);
        extensionsHeaderLabel.setText(Strings.LABEL_EXTENSIONS);
        extensionsHeaderContainer.addChild(extensionsHeaderLabel);
        
        RamImageComponent addImage = new RamImageComponent(Icons.ICON_ADD, Colors.ICON_STRUCT_DEFAULT_COLOR,
                ICON_SIZE, ICON_SIZE);
        addExtensionButton = new RamButton(addImage);
        addExtensionButton.setActionCommand(ACTION_ALTERNATE_FLOW_ADD);
        addExtensionButton.addActionListener(UseCaseModelHandlerFactory.INSTANCE.getFlowViewHandler());
        addExtensionButton.setBufferSize(Cardinal.NORTH, 5);
        addExtensionButton.setBufferSize(Cardinal.EAST, 5);
        
        extensionsHeaderContainer.addChild(addExtensionButton);
        
        alternateFlowsContainer.addChild(extensionsHeaderContainer);
        
        flowsContainer = new RamRectangleComponent(new VerticalLayout());
        flowsContainer.setNoFill(true);
        flowsContainer.setNoStroke(true);
        flowsContainer.setBufferSize(Cardinal.WEST, (depth + 1) * FLOW_PADDING);
        
        sortedAlternateFlows.addAll(flow.getAlternateFlows());
        int i = 0;
        // Iterating over the elements in the set
        Iterator<Flow> it = sortedAlternateFlows.iterator();

        while (it.hasNext()) {
            Flow element = (Flow) it.next();
            addAlternateFlow(element, i);
            i++;
        }
        
        alternateFlowsContainer.addChild(flowsContainer);
        
        addChild(alternateFlowsContainer);   
    }
    
    private void addStep(Step step, int stepIndex) {
        StepView view;
        if (step instanceof CommunicationStep) {
            view = new CommunicationStepView(useCaseDiagramView, step, readonly);
        } else if (step instanceof ContextStep) {
            view = new ContextStepView(useCaseDiagramView, step, readonly);
        } else if (step instanceof UseCaseReferenceStep) {
            view = new UseCaseReferenceStepView(useCaseDiagramView, step, readonly);                    
        } else if (step instanceof AnythingStep) {
            view = new AnythingStepView(useCaseDiagramView, step, readonly);
        } else {
            return;
        }
        
        stepToViewMap.put(step, view);
        stepsContainer.addChild(stepIndex, view);
        view.setHandler(UseCaseModelHandlerFactory.INSTANCE.getStepViewHandler());
    }
    
    private void removeStep(Step step) {
        StepView view = stepToViewMap.remove(step);
        stepsContainer.removeChild(view);
        view.destroy();
        
        if (step instanceof UseCaseReferenceStep) {
            UseCaseReferenceStep refStep = (UseCaseReferenceStep) step;
            UseCaseAssociationType type = UseCaseAssociationType.INCLUDE;
            
            if (!UcModelUtil.useCaseLinkExists(getUseCase(), refStep.getUsecase(), type)) {
                useCaseDiagramView.removeUseCaseAssociationView(getUseCase(), refStep.getUsecase(), type);    
            }            
        }
    }
    
    private void addAlternateFlow(Flow f, int position) {
        UseCase useCase = EMFModelUtil.getRootContainerOfType(f, UcPackage.Literals.USE_CASE);
        FlowView view = new FlowView(useCaseDiagramView, useCase, f, depth + 1);
        alternateFlowToViewMap.put(f, view);
        flowsContainer.addChild(position, view);
        view.setHandler(UseCaseModelHandlerFactory.INSTANCE.getFlowViewHandler());
    }
    
    private void removeAlternateFlow(Flow f) {
        FlowView view = alternateFlowToViewMap.remove(f);
        sortedAlternateFlows.remove(f);
        removeChild(view);
        view.destroy();
    }
    
    private void setStepPosition(Step step, int position) {
        RamApp.getApplication().invokeLater(new Runnable() {
            @Override
            public void run() {
                StepView stepView = stepToViewMap.get(step);

                stepsContainer.removeChild(stepView);
                stepsContainer.addChild(position, stepView);
            }
        });
    }
    
    private void setFlowPosition(Flow f, int position) {
        RamApp.getApplication().invokeLater(new Runnable() {
            @Override
            public void run() {
                FlowView flowView = alternateFlowToViewMap.get(f);

                flowsContainer.removeChild(flowView);
                flowsContainer.addChild(position, flowView);
            }
        });
    }
    
    private void updateStepLabelNumbers() {
        for (Step step : flow.getSteps()) {
            StepView view = stepToViewMap.get(step);
            view.setStepNumber(UseCaseTextUtils.getStepNumber(step));
        }        
    }
    
    private void buildPostconditionField() {
        Condition postCondition = flow.getPostCondition();
        postconditionContainer = new RamRectangleComponent(new HorizontalLayout());
        postconditionContainer.setNoFill(true);
        postconditionContainer.setNoStroke(true);
        
        postconditionLabel = new RamTextComponent(Fonts.FONT_CLASS_NAME_ITALIC);
        postconditionLabel.setText(Strings.LABEL_POSTCONDITION);
        postconditionLabel.setBufferSize(Cardinal.EAST, 30);
        postconditionContainer.addChild(postconditionLabel);
        
        postconditionField = new TextView(postCondition, UcPackage.Literals.CONDITION__TEXT);
        postconditionField.setPlaceholderText(Strings.PH_ENTER_POSTCONDITION);
        postconditionField.setMaximumWidth(UcModelUtils.getMaxComponentWidth());
        postconditionField.setBufferSize(Cardinal.WEST, 0);
        postconditionContainer.addChild(postconditionField);
        postconditionField.setHandler(UseCaseModelHandlerFactory.INSTANCE.getTextViewHandler());
        
        if (alternateFlowsContainer != null) {
            alternateFlowsContainer.addChild(0, postconditionContainer);
        } else {
            addChild(postconditionContainer);    
        }        
    }
    
    @Override
    public void destroy() {
        EMFEditUtil.removeListenerFor(flow, this);
        for (Step step : flow.getSteps()) {
            EMFEditUtil.removeListenerFor(step, this);
        }
        
        headerContainer.destroy();
        
        if (postconditionContainer != null) {
            postconditionContainer.destroy();    
        }        
        
        for (StepView view : stepToViewMap.values()) {
            view.destroy();
        }
        
        stepsContainer.destroy();
        
        for (FlowView view : alternateFlowToViewMap.values()) {
            view.destroy();
        }
        
        if (conclusionContainer != null) {
            conclusionContainer.destroy();    
        }
        
        if (alternateFlowsContainer != null) {
            alternateFlowsContainer.destroy();    
        }        
        
        super.destroy();
    }
}
