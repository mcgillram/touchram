package ca.mcgill.sel.usecases.ui.views.handler.impl;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.mt4j.components.TransformSpace;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.core.COREPartialityType;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamSelectorComponent;
import ca.mcgill.sel.ram.ui.components.listeners.AbstractDefaultRamSelectorListener;
import ca.mcgill.sel.ram.ui.components.menu.RamLinkedMenu;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.usecases.Actor;
import ca.mcgill.sel.usecases.UseCase;
import ca.mcgill.sel.usecases.language.controller.UseCaseController;
import ca.mcgill.sel.usecases.language.controller.UseCaseControllerFactory;
import ca.mcgill.sel.usecases.ui.scenes.DisplayUseCaseModelScene;
import ca.mcgill.sel.usecases.ui.views.BaseView;
import ca.mcgill.sel.usecases.ui.views.UseCaseView;
import ca.mcgill.sel.usecases.ui.views.handler.IUseCaseViewHandler;

public class UseCaseViewHandler extends BaseViewHandler implements IUseCaseViewHandler {

    private static final String ACTION_USE_CASE_GOTO_DETAILVIEW = "view.usecase.detail";
    private static final String ACTION_USE_CASE_DELETE_ASSOCIATION = "view.usecase.association.delete";
    private static final String ACTION_ABSTRACT = "view.usecase.abstract";
    private static final String ACTION_PUBLIC_PARTIAL = "view.usecase.public_partial";
    private static final String ACTION_CONCERN_PARTIAL = "view.usecase.concern_partial";
    private static final String ACTION_NOT_PARTIAL = "view.usecase.not_partial";    
    private static final String SUBMENU_USE_CASE_CHARACTERISTICS = "sub.usecase.partiality";
    
    @Override
    public void removeRepresented(BaseView<?> baseView) {
        UseCase useCase = (UseCase) baseView.getRepresented();
        UseCaseControllerFactory.INSTANCE.getUseCaseDiagramController().removeUseCase(useCase);
    }
    
    @Override
    public void initMenu(RamLinkedMenu menu, TapAndHoldEvent event) {
        super.initMenu(menu, event);
        menu.addAction(Strings.MENU_GOTO_USECASEDETAIL, Strings.MENU_ENABLE_USECASEDETAIL, 
                Icons.ICON_MENU_USE_CASE_DETAIL, Icons.ICON_MENU_ADD, ACTION_USE_CASE_GOTO_DETAILVIEW,
                this, true, false);
        menu.addAction(Strings.MENU_DELETE_ASSOCIATION,  Icons.ICON_MENU_CLEAR_SELECTION, 
                ACTION_USE_CASE_DELETE_ASSOCIATION,
                this, true);
        menu.addAction(Strings.MENU_ABSTRACT, Strings.MENU_NOT_ABSTRACT, Icons.ICON_MENU_ABSTRACT,
                Icons.ICON_MENU_NOT_ABSTRACT, ACTION_ABSTRACT, this, true, false);
        menu.addSubMenu(1, SUBMENU_USE_CASE_CHARACTERISTICS);
        menu.addAction(Strings.MENU_PUBLIC_PARTIAL, Icons.ICON_MENU_PUBLIC_PARTIAL, ACTION_PUBLIC_PARTIAL, this,
                SUBMENU_USE_CASE_CHARACTERISTICS, true);
        menu.addAction(Strings.MENU_CONCERN_PARTIAL, Icons.ICON_MENU_CONCERN_PARTIAL, ACTION_CONCERN_PARTIAL,
                this, SUBMENU_USE_CASE_CHARACTERISTICS, true);
        menu.addAction(Strings.MENU_NO_PARTIAL, Icons.ICON_MENU_NOT_PARTIAL, ACTION_NOT_PARTIAL, this,
                SUBMENU_USE_CASE_CHARACTERISTICS, true);
        
        updateButtons(menu);
    }
    
    @Override
    public void actionPerformed(ActionEvent event) {
        String actionCommand = event.getActionCommand();
        RamRectangleComponent pressedButton = (RamRectangleComponent) event.getTarget();
        RamLinkedMenu linkedMenu = (RamLinkedMenu) pressedButton.getParentOfType(RamLinkedMenu.class);
        UseCaseView useCaseView = (UseCaseView) linkedMenu.getLinkedView();
        UseCase useCase = useCaseView.getUseCase();
        
        if (ACTION_USE_CASE_GOTO_DETAILVIEW.equals(actionCommand)) {
            goToDetailView(useCase);
        } else if (ACTION_USE_CASE_DELETE_ASSOCIATION.equals(actionCommand)) {
            Vector3D position = linkedMenu.getPosition(TransformSpace.GLOBAL); 
            linkedMenu.destroy();
            deleteActorAssociation(useCase, position);
        } else if (ACTION_ABSTRACT.equals(actionCommand)) {
            switchToAbstract(useCase);
        } else if (ACTION_PUBLIC_PARTIAL.equals(actionCommand)) {
            switchPartiality(useCase, COREPartialityType.PUBLIC);
        } else if (ACTION_CONCERN_PARTIAL.equals(actionCommand)) {
            switchPartiality(useCase, COREPartialityType.CONCERN);
        } else if (ACTION_NOT_PARTIAL.equals(actionCommand)) {
            switchPartiality(useCase, COREPartialityType.NONE);
        } else {
            super.actionPerformed(event);
        }
    }
    
    @Override
    public void updateMenu(RamLinkedMenu menu, Notification notification) {
        if (notification.getEventType() == Notification.SET
                || notification.getEventType() == Notification.UNSET
                || notification.getEventType() == Notification.ADD
                || notification.getEventType() == Notification.REMOVE) {
            updateButtons(menu);
        }
        super.updateMenu(menu, notification);
    }

    @SuppressWarnings("static-method")
    private void goToDetailView(UseCase useCase) {
        final DisplayUseCaseModelScene scene = (DisplayUseCaseModelScene) RamApp.getActiveScene();
        
        if (useCase.getMainSuccessScenario() == null) {
            UseCaseControllerFactory.INSTANCE.getUseCaseDiagramController().initializeUseCase(useCase);
        } else {
            scene.showUseCaseDetail(useCase);    
        }        
    }
    
    @SuppressWarnings("static-method")
    private void deleteActorAssociation(UseCase useCase, Vector3D menuPosition) {
        List<Actor> availableActors = new ArrayList<Actor>();
        
        availableActors.addAll(useCase.getPrimaryActors());
        availableActors.addAll(useCase.getSecondaryActors());
        
        RamSelectorComponent<Actor> selector = 
                new RamSelectorComponent<Actor>(availableActors);
        
        RamApp.getActiveScene().addComponent(selector, menuPosition);
        
        selector.registerListener(new AbstractDefaultRamSelectorListener<Actor>() {

            @Override
            public void elementSelected(RamSelectorComponent<Actor> selector, Actor element) {
                UseCaseController controller = UseCaseControllerFactory.INSTANCE.getUseCaseController();
                if (useCase.getPrimaryActors().contains(element)) {
                    controller.removePrimaryActor(element, useCase);
                } else {
                    controller.removeSecondaryActor(element, useCase);
                }
                
                selector.destroy();
            }
        });        
    }
    
    @SuppressWarnings("static-method")
    private void switchToAbstract(UseCase useCase) {
        UseCaseControllerFactory.INSTANCE.getUseCaseController().switchAbstract(useCase);
    }
    
    @SuppressWarnings("static-method")
    private void updateButtons(RamLinkedMenu menu) {
        UseCase useCase = (UseCase) menu.geteObject();
        menu.toggleAction(useCase.isAbstract(), ACTION_ABSTRACT);
        menu.toggleAction(useCase.getMainSuccessScenario() == null, ACTION_USE_CASE_GOTO_DETAILVIEW);
        menu.enableAction(!useCase.getPartiality().equals(COREPartialityType.NONE), ACTION_NOT_PARTIAL);
        menu.enableAction(!useCase.getPartiality().equals(COREPartialityType.CONCERN), ACTION_CONCERN_PARTIAL);
        menu.enableAction(!useCase.getPartiality().equals(COREPartialityType.PUBLIC), ACTION_PUBLIC_PARTIAL);
    }
    
    @SuppressWarnings("static-method")
    private void switchPartiality(UseCase useCase, COREPartialityType type) {
        UseCaseController controller = UseCaseControllerFactory.INSTANCE.getUseCaseController();
        controller.changeUseCasePartiality(useCase, type);
    }
}
