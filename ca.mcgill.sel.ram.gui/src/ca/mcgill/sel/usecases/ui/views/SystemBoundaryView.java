package ca.mcgill.sel.usecases.ui.views;

import org.mt4j.input.gestureAction.TapAndHoldVisualizer;
import org.mt4j.input.inputProcessors.IGestureEventListener;
import org.mt4j.input.inputProcessors.componentProcessors.panProcessor.PanProcessorTwoFingers;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldProcessor;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapProcessor;
import org.mt4j.input.inputProcessors.componentProcessors.unistrokeProcessor.UnistrokeUtils.Direction;
import org.mt4j.input.inputProcessors.componentProcessors.unistrokeProcessor.UnistrokeUtils.UnistrokeGesture;
import org.mt4j.input.inputProcessors.componentProcessors.zoomProcessor.ZoomProcessor;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent.Alignment;
import ca.mcgill.sel.ram.ui.events.MouseWheelProcessor;
import ca.mcgill.sel.ram.ui.events.RightClickDragProcessor;
import ca.mcgill.sel.ram.ui.events.UnistrokeProcessorLeftClick;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.GUIConstants;
import ca.mcgill.sel.ram.ui.views.AbstractView;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.ITextViewHandler;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.UseCaseModel;
import ca.mcgill.sel.usecases.ui.utils.UseCaseModelHandlerFactory;
import ca.mcgill.sel.usecases.ui.views.handler.ISystemBoundaryViewHandler;

public class SystemBoundaryView extends AbstractView<ISystemBoundaryViewHandler> {
    private static final float SYSTEM_BOX_PADDING_X = 10.0f;
    private static final float SYSTEM_BOX_PADDING_Y = 30.0f;
    
    private UseCaseModel ucm;
    
    private RamRectangleComponent labelContainer;
    private TextView labelComponent;
        
    public SystemBoundaryView(UseCaseModel ucm, float width, float height) {
        super(width, height);
        this.ucm = ucm;
        
        setNoStroke(false);
        setStrokeColor(Colors.DEFAULT_ELEMENT_STROKE_COLOR);
        setNoFill(false);
        setFillColor(Colors.DEFAULT_ELEMENT_FILL_COLOR);
                
        labelContainer = new RamRectangleComponent();
        labelContainer.setNoFill(true);
        labelContainer.setNoStroke(false);
        labelContainer.setStrokeColor(Colors.TRANSPARENT_DARK_GREY);
                        
        labelComponent = new TextView(this.ucm, UcPackage.Literals.USE_CASE_MODEL__SYSTEM_NAME);
        labelComponent.setAlignment(Alignment.CENTER_ALIGN);
        labelContainer.addChild(labelComponent);
        ITextViewHandler handler = UseCaseModelHandlerFactory.INSTANCE.getTextViewHandler();
        labelComponent.setHandler(handler);
        labelComponent.registerTapProcessor(handler);
        addChild(labelContainer);
    }
    
    @Override
    protected void registerGestureListeners(IGestureEventListener listener) {
        super.registerGestureListeners(listener);

        addGestureListener(TapProcessor.class, listener);
        addGestureListener(TapAndHoldProcessor.class, listener);
        addGestureListener(TapAndHoldProcessor.class, new TapAndHoldVisualizer(RamApp.getApplication(), getParent()));
    }

    @Override
    protected void registerInputProcessors() {
        registerInputProcessor(new TapProcessor(RamApp.getApplication(), GUIConstants.TAP_MAX_FINGER_UP, false,
                GUIConstants.TAP_DOUBLE_TAP_TIME));
        registerInputProcessor(new TapAndHoldProcessor(RamApp.getApplication(), GUIConstants.TAP_AND_HOLD_DURATION));
        registerInputProcessor(new PanProcessorTwoFingers(RamApp.getApplication()));
        registerInputProcessor(new RightClickDragProcessor(RamApp.getApplication()));
        registerInputProcessor(new ZoomProcessor(RamApp.getApplication()));
        registerInputProcessor(new MouseWheelProcessor(RamApp.getApplication()));

        UnistrokeProcessorLeftClick up = new UnistrokeProcessorLeftClick(RamApp.getApplication());
        up.addTemplate(UnistrokeGesture.CIRCLE, Direction.CLOCKWISE);
        up.addTemplate(UnistrokeGesture.CIRCLE, Direction.COUNTERCLOCKWISE);

        registerInputProcessor(up);
    }
    
    /**
     * Do any additional movements required to resize the system box.
     * @param position The position of the top left of the box.
     * @param width The width of the box.
     * @param height The height of the box.
     */
    public void resize(Vector3D position, float width, float height) {
        this.setPositionRelativeToParent(
                new Vector3D(position.x - SYSTEM_BOX_PADDING_X, position.y - SYSTEM_BOX_PADDING_Y));
        this.setWidthLocal(width + 2 * SYSTEM_BOX_PADDING_X);
        this.setHeightLocal(height + 2 * SYSTEM_BOX_PADDING_Y);
        labelComponent.setWidthLocal(this.getWidth());
    }
}
