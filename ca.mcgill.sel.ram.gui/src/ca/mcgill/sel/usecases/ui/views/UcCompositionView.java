package ca.mcgill.sel.usecases.ui.views;

import ca.mcgill.sel.core.COREMapping;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.views.CompositionContainerView;
import ca.mcgill.sel.ram.ui.views.CompositionTitleView;
import ca.mcgill.sel.ram.ui.views.CompositionView;
import ca.mcgill.sel.ram.ui.views.handler.HandlerFactory;
import ca.mcgill.sel.usecases.ActorMapping;
import ca.mcgill.sel.usecases.UseCaseMapping;
import ca.mcgill.sel.usecases.ui.views.handler.IUcCompositionViewHandler;

public class UcCompositionView extends CompositionView<IUcCompositionViewHandler> {
    /**
     * Creates a new composition view.
     * 
     * @param modelComposition the model composition to create a view for
     * @param reuse the reuse the composition is for
     * @param containerView  the composition container view that contains this composition view. 
     */
    public UcCompositionView(COREModelComposition modelComposition, COREReuse reuse, 
                                CompositionContainerView containerView) {
        super(modelComposition, reuse, containerView);
        setHandler(HandlerFactory.INSTANCE.getUcCompositionViewHandler());
    }
    
    @Override
    public CompositionTitleView getCompositionTitleView(CompositionContainerView container,
            CompositionView<IUcCompositionViewHandler> compositionView, COREReuse reuse, boolean detailedViewOn) {
        return new UcCompositionTitleView(container, compositionView, reuse, detailedViewOn);
    }

    @Override
    public RamRectangleComponent getMappingContainerView(COREMapping<?> newMapping) {
        RamRectangleComponent mappingContainerView;
        if (newMapping instanceof UseCaseMapping) {
            mappingContainerView = new UseCaseMappingContainerView((UseCaseMapping) newMapping);
        } else {
            mappingContainerView = new ActorMappingContainerView((ActorMapping) newMapping);
        }
        return mappingContainerView;
    }

}
