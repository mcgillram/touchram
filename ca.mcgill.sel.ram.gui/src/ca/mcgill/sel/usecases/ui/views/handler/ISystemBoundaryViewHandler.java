package ca.mcgill.sel.usecases.ui.views.handler;

import ca.mcgill.sel.ram.ui.events.listeners.ITapAndHoldListener;
import ca.mcgill.sel.ram.ui.events.listeners.ITapListener;
import ca.mcgill.sel.ram.ui.views.handler.IAbstractViewHandler;

public interface ISystemBoundaryViewHandler extends IAbstractViewHandler, ITapListener, ITapAndHoldListener {

}
