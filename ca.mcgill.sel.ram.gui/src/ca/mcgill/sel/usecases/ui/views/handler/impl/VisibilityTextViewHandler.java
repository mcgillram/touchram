package ca.mcgill.sel.usecases.ui.views.handler.impl;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import ca.mcgill.sel.core.COREVisibilityType;
import ca.mcgill.sel.ram.ui.views.handler.impl.TextViewHandler;
import ca.mcgill.sel.usecases.MappableElement;
import ca.mcgill.sel.usecases.language.controller.UseCaseControllerFactory;
import ca.mcgill.sel.usecases.language.controller.UseCaseModelController;

public class VisibilityTextViewHandler extends TextViewHandler {
    @Override
    protected void setValue(EObject data, EStructuralFeature feature, Object value) {
        MappableElement element = (MappableElement) data;
        COREVisibilityType visibility = (COREVisibilityType) value;
        
        UseCaseModelController controller = UseCaseControllerFactory.INSTANCE.getUseCaseDiagramController();
        
        controller.updateElementVisiblity(element, visibility);
    }
}
