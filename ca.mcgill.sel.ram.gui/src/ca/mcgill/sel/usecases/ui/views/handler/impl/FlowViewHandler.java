package ca.mcgill.sel.usecases.ui.views.handler.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.mt4j.components.visibleComponents.shapes.MTRectangle.PositionAnchor;
import org.mt4j.input.inputProcessors.componentProcessors.dragProcessor.DragEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.commons.StringUtil;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.util.COREModelUtil;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamSelectorComponent;
import ca.mcgill.sel.ram.ui.components.listeners.AbstractDefaultRamSelectorListener;
import ca.mcgill.sel.ram.ui.components.menu.RamLinkedMenu;
import ca.mcgill.sel.ram.ui.events.listeners.ActionListener;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.OptionSelectorView;
import ca.mcgill.sel.usecases.ContextType;
import ca.mcgill.sel.usecases.Direction;
import ca.mcgill.sel.usecases.Flow;
import ca.mcgill.sel.usecases.Step;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.UseCase;
import ca.mcgill.sel.usecases.UseCaseModel;
import ca.mcgill.sel.usecases.language.controller.UseCaseControllerFactory;
import ca.mcgill.sel.usecases.language.controller.FlowController;
import ca.mcgill.sel.usecases.ui.views.BaseView;
import ca.mcgill.sel.usecases.ui.views.FlowView;
import ca.mcgill.sel.usecases.ui.views.StepNamer;
import ca.mcgill.sel.usecases.ui.views.handler.IFlowViewHandler;
import ca.mcgill.sel.usecases.util.UcInterfaceUtil;
import ca.mcgill.sel.usecases.util.UcModelUtil;
import ca.mcgill.sel.usecases.util.UseCasePreconditionType;

public class FlowViewHandler extends BaseViewHandler implements IFlowViewHandler {

    private static final int MAX_SELECTOR_WIDTH = 200;
    private static final String ACTION_FLOW_SHIFT_UP = "view.flow.shiftUp";
    private static final String ACTION_FLOW_SHIFT_DOWN = "view.flow.shiftDown";
    
    private enum StepTypes {
        COMMUNICATION,
        USE_CASE_REFERENCE,
        CONTEXT,
        ANYTHING
    }
    
    @Override
    public void removeRepresented(BaseView<?> baseView) {
        FlowView flowView = (FlowView) baseView;
        Flow flowToRemove = flowView.getFlow();
        Flow flow = (Flow) flowToRemove.eContainer();
        
        FlowController controller = UseCaseControllerFactory.INSTANCE.getFlowController();
        controller.removeAlternativeFlow(flow, flowToRemove);
    }

    @Override
    public boolean processDragEvent(DragEvent dragEvent) {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public boolean processTapEvent(TapEvent tapEvent) {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public void initMenu(RamLinkedMenu menu, TapAndHoldEvent event) {
        super.initMenu(menu, event);
        
        menu.setAnchor(PositionAnchor.LOWER_RIGHT);
        
        FlowView flowView = (FlowView) menu.getLinkedView();
        Flow flow = flowView.getFlow();
        List<Flow> alternateFlows = UcModelUtil.getComparableFlows(flow);            
        if (alternateFlows.size() > 1) {
            if (alternateFlows.indexOf(flow) != 0) {
                menu.addAction(Strings.MENU_FLOW_SHIFT_UP, Icons.ICON_MENU_ELEMENT_SHIFT_UP, 
                        ACTION_FLOW_SHIFT_UP, this, true);    
            } 
            if (alternateFlows.indexOf(flow) != alternateFlows.size() - 1) {
                menu.addAction(Strings.MENU_FLOW_SHIFT_DOWN, Icons.ICON_MENU_ELEMENT_SHIFT_DOWN, 
                        ACTION_FLOW_SHIFT_DOWN, this, true);    
            }            
        }  
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        RamRectangleComponent pressedButton = (RamRectangleComponent) event.getTarget();
        RamLinkedMenu linkedMenu = (RamLinkedMenu) pressedButton.getParentOfType(RamLinkedMenu.class);
        FlowView view;
        Flow flow;
        if (linkedMenu == null) {
            view = (FlowView) pressedButton.getParentOfType(FlowView.class);
            flow = view.getFlow();
        } else {
            view = (FlowView) linkedMenu.getLinkedView();
            flow = view.getFlow();
        }
        
        if (event.getActionCommand() == FlowView.ACTION_STEP_ADD) {
            OptionSelectorView<StepTypes> selector = 
                    new OptionSelectorView<StepTypes>(StepTypes.values());
        
            RamApp.getActiveScene().addComponent(selector, pressedButton.getCenterPointGlobal());
            // CHECKSTYLE:IGNORE AnonInnerLength: Okay here.
            selector.registerListener(new AbstractDefaultRamSelectorListener<StepTypes>() {
    
                @Override
                public void elementSelected(RamSelectorComponent<StepTypes> selector, StepTypes element) {
                    switch (element) {
                        case COMMUNICATION:
                            selectCommunicationType(flow, selector.getCenterPointGlobal());
                            break;
                        case USE_CASE_REFERENCE:
                            selectUseCase(flow, selector.getCenterPointGlobal());
                            break;
                        case CONTEXT:
                            selectContextType(flow, selector.getCenterPointGlobal());
                            break;
                        case ANYTHING:
                            UseCaseControllerFactory.INSTANCE.getFlowController().addAnythingStep(flow);
                            break;
                    }
                }
            });
        } else if (event.getActionCommand() == FlowView.ACTION_FLOW_DELETE) {
            removeRepresented(view);
        } else if (event.getActionCommand() == FlowView.ACTION_FLOW_ENABLE_POSTCONDITION) {
            FlowController controller = UseCaseControllerFactory.INSTANCE.getFlowController();
            controller.createPostCondition(flow);
        } else if (event.getActionCommand() == FlowView.ACTION_FLOW_ENABLE_EXTENSIONS) {
            view.enableExtensions();
        } else if (event.getActionCommand() == ACTION_FLOW_SHIFT_UP) {
            shiftFlowUp(flow);
        } else if (event.getActionCommand() == ACTION_FLOW_SHIFT_DOWN) {
            shiftFlowDown(flow);
        } else if (event.getActionCommand() == FlowView.ACTION_ALTERNATE_FLOW_ADD) {
            List<Step> selectedSteps = new ArrayList<Step>();
            List<Step> availableSteps = UcModelUtil.getExtendableSteps(flow);
            
            if (availableSteps.size() == 0) {
                return;
            }
            
            Set<String> names = new HashSet<String>();

            for (Flow f : flow.getAlternateFlows()) {
                names.add(f.getName());
            }
            
            String flowName = StringUtil.createUniqueName(Strings.DEFAULT_ALT_FLOW_NAME, names);
            
            RamSelectorComponent<Step> selector = 
                    new RamSelectorComponent<Step>(availableSteps);
            
            selector.setMaximumWidth(MAX_SELECTOR_WIDTH);
            selector.setNamer(new StepNamer());
            selector.addPlusButton(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent event) {
                    Vector3D position = selector.getCenterPointGlobal();
                    selector.destroy();
                    selectPreconditionType(flow, selectedSteps, flowName, position);
                }
                
            });
        
            RamApp.getActiveScene().addComponent(selector, pressedButton.getCenterPointGlobal());
            
            // CHECKSTYLE:IGNORE AnonInnerLength: Okay here.
            selector.registerListener(new AbstractDefaultRamSelectorListener<Step>() {
    
                @Override
                public void elementSelected(RamSelectorComponent<Step> selector, Step element) {
                    if (selectedSteps.contains(element)) {
                        selectedSteps.remove(element);
                        selector.deselectElement(element);
                    } else {
                        selectedSteps.add(element);
                        selector.selectElement(element, Colors.SELECTION_OPERATION_LIGHT);
                    }
                }
            });
        } else {
            super.actionPerformed(event);
        }
    }
    
    @SuppressWarnings("static-method")
    private void selectCommunicationType(Flow flow, Vector3D menuPosition) {
        OptionSelectorView<Direction> selector = 
                new OptionSelectorView<Direction>(Direction.values());
    
        RamApp.getActiveScene().addComponent(selector, menuPosition);
        // CHECKSTYLE:IGNORE AnonInnerLength: Okay here.
        selector.registerListener(new AbstractDefaultRamSelectorListener<Direction>() {

            @Override
            public void elementSelected(RamSelectorComponent<Direction> selector, Direction element) {
                FlowController controller = UseCaseControllerFactory.INSTANCE.getFlowController();
                controller.addCommunicationStep(flow, element);
            }
        });
    }
    
    @SuppressWarnings("static-method")
    private void selectUseCase(Flow flow, Vector3D menuPosition) {
        UseCaseModel ucm = 
                EMFModelUtil.getRootContainerOfType(flow, UcPackage.Literals.USE_CASE_MODEL);
        
        UseCase currentUseCase = EMFModelUtil.getRootContainerOfType(flow, UcPackage.Literals.USE_CASE);
        List<UseCase> availableUseCases = new ArrayList<UseCase>(UcInterfaceUtil.getAvailableUseCases(ucm));
        COREModelUtil.filterMappedElements(availableUseCases);
        availableUseCases.remove(currentUseCase);
        
        RamSelectorComponent<UseCase> selector = 
                new RamSelectorComponent<UseCase>(availableUseCases);
        
        RamApp.getActiveScene().addComponent(selector, menuPosition);
        
        selector.registerListener(new AbstractDefaultRamSelectorListener<UseCase>() {

            @Override
            public void elementSelected(RamSelectorComponent<UseCase> selector, UseCase element) {
                FlowController controller = UseCaseControllerFactory.INSTANCE.getFlowController();
                controller.addUseCaseReferenceStep(flow, element);
                selector.destroy();
            }
        });
    }
    
    @SuppressWarnings("static-method")
    private void selectContextType(Flow flow, Vector3D menuPosition) {
        List<ContextType> options = Arrays.stream(ContextType.values())
                .collect(Collectors.toList());                
        options.remove(ContextType.TIMEOUT);
        
        OptionSelectorView<ContextType> selector = 
                new OptionSelectorView<ContextType>(options);
    
        RamApp.getActiveScene().addComponent(selector, menuPosition);
        // CHECKSTYLE:IGNORE AnonInnerLength: Okay here.
        selector.registerListener(new AbstractDefaultRamSelectorListener<ContextType>() {

            @Override
            public void elementSelected(RamSelectorComponent<ContextType> selector, ContextType element) {
                FlowController controller = UseCaseControllerFactory.INSTANCE.getFlowController();
                controller.addContextStep(flow, element);
            }
        });
    }
    
    @SuppressWarnings("static-method")
    private void selectPreconditionType(Flow flow, List<Step> selectedSteps, String name, Vector3D menuPosition) {
        OptionSelectorView<UseCasePreconditionType> selector = 
                new OptionSelectorView<UseCasePreconditionType>(UseCasePreconditionType.values());
    
        RamApp.getActiveScene().addComponent(selector, menuPosition);
        // CHECKSTYLE:IGNORE AnonInnerLength: Okay here.
        selector.registerListener(new AbstractDefaultRamSelectorListener<UseCasePreconditionType>() {

            @Override
            public void elementSelected(RamSelectorComponent<UseCasePreconditionType> selector, 
                    UseCasePreconditionType element) {
                FlowController controller = UseCaseControllerFactory.INSTANCE.getFlowController();
                controller.createAlternateFlow(flow, selectedSteps, name, element);
            }
        });
    }
    
    @SuppressWarnings("static-method")
    private void shiftFlowUp(Flow flow) {
        Flow parentFlow = (Flow) flow.eContainer();
        int currentIndex = parentFlow.getAlternateFlows().indexOf(flow);
        UseCaseControllerFactory.INSTANCE.getFlowController()
            .setAlternateFlowPosition(parentFlow, flow, currentIndex - 1);
    }
    
    @SuppressWarnings("static-method")
    private void shiftFlowDown(Flow flow) {
        Flow parentFlow = (Flow) flow.eContainer();
        int currentIndex = parentFlow.getAlternateFlows().indexOf(flow);
        UseCaseControllerFactory.INSTANCE.getFlowController()
            .setAlternateFlowPosition(parentFlow, flow, currentIndex + 1);
    }
}
