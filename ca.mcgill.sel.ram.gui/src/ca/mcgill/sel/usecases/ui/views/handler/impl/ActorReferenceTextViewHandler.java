package ca.mcgill.sel.usecases.ui.views.handler.impl;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.impl.TextViewHandler;
import ca.mcgill.sel.usecases.Actor;
import ca.mcgill.sel.usecases.ActorReferenceText;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.UseCase;
import ca.mcgill.sel.usecases.language.controller.ActorReferenceTextController;
import ca.mcgill.sel.usecases.language.controller.UseCaseControllerFactory;
import ca.mcgill.sel.usecases.ui.views.ActorReferenceTextView;
import ca.mcgill.sel.usecases.util.UseCaseTextUtils;

public class ActorReferenceTextViewHandler extends TextViewHandler {
    
    //private static final String THE_SYSTEM = "the system";
    
    @Override
    public void keyboardOpened(TextView textView) {
        updateMessageText(textView);
    }
    
    @Override
    public void keyboardCancelled(TextView textView) {
        super.keyboardCancelled(textView);
        updateMessageText(textView);
    }  
    
    @Override
    public boolean shouldDismissKeyboard(TextView textView) {
        // Each communication step must contain:
        // - An actor's name
        // - The words "the system"
        EObject data = textView.getData();
        ActorReferenceTextView actorTextView = (ActorReferenceTextView) textView;
        
        // Text editing is only available for attributes.
        EAttribute feature = (EAttribute) textView.getFeature();
        String text = textView.getText();
        
        
        //if (!text.contains(THE_SYSTEM)) {
            // return false;
        //}        
        
        UseCase useCase = EMFModelUtil.getRootContainerOfType(data, UcPackage.Literals.USE_CASE);
        List<Actor> referencedActors = new ArrayList<Actor>();
        String newText = UseCaseTextUtils.parseActorNames(text, useCase, referencedActors);
        
        if (referencedActors.isEmpty() && actorTextView.isActorReferenceRequired()) {
            // No actors were replaced, so we shouldn't save.
            return false;
        }
        
        ActorReferenceTextController controller = UseCaseControllerFactory.INSTANCE.getActorReferenceTextController();
        controller.updateActorReferenceText((ActorReferenceText) data, newText, referencedActors);
        
        setValue(data, feature, newText);
        
        return true;
    }
    
    @SuppressWarnings("static-method")
    private void updateMessageText(TextView textView) {        
        ActorReferenceText referenceObject = (ActorReferenceText) textView.getData();
        String displayText = UseCaseTextUtils.replaceActorTokens(referenceObject);
        textView.setText(displayText);
    }
}
