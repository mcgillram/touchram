package ca.mcgill.sel.usecases.ui.views.handler.impl;

import java.io.File;

import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.COREModelExtension;
import ca.mcgill.sel.core.controller.COREControllerFactory;
import ca.mcgill.sel.core.controller.ReuseController;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.core.util.COREModelUtil;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamPopup.PopupType;
import ca.mcgill.sel.ram.ui.components.browser.GenericFileBrowser;
import ca.mcgill.sel.ram.ui.components.browser.interfaces.FileBrowserListener;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.structural.handler.ICompositionContainerViewHandler;
import ca.mcgill.sel.usecases.UseCaseModel;

public class UcExtensionsContainerViewHandler implements ICompositionContainerViewHandler {

    @Override
    public void deleteModelComposition(COREModelComposition modelComposition) {
        // Disallow deleting the COREModelComposition if split view is enabled.
        boolean splitModeEnabled = false;
        // TODO: Used to be RamApp.getActiveAspectScene().getCurrentView() instanceof CompositionSplitEditingView;
        
        if (!splitModeEnabled) {
            COREControllerFactory.INSTANCE.getReuseController()
                .removeModelExtension((COREModelExtension) modelComposition);
        } else {
            RamApp.getActiveAspectScene().displayPopup(Strings.POPUP_CANT_DELETE_INST_EDIT);
        }

    }

    @Override
    public void loadBrowser(final COREArtefact mainModel) {
        // Ask the user to load a class diagram
        GenericFileBrowser.loadModel("uc", new FileBrowserListener() {

            @Override
            public void modelLoaded(final EObject model) {
                extendAspect(mainModel, (UseCaseModel) model);
            }

            @Override
            public void modelSaved(File file) {
            }
        });

    }

    @Override
    public void tailorExistingReuse(COREArtefact artefact) {
        // Not implemented
    }
    
    /**
     * Create an extend relationship between extendedModel and extendingModel.
     * Does nothing if the creation is not valid.
     *
     * @param extendingArtefact - The aspect that extends
     * @param ucToExtend - The use case model to extend.
     */
    private static void extendAspect(final COREArtefact extendingArtefact, final UseCaseModel ucToExtend) {
        RamApp.getApplication().invokeLater(new Runnable() {
            @SuppressWarnings("unlikely-arg-type")
            @Override
            public void run() {
                String errorMessage = null;
                UseCaseModel extendingUc = (UseCaseModel)
                        ((COREExternalArtefact) extendingArtefact).getRootModelElement();
                // Look for error cases.
                if (ucToExtend.equals(extendingUc)) {
                    errorMessage = Strings.POPUP_ERROR_SELF_EXTENDS;
                } else if (COREModelUtil.collectExtendedModels(ucToExtend).contains(extendingUc)) {
                    errorMessage = Strings.POPUP_ERROR_CYCLIC_EXTENDS;
                }
                // Check if the extendingCdm is already extended.
                for (COREModelComposition composition : extendingArtefact.getModelExtensions()) {
                    if (composition.getSource() == extendingUc) {
                        errorMessage = Strings.POPUP_ERROR_SAME_EXTENDS;
                        break;
                    }
                }
                if (errorMessage != null) {
                    RamApp.getActiveScene().displayPopup(errorMessage, PopupType.ERROR);
                    return;
                }
                // We can create the composition.
                ReuseController controller = COREControllerFactory.INSTANCE.getReuseController();
                controller.createModelExtension(extendingArtefact,
                        COREArtefactUtil.getReferencingExternalArtefact(ucToExtend));
            }
        });
    }

}
