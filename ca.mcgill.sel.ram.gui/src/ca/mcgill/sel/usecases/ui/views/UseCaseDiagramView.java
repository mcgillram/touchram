package ca.mcgill.sel.usecases.ui.views;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.provider.INotifyChangedListener;
import org.mt4j.components.TransformSpace;
import org.mt4j.input.gestureAction.TapAndHoldVisualizer;
import org.mt4j.input.inputProcessors.IGestureEventListener;
import org.mt4j.input.inputProcessors.componentProcessors.panProcessor.PanProcessorTwoFingers;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldProcessor;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapProcessor;
import org.mt4j.input.inputProcessors.componentProcessors.unistrokeProcessor.UnistrokeUtils.Direction;
import org.mt4j.input.inputProcessors.componentProcessors.unistrokeProcessor.UnistrokeUtils.UnistrokeGesture;
import org.mt4j.input.inputProcessors.componentProcessors.zoomProcessor.ZoomProcessor;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.events.MouseWheelProcessor;
import ca.mcgill.sel.ram.ui.events.RightClickDragProcessor;
import ca.mcgill.sel.ram.ui.events.UnistrokeProcessorLeftClick;
import ca.mcgill.sel.ram.ui.utils.GUIConstants;
import ca.mcgill.sel.ram.ui.utils.MathUtils;
import ca.mcgill.sel.ram.ui.views.AbstractView;
import ca.mcgill.sel.ram.ui.views.RelationshipView;
import ca.mcgill.sel.usecases.Actor;
import ca.mcgill.sel.usecases.LayoutElement;
import ca.mcgill.sel.usecases.NamedElement;
import ca.mcgill.sel.usecases.Note;
import ca.mcgill.sel.usecases.UcFactory;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.UseCase;
import ca.mcgill.sel.usecases.UseCaseModel;
import ca.mcgill.sel.usecases.impl.ContainerMapImpl;
import ca.mcgill.sel.usecases.impl.ElementMapImpl;
import ca.mcgill.sel.usecases.language.controller.UseCaseControllerFactory;
import ca.mcgill.sel.usecases.language.controller.UseCaseModelController;
import ca.mcgill.sel.usecases.ui.utils.UseCaseModelHandlerFactory;
import ca.mcgill.sel.usecases.ui.views.ActorUseCaseAssociationView.AssociationType;
import ca.mcgill.sel.usecases.ui.views.handler.IUseCaseDiagramViewHandler;
import ca.mcgill.sel.usecases.util.UcModelUtil;
import ca.mcgill.sel.usecases.util.UseCaseAssociationType;

public class UseCaseDiagramView extends AbstractView<IUseCaseDiagramViewHandler>
        implements INotifyChangedListener {
    private static final float ACTOR_PADDING_X = 10.0f;
    private static final float ACTOR_PADDING_Y = 10.0f;
    private static final float DISTANCE_BETWEEN_ACTORS = 150.0f;
    
    private UseCaseModel useCaseModel;
    private ContainerMapImpl layout;
    
    private Set<BaseView<?>> selectedElements;
    
    private HashMap<Actor, ActorView> actorToViewMap;
    private HashMap<UseCase, UseCaseView> useCaseToViewMap;
    private HashMap<Note, NoteView> noteToViewMap;
    private List<ActorUseCaseAssociationView> associationViewList;
    private List<AnnotationView> annotationViewList;
    private List<UseCaseAssociationView> useCaseAssociationViewList;
    private List<InheritanceView> inheritanceViewList;
    
    private SystemBoundaryView systemBoundary;
        
    public UseCaseDiagramView(UseCaseModel ucd, ContainerMapImpl layout, float width, float height) {
        super(width, height);
        
        this.useCaseModel = ucd;
        this.layout = layout;
        
        this.actorToViewMap = new HashMap<Actor, ActorView>();
        this.useCaseToViewMap = new HashMap<UseCase, UseCaseView>();
        this.noteToViewMap = new HashMap<Note, NoteView>();
        this.associationViewList = new ArrayList<ActorUseCaseAssociationView>();
        this.annotationViewList = new ArrayList<AnnotationView>();
        this.useCaseAssociationViewList = new ArrayList<UseCaseAssociationView>();
        this.inheritanceViewList = new ArrayList<InheritanceView>();
        
        buildAndLayout(width, height);
        
        EMFEditUtil.addListenerFor(ucd, this);
        // register to the ContainerMap to receive adds/removes of ElementMaps
        EMFEditUtil.addListenerFor(this.layout, this);
        
        this.selectedElements = new HashSet<BaseView<?>>();
    }

    @Override
    public void notifyChanged(Notification notification) {
        // The map as a whole can be changed
        if (notification.getFeature() == UcPackage.Literals.CONTAINER_MAP__VALUE) {
            if (notification.getEventType() == Notification.ADD) {
                ElementMapImpl elementMap = (ElementMapImpl) notification.getNewValue();
                if (elementMap.getKey() instanceof Actor) {
                    actorToViewMap.get(elementMap.getKey()).setLayoutElement(elementMap.getValue());
                    repositionActors();
                } else if (elementMap.getKey() instanceof UseCase) {
                    useCaseToViewMap.get(elementMap.getKey()).setLayoutElement(elementMap.getValue());
                    resizeSystemBoundary();
                } else if (elementMap.getKey() instanceof Note) {
                    noteToViewMap.get(elementMap.getKey()).setLayoutElement(elementMap.getValue());
                }
            }
        } else if (notification.getFeature() == UcPackage.Literals.USE_CASE_MODEL__ACTORS) {
            Actor actor = null;
            switch (notification.getEventType()) {
                case Notification.ADD:
                    actor = (Actor) notification.getNewValue();
                    addActor(actor, layout.getValue().get(actor));
                    if (actor.getGeneralization() != null) {
                        addInheritanceAssociationView(actor, actor.getGeneralization());
                    }
                    for (UseCase uc : UcModelUtil.getUseCasesAssociatedToActor(actor)) {
                        addActorAssociationViews(uc);
                    }
                    
                    break;
                case Notification.REMOVE:
                    actor = (Actor) notification.getOldValue();
                    deleteActor(actor);
                    break;
            }
        } else if (notification.getFeature() == UcPackage.Literals.USE_CASE_MODEL__USE_CASES) {
            UseCase useCase = null;
            switch (notification.getEventType()) {
                case Notification.ADD:
                    useCase = (UseCase) notification.getNewValue();
                    addUseCase(useCase, layout.getValue().get(useCase));
                    addAssociationViews(useCase);
                    break;
                case Notification.REMOVE:
                    useCase = (UseCase) notification.getOldValue();
                    deleteUseCase(useCase);
                    resizeSystemBoundary();
                    break;
            }
        } else if (notification.getFeature() == UcPackage.Literals.USE_CASE_MODEL__NOTES) {
            Note note = null;
            switch (notification.getEventType()) {
                case Notification.ADD:
                    note = (Note) notification.getNewValue();
                    addNote(note, layout.getValue().get(note));
                    for (NamedElement annotated : note.getNotedElement()) {
                        addAnnotationView(note, annotated);
                    }
                    break;
                case Notification.REMOVE:
                    note = (Note) notification.getOldValue();
                    deleteNote(note);
                    break;
            }
        }
    }
    
    @Override
    protected void registerGestureListeners(IGestureEventListener listener) {
        super.registerGestureListeners(listener);

        addGestureListener(TapProcessor.class, listener);
        addGestureListener(TapAndHoldProcessor.class, listener);
        addGestureListener(TapAndHoldProcessor.class, new TapAndHoldVisualizer(RamApp.getApplication(), getParent()));
    }

    @Override
    protected void registerInputProcessors() {
        registerInputProcessor(new TapProcessor(RamApp.getApplication(), GUIConstants.TAP_MAX_FINGER_UP, false,
                GUIConstants.TAP_DOUBLE_TAP_TIME));
        registerInputProcessor(new TapAndHoldProcessor(RamApp.getApplication(), GUIConstants.TAP_AND_HOLD_DURATION));
        registerInputProcessor(new PanProcessorTwoFingers(RamApp.getApplication()));
        registerInputProcessor(new RightClickDragProcessor(RamApp.getApplication()));
        registerInputProcessor(new ZoomProcessor(RamApp.getApplication()));
        registerInputProcessor(new MouseWheelProcessor(RamApp.getApplication()));

        UnistrokeProcessorLeftClick up = new UnistrokeProcessorLeftClick(RamApp.getApplication());
        up.addTemplate(UnistrokeGesture.CIRCLE, Direction.CLOCKWISE);
        up.addTemplate(UnistrokeGesture.CIRCLE, Direction.COUNTERCLOCKWISE);

        registerInputProcessor(up);
    }
    
    /**
     * Gets the use case diagram associated with the view.
     * @return The use case diagram
     */
    public UseCaseModel getUseCaseModel() {
        return this.useCaseModel;
    }
    
    /**
     * Returns a collection of all base views.
     * This includes {@link ClassView}, {@link NoteView}, {@link ImplementationClassView} and {@link EnumView}.
     * 
     * @return a collection of all base views
     */
    public Collection<BaseView<?>> getBaseViews() {
        Collection<BaseView<?>> baseViews = new HashSet<BaseView<?>>();

        baseViews.addAll(actorToViewMap.values());
        baseViews.addAll(useCaseToViewMap.values());
        baseViews.addAll(noteToViewMap.values());

        return baseViews;
    }
    
    /**
     * Removes the given {@link BaseView} from the list of selected views.
     * 
     * @param baseView the {@link BaseView} to remove
     */
    protected void elementDeselected(BaseView<?> baseView) {
        selectedElements.remove(baseView);
    }

    /**
     * Adds the given {@link BaseView} to the list of selected views.
     * 
     * @param baseView the baseView to add to the selection
     */
    protected void elementSelected(BaseView<?> baseView) {
        selectedElements.add(baseView);
    }
    
    public Set<BaseView<?>> getSelectedElements() {
        return this.selectedElements;
    }
    
    public ActorView getActorView(Actor actor) {
        return this.actorToViewMap.get(actor);
    }
    
    public UseCaseView getUseCaseView(UseCase useCase) {
        return this.useCaseToViewMap.get(useCase);
    }
    
    public NoteView getNoteView(Note note) {
        return this.noteToViewMap.get(note);
    }
    
    /**
     * Deselects all currently selected classifiers.
     */
    public void deselect() {
        // Use separate set here, otherwise a concurrent modification occurs,
        // because the view notifies us that it was deselected, which triggers
        // the removal of the view from our set.
        for (BaseView<?> baseView : new HashSet<BaseView<?>>(selectedElements)) {
            baseView.setSelect(false);
        }

        selectedElements.clear();
    }
    
    @Override
    public void destroy() {
        // remove listeners
        EMFEditUtil.removeListenerFor(layout, this);
        EMFEditUtil.removeListenerFor(this.useCaseModel, this);

        for (ActorUseCaseAssociationView view : associationViewList) {
            view.destroy();
        }
        
        for (UseCaseAssociationView view : useCaseAssociationViewList) {
            view.destroy();
        }
        
        for (InheritanceView view : inheritanceViewList) {
            view.destroy();
        }

        // destroy basic views
        for (BaseView<?> view : getBaseViews()) {
            view.destroy();
        }
        
        // do rest
        super.destroy();
    }    
    
    public void addAnnotationView(Note note, NamedElement annotatedElement) {
        BaseView<?> annotatedElementView = null;
        NoteView noteView = noteToViewMap.get(note);

        if (actorToViewMap.containsKey(annotatedElement)) {
            annotatedElementView = actorToViewMap.get(annotatedElement);
        } else if (useCaseToViewMap.containsKey(annotatedElement)) {
            annotatedElementView = useCaseToViewMap.get(annotatedElement);
        } else {
            return;
        }

        AnnotationView annotationView = new AnnotationView(noteView, annotatedElementView);

        annotationViewList.add(annotationView);

        addChild(annotationView);
        annotationView.updateLines();
    }
    
    public void removeAnnotationView(Note note, NamedElement annotatedElement) {
        AnnotationView annotViewToBeDeleted = null;

        for (AnnotationView annotView : annotationViewList) {

            Note noteFrom = annotView.getNoteView().getNote();
            NamedElement annotatedElementTo = (NamedElement) annotView.getAnnotatedElementView().getRepresented();

            if (noteFrom == note && annotatedElementTo == annotatedElement) {
                annotViewToBeDeleted = annotView;
                break;
            }
        }

        if (annotViewToBeDeleted != null) {
            annotationViewList.remove(annotViewToBeDeleted);
            removeChild(annotViewToBeDeleted);

            annotViewToBeDeleted.destroy();
        }
    }
    
    public void addAssociationView(UseCase useCase, Actor actor, AssociationType type) {
        UseCaseView useCaseView = useCaseToViewMap.get(useCase);
        ActorView actorView = actorToViewMap.get(actor);
        ActorUseCaseAssociationView associationView = new ActorUseCaseAssociationView(
                useCase, useCaseView, actor, actorView, type);
        associationView.setBackgroundColor(this.getFillColor());
        associationView.updateLines();
        associationView.setHandler(UseCaseModelHandlerFactory.INSTANCE.getAssociationViewHandler());
        associationViewList.add(associationView);
        addChild(associationView);
    }
    
    public void removeAssociationView(EObject end1, EObject end2) {
        ActorUseCaseAssociationView viewToDelete = null;
        
        for (ActorUseCaseAssociationView view : associationViewList) {
            EObject e1 = view.getToEnd().getModel();
            EObject e2 = view.getFromEnd().getModel();

            if (end1 == e1 && end2 == e2 || end1 == e2 && end2 == e1) {
                viewToDelete = view;
                break;
            }
        }

        if (viewToDelete != null) {
            associationViewList.remove(viewToDelete);
            removeChild(viewToDelete);

            viewToDelete.destroy();
        }
    }
    
    public void addUseCaseAssociationView(UseCase source, UseCase dest, UseCaseAssociationType type) {
        // Don't add an association if one already exists
        for (UseCaseAssociationView view : useCaseAssociationViewList) {
            EObject sourceEnd = type == UseCaseAssociationType.EXTEND 
                    ? (UseCase) view.getToEnd().getModel() : view.getFromEnd().getModel();
            EObject destEnd = type == UseCaseAssociationType.EXTEND 
                    ? (UseCase) view.getFromEnd().getModel() : view.getToEnd().getModel();

            if (source == sourceEnd && dest == destEnd && view.getType() == type) {
                return;
            }
        }
        
        if (type == UseCaseAssociationType.INCLUDE) {
            addInclusionView(source, dest);
        }
    }
    
    public void removeUseCaseAssociationView(UseCase end1, UseCase end2, UseCaseAssociationType type) {
        UseCaseAssociationView viewToDelete = null;

        for (UseCaseAssociationView view : useCaseAssociationViewList) {
            EObject sourceEnd = type == UseCaseAssociationType.EXTEND 
                    ? (UseCase) view.getToEnd().getModel() : view.getFromEnd().getModel();
            EObject destEnd = type == UseCaseAssociationType.EXTEND 
                    ? (UseCase) view.getFromEnd().getModel() : view.getToEnd().getModel();

            if (end1 == sourceEnd && end2 == destEnd && view.getType() == type) {
                viewToDelete = view;
                break;
            }
        }

        if (viewToDelete != null) {
            useCaseAssociationViewList.remove(viewToDelete);
            removeChild(viewToDelete);

            viewToDelete.destroy();
        }
    }
    
    public void addInheritanceAssociationView(NamedElement source, NamedElement destination) {
        BaseView<?> sourceView;
        BaseView<?> destView;
        if (source instanceof UseCase && destination instanceof UseCase) {
            sourceView = useCaseToViewMap.get((UseCase) source);
            destView = useCaseToViewMap.get((UseCase) destination);
        } else if (source instanceof Actor && destination instanceof Actor) {
            sourceView = actorToViewMap.get((Actor) source);
            destView = actorToViewMap.get((Actor) destination);
        } else {
            throw new IllegalArgumentException();
        }
        
        InheritanceView associationView = new InheritanceView(
                source, sourceView, destination, destView);
        associationView.setBackgroundColor(this.getFillColor());
        associationView.setHandler(UseCaseModelHandlerFactory.INSTANCE.getInheritanceAssociationViewHandler());
        associationView.updateLines();
        inheritanceViewList.add(associationView);
        addChild(associationView);
    }
    
    public void removeInheritanceAssociationView(NamedElement end1, NamedElement end2) {
        InheritanceView viewToDelete = null;

        for (InheritanceView view : inheritanceViewList) {
            EObject e1 = view.getToEnd().getModel();
            EObject e2 = view.getFromEnd().getModel();

            if (end1 == e1 && end2 == e2 || end1 == e2 && end2 == e1) {
                viewToDelete = view;
                break;
            }
        }

        if (viewToDelete != null) {
            inheritanceViewList.remove(viewToDelete);
            removeChild(viewToDelete);

            viewToDelete.destroy();
        }
    }
    
    /**
     * Removes a relationship view from the model.
     * @param relationship The relationship view.
     */
    public void removeRelationshipView(RelationshipView<?, ?> relationship) {
        if (relationship instanceof ActorUseCaseAssociationView) {
            associationViewList.remove(relationship);
        } else if (relationship instanceof UseCaseAssociationView) {
            useCaseAssociationViewList.remove(relationship);
        } else if (relationship instanceof AnnotationView) {
            annotationViewList.remove(relationship);
        } else if (relationship instanceof InheritanceView) {
            inheritanceViewList.remove(relationship);
        }
    }
    
    /**
     * Resizes the bounding box for the system.
     * TODO: move actors outside of the resized box.
     */
    public void resizeSystemBoundary() {
        float minX = 0;
        float minY = 0; 
        float maxX = 0;
        float maxY = 0;
        
        for (UseCaseView view : useCaseToViewMap.values()) {
            float x1 = view.layoutElement.getX();
            float x2 = view.layoutElement.getX() + view.getWidthXY(TransformSpace.LOCAL);
            float y1 = view.layoutElement.getY();
            float y2 = view.layoutElement.getY() + view.getHeightXY(TransformSpace.LOCAL);
            
            if (minX == 0 || x1 < minX) {
                minX = x1;
            }
            
            if (maxX == 0 || x2 > maxX) {
                maxX = x2;
            }
            
            if (minY == 0 || y1 < minY) {
                minY = y1;
            }
            
            if (maxY == 0 || y2 > maxY) {
                maxY = y2;
            }
        }
        
        Vector3D position = new Vector3D(minX, minY);
        float width = maxX - minX;
        float height = maxY - minY;

        systemBoundary.resize(position, width, height);
        
        repositionActors();
    }
    
    /**
     * Toggles the visibility of all NoteViews and their associated AnnotationViews.
     * 
     * @return the new visibility status of notes
     */
    public boolean toggleNotes() {
        boolean newVisibility = true;
        for (NoteView noteView : noteToViewMap.values()) {
            newVisibility = !noteView.isVisible();
            noteView.setVisible(newVisibility);
            for (RelationshipView<?, ? extends RamRectangleComponent> annotView : noteView.getAllRelationshipViews()) {
                annotView.setVisible(newVisibility);
            }
            noteView.updateRelationships();
        }
        return newVisibility;
    }
    
    private void buildAndLayout(float width, float height) {
        Vector3D actorPosition = new Vector3D(100, 100);
        Vector3D useCasePosition = new Vector3D(200, 100);
        Vector3D position = new Vector3D(100, 100);

        float maxX = width;
        float maxY = height;
        
        systemBoundary = new SystemBoundaryView(useCaseModel, width, height);
        addChild(systemBoundary);
        systemBoundary.setHandler(UseCaseModelHandlerFactory.INSTANCE.getSystemBoundaryViewHandler());
        
        for (Actor actor : useCaseModel.getActors()) {
            LayoutElement layoutElement = layout == null ? null : layout.getValue().get(actor);
            addActor(actor, layoutElement);
            ActorView actorView = actorToViewMap.get(actor);

            if (layoutElement == null) {

                layoutElement = UcFactory.eINSTANCE.createLayoutElement();
                this.layout.getValue().put(actor, layoutElement);
                
                actorPosition.setY(actorPosition.getY() + DISTANCE_BETWEEN_ACTORS);
                position.setY(actorPosition.getY());
                
                layoutElement.setX(actorPosition.getX());
                layoutElement.setY(actorPosition.getY());
                actorView.setLayoutElement(layoutElement);
            } else {
                actorPosition.setX(Math.max(actorPosition.getX(), layoutElement.getX() + actorView.getWidth()));
                actorPosition.setY(Math.max(actorPosition.getY(), layoutElement.getY() + actorView.getHeight()));
                position.setX(actorPosition.getX());
                position.setY(actorPosition.getY());
            }

            maxX = Math.max(maxX, actorView.getX() + actorView.getWidth());
            maxY = Math.max(maxY, actorView.getY() + actorView.getHeight());
        }
        
        for (UseCase useCase : useCaseModel.getUseCases()) {
            LayoutElement layoutElement = layout == null ? null : layout.getValue().get(useCase);
            addUseCase(useCase, layoutElement);
            UseCaseView useCaseView = useCaseToViewMap.get(useCase);

            if (layoutElement == null) {

                layoutElement = UcFactory.eINSTANCE.createLayoutElement();
                this.layout.getValue().put(useCase, layoutElement);

                useCasePosition.setY(useCasePosition.getY() + DISTANCE_BETWEEN_ACTORS);
                
                layoutElement.setX(useCasePosition.getX());
                layoutElement.setY(useCasePosition.getY());
                useCaseView.setLayoutElement(layoutElement);
            } else {
                useCasePosition.setX(Math.max(useCasePosition.getX(), layoutElement.getX() + useCaseView.getWidth()));
                useCasePosition.setY(Math.max(useCasePosition.getY(), 
                        layoutElement.getY() + useCaseView.getHeight()));
                position.setX(useCasePosition.getX());
                position.setY(useCasePosition.getY());
            }

            maxX = Math.max(maxX, useCaseView.getX() + useCaseView.getWidth());
            maxY = Math.max(maxY, useCaseView.getY() + useCaseView.getHeight());
        }
        
        for (Note note : useCaseModel.getNotes()) {
            LayoutElement layoutElement = layout == null ? null : layout.getValue().get(note);
            addNote(note, layoutElement);
            NoteView noteView = noteToViewMap.get(note);

            position.setX(Math.max(position.getX(), layoutElement.getX() + noteView.getWidth()));
            position.setY(Math.max(position.getY(), layoutElement.getY() + noteView.getHeight()));

            maxX = Math.max(maxX, noteView.getX() + noteView.getWidth());
            maxY = Math.max(maxY, noteView.getY() + noteView.getHeight());
        }
        
        for (Actor actor : useCaseModel.getActors()) {
            if (actor.getGeneralization() != null) {
                addInheritanceAssociationView(actor, actor.getGeneralization());
            }
        }
        
        for (UseCase useCase : useCaseModel.getUseCases()) {
            addAssociationViews(useCase);            
        }
        
     // Create annotation links.
        for (Note note : useCaseModel.getNotes()) {
            for (NamedElement annotatedElement : note.getNotedElement()) {
                addAnnotationView(note, annotatedElement);
            }
        }       
        
        resizeSystemBoundary();
    }
    
    private void addActor(Actor actor, LayoutElement layoutElement) {
        ActorView view = new ActorView(this, actor, layoutElement);
        actorToViewMap.put(actor, view);
        addChild(view, actor);
        view.setHandler(UseCaseModelHandlerFactory.INSTANCE.getActorViewHandler());
    }
    
    private void deleteActor(Actor actor) {
        ActorView actorView = this.actorToViewMap.remove(actor);        
        removeChild(actorView);
        actorView.destroy();        
    }    
    
    private void addUseCase(UseCase useCase, LayoutElement layoutElement) {        
        UseCaseView view = new UseCaseView(this, useCase, layoutElement);
        useCaseToViewMap.put(useCase, view);
        addChild(view, useCase);
        view.setHandler(UseCaseModelHandlerFactory.INSTANCE.getUseCaseViewHandler());
    }
    
    private void deleteUseCase(UseCase useCase) {
        UseCaseView useCaseView = this.useCaseToViewMap.remove(useCase);        
        removeChild(useCaseView);
        useCaseView.destroy();        
    }    
    
    private void addNote(Note note, LayoutElement layoutElement) {
        NoteView noteView = new NoteView(this, note, layoutElement);
        noteToViewMap.put(note, noteView);
        addChild(noteView, note);
        noteView.setHandler(UseCaseModelHandlerFactory.INSTANCE.getNoteViewHandler());
    }
    
    private void deleteNote(Note note) {
        NoteView noteView = this.noteToViewMap.remove(note);        
        removeChild(noteView);
        noteView.destroy();
    }
    
    private void addAssociationViews(UseCase useCase) {
        addActorAssociationViews(useCase);        
        
        // TODO: better way to do this
        if (useCase.getGeneralization() != null) {
            addInheritanceAssociationView(useCase, useCase.getGeneralization());
        }
        
        for (UseCase includedUseCase : UcModelUtil.getIncludedUseCases(useCase)) {
            addUseCaseAssociationView(useCase, includedUseCase, UseCaseAssociationType.INCLUDE);
        }
        
        for (UseCase includingUseCase : UcModelUtil.getUseCasesIncluding(useCase)) {
            addUseCaseAssociationView(includingUseCase, useCase, UseCaseAssociationType.INCLUDE);
        }
    }

    private void addActorAssociationViews(UseCase useCase) {
        UseCaseView useCaseView = useCaseToViewMap.get(useCase);
        
        Collection<Actor> primaryActors = useCase.getPrimaryActors();
        if (!primaryActors.isEmpty()) {
            for (Actor primaryActor : primaryActors) {
                ActorView actorView = actorToViewMap.get(primaryActor);
                ActorUseCaseAssociationView associationView = new ActorUseCaseAssociationView(
                        useCase, useCaseView, primaryActor, actorView, AssociationType.PRIMARY);
                associationView.setBackgroundColor(this.getFillColor());
                associationView.updateLines();
                associationView.setHandler(UseCaseModelHandlerFactory.INSTANCE.getAssociationViewHandler());
                addChild(associationView);
                associationViewList.add(associationView);
            }
        }
        
        Collection<Actor> secondaryActors = useCase.getSecondaryActors();
        if (!secondaryActors.isEmpty()) {
            for (Actor secondaryActor : secondaryActors) {
                ActorView actorView = actorToViewMap.get(secondaryActor);
                ActorUseCaseAssociationView associationView = new ActorUseCaseAssociationView(
                        useCase, useCaseView, secondaryActor, actorView, AssociationType.SECONDARY);
                associationView.setBackgroundColor(this.getFillColor());
                associationView.updateLines();
                associationView.setHandler(UseCaseModelHandlerFactory.INSTANCE.getAssociationViewHandler());
                addChild(associationView);
                associationViewList.add(associationView);
            }
        }
        
        Collection<Actor> facilitatorActors = useCase.getFacilitatorActors();
        if (!facilitatorActors.isEmpty()) {
            for (Actor facilitatorActor : facilitatorActors) {
                ActorView actorView = actorToViewMap.get(facilitatorActor);
                ActorUseCaseAssociationView associationView = new ActorUseCaseAssociationView(
                        useCase, useCaseView, facilitatorActor, actorView, AssociationType.FACILITATOR);
                associationView.setBackgroundColor(this.getFillColor());
                associationView.updateLines();
                associationView.setHandler(UseCaseModelHandlerFactory.INSTANCE.getAssociationViewHandler());
                addChild(associationView);
                associationViewList.add(associationView);
            }
        }
    }
    
    private void addInclusionView(UseCase useCase, UseCase includedUseCase) {
        if (useCase == includedUseCase) {
            return;
        }
        
        UseCaseView fromView = useCaseToViewMap.get(useCase);        
        UseCaseView toView = useCaseToViewMap.get(includedUseCase);
        
        UseCaseAssociationView view = new UseCaseAssociationView(
                useCase, fromView, includedUseCase, toView, UseCaseAssociationType.INCLUDE);
        view.setBackgroundColor(this.getFillColor());
        view.setHandler(UseCaseModelHandlerFactory.INSTANCE.getUseCaseAssociationViewHandler());
        view.updateLines();
        addChild(view);
        useCaseAssociationViewList.add(view);
    }   
    
    private void repositionActors() {
        float systemMinX = systemBoundary.getCenterPointGlobal().x - (systemBoundary.getWidth() / 2);
        float systemMinY = systemBoundary.getCenterPointGlobal().y - (systemBoundary.getHeight() / 2);
        float systemMaxX = systemBoundary.getCenterPointGlobal().x + (systemBoundary.getWidth() / 2);
        float systemMaxY = systemBoundary.getCenterPointGlobal().y + (systemBoundary.getHeight() / 2);
        Map<NamedElement, LayoutElement> positionMap = new HashMap<NamedElement, LayoutElement>();
        
        for (Actor actor : actorToViewMap.keySet()) {
            boolean isModified = false;
            ActorView view = actorToViewMap.get(actor);            

            // The layout element's position represents the top left of the view
            LayoutElement layoutElement = view.layoutElement;
            
            if (MathUtils.rectanglesOverlap(systemBoundary, view)) {
                isModified = true;
                
                float minX = layoutElement.getX();
                float minY = layoutElement.getY();
                float maxX = minX + view.getWidth();
                float maxY = minY + view.getHeight();
                if (systemMinX <= minX || systemMaxX >= maxX) {
                    // Have to move in the x-direction
                    float deltaLeft = minX - systemMinX;
                    float deltaRight = systemMaxX - maxX;
                    if (deltaLeft <= deltaRight) {
                        layoutElement.setX(systemMinX - view.getWidth() - ACTOR_PADDING_X);
                    } else {
                        layoutElement.setX(systemMaxX + ACTOR_PADDING_X);
                    }
                } else if (systemMinY <= minY || systemMaxY >= maxY) {
                    // Have to move in the y-direction
                    float deltaUp = minY - systemMinY;
                    float deltaDown = systemMaxY - maxY;
                    if (deltaUp <= deltaDown) {
                        layoutElement.setY(systemMinY - view.getHeight() - ACTOR_PADDING_Y);
                    } else {
                        layoutElement.setY(systemMaxY + ACTOR_PADDING_Y);
                    }
                }
                
            }
            
            if (isModified) {
                positionMap.put(actor, layoutElement);
            }
        }
        
        UseCaseModelController controller = UseCaseControllerFactory.INSTANCE.getUseCaseDiagramController();
        controller.moveNamedElements(useCaseModel, positionMap);              
    }
}
