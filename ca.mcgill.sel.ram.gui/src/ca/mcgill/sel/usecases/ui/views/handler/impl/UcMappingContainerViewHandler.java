package ca.mcgill.sel.usecases.ui.views.handler.impl;

import ca.mcgill.sel.usecases.ActorMapping;
import ca.mcgill.sel.usecases.FlowMapping;
import ca.mcgill.sel.usecases.StepMapping;
import ca.mcgill.sel.usecases.UseCaseMapping;
import ca.mcgill.sel.usecases.language.controller.UcMappingsController;
import ca.mcgill.sel.usecases.language.controller.UseCaseControllerFactory;
import ca.mcgill.sel.usecases.ui.views.handler.IUcMappingContainerViewHandler;

public class UcMappingContainerViewHandler implements IUcMappingContainerViewHandler {

    @Override
    public void addFlowMapping(UseCaseMapping mapping) {
        if (mapping.getFrom() != null && mapping.getTo() != null) {            
            UcMappingsController mappingsController = UseCaseControllerFactory.INSTANCE.getMappingsController();
            mappingsController.createFlowMapping(mapping);
        }

    }
    
    @Override
    public void addStepMapping(FlowMapping mapping) {
        if (mapping.getFrom() != null && mapping.getTo() != null) {            
            UcMappingsController mappingsController = UseCaseControllerFactory.INSTANCE.getMappingsController();
            mappingsController.createStepMapping(mapping);
        }

    }

    @Override
    public void deleteActorMapping(ActorMapping mapping) {
        UcMappingsController mappingsController = UseCaseControllerFactory.INSTANCE.getMappingsController();
        mappingsController.deleteMapping(mapping);
    }

    @Override
    public void deleteUseCaseMapping(UseCaseMapping mapping) {
        UcMappingsController mappingsController = UseCaseControllerFactory.INSTANCE.getMappingsController();
        mappingsController.deleteMapping(mapping);
    }
    
    @Override
    public void deleteFlowMapping(FlowMapping mapping) {
        UcMappingsController mappingsController = UseCaseControllerFactory.INSTANCE.getMappingsController();
        mappingsController.deleteMapping(mapping);
    }

    @Override
    public void deleteStepMapping(StepMapping mapping) {
        UcMappingsController mappingsController = UseCaseControllerFactory.INSTANCE.getMappingsController();
        mappingsController.deleteMapping(mapping);
    }

}
