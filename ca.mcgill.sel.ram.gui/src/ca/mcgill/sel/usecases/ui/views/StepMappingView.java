package ca.mcgill.sel.usecases.ui.views;

import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.ram.ui.components.RamButton;
import ca.mcgill.sel.ram.ui.components.RamImageComponent;
import ca.mcgill.sel.ram.ui.components.RamRoundedRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent.Alignment;
import ca.mcgill.sel.ram.ui.events.listeners.ActionListener;
import ca.mcgill.sel.ram.ui.layouts.HorizontalLayoutVerticallyCentered;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.Fonts;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.HandlerFactory;
import ca.mcgill.sel.usecases.StepMapping;
import ca.mcgill.sel.usecases.util.UseCaseTextUtils;

public class StepMappingView extends RamRoundedRectangleComponent implements ActionListener {
    private static final String ACTION_STEP_MAPPING_DELETE = "view.stepMapping.delete";

    private static final float ICON_SIZE = Fonts.FONTSIZE_COMPOSITION + 2;
    
    private StepMapping stepMapping;
    
    private RamButton deleteStepMappingButton;
    
    private TextView stepMappingFromElement;
    
    private TextView stepMappingToElement;
    
    private RamImageComponent arrow;
    
    public StepMappingView(StepMapping mapping) {
        super(4);
        setNoStroke(true);
        setNoFill(true);
        setBuffers(0);
        stepMapping = mapping;
        
        // Add a button for deleting the step mapping

        RamImageComponent deleteStepMappingImage =
                new RamImageComponent(Icons.ICON_DELETE, Colors.ICON_DELETE_COLOR, ICON_SIZE, ICON_SIZE);
        deleteStepMappingButton = new RamButton(deleteStepMappingImage);
        deleteStepMappingButton.setActionCommand(ACTION_STEP_MAPPING_DELETE);
        deleteStepMappingButton.addActionListener(this);
        deleteStepMappingButton.setBuffers(0);
        deleteStepMappingButton.setBufferSize(Cardinal.WEST, 5);
        addChild(deleteStepMappingButton);
        
        // Label
        RamTextComponent stepText = new RamTextComponent(Strings.LABEL_STEP);
        stepText.setFont(Fonts.FONT_COMPOSITION);
        stepText.setBufferSize(Cardinal.SOUTH, 0);
        stepText.setBufferSize(Cardinal.EAST, 0);
        stepText.setBufferSize(Cardinal.WEST, 2 * Fonts.FONTSIZE_COMPOSITION);
        this.addChild(stepText);
        
        // From element
        stepMappingFromElement = new TextView(stepMapping, CorePackage.Literals.CORE_LINK__FROM) {
            @Override
            protected String getModelText() {
                return stepMapping.getFrom() == null ? "" : UseCaseTextUtils.getStepShortDisplay(stepMapping.getFrom());
            }
        };
        stepMappingFromElement.setHandler(HandlerFactory.INSTANCE.getUcStepMappingSelectorHandler());
        stepMappingFromElement.setFont(Fonts.FONT_COMPOSITION);
        stepMappingFromElement.setBufferSize(Cardinal.SOUTH, 0);
        stepMappingFromElement.setBufferSize(Cardinal.EAST, 0);
        stepMappingFromElement.setBufferSize(Cardinal.WEST, 0);
        stepMappingFromElement.setAlignment(Alignment.CENTER_ALIGN);
        stepMappingFromElement.setPlaceholderText(Strings.PH_SELECT_STEP);
        stepMappingFromElement.setAutoMinimizes(true);
        this.addChild(stepMappingFromElement);

        // Arrow
        arrow = new RamImageComponent(Icons.ICON_ARROW_RIGHT, Colors.ICON_ARROW_COLOR, ICON_SIZE, ICON_SIZE);
        arrow.setBufferSize(Cardinal.EAST, 1.0f);
        arrow.setBufferSize(Cardinal.WEST, 1.0f);
        this.addChild(arrow);

        // To element
        stepMappingToElement = new TextView(stepMapping, CorePackage.Literals.CORE_LINK__TO) {
            @Override
            protected String getModelText() {                
                return stepMapping.getTo() == null ? "" : UseCaseTextUtils.getStepShortDisplay(stepMapping.getTo());
            }
        };
        stepMappingToElement.setHandler(HandlerFactory.INSTANCE.getUcStepMappingSelectorHandler());
        stepMappingToElement.setFont(Fonts.FONT_COMPOSITION);
        stepMappingToElement.setBufferSize(Cardinal.SOUTH, 0);
        stepMappingToElement.setBufferSize(Cardinal.EAST, 0);
        stepMappingToElement.setBufferSize(Cardinal.WEST, 0);
        stepMappingToElement.setAlignment(Alignment.CENTER_ALIGN);
        stepMappingToElement.setPlaceholderText(Strings.PH_SELECT_STEP);
        stepMappingToElement.setAutoMinimizes(true);
        this.addChild(stepMappingToElement);

        setLayout(new HorizontalLayoutVerticallyCentered(Fonts.FONTSIZE_COMPOSITION / 5));
    }
    
    @Override
    public void actionPerformed(ActionEvent event) {
        String actionCommand = event.getActionCommand();

        if (ACTION_STEP_MAPPING_DELETE.equals(actionCommand)) {
            HandlerFactory.INSTANCE.getUcMappingContainerViewHandler()
                .deleteStepMapping(stepMapping);
        }
        
    }
    
    /**
     * Returns the Step Mapping.
     * 
     * @return {@link StepMapping}
     */
    public StepMapping getStepMapping() {
        return stepMapping;
    }

}
