package ca.mcgill.sel.usecases.ui.views.handler;

import ca.mcgill.sel.usecases.ActorMapping;
import ca.mcgill.sel.usecases.FlowMapping;
import ca.mcgill.sel.usecases.StepMapping;
import ca.mcgill.sel.usecases.UseCaseMapping;

public interface IUcMappingContainerViewHandler {
    /**
     * Adds a new {@link FlowMapping}.
     * @param mapping The parent use case mapping.
     */
    void addFlowMapping(UseCaseMapping mapping);
    
    /**
     * Adds a new {@link StepMapping}.
     * @param mapping The parent flow mapping.
     */
    void addStepMapping(FlowMapping mapping);
    
    /**
     * Deletes a {@link ActorMapping}.
     * @param mapping The actor mapping.
     */
    void deleteActorMapping(ActorMapping mapping);
    
    /**
     * Deletes a {@link UseCaseMapping}.
     * @param mapping The use case mapping.
     */
    void deleteUseCaseMapping(UseCaseMapping mapping);
    
    /**
     * Deletes a {@link FlowMapping}.
     * @param mapping The use case mapping.
     */
    void deleteFlowMapping(FlowMapping mapping);
    
    /**
     * Deletes a {@link StepMapping}.
     * @param mapping The step mapping.
     */
    void deleteStepMapping(StepMapping mapping);
}
