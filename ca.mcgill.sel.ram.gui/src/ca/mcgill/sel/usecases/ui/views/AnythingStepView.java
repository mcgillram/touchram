package ca.mcgill.sel.usecases.ui.views;

import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.usecases.Step;

public class AnythingStepView extends StepView {
    private RamTextComponent label;
    
    protected AnythingStepView(UseCaseDiagramView useCaseDiagramView, Step represented, boolean readonly) {
        super(useCaseDiagramView, represented, readonly);
    }
    
    @Override
    protected void buildStepView() {
        label = new RamTextComponent();
        label.setText(getStep().getStepText());
        label.setBuffers(0);
        addChild(label);
    }

}
