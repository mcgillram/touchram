package ca.mcgill.sel.usecases.ui.views.handler.impl;

import org.mt4j.components.visibleComponents.shapes.MTRectangle.PositionAnchor;
import org.mt4j.input.inputProcessors.componentProcessors.dragProcessor.DragEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;

import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.COREPartialityType;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.menu.RamLinkedMenu;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.usecases.Flow;
import ca.mcgill.sel.usecases.Step;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.language.controller.UseCaseControllerFactory;
import ca.mcgill.sel.usecases.language.controller.FlowController;
import ca.mcgill.sel.usecases.language.controller.StepController;
import ca.mcgill.sel.usecases.ui.views.BaseView;
import ca.mcgill.sel.usecases.ui.views.StepView;
import ca.mcgill.sel.usecases.ui.views.handler.IStepViewHandler;
import ca.mcgill.sel.usecases.util.UcModelUtil;

public class StepViewHandler extends BaseViewHandler implements IStepViewHandler {    
    private static final String ACTION_STEP_SHIFT_UP = "view.step.shiftUp";
    private static final String ACTION_STEP_SHIFT_DOWN = "view.step.shiftDown";
    private static final String SUBMENU_STEP_CHARACTERISTICS = "sub.step.partiality";
    private static final String ACTION_PUBLIC_PARTIAL = "view.step.public_partial";
    private static final String ACTION_CONCERN_PARTIAL = "view.step.concern_partial";
    private static final String ACTION_NOT_PARTIAL = "view.step.not_partial";  
    
    @Override
    public void removeRepresented(BaseView<?> baseView) {
        StepView view = (StepView) baseView;
        Step step = view.getStep();
        Flow flow = EMFModelUtil.getRootContainerOfType(step, UcPackage.Literals.FLOW);
        FlowController controller = UseCaseControllerFactory.INSTANCE.getFlowController();
        controller.deleteStep(flow, step);
    }

    @Override
    public boolean processDragEvent(DragEvent dragEvent) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean processTapEvent(TapEvent tapEvent) {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public void actionPerformed(ActionEvent event) {
        String actionCommand = event.getActionCommand();
        RamRectangleComponent pressedButton = (RamRectangleComponent) event.getTarget();
        RamLinkedMenu linkedMenu = (RamLinkedMenu) pressedButton.getParentOfType(RamLinkedMenu.class);
        if (linkedMenu != null) {
            StepView stepView = (StepView) linkedMenu.getLinkedView();
            Step step = stepView.getStep();
            if (ACTION_STEP_SHIFT_UP.equals(actionCommand)) {
                shiftStepUp(step);
            } else if (ACTION_STEP_SHIFT_DOWN.equals(actionCommand)) {
                shiftStepDown(step);
            } else if (ACTION_PUBLIC_PARTIAL.equals(actionCommand)) {
                switchPartiality(step, COREPartialityType.PUBLIC);
            } else if (ACTION_CONCERN_PARTIAL.equals(actionCommand)) {
                switchPartiality(step, COREPartialityType.CONCERN);
            } else if (ACTION_NOT_PARTIAL.equals(actionCommand)) {
                switchPartiality(step, COREPartialityType.NONE);
            } else {
                super.actionPerformed(event);
            }
        }
    }
    
    @Override
    public void initMenu(RamLinkedMenu menu, TapAndHoldEvent event) {
        super.initMenu(menu, event);
        
        menu.setAnchor(PositionAnchor.LOWER_RIGHT);
        
        StepView stepView = (StepView) menu.getLinkedView();
        Step step = stepView.getStep();
        
        if (stepView.getFlow().getSteps().size() > 1) {
            if (UcModelUtil.getStepIndex(step) != 0) {
                menu.addAction(Strings.MENU_STEP_SHIFT_UP, Icons.ICON_MENU_ELEMENT_SHIFT_UP, 
                        ACTION_STEP_SHIFT_UP, this, true);    
            } 
            if (UcModelUtil.getStepIndex(step) != stepView.getFlow().getSteps().size() - 1) {
                menu.addAction(Strings.MENU_STEP_SHIFT_DOWN, Icons.ICON_MENU_ELEMENT_SHIFT_DOWN, 
                        ACTION_STEP_SHIFT_DOWN, this, true);    
            }
        }
        
        menu.addSubMenu(1, SUBMENU_STEP_CHARACTERISTICS);
        menu.addAction(Strings.MENU_PUBLIC_PARTIAL, Icons.ICON_MENU_PUBLIC_PARTIAL, ACTION_PUBLIC_PARTIAL, this,
                SUBMENU_STEP_CHARACTERISTICS, true);
        menu.addAction(Strings.MENU_CONCERN_PARTIAL, Icons.ICON_MENU_CONCERN_PARTIAL, ACTION_CONCERN_PARTIAL,
                this, SUBMENU_STEP_CHARACTERISTICS, true);
        menu.addAction(Strings.MENU_NO_PARTIAL, Icons.ICON_MENU_NOT_PARTIAL, ACTION_NOT_PARTIAL, this,
                SUBMENU_STEP_CHARACTERISTICS, true);
    }
    
    @SuppressWarnings("static-method")
    private void shiftStepUp(Step step) {
        int currentIndex = UcModelUtil.getStepIndex(step);
        UseCaseControllerFactory.INSTANCE.getFlowController().setStepPosition(step, currentIndex - 1);
    }
    
    @SuppressWarnings("static-method")
    private void shiftStepDown(Step step) {
        int currentIndex = UcModelUtil.getStepIndex(step);
        UseCaseControllerFactory.INSTANCE.getFlowController().setStepPosition(step, currentIndex + 1);        
    }
    
    @SuppressWarnings("static-method")
    private void switchPartiality(Step step, COREPartialityType type) {
        StepController controller = UseCaseControllerFactory.INSTANCE.getStepController();
        controller.changeStepPartiality(step, type);
    }
    
}
