package ca.mcgill.sel.usecases.ui.views;

import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.ram.ui.components.RamButton;
import ca.mcgill.sel.ram.ui.components.RamImageComponent;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent.Alignment;
import ca.mcgill.sel.ram.ui.events.listeners.ActionListener;
import ca.mcgill.sel.ram.ui.layouts.HorizontalLayoutVerticallyCentered;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.Fonts;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.HandlerFactory;
import ca.mcgill.sel.usecases.UseCaseMapping;

public class UseCaseMappingView extends RamRectangleComponent implements ActionListener {
    private static final String ACTION_FLOW_MAPPING_ADD = "view.flowMapping.add";
    private static final String ACTION_USE_CASE_MAPPING_DELETE = "view.useCaseMapping.delete";

    private static final float ICON_SIZE = Fonts.FONTSIZE_COMPOSITION + 2;
    
    private UseCaseMapping useCaseMapping;    
    
    /**
     * Button to add Step Mapping.
     */
    private RamButton buttonFlowMappingAdd;
    
    /**
     * Button to delete UseCase Mapping.
     */
    private RamButton buttonUseCaseMappingDelete;

    /**
     * UseCaseMapping from element.
     */
    private TextView textUseCaseFromElement;

    /**
     * UseCaseMapping to element.
     */
    private TextView textUseCaseToElement;

    /**
     * Image for an arrow between mapping elements.
     */
    private RamImageComponent arrow;

    public UseCaseMappingView(UseCaseMapping useCaseMapping) {
        setNoStroke(true);
        setNoFill(false);
        setFillColor(Colors.USE_CASE_MAPPING_VIEW_FILL_COLOR);
        setBuffers(0);
        
        this.useCaseMapping = useCaseMapping;
        
        // Add delete button
        RamImageComponent deleteUseCaseMappingImage = new RamImageComponent(Icons.ICON_DELETE,
                Colors.ICON_DELETE_COLOR);
        deleteUseCaseMappingImage.setMinimumSize(ICON_SIZE, ICON_SIZE);
        deleteUseCaseMappingImage.setMaximumSize(ICON_SIZE, ICON_SIZE);
        buttonUseCaseMappingDelete = new RamButton(deleteUseCaseMappingImage);
        buttonUseCaseMappingDelete.setActionCommand(ACTION_USE_CASE_MAPPING_DELETE);
        buttonUseCaseMappingDelete.addActionListener(this);
        addChild(buttonUseCaseMappingDelete);

        // Add UseCase From
        textUseCaseFromElement = new TextView(useCaseMapping, CorePackage.Literals.CORE_LINK__FROM);
        textUseCaseFromElement.setHandler(HandlerFactory.INSTANCE.getTextViewHandler());
        textUseCaseFromElement.setFont(Fonts.FONT_COMPOSITION);
        textUseCaseFromElement.setBufferSize(Cardinal.SOUTH, 0);
        textUseCaseFromElement.setBufferSize(Cardinal.EAST, 0);
        textUseCaseFromElement.setBufferSize(Cardinal.WEST, 0);
        textUseCaseFromElement.setAlignment(Alignment.CENTER_ALIGN);
        textUseCaseFromElement.setPlaceholderText(Strings.PH_SELECT_USE_CASE);
        textUseCaseFromElement.setAutoMinimizes(true);
        this.addChild(textUseCaseFromElement);

        // Arrow between UseCase From and UseCase To
        arrow = new RamImageComponent(Icons.ICON_ARROW_RIGHT, Colors.ICON_ARROW_COLOR);
        arrow.setMinimumSize(ICON_SIZE, ICON_SIZE);
        arrow.setMaximumSize(ICON_SIZE, ICON_SIZE);
        this.addChild(arrow);

        // Add UseCase To
        textUseCaseToElement = new TextView(useCaseMapping, CorePackage.Literals.CORE_LINK__TO);
        textUseCaseToElement.setFont(Fonts.FONT_COMPOSITION);
        textUseCaseToElement.setBufferSize(Cardinal.SOUTH, 0);
        textUseCaseToElement.setBufferSize(Cardinal.EAST, 0);
        textUseCaseToElement.setBufferSize(Cardinal.WEST, 0);
        textUseCaseToElement.setAlignment(Alignment.CENTER_ALIGN);
        textUseCaseToElement.setPlaceholderText(Strings.PH_SELECT_USE_CASE);
        textUseCaseToElement.setAutoMinimizes(true);
        textUseCaseToElement.setHandler(HandlerFactory.INSTANCE.getTextViewHandler());
        this.addChild(textUseCaseToElement);
        
        // Add buttons for adding enum literal mapping,
        // and a button for enum mapping deleting
        RamImageComponent flowMappingAddImage = new RamImageComponent(Icons.ICON_FLOW_MAPPING_ADD,
                Colors.ICON_STRUCT_DEFAULT_COLOR);
        flowMappingAddImage.setMinimumSize(ICON_SIZE, ICON_SIZE);
        flowMappingAddImage.setMaximumSize(ICON_SIZE, ICON_SIZE);
        buttonFlowMappingAdd = new RamButton(flowMappingAddImage);
        buttonFlowMappingAdd.setActionCommand(ACTION_FLOW_MAPPING_ADD);
        buttonFlowMappingAdd.addActionListener(this);
        addChild(buttonFlowMappingAdd);
        
        setLayout(new HorizontalLayoutVerticallyCentered(Fonts.FONTSIZE_COMPOSITION / 5));
    }
    
    @Override
    public void actionPerformed(ActionEvent event) {
        String actionCommand = event.getActionCommand();
        if (ACTION_USE_CASE_MAPPING_DELETE.equals(actionCommand)) {
            HandlerFactory.INSTANCE.getUcMappingContainerViewHandler().deleteUseCaseMapping(useCaseMapping);
        } else if (ACTION_FLOW_MAPPING_ADD.equals(actionCommand)) {
            HandlerFactory.INSTANCE.getUcMappingContainerViewHandler().addFlowMapping(useCaseMapping);
        }
    }
}
