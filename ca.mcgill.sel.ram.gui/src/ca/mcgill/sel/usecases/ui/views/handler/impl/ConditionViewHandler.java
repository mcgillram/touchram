package ca.mcgill.sel.usecases.ui.views.handler.impl;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamSelectorComponent;
import ca.mcgill.sel.ram.ui.components.listeners.AbstractDefaultRamSelectorListener;
import ca.mcgill.sel.ram.ui.events.listeners.ActionListener;
import ca.mcgill.sel.ram.ui.events.listeners.ITapAndHoldListener;
import ca.mcgill.sel.ram.ui.views.OptionSelectorView;
import ca.mcgill.sel.ram.ui.views.SelectorView;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.impl.TextViewHandler;
import ca.mcgill.sel.usecases.Condition;
import ca.mcgill.sel.usecases.ConditionType;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.language.controller.ConditionController;
import ca.mcgill.sel.usecases.language.controller.UseCaseControllerFactory;

public class ConditionViewHandler extends TextViewHandler 
    implements ITapAndHoldListener {
    /**
     * Code for adding a condition.
     */
    public static final String ACTION_CONDITION_ADD = "condition.add";
    
    /**
     * Code for clearing a condition.
     */
    public static final String ACTION_CONDITION_CLEAR = "condition.clear";    

    @Override
    public void keyboardOpened(TextView textView) {
        EObject data = textView.getData();
        // Text editing is only available for attributes.
        EStructuralFeature feature = (EStructuralFeature) textView.getFeature();
        Object condition = data.eGet(feature);
        if (condition == null) {
            textView.closeKeyboard();
        }
    }
    
    @Override
    public boolean shouldDismissKeyboard(TextView textView) {
        EObject data = textView.getData();
        // Text editing is only available for attributes.
        EStructuralFeature feature = (EStructuralFeature) textView.getFeature();
        String text = textView.getText();

        Condition condition = (Condition) data.eGet(feature);
        EMFEditUtil.getPropertyDescriptor(condition, UcPackage.Literals.CONDITION__TEXT)
            .setPropertyValue(condition, text);
        
        return true;
    }
    
    @Override
    public boolean processTapEvent(TapEvent tapEvent) {
        if (tapEvent.isDoubleTap()) {
            final TextView target = (TextView) tapEvent.getTarget();
            
            // Text editing is only available when a condition is selected.
            EObject data = target.getData();
            EStructuralFeature feature = (EStructuralFeature) target.getFeature();
            Object condition = data.eGet(feature);
            if (condition != null) {
                target.showKeyboard();
                return true;
            }
        }
        
        return false;
    }

    @Override
    public boolean processTapAndHoldEvent(TapAndHoldEvent tapAndHoldEvent) {
        if (tapAndHoldEvent.isHoldComplete()) {
            final TextView target = (TextView) tapAndHoldEvent.getTarget();
            EObject data = target.getData();
            
            RamSelectorComponent<Object> selector = new SelectorView(target.getData(), target.getFeature());
            selector.addPlusButton(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent event) {
                    selectConditionType(data, target, selector.getCenterPointGlobal());
                    selector.destroy();
                    target.showKeyboard();
                }
                
            });
            
            RamApp.getActiveScene().addComponent(selector, tapAndHoldEvent.getLocationOnScreen());

            // TODO: mschoettle: this could be done by the selector itself. but does it make sense?
            selector.registerListener(new AbstractDefaultRamSelectorListener<Object>() {
                @Override
                public void elementSelected(RamSelectorComponent<Object> selector, Object element) {
                    setValue(target.getData(), target.getFeature(), element);
                }
            });
            
            return true;
        }
        
        return false;
    }
    
    @SuppressWarnings("static-method")
    private void selectConditionType(EObject data, TextView target, Vector3D menuPosition) {
        OptionSelectorView<ConditionType> selector = 
                new OptionSelectorView<ConditionType>(ConditionType.values());
    
        RamApp.getActiveScene().addComponent(selector, menuPosition);
        // CHECKSTYLE:IGNORE AnonInnerLength: Okay here.
        selector.registerListener(new AbstractDefaultRamSelectorListener<ConditionType>() {

            @Override
            public void elementSelected(RamSelectorComponent<ConditionType> selector, ConditionType element) {
                ConditionController controller = UseCaseControllerFactory.INSTANCE.getConditionController();
                Condition newCondition = controller.createCondition(data, "", element);
                setValue(data, target.getFeature(), newCondition);
            }
        });
    }
}
