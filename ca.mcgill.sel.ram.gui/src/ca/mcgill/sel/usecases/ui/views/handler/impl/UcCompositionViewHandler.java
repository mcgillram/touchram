package ca.mcgill.sel.usecases.ui.views.handler.impl;

import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;

import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.scenes.RamAbstractScene;
import ca.mcgill.sel.ram.ui.scenes.SceneCreationAndChangeFactory;
import ca.mcgill.sel.ram.ui.scenes.SceneCreationAndChangeFactory.CurrentMode;
import ca.mcgill.sel.ram.ui.views.CompositionView;
import ca.mcgill.sel.ram.ui.views.handler.BaseHandler;
import ca.mcgill.sel.ram.ui.views.handler.IDisplaySceneHandler;
import ca.mcgill.sel.ram.ui.views.structural.CompositionSplitEditingView;
import ca.mcgill.sel.usecases.language.controller.UseCaseControllerFactory;
import ca.mcgill.sel.usecases.ui.utils.UseCaseModelHandlerFactory;
import ca.mcgill.sel.usecases.ui.views.handler.IUcCompositionViewHandler;

public class UcCompositionViewHandler extends BaseHandler implements IUcCompositionViewHandler {
    @Override
    public void addActorMapping(COREModelComposition composition) {
        UseCaseControllerFactory.INSTANCE.getMappingsController().createActorMapping(composition);        
    }

    @Override
    public void addUseCaseMapping(COREModelComposition composition) {
        UseCaseControllerFactory.INSTANCE.getMappingsController().createUseCaseMapping(composition);        
    }
    
    @Override
    public void hideMappingDetails(CompositionView<?> compositionView) {
        compositionView.hideMappingDetails();
    }
    
    @Override
    public boolean processTapEvent(TapEvent tapEvent) {
        // we don't want anything to happen when we tap in an empty area in composition view
        return true;
    }
    
    @Override
    public void showExternalModelOfComposition(CompositionView<IUcCompositionViewHandler> compositionView) {
        COREModelComposition modelComposition = compositionView.getModelComposition();
        Aspect source = (Aspect) modelComposition.getSource();
        boolean splitCompositionModeEnabled = isSplitViewEnabled(RamApp.getActiveAspectScene());
        if (!splitCompositionModeEnabled) {
            SceneCreationAndChangeFactory.getFactory().navigateToModel(source, CurrentMode.EDIT);
        } else {
            RamApp.getActiveAspectScene().getHandler().back(RamApp.getActiveAspectScene());
            SceneCreationAndChangeFactory.getFactory().navigateToModel(source, CurrentMode.EDIT);
        }
        
    }
    
    @Override
    public void showMappingDetails(CompositionView<?> compositionView) {
        compositionView.showMappingDetails();
    }
    
    @Override
    public void switchToSplitView(CompositionView<?> compositionView) {
        // Get the latest used display aspect before switching into composition edit mode
        
        RamAbstractScene<?> activeScene = RamApp.getActiveScene();
        boolean splitCompositionModeEnabled = activeScene != null && isSplitViewEnabled(activeScene);
        
        // Get the handler of the display aspect scene to call its function for switching into composition edit mode.
        IDisplaySceneHandler handler = UseCaseModelHandlerFactory.INSTANCE.getUseCaseDiagramDisplaySceneHandler();
        
        if (splitCompositionModeEnabled) {
            // we are already in split composition editing mode , do the opposite..
            handler.closeSplitView(activeScene);
        } else {
            // trigger the mode switching
            handler.switchToCompositionEditMode(activeScene, compositionView);
        }
    }
    
    /**
     * Returns whether split view is currently enabled.
     * 
     * @param scene the current {@link RamAbstractScene}
     * @return true, whether split view is enabled, false otherwise
     */
    private static boolean isSplitViewEnabled(RamAbstractScene<?> scene) {
        return scene.getCurrentView() instanceof CompositionSplitEditingView;
    }
    

    @Override
    public void showExternalAspectOfComposition(CompositionView<?> myCompositionView) {
        // TODO Auto-generated method stub
        
    }  
}
