package ca.mcgill.sel.usecases.ui.views;

import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.ram.ui.components.RamListComponent.Namer;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.usecases.Flow;
import ca.mcgill.sel.usecases.Step;
import ca.mcgill.sel.usecases.util.UseCaseTextUtils;

/**
     * A namer that is capable of displaying different objects depending on the choice of values, i.e., {@link EObject}
     * s, enums or primitive objects.
     * @author ryanl
     */
public class StepNamer implements Namer<Step> {
    
    private static final int MAX_TEXT_LENGTH = 25;
        
    @Override
    public RamRectangleComponent getDisplayComponent(Step element) {
        RamTextComponent view = null;
        Flow flow = (Flow) element.eContainer();
        String stepText = UseCaseTextUtils.getFlowStepNumberText(flow)
                 + UseCaseTextUtils.getStepText(flow, element);
        
        if (stepText.length() > MAX_TEXT_LENGTH) {
            stepText = stepText.substring(0, MAX_TEXT_LENGTH - 3) + "...";
        }        
        
        view = new RamTextComponent(stepText);            

        view.setNoFill(false);
        view.setFillColor(Colors.DEFAULT_ELEMENT_FILL_COLOR);
        view.setNoStroke(false);
        return view;
    }

    @Override
    public String getSortingName(Step element) {
        Flow flow = (Flow) element.eContainer();
        return UseCaseTextUtils.getFlowStepNumberText(flow)
                 + UseCaseTextUtils.getSortingText(flow, element);
    }

    @Override
    public String getSearchingName(Step element) {
        Flow flow = (Flow) element.eContainer();
        return UseCaseTextUtils.getFlowStepNumberText(flow)
                 + UseCaseTextUtils.getStepText(flow, element);
    }

}