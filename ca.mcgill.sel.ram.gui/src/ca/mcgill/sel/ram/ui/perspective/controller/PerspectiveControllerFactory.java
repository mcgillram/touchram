package ca.mcgill.sel.ram.ui.perspective.controller;

import ca.mcgill.sel.core.perspective.ActionValidator;

/**
 * A factory to obtain all perspectiveControllers related to COREPerspective.
 * @author hyacinthali
 *
 */
public final class PerspectiveControllerFactory {

    /**
     * The singleton instance of this factory.
     * 
     */
    public static final PerspectiveControllerFactory INSTANCE = new PerspectiveControllerFactory();

    private ActionValidator actionValidator;
    private ReexposedClassDiagramAction cdmPerspectiveController;
    private DomainDesignModelController domainDesignModelController;
    private DomainDesignUseCaseController domainDesignUseCaseController;
    private DomainUseCaseController domainUseCaseController;
    private BasePerspectiveController basePerspectiveController;
    private RedefinedClassDiagramAction redefinedClassDiagramAction;
    private RedefinedUseCaseAction redefinedUseCaseAction;
    private RedefinedOperationModelAction redefinedOperationModelAction;
    private RedefinedEnvironmentModelAction redefinedEnvironmentModelAction;

    /**
     * Creates a new instance of {@link PerspectiveControllerFactory}.
     */
    private PerspectiveControllerFactory() {

    }

    /**
     * Returns the controller for {@link ActionValidator}s.
     *
     * @return the controller for {@link ActionValidator}s
     */
    public ActionValidator getActionValidator() {
        if (actionValidator == null) {
            actionValidator = new ActionValidator();
        }
        return actionValidator;
    }

    /**
     * Returns the controller for {@link DomainDesignModelController}s.
     *
     * @return the controller for {@link DomainDesignModelController}s
     */
    public DomainDesignModelController getDomainDesignModelController() {
        if (domainDesignModelController == null) {
            domainDesignModelController = new DomainDesignModelController();
        }
        return domainDesignModelController;
    }


    /**
     * Returns the controller for {@link ReexposedClassDiagramAction}s.
     *
     * @return the controller for {@link ReexposedClassDiagramAction}s
     */
    public ReexposedClassDiagramAction getCdmPerspectiveController() {
        if (cdmPerspectiveController == null) {
            cdmPerspectiveController = new ReexposedClassDiagramAction();
        }
        return cdmPerspectiveController;
    }

    /**
     * Returns the controller for {@link DomainDesignUseCaseController}s.
     *
     * @return the controller for {@link DomainDesignUseCaseController}s
     */
    public DomainDesignUseCaseController getDomainDesignUseCaseController() {
        if (domainDesignUseCaseController == null) {
            domainDesignUseCaseController = new DomainDesignUseCaseController();
        }
        return domainDesignUseCaseController;
    }

    /**
     * Returns the controller for {@link DomainUseCaseController}s.
     *
     * @return the controller for {@link DomainUseCaseController}s
     */
    public DomainUseCaseController getDomainUseCaseController() {
        if (domainUseCaseController == null) {
            domainUseCaseController = new DomainUseCaseController();
        }
        return domainUseCaseController;
    }

    /**
     * Returns the controller for {@link BasePerspectiveController}s.
     *
     * @return the controller for {@link BasePerspectiveController}s
     */
    public BasePerspectiveController getBasePerspectiveController() {
        if (basePerspectiveController == null) {
            basePerspectiveController = new BasePerspectiveController();
        }
        return basePerspectiveController;
    }

    /**
     * Returns the controller for {@link RedefinedAModelActionGen}s.
     *
     * @return the controller for {@link RedefinedAModelActionGen}s
     */
    public RedefinedClassDiagramAction getRedefinedClassDiagramAction() {
        if (redefinedClassDiagramAction == null) {
            redefinedClassDiagramAction = new RedefinedClassDiagramAction();
        }
        return redefinedClassDiagramAction;
    }

    /**
     * Returns the controller for {@link RedefinedUseCaseAction}s.
     *
     * @return the controller for {@link RedefinedUseCaseAction}s
     */
    public RedefinedUseCaseAction getRedefinedUseCaseAction() {
        if (redefinedUseCaseAction == null) {
            redefinedUseCaseAction = new RedefinedUseCaseAction();
        }
        return redefinedUseCaseAction;
    }

    /**
     * Returns the controller for {@link RedefinedOperationModelAction}s.
     *
     * @return the controller for {@link RedefinedOperationModelAction}s
     */
    public RedefinedOperationModelAction getRedefinedOperationModelAction() {
        if (redefinedOperationModelAction == null) {
            redefinedOperationModelAction = new RedefinedOperationModelAction();
        }
        return redefinedOperationModelAction;
    }

    /**
     * Returns the controller for {@link RedefinedEnvironmentModelAction}s.
     *
     * @return the controller for {@link RedefinedEnvironmentModelAction}s
     */
    public RedefinedEnvironmentModelAction getRedefinedEnvironmentModelAction() {
        if (redefinedEnvironmentModelAction == null) {
            redefinedEnvironmentModelAction = new RedefinedEnvironmentModelAction();
        }
        return redefinedEnvironmentModelAction;
    }
}
