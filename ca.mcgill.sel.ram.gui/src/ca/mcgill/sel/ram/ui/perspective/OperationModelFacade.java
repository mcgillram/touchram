package ca.mcgill.sel.ram.ui.perspective;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.classdiagram.CdmPackage;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.environmentmodel.EmPackage;
import ca.mcgill.sel.environmentmodel.EnvironmentModel;
import ca.mcgill.sel.environmentmodel.language.controller.EnvironmentModelControllerFactory;
import ca.mcgill.sel.operationmodel.OperationSchema;
import ca.mcgill.sel.operationmodel.language.controller.OperationModelControllerFactory;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.operationmodel.Actor;

/**
 * This class contains methods, each calls a corresponding operation model
 * language action to create new element.
 * 
 * @author hyacinth
 *
 */
public final class OperationModelFacade {

    private OperationModelFacade() {
        
    }
    
    /**
     * Creates a new actor in operation schema.
     * 
     * @param perspective - the perspective under development
     * @param roleName - the role name of the model to contain the new element.
     * @param otherOwner - the container of the corresponding element to be
     * mapped with the new actor.
     * @param name - the name of the new actor element
     * @return the created element
     */
    protected static EObject createActor(COREPerspective perspective, String roleName, EObject otherOwner,
            String name) {
        EObject actorOwner = FacadeAction.getOwner(perspective, otherOwner, roleName);

        if (actorOwner == null) {
            return null;
        }

        OperationSchema ownerActor = (OperationSchema) actorOwner;
        List<EObject> elements = new ArrayList<EObject>();
        List<EObject> initialElements = new ArrayList<EObject>();
        initialElements.addAll(ownerActor.getActors());
        OperationModelControllerFactory.INSTANCE.getOperationModelController().createActor(ownerActor, name);
        elements.addAll(ownerActor.getActors());
        elements.removeAll(initialElements);

        return elements.get(0);
    }

    /**
     * Creates a new classifier in operation schema.
     * 
     * @param perspective - the perspective under development
     * @param roleName - the role name of the model to contain the new element.
     * @param otherOwner - the container of the corresponding element to be
     * mapped with the new actor.
     * @param name - the name of the new class element
     * @return the created element
     */
    protected static EObject createClassifier(COREPerspective perspective, String roleName, EObject otherOwner,
            String name) {
        EObject owner = FacadeAction.getOwner(perspective, otherOwner, roleName);

        if (owner == null) {
            return null;
        }

        OperationSchema classifierOwner = (OperationSchema) owner;
        List<EObject> elements = new ArrayList<EObject>();
        List<EObject> initialElements = new ArrayList<EObject>();
        initialElements.addAll(classifierOwner.getScope());
        OperationModelControllerFactory.INSTANCE.getOperationModelController().createClass(classifierOwner, name);
        elements.removeAll(initialElements);

        // TODO: handling of complex action, i.e., when another element is
        // created because of creating
        // the main element. May the facade action methods should return a list
        // of the created elements.
        return elements.get(0);
    }

    /**
     * Creates a new input message in operation schema.
     * 
     * @param perspective
     * @param roleName - the role name of the model to contain the new element.
     * @param otherOwner - the container of the corresponding element to be
     * mapped with the new actor.
     * @param name - the name of the new message
     * @return the created element
     */
    protected static EObject createInputMessage(COREPerspective perspective, String roleName, EObject otherOwner,
            String name) {
        EObject owner = FacadeAction.getOwner(perspective, otherOwner, roleName);

        if (owner == null) {
            return null;
        }

        OperationSchema messageOwner = (OperationSchema) owner;
        List<EObject> elements = new ArrayList<EObject>();
        List<EObject> initialElements = new ArrayList<EObject>();
        initialElements.addAll(messageOwner.getMessages());
        OperationModelControllerFactory.INSTANCE.getOperationModelController().createMessage(messageOwner, null, name,
                true);

        // TODO: handling of complex action, i.e., when another element is
        // created because of creating
        // the main element. May the facade action methods should return a list
        // of the created elements.
        return elements.get(0);
    }

    /**
     * Creates a new output message in operation schema.
     * 
     * @param perspective
     * @param roleName - the role name of the model to contain the new element.
     * @param otherOwner - the container of the corresponding element to be
     * mapped with the new actor.
     * @param actor - the actor
     * @param name - the name of the new message
     * @return the created element
     */
    protected static EObject createOutputMessage(COREPerspective perspective, String roleName, EObject otherOwner,
            Actor actor, String name) {
        EObject owner = FacadeAction.getOwner(perspective, otherOwner, roleName);

        if (owner == null) {
            return null;
        }

        OperationSchema messageOwner = (OperationSchema) owner;
        List<EObject> elements = new ArrayList<EObject>();
        List<EObject> initialElements = new ArrayList<EObject>();
        initialElements.addAll(messageOwner.getMessages());
        OperationModelControllerFactory.INSTANCE.getOperationModelController().createMessage(messageOwner, actor, name,
                false);

        // TODO: handling of complex action, i.e., when another element is
        // created because of creating
        // the main element. May the facade action methods should return a list
        // of the created elements.
        return elements.get(0);
    }

    public static EObject createOMActorAndOthers(COREPerspective perspective,
            String toCreateRoleName, EObject metaclass,
            EObject owner, String name) {

        EObject newElement = null;

        if (metaclass.equals(EmPackage.eINSTANCE.getActor())) {
            EnvironmentModelFacade.createActor(perspective, toCreateRoleName, owner, name, 0, 10, 0, 10);
        } else if (metaclass.equals(UcPackage.eINSTANCE.getActor())) {
            newElement = UseCaseFacade.createActor(perspective, toCreateRoleName, owner, name, 0, 0);
        } else if (metaclass.equals(CdmPackage.eINSTANCE.getClass_())) {
            newElement = ClassDiagramFacade.createClass(perspective, owner, toCreateRoleName, name, false, 0, 0);
        }

        return newElement;
    }

}
