package ca.mcgill.sel.ram.ui.perspective;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.classdiagram.Class;
import ca.mcgill.sel.classdiagram.ClassDiagram;
import ca.mcgill.sel.classdiagram.ObjectType;
import ca.mcgill.sel.classdiagram.language.controller.ClassDiagramController;
import ca.mcgill.sel.classdiagram.language.controller.ControllerFactory;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.ram.ui.perspective.controller.PerspectiveException;

/**
 * This class contains methods, each calls a corresponding class diagram language action to create elements.
 * @author hyacinth
 *
 */
public final class ClassDiagramFacade {

    private ClassDiagramFacade() {
        
    }
    
    /**
     * Creates a new class element.
     * 
     * @param perspective - the perspective of the model.
     * @param otherOwner - the container of the model element to be mapped with the new class.
     * @param roleName - role of the model to contain the new element.
     * @param name - name of the class to create.
     * @param dataType
     * @param x
     * @param y
     * @return the newly created model element.
     */
    protected static EObject createClass(COREPerspective perspective, EObject otherOwner, String roleName,
            String name, boolean dataType, float x, float y) {

        EObject classOwner = FacadeAction.getOwner(perspective, otherOwner, roleName);

        if (classOwner == null) {
            return null;
        }

        ClassDiagram classDiagram = (ClassDiagram) classOwner;
        List<EObject> elements = new ArrayList<EObject>();
        List<EObject> initialElements = new ArrayList<EObject>();
        initialElements.addAll(classDiagram.getClasses());
        ControllerFactory.INSTANCE.getClassDiagramController().createNewClass(
                (ClassDiagram) classOwner, name, dataType, false, x, y);

        elements.addAll(classDiagram.getClasses());
        elements.removeAll(initialElements);

        return elements.get(0);
    }

    public static EObject createClass(COREPerspective perspective, String toCreateRoleName,
            HashMap<Object, Object> parameters) {

        String roleName = String.valueOf(parameters.get("roleName"));
        EObject otherOwner = (EObject) parameters.get("owner");
        EObject owner = FacadeAction.getOwner(perspective, otherOwner, roleName);

        if (owner == null) {
            return null;
        }

        ClassDiagram classOwner = (ClassDiagram) owner;
        List<EObject> elements = new ArrayList<EObject>();
        List<EObject> initialElements = new ArrayList<EObject>();

        java.lang.Class<ClassDiagramController> controllerClass = ClassDiagramController.class;
        Method createClassMethod = null;
        try {
            createClassMethod = controllerClass.getMethod("createNewClass",
                    ClassDiagram.class, String.class, boolean.class, float.class, float.class);
        } catch (NoSuchMethodException | SecurityException e) {
            throw new PerspectiveException("Error: The method create clas can't be retrieved");
        }
        Object name = parameters.get("name");
        Object dataType = parameters.get("dataType");
        Object x = parameters.get("x");
        Object y = parameters.get("y");
        ClassDiagramController classDiagramController = ControllerFactory.INSTANCE.getClassDiagramController();

        initialElements.addAll(classOwner.getClasses());
        if (createClassMethod != null) {
            try {
                createClassMethod.invoke(classDiagramController, classOwner, name, dataType, x, y);
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                throw new PerspectiveException("Error: The method create class can't be invoked");
            }
        }

        elements.addAll(classOwner.getClasses());
        elements.removeAll(initialElements);

        return elements.get(0);
    }

    /**
     * Creates new class attribute.
     * @param perspective 
     * @param owner - the container of the element to be mapped with the attribute.
     * @param roleName - role name of the model to contain the new attribute.
     * @param name
     * @param objectType
     * 
     * @return the new attribute element
     * @throws PerspectiveException
     */
    protected static EObject createAttribute(COREPerspective perspective, EObject owner, String roleName, String name, 
            ObjectType objectType) throws PerspectiveException {

        EObject attributeOwner = FacadeAction.getOwner(perspective, owner, roleName);

        if (attributeOwner == null) {
            throw new PerspectiveException("The container of the model element need to be mapped first");
        }

        Class clazzOwner = (Class) attributeOwner;
        List<EObject> elements = new ArrayList<EObject>();
        List<EObject> initialElements = new ArrayList<EObject>();
        initialElements.addAll(clazzOwner.getAttributes());

        // The attribute size is maximum index + 1
        int index = clazzOwner.getAttributes().size();
        ControllerFactory.INSTANCE.getClassController().createAttribute(clazzOwner, index, name, objectType);

        elements.addAll(clazzOwner.getAttributes());
        elements.removeAll(initialElements);

        return elements.get(0);
    }


}
