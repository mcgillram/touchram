package ca.mcgill.sel.ram.ui.perspective.controller;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public final class TestReflection {

    private TestReflection() {
        
    }
    
    public static void main(String[] args) throws NoSuchMethodException, SecurityException {
        Method method = TestReflectionMethods.class.getMethod("testReflection", List.class);
        List<Object> values = new ArrayList<Object>();
        TestReflectionMethods testReflectionMethods = new TestReflectionMethods();
        int x = 1;
        values.add(x);
        values.add(2);
        values.add("name");
        try {
            method.invoke(testReflectionMethods, values);
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        System.out.println(true);

        Method method2 = TestReflectionMethods.class.getMethod("testReflection2", HashMap.class);
        HashMap<Object, Object> testValues = new HashMap<Object, Object>();
        testValues.put("one", 1);
        testValues.put(2, 1);
        testValues.put("three", "five");

        try {
            method2.invoke(testReflectionMethods, testValues);
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        System.out.println(true);
    }

}
