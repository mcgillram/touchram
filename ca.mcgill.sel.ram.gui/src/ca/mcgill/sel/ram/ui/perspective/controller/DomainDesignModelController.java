package ca.mcgill.sel.ram.ui.perspective.controller;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import ca.mcgill.sel.classdiagram.CdmPackage;
import ca.mcgill.sel.classdiagram.ClassDiagram;
import ca.mcgill.sel.classdiagram.Classifier;
import ca.mcgill.sel.core.CORELanguageElement;
import ca.mcgill.sel.core.CORELanguageElementMapping;
import ca.mcgill.sel.core.COREModelElementMapping;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.COREScene;
import ca.mcgill.sel.core.MappingEnd;
import ca.mcgill.sel.core.perspective.ActionType;
import ca.mcgill.sel.core.perspective.COREPerspectiveUtil;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.ram.ui.perspective.PerspectiveHelper;
import ca.mcgill.sel.ram.ui.perspective.QueryAction;

public class DomainDesignModelController {

//    List<EObject> updatedElements;
//    List<EObject> deleteElements;
//    List<COREModelElementMapping> deleteMappings;
//
//    /**
//     * Creates a new instance of {@link DomainDesignModelController}.
//     */
//    public DomainDesignModelController() {
//        updatedElements = new ArrayList<EObject>();
//        deleteElements = new ArrayList<EObject>();
//        deleteMappings = new ArrayList<COREModelElementMapping>();
//    }
//
//    // /**
//    // * Compulsory Optional template (from 1 -- 0..1 to) Template.
//    // *
//    // * @param perspective
//    // * @param owner
//    // * @param name
//    // * @param dataType
//    // * @param x
//    // * @param y
//    // */
//    // public void createNewDomainModelClass(ClassDiagram owner, String name,
//    // boolean dataType,
//    // float x, float y, COREPerspective perspective) {
//    // // Ask user whether to create a mapping
//    // //QueryAction.INSTANCE.askZeroOrOne();
//    // boolean map = QueryAction.INSTANCE.isCreateOtherElement();
//    // EObject newDomainModelClass =
//    // (EObject) UseCaseControllerFactory.INSTANCE.getClassDiagramController().createNewClass(owner,
//    // name, dataType, x, y);
//    // if (map) {
//    // // check if the corresponding owner model exists.
//    // EObject designCdm = null;
//    // for (COREModelElementMapping mapping : getMappings(perspective, MAPPING_IDENTIFIER,
//    // owner)) {
//    // designCdm = getElementOther(mapping, owner);
//    // break;
//    // }
//    //
//    // if (designCdm == null) {
//    // designCdm = createClassDiagramAndMappings(perspective, owner, 1);
//    // }
//    // createOtherElementsAndMappings(perspective, newDomainModelClass, 1, (ClassDiagram) designCdm,
//    // name, dataType, x, y);
//    // }
//    // }
//    //
//    // public void createNewDesignModelClass(ClassDiagram owner, String name,
//    // boolean dataType, float x, float y, COREPerspective perspective) {
//    // EObject newDesignModelClass = (EObject) UseCaseControllerFactory.INSTANCE.getClassDiagramController()
//    // .createNewClass(owner, name, dataType, x, y);
//    //
//    // // check if the corresponding owner model exists.
//    // EObject domainCdm = null;
//    // for (COREModelElementMapping mapping : getMappings(perspective, MAPPING_IDENTIFIER,
//    // owner)) {
//    // domainCdm = getElementOther(mapping, owner);
//    // break;
//    // }
//    // if (domainCdm != null) {
//    // // CHALLENGE: code generation may not handle this efficiently,
//    // // A user may be required to edit the generate code
//    // createFirstMappingOrElementAndMapping(perspective, MAPPING_IDENTIFIER, newDesignModelClass,
//    // DOMAIN_MODEL_ROLENAME, (ClassDiagram) domainCdm, name, dataType, x, y);
//    // }
//    // }
//    //
//    // ===============
//
//    public void createNewClass(COREPerspective perspective, String currentRole, ClassDiagram owner,
//            String name, boolean dataType, float x, float y) {
//        // creates new class
//        EObject newClass = (EObject) UseCaseControllerFactory.INSTANCE.getClassDiagramController().createNewClass(owner,
//                name, dataType, x, y);
//
//        int numberOfMappings;
//        List<Object> parameters = new ArrayList<Object>();
//        EObject languageElement = CdmPackage.eINSTANCE.getClass_();
//        
//        Set<CORELanguageElementMapping> instances = PerspectiveHelper.getInstance()
//                .getLanguageElementMappings(perspective, languageElement);
//
//        for (CORELanguageElementMapping mappingType : instances) {
//
//            MappingEnd fromMappingEnd = mappingType.getMappingEnds().get(0);
//            MappingEnd toMappingEnd = mappingType.getMappingEnds().get(1);
////            CORELanguageElement fromLanguageElement = fromMappingEnd.getLanguageElement();
////            CORELanguageElement toLanguageElement = toMappingEnd.getLanguageElement();
//
//            boolean isFrom = fromMappingEnd.getRoleName().equals(currentRole);
//            switch (PerspectiveHelper.getInstance().getCreateType(mappingType, isFrom)) {
//                // C1.1
//                case CREATE_1_1:
//                    numberOfMappings = QueryAction.INSTANCE.askZeroOrOne();
//                    // elementHere = C(h);
//                    if (numberOfMappings > 0) {
//                        EObject ownerOther = null;
//                        for (COREModelElementMapping mapping : PerspectiveHelper.getInstance()
//                                .getMappings(perspective, mappingType, owner)) {
//                            ownerOther = PerspectiveHelper.getInstance().getElementOther(mapping, owner);
//                            break;
//                        }
//                        ownerOther = (ClassDiagram) ownerOther;
//                        parameters.add(ownerOther);
//                        parameters.add(name);
//                        parameters.add(dataType);
//                        parameters.add(x);
//                        parameters.add(y);
//                        try {
//                            PerspectiveHelper.getInstance().createOtherElementsAndMappings(perspective, mappingType,
//                                    newClass, 1, parameters);
//                        } catch (ClassNotFoundException | IllegalAccessException | IllegalArgumentException
//                                | InvocationTargetException | InstantiationException | NoSuchMethodException
//                                | SecurityException e) {
//                            // TODO Display something to the user
//                            e.printStackTrace();
//                        }
//                    }
//                    break;
//
//                // // C2.1
//                // case CREATE:
//                // createOtherElementsAndMappings(typeMapping, elementHere, 1);
//                // break;
//                //
//                // // C3.1
//                // case CAN_CREATE_MANY:
//                // numberOfMappings = askMany(otherLanguageElement);
//                // // elementHere = C(h);
//                // createOtherElementsAndMappings(typeMapping, elementHere, numberOfMappings);
//                // break;
//                //
//                // // C4.1
//                // case CREATE_AT_LEAST_ONE:
//                // numberOfMappings = askAtLeastOne(otherLanguageElement);
//                // // elementHere = C(h);
//                // createOtherElementsAndMappings(typeMapping, elementHere, numberOfMappings);
//                // break;
//                //
//                // // C5.1
//                // case CAN_CREATE_OR_USE_NON_MAPPED:
//                // numberOfMappings = askZeroOrOne(otherLanguageElement);
//                // // elementHere = C(h);
//                // if (numberOfMappings == 1) {
//                // createFirstMappingOrElementAndMapping(typeMapping, elementHere);
//                // }
//                // break;
//                //
//                // // C6.1
//                // case CAN_CREATE_OR_USE:
//                // numberOfMappings = askZeroOrOne(otherLanguageElement);
//                // // elementHere = C(h);
//                // if (numberOfMappings == 1) {
//                // createMappingOrElementAndMapping(typeMapping, elementHere);
//                // }
//                // break;
//                //
//                // // C8.1
//                // case CREATE_OR_USE_AT_LEAST_ONE:
//                // numberOfMappings = askAtLeastOne(otherLanguageElement);
//                // // elementHere = C(h);
//                // createMappingsAndElementsAndMappings(typeMapping, elementHere, numberOfMappings);
//                //
//                // break;
//                //
//                // // C9.1
//                // case CAN_CREATE_OR_USE_MANY:
//                // numberOfMappings = askMany(otherLanguageElement);
//                // // elementHere = C(h);
//                // createMappingsAndElementsAndMappings(typeMapping, elementHere, numberOfMappings);
//                // break;
//
//                // C1.2
//                case CREATE_1_2:
//                    // gather the parameters
//                    EObject ownerOther = null;
//                    for (COREModelElementMapping mapping : PerspectiveHelper.getInstance().getMappings(perspective,
//                            mappingType,
//                            owner)) {
//                        ownerOther = PerspectiveHelper.getInstance().getElementOther(mapping, owner);
//                        break;
//                    }
//                    ownerOther = (ClassDiagram) ownerOther;
//                    parameters.add(ownerOther);
//                    parameters.add(name);
//                    parameters.add(dataType);
//                    parameters.add(x);
//                    parameters.add(y);
//                    try {
//                        PerspectiveHelper.getInstance().createFirstMappingOrElementAndMapping(perspective,
//                                mappingType, newClass, fromMappingEnd.getRoleName(), fromLanguageElement,
//                                parameters);
//                    } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
//                            | ClassNotFoundException | InstantiationException | NoSuchMethodException
//                            | SecurityException e) {
//                        // TODO Display something to the user
//                        e.printStackTrace();
//                    }
//
//                    // // C3.2
//                    // case CREATE_OR_USE:
//                    // createMappingOrElementAndMapping(mappingType, newClass);
//                    //
//                    // // C6.2
//                    // case CREATE_OR_USE_NON_MAPPED_AT_LEAST_ONE:
//                    // numberOfMappings = askAtLeastOne(otherLanguageElement);
//                    // // elementHere = C(h);
//                    // createFirstMappingsAndElementsAndMappings(mappingType, newClass, numberOfMappings);
//                    //
//                    // // C7.2
//                    // case CAN_CREATE_OR_USE_NON_MAPPED_MANY:
//                    // numberOfMappings = askMany(otherLanguageElement);
//                    // // elementHere = C(h);
//                    // createFirstMappingsAndElementsAndMappings(mappingType, elementHere, numberOfMappings);
//
//            }
//        }
//    }
//
//    // =============
//
//    // public void deleteDomainModelClass(COREPerspective perspective, Classifier classifier) {
//    // List<COREModelElementMapping> mappings = getMappings(perspective, MAPPING_IDENTIFIER, classifier);
//    // for (COREModelElementMapping mapping : mappings) {
//    // EObject designClassifier = getElementOther(mapping, classifier);
//    // UseCaseControllerFactory.INSTANCE.getClassDiagramController().removeClassifier((Classifier) designClassifier);
//    // }
//    // CORELanguageElementMapping mappingType = getMappingType(perspective, MAPPING_IDENTIFIER);
//    // mappingType.getInstances().removeAll(mappings);
//    // UseCaseControllerFactory.INSTANCE.getClassDiagramController().removeClassifier(classifier);
//    // }
//
////     public void deleteDesignModelClass(COREPerspective perspective, Classifier classifier) {
////     List<COREModelElementMapping> mappings = getMappings(perspective, MAPPING_IDENTIFIER, classifier);
////     CORELanguageElementMapping mappingType = getMappingType(perspective, MAPPING_IDENTIFIER);
////     mappingType.getInstances().removeAll(mappings);
////     UseCaseControllerFactory.INSTANCE.getClassDiagramController().removeClassifier(classifier);
////     }
//
//    public void deleteClassifier(COREPerspective perspective, String currentRole, Classifier classifier) {
//        deleteElements.clear();
//        deleteMappings.clear();
//        for (CORELanguageElementMapping mappingType : PerspectiveHelper.getInstance()
//                .getLanguageElementMappings(perspective, classifier.eClass())) {
//            
//        }
//        deleteElements(perspective, currentRole, classifier);
//        COREScene scene = COREArtefactUtil.getReferencingExternalArtefact(classifier).getScene();
//        scene.getElementMappings().removeAll(deleteMappings);
//        for (COREModelElementMapping mapping : deleteMappings) {
//            EcoreUtil.delete(mapping);
//        }
//        // this need to be generic, hard coded for now
//        for (EObject obj : deleteElements) {
//            UseCaseControllerFactory.INSTANCE.getClassDiagramController().removeClassifier((Classifier) obj);
//        }
//        
//        deleteElements.clear();
//        deleteMappings.clear();
//    }
//
//    private void deleteElements(COREPerspective perspective, String currentRole, EObject element) {
//        //retrieve all mappings, irrespective of a type
//        List<COREModelElementMapping> mappings = COREPerspectiveUtil.INSTANCE.getMappings(element);
//        for (COREModelElementMapping mapping : mappings) {
//
//            if (!deleteElements.contains(element)) {
//                deleteElements.add(element);
//            }
//            if (!deleteMappings.contains(mapping)) {
//                deleteMappings.add(mapping);
//            }
//
//            CORELanguageElementMapping mappingType = COREPerspectiveUtil.INSTANCE.getMappingType(perspective, mapping);
//            ActionType deleteType = null;
//            MappingEnd fromMappingEnd = mappingType.getMappingEnds().get(0);
//            MappingEnd toMappingEnd = mappingType.getMappingEnds().get(1);
//            
//            //retrieve mappings with a given type
//            List<COREModelElementMapping> typeInstances = COREPerspectiveUtil.INSTANCE.getMappings(mappingType, 
//                    element);
//
//            if (fromMappingEnd.getRoleName().equals(currentRole)) {
//                deleteType = PerspectiveHelper.getInstance().getDeleteType(fromMappingEnd);
//            } else {
//                deleteType = PerspectiveHelper.getInstance().getDeleteType(toMappingEnd);
//            }
//            switch (deleteType) {
//
//                case DELETE_OTHERS:
//                    for (COREModelElementMapping typeInstance : typeInstances) {
//                        EObject elementOther = PerspectiveHelper.getInstance().getElementOther(typeInstance, element);
//                        if (!deleteElements.contains(elementOther)) {
//                            String otherRoleName = COREPerspectiveUtil.INSTANCE.getOtherRoleName(perspective, 
//                                    currentRole);
//                            deleteElements(perspective, otherRoleName, elementOther);
//                        }
//                    }
//                    break;
//
//                case DELETE_SINGLEMAPPED:
//                    for (COREModelElementMapping typeInstance : typeInstances) {
//                        EObject elementOther = PerspectiveHelper.getInstance().getElementOther(typeInstance, element);
//                        List<COREModelElementMapping> elementOtherMappings = COREPerspectiveUtil.INSTANCE.getMappings(
//                                mappingType, elementOther);
//                        if (!deleteElements.contains(elementOther) && elementOtherMappings.size() == 1) {
//                            String otherRoleName = COREPerspectiveUtil.INSTANCE.getOtherRoleName(perspective, 
//                                    currentRole);
//                            deleteElements(perspective, otherRoleName, elementOther);
//                        }
//                    }
//                    break;
//            }
//
//        }
//    }

}
