package ca.mcgill.sel.ram.ui.views.handler.impl;

import org.eclipse.emf.ecore.EObject;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;

import ca.mcgill.sel.ram.ui.components.navigationbar.NavigationBarMenuElement;
import ca.mcgill.sel.ram.ui.events.listeners.ITapAndHoldListener;
import ca.mcgill.sel.ram.ui.views.TextView;

public class TextViewHandlerNavBar extends TextViewHandler implements ITapAndHoldListener {
    
    private NavigationBarMenuElement<EObject> element;
    private EObject parentObject;
    private boolean reuse;
    
    public TextViewHandlerNavBar(NavigationBarMenuElement<EObject> elem, EObject parentEObject, boolean reuse) {
        element = elem;
        parentObject = parentEObject;
        this.reuse = reuse;
    }
    
    @Override
    public boolean processTapEvent(TapEvent tapEvent) {
        boolean returnValue = super.processTapEvent(tapEvent);
        if (!returnValue) {
            if (tapEvent.isTapped()) {
                TextView target = (TextView) tapEvent.getTarget();
                element.callNamer(target.getData(), parentObject, reuse);
                return true;
            }
            return false;
        }
        return true;
    }

    @Override
    public boolean processTapAndHoldEvent(TapAndHoldEvent tapAndHoldEvent) {
        return true;
    }

}
