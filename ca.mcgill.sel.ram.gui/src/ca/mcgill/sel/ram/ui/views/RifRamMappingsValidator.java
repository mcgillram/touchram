package ca.mcgill.sel.ram.ui.views;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREModelElementMapping;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.Operation;
import ca.mcgill.sel.ram.Parameter;
import ca.mcgill.sel.ram.StructuralView;
import ca.mcgill.sel.ram.Type;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.scenes.TrafoUtils;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.restif.AccessMethod;
import ca.mcgill.sel.restif.DynamicFragment;
import ca.mcgill.sel.restif.PathFragment;
import ca.mcgill.sel.restif.Resource;
import ca.mcgill.sel.restif.RestIF;

/**
 * Implementation of {@link CoreModelElementMappingsValidator} for {@link RifRamSplitViewWithMappings}.
 * 
 * @author Bowen
 */
public class RifRamMappingsValidator implements CoreModelElementMappingsValidator {
    
    //TODO: perhaps there's a better way to check for valid primitive types for mappings
    private static final String[] VALID_PRIMITIVE_TYPES_FOR_MAPPINGS = {"String", "int", "Integer", "long", "Long", 
        "boolean", "Boolean", "float", "Float", "double", "Double", "char", "Character"};
    
    public RifRamMappingsValidator() {
        
    }
    
    @Override
    public boolean validateNewMapping(EObject firstModelElement, EObject secondModelElement, 
            List<COREModelElementMapping> mappings, COREConcern concern) {
        if (firstModelElement instanceof DynamicFragment && secondModelElement instanceof Parameter 
                || firstModelElement instanceof Parameter && secondModelElement instanceof DynamicFragment) {
            
            // set parameter and dynamic fragment of input mapping
            Parameter parameter = null;
            DynamicFragment dynamicFragment = null;
            
            if (firstModelElement instanceof Parameter) {
                parameter = (Parameter) firstModelElement;
                dynamicFragment = (DynamicFragment) secondModelElement;
            } else {
                parameter = (Parameter) secondModelElement;
                dynamicFragment = (DynamicFragment) firstModelElement;
            }
            
            Type typeOfParameter = parameter.getType();          
            String typeName = typeOfParameter.getName();
            if (!isPrimitiveType(typeName)) {
                RamApp.getActiveScene().displayPopup(Strings.POPUP_MAPPING_RIFRAM_NOT_PRIMITIVE);
                return false;
            }

            // get the operation of the input parameter
            Operation operationOfParameter = (Operation) parameter.eContainer();
            
            // get the operation-access-method mapping associated with the operation, and find the associated
            // accessmethod's path fragment
            PathFragment pathFragmentOfOperationMapping = null;
            for (COREModelElementMapping mapping : mappings) {
                // extract model elements of mapping
                EObject mappingObjectOne = mapping.getModelElements().get(0);
                EObject mappingObjectTwo = mapping.getModelElements().get(1);
                
                // enforce only 1 mapping per parameter
                if (mappingObjectOne instanceof Parameter || mappingObjectTwo instanceof Parameter) {
                    Parameter mappedParameter = getParameterFromParameterDynamicFragmentMapping(mapping);   
                    if (mappedParameter.equals(parameter)) {
                        RamApp.getActiveScene().displayPopup(Strings.POPUP_MAPPING_RIFRAM_PARAMETER_ALREADY_MAPPED);
                        
                        return false;
                    }
                }
                
                if (mappingObjectOne instanceof Operation || mappingObjectTwo instanceof Operation) {
                    Operation mappedOperation = getOperationFromOperationAccessMethodMapping(mapping);
                    if (mappedOperation.equals(operationOfParameter)) {
                        AccessMethod mappedAccessMethod = getAccessMethodFromOperationAccessMethodMapping(mapping);
                        
                        Resource resource = (Resource) mappedAccessMethod.eContainer();
                        pathFragmentOfOperationMapping = resource.getEndpoint();
                    }
                }
            }
            
            // if the associated path fragment exists
            if (pathFragmentOfOperationMapping != null) {
                // if the associated path fragment is a descendant or equal to the input dynamic fragment, the mapping 
                // is valid, otherwise display error message and return false
                if (isPathFragOneDescendantOrEqualToPathFragTwo(
                        pathFragmentOfOperationMapping, dynamicFragment)) {
                    return true;
                } else {
                    RamApp.getActiveScene().displayPopup(Strings.POPUP_MAPPING_RIFRAM_INCORRECT_PATH);
                    
                    return false;
                }
            } else {
                // if the associated path fragment does not exist, display error meessage and return false
                RamApp.getActiveScene().displayPopup(Strings.POPUP_MAPPING_RIFAM_OPERATION_NOT_MAPPED);
                
                return false;
            }

        } else if (firstModelElement instanceof AccessMethod && secondModelElement instanceof Operation 
                || firstModelElement instanceof Operation && secondModelElement instanceof AccessMethod) {
            
            // set parameter and dynamic fragment of input mapping
            AccessMethod accessMethod = null;
            Operation operation = null;
            
            if (firstModelElement instanceof AccessMethod) {
                accessMethod = (AccessMethod) firstModelElement;
                operation = (Operation) secondModelElement;
            } else {
                accessMethod = (AccessMethod) secondModelElement;
                operation = (Operation) firstModelElement;
            }
            
            for (COREModelElementMapping mapping : mappings) {
                // extract model elements of mapping
                EObject mappingObjectOne = mapping.getModelElements().get(0);
                EObject mappingObjectTwo = mapping.getModelElements().get(1);
                
                // enforce 1 mapping per access method / operation
                if (mappingObjectOne instanceof AccessMethod || mappingObjectTwo instanceof AccessMethod) {
                    AccessMethod mappedAccessMethod = getAccessMethodFromOperationAccessMethodMapping(mapping);
                    
                    if (mappedAccessMethod.equals(accessMethod)) {
                        RamApp.getActiveScene().displayPopup(Strings.POPUP_MAPPING_RIFRAM_ACCESSMETHOD_ALREADY_MAPPED);
                        
                        return false;
                    }

                    Operation mappedOperation = getOperationFromOperationAccessMethodMapping(mapping);
                    
                    if (mappedOperation.equals(operation)) {
                        RamApp.getActiveScene().displayPopup(Strings.POPUP_MAPPING_RIFRAM_OPERATION_ALREADY_MAPPED);
                        
                        return false;
                    }
                }
            }
            
            return true;
        }
        RamApp.getActiveScene().displayPopup(Strings.POPUP_MAPPING_RIFRAM_INVALID_TYPE);
        return false;
    }

    @Override
    public List<COREModelElementMapping> validateMappingsForEditing(List<COREModelElementMapping> mappings, 
            COREConcern concern) {
        List<COREModelElementMapping> mappingsToRemove = new ArrayList<>();
        
        // 1. We verify that the all mapping elements exist. We assume that mapping sizes are 2.
        
        // copy mappings list to avoid ConcurrentModificationException
        List<COREModelElementMapping> mappingsCopy = new ArrayList<>(mappings);
        
        // remove all mappings that have elements that do not exist in the model anymore
        for (COREModelElementMapping mapping : mappingsCopy) {
            EList<EObject> mappingElements = mapping.getModelElements();
            
            EObject firstElement = mappingElements.get(0);
            EObject secondElement = mappingElements.get(1);
            
            // get models from concern
            RestIF restIF = getRestifFromConcern(concern);
            StructuralView structuralView = getStructuralViewFromConcern(concern);
            
            boolean firstElementExists = false;
            boolean secondElementExists = false;
            
            // verify that the elements exist in their models
            if (isElementInRifModel(firstElement)) {
                firstElementExists = elementExistsInRifModel(firstElement, restIF);
                secondElementExists = elementExistsInRamModel(secondElement, structuralView);
            } else {
                firstElementExists = elementExistsInRifModel(secondElement, restIF);
                secondElementExists = elementExistsInRamModel(firstElement, structuralView);
            }
            
            // remove mappings that include elements that are no longer in their models
            if (!firstElementExists || !secondElementExists) {
                mappingsToRemove.add(mapping);
                mappings.remove(mapping);
            }
        }
                
        // 2. We verify that for each Parameter mapping, there exists an Operation mapping of the Operation containing
        // the Parameter.
        
        // If there exists an Operation mapping, we verify that the DynamicFragment of the Operation mapping is a 
        // descendant of the DynamicFragment of the Parameter mapping.
        
        // loop through every mapping, and separate them into parameter and operation mappings
        List<COREModelElementMapping> parameterMappings = new ArrayList<>();
        List<COREModelElementMapping> operationMappings = new ArrayList<>();
        
        for (COREModelElementMapping mapping : mappings) {
            if (mapping.getModelElements().get(0) instanceof Parameter || mapping.getModelElements().get(1) 
                    instanceof Parameter) {
                parameterMappings.add(mapping);
            } else {
                operationMappings.add(mapping);
            }
        }
        
        // copy parameterMappings list to avoid ConcurrentModificationException
        List<COREModelElementMapping> parameterMappingsCopy = new ArrayList<>(parameterMappings);
        
        for (COREModelElementMapping parameterMapping : parameterMappingsCopy) {
            Parameter parameter = getParameterFromParameterDynamicFragmentMapping(parameterMapping);
            DynamicFragment dynamicFragment = getDynamicFragmentFromParameterDynamicFragmentMapping(parameterMapping);
            Operation operationOfParameter = (Operation) parameter.eContainer();
            
            boolean operationMappingExists = false;
            PathFragment pathFragmentOfOperationMapping = null;
            
            for (COREModelElementMapping operationMapping : operationMappings) {
                Operation operation = getOperationFromOperationAccessMethodMapping(operationMapping);
                
                if (operation.equals(operationOfParameter)) {
                    operationMappingExists = true;
                    
                    AccessMethod mappedAccessMethod = getAccessMethodFromOperationAccessMethodMapping(operationMapping);
                    
                    Resource resource = (Resource) mappedAccessMethod.eContainer();
                    pathFragmentOfOperationMapping = resource.getEndpoint();
                }
            }
            
            // if operation mapping does not exist, the mapping is invalid
            if (!operationMappingExists) {
                mappingsToRemove.add(parameterMapping);
            } else {
                // if path fragment of operation mapping is not a descendant or equal to dynamic fragment of parameter
                // mapping, the mapping is invalid
                if (!isPathFragOneDescendantOrEqualToPathFragTwo(pathFragmentOfOperationMapping, dynamicFragment)) {
                    mappingsToRemove.add(parameterMapping);
                }
            }
        }

        return mappingsToRemove;
    }

    @Override
    public boolean validateMappingsForTransformation(List<COREModelElementMapping> mappings, COREConcern concern) {
        // verify that all of the current mappings satisfy the rules defines in validateMappingsForEditing
        List<COREModelElementMapping> invalidMappings = validateMappingsForEditing(mappings, concern);
        if (invalidMappings.size() > 0) {
            return false;
        }
        
        // loop through every mapping, and separate them into parameter and operation mappings
        List<COREModelElementMapping> parameterMappings = new ArrayList<>();
        List<COREModelElementMapping> operationMappings = new ArrayList<>();
        
        for (COREModelElementMapping mapping : mappings) {
            if (mapping.getModelElements().get(0) instanceof Parameter || mapping.getModelElements().get(1) 
                    instanceof Parameter) {
                parameterMappings.add(mapping);
            } else {
                operationMappings.add(mapping);
            }
        }
        
        // verify that the amount of unannotated parameters in an operation mapping is less than or equal to 1
        for (COREModelElementMapping operationMapping : operationMappings) {
            // count the number of annotated parameters
            int annotatedParametersCount = 0;
            
            Operation operation = getOperationFromOperationAccessMethodMapping(operationMapping);
            
            // figure out how many parameters are annotated
            for (Parameter parameterOfOperation : operation.getParameters()) {
                for (COREModelElementMapping parameterMapping : parameterMappings) {
                    Parameter mappedParameter = getParameterFromParameterDynamicFragmentMapping(parameterMapping);
                    
                    if (parameterOfOperation.equals(mappedParameter)) {
                        annotatedParametersCount++;
                    }
                }
            }
            
            // calculate the number of unannotated parameters
            int unannotatedParametersCount = operation.getParameters().size() - annotatedParametersCount;
            
            // if size of unannotated parameters is greater than 1, the mappings are invalid for transformation
            if (unannotatedParametersCount > 1) {                
                return false;
            }
        }
        
        return true;
    }
    
    /**
     * Helper method to check if an element exists in the Rif model.
     * 
     * @param element input model element
     * @param restIF input Rif model
     * @return whether the input model element exists in the Rif model or not
     */
    private static boolean elementExistsInRifModel(EObject element, RestIF restIF) {
        if (element instanceof PathFragment) {
            // check every path fragment by BFS, check if any are equal to the input model element
            ArrayList<PathFragment> currentPathFragments = new ArrayList<>();
            currentPathFragments.add(restIF.getRoot());
                        
            while (currentPathFragments.size() > 0) {
                ArrayList<PathFragment> children = new ArrayList<>();
                
                for (PathFragment pathFragment : currentPathFragments) {
                    if (pathFragment.equals(element)) {
                        return true;
                    }
                    
                    children.addAll(pathFragment.getChild());
                }
                currentPathFragments = children;
            }
        } else if (element instanceof AccessMethod) {
            // check every resource's accessmethod to see if any are equal to the input model element
            for (Resource resource : restIF.getResource()) {
                for (AccessMethod accessMethod : resource.getAccessmethod()) {
                    if (accessMethod.equals(element)) {
                        return true;
                    }
                }
            }
        }
        
        return false;
    }
    
    /**
     * Helper method to check if an element exists in the Ram model.
     * 
     * @param element input model element
     * @param structuralView input Ram model
     * @return whether the input model element exists in the Ram model or not
     */
    private static boolean elementExistsInRamModel(EObject element, StructuralView structuralView) {
        if (element instanceof Operation) {
            // check all operations of classes of structural view to see if any are equal to the input model element
            for (Classifier classifier : structuralView.getClasses()) {
                for (Operation operation : classifier.getOperations()) {
                    if (operation.equals(element)) {
                        return true;
                    }
                }
            }
        } else if (element instanceof Parameter) {
            // check all parameters of operations of classes of structural view to see if any are equal to the input 
            // model element
            for (Classifier classifier : structuralView.getClasses()) {
                for (Operation operation : classifier.getOperations()) {
                    for (Parameter parameter : operation.getParameters()) {
                        if (parameter.equals(element)) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }
    
    /**
     * Helper method to check if the input element is in the Rif model.
     * 
     * @param element input element
     * @return whether the input element is of type {@link DynamicFragment} or {@link AccessMethod}
     */
    private static boolean isElementInRifModel(EObject element) {
        if (element instanceof DynamicFragment || element instanceof AccessMethod) {
            return true;
        }
        return false;
    }
    
    /**
     * Helper method to check if an input type name of a Parameter is of primitive type for mappings or not.
     * 
     * @param typeName type name of mapped Parameter
     * @return whether the type name is of primitive type for mappings or not
     */
    private static boolean isPrimitiveType(String typeName) {
        for (String validType : VALID_PRIMITIVE_TYPES_FOR_MAPPINGS) {
            if (typeName.equals(validType)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Helper method to check if the input path fragment one is a descendant or equal to input path fragment two.
     * 
     * @param pathFragmentOne input path fragment one
     * @param pathFragmentTwo input path fragment two
     * @return whether or not the input path fragment one is a descendant or equal to input path fragment two
     */
    private static boolean isPathFragOneDescendantOrEqualToPathFragTwo(PathFragment pathFragmentOne, PathFragment 
            pathFragmentTwo) {
        EList<PathFragment> currentNodes = new BasicEList<PathFragment>();
        currentNodes.add(pathFragmentTwo);
        
        while (currentNodes.size() > 0) {
            EList<PathFragment> children = new BasicEList<PathFragment>();
            
            for (PathFragment node : currentNodes) {
                if (node.equals(pathFragmentOne)) {
                    return true;
                }
                
                children.addAll(node.getChild());
            }
            
            currentNodes = children;
        }
        
        return false;
    }
    
    /**
     * Helper method to get a Parameter from a Parameter-DynamicFragment mapping.
     * 
     * @param mapping Parameter-DynamicFragment mapping
     * @return the Parameter object
     */
    private static Parameter getParameterFromParameterDynamicFragmentMapping(COREModelElementMapping mapping) {
        // extract model elements of mapping
        EObject mappingObjectOne = mapping.getModelElements().get(0);
        EObject mappingObjectTwo = mapping.getModelElements().get(1);
        
        if (mappingObjectOne instanceof Parameter) {
            return (Parameter) mappingObjectOne;
        } else {
            return (Parameter) mappingObjectTwo;
        }
    }
    
    /**
     * Helper method to get a DynamicFragment from a DynamicFragment-DynamicFragment mapping.
     * 
     * @param mapping Parameter-DynamicFragment mapping
     * @return the Parameter object
     */
    private static DynamicFragment getDynamicFragmentFromParameterDynamicFragmentMapping(COREModelElementMapping 
            mapping) {
        // extract model elements of mapping
        EObject mappingObjectOne = mapping.getModelElements().get(0);
        EObject mappingObjectTwo = mapping.getModelElements().get(1);
        
        if (mappingObjectOne instanceof DynamicFragment) {
            return (DynamicFragment) mappingObjectOne;
        } else {
            return (DynamicFragment) mappingObjectTwo;
        }
    }
    
    /**
     * Helper method to get a Operation from a Operation-AccessMethod mapping.
     * 
     * @param mapping Operation-AccessMethod mapping
     * @return the Operation object
     */
    private static Operation getOperationFromOperationAccessMethodMapping(COREModelElementMapping mapping) {
        // extract model elements of mapping
        EObject mappingObjectOne = mapping.getModelElements().get(0);
        EObject mappingObjectTwo = mapping.getModelElements().get(1);
        
        if (mappingObjectOne instanceof Operation) {
            return (Operation) mappingObjectOne;
        } else {
            return (Operation) mappingObjectTwo;
        }
    }
    
    /**
     * Helper method to get an AccessMethod from a Operation-AccessMethod mapping.
     * 
     * @param mapping Operation-AccessMethod mapping
     * @return the AccessMethod object
     */
    private static AccessMethod getAccessMethodFromOperationAccessMethodMapping(COREModelElementMapping mapping) {
        // extract model elements of mapping
        EObject mappingObjectOne = mapping.getModelElements().get(0);
        EObject mappingObjectTwo = mapping.getModelElements().get(1);
        
        if (mappingObjectOne instanceof AccessMethod) {
            return (AccessMethod) mappingObjectOne;
        } else {
            return (AccessMethod) mappingObjectTwo;
        }
    }
    
    /**
     * Helper method to get the {@link RestIF} object from the concern.
     * 
     * @param concern the concern that contains the inner models as COREExternalArtefacts
     * @return the {@link RestIF} object if it exists
     */
    private static RestIF getRestifFromConcern(COREConcern concern) {
        COREExternalArtefact rifArtefact = TrafoUtils.extractCoreArtefactByKey(concern, "Exposed Interface");

        if (rifArtefact != null) {
            return (RestIF) rifArtefact.getRootModelElement();
        }
        
        return null;
    }
    
    /**
     * Helper method to get the {@link StructuralView} object from the concern.
     * 
     * @param concern the concern that contains the inner models as COREExternalArtefacts
     * @return the {@link StructuralView} object if it exists
     */
    private static StructuralView getStructuralViewFromConcern(COREConcern concern) {
        COREExternalArtefact ramArtefact = TrafoUtils.extractCoreArtefactByKey(concern, "Business Logic");
        
        if (ramArtefact != null) {
            Aspect aspect = (Aspect) ramArtefact.getRootModelElement();
            return aspect.getStructuralView();
        }
        
        return null;
    }
}
