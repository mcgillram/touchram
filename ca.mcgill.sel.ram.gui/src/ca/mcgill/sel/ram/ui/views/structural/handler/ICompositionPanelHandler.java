package ca.mcgill.sel.ram.ui.views.structural.handler;

import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.ram.ui.components.CompositionsPanel;
import ca.mcgill.sel.ram.ui.events.listeners.IDragListener;
import ca.mcgill.sel.ram.ui.events.listeners.ITapListener;

/**
 * This interface can be implemented by a handler that can handle events for the container of
 * {@link ca.mcgill.sel.ram.ui.views.CompositionContainerView} which is showing the list of model
 * compositions in a model.
 * 
 * @author eyildirim
 */
public interface ICompositionPanelHandler extends ITapListener, IDragListener {

    /**
     * Handles switching back from the split mode.
     * This function is used to rolling back the changes of composition container view
     * (such as adding a mode switch button, showing only the title of the composition
     * which is being edited) due to switching the mode of the scene to split composition editing mode.
     * 
     * @param compositionsContainerView
     *            the view for which we are doing all changes
     * @param modelComposition
     *            the {@link COREModelComposition} which is not going to be in the edit mode anymore.
     */
    void switchFromCompositionSplitEditMode(CompositionsPanel compositionsContainerView,
            COREModelComposition modelComposition);

    /**
     * Handles switching to split mode.
     * This function is used to make changes to composition container view
     * (such as adding a mode switch button, showing only the title of the composition which is
     * being edited) while switching the mode of the scene to split composition editing mode.
     * 
     * @param compositionsContainerView
     *            the view for which we are doing all changes
     * @param modelComposition
     *            the {@link COREModelComposition}which is going to be put into edit mode.
     */
    void switchToCompositionSplitEditMode(CompositionsPanel compositionsContainerView,
            COREModelComposition modelComposition);
}
