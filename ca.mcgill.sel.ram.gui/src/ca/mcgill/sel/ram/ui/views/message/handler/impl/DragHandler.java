package ca.mcgill.sel.ram.ui.views.message.handler.impl;

import org.eclipse.emf.ecore.EClass;
import org.mt4j.components.TransformSpace;
import org.mt4j.input.inputProcessors.MTGestureEvent;
import org.mt4j.input.inputProcessors.componentProcessors.dragProcessor.DragEvent;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.ram.CombinedFragment;
import ca.mcgill.sel.ram.FragmentContainer;
import ca.mcgill.sel.ram.InteractionFragment;
import ca.mcgill.sel.ram.Lifeline;
import ca.mcgill.sel.ram.Message;
import ca.mcgill.sel.ram.MessageEnd;
import ca.mcgill.sel.ram.MessageOccurrenceSpecification;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.controller.ControllerFactory;
import ca.mcgill.sel.ram.controller.FragmentsController;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.events.DelayedDrag;
import ca.mcgill.sel.ram.ui.events.listeners.IDragListener;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.BaseHandler;
import ca.mcgill.sel.ram.ui.views.message.AssignmentStatementView;
import ca.mcgill.sel.ram.ui.views.message.CombinedFragmentView;
import ca.mcgill.sel.ram.ui.views.message.LifelineView;
import ca.mcgill.sel.ram.ui.views.message.MessageCallView;
import ca.mcgill.sel.ram.ui.views.message.MessageViewView;
import ca.mcgill.sel.ram.util.MessageViewUtil;

/**
 * The default handler for {@link ca.mcgill.sel.ram.Message}s.
 * 
 * @author Tony Chen
 * @author mschoettle
 */
public class DragHandler extends BaseHandler implements IDragListener {
       
    private DelayedDrag dragAction = new DelayedDrag(5f);
    
    private Vector3D startLocation;
    
    private RamRectangleComponent currentSpacer;
    
    @Override
    public boolean processDragEvent(DragEvent dragEvent) {
        RamRectangleComponent targetView = (RamRectangleComponent) dragEvent.getTarget();
        InteractionFragment fragment = null;
        EClass dragObject = RamPackage.Literals.INTERACTION_FRAGMENT;
        
        // Get fragment and targetView based on the view being dragged
        // both CombinedFragment and executionStatement have TextView as dragEvent target. 
        if (targetView instanceof TextView) {
            TextView textView = (TextView) targetView;
            fragment = (InteractionFragment) textView.getData();
            
            // Change the target of the drag event to the whole CombinedFragmentView.
            if (fragment instanceof CombinedFragment) {
                dragObject = RamPackage.Literals.COMBINED_FRAGMENT;
                targetView = (CombinedFragmentView) textView.getParent().getParent().getParent();
                dragEvent.setTarget(targetView);
            }
        } else if (targetView instanceof AssignmentStatementView) {
            fragment = (InteractionFragment) ((AssignmentStatementView) targetView).getAssignmentStatement();
        } else if (targetView instanceof MessageCallView) {
            dragObject = RamPackage.Literals.MESSAGE;
            
            Message message = ((MessageCallView) targetView).getMessage();
            
            // Disable for messages with gates.
            if (!message.getInteraction().getFormalGates().contains(message.getSendEvent())
                    && !message.getInteraction().getFormalGates().contains(message.getReceiveEvent())) {
                fragment = (InteractionFragment) message.getSendEvent();
            }
        }
        
        if (fragment == null) {
            return false;
        }
        
        MessageViewView messageViewView = (MessageViewView) targetView.getParent().getParent();
        Lifeline lifeline = fragment.getCovered().get(0);
        LifelineView lifelineView = messageViewView.getLifelineView(lifeline);
        Vector3D targetLocation = targetView.getPosition(TransformSpace.GLOBAL);
        
        switch (dragEvent.getId()) {
            case MTGestureEvent.GESTURE_STARTED:
                startLocation = targetLocation;
                dragAction.processGestureEvent(dragEvent);

                break;
            case MTGestureEvent.GESTURE_UPDATED:
                dragAction.processGestureEvent(dragEvent);
                
                if (dragAction.wasDragPerformed()) {
                    float lifelineX = lifelineView.getCenterPointGlobal().getX();
                    Vector3D currentLocation;
                    
                    /**
                     * Get location of view to highlight spacer. 
                     * Highlighting the spacer shows the user which spacer the view is being dragged to.
                     */
                    if (dragObject == RamPackage.Literals.MESSAGE) {
                        Vector3D messageLocation = ((MessageCallView) targetView).getFromEnd().getLocation();
                        currentLocation = new Vector3D(lifelineX, messageLocation.getY() + targetLocation.getY());
                    } else {
                        // Remove background to be able to see the highlighted spacer.
                        targetView.setNoFill(true);
                        currentLocation = new Vector3D(lifelineX, targetView.getY());
                    }
                    
                    // Unhighlight previous spacer.
                    if (currentSpacer != null) {
                        currentSpacer.highlight(Colors.MESSAGE_SPACER_FILL_COLOR, null);
                        currentSpacer = null;
                    }
                    
                    RamRectangleComponent spacer = lifelineView.getSpacerAt(currentLocation);
                    
                    // If we are currently positioning over a spacer, verify that it is a valid place to drag to.
                    if (spacer != null && isValidDestination(fragment, 
                                            lifelineView, currentLocation)) {
                        currentSpacer = spacer;
                        currentSpacer.highlight(Colors.MESSAGE_SPACER_HIGHLIGHT_COLOR, null);
                    }
                }
                
                break;
            case MTGestureEvent.GESTURE_ENDED:
                // The spacer is only set if the position is valid (within bounds).
                if (dragAction.wasDragPerformed() && currentSpacer != null) {
                    Vector3D endLocation = currentSpacer.getCenterPointGlobal();
                    
                    int newIndex = getDestinationIndex(fragment, lifelineView, endLocation);
                    
                    if (newIndex >= 0) {
                        FragmentContainer destinationContainer = lifelineView.getFragmentContainerAt(endLocation);
                        FragmentsController fragmentsController = ControllerFactory.INSTANCE.getFragmentsController();
                        fragmentsController.moveFragment(fragment, destinationContainer, newIndex);
                    }
                    
                    // Remove highlighting of selected spacer.
                    currentSpacer.highlight(Colors.MESSAGE_SPACER_FILL_COLOR, null);
                    currentSpacer = null;
                    
                }
                
                // Enable background color again.
                if (dragObject == RamPackage.Literals.INTERACTION_FRAGMENT) {
                    targetView.setNoFill(false);
                }
                
                // Set the view back to it's previous position.
                targetView.setPositionGlobal(startLocation);
                break;
        }
        
        return true;
    }

    /**
     * Returns whether the given position is within valid range for moving fragments to.
     * The valid range is between the first and last fragment of a message view, or within an operand.
     * 
     * @param fragment the current fragment that should be dragged
     * @param lifelineView the lifeline view the fragment is located on
     * @param position the position to be considered
     * @return true, if the position is valid, false otherwise
     */
    private static boolean isValidDestination(InteractionFragment fragment,
            LifelineView lifelineView, Vector3D position) {
        return getDestinationIndex(fragment, lifelineView, position) >= 0;
    }
    
    /**
     * Returns the destination index for the given fragment and position.
     * Returns <code>-1</code> if the position is invalid, i.e., the fragment cannot moved to this place.
     * For example, this is the case when it is outside of the initial and return message of the message view.
     * 
     * @param fragment the {@link InteractionFragment} that is moved
     * @param lifelineView the {@link LifelineView} on which the fragment is located on
     * @param position the visual position where the fragment should be moved to
     * @return the new index for the fragment and the given position, <code>-1</code> if it is an invalid position
     */
    private static int getDestinationIndex(InteractionFragment fragment,
            LifelineView lifelineView, Vector3D position) {
        FragmentContainer container = fragment.getContainer();
        FragmentContainer destinationContainer = lifelineView.getFragmentContainerAt(position);
        InteractionFragment destinationFragment = lifelineView.getFragmentBefore(position);
        
        // Ensure that the two containers are within the same message view.
        // I.e., disallow dragging across message views for expanded messages.
        Object sourceInteraction =
                EMFModelUtil.getRootContainerOfType(container, RamPackage.Literals.ABSTRACT_MESSAGE_VIEW);
        Object targetInteraction =
                EMFModelUtil.getRootContainerOfType(destinationContainer, RamPackage.Literals.ABSTRACT_MESSAGE_VIEW);
        
        if (sourceInteraction != targetInteraction) {
            return -1;
        }
        
        boolean acrossContainers = container != destinationContainer;
        
        int oldIndex = container.getFragments().indexOf(fragment);
        int newIndex = 0;
        // Fragment is null if we're at the top of an operand, we use 0 as the newIndex.
        if (destinationFragment != null) {
            newIndex = MessageViewUtil.getIndexAfter(destinationContainer, destinationFragment);
            /**
             * If moving the fragment downwards, we decrease the newIndex because 
             * the spacer after the fragment gives the index of the following fragment.
             * However, visually the spacer before and after the dragged fragment is invalid.
             */
            if (!acrossContainers) {
                if (oldIndex < newIndex) {
                    if (fragment instanceof MessageOccurrenceSpecification) {
                        newIndex -= 2;
                    } else {
                        newIndex--;
                    }
                }
            }
        }
        
        // Disallow the spot within a self message.
        if (destinationFragment instanceof MessageEnd) {
            MessageEnd end = (MessageEnd) destinationFragment;
            if (end.getMessage().getSendEvent() == end && end.getMessage().isSelfMessage()) {
                return -1;
            }
        }
        
        if (newIndex >= 0 
                && (acrossContainers || newIndex != oldIndex)
                && newIndex >= MessageViewUtil.findLowerBound(destinationContainer) 
                && newIndex < MessageViewUtil.findUpperBound(destinationContainer)) {
            return newIndex;
        }
        
        return -1;
    }
}
