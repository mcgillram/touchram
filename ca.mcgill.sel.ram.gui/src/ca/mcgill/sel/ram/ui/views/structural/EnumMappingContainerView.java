package ca.mcgill.sel.ram.ui.views.structural;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.mt4j.components.MTComponent;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.COREMapping;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.ram.EnumLiteralMapping;
import ca.mcgill.sel.ram.EnumMapping;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamRoundedRectangleComponent;
import ca.mcgill.sel.ram.ui.components.listeners.UINotifyChangedListener;
import ca.mcgill.sel.ram.ui.layouts.VerticalLayout;
import ca.mcgill.sel.ram.ui.utils.Colors;

/**
 * This view contains a EnumrMappingView and all mapping related container views for a enum mapping.
 * 
 * @author joerg
 */
public class EnumMappingContainerView extends RamRoundedRectangleComponent implements UINotifyChangedListener {
    // It is useful to keep the child as a reference for hiding/showing purposes
    private EnumMappingView myEnumMappingView;

    // Enum Mapping information
    private EnumMapping myEnumMapping;

    // All the enum literal mapping related views will be in this view:
    private RamRectangleComponent hideableEnumLiteralContainer;

    /**
     * Creates a new mapping container view for enum mapopings.
     * 
     * @param enumMapping - {@link EnumMapping} which we want to create a {@link EnumMappingContainerView} for.
     */
    public EnumMappingContainerView(EnumMapping enumMapping) {
        super(0);
        setNoStroke(false);
        setStrokeWeight(3);
        setNoFill(false);
        setFillColor(Colors.MAPPING_CONTAINER_VIEW_FILL_COLOR);

        myEnumMappingView = new EnumMappingView(this, enumMapping);
        this.addChild(myEnumMappingView);

        myEnumMapping = enumMapping;

        hideableEnumLiteralContainer = new RamRectangleComponent();
        hideableEnumLiteralContainer.setLayout(new VerticalLayout(0));
        hideableEnumLiteralContainer.setFillColor(Colors.ATTRIBUTE_MAPPING_VIEW_FILL_COLOR);
        hideableEnumLiteralContainer.setNoFill(false);
        hideableEnumLiteralContainer.setBufferSize(Cardinal.EAST, 0);

        this.addChild(hideableEnumLiteralContainer);

        setLayout(new VerticalLayout());

        // setBufferSize(Cardinal.EAST, 0);
        setBuffers(0);
        // add all the operation and attribute mappings related to this classifier mapping.
        addAllEnumLiteralMappings();

        EMFEditUtil.addListenerFor(myEnumMapping, this);
    }

    /**
     * Adds all the enum literal mappings which belongs to the enum mapping.
     */
    private void addAllEnumLiteralMappings() {
        EList<COREMapping<?>> coreMappings = myEnumMapping.getMappings();
        for (COREMapping<?> mapping : coreMappings) {
            if (mapping instanceof EnumLiteralMapping) {
                addEnumLiteralMappingView((EnumLiteralMapping) mapping);
            }
        }
    }

    /**
     * Adds an {@link EnumLiteralMappingView} to the mapping container view (inside of hideableEnumLiteralContainer).
     * 
     * @param newEnumLiteralMapping the {@link EnumLiteralMapping} to add a view for
     */
    private void addEnumLiteralMappingView(EnumLiteralMapping newEnumLiteralMapping) {
        EnumLiteralMappingView enumLiteralMappingView = new EnumLiteralMappingView(this, newEnumLiteralMapping);
        hideableEnumLiteralContainer.addChild(enumLiteralMappingView);
    }

    /**
     * Deletes an {@link EnumLiteralMappingView} from the 
     * mapping container view (inside the hideableEnumLiteralContainer).
     * 
     * @param deletedEnumLiteralMapping the {@link EnumLiteralMapping} to remove the view for
     */
    private void deleteEnumLiteralMappingView(EnumLiteralMapping deletedEnumLiteralMapping) {
        MTComponent[] enumLiteralMappingViews = hideableEnumLiteralContainer.getChildren();
        for (MTComponent view : enumLiteralMappingViews) {
            if (view instanceof EnumLiteralMappingView) {
                EnumLiteralMappingView enumLiteralMappingView = (EnumLiteralMappingView) view;
                if (enumLiteralMappingView.getEnumLiteralMapping() == deletedEnumLiteralMapping) {
                    hideableEnumLiteralContainer.removeChild(enumLiteralMappingView);
                }
            }
        }
    }

    @Override
    public void destroy() {
        EMFEditUtil.removeListenerFor(myEnumMapping, this);
        super.destroy();
    }

    /**
     * Getter for EnumMapping information of the view.
     * 
     * @return {@link EnumMapping}
     */
    public EnumMapping getClassifierMapping() {
        return myEnumMapping;
    }

    @Override
    public void handleNotification(Notification notification) {
        if (notification.getNotifier() == myEnumMapping) {
            if (notification.getFeature() == CorePackage.Literals.CORE_MAPPING__MAPPINGS) {
                switch (notification.getEventType()) {
                    case Notification.ADD:

                        EnumLiteralMapping newEnumLiteralMapping = (EnumLiteralMapping) notification.getNewValue();
                        addEnumLiteralMappingView(newEnumLiteralMapping);
                        break;

                    case Notification.REMOVE:
                        EnumLiteralMapping oldEnumLiteralMapping = (EnumLiteralMapping) notification.getOldValue();
                        deleteEnumLiteralMappingView(oldEnumLiteralMapping);
                        break;
                }
            }
        }

    }
}
