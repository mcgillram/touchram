package ca.mcgill.sel.ram.ui.views;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.mt4j.util.MTColor;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.classdiagram.Classifier;
import ca.mcgill.sel.classdiagram.ui.scenes.DisplayClassDiagramScene;
import ca.mcgill.sel.classdiagram.ui.views.ClassDiagramView;
import ca.mcgill.sel.classdiagram.ui.views.ClassView;
import ca.mcgill.sel.core.COREModelElementMapping;
import ca.mcgill.sel.ram.ui.components.RamLineComponent;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.scenes.RamAbstractScene;
import ca.mcgill.sel.ram.ui.scenes.handler.IRamAbstractSceneHandler;
import ca.mcgill.sel.ram.ui.views.handler.IAbstractViewHandler;
import ca.mcgill.sel.usecases.Actor;
import ca.mcgill.sel.usecases.ui.scenes.DisplayUseCaseModelScene;
import ca.mcgill.sel.usecases.ui.views.ActorView;
import ca.mcgill.sel.usecases.ui.views.UseCaseDiagramView;

/**
 * This is an implementation of {@link GenericSplitViewWithMappings} that displays a Domain and Use Case model in the 
 * split view.
 * 
 * @author Bowen
 * 
 * @param <S> type that extends {@link IRamAbstractSceneHandler} for the inner level scene
 * @param <T> type that extends {@link IAbstractViewHandler} for the outer level view
 * @param <U> type that extends {@link IAbstractViewHandler} for the inner level view
 */
public class DomainUsecaseSplitViewWithMappings<S extends IRamAbstractSceneHandler, 
    T extends IAbstractViewHandler, U extends IAbstractViewHandler> extends GenericSplitViewWithMappings<S, T, U> {

    public DomainUsecaseSplitViewWithMappings(RamAbstractScene<S> displaySceneInnerLevel,
            AbstractView<T> outerLevelView, boolean isHorizontal, EList<COREModelElementMapping> mappings) {
        super(displaySceneInnerLevel, outerLevelView, isHorizontal, mappings);
    }

    /**
     * Get the associated view of the model element.
     * 
     * @param element the {@link EObject} model element
     * 
     * @return the associated {@link RamRectangleComponent}
     */
    @Override
    public RamRectangleComponent getViewOf(EObject element) {
        RamRectangleComponent mappedView = null;

        if (element instanceof Actor) {
            if (outerLevelView instanceof UseCaseDiagramView) {
                mappedView = ((UseCaseDiagramView) outerLevelView).getActorView((Actor) element);
            } else {
                mappedView = ((DisplayUseCaseModelScene) displaySceneInnerLevel)
                        .getUseCaseDiagramView().getActorView((Actor) element);
            }
        } else if (element instanceof Classifier) {
            if (outerLevelView instanceof ClassDiagramView) {
                mappedView = ((ClassDiagramView) outerLevelView).getClassViewOf((Classifier) element);
            } else {
                mappedView = ((DisplayClassDiagramScene) displaySceneInnerLevel)
                        .getClassDiagramView().getClassViewOf((Classifier) element);
            }
        }
        
        return mappedView;
    }

    /**
     * Get a modified x and y coordinate in the form of a Vector3D from the given view.
     * 
     * @param view the {@link RamRectangleComponent}
     * 
     * @return the modified {@link Vector3D}
     */
    @Override
    public Vector3D getModifiedXYFromView(RamRectangleComponent view) {
        Vector3D xyVector = new Vector3D();

        if (view instanceof ActorView) {
            xyVector.setX(view.getCenterPointGlobal().getX());
            xyVector.setY(view.getCenterPointGlobal().getY());
        } else if (view instanceof ClassView) {
            xyVector.setX(view.getX() + view.getGlobalWidth() / 2);
            xyVector.setY(view.getY());
        } else {
            xyVector.setX(view.getX());
            xyVector.setY(view.getY());
        }
        
        return xyVector;        
    }

    /**
     * Set the color of the given mapping line depending on the types of the first and second mapped elements.
     * 
     * @param mappingLine the associated {@link RamLineComponent}
     * @param firstMappedElement the first {@link RamRectangleComponent} associated with the line
     * @param secondMappedElement the second {@link RamRectangleComponent} associated with the line
     */
    @Override
    public void setLineColorBasedOnMapping(RamLineComponent mappingLine, RamRectangleComponent firstMappedElement,
            RamRectangleComponent secondMappedElement) {
        mappingLine.setStrokeColor(MTColor.GREEN);        
    }

    /**
     * Check if a given mapped element is within the inner view.
     * 
     * @param mappedElement the given {@link RamRectangleComponent}
     * 
     * @return whether or not the element is within the inner view
     */
    @Override
    protected boolean checkElementWithinInnerView(RamRectangleComponent mappedElement) {
        if (displaySceneInnerLevel instanceof DisplayUseCaseModelScene) {
            if (mappedElement instanceof ActorView) {
                return true;
            }
        } else {
            if (mappedElement instanceof ClassView) {
                return true;
            }
        }
        
        return false;
    }

    /**
     * Create a stippled line given the position of the view and the actual view.

     * @param mappedElementXY the given {@link Vector3D} coordinates
     * @param mappedElement the given {@link RamRectangleComponent} view
     * 
     * @return the created stippled {@link RamLineComponent}
     */
    @Override
    protected RamLineComponent createStippledLine(Vector3D mappedElementXY, RamRectangleComponent mappedElement) {
        RamLineComponent mappingLine;
        
        mappingLine = new RamLineComponent(mappedElementXY.getX(),
                mappedElementXY.getY(), mappedElementXY.getX() 
                - mappedElement.getGlobalWidth() / 2, mappedElementXY.getY());
        
        mappingLine.setLineStipple((short) 1000);

        return mappingLine;
    }

    /**
     * Get the view from a scene. 
     * 
     * @param scene the {@link RamAbstractScene} to get the view from
     * 
     * @return the {@link AbstractView}
     */
    @Override
    public AbstractView<?> getViewFromScene(RamAbstractScene<?> scene) {
        if (scene instanceof DisplayUseCaseModelScene) {
            return ((DisplayUseCaseModelScene) scene).getUseCaseDiagramView();
        } else {
            return ((DisplayClassDiagramScene) scene).getClassDiagramView();
        }
    }
}
