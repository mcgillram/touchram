package ca.mcgill.sel.ram.ui.views.feature;

import org.mt4j.input.gestureAction.TapAndHoldVisualizer;
import org.mt4j.input.inputProcessors.componentProcessors.panProcessor.PanProcessorTwoFingers;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldProcessor;
import org.mt4j.input.inputProcessors.componentProcessors.zoomProcessor.ZoomProcessor;

import ca.mcgill.sel.core.COREFeatureModel;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.events.MouseWheelProcessor;
import ca.mcgill.sel.ram.ui.events.RightClickDragProcessor;
import ca.mcgill.sel.ram.ui.scenes.DisplayConcernSelectScene.DisplayMode;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.views.handler.IAbstractViewHandler;

/**
 * View used to represent a feature model in select mode, when we choose feature for reusing.
 * 
 * @author Nishanth
 */
public class FeatureDiagramSelectView extends FeatureDiagramView<IAbstractViewHandler> {

    private DisplayMode currentMode;

    /**
     * Create a feature diagram representation view.
     * 
     * @param width - The width of the view
     * @param height - The height of the view
     * @param featureModel - The represented FeatureModel
     */
    public FeatureDiagramSelectView(float width, float height, COREFeatureModel featureModel) {
        super(width, height, featureModel);
    }

    @Override
    protected void registerListeners(FeatureView feature) {
        feature.setListenersSelectMode();
        if (!feature.getIsRoot()) {
            feature.addGestureListener(TapAndHoldProcessor.class, new TapAndHoldVisualizer(RamApp.getApplication(),
                    containerLayer));
        }
    }

    @Override
    protected void registerInputProcessors() {
        registerInputProcessor(new PanProcessorTwoFingers(RamApp.getApplication()));
        registerInputProcessor(new RightClickDragProcessor(RamApp.getApplication()));
        registerInputProcessor(new ZoomProcessor(RamApp.getApplication()));
        registerInputProcessor(new MouseWheelProcessor(RamApp.getApplication()));
    }

    /**
     * Set the current display mode for the diagram.
     * 
     * @param currentMode - The new mode
     */
    public void setCurrentMode(DisplayMode currentMode) {
        this.currentMode = currentMode;
    }

    /*
     * -------------------------- DISPLAY ---------------------------
     */
    /**
     * Update color for each {@link FeatureView} based on its selection status.
     */
    public void updateFeatureColors() {
        for (FeatureView feature : getFeatureViews()) {
            feature.updateSelectionColor();
        }
    }

    @Override
    public void updateFeaturesDisplay(boolean repopulate) {
        super.updateFeaturesDisplay(repopulate);
        // Put correct color to features
        updateFeatureColors();
    }

    @Override
    protected DisplayOptions shouldDisplay(FeatureView child) {
        // In next mode, don't display if parent is not selected.
        if (currentMode == DisplayMode.NEXT) {
            FeatureView parent = child.getParentFeatureView();
            if (parent != null && !parent.getSelectionFeature().isSelected()) {
                return DisplayOptions.HIDE;
            }
        }
        return super.shouldDisplay(child);
    }

    /**
     * Check if there are any clashes with the selection of features.
     * 
     * @return true if there are no clashes, false otherwise
     */
    public boolean checkForClashes() {
        for (FeatureView feature : getFeatureViews()) {
            if (feature.getLineToParent() != null
                    && feature.getLineToParent().getStrokeColor().equals(Colors.FEATURE_SELECTION_CLASH_COLOR)) {
                return false;
            }
        }
        return true;
    }

}
