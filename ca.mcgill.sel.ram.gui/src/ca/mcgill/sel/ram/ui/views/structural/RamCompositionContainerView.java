package ca.mcgill.sel.ram.ui.views.structural;

import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.ram.ui.views.CompositionContainerView;
import ca.mcgill.sel.ram.ui.views.CompositionView;
import ca.mcgill.sel.ram.ui.views.handler.HandlerFactory;

/**
 * This subclass of CompositionContainerView is used for creating RamCompositionViews inside the CompositionPanel
 * when displaying an aspect.
 *
 * @author joerg
 */
public class RamCompositionContainerView extends CompositionContainerView {

    /**
     * A constant identifying the "add classifier mapping" action.
     */
    public static final String ACTION_CLASSIFIER_MAPPING_ADD = "mapping.classifier.add";

    /**
     * A constant identifying the "add enum mapping" action.
     */    
    public static final String ACTION_ENUM_MAPPING_ADD = "mapping.enum.add";    
    
    public RamCompositionContainerView(COREArtefact currentArtefact, boolean isModelReuseContainer, String title) {
        super(currentArtefact, isModelReuseContainer, title);
    }

    /**
     * This method must return the appropriate CompositionView depending on the language.
     * @param composition the composition
     * @param reuse whether or not this is a reuse
     * @return the CompositionView corresponding to the language
     */
    @Override
    public CompositionView createCompositionView(COREModelComposition composition, COREReuse reuse) {
        return new RamCompositionView(composition, reuse, this);
    }
    
    @Override
    public void customizableActionPerformed(ActionEvent event, EObject mappingContainer) {
        String actionCommand = event.getActionCommand();
    
        if (ACTION_CLASSIFIER_MAPPING_ADD.equals(actionCommand)) {
            HandlerFactory.INSTANCE.getRamCompositionViewHandler()
                .addClassifierMapping((COREModelComposition) mappingContainer);
        } else if (ACTION_ENUM_MAPPING_ADD.equals(actionCommand)) {
            HandlerFactory.INSTANCE.getRamCompositionViewHandler()
                .addEnumMapping((COREModelComposition) mappingContainer);
        }        
    }
}
