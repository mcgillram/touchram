package ca.mcgill.sel.ram.ui.views;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.mt4j.util.MTColor;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.core.COREModelElementMapping;
import ca.mcgill.sel.ram.Operation;
import ca.mcgill.sel.ram.Parameter;
import ca.mcgill.sel.ram.ui.components.RamLineComponent;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.scenes.DisplayAspectScene;
import ca.mcgill.sel.ram.ui.scenes.RamAbstractScene;
import ca.mcgill.sel.ram.ui.scenes.handler.IRamAbstractSceneHandler;
import ca.mcgill.sel.ram.ui.views.handler.IAbstractViewHandler;
import ca.mcgill.sel.ram.ui.views.structural.OperationView;
import ca.mcgill.sel.ram.ui.views.structural.ParameterView;
import ca.mcgill.sel.ram.ui.views.structural.StructuralDiagramView;
import ca.mcgill.sel.restif.AccessMethod;
import ca.mcgill.sel.restif.DynamicFragment;
import ca.mcgill.sel.restif.PathFragment;
import ca.mcgill.sel.restif.ui.components.ResourceRamButton;
import ca.mcgill.sel.restif.ui.scenes.DisplayRestTreeScene;
import ca.mcgill.sel.restif.ui.views.PathFragmentView;
import ca.mcgill.sel.restif.ui.views.RestTreeView;

/**
 * This is an implementation of {@link GenericSplitViewWithMappings} that displays a Ram and Rif model in the 
 * split view.
 * 
 * @author Bowen
 * 
 * @param <S> type that extends {@link IRamAbstractSceneHandler} for the inner level scene
 * @param <T> type that extends {@link IAbstractViewHandler} for the outer level view
 * @param <U> type that extends {@link IAbstractViewHandler} for the inner level view
 */
public class RifRamSplitViewWithMappings<S extends IRamAbstractSceneHandler,
    T extends IAbstractViewHandler, U extends IAbstractViewHandler> extends GenericSplitViewWithMappings<S, T, U> {

    public RifRamSplitViewWithMappings(RamAbstractScene<S> displaySceneInnerLevel,
            AbstractView<T> outerLevelView, boolean isHorizontal, EList<COREModelElementMapping> mappings) {
        super(displaySceneInnerLevel, outerLevelView, isHorizontal, mappings);
    }
    
    /**
     * Get the associated view of the model element. Only allowed model elements for mappings are {@link Operation}, 
     * {@link Parameter}, {@link AcessMethod}, and {@link DynamicFragment}.
     * 
     * @param element the {@link EObject} model element
     * 
     * @return the associated {@link RamRectangleComponent}
     */
    @Override
    public RamRectangleComponent getViewOf(EObject element) {
        RamRectangleComponent mappedView = null;
        
        if (element instanceof AccessMethod) {
            if (outerLevelView instanceof RestTreeView) {
                mappedView = ((RestTreeView) outerLevelView).getResourceRamButtonOf((AccessMethod) element);
            } else {
                mappedView = ((RestTreeView) innerLevelView).getResourceRamButtonOf((AccessMethod) element);
            }
            
        } else if (element instanceof DynamicFragment) {
            if (outerLevelView instanceof RestTreeView) {
                mappedView = ((RestTreeView) outerLevelView).getPathFragmentViewOf((PathFragment) element);
            } else {
                mappedView = ((RestTreeView) innerLevelView).getPathFragmentViewOf((PathFragment) element);
            }
        } else if (element instanceof Operation) {
            if (outerLevelView instanceof StructuralDiagramView) {
                mappedView = ((StructuralDiagramView) outerLevelView).getOperationViewOf((Operation) element);
            } else {
                mappedView = ((StructuralDiagramView) innerLevelView).getOperationViewOf((Operation) element);
            }
        } else if (element instanceof Parameter) {
            if (outerLevelView instanceof StructuralDiagramView) {
                mappedView = ((StructuralDiagramView) outerLevelView).getParameterViewOf((Parameter) element);
            } else {
                mappedView = ((StructuralDiagramView) innerLevelView).getParameterViewOf((Parameter) element);
            }
        }
        
        return mappedView;
    }

    /**
     * Get a modified x and y coordinate in the form of a Vector3D from the given view.
     * 
     * @param view the {@link RamRectangleComponent}
     * 
     * @return the modified {@link Vector3D}
     */
    @Override
    public Vector3D getModifiedXYFromView(RamRectangleComponent view) {
        Vector3D xyVector = new Vector3D();
        
        if (view != null) {
            if (view instanceof OperationView || view instanceof ParameterView) {
                xyVector.setX(view.getX());
                xyVector.setY(view.getY() + view.getGlobalHeight() / 2);
            } else if (view instanceof ResourceRamButton) {
                xyVector.setX(view.getCenterPointGlobal().getX());
                xyVector.setY(view.getY() + view.getGlobalHeight() / 2);
            } else if (view instanceof PathFragmentView) {
                xyVector.setX(view.getX() + view.getGlobalWidth() / 2);
                xyVector.setY(view.getY() + view.getGlobalHeight() / 4);
            } else {
                xyVector.setX(view.getX());
                xyVector.setY(view.getY());
            }
        }
        
        return xyVector;
    }

    /**
     * Set the color of the given mapping line depending on the types of the first and second mapped elements.
     * 
     * @param mappingLine the associated {@link RamLineComponent}
     * @param firstMappedElement the first {@link RamRectangleComponent} associated with the line
     * @param secondMappedElement the second {@link RamRectangleComponent} associated with the line
     */
    @Override
    public void setLineColorBasedOnMapping(RamLineComponent mappingLine, RamRectangleComponent firstMappedElement,
            RamRectangleComponent secondMappedElement) {
        if (firstMappedElement instanceof PathFragmentView && secondMappedElement instanceof ParameterView 
                || firstMappedElement instanceof ParameterView && secondMappedElement instanceof PathFragmentView) {
            mappingLine.setStrokeColor(MTColor.GREEN);
        } else {
            mappingLine.setStrokeColor(MTColor.BLUE);
        }
    }

    /**
     * Check if a given mapped element is within the inner view.
     * 
     * @param mappedElement the given {@link RamRectangleComponent}
     * 
     * @return whether or not the element is within the inner view
     */
    @Override
    protected boolean checkElementWithinInnerView(RamRectangleComponent mappedElement) {
        if (displaySceneInnerLevel instanceof DisplayRestTreeScene) {
            if (mappedElement instanceof PathFragmentView || mappedElement instanceof ResourceRamButton) {
                return true;
            }
        } else {
            if (mappedElement instanceof OperationView || mappedElement instanceof ParameterView) {
                return true;
            }
        }
        
        return false;
    }

    /**
     * Create a stippled line given the position of the view and the actual view.

     * @param mappedElementXY the given {@link Vector3D} coordinates
     * @param mappedElement the given {@link RamRectangleComponent} view
     * 
     * @return the created stippled {@link RamLineComponent}
     */
    @Override
    protected RamLineComponent createStippledLine(Vector3D mappedElementXY, RamRectangleComponent mappedElement) {
        RamLineComponent mappingLine;
        
        if (mappedElement instanceof OperationView) {            
            mappingLine = new RamLineComponent(mappedElementXY.getX(),
                    mappedElementXY.getY(), mappedElementXY.getX() 
                    - mappedElement.getGlobalWidth() / 2, mappedElementXY.getY());
        } else if (mappedElement instanceof ResourceRamButton) {
            mappingLine = new RamLineComponent(mappedElementXY.getX(),
                    mappedElementXY.getY(), mappedElementXY.getX(),
                    mappedElementXY.getY() + mappedElement.getGlobalHeight());
        } else {
            mappingLine = new RamLineComponent(mappedElementXY.getX(),
                    mappedElementXY.getY(), mappedElementXY.getX() 
                    + mappedElement.getGlobalWidth() / 2, mappedElementXY.getY());
        }
        mappingLine.setLineStipple((short) 1000);

        return mappingLine;
    }

    /**
     * Get the view from a scene. 
     * 
     * @param scene the {@link RamAbstractScene} to get the view from
     * 
     * @return the {@link AbstractView}
     */
    @Override
    public AbstractView<?> getViewFromScene(RamAbstractScene<?> scene) {
        if (scene instanceof DisplayRestTreeScene) {
            return ((DisplayRestTreeScene) scene).getRestTreeView();
        } else {
            return ((DisplayAspectScene) scene).getStructuralDiagramView();
        }
    }
}
