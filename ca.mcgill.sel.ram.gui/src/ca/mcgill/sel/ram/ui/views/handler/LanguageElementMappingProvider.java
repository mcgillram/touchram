package ca.mcgill.sel.ram.ui.views.handler;

import org.eclipse.emf.common.util.EList;

import ca.mcgill.sel.core.COREModelElementMapping;
import ca.mcgill.sel.ram.ui.views.GenericSplitView;

/**
 * This is a helper class for {@link GenericSplitView} to find the next available id for mappings.
 * 
 * @author Bowen
 */
public class LanguageElementMappingProvider {
    // variable to store the next available id
    private int nextLEMid;

    /**
     * This constructor takes in mappings as a parameter and sets nextLEMid to the highest existing id in mappings.
     * 
     * @param mappings
     */
    public LanguageElementMappingProvider(EList<COREModelElementMapping> mappings) {
        nextLEMid = findHighestLEMid(mappings);
    }
    
    /**
     * This method finds the highest LEMid with a simple for loop.
     * 
     * @param mappings the given {@link EList}
     * @return the highest LEMid found in mappings
     */
    public int findHighestLEMid(EList<COREModelElementMapping> mappings) {
        int maxLEMid = 0;
        
        for (COREModelElementMapping mapping : mappings) {
            if (mapping.getLEMid() > maxLEMid) {
                maxLEMid = mapping.getLEMid();
            }
        }
        
        return maxLEMid;    
    }
    
    /**
     * Increments LEMid and return it.
     * 
     * @return LEMid
     */
    public int getNextLEMid() {
        nextLEMid++;
        return nextLEMid;
    }
}
