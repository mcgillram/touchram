package ca.mcgill.sel.ram.ui.views.state.handler;

import ca.mcgill.sel.ram.ui.events.listeners.ITapAndHoldListener;
import ca.mcgill.sel.ram.ui.events.listeners.ITapListener;
import ca.mcgill.sel.ram.ui.views.handler.IAbstractViewHandler;
import ca.mcgill.sel.ram.ui.views.state.StateDiagramView;
import ca.mcgill.sel.ram.ui.views.state.StateViewView;

/**
 * This interface is implemented by something that can handle events for a {@link StateDiagramView}.
 * 
 * @author mschoettle
 */
public interface IStateViewHandler extends IAbstractViewHandler, ITapListener, ITapAndHoldListener {
    
    /**
     * Operation to handle tap and hold on state views.
     * 
     * @param stateDigram the diagram
     * @param stateView the state view view
     * @return whether or not the tap and hold has been handled
     */
    boolean handleTapAndHoldOnStateView(StateDiagramView stateDigram, StateViewView stateView);
    
}
