package ca.mcgill.sel.ram.ui.views.handler.impl;

import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;
import org.mt4j.sceneManagement.transition.BlendTransition;

import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.COREModelExtension;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamSelectorComponent;
import ca.mcgill.sel.ram.ui.components.listeners.AbstractDefaultRamSelectorListener;
import ca.mcgill.sel.ram.ui.components.navigationbar.NavigationBar;
import ca.mcgill.sel.ram.ui.components.navigationbar.SlideUpDownTransition;
import ca.mcgill.sel.ram.ui.scenes.DisplayAspectScene;
import ca.mcgill.sel.ram.ui.scenes.RamAbstractScene;
import ca.mcgill.sel.ram.ui.scenes.SceneCreationAndChangeFactory;
import ca.mcgill.sel.ram.ui.scenes.SceneCreationAndChangeFactory.CurrentMode;
import ca.mcgill.sel.ram.ui.utils.GenericWeaverRunner;
import ca.mcgill.sel.ram.ui.utils.WeaverRunner;
import ca.mcgill.sel.ram.ui.views.CompositionTitleView;
import ca.mcgill.sel.ram.ui.views.CompositionView;
import ca.mcgill.sel.ram.ui.views.OptionSelectorView;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.ICompositionNameHandler;
import ca.mcgill.sel.ram.ui.views.structural.CompositionSplitEditingView;

/**
 * The default handler for a model composition name which is represented by a {@link TextView} in the
 * {@link CompositionTitleView}.
 * 
 * @author eyildirim
 */
public class CompositionDefaultNameHandler extends TextViewHandler implements ICompositionNameHandler {

    /**
     * Enum with possible actions to do on model compositions.
     */
    private enum Actions {
        OPEN_MODEL, WEAVE_MODEL
    }

    @Override
    public boolean processTapAndHoldEvent(final TapAndHoldEvent tapAndHoldEvent) {
        if (tapAndHoldEvent.isHoldComplete()) {
            OptionSelectorView<Actions> selector = new OptionSelectorView<Actions>(Actions.values());
            ((RamAbstractScene<?>) RamApp.getActiveScene())
                    .addComponent(selector, tapAndHoldEvent.getLocationOnScreen());

            selector.registerListener(new AbstractDefaultRamSelectorListener<Actions>() {

                @Override
                public void elementSelected(RamSelectorComponent<Actions> selector, Actions element) {
                    final TextView target = (TextView) tapAndHoldEvent.getTarget();
                    if (!(target.getParent() instanceof CompositionTitleView)) {
                        return;
                    }
                    CompositionView view = (CompositionView) target.getParent().getParent();
                    COREModelComposition modelComposition = view.getModelComposition();

                    if (element == Actions.WEAVE_MODEL) {
                        weaveModel(modelComposition);
                    } else if (element == Actions.OPEN_MODEL) {
                        openModel(modelComposition);
                    }
                }
            });
        }

        return true;
    }

    /**
     * Open the source model corresponding to the composition.
     * 
     * @param modelComposition the model composition to display
     */
    protected void openModel(COREModelComposition modelComposition) {
        // Get the handler of the display aspect scene to call its function for switching into composition edit mode.
        //IDisplaySceneHandler handler = HandlerFactory.INSTANCE.getDisplayAspectSceneHandler();

        // Get the latest used display scene
        RamAbstractScene<?> displayScene = RamApp.getActiveScene();

        COREExternalArtefact source = (COREExternalArtefact) modelComposition.getSource();
//        if (displayScene != null && isSplitViewEnabled(displayScene)) {
//            // we are already in split composition editing mode, switch back to normal mode and then load aspect.
//            handler.closeSplitView(displayAspectScene);
//            RamApp.getApplication().loadScene((COREExternalArtefact) modelComposition.getSource(), source);
//        } else {
        
        // loading external model specifically coming from a model extension context
        if (modelComposition instanceof COREModelExtension) {
            displayScene.setTransition(new SlideUpDownTransition(RamApp.getApplication(), 700, false));
            SceneCreationAndChangeFactory.getFactory().navigateToModel(source.getRootModelElement(), CurrentMode.EDIT);
        } else {
            // loading external model specifically coming from a model reuse context
            displayScene.setTransition(new BlendTransition(RamApp.getApplication(), 700));
            NavigationBar.getInstance().collapseStartingEnvironment(displayScene.getSceneRootModelObject());            
            SceneCreationAndChangeFactory.getFactory().navigateToModel(source.getRootModelElement(), CurrentMode.EDIT);
        }        
        return;
    }

    /**
     * Weave the current model with the selected one.
     * 
     * @param modelComposition the model composition to display
     */
    private static void weaveModel(COREModelComposition modelComposition) {
        // check if the split composition editing mode is enabled. If not we can do the weaving.
//        boolean splitModeEnabled = isSplitViewEnabled(RamApp.getActiveAspectScene());
//
//        if (!splitModeEnabled) {
            // Weave the tapped composition
        if (((COREExternalArtefact) modelComposition.eContainer()).getRootModelElement() instanceof Aspect) {
            WeaverRunner.getInstance().weaveSingle(modelComposition);            
        } else {
            GenericWeaverRunner.getInstance().weaveSingle((COREExternalArtefact) modelComposition.eContainer(),
                    modelComposition);
        }
        
//        } else {
//            RamApp.getActiveScene().displayPopup(Strings.POPUP_CANT_WEAVE_EDITMODE);
//        }
    }

    /**
     * Returns whether split view mode is enabled.
     * 
     * @param scene the current scene
     * @return true, if split view mode is enabled, false otherwise
     */
    private static boolean isSplitViewEnabled(DisplayAspectScene scene) {
        return scene.getCurrentView() instanceof CompositionSplitEditingView;
    }

}
