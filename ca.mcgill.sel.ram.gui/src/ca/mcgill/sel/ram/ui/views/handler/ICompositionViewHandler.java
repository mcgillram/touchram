package ca.mcgill.sel.ram.ui.views.handler;

import ca.mcgill.sel.ram.ui.events.listeners.ITapListener;
import ca.mcgill.sel.ram.ui.views.CompositionView;

public interface ICompositionViewHandler extends ITapListener {
    /**
     * Hides all mapping details.
     * 
     * @param compositionView the composition view
     */
    void hideMappingDetails(CompositionView<?> compositionView);
    
    /**
     * Loads the external Aspect in full mode.
     * 
     * @param myCompositionView the composition
     */
    void showExternalAspectOfComposition(CompositionView<?> myCompositionView);
    
    /**
     * Shows all classifier mapping details.
     * 
     * @param compositionView the composition view
     */
    void showMappingDetails(CompositionView<?> compositionView);
    
    /**
     * Switches to the split view for mapping specifications.
     * 
     * @param myCompositionView the composition
     */
    void switchToSplitView(CompositionView<?> myCompositionView);
}
