package ca.mcgill.sel.ram.ui.views;

import java.util.List;

import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREModelElementMapping;

/**
 * Interface for implementations of {@link GenericSplitViewWithMappings}. Implementations of the split view with
 * mappings are required to also define a mappings validator to ensure that the mappings are correct.
 * 
 * @author Bowen
 */
public interface CoreModelElementMappingsValidator {
    /**
     * Method to check if the input 2 elements are valid as a mapping. Implementing this method requires one to
     * check the types of the elements and perhaps enforce some rules about the accepted mappings. If the mapping is not
     * valid, display an error message to the user.
     * 
     * @param firstModelElement first model in mapping
     * @param secondModelElement second model in mapping
     * @param mappings the current mappings of the core scene
     * @param concern the concern that contains the inner models as COREExternalArtefacts
     * @return whether or not the mapping is valid or not
     */
    boolean validateNewMapping(EObject firstModelElement, EObject secondModelElement, 
            List<COREModelElementMapping> mappings, COREConcern concern);
    
    /**
     * Method to check if the input mappings are valid. This method will return the mappings that are invalid,
     * which need to be removed. Implementing this method requires one to check for enforced rules about mappings.
     * 
     * @param mappings input mappings
     * @param concern the concern that contains the inner models as COREExternalArtefacts
     * @return invalid mappings that violate the enforced rules
     */
    List<COREModelElementMapping> validateMappingsForEditing(List<COREModelElementMapping> mappings, 
            COREConcern concern);
    
    /**
     * Method to check if all of the input mappings are valid for a transformation to undergo. This method can
     * potentially validate mappings with the method {@link #validateMappingsForEditing(List, COREConcern)}, while
     * checking for even more validation rules.
     * 
     * @param mappings input mappings
     * @param concern the concern that contains the inner models as COREExternalArtefacts
     * @return whether the mappings are valid for a transformation or not
     */
    boolean validateMappingsForTransformation(List<COREModelElementMapping> mappings, COREConcern concern);
}
