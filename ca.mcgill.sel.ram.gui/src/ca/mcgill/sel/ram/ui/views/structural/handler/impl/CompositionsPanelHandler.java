package ca.mcgill.sel.ram.ui.views.structural.handler.impl;

import java.util.Collection;
import java.util.Map;

import org.mt4j.input.inputProcessors.MTGestureEvent;
import org.mt4j.input.inputProcessors.componentProcessors.dragProcessor.DragEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.CompositionsPanel;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.views.CompositionContainerView;
import ca.mcgill.sel.ram.ui.views.CompositionView;
import ca.mcgill.sel.ram.ui.views.handler.BaseHandler;
import ca.mcgill.sel.ram.ui.views.structural.handler.ICompositionPanelHandler;

/**
 * Handles events for a {@link CompositionsPanel} which is showing the list {@link CompositionContainerView} in an
 * aspect.
 * 
 * @author eyildirim
 * @author oalam
 */
public class CompositionsPanelHandler extends BaseHandler implements ICompositionPanelHandler {

    @Override
    public boolean processDragEvent(DragEvent dragEvent) {

        CompositionsPanel target =
                (CompositionsPanel) dragEvent.getTarget();

        switch (dragEvent.getId()) {
            case MTGestureEvent.GESTURE_CANCELED:
                break;
            case MTGestureEvent.GESTURE_ENDED:
                break;
            case MTGestureEvent.GESTURE_UPDATED:
                // we just want it along y axis
                float translation = dragEvent.getTranslationVect().y;
                
                if (translation < 0) {
                    target.setBottomStick(false);
                }
                
                float currentY = target.getY();

                float correctY = target.getCorrectYPosition(currentY + translation);
                translation = -currentY + correctY;
                
                if (correctY == RamApp.getApplication().getSize().height - target.getHeight()) {
                    target.setBottomStick(true);
                }

                Vector3D translationVector = new Vector3D(0, translation, 0);
                target.translate(translationVector);
                break;
        }

        return true;

    }

    @Override
    public boolean processTapEvent(TapEvent tapEvent) {
        if (tapEvent.isTapped()) {
            CompositionsPanel target =
                    (CompositionsPanel) tapEvent.getTarget();
            target.doSlideInOut();
        }

        return true;
    }

    @Override
    public void switchFromCompositionSplitEditMode(
            CompositionsPanel compositionsContainerContainer,
            COREModelComposition composition) {

        for (CompositionContainerView compositionsContainerView : compositionsContainerContainer
                .getCompositionsContainers()) {
            Map<COREModelComposition, CompositionView> compositions = 
                    compositionsContainerView.getCompositionViews();
            if (!compositions.containsKey(composition)) {
                // Display given container again
                compositionsContainerContainer.showCompositionsContainerView(compositionsContainerView);
            } else {
                // Add plus button.
                compositionsContainerView.initButtons();
                // Remember, everything we keep in compositionsContainerView is kept in the
                // roundCompositionsContainer which is a child of it.
                RamRectangleComponent compositionsContainer =
                        compositionsContainerView.getCompositionsContainer();

                // First remove all children including the buttonContainer
                compositionsContainer.removeAllChildren();

                Collection<CompositionView> compositionsToCompositionViews = compositionsContainerView
                        .getCompositionViews().values();

                for (CompositionView compositionView : compositionsToCompositionViews) {
                    compositionsContainer.addChild(compositionView);
                }
            }
        }

    }

    @Override
    public void switchToCompositionSplitEditMode(CompositionsPanel container,
            COREModelComposition composition) {
        for (CompositionContainerView compositionsContainerView : container
                .getCompositionsContainers()) {
            Map<COREModelComposition, CompositionView> compositions = 
                    compositionsContainerView.getCompositionViews();
            if (!compositions.containsKey(composition)) {
                // Display given container again
                container.hideCompositionsContainerView(compositionsContainerView);
            } else {
                // Remember, everything we keep in compositionsContainerView is kept in the container
                // which is a child of it.
                RamRectangleComponent containerView = compositionsContainerView.getCompositionsContainer();

                // Remove the composition adding button
                RamRectangleComponent buttonContainer = compositionsContainerView.getButtonContainer();
                buttonContainer.removeAllChildren();

                // First remove all children including the buttonContainer
                containerView.removeAllChildren();

                // get the composition view that we want to show while the split composition editing view is active
                CompositionView compositionViewBeingEdited = compositions.get(composition);

                containerView.addChild(compositionViewBeingEdited);
                compositionViewBeingEdited.showMappingDetails();
            }
        }
    }
}
