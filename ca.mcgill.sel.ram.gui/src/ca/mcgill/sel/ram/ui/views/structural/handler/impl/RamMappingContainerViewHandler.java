package ca.mcgill.sel.ram.ui.views.structural.handler.impl;

import ca.mcgill.sel.ram.AttributeMapping;
import ca.mcgill.sel.ram.ClassifierMapping;
import ca.mcgill.sel.ram.EnumLiteralMapping;
import ca.mcgill.sel.ram.EnumMapping;
import ca.mcgill.sel.ram.OperationMapping;
import ca.mcgill.sel.ram.ParameterMapping;
import ca.mcgill.sel.ram.controller.ControllerFactory;
import ca.mcgill.sel.ram.controller.RamMappingsController;
import ca.mcgill.sel.ram.ui.views.structural.handler.IRamMappingContainerViewHandler;

/**
 * Default handler for {@link ca.mcgill.sel.ram.ui.views.structural.ClassifierMappingContainerView}.
 * 
 * @author eyildirim
 */
public class RamMappingContainerViewHandler implements IRamMappingContainerViewHandler {
    
    @Override
    public void addAttributeMapping(ClassifierMapping classifierMapping) {
        if (classifierMapping.getFrom() != null && classifierMapping.getTo() != null) {
            
            RamMappingsController mappingsController = ControllerFactory.INSTANCE.getRamMappingsController();
            mappingsController.createAttributeMapping(classifierMapping);
        }
        
    }
    
    @Override
    public void addEnumLiteralMapping(EnumMapping enumMapping) {
        if (enumMapping.getFrom() != null && enumMapping.getTo() != null) {
            
            RamMappingsController mappingsController = ControllerFactory.INSTANCE.getRamMappingsController();
            mappingsController.createEnumLiteralMapping(enumMapping);
        }
        
    }
    
    @Override
    public void addOperationMapping(ClassifierMapping classifierMapping) {
        if (classifierMapping.getFrom() != null && classifierMapping.getTo() != null) {
            RamMappingsController mappingsController = ControllerFactory.INSTANCE.getRamMappingsController();
            mappingsController.createOperationMapping(classifierMapping);
        }
    }
    
    @Override
    public void addParameterMapping(OperationMapping operationMapping) {
        if (operationMapping.getFrom() != null && operationMapping.getTo() != null) {
            RamMappingsController mappingsController = ControllerFactory.INSTANCE.getRamMappingsController();
            mappingsController.createParameterMapping(operationMapping);
        }
    }
    
    @Override
    public void deleteAttributeMapping(AttributeMapping attributeMapping) {
        RamMappingsController mappingsController = ControllerFactory.INSTANCE.getRamMappingsController();
        mappingsController.deleteMapping(attributeMapping);
    }
    
    @Override
    public void deleteEnumLiteralMapping(EnumLiteralMapping enumLiteralMapping) {
        RamMappingsController mappingsController = ControllerFactory.INSTANCE.getRamMappingsController();
        mappingsController.deleteMapping(enumLiteralMapping);
    }
    
    @Override
    public void deleteClassifierMapping(ClassifierMapping classifierMapping) {
        RamMappingsController mappingsController = ControllerFactory.INSTANCE.getRamMappingsController();
        mappingsController.deleteMapping(classifierMapping);
    }
    
    @Override
    public void deleteEnumMapping(EnumMapping enumMapping) {
        RamMappingsController mappingsController = ControllerFactory.INSTANCE.getRamMappingsController();
        mappingsController.deleteMapping(enumMapping);
    }

    @Override
    public void deleteOperationMapping(OperationMapping operationMapping) {
        RamMappingsController mappingsController = ControllerFactory.INSTANCE.getRamMappingsController();
        mappingsController.deleteMapping(operationMapping);
    }

    @Override
    public void deleteParameterMapping(ParameterMapping parameterMapping) {
        RamMappingsController mappingsController = ControllerFactory.INSTANCE.getRamMappingsController();
        mappingsController.deleteMapping(parameterMapping);
    }

}
