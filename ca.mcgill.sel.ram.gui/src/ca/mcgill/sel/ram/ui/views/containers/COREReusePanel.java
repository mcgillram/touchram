package ca.mcgill.sel.ram.ui.views.containers;

import java.util.ArrayList;
import java.util.List;

import org.mt4j.sceneManagement.transition.BlendTransition;

import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREModelReuse;
import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.core.COREReuseArtefact;
import ca.mcgill.sel.core.controller.COREControllerFactory;
import ca.mcgill.sel.ram.ui.components.RamListComponent.Namer;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamButton;
import ca.mcgill.sel.ram.ui.components.RamImageComponent;
import ca.mcgill.sel.ram.ui.components.RamPanelListComponent;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.ram.ui.components.listeners.RamListListener;
import ca.mcgill.sel.ram.ui.components.navigationbar.NavigationBar;
import ca.mcgill.sel.ram.ui.events.listeners.ActionListener;
import ca.mcgill.sel.ram.ui.layouts.HorizontalLayoutVerticallyCentered;
import ca.mcgill.sel.ram.ui.scenes.SceneCreationAndChangeFactory;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;

/**
 * Panel container that visualizes the Reuses of a feature model.
 * 
 * @author arthurls
 */
public class COREReusePanel extends RamPanelListComponent<COREReuse> {
    
    private static final float ICON_SIZE = Icons.ICON_ADD.width - 8;
    
    private List<COREReuse> reuses;
        
    /**
     * Creates a new panel with the given horizontal and vertical sticks.
     * 
     * @param horizontalStick The horizontalStick for the panel
     * @param verticalStick The verticalStick for the panel
     * @param reuses List of the concern reuses
     * @param listener The custom listener for that panel
     */
    public COREReusePanel(HorizontalStick horizontalStick, VerticalStick verticalStick, List<COREReuse> reuses, 
            RamListListener<COREReuse> listener) {
        super(5, Strings.LABEL_MODEL_REUSE, horizontalStick, verticalStick);
        
        this.reuses = reuses;
        setNamer(new InternalReusePanelNamer());
        setListener(listener);
        setElements(reuses);
    }

    
    /**
     * Internal Reuse Panel Namer.
     */
    private class InternalReusePanelNamer implements Namer<COREReuse>, ActionListener {

        @Override
        public RamRectangleComponent getDisplayComponent(COREReuse reuse) {
            return createReusePanelComponent(reuse, false);
        }

        @Override
        public String getSortingName(COREReuse reuse) {
            return reuse.getName();
        }

        @Override
        public String getSearchingName(COREReuse reuse) {
            return reuse.getName();
        }
        
        /**
         * Internal method to create an element of the list.
         * @param reuse the reuse we want to display in the list
         * @param noFill 
         * @return the RamRectangle Component
         */
        private RamRectangleComponent createReusePanelComponent(COREReuse reuse, boolean noFill) {
            RamRectangleComponent view = new RamRectangleComponent();
            view.setLayout(new HorizontalLayoutVerticallyCentered());
            
            if (isReuseFromReuseArtefact(reuse)) {
                // add a button for editing the configuration of the core reuse's model reuses
                RamImageComponent editConfigurationImage = new RamImageComponent(Icons.ICON_TAILOR_REUSE,
                        Colors.ICON_STRUCT_DEFAULT_COLOR, ICON_SIZE, ICON_SIZE);
                RamButton editConfigurationButton = new RamButton(editConfigurationImage);
                editConfigurationButton.setActionCommand(reuse.getName() + "_add");
                editConfigurationButton.addActionListener(this);
                            
                view.addChild(editConfigurationButton);
                
                // add a button for removing a selected core reuse
                RamImageComponent removeReuseImage = new RamImageComponent(Icons.ICON_DELETE, 
                        Colors.ICON_STRUCT_DEFAULT_COLOR, ICON_SIZE, ICON_SIZE);
                RamButton removeReuseButton = new RamButton(removeReuseImage);
                removeReuseButton.setActionCommand(reuse.getName() + "_delete");
                removeReuseButton.addActionListener(this);
                
                view.addChild(removeReuseButton);
            }

            // Removes quotes of the message
            RamTextComponent text = new RamTextComponent(reuse.getName());
            text.setPickable(false);

            // Adds spaces in left and right
            final int space = 5;
            view.setBufferSize(Cardinal.WEST, space);
            view.setBufferSize(Cardinal.EAST, space);

            view.addChild(text);

            view.setNoFill(noFill);
            view.setFillColor(Colors.DEFAULT_ELEMENT_FILL_COLOR);
            view.setNoStroke(false);

            return view;
        }

        @Override
        public void actionPerformed(ActionEvent event) {
            // called when edit configuration and remove concern buttons are pressed
            
            String actionCommand = event.getActionCommand();
            
            int indexOfLastUnderscore = actionCommand.lastIndexOf('_');
            
            // extract reuse name from action command
            String selectedReuseName = actionCommand.substring(0, indexOfLastUnderscore);
            // extract selected action from action command (add/delete)
            String selectedAction = actionCommand.substring(indexOfLastUnderscore + 1);
            
            COREConcern currentConcern = NavigationBar.getInstance().getCurrentConcern();
            
            // copy reuses of current concern to avoid concurrent modification exception
            List<COREReuse> reusesCopy = new ArrayList<>();
            reusesCopy.addAll(currentConcern.getReuses());
            
            // loop through all reuses to find the associated reuse with the action
            for (COREReuse reuse : reusesCopy) {
                if (reuse.getName().equals(selectedReuseName)) {
                    COREConcern reusedConcern = reuse.getReusedConcern();
                    
                    // if selected action is add
                    // transition to the ConcernSelectScene with the reused concern to choose a new configuration
                    if ("add".equals(selectedAction)) {
                        RamApp.getActiveScene().setTransition(new BlendTransition(RamApp.getApplication(), 700));
                        
                        SceneCreationAndChangeFactory.getFactory()
                            .navigateToConcernSelectSceneAndSetReuse(reusedConcern, reuse);
                    } else if ("delete".equals(selectedAction)) {
                        // if selected action is delete
                        // delete the core reuse and all associated model reuses from the current concern
                        COREControllerFactory.INSTANCE.getReuseController().removeCOREReuseAndModelReuses(NavigationBar
                                .getInstance().getCurrentConcern(), reuse);
                    }
                }
            }
        }
    }
    
    /**
     * Returns the list of reuses contained in the list.
     * @return list of reuses
     */
    public List<COREReuse> getReuses() {
        return reuses;
    }
    
    /**
     * Changes the color of the current selected reuse in the list.
     * Returns true if the reuse given is in the list
     * Returns false if the reuse is not in the list
     * @param reuse to highlight in the list
     * @return list.contains(reuse)
     */
    public boolean setSelectedReuse(COREReuse reuse) {
        if (list.getElementMap().containsKey(reuse)) {
            list.getElementMap().get(reuse).setFillColor(Colors.FEATURE_VIEW_IS_REUSING);
            return true;
        }
        return false;
    }
    
    public void clearSelections() {
        for (COREReuse r : reuses) {
            list.getElementMap().get(r).setFillColor(Colors.DEFAULT_ELEMENT_FILL_COLOR);
        }
    }
    
    /**
     * Checks if the reuse comes from the {@link COREReuseArtefact} from the current concern.
     * 
     * @param reuse input reuse
     * @return whether or not the input reuse comes from a reuse artefact
     */
    public boolean isReuseFromReuseArtefact(COREReuse reuse) {
        COREConcern concern = NavigationBar.getInstance().getCurrentConcern();
        
        for (COREArtefact artefact : concern.getArtefacts()) {
            if (artefact instanceof COREReuseArtefact) {
                for (COREModelReuse modelReuse : artefact.getModelReuses()) {
                    if (modelReuse.getReuse().equals(reuse)) {
                        return true;
                    }
                }
            }
        }
        
        return false;
    }
    
}
