package ca.mcgill.sel.ram.ui.views.structural;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.mt4j.components.MTComponent;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.COREMapping;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.ram.AttributeMapping;
import ca.mcgill.sel.ram.ClassifierMapping;
import ca.mcgill.sel.ram.OperationMapping;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamRoundedRectangleComponent;
import ca.mcgill.sel.ram.ui.components.listeners.UINotifyChangedListener;
import ca.mcgill.sel.ram.ui.layouts.VerticalLayout;
import ca.mcgill.sel.ram.ui.utils.Colors;

/**
 * This view contains a ClassifierMappingView and all mapping related container views for a classifier mapping.
 * 
 * @author eyildirim
 */
public class ClassifierMappingContainerView extends RamRoundedRectangleComponent implements UINotifyChangedListener {
    // It is useful to keep the child as a reference for hiding/showing purposes
    private ClassifierMappingView myClassifierMappingView;

    // Classifier Mapping information
    private ClassifierMapping myClassifierMapping;

    // All the attribute mapping related views will be in this view:
    private RamRectangleComponent hideableAttributeContainer;

    // All the operation mapping related views will be in this view:
    private RamRectangleComponent hideableOperationContainer;    
    
    /**
     * Creates a new mapping container view.
     * 
     * @param classifierMapping {@link ClassifierMapping} which we want to create a
     *            {@link ClassifierMappingContainerView} for.
     */
    public ClassifierMappingContainerView(ClassifierMapping classifierMapping) {
        super(0);
        setNoStroke(false);
        setStrokeWeight(3);
        setNoFill(false);
        setFillColor(Colors.MAPPING_CONTAINER_VIEW_FILL_COLOR);

        myClassifierMappingView = new ClassifierMappingView(classifierMapping);
        this.addChild(myClassifierMappingView);

        myClassifierMapping = classifierMapping;

        // Container for the Attributes
        hideableAttributeContainer = new RamRectangleComponent();
        hideableAttributeContainer.setLayout(new VerticalLayout(0));
        hideableAttributeContainer.setFillColor(Colors.ATTRIBUTE_MAPPING_VIEW_FILL_COLOR);
        hideableAttributeContainer.setNoFill(false);
        hideableAttributeContainer.setBufferSize(Cardinal.EAST, 0);

        // Container for the Operation
        hideableOperationContainer = new RamRectangleComponent();
        hideableOperationContainer.setLayout(new VerticalLayout(0));
        hideableOperationContainer.setFillColor(Colors.OPERATION_MAPPING_VIEW_FILL_COLOR);
        hideableOperationContainer.setNoFill(false);
        hideableOperationContainer.setBufferSize(Cardinal.EAST, 0);
        
        this.addChild(hideableAttributeContainer);
        this.addChild(hideableOperationContainer);

        setLayout(new VerticalLayout());
        setBuffers(0);
        
        // Add all the operation and attribute mappings related to this classifier mapping.
        addAllOperationMappings();
        addAllAttributeMappings();

        EMFEditUtil.addListenerFor(myClassifierMapping, this);
    }

    /**
     * Adds all the attribute mappings which belongs to the classifier mapping.
     */
    private void addAllAttributeMappings() {
        EList<COREMapping<?>> coreMappings = myClassifierMapping.getMappings();
        for (COREMapping<?> mapping : coreMappings) {
            if (mapping instanceof AttributeMapping) {
                addAttributeMappingView((AttributeMapping) mapping);
            }
        }
    }

    /**
     * Adds all the operation mappings which belongs to the classifier mapping.
     * The operation are contained in a {@link OperationMappingContainerView}
     */
    private void addAllOperationMappings() {
        EList<COREMapping<?>> coreMappings = myClassifierMapping.getMappings();
        for (COREMapping<?> mapping : coreMappings) {
            if (mapping instanceof OperationMapping) {
                addOperationMappingContainerView((OperationMapping) mapping);
            }
        }
    }
    
    /**
     * Adds an {@link AttributeMappingView} to the mapping container view (inside of hideableAttributeContainer).
     * 
     * @param newAttributeMapping the {@link AttributeMapping} to add a view for
     */
    private void addAttributeMappingView(AttributeMapping newAttributeMapping) {
        AttributeMappingView attributeMappingView = new AttributeMappingView(newAttributeMapping);
        hideableAttributeContainer.addChild(attributeMappingView);
    }

    /**
     * Adds an {@link OperationMappingContainerView} to the mapping container view.
     * 
     * @param newOperationMapping the {@link OperationMapping} to add a container for
     */
    private void addOperationMappingContainerView(OperationMapping newOperationMapping) {
        OperationMappingContainerView operationMappingContainerView = 
                new OperationMappingContainerView(newOperationMapping);
        hideableOperationContainer.addChild(operationMappingContainerView);
    }

    /**
     * Deletes an {@link AttributeMappingView} from the mapping container view.
     * 
     * @param deletedAttributeMapping the {@link AttributeMapping} to remove the view for
     */
    private void deleteAttributeMappingView(AttributeMapping deletedAttributeMapping) {
        MTComponent[] attributeMappingViews = hideableAttributeContainer.getChildren();
        for (MTComponent view : attributeMappingViews) {
            if (view instanceof AttributeMappingView) {
                AttributeMappingView attributeMappingView = (AttributeMappingView) view;
                if (attributeMappingView.getAttributeMapping() == deletedAttributeMapping) {
                    hideableAttributeContainer.removeChild(attributeMappingView);
                }
            }
        }
    }

    /**
     * Deletes an {@link OperationMappingContainerView} from the mapping container view.
     * 
     * @param deletedOperationMapping the {@link OperationMapping} to remove the container for
     */
    private void deleteOperationMappingContainerView(OperationMapping deletedOperationMapping) {
        MTComponent[] operationMappingContainerViews = hideableOperationContainer.getChildren();
        for (MTComponent view : operationMappingContainerViews) {
            if (view instanceof OperationMappingContainerView) {
                OperationMappingContainerView operationMappingContainerView = (OperationMappingContainerView) view;
                if (operationMappingContainerView.getOperationMapping() == deletedOperationMapping) {
                    hideableOperationContainer.removeChild(operationMappingContainerView);
                }
            }
        }
    }

    @Override
    public void destroy() {
        EMFEditUtil.removeListenerFor(myClassifierMapping, this);
        super.destroy();
    }

    /**
     * Getter for ClassifierMapping information of the view.
     * 
     * @return {@link ClassifierMapping}
     */
    public ClassifierMapping getClassifierMapping() {
        return myClassifierMapping;
    }

    @Override
    public void handleNotification(Notification notification) {
        if (notification.getNotifier() == myClassifierMapping) {
            if (notification.getFeature() == CorePackage.Literals.CORE_MAPPING__MAPPINGS) {
                switch (notification.getEventType()) {
                    case Notification.ADD:
                        COREMapping<?> newMapping = (COREMapping<?>) notification.getNewValue();
                        
                        if (newMapping instanceof AttributeMapping) {
                            AttributeMapping newAttributeMapping = (AttributeMapping) newMapping;
                            addAttributeMappingView(newAttributeMapping);
                        } else if (newMapping instanceof OperationMapping) {
                            OperationMapping newOperationMapping = (OperationMapping) newMapping;
                            addOperationMappingContainerView(newOperationMapping);                          
                        }
                        break;

                    case Notification.REMOVE:
                        COREMapping<?> oldMapping = (COREMapping<?>) notification.getOldValue();

                        if (oldMapping instanceof AttributeMapping) {
                            AttributeMapping oldAttributeMapping = (AttributeMapping) oldMapping;
                            deleteAttributeMappingView(oldAttributeMapping);
                        } else if (oldMapping instanceof OperationMapping) {
                            OperationMapping oldOperationMapping = (OperationMapping) oldMapping;
                            deleteOperationMappingContainerView(oldOperationMapping);
                        }
                        break;
                }
            }
        }
    }
}
