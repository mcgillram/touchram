package ca.mcgill.sel.ram.ui.views.handler;

import org.mt4j.components.MTComponent;
import org.mt4j.components.PickResult;
import org.mt4j.components.PickResult.PickEntry;
import org.mt4j.input.inputProcessors.IGestureEventListener;
import org.mt4j.input.inputProcessors.MTGestureEvent;
import org.mt4j.input.inputProcessors.componentProcessors.dragProcessor.DragEvent;
import org.mt4j.input.inputProcessors.componentProcessors.scaleProcessor.ScaleEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;
import org.mt4j.input.inputProcessors.componentProcessors.unistrokeProcessor.UnistrokeEvent;
import org.mt4j.util.MTColor;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamButton;
import ca.mcgill.sel.ram.ui.components.RamLineComponent;
import ca.mcgill.sel.ram.ui.events.WheelEvent;
import ca.mcgill.sel.ram.ui.scenes.handler.IRamAbstractSceneHandler;
import ca.mcgill.sel.ram.ui.views.AbstractView;
import ca.mcgill.sel.ram.ui.views.GenericSplitView;
import ca.mcgill.sel.ram.ui.views.structural.handler.impl.CompositionSplitViewHandler;

/**
 * This is the handler for {@link GenericSplitView} and processes all the gestures captured by the overlay.
 * 
 * This handler is a generic modification from {@link CompositionSplitViewHandler}
 * 
 * @author Bowen
 * 
 * @param <S> type that extends {@link IRamAbstractSceneHandler} for the inner level scene
 * @param <T> type that extends {@link IRamAbstractSceneHandler} for the outer level scene
 * @param <U> type that extends {@link IAbstractViewHandler} for the inner level view
 */
public class GenericSplitViewHandler<S extends IRamAbstractSceneHandler, T extends IAbstractViewHandler, 
    U extends IAbstractViewHandler> implements IGestureEventListener {

    /**
     * Associated generic split view.
     */
    protected GenericSplitView<S, T, U> genericSplitView;

    /**
     * Inner level view of the generic split view.
     */
    protected AbstractView<U> innerLevelView;
    
    /**
     * Outer level view of the generic split view.
     */
    protected AbstractView<T> outerLevelView;

    /**
     * Workaround to prevent receiving a tap and tap and hold event at the same time.
     */
    protected boolean tapAndHoldPerformed;

    /**
     * Creates a new instance of the generic split view.
     * 
     * @param genericSplitView the associated {@link GenericSplitView}
     */
    public GenericSplitViewHandler(GenericSplitView<S, T, U> genericSplitView) {
        this.genericSplitView = genericSplitView;
        this.innerLevelView = genericSplitView.getInnerLevelView();
        this.outerLevelView = genericSplitView.getOuterLevelView();
    }

    /**
     * Handles drag event in the generic split view.
     * 
     * @param dragEvent - The received {@link DragEvent}.
     * 
     * @return whether or not the drag event was handled by the generic split view
     */
    protected boolean handleDragEvent(DragEvent dragEvent) {
        Vector3D pickPoint = dragEvent.getFrom();
        Vector3D lineCenter = genericSplitView.getSplitLine().getCenterPointGlobal();
        Vector3D translationVect = dragEvent.getTranslationVect();

        int width = RamApp.getApplication().getWidth();
        int height = RamApp.getApplication().getHeight();

        // set translation vector to 0 if the translation is out of bounds
        if (genericSplitView.isHorizontal()) {
            if (lineCenter.getY() + translationVect.getY() > height || lineCenter.getY() + translationVect.getY() < 0) {
                translationVect.setY(0);
            }
        } else {
            if (lineCenter.getX() + translationVect.getX() > width || lineCenter.getX() + translationVect.getX() < 0) {
                translationVect.setX(0);
            }
        }

        /**
         * Check if the cursor is within 5 units of the center of the split line
         * (compared by x or y depending on orientation)
         * if it is within 5 units, drag the split line accordingly and set it to yellow then back to black when done
         * return true if it is within 5 units and can be processed here, false if not
         */
        if (genericSplitView.isHorizontal() && Math.abs(pickPoint.getY() - lineCenter.getY()) < 5) {
            switch (dragEvent.getId()) {
                case MTGestureEvent.GESTURE_ENDED:
                    genericSplitView.getSplitLine().setStrokeColor(MTColor.BLACK);

                    break;
                case MTGestureEvent.GESTURE_UPDATED:
                    translationVect.setX(0);

                    genericSplitView.getSplitLine().setStrokeColor(MTColor.YELLOW);
                    genericSplitView.getSplitLine().translate(translationVect);

                    outerLevelView.translateGlobal(translationVect);
                    innerLevelView.translateGlobal(translationVect);

                    break;
            }
            return true;
        } else if (!genericSplitView.isHorizontal() && Math.abs(pickPoint.getX() - lineCenter.getX()) < 5) {
            switch (dragEvent.getId()) {
                case MTGestureEvent.GESTURE_ENDED:
                    genericSplitView.getSplitLine().setStrokeColor(MTColor.BLACK);

                    break;
                case MTGestureEvent.GESTURE_UPDATED:
                    translationVect.setY(0);

                    genericSplitView.getSplitLine().setStrokeColor(MTColor.YELLOW);
                    genericSplitView.getSplitLine().translate(translationVect);

                    outerLevelView.translateGlobal(translationVect);
                    innerLevelView.translateGlobal(translationVect);

                    break;
            }
            return true;
        }
        return false;
    }

    /**
     * Handles the visualization of the unistroke event in the generic split view and passes on the gesture
     * to handleUnistrokeGesture(UnistrokeEvent unistrokeEvent) if the gesture ended.
     * 
     * @param unistrokeEvent - The received {@link UnistrokeEvent}.
     * 
     * @return always true because when this method is called it will be handled in the generic split view
     */
    private boolean handleUniStrokeEvent(UnistrokeEvent unistrokeEvent) {

        switch (unistrokeEvent.getId()) {
            case MTGestureEvent.GESTURE_STARTED:
                genericSplitView.getUnistrokeLayer().addChild(unistrokeEvent.getVisualization());
                break;
            case MTGestureEvent.GESTURE_UPDATED:
                break;
            case MTGestureEvent.GESTURE_ENDED:
                // handle the unistroke gesture appropriately
                Vector3D startPosition = unistrokeEvent.getCursor().getStartPosition();
                Vector3D endPosition = unistrokeEvent.getCursor().getPosition();

                // only handle gesture if start and end positions are different
                // this is a workaround to when tap gestures are also received as unistroke
                if (startPosition.getX() != endPosition.getX() || startPosition.getY() != endPosition.getY()) {
                    handleUnistrokeGesture(unistrokeEvent);
                }
                
                genericSplitView.cleanUnistrokeLayer();
                break;
            case MTGestureEvent.GESTURE_CANCELED:
                genericSplitView.cleanUnistrokeLayer();
                break;
        }

        return true;
    }

    /**
     * Handles the unistroke event in the generic split view by letting the inner or outer level views
     * handle the unistroke event if the start and end positions are in the same view.
     * 
     * @param unistrokeEvent - The received {@link UnistrokeEvent}.
     */
    private void handleUnistrokeGesture(UnistrokeEvent unistrokeEvent) {
        Vector3D startPosition = unistrokeEvent.getCursor().getStartPosition();
        Vector3D endPosition = unistrokeEvent.getCursor().getPosition();

        // pass the event to one of the components at the pick point
        // iff start position and end position are in the same view
        if (isInInnerLevelView(startPosition) == isInInnerLevelView(endPosition)) {
            PickResult pickResults = genericSplitView.getContainerLayer().pick(
                    endPosition.getX(), endPosition.getY(), false);

            for (final PickEntry pick : pickResults.getPickList()) {
                final MTComponent pickComponent = pick.hitObj;
                unistrokeEvent.setTarget(pickComponent);
                if (pickComponent instanceof IHandled) {
                    // see if the component wants to handle the event
                    IHandled<?> handledComponent = (IHandled<?>) pickComponent;

                    if (handledComponent.getHandler() != null && handledComponent
                            .getHandler().processGestureEvent(unistrokeEvent)) {
                        return;
                    }
                }
            }
        }
    }

    /**
     * Returns whether the given position is within the inner level view.
     * 
     * @param position - the position of the cursor
     * 
     * @return whether or not the position lies within the inner level view
     */
    protected boolean isInInnerLevelView(Vector3D position) {
        Vector3D centerOfLine = genericSplitView.getSplitLine().getCenterPointGlobal();

        if (genericSplitView.isHorizontal()) {
            return position.getY() < centerOfLine.getY();
        } else {
            return position.getX() < centerOfLine.getX();
        }
    }

    /**
     * Switches the split orientation from vertical to horizontal or vice versa.
     * Also resets the split line position by removing it then adding a new one.
     */
    public void switchSplitOrientation() {
        Vector3D locationInnerLevel;
        Vector3D locationOuterLevel;

        RamLineComponent splitLine;

        int width = RamApp.getApplication().getWidth();
        int height = RamApp.getApplication().getHeight();

        if (!genericSplitView.isHorizontal()) {
            locationInnerLevel = new Vector3D(0, 0);
            locationOuterLevel = new Vector3D(0, height / 2);

            splitLine = new RamLineComponent(0, height / 2, width, height / 2);
        } else {
            locationInnerLevel = new Vector3D(-1 * width / 3, 0);
            locationOuterLevel = new Vector3D(width / 2, 0);
            splitLine = new RamLineComponent(width / 2, 0, width / 2, height);
        }

        innerLevelView.setPositionGlobal(locationInnerLevel);
        outerLevelView.setPositionGlobal(locationOuterLevel);

        // remove old split line and update it to the rotated centered one.
        genericSplitView.getContainerLayer().removeChild(genericSplitView.getSplitLine());
        genericSplitView.getContainerLayer().addChild(2, splitLine);
        genericSplitView.setSplitLine(splitLine);

        genericSplitView.setHorizontal(!genericSplitView.isHorizontal());
    }

    /**
     * Gestures from the overlay of the generic split view are processed here.
     * This method first checks if the generic split view can handle the gestures itself,
     * if not, it will pass it to one of the underlying views and process it there.
     * 
     * @param gestureEvent - the gesture event passed from the generic split view
     * 
     * @return whether or not the gesture was processed here
     */
    @Override
    public boolean processGestureEvent(MTGestureEvent gestureEvent) {
        boolean dragProcessedBySplitView = false;
        boolean unistrokeProcessedBySplitView = false;

        if (gestureEvent instanceof DragEvent) {
            dragProcessedBySplitView = handleDragEvent((DragEvent) gestureEvent);
        } else if (gestureEvent instanceof UnistrokeEvent) {
            unistrokeProcessedBySplitView = handleUniStrokeEvent((UnistrokeEvent) gestureEvent);
        }

        if (!dragProcessedBySplitView && !unistrokeProcessedBySplitView) {
            Vector3D pickPoint = null;
            boolean stopProcessing = false;

            if (gestureEvent instanceof TapEvent) {
                // prevent a tap event from being processed when a tap and hold event was processed before
                if (tapAndHoldPerformed) {
                    tapAndHoldPerformed = false;
                    stopProcessing = true;
                }

                pickPoint = ((TapEvent) gestureEvent).getLocationOnScreen();
            } else if (gestureEvent instanceof DragEvent) {
                pickPoint = ((DragEvent) gestureEvent).getFrom();
            } else if (gestureEvent instanceof TapAndHoldEvent) {
                TapAndHoldEvent tapAndHoldEvent = (TapAndHoldEvent) gestureEvent;
                // used for workaround to prevent tap event from being executed at the same time
                switch (tapAndHoldEvent.getId()) {
                    case MTGestureEvent.GESTURE_ENDED:
                        if (tapAndHoldEvent.isHoldComplete()) {
                            tapAndHoldPerformed = true;
                        }
                        break;
                }

                pickPoint = tapAndHoldEvent.getLocationOnScreen();
            } else if (gestureEvent instanceof ScaleEvent) {
                pickPoint = ((ScaleEvent) gestureEvent).getScalingPoint();
            } else if (gestureEvent instanceof WheelEvent) {
                pickPoint = ((WheelEvent) gestureEvent).getLocationOnScreen();
            } else if (gestureEvent instanceof UnistrokeEvent) {
                pickPoint = ((UnistrokeEvent) gestureEvent).getCursor().getStartPosition();
                if (pickPoint == null) {
                    stopProcessing = true;
                }
            } else {
                stopProcessing = true;
            }

            if (!stopProcessing) {
                PickResult pickResults = genericSplitView.getContainerLayer().pick(
                        pickPoint.getX(), pickPoint.getY(), false);

                for (final PickEntry pick : pickResults.getPickList()) {

                    final MTComponent pickComponent = pick.hitObj;
                    if (pickComponent != genericSplitView.getOverlay()) {
                        gestureEvent.setTarget(pickComponent);
                        if (pickComponent instanceof IHandled) {
                            // see if the component wants to handle the event
                            IHandled<?> handledComponent = (IHandled<?>) pickComponent;

                            if (handledComponent.getHandler() != null
                                    && handledComponent.getHandler().processGestureEvent(gestureEvent)) {
                                return false;
                            }
                        } else if (pickComponent instanceof RamButton) {
                            if (pickComponent.processGestureEvent(gestureEvent)) {
                                return false;
                            }
                        }
                    }
                }
            }
        }

        return true;
    }
}