package ca.mcgill.sel.ram.ui.views.structural.handler.impl;

import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;

import ca.mcgill.sel.ram.REnum;
import ca.mcgill.sel.ram.REnumLiteral;
import ca.mcgill.sel.ram.controller.ControllerFactory;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.menu.RamLinkedMenu;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.handler.BaseHandler;
import ca.mcgill.sel.ram.ui.views.handler.ILinkedMenuListener;
import ca.mcgill.sel.ram.ui.views.structural.EnumLiteralView;
import ca.mcgill.sel.ram.ui.views.structural.handler.IEnumLiteralViewHandler;

/**
 * The default handler for a {@link EnumLiteralView}.
 * 
 * @author Franz
 */
public class EnumLiteralViewHandler extends BaseHandler implements IEnumLiteralViewHandler, ILinkedMenuListener {

    private static final String ACTION_LITERAL_REMOVE = "view.literal.remove";
    private static final String ACTION_LITERAL_SHIFT_UP = "view.literal.shiftUp";
    private static final String ACTION_LITERAL_SHIFT_DOWN = "view.literal.shiftDown";

    @Override
    public void removeLiteral(EnumLiteralView eLiteralView) {
        ControllerFactory.INSTANCE.getEnumController().removeLiteral(eLiteralView.getLiteral());
    }
    
    public void shiftLiteralUp(EnumLiteralView eLiteralView) {
        EList<REnumLiteral> literals = ((REnum) eLiteralView.getLiteral().eContainer()).getLiterals();
        int index = 0;

        for (int i = 0; i < literals.size(); i++) {
            if (literals.get(i).equals(eLiteralView.getLiteral())) {
                index = i;
            }
        }

        if (index > 0) {
            ControllerFactory.INSTANCE.getEnumController().setLiteralPosition(eLiteralView.getLiteral(), index - 1);
        }
    }

    public void shiftLiteralDown(EnumLiteralView eLiteralView) {
        EList<REnumLiteral> literals = ((REnum) eLiteralView.getLiteral().eContainer()).getLiterals();
        int index = 0;

        for (int i = 0; i < literals.size(); i++) {
            if (literals.get(i).equals(eLiteralView.getLiteral())) {
                index = i;
            }
        }

        if (index < literals.size() - 1) {
            ControllerFactory.INSTANCE.getEnumController().setLiteralPosition(eLiteralView.getLiteral(), index + 1);
        }
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        String actionCommand = event.getActionCommand();

        RamRectangleComponent pressedButton = (RamRectangleComponent) event.getTarget();
        RamLinkedMenu linkedMenu = (RamLinkedMenu) pressedButton.getParentOfType(RamLinkedMenu.class);
        if (linkedMenu != null) {

            EnumLiteralView literalView = (EnumLiteralView) linkedMenu.getLinkedView();

            if (ACTION_LITERAL_REMOVE.equals(actionCommand)) {
                removeLiteral(literalView);
            } else if (ACTION_LITERAL_SHIFT_UP.equals(actionCommand)) {
                shiftLiteralUp(literalView);
            } else if (ACTION_LITERAL_SHIFT_DOWN.equals(actionCommand)) {
                shiftLiteralDown(literalView);
            }
        }
    }

    @Override
    public EObject getEobject(RamRectangleComponent rectangle) {
        return ((EnumLiteralView) rectangle).getLiteral();
    }

    @Override
    public void initMenu(RamLinkedMenu menu, TapAndHoldEvent event) {
        menu.addAction(Strings.MENU_DELETE, Icons.ICON_MENU_TRASH, ACTION_LITERAL_REMOVE, this, true);

        EnumLiteralView literalView = (EnumLiteralView) menu.getLinkedView();

        // add the literal shift up and down buttons if there is more than 1
        // literal
        if (((REnum) literalView.getLiteral().eContainer()).getLiterals().size() > 1) {
            menu.addAction(Strings.MENU_LITERAL_SHIFT_UP, Icons.ICON_MENU_ELEMENT_SHIFT_UP, ACTION_LITERAL_SHIFT_UP,
                    this, true);
            menu.addAction(Strings.MENU_LITERAL_SHIFT_DOWN, Icons.ICON_MENU_ELEMENT_SHIFT_DOWN,
                    ACTION_LITERAL_SHIFT_DOWN, this, true);
        }

        menu.setLinkInCorners(false);
    }

    @Override
    public List<EObject> getEObjectToListenForUpdateMenu(RamRectangleComponent rectangle) {
        return null;
    }

    @Override
    public void updateMenu(RamLinkedMenu menu, Notification notification) {

    }

    @Override
    public RamRectangleComponent getVisualLinkedComponent(RamRectangleComponent link) {
        return link;
    }

}
