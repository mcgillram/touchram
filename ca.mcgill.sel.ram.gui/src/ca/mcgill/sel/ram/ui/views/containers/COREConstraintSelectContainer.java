package ca.mcgill.sel.ram.ui.views.containers;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.SelectionsSingleton;
import ca.mcgill.sel.ram.ui.views.feature.FeatureDiagramSelectView;
import ca.mcgill.sel.ram.ui.views.feature.helpers.Constraint;
import ca.mcgill.sel.ram.ui.views.feature.helpers.SelectionFeature;

/**
 * {@link COREConstraintContainer} containing the constraints on a feature model.
 * 
 * @author CCcamillieri
 */
public class COREConstraintSelectContainer extends COREConstraintContainer {

    /**
     * The container takes in the root feature of the model It displays all the constraint on the rootFeature and its
     * children.
     * 
     * @param hStick - The horizontalStick for the Panel
     * @param vStick - The verticalStick for the Panel
     */
    public COREConstraintSelectContainer(HorizontalStick hStick, VerticalStick vStick) {
        super(hStick, vStick, false);
    }

    /**
     * Puts back the constraints to their default background color.
     */
    private void resetColors() {
        for (RamRectangleComponent rectangelDisplay : list.getElementMap().values()) {
            rectangelDisplay.setFillColor(Colors.DEFAULT_ELEMENT_FILL_COLOR);
        }
    }

    /**
     * Checks if any constraints are violated in the selectedFeatures and color them red if so.
     * 
     * @param featureDiagramView - The diagram that constains selections.
     * @return true if no constraint is violated, false otherwise
     */
    public boolean validate(FeatureDiagramSelectView featureDiagramView) {
        boolean returnValue = true;

        // reset the color in the constraints list
        resetColors();
        
        // Used to avoid requesting the same features several times.
        Map<SelectionFeature, Set<COREFeature>> reexposed = new HashMap<SelectionFeature, Set<COREFeature>>();
        Map<SelectionFeature, Set<COREFeature>> selected = new HashMap<SelectionFeature, Set<COREFeature>>();

        Set<Constraint> constraintSet = list.getElementMap().keySet();
        // Loop through the constraint set
        for (Constraint constraint : constraintSet) {
            // We could also use target feature. Both come from the same models and reuses.
            SelectionFeature feature = constraint.getOwnerSelectionFeature();
            
            // Get set of reexposed and selected features for the reuse of this constraint.
            Set<COREFeature> selectedFeatures = selected.get(feature);
            Set<COREFeature> reExposedFeatures = reexposed.get(feature);
            if (selectedFeatures == null) {
                selectedFeatures = SelectionsSingleton.getInstance().collectSelected(feature);
                selected.put(feature, selectedFeatures);
                reExposedFeatures = SelectionsSingleton.getInstance().collectReexposed(feature);
                reexposed.put(feature, reExposedFeatures);
            }
            
            // Check if constraint is violated.
            if (!constraint.isValid(selectedFeatures, reExposedFeatures)) {
                list.getElementMap().get(constraint).setFillColor(Colors.FEATURE_SELECTION_CLASH_COLOR);
                returnValue = false;
            }
        }

        return returnValue;
    }

}
