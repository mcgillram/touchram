package ca.mcgill.sel.ram.ui.views.handler;

import ca.mcgill.sel.ram.ui.events.listeners.ITapAndHoldListener;

/**
 * This interface is implemented by something that can handle events for a {@link ca.mcgill.sel.ram.ui.views.TextView}
 * representing the name of an {@link a.mcgill.sel.core.COREModelComposition}.
 * 
 * @author eyildirim
 */
public interface ICompositionNameHandler extends ITextViewHandler, ITapAndHoldListener {
    
}
