package ca.mcgill.sel.ram.ui.views.structural;

import org.eclipse.emf.common.notify.Notification;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.COREMapping;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.ram.ClassifierMapping;
import ca.mcgill.sel.ram.EnumMapping;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.views.CompositionContainerView;
import ca.mcgill.sel.ram.ui.views.CompositionTitleView;
import ca.mcgill.sel.ram.ui.views.CompositionView;
import ca.mcgill.sel.ram.ui.views.handler.HandlerFactory;
import ca.mcgill.sel.ram.ui.views.structural.handler.IRamCompositionViewHandler;

/**
 * This view subclasses CompositionView and provides implementations for all the RAM-specific mappings.  
 * 
 * @author joerg
 */
public class RamCompositionView extends CompositionView<IRamCompositionViewHandler> {
    
    /**
     * The action to add a classifier mapping.
     */
    protected static final String ACTION_CLASSIFIER_MAPPING_ADD = "mapping.classifier.add";

    /**
     * The action to add an enum mapping.
     */
    protected static final String ACTION_ENUM_MAPPING_ADD = "mapping.enum.add";
       
    /**
     * Creates a new composition view.
     * 
     * @param modelComposition the model composition to create a view for
     * @param reuse the reuse the composition is for
     * @param containerView  the composition container view that contains this composition view. 
     */
    public RamCompositionView(COREModelComposition modelComposition, COREReuse reuse, 
                                CompositionContainerView containerView) {
        super(modelComposition, reuse, containerView);
        setHandler(HandlerFactory.INSTANCE.getRamCompositionViewHandler());
    }
    
//    @Override
//    public void actionPerformed(ActionEvent event) {
//        String actionCommand = event.getActionCommand();
//        
//        //TODO: check if this method is still useful
//        if (ACTION_CLASSIFIER_MAPPING_ADD.equals(actionCommand)) {
//            // handler.addClassifierMapping(this);
//        }
//    }
    
    @Override
    public void destroy() {
        super.destroy();
        
        /**
         * The container needs to be destroyed manually if it is currently not displayed.
         */
        if (!detailedViewOn) {
            hideableMappingContainer.destroy();
        }
        
        if (modelComposition != null) {
            EMFEditUtil.removeListenerFor(modelComposition, this);
        }
    }
    
    @Override
    public IRamCompositionViewHandler getHandler() {
        return handler;
    }
        
    @Override
    public void handleNotification(Notification notification) {
        if (notification.getNotifier() == modelComposition) {
            if (notification.getFeature() == CorePackage.Literals.CORE_MODEL_COMPOSITION__COMPOSITIONS) {
                switch (notification.getEventType()) {
                    case Notification.ADD:
                        addMappingContainerView((COREMapping<?>) notification.getNewValue());
                        break;
                    case Notification.REMOVE:
                        deleteMappingContainerView((COREMapping<?>) notification.getOldValue());
                        break;
                }
            }
        }
    }
    
    @Override
    public void setHandler(IRamCompositionViewHandler handler) {
        this.handler = handler;
        
    }
    
    @Override
    public CompositionTitleView getCompositionTitleView(CompositionContainerView container,
            CompositionView<IRamCompositionViewHandler> compositionView, COREReuse reuse, boolean detailedViewOn) {
        return new RamCompositionTitleView(container, compositionView, reuse, detailedViewOn);
    }

    @Override
    public RamRectangleComponent getMappingContainerView(COREMapping<?> newMapping) {
        RamRectangleComponent mappingContainerView;
        if (newMapping instanceof ClassifierMapping) {
            mappingContainerView = new ClassifierMappingContainerView((ClassifierMapping) newMapping);
        } else {
            mappingContainerView = new EnumMappingContainerView((EnumMapping) newMapping);
        }
        return mappingContainerView;
    }
    
}
