package ca.mcgill.sel.ram.ui.views;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREModelElementMapping;

/**
 * Implementation of {@link CoreModelElementMappingsValidator} for {@link DomainUsecaseSplitViewWithMappings}.
 * TODO: this class is empty and not yet implemented.
 * 
 * @author Bowen
 */
public class DomainUsecaseMappingsValidator implements CoreModelElementMappingsValidator {

    public DomainUsecaseMappingsValidator() {
        
    }
    
    @Override
    public boolean validateNewMapping(EObject firstModelElement, EObject secondModelElement, 
            List<COREModelElementMapping> mappings, COREConcern concern) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public List<COREModelElementMapping> validateMappingsForEditing(List<COREModelElementMapping> mappings, 
            COREConcern concern) {
        // TODO Auto-generated method stub
        return new ArrayList<>();
    }

    @Override
    public boolean validateMappingsForTransformation(List<COREModelElementMapping> mappings, COREConcern concern) {
        // TODO Auto-generated method stub
        return false;
    }

}
