package ca.mcgill.sel.ram.ui.views.handler;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.mt4j.components.MTComponent;
import org.mt4j.components.PickResult;
import org.mt4j.components.PickResult.PickEntry;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.core.COREModelElementMapping;
import ca.mcgill.sel.ram.Operation;
import ca.mcgill.sel.ram.Parameter;
import ca.mcgill.sel.ram.Type;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.scenes.handler.IRamAbstractSceneHandler;
import ca.mcgill.sel.ram.ui.views.CoreModelElementMappingsValidator;
import ca.mcgill.sel.ram.ui.views.GenericSplitViewWithMappings;
import ca.mcgill.sel.ram.ui.views.RifRamSplitViewWithMappings;
import ca.mcgill.sel.ram.ui.views.structural.OperationView;
import ca.mcgill.sel.ram.ui.views.structural.ParameterView;
import ca.mcgill.sel.ram.ui.views.structural.StructuralDiagramView;
import ca.mcgill.sel.restif.AccessMethod;
import ca.mcgill.sel.restif.DynamicFragment;
import ca.mcgill.sel.restif.PathFragment;
import ca.mcgill.sel.restif.Resource;
import ca.mcgill.sel.restif.ui.components.ResourceRamButton;
import ca.mcgill.sel.restif.ui.views.PathFragmentView;
import ca.mcgill.sel.restif.ui.views.RestTreeView;

/**
 * This is an implementation of {@link GenericSplitViewWithMappingsHandler} that handles a Ram and Rif model in the 
 * split view.
 * 
 * @author Bowen
 * 
 * @param <S> type that extends {@link IRamAbstractSceneHandler} for the inner level scene
 * @param <T> type that extends {@link IAbstractViewHandler} for the outer level view
 * @param <U> type that extends {@link IAbstractViewHandler} for the inner level view
 */
public class RifRamSplitViewWithMappingsHandler<S extends IRamAbstractSceneHandler, T extends IAbstractViewHandler,
    U extends IAbstractViewHandler> extends GenericSplitViewWithMappingsHandler<S, T, U> {
    
    public RifRamSplitViewWithMappingsHandler(GenericSplitViewWithMappings<S, T, U> genericSplitViewWithMappings, 
            CoreModelElementMappingsValidator mappingsValidator) {
        super(genericSplitViewWithMappings, mappingsValidator);
    }

    /**
     * Get a view from a given point. Only allowed model elements for mappings are {@link Operation}, 
     * {@link Parameter}, {@link AcessMethod}, and {@link PathFragment}.
     * 
     * @param position the given {@link Vector3D} point
     * 
     * @return the associated {@link MTComponent}
     */
    @Override
    protected MTComponent getViewFromPosition(Vector3D position) {
        boolean isInInnerLevel = isInInnerLevelView(position);

        // get all possible views from the position
        PickResult pickResults = genericSplitView.getContainerLayer().pick(
                position.getX(), position.getY(), false);

        for (final PickEntry pick : pickResults.getPickList()) {
            final MTComponent pickComponent = pick.hitObj;

            if (isInInnerLevel) {
                // RAM model
                if (genericSplitView.getInnerLevelView() instanceof StructuralDiagramView) {
                    if (pickComponent instanceof ParameterView || pickComponent instanceof OperationView) {
                        return pickComponent;
                    }
                    // RestIF model
                } else if (genericSplitView.getInnerLevelView() instanceof RestTreeView) {
                    if (pickComponent instanceof ResourceRamButton || pickComponent instanceof PathFragmentView) {
                        return pickComponent;
                    }
                }

            } else {
                // RAM model
                if (genericSplitView.getOuterLevelView() instanceof StructuralDiagramView) {
                    if (pickComponent instanceof ParameterView || pickComponent instanceof OperationView) {
                        return pickComponent;
                    }
                    // RestIF model
                } else if (genericSplitView.getOuterLevelView() instanceof RestTreeView) {
                    if (pickComponent instanceof ResourceRamButton || pickComponent instanceof PathFragmentView) {
                        return pickComponent;
                    }
                }

            }
        }

        return null;
    }

    /**
     * Get the model element from a given view. Only allowed model elements for mappings are {@link Operation}, 
     * {@link Parameter}, {@link AcessMethod}, and {@link PathFragment}.
     * 
     * @param view the given {@link MTComponent}
     * 
     * @return the associated {@link EOBject} model element
     */
    @Override
    protected EObject getModelElementFromView(MTComponent view) {
        if (view instanceof ParameterView) {
            return ((ParameterView) view).getParameter();
        } else if (view instanceof OperationView) {
            return ((OperationView) view).getOperation();
        } else if (view instanceof ResourceRamButton) {
            return ((ResourceRamButton) view).getAccessMethod();
        } else if (view instanceof PathFragmentView) {
            return ((PathFragmentView) view).getPathFragment();
        }
        
        return null;
    }

    /**
     * Check whether a view is the outmost view.
     * 
     * @param pickComponent the given {@link MTComponent}
     * 
     * @return whether or not the given view is the outmost view
     */
    @Override
    protected boolean isOutmostView(MTComponent pickComponent) {
        if (pickComponent instanceof RestTreeView || pickComponent instanceof StructuralDiagramView) {
            return true;
        }

        return false;
    }
}