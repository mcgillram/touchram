package ca.mcgill.sel.ram.ui.utils;

import java.util.concurrent.CountDownLatch;

import javax.management.RuntimeErrorException;

import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.language.weaver.util.WeaverListener;

/**
 * Headless weaver runner implementation that works entirely without any GUI
 * assumptions. Can be used in any context where weaving is triggered
 * programmatically. This implementation has an internal latch that blocks the
 * "getWeaverResult" method (to be called after the weaving start) until the
 * weaving is finished. This renders weaving a thread safe operation.
 * 
 * @author Maximilian Schiedermeier
 *
 */
public class HeadlessWeaverRunner implements WeaverListener {

    // Internal latch that can is used to unblock getWeaverResult method.
    private CountDownLatch awaitWeaverLatch;

    // Local variable to pass weaver result from "weavingFinished" notification
    // to blocking "getWeaverResult" method.
    private COREExternalArtefact weaverResult;

    /**
     * Just a default constructor.
     */
    public HeadlessWeaverRunner() {
        awaitWeaverLatch = new CountDownLatch(1);
    }

    @Override
    public void weavingStarted() {
        System.out.println("Uhaa uhaa, the weaving has started.");

    }

    @Override
    public void weavingFinished(COREExternalArtefact artefact, EObject result) {
        weaverResult = artefact;
        awaitWeaverLatch.countDown();
    }

    @Override
    public void weavingFailed(Exception exception) {
        System.out.println("Boohoo boohoo, the weaving has failed.");

    }

    public COREExternalArtefact awaitWeaverResult() {
        
        // Print log message in case weaver has not yet produced a result.
        if (awaitWeaverLatch.getCount() > 0) {
            System.out.println("Awaiting weaver result. This might take a while...");
        }          
        
        // Await weaver success
        try {
            awaitWeaverLatch.await();
        } catch (InterruptedException e) {
            throw new RuntimeException("Failed to await weaver success. The thread has been interrupted.");
        }
        System.out.println("Weaver finished weaving.");
        
        // Verify the weaver actually produced something
        if (weaverResult == null) {
            throw new RuntimeException("Weaver result requested, but the weaver did not produce a result.");
        }
        
        // Return weaver result
        return weaverResult;
    }
}
