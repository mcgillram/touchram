package ca.mcgill.sel.ram.ui.layouts;

import org.mt4j.components.MTComponent;
import org.mt4j.components.TransformSpace;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;

/**
 * The default layout class.
 * @author gemini
 *
 */
public class DefaultLayout extends AbstractBaseLayout {

    private float maxHeight;
    private float maxWidth;

    /**
     * The default constructor.
     */
    public DefaultLayout() {
        this.maxHeight = 0;
        this.maxWidth = 0;
    }

    /**
     * A constructor that sets the maximum width and height.
     * @param maxWidth the width
     * @param maxHeight the height
     */
    public DefaultLayout(float maxWidth, float maxHeight) {
        this.maxWidth = maxWidth;
        this.maxHeight = maxHeight;
    }

    @Override
    public void layout(RamRectangleComponent component, LayoutUpdatePhase updatePhase) {

        float maxChildHeight = 0;
        float maxChildWidth = 0;

        for (MTComponent child : component.getChildren()) {
            if (!(child instanceof RamRectangleComponent)) {
                continue;
            }

            RamRectangleComponent rectangle = (RamRectangleComponent) child;
            Vector3D childPosition = rectangle.getPosition(TransformSpace.RELATIVE_TO_PARENT);

            maxChildHeight =
                    Math.max(maxChildHeight, childPosition.y
                            + rectangle.getHeightXY(TransformSpace.RELATIVE_TO_PARENT));
            maxChildWidth =
                    Math.max(maxChildWidth, childPosition.x
                            + rectangle.getWidthXY(TransformSpace.RELATIVE_TO_PARENT));

        }

        float width;
        float height;
        if (maxWidth > 0) {
            if (maxChildWidth < maxWidth) {
                width = maxChildWidth;
            } else {
                width = maxWidth;
            }
        } else {
            width = maxChildWidth;
        }

        if (maxHeight > 0) {
            if (maxChildHeight < maxHeight) {
                height = maxChildHeight;
            } else {
                height = maxHeight;
            }
        } else {
            height = maxChildHeight;
        }

        setMinimumSize(component, width, height);
    }
}
