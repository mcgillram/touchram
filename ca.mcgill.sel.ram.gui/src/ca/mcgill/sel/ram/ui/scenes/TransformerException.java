package ca.mcgill.sel.ram.ui.scenes;

/**
 * Custom Exception to report errors that occur in the RIF-RAM transformer.
 * Should be placed to a more appropriate package, once we have a dedicated
 * location for perspective logic.
 * 
 * @author Maximilian Schiedermeier
 *
 */
@SuppressWarnings("serial")
public class TransformerException extends RuntimeException {
    public TransformerException(String cause) {
        super(cause);
    }
}