package ca.mcgill.sel.ram.ui.scenes.handler.impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * This class is used for the {@link weaveTwoXmlFiles} method to weave two pom xml files into one and 
 * output it.
 * 
 * @author Bowen
 */
public final class PomXmlWeaver {
    
    /**
     * Empty private constructor as this class only contains static methods.
     */
    private PomXmlWeaver() {
    }
    
    /**
     * This method takes in file locations from 2 input xml files, and a third file location for the weaved xml file.
     * This method parses the two input xml files into {@link Document} objects, and creates a new {@link Document} for
     * the weaved document.
     * 
     * @param fileLocationOne - file location of first xml file
     * @param fileLocationTwo - file location of second xml file
     * @param weavedFileLocatoin - file location of desired output weaved xml file
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SAXException
     * @throws TransformerException
     */
    public static void weaveTwoXmlFiles(String fileLocationOne, String fileLocationTwo, String weavedFileLocatoin) 
            throws ParserConfigurationException, IOException, SAXException, TransformerException {
        XmlIOUtils xmlIOUtils = XmlIOUtils.getInstance();
        
        // extract Document objects from input xml files
        Document documentOne = xmlIOUtils.parseXmlFromDiskToDocument(new File(fileLocationOne));
        Document documentTwo = xmlIOUtils.parseXmlFromDiskToDocument(new File(fileLocationTwo));
                
        // create empty document for weaved document
        Document weavedDocument = xmlIOUtils.createEmptyDocument();

        // get root nodes of document one and two
        Node rootNodeOne = documentOne.getDocumentElement();        
        Node rootNodeTwo = documentTwo.getDocumentElement();
        
        // get children nodes from root nodes
        NodeList childrenNodesOne = rootNodeOne.getChildNodes();
        NodeList childrenNodesTwo = rootNodeTwo.getChildNodes();
        
        // used to store direct children nodes from root nodes one and two
        ArrayList<Node> elementNodesOne = new ArrayList<>();
        ArrayList<Node> elementNodesTwo = new ArrayList<>();
        
        // add all direct element node children from root node one to elementNodesOne
        for (int i = 0; i < childrenNodesOne.getLength(); i++) {
            Node node = childrenNodesOne.item(i);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Node importedNode = weavedDocument.importNode(node, true);
                elementNodesOne.add(importedNode);
            }
        }
        
        // add all direct element node children from root node two to elementNodesTwo
        for (int i = 0; i < childrenNodesTwo.getLength(); i++) {
            Node node = childrenNodesTwo.item(i);
            
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Node importedNode = weavedDocument.importNode(node, true);
                elementNodesTwo.add(importedNode);
            }            
        }
        
        // sort element nodes one and two based on xml tag priority
        ArrayList<Node> sortedElementNodesOne = PomXmlMergeSort.mergeSort(elementNodesOne);
        ArrayList<Node> sortedElementNodesTwo = PomXmlMergeSort.mergeSort(elementNodesTwo);
        
        // sort and merge the two element nodes one and two into one list 
        ArrayList<Node> sortedElements = PomXmlMergeSort.merge(sortedElementNodesOne, sortedElementNodesTwo, true);
        
        // create template root maven node
        // currently using 4.0.0 POM version
        Node weavedDocumentRootNode = createRootMavenNode(weavedDocument);
        
        // error check of input root node attributes
        // get root nodes of document one and two to compare the attributes with the weaved document root node
        Node importRootOne = weavedDocument.importNode(rootNodeOne, false);
        Node importRootTwo = weavedDocument.importNode(rootNodeTwo, false);
        if (!PomXmlMergeSort.checkNodeEquality(weavedDocumentRootNode, importRootOne) 
                || !PomXmlMergeSort.checkNodeEquality(weavedDocumentRootNode, importRootTwo)) {
            System.out.println("WARNING: input root node attributes differ from pom-4.0.0.xml.");
            System.out.println("Weaved document root nodes attributes set to pom-4.0.0.xml.");
        }
        
        // add nodes in sorted elements to weaved document root node
        for (Node node : sortedElements) {
            weavedDocumentRootNode.appendChild(node);
        }
        
        // add weaved document root node to weaved document
        weavedDocument.appendChild(weavedDocumentRootNode);
        
        // output weaved document to disk
        xmlIOUtils.writeXmlDocumentToDisk(weavedDocument, weavedFileLocatoin);
    }

    /**
     * This method creates a new maven root {@link Node} for the input document with the 4.0.0 POM version.
     * 
     * @param document - input document
     * @return - created maven root node
     */
    private static Node createRootMavenNode(Document document) {
        Element root = document.createElement("project");
        
        root.setAttribute("xmlns", "http://maven.apache.org/POM/4.0.0");
        root.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        root.setAttribute("xsi:schemaLocation", "http://maven.apache.org/POM/4.0.0 "
                + "http://maven.apache.org/xsd/maven-4.0.0.xsd");
        
        return root;
    }
}
