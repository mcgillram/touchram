package ca.mcgill.sel.ram.ui.scenes;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;
import org.xml.sax.SAXException;

import ca.mcgill.sel.restif.AccessMethod;
import ca.mcgill.sel.restif.DynamicFragment;
import ca.mcgill.sel.restif.impl.AccessMethodImpl;
import ca.mcgill.sel.restif.impl.DynamicFragmentImpl;
import ca.mcgill.sel.ram.impl.AspectImpl;
import ca.mcgill.sel.ram.impl.OperationImpl;
import ca.mcgill.sel.ram.impl.ParameterImpl;
import ca.mcgill.sel.ram.impl.RamFactoryImpl;
import ca.mcgill.sel.ram.ui.components.navigationbar.NavigationBar;
import ca.mcgill.sel.ram.ui.scenes.handler.impl.MavenGeneratorHandlerTools;
import ca.mcgill.sel.ram.ui.utils.HeadlessWeaverRunner;
import ca.mcgill.sel.ram.ui.views.RifRamMappingsValidator;
import ca.mcgill.sel.commons.UniversalFileLoader;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREConfiguration;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.COREModelElementMapping;
import ca.mcgill.sel.core.COREModelExtension;
import ca.mcgill.sel.core.COREModelReuse;
import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.core.COREReuseArtefact;
import ca.mcgill.sel.core.COREScene;
import ca.mcgill.sel.core.impl.COREConcernImpl;
import ca.mcgill.sel.core.impl.CoreFactoryImpl;
import ca.mcgill.sel.core.language.weaver.COREWeaver;
import ca.mcgill.sel.ram.ArtifactSignature;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.Operation;
import ca.mcgill.sel.ram.RAMVisibilityType;
import ca.mcgill.sel.ram.generator.markdown.ReadmeGenerator;
import ca.mcgill.sel.ram.util.RAMModelUtil;

/**
 * Helper class for all RIF/RAM-Trafo related operations. Eventually this class
 * should be moved to a location outside of the GUI package.
 * 
 * @author Maximilian Schiedermeier
 *
 */
public final class TrafoUtils {

    /** Key used to identify / extract Business Logic (FR-CDM) of a concern. */
    public static final String BL_KEY = "Business Logic";

    /** Key used to identify / extract RestInterface (RIFs) of a concern. */
    public static final String RIF_KEY = "Exposed Interface";

    /** Key used to identify / extract RestInterface (RIFs) of a concern. */
    public static final String FPCDM_KEY = "FPCDM";

    /**
     * Key used for the model internal subfolder containing generated maven code.
     */
    public static final String CODEGEN_DIR = "generated-maven-project";

    private TrafoUtils() {
    }

    /**
     * Converts a CORE-Concern based on a RIF-RAM Perspective into a FP-CDM
     * model, required for weaving.
     * 
     * @param concernWithRifRamMapping as input model is a CORE concern that
     * internally contains: A RAM model (business logic), A RIF model (exposing
     * interface. A scene with a LEM (languageElement-based mapping) that links
     * individual RAM to RIF elements.
     * @param selectedRestifyImplementation the Restify implementation the user
     * has selected as a core reuse.
     * @return the FPCDM created throughout the transformation process.
     * 
     * The transformation runs in two stages: 1) Creation of a stripped copy of
     * the original RAM as FP-CDM. 2) Decoration of the FP-CDM with the required
     * REST-annotations.
     * 
     * TODO: Add enum parameter that encodes selected REST technology.
     * 
     */
    public static COREExternalArtefact transform(COREConcernImpl concernWithRifRamMapping,
            RestifyImplementation selectedRestifyImplementation) {

        // Step 1) create additional RAM model (FP) and place it in the
        // encompassing concern.
        // After this step, the FP-CDM contains only stubs of the rest-relevant
        // methods, that are mapped to the (extended) original RAM by internal
        // CoreMappings. No annotations are yet present.
        COREExternalArtefact fpcdm = createFpCdm(concernWithRifRamMapping);


        // Step 2) Analyze the original RIF-RAM mappings and decorate the FP-CDM
        // classes, method signatures and parameters with equivalent REST
        // annotations.

        // annotate with spring boot or jax-rs based on selected Restify implementation
        if (selectedRestifyImplementation == RestifyImplementation.SPRING_BOOT) {
            annotateFpCdmSpringBoot(concernWithRifRamMapping, fpcdm);
        } else {
            annotateFpCdmJaxRS(concernWithRifRamMapping, fpcdm);
        }

        // Step 3) Link generated FPCDM controller endpoints to business logic
        // with sequence diagrams that delegate incoming calls. Also link novel
        // launcher class to spring framework (or any other selected REST
        // technology)

        // commented out since we are using generating the launcher class as a static class with Acceleo now
        //        new SpringBootTrafoDelegationUtils().linkLauncherToRestFramework(fpcdm);

        new SpringBootTrafoDelegationUtils().linkControllersToBusinessLogic(
                concernWithRifRamMapping, fpcdm);

        return fpcdm;
    }

    /**
     * Interprets the language element mappings of a provided core concern (must
     * include a RIF/RAM mapping) and creates functionality poor cdm that
     * imitates the exposed (mapped to a RIF element) structure of the concern's
     * ram model (functionality rich cdm). Also adds boilerplate code like
     * singleton access methods and a spring launcher class. Note that the
     * generated fpcdm only extends the original frcdm (is to say extends that
     * aspect), but does not yet contain any rest specific java annotations or
     * delegation to target legacy methods.
     * 
     * @param concern as the concern that houses the frcdm. The newly created
     * functionality poor CDM (frcdm) will be added to this concern as an
     * additional Core-ExternalArtefact.
     * 
     * @return a new CoreExternalArtefact that houses the newly created fpcdm
     * aspect (no annotations included).
     */
    public static COREExternalArtefact createFpCdm(COREConcernImpl concern) {

        // Verify the provided concern has a RIF, a RAM, a LEM mapping.
        if (!isConcernWithRifRamMapping(concern)) {
            throw new TransformerException("Functionality poor cdm can not be created for this concern. "
                    + "At least one of the required input models (Rif, Ram, Mapping) is not present.");
        }

        // Create a new RAM for the FP-CDM and add it to the same concern within
        // a dedicated COREExternalArtefact. Embed it in provided concern,
        // alongside existing artifacts.
        COREExternalArtefact fpcdmArtefact = prepareEmptyAspectInConcern(concern, FPCDM_KEY);

        // Verify the classifiers of all mapped frcdm operations provide a
        // getInstance method.
        Set<Classifier> originalTargetClassifiers = getTargetClassifiers(concern);
        verifySingletonAccess(originalTargetClassifiers);

        // Clone all signatures of FR-CDM that are referenced by LEMs into the
        // FP-CDM. (ignores the parameter-mappings)
        // This also takes care of imports / maven signatures in case some of
        // the classifiers are implementation classes.
        // The newly added fpcdm classifiers are added as implementation classes! (I think...)
        cloneAllMappedSignaturesAndTheirClassifiers(concern, fpcdmArtefact);

        // Re-clone all classifiers (including operations and parameters) so
        // they appear in the FPCDM as future controllers (eventually annotated proxy classes).
        AbstractFunctionalityPoorCdmFillUtil fpcdmFillUtil = new SpringBootFunctionalityPoorCdmFillUtil();
        fpcdmFillUtil.addControllers(fpcdmArtefact, originalTargetClassifiers);

        // Clone and map singleton access methods (getInstance) into all
        // non-controllers
        fpcdmFillUtil.addAndMapClassifierSingletonAccessMethods(fpcdmArtefact, originalTargetClassifiers);

        // Add generic launcher class and delegation to the implementation
        // classes it depends on.

        // commented out since we are using generating the launcher class as a static class with Acceleo now
        //		Classifier launcher = fpcdmFillUtil.addRestApplicationLauncherClass(fpcdmArtefact);

        // Add generic maven signature information for the restified model
        // itself.	
        // commented out since we are using generating the launcher class as a static class with Acceleo now
        //AbstractFunctionalityPoorCdmFillUtil.addMavenSignature(fpcdmArtefact, launcher, extractFrcdm(concern).getStructuralView().getSignature());
        AbstractFunctionalityPoorCdmFillUtil.addMavenSignature(fpcdmArtefact, null, extractFrcdm(concern).getStructuralView().getSignature());

        // Add the maven signature information of the ram model of the concern as a maven dependency to the fpcdm.
        addArtifactSignatureOfConcernToArtefact(concern, fpcdmArtefact);

        return fpcdmArtefact;
    }

    /**
     * Helper method to extract the original frcdm ram model of a mapped RIF-RAM model.
     */
    private static Aspect extractFrcdm(COREConcern concern) {

        // 1) The new maven signature is extracted from the existing original core-contained ram model (FRCDM)
        // extract ram model with key
        COREExternalArtefact ramArtefact = extractCoreArtefactByKey(concern, BL_KEY);
        // extract aspect of ram model
        return (AspectImpl) ramArtefact.getRootModelElement();
    }

    /**
     * Extracts the maven artifact signature of the input concern and adds it as a dependency to the input artefact.
     * 
     * @param concern as the concern that houses the frcdm.
     * @param fpcdmArtefact as the fpcdm the maven dependency is added to.
     */
    public static void addArtifactSignatureOfConcernToArtefact(COREConcernImpl concern, 
            COREExternalArtefact fpcdmArtefact) {

        // Extract frcdm so we have access to the original dependencies
        // (For now only the "*internals" / base application signature)
        Aspect ramAspect = extractFrcdm(concern);

        // extract target aspect of fpcdm artefact (dependencies are added here)
        Aspect fpcdmAspect = (AspectImpl) fpcdmArtefact.getRootModelElement();
        // add the maven signature information of the ram aspect to the fpcdm aspect
        EList<ArtifactSignature> targetDependencies = fpcdmAspect.getStructuralView().getDependencies();

        // Clone all frcdm dependencies to fpcdm
        for (ArtifactSignature dependency : ramAspect.getStructuralView().getDependencies()) {
            targetDependencies.add(cloneDependency(dependency));
        }


    }

    /**
     * Helper method to create a deep clone of a (e.g. frcdm provided) dependency that can be added to a distinct model without creating inter-model references.
     * @param dependency as the original dependency
     * @return a deep clone of the provided original dependency
     */
    private static ArtifactSignature cloneDependency(ArtifactSignature dependency) {

        ArtifactSignature clone = new RamFactoryImpl().createArtifactSignature();
        clone.setArtifactId(dependency.getArtifactId());
        clone.setGroupId(dependency.getGroupId());
        clone.setVersion(dependency.getVersion());
        clone.setJar(dependency.getJar());
        clone.setName(dependency.getName());
        return clone;
    }

    /**
     * Resolves the frcdm operations of a concern's RIF-RAM mappings to a
     * duplicate free set of frcdm classifiers.
     * 
     * @param concern as a concern with a rif-ram mapping.
     * @return a set of all indirectly referenced classifiers.
     */
    private static Set<Classifier> getTargetClassifiers(COREConcernImpl concern) {

        Set<Classifier> targetClassifiers = new LinkedHashSet<>();

        for (COREModelElementMapping mapping : getRifRamLanguageElementMappings(concern)) {

            // In this method we are only interested on operation mappings
            // (therefore we run a preliminary check that skips other mappings,
            // e.g. parameter mappings.)
            if (isOperationMapping(mapping)) {
                // TODO: Consider that not all mappings are between operations
                // and
                // REST-Methods. There are also parameter mappings!
                OperationImpl ramSignature = getRamOperationMappingEnd(mapping);

                // make sure the container (ram classifier) for that operation
                // is added to the set of unique target classifiers
                if (ramSignature != null) {

                    // ... resolve to the operation's parent classifier
                    targetClassifiers.add((Classifier) ramSignature.eContainer());
                }
            }
        }

        return targetClassifiers;

    }

    /**
     * Iterates over all target operations of RIF-RAM mappings and verifies the
     * housing classifiers provide a singleton "getInstance()" method. A
     * transformerException is thrown if not all resolved classifiers provide
     * this method.
     * 
     * @param concern as the concern housing the RIF-RAM mapping
     */
    private static void verifySingletonAccess(Set<Classifier> classifiers) {

        for (Classifier classifier : classifiers) {
            // ... verify the parent classifier has a "getInstance" method.
            // Calling this check throws a TrafoException if the classifier does
            // not have a
            // getInstance() method.
            obtainSingletonAccessOperation(classifier);
        }
    }

    /**
     * Receives a classifier EObject and verifies it has a public static
     * "getInstance()" method.
     * 
     * @param classifier as the RAM class to be tested for a getInstance method.
     */
    public static Operation obtainSingletonAccessOperation(Classifier classifier) {

        // Return as soon as the matching "getInstance" method is found
        for (Operation operation : classifier.getOperations()) {

            boolean nameMatch = operation.getName().toLowerCase().equals("getinstance");
            boolean isPublic = operation.getVisibility().equals(RAMVisibilityType.PUBLIC);
            boolean isNotAbstract = !operation.isAbstract();
            boolean isStatic = operation.isStatic();

            if (nameMatch && isPublic && isNotAbstract && isStatic) {
                // looks good, the classifier passed the test.
                return operation;
            }
        }

        throw new TransformerException("Classifier " + classifier.getName()
        + " does not provide a public/static getInstance() method. The classifier's "
        + "operations are therefore not eligible for RESTify frcdm target mappings.");
    }

    /**
     * Helper method that iterates over all RIF-RAM mappings and ensures the
     * target frcdm operations are copied into a provided fpcdm, including their
     * encompassing classifiers.
     * 
     * @param concern as the model housing frcdm and fpcdm artifact.
     * @param fpcdmArtefact as the fpcdm the operations (within the newly
     * created classifier copies) must be placed into.
     */
    private static void cloneAllMappedSignaturesAndTheirClassifiers(COREConcernImpl concern,
            COREExternalArtefact fpcdmArtefact) {
        for (COREModelElementMapping mapping : getRifRamLanguageElementMappings(concern)) {

            // if the mapping has EXACTLY one end that matches a RAM
            // signature... (otherwise ignore mapping here, is a parameter
            // mapping.
            if (isOperationMapping(mapping)) {

                OperationImpl ramSignature = getRamOperationMappingEnd(mapping);
                if (ramSignature != null) {

                    // ... clone this signature into the new aspect (FPCDM)
                    cloneSignatureIntoFunctionalityPoorAspect(ramSignature, fpcdmArtefact);
                }
            }
        }
    }

    /**
     * Helper method to create an empty RAM as external core artifact in a core
     * concern. The artifact will ultimately house the functionality poor
     * (annotated) CDM.
     * 
     * @param concern as the concern that must house the newly created aspect
     * @param name as the identifier used for the added core-external-artifact
     * 
     * @return a new coreexternalartefact, containing the aspect. This is the
     * artefact that has been added to the concern.
     */
    private static COREExternalArtefact prepareEmptyAspectInConcern(COREConcernImpl concern, String name) {

        // Create empty RAM
        Aspect fpcdm = RAMModelUtil.createAspect(FPCDM_KEY); // TODO: verify if
        // this
        // correctly
        // sets type
        // name
        // fields.

        // Create empty core-external-artifact that references concern and
        // aspect
        COREExternalArtefact fpcdmArtefact = CoreFactoryImpl.init().createCOREExternalArtefact();
        fpcdmArtefact.setLanguageName("Reusable Aspect Models");
        fpcdmArtefact.setName(name);
        fpcdmArtefact.setRootModelElement(fpcdm);
        fpcdmArtefact.setCoreConcern(concern);

        // Add model extension for future mappings between Fr/Fp-Cdm to the new
        // Artefact. Source is the extended model (BL/FRCDM)
        // The mapping extension carries only a classifier mappings at top
        // level.
        // Operations and parameters are only added later as nested sub
        // elements.
        COREModelExtension mappingExtension = new CoreFactoryImpl().createCOREModelExtension();
        mappingExtension.setSource(extractCoreArtefactByKey(concern, BL_KEY));
        fpcdmArtefact.getModelExtensions().add(mappingExtension);

        // place aspect in concern
        concern.getArtefacts().add(fpcdmArtefact);

        // return aspect
        return fpcdmArtefact;
    }

    /**
     * Clones the signature of a provided RAM operation into a provided
     * different aspect. If required the operation's encompassing class is
     * created, too. (And if it is an implementation class this also clones the
     * requires imports / maven signatures.) Operation parameters are also
     * copied. Does not copy the internals of the provided operation, that is to
     * say the copy is limited to a signature stub without implementation
     * details.
     *
     * @param originalOperation
     * @param classifierMapping as the mapping from fpcdm to original classifier
     * that the operation mapping must be added to.
     * @param fpCdmArtefact as the artefact holding aspect and mapping
     */
    private static void cloneSignatureIntoFunctionalityPoorAspect(OperationImpl originalOperation,
            COREExternalArtefact fpCdmArtefact) {

        // Unless the class containing the references ramSignature is already
        // present (and mapped) by the fpcdmArtefat, add (and map) it now.
        Classifier originalClassifier = (Classifier) originalOperation.eContainer();
        Classifier fpcdmClassifier = AbstractFunctionalityPoorCdmFillUtil.ensureClassifierExists(originalClassifier,
                fpCdmArtefact);

        // Clone the provided operation, add it the the (cloned) target
        // classifier in the fpcdm, map the cloned operation to the original
        // operation.
        // Do the same for all operation parameters.
        AbstractFunctionalityPoorCdmFillUtil.cloneOperation(originalOperation, fpcdmClassifier, originalClassifier,
                fpCdmArtefact, true);

        System.out.println("FPCDM clone complete :)");
    }

    /**
     * Interprets the language element mappings of a provided core concern (must
     * include a fpcdm, a frcdm, LEMs) and inserts the corresponding REST
     * mappings (specifically: spring boot).
     * 
     * @param concern as the concern that houses the required fpcdm, frcdm,
     * LEMs.
     */
    public static void annotateFpCdmSpringBoot(COREConcern concern, COREExternalArtefact fpcdm) {

        // Create static maven artifacts and imports required for annotations.
        AbstractFunctionalityPoorAnnotateUtil annotateUtil = new SpringBootFunctionalityPoorAnnotateUtil();
        annotateUtil.addAnnotationSupport(fpcdm);

        // Add the actual annotations, based on RIF-RAM mappings
        annotateUtil.addAnnotations(fpcdm, concern);
    }

    /**
     * Interprets the language element mappings of a provided core concern (must
     * include a fpcdm, a frcdm, LEMs) and inserts the corresponding REST
     * mappings (specifically: Jax-RS).
     * 
     * @param concern as the concern that houses the required fpcdm, frcdm, LEMs.
     * @param fpcdm input fpcdm RAM model
     */
    public static void annotateFpCdmJaxRS(COREConcern concern, COREExternalArtefact fpcdm) {

        // Create static maven artifacts and imports required for annotations.
        AbstractFunctionalityPoorAnnotateUtil annotateUtil = new JaxRSFunctionalityPoorAnnotateUtil();
        annotateUtil.addAnnotationSupport(fpcdm);

        // Add the actual annotations, based on RIF-RAM mappings
        annotateUtil.addAnnotations(fpcdm, concern);
    }

    public static void weaveAndGenerate(COREExternalArtefact fpcdm, 
            RestifyImplementation selectedRestifyImplementation) {

        // Fiddle out the weaver arguments...
        // Intuitively it should be three things: FPCDM, FRCDM, mappings between
        // those two.
        // However the weaver actually expects following arguments:
        // 1: FPCDM, 2: COREModelExtension holds mapping and internally declares
        // FRCDM as source.
        COREModelExtension mapping = fpcdm.getModelExtensions().get(0);

        // Create a dummy weaver listener. Is a dummy - we only need it to make
        // the weaver happy.
        HeadlessWeaverRunner dummyListener = new HeadlessWeaverRunner();

        // Create a named dummy scene that is referenced by the FPCDM.
        // Is a dummy - we only need it to make the weaver happy.
        COREScene dummyScene = CoreFactoryImpl.init().createCOREScene();
        dummyScene.setName("FPCDM-SCENE");
        dummyScene.setPerspectiveName("FPCDM-PERSPECTIVE");
        fpcdm.setScene(dummyScene);

        // Obtain a weaver, let it fuse FRCDM into FPCDM:
        COREWeaver weaver = COREWeaver.getInstance();
        weaver.weaveSingle(fpcdm, mapping, dummyListener);

        // Obtain woven RAM model - note: this is a blocking operation.
        COREExternalArtefact wovenModel = dummyListener.awaitWeaverResult();
        Aspect wovenAspect = (Aspect) wovenModel.getRootModelElement();

        // Find out where the code generator should save to (subfolder
        // "generated-code" in model folder.
        COREExternalArtefact frcdm = extractCoreArtefactByKey(fpcdm.getCoreConcern(), BL_KEY);
        File modelBaseLocation = getModelBasePath(frcdm.getRootModelElement());

        // Find the name of the directory for the specific Restify implementation.
        String restifyDir = selectedRestifyImplementation.toString().toLowerCase().replace('_', '-');

        // Find the directory hierarchy of the code generation directories.
        String generatedCodeDirHierarchy = CODEGEN_DIR + File.separator + restifyDir;

        File generatedCodeLocation = new File(modelBaseLocation.getAbsolutePath() + File.separator
                + generatedCodeDirHierarchy);

        if (generatedCodeLocation.exists()) {
            UniversalFileLoader.deleteDir(generatedCodeLocation);
        }

        // Note: Annotation weaving is not necessary, because all annotations
        // (and annotation requirements) are already present in base aspect and
        // survive weaving of structural views.
        try {
            MavenGeneratorHandlerTools mavenGeneratorHandlerTools = new MavenGeneratorHandlerTools(wovenAspect, 
                    modelBaseLocation, generatedCodeDirHierarchy);

            mavenGeneratorHandlerTools.generateJavaCode();

            mavenGeneratorHandlerTools.generateGenericPomXml();

            if (selectedRestifyImplementation == RestifyImplementation.SPRING_BOOT) {
                mavenGeneratorHandlerTools.generateSpringBootPomXml();
                mavenGeneratorHandlerTools.generateSpringBootLauncher();

                mavenGeneratorHandlerTools.generateWovenPomXml("GenericPom.xml", "SpringBootPom.xml");
            } else {
                mavenGeneratorHandlerTools.generateJaxRSApplicationClass();

                if (selectedRestifyImplementation == RestifyImplementation.ECLIPSE_JERSEY) {
                    mavenGeneratorHandlerTools.generateJaxRSJerseyEasyPomXml();
                    mavenGeneratorHandlerTools.generateWovenPomXml("GenericPom.xml", "JaxRSJerseyPom.xml");
                } else if (selectedRestifyImplementation == RestifyImplementation.JBOSS_RESTEASY) {
                    mavenGeneratorHandlerTools.generateJaxRSJBossRESTEasyPomXml();
                    mavenGeneratorHandlerTools.generateWovenPomXml("GenericPom.xml", "JaxRSJBossRESTEasyPom.xml");
                } else if (selectedRestifyImplementation == RestifyImplementation.APACHE_CXF) {
                    mavenGeneratorHandlerTools.generateJaxRSApacheCXFPomXml();

                    mavenGeneratorHandlerTools.generateWovenPomXml("GenericPom.xml", "JaxRSApacheCXFPom.xml");
                    mavenGeneratorHandlerTools.generateApacheCXFLauncher();
                }
            }

            //MavenGeneratorHandlerTools.getMavenCodeLocation(wovenAspect, codeGenSaveLocation);
        } catch (IOException | ParserConfigurationException | SAXException
                | javax.xml.transform.TransformerException e) {
            throw new RuntimeException(e);
        }

        // Finally add a README file to the generated code folder:
        File readmeFile = new File(modelBaseLocation.getAbsolutePath() + File.separator
                + generatedCodeDirHierarchy);
        // + File.separator + "README.md");
        // if (readmeFile.exists())
        // readmeFile.delete();
        //
        // try {
        // readmeFile.createNewFile();
        // String content = "# README\n\n## Build Instructions\n\nMvn...";
        // String path = readmeFile.getAbsolutePath();
        // Files.write(Paths.get(path), content.getBytes());
        // } catch (IOException e) {
        // throw new TransformerException(e.getMessage());
        // }
        // Generate an additional "pom.xml" (maven configuration file)
        ReadmeGenerator readmeGenerator;
        try {
            readmeGenerator = new ReadmeGenerator(wovenAspect, readmeFile, new ArrayList<Object>());
            readmeGenerator.doGenerate(null);

        } catch (IOException e) {
            new TransformerException(e.getMessage());
        }
    }

    /**
     * Analyzes a provided concern and returns true if the concern is eligible
     * for a RIF-RAM to RAM-RAM transformation. Returns true if there are
     * exactly: one RIF, one RAM, one scene with LEM mappings. False otherwise.
     * 
     * @param concern as the concern to be analyzed.
     * @return true if all required input models were detected, false otherwise.
     */
    private static boolean isConcernWithRifRamMapping(COREConcern concern) {

        // Verify the concern holds exactly one business logic
        if (extractCoreArtefactByKey(concern, BL_KEY) == null) {
            return false;
        }

        // Verify the concern holds exactly one expose interface
        if (extractCoreArtefactByKey(concern, RIF_KEY) == null) {
            return false;
        }

        // Verify the concern holds LEMs. (does not verify if the referenced
        // elements actually map between RIF and RAM.)
        if (getRifRamLanguageElementMappings(concern).isEmpty()) {
            return false;
        }

        // Looks good, there is a RIF, a RAM and mappings.
        return true;
    }

    /**
     * Helper method: Extracts a specific core-artifact (identified by key) from
     * a concern.
     * 
     * @param key as the string identifier of the artifact (e.g. "Business
     * Logic", "Exposed Interface")
     * @param concern as the concern to be searched for the artefact.
     * @return The contained artifact, if exactly one representative was found.
     * Null otherwise.
     */
    public static COREExternalArtefact extractCoreArtefactByKey(COREConcern concern, String key) {

        // Verify the concern has exactly one scene
        validateConcernSceneAmount(concern, 1);

        // Extract all CoreArtefacts of the provided concern
        EMap<String, EList<COREArtefact>> inputArtefacts = extractCoreArtefactMap(concern);

        // Make sure there is only a single CoreArtefact declared for the
        // provided key
        if (!inputArtefacts.containsKey(key) || inputArtefacts.get(key).size() != 1) {
            // Not a unique key in the provided concern
            return null;
        }

        // Looks good. Return the RAM declared as "Business Logic"
        return (COREExternalArtefact) inputArtefacts.get(key).get(0);
    }

    /**
     * Extract all LEMs defined in the concern's scene.
     * 
     * @param concern
     * @return A list of all detected elements. Can be empty if none were found.
     */
    public static EList<COREModelElementMapping> getRifRamLanguageElementMappings(COREConcern concern) {

        // Verify the concern has exactly one scene
        validateConcernSceneAmount(concern, 1);

        // Extract element mappings and return them.
        return concern.getScenes().get(0).getElementMappings();
    }

    /**
     * Extracts all internal CoreArtefacts of a concern.
     * 
     * @param concern to be analyzed
     * @return Map of COREArtefact-Collections, indexed by identifier-key.
     */
    private static EMap<String, EList<COREArtefact>> extractCoreArtefactMap(COREConcern concern) {
        return concern.getScenes().get(0).getArtefacts();
    }

    /**
     * Throws a transformer exception if the provided concern does not have
     * exactly one scene.
     * 
     * @param concern to be analyzed
     * @param amount of scenes expected
     */
    private static void validateConcernSceneAmount(COREConcern concern, int amount) {
        if (concern.getScenes().size() != amount) {
            throw new TransformerException(
                    "Concern can not be analyzed for core artefacts for it does not possess exactly one scene.");
        }
    }

    /**
     * Analyzes ONE received language element mapping END and verifies if the
     * referenced business-logic element is of type "method signature".
     * 
     * @param end mapping to be analyzed
     * @return true if a FR-CDM signature is found at one end of the mapping
     */
    private static boolean isRamOperationMappingEnd(EObject end) {
        return end.getClass().equals(OperationImpl.class);
    }

    /**
     * Analyzes ONE received language element mapping END and verifies if the
     * referenced business-logic element is of type "Parameter".
     * 
     * @param end
     *            mapping to be analyzed
     * @return true if a FR-CDM signature is found at one end of the mapping
     */
    private static boolean isRamParameterMappingEnd(EObject end) {
        return end.getClass().equals(ParameterImpl.class);
    }

    /**
     * RIF Counterpart to isRamOperationMappingEnd. Analyzes ONE received
     * language element mapping END and verifies if the referenced RIF element
     * is of type "Access Method".
     * 
     * @param end mapping to be analyzed
     * @return true if a FR-CDM signature is found at one end of the mapping
     */
    private static boolean isRestAccessMethodMappingEnd(EObject end) {

        // the actual type is stored as attribute of "AccessMethod". Not
        // relevant here.
        return end.getClass().equals(AccessMethodImpl.class);
    }

    /**
     * RIF Counterpart to isRamOperationMappingEnd. Analyzes ONE received
     * language element mapping END and verifies if the referenced RIF element
     * is of type "Dynamic Fragment".
     * 
     * @param end
     *            mapping to be analyzed
     * @return true if a FR-CDM signature is found at one end of the mapping
     */
    private static boolean isRestDynamicFragmentMappingEnd(EObject end) {

        // the actual type is stored as attribute of "DynamicFragment". Not
        // relevant here.
        return end.getClass().equals(DynamicFragmentImpl.class);
    }

    // TODO: Consider that not all mappings are between operations and
    // REST-Methods. There are also parameter mappings!
    /**
     * Analyzes a provided LangaugeElementMapping and extracts the associated
     * RAM/CDM signature, if there is exactly ONE ending with matching type.
     *
     * @param mapping as the languageElementMapping to be analyzed
     * @return the OperationImpl referenced by the mapping end of matching type.
     * Throws transformer exception if not exactly one of the endings matched
     * type OperationImpl.class
     *
     */
    public static OperationImpl getRamOperationMappingEnd(COREModelElementMapping mapping) {

        // find the index of the mapping end we are interested in (is negative
        // if none matches)
        int pos = getPositionOfRamOperationMapping(mapping);

        // try to extract the ONLY end that is a RAM signature
        if (pos >= 0) {
            EList<EObject> dualMappingEnds = tryToExtractDualMappingEnds(mapping);
            return (OperationImpl) dualMappingEnds.get(pos);
        }

        // reject if none of the ends is a RAM operation
        throw new TransformerException(
                "Can not extract Operation of provided LEM. None of ends matched target type.");
    }

    /**
     * Analyzes a provided LangaugeElementMapping and extracts the associated
     * RAM/CDM signature, if there is exactly ONE ending with matching type.
     *
     * @param mapping
     *            as the languageElementMapping to be analyzed
     * @return the ParameterImpl referenced by the mapping end of matching type.
     *         Throws transformer exception if not exactly one of the endings
     *         matched type OperationImpl.class
     *
     */
    public static ParameterImpl getRamParameterMappingEnd(
            COREModelElementMapping mapping) {

        // find the index of the mapping end we are interested in (is negative
        // if none matches)
        int pos = getPositionOfRamParameterMapping(mapping);

        // try to extract the ONLY end that is a RAM signature
        if (pos >= 0) {
            EList<EObject> dualMappingEnds = tryToExtractDualMappingEnds(
                    mapping);
            return (ParameterImpl) dualMappingEnds.get(pos);
        }

        // reject if none of the ends is a RAM operation
        throw new TransformerException(
                "Can not extract Operation of provided LEM. None of ends matched target type.");
    }

    /**
     * Helper method to determine whether a given core model element mapping
     * links operations. (There can also be parameter mappings).
     * 
     * @param mapping
     *            as there CoreModelElementMapping to inspect for a operation
     *            ending
     * @return true if an operation mapping was found, false otherwise.
     */
    public static boolean isOperationMapping(COREModelElementMapping mapping) {

        // look up if the desired mapping end can be resolved to a valid index.
        return getPositionOfRamOperationMapping(mapping) >= 0;
    }

    /**
     * For internal use only. Can be used by RamOperation extractor
     * (getRamOperationMappingEnd) and RamOperation verifyer
     * (isMappingWithRamOperationEnd).
     * 
     * @param mapping as the RifRam mapping instance to be analyzed
     * 
     * @return Returns the position of the ram operation end, if found. Returns
     * -1 otherwise.
     */
    private static int getPositionOfRamOperationMapping(COREModelElementMapping mapping) {

        // Verify there are exactly two LEM ends.
        EList<EObject> dualMappingEnds = tryToExtractDualMappingEnds(mapping);

        // reject mapping if both ends are RAM operations
        EObject firstEnd = dualMappingEnds.get(0);
        EObject secondEnd = dualMappingEnds.get(1);
        boolean firstEndIsSignature = isRamOperationMappingEnd(firstEnd);
        boolean secondEndIsSignature = isRamOperationMappingEnd(secondEnd);
        if (firstEndIsSignature && secondEndIsSignature) {
            throw new TransformerException("Can not extract Operation of provided LEM. Both ends matched target type.");
        }

        // try to extract the ONLY end that is a RAM signature
        if (firstEndIsSignature) {
            return 0;
        }
        if (secondEndIsSignature) {
            return 1;
        }
        // If none of the ends matched the requested type, return -1, ton
        // indicate that resolving is not possible.
        return -1;
    }

    /**
     * For internal use only. Can be used by RamParameter extractor
     * (getRamParameterMappingEnd) and RamParameter verifyer
     * (isMappingWithRamOperationEnd).
     * 
     * @param mapping
     *            as the RifRam mapping instance to be analyzed
     * 
     * @return Returns the position of the ram parameter end, if found. Returns
     *         -1 otherwise.
     */
    private static int getPositionOfRamParameterMapping(
            COREModelElementMapping mapping) {

        // Verify there are exactly two LEM ends.
        EList<EObject> dualMappingEnds = tryToExtractDualMappingEnds(mapping);

        // reject mapping if both ends are RAM operations
        EObject firstEnd = dualMappingEnds.get(0);
        EObject secondEnd = dualMappingEnds.get(1);
        boolean firstEndIsSignature = isRamParameterMappingEnd(firstEnd);
        boolean secondEndIsSignature = isRamParameterMappingEnd(secondEnd);
        if (firstEndIsSignature && secondEndIsSignature) {
            throw new TransformerException(
                    "Can not extract Operation of provided LEM. Both ends matched target type.");
        }

        // try to extract the ONLY end that is a RAM signature
        if (firstEndIsSignature) {
            return 0;
        }
        if (secondEndIsSignature) {
            return 1;
        }
        // If none of the ends matched the requested type, return -1, ton
        // indicate that resolving is not possible.
        return -1;
    }

    /**
     * Counterpart to getRamOperationMappingEnd. Takes a RIF-RAM
     * accessMethod/Operation mapping and resolves it to the AccessMethod
     * (precisely the Get/Put/Post/Delete implementation) on RIF side.
     * 
     * @param mapping
     *            as the LEM to be searched for a RIF access method. throws
     *            transformer exception if not exactly one of the contained
     *            mapping ends references a RIF access method.
     * @return the access method referenced by the LEM, if there is exactly one.
     *         throws a transformer exception, if not exactly one AccessMethod
     *         contained in LEM.
     */
    public static AccessMethod getRestAccessMethodMappingEnd(
            COREModelElementMapping mapping) {

        // Verify there are exactly two LEM ends.
        EList<EObject> dualMappingEnds = tryToExtractDualMappingEnds(mapping);

        // Verify exactly one of the ends is a RIF access-method.
        // reject mapping if both ends are RIF access methods
        EObject firstEnd = dualMappingEnds.get(0);
        EObject secondEnd = dualMappingEnds.get(1);
        boolean firstEndIsAccessMethod = isRestAccessMethodMappingEnd(firstEnd);
        boolean secondEndIsAccessMethod = isRestAccessMethodMappingEnd(secondEnd);
        if (firstEndIsAccessMethod && secondEndIsAccessMethod) {
            throw new TransformerException(
                    "Can not extract Access Method of provided LEM. Both ends matched target type.");
        }

        // try to extract the ONLY end that is a RAM signature
        if (firstEndIsAccessMethod) {
            return (AccessMethod) firstEnd;
        }
        if (secondEndIsAccessMethod) {
            return (AccessMethod) secondEnd;
        }

        // reject if none of the ends is a RAM operation
        throw new TransformerException(
                "Can not extract Access Method of provided LEM. None of ends matched target type.");
    }

    /**
     * Counterpart to getRamOperationMappingEnd. Takes a RIF-RAM
     * accessMethod/Operation mapping and resolves it to the DynamicFragment on RIF side.
     * 
     * @param mapping
     *            as the LEM to be searched for a RIF dynamic fragment. throws
     *            transformer exception if not exactly one of the contained
     *            mapping ends references a RIF dynamic fragment.
     * @return the DynamicFragment referenced by the LEM, if there is exactly one.
     *         throws a transformer exception, if not exactly one DynamicFragment
     *         contained in LEM.
     */
    public static DynamicFragment getRestDynamicFragmentMappingEnd(
            COREModelElementMapping mapping) {

        // Verify there are exactly two LEM ends.
        EList<EObject> dualMappingEnds = tryToExtractDualMappingEnds(mapping);

        // Verify exactly one of the ends is a RIF dynamic fragments.
        // reject mapping if both ends are RIF dynamic fragments
        EObject firstEnd = dualMappingEnds.get(0);
        EObject secondEnd = dualMappingEnds.get(1);
        boolean firstEndIsAccessMethod = isRestDynamicFragmentMappingEnd(firstEnd);
        boolean secondEndIsAccessMethod = isRestDynamicFragmentMappingEnd(secondEnd);
        if (firstEndIsAccessMethod && secondEndIsAccessMethod) {
            throw new TransformerException(
                    "Can not extract Access Method of provided LEM. Both ends matched target type.");
        }

        // try to extract the ONLY end that is a RAM signature
        if (firstEndIsAccessMethod) {
            return (DynamicFragment) firstEnd;
        }
        if (secondEndIsAccessMethod) {
            return (DynamicFragment) secondEnd;
        }

        // reject if none of the ends is a RAM operation
        throw new TransformerException(
                "Can not extract Access Method of provided LEM. None of ends matched target type.");
    }

    /**
     * Verifies a LEM holds exactly two ends, and returns the corresponding list
     * of EObjects.
     * 
     * @param mapping as the mapping model, containing the LEMs.
     * @return returns the two mapping ends as an EList. Throws
     * Transformer-exception if there are not exactly two.
     */
    private static EList<EObject> tryToExtractDualMappingEnds(COREModelElementMapping mapping) {

        // Check the concrete class of both endings.
        EList<EObject> mappingEnds = mapping.getModelElements();

        if (mappingEnds.size() != 2) {
            throw new TransformerException(
                    "Unable to extract unique RAM signature end of provided binary LEM. The mapping is not two-ended.");
        }

        return mappingEnds;
    }

    /**
     * Returns the file (directory) location on disk where the code for the
     * current (RAM Aspect) model should be placed. (the maven codegenerator
     * will create a new subfolder, if needed)
     * 
     * @return File modelLocation as the location on disk where code should be
     * generated to.
     */
    private static File getModelBasePath(EObject aspect) {

        // Obtain model base path
        File ramModelBasePath = new File(aspect.eResource().getURI().toFileString());
        File modelBasePath = ramModelBasePath.getParentFile().getParentFile();
        return modelBasePath;

    }

    /**
     * Finds the selected Restify implementation from the model reuses in the given concern if it exists.
     * 
     * @author Bowen
     * 
     * @param concern input concern the user is working on
     * @return selected Restify implementation if it exists
     */
    public static RestifyImplementation getSelectedRestifyImplementation(COREConcern concern) {
        COREReuseArtefact coreReuseArtefact = getCOREReuseArtefactFrom(concern);

        if (coreReuseArtefact != null) {
            COREModelReuse restifyModelReuse = getRestifyModelReuseFrom(coreReuseArtefact);
            if (restifyModelReuse != null) {	            
                COREConfiguration restifyConfiguration = restifyModelReuse.getConfiguration();

                for (COREFeature selectedRestifyFeature : restifyConfiguration.getSelected()) {
                    if (selectedRestifyFeature.getName().equals("Spring Boot")) {
                        return RestifyImplementation.SPRING_BOOT;
                    } else if (selectedRestifyFeature.getName().equals("Eclipse Jersey")) {
                        return RestifyImplementation.ECLIPSE_JERSEY;
                    } else if (selectedRestifyFeature.getName().equals("JBoss RESTEasy")) {
                        return RestifyImplementation.JBOSS_RESTEASY;
                    } else if (selectedRestifyFeature.getName().equals("Apache CXF")) {
                        return RestifyImplementation.APACHE_CXF;
                    }
                }
            }
        }

        return RestifyImplementation.NONE;
    }

    /**
     * Verifies that the mappings are correct for transformation with the {@link RifRamMappingsValidator}.
     * 
     * @author Bowen
     * 
     * @return whether or not the mappings in the current concern are correct or not.
     */
    public static boolean validateMappingsForTransformation() {
        RifRamMappingsValidator rifRamMappingsValidator = new RifRamMappingsValidator();

        COREConcern concernWithRifRamMappings = NavigationBar.getInstance().getCurrentConcern();

        List<COREModelElementMapping> mappings = getRifRamLanguageElementMappings(concernWithRifRamMappings);

        return rifRamMappingsValidator.validateMappingsForTransformation(mappings, concernWithRifRamMappings);
    }

    /**
     * Finds the first reuse artefact from the input core concern. (Unsafe, there could be multiple reuse  artefacts
     * from the input core concern).
     * 
     * @author Bowen
     * 
     * @param concern input concern the user is working on
     * @return first reuse artefact from input concern's artefacts
     */
    private static COREReuseArtefact getCOREReuseArtefactFrom(COREConcern concern) {
        for (COREArtefact coreArtefact : concern.getArtefacts()) {
            if (coreArtefact instanceof COREReuseArtefact) {
                return (COREReuseArtefact) coreArtefact;
            }
        }

        return null;
    }

    /**
     * Look in the input reuse artefact and return the model reuse that contains the Restify reuse.
     * 
     * @author Bowen
     * 
     * @param coreReuseArtefact input reuse artefact
     * @return model reuse that contains the Restify reuse
     */
    private static COREModelReuse getRestifyModelReuseFrom(COREReuseArtefact coreReuseArtefact) {
        for (COREModelReuse modelReuse : coreReuseArtefact.getModelReuses()) {
            COREReuse reuse = modelReuse.getReuse();

            if (reuse.getName().equals("RESTify")) {
                return modelReuse;
            }
        }

        return null;
    }
}
