package ca.mcgill.sel.ram.ui.scenes;

import org.eclipse.emf.common.util.EList;

import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREModelElementMapping;
import ca.mcgill.sel.core.impl.COREConcernImpl;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.Interaction;
import ca.mcgill.sel.ram.Lifeline;
import ca.mcgill.sel.ram.Message;
import ca.mcgill.sel.ram.MessageSort;
import ca.mcgill.sel.ram.MessageView;
import ca.mcgill.sel.ram.Operation;
import ca.mcgill.sel.ram.Parameter;
import ca.mcgill.sel.ram.ParameterValue;
import ca.mcgill.sel.ram.ParameterValueMapping;
import ca.mcgill.sel.ram.RamFactory;
import ca.mcgill.sel.ram.Reference;
import ca.mcgill.sel.ram.TypedElement;
import ca.mcgill.sel.ram.controller.AspectController;
import ca.mcgill.sel.ram.controller.ControllerFactory;
import ca.mcgill.sel.ram.controller.MessageViewController;
import ca.mcgill.sel.ram.impl.OperationImpl;
import ca.mcgill.sel.ram.impl.RamFactoryImpl;

/**
 * Class with helper methods that ensure a correct wiring of a fpcdm's REST
 * controllers and the corresponding backend model counterparts. Subclasses of
 * this extend framework specific launcher delegations (e.g. to power up a REST
 * framework at application start).
 * 
 * @author Maximilian Schiedermeier
 *
 */
public abstract class AbstractTrafoDelegationUtils {

    /**
     * Main access method to take care of wiring between generated FPCDM and BL
     * backend (FRCDM) via sequence diagrams. Subclasses of this abstract class
     * ensure eg that the classes annotated with @RestController (spring
     * controllers) correctly delegate to corresponding counterparts in the
     * business logic.
     * 
     * @param concern as the overall concern that requires mapping of
     * controllers to legacy functionality (implementation classes from jar).
     * @param fpcdm as the functionality poor class diagram model (annotated
     * rest controllers) that should delegate to legacy functionality
     * (implementation classes from jar).
     */
    public void linkControllersToBusinessLogic(COREConcernImpl concern, COREExternalArtefact fpcdm) {

        // Identify all controller operations that require linking to business
        // logic:
        for (COREModelElementMapping mapping : TrafoUtils.getRifRamLanguageElementMappings(concern)) {

            // Delegations are only required for method mappings, parameter
            // mappings can be ignored here.
            if (TrafoUtils.isOperationMapping(mapping)) {

                // Use mappings RAM end to identify the corresponding business
                // logic
                // operation.
                OperationImpl mappedRamOperation = TrafoUtils.getRamOperationMappingEnd(mapping);

                // Resolve the ram operation to it's controller counterpart
                // (only
                // controllers are decorated with annotations.)
                Operation controllerOperation = AbstractFunctionalityPoorCdmFillUtil
                        .resolveToControllerOperation(mappedRamOperation, fpcdm);

                // Link those two by SD-delegation (controllerOperation must
                // delegate to ramOperation)
                addDelegatingSequenceDiagram(fpcdm, controllerOperation, mappedRamOperation);
            }
        }
    }

    /**
     * Helper method that takes care of planting a sequence diagram in a
     * provided FPCDM controller that delegates the call to the corresponding
     * back-end controller. This method must be called for every FPCDM-exposed
     * controller resource.
     * 
     * @param fpcdm as the RAM model that houses the operation we want to
     * specify a behavioral model for.
     * @param resourceOperation as the spring controller operation that
     * delegates.
     * @param backendMethod as the cloned (and mapped) backend operation that is
     * invoked upon delegation.
     */
    @SuppressWarnings("static-method")
    private void addDelegatingSequenceDiagram(COREExternalArtefact fpcdm, Operation resourceOperation,
            Operation backendMethod) {

        // Verify the provided fpcdm actually is a RAM (aspect) and extract it.
        Aspect fpcdmAspect = verifyAndExtractAspect(fpcdm);

        // Initialize the delegation SD and obtain the base meta-classe's
        // lifeline.
        MessageView messageView = initializeSequenceDiagram(fpcdmAspect, resourceOperation);

        // Add lifeline / message to obtain backend instance ("getMessage" call)
        // Also saves the obtained instance in a local variable.
        Classifier backendMetaClass = (Classifier) backendMethod.eContainer();
        TypedElement backendInstance = addObtainInstanceSequence(messageView, backendMetaClass);

        // Now use the obtained backend instance to delegate the actual API
        // call.
        // This requires another lifeline, because the metaclass lifeline is not
        // identical to the concrete instance's lifeline.
        TypedElement delegationResult = addDelegateToControllerSequenceAndGetResult(messageView, backendInstance,
                backendMethod);

        // Finally modify the SD to that the result of the delegation call is
        // returned as overall result. (if not null - which encodes void)
        if (delegationResult != null) {
            associateDelegationResult(messageView.getSpecification(), delegationResult);
        }
    }

    /**
     * Inspects a provided coreexternalartefact type. Verifies if it is an
     * aspect or aspect subclass. Returns the cast aspect if verification
     * succeeded, throws trafo exception otherwise.
     * 
     * @param coreExternalArtefact as the artefact to inspect.
     * @return
     */
    @SuppressWarnings("static-method")
    protected Aspect verifyAndExtractAspect(COREExternalArtefact coreExternalArtefact) {
        // Verify the provided fpcdm actually is a RAM (aspect).
        if (!(coreExternalArtefact.getRootModelElement() instanceof Aspect)) {
            throw new RuntimeException("Impossible to add a message view to the provided artefact. "
                    + "The containing core external artefact is not an aspect.");
        }
        return (Aspect) coreExternalArtefact.getRootModelElement();
    }

    /**
     * Helper method to modify the SD so that the result of the delegation call
     * is returned as overall SD result.
     * 
     * @param messageViewSpecification as the SD specification to alter.
     * @param delegationResult as the overall result to be returned by this SD.
     */
    private void associateDelegationResult(Interaction messageViewSpecification, TypedElement delegationResult) {

        // First find the one message that represents this SDs return message.
        // Has type return and is contained in interaction.messages:
        Message sdFinalReturnMessage = fiddleOutFinalReturnMessage(messageViewSpecification);

        // Parameter sets the type
        Parameter returnMessageParameterType = RamFactoryImpl.eINSTANCE.createParameter();
        returnMessageParameterType.setType(delegationResult.getType());
        returnMessageParameterType.setName(delegationResult.getName());

        // ParameterValue sets the actual result, refering to the previously
        // created type
        ParameterValue returnParameterValue = RamFactoryImpl.eINSTANCE.createParameterValue();
        returnParameterValue.setParameter(returnMessageParameterType);
        sdFinalReturnMessage.setReturns(returnParameterValue);
    }

    /**
     * Adds the delegation to an instantiated backend class to a sequence
     * diagram.
     * 
     * @param messageView that requires the delegation.
     * @param backendClassInstance as the entity of the backend class the
     * operation is called on.
     * @param backendMethod as the operation to be called on the backend class.
     * @return typedElement as the result value of the delegation call.
     */
    private TypedElement addDelegateToControllerSequenceAndGetResult(MessageView messageView,
            TypedElement backendClassInstance, Operation backendMethod) {

        // Look up the original SD lifeline (sender), specification.
        Lifeline senderLifeline = extractBaseLifeline(messageView);
        Interaction messageviewSpecification = messageView.getSpecification();
        MessageViewController messageViewController = ControllerFactory.INSTANCE.getMessageViewController();

        // If messageView has input parameters (controller call has parameters),
        // add those as values to target operation
        // backendMethod.getParameters().add(messageviewSpecification)

        // Create a new lifeline for the instantiated backend class and delegate
        // the original controller operation call
        messageViewController.createLifelineWithMessage(messageviewSpecification, backendClassInstance, 0, 0,
                senderLifeline, messageviewSpecification, backendMethod, 2);

        // Pass through controller arguments
        passThroughArguments(messageviewSpecification, backendMethod);

        // If (and only if) there is a result:
        // Fiddle out the actual result, by inspecting the lifeline message we
        // just created
        if (backendMethod.getReturnType().toString().contains("Void")) {
            return null;
        } else {
            Message backendDelegationMessage = getMostRecentlyAddedLifelineMessage(messageviewSpecification);
            TypedElement backendCallResult = ControllerFactory.INSTANCE.getMessageController()
                    .createTemporaryAssignment(backendDelegationMessage);

            // Return whatever has been produced by the backend.
            return backendCallResult;
        }
    }

    /**
     * Modify the previously created Lifeline message, so that the actual call
     * also contains call parameters. All input parameters of the original
     * messageView are passed through.
     * 
     * @param messageviewSpecification
     * @param backendMethod
     */
    @SuppressWarnings("static-method")
    private void passThroughArguments(Interaction messageviewSpecification, Operation backendMethod) {

        Message backendDelegationMessage = getMostRecentlyAddedLifelineMessage(messageviewSpecification);
        EList<Parameter> expectedArguments = backendMethod.getParameters();

        /*
         * Parameters are referenced by name. Controller parameter names are
         * "arg0", "arg1", We use an integer counter to build the parameter name
         * strings.
         */
       // int argCounter = 0;
        for (Parameter parameter : expectedArguments) {
            // Construct name for parameter
            //String argName = "arg" + argCounter++;

            // Actually pass a parameter with this name in the message call to
            // the backend Method.
            // We need a parameter mapping that internally links parameter and
            // parameter value. The parameter value additionally also needs a
            // reference to the parameter.
            ParameterValue parameterValue = RamFactoryImpl.eINSTANCE.createParameterValue();
            parameterValue.setParameter(parameter);
            ParameterValueMapping parameterValueMapping = RamFactoryImpl.eINSTANCE.createParameterValueMapping();
            parameterValueMapping.setParameter(parameter);
            parameterValueMapping.setValue(parameterValue);

            // Actually add this parameter/value mapping to the lifeline message, so the parameter is included
            // in the call
            backendDelegationMessage.getArguments().add(parameterValueMapping);
        }
    }

    /**
     * Prepares the injection of behavioural logic for a a given operation
     * Returns the base lifeline of the created sequence diagram.
     * 
     * @param fpcdmAspect as the aspect that required a SD
     * @param resourceOperation as the precise operation that requires a SD.
     * @return messageview instantiated by this method.
     */
    @SuppressWarnings("static-method")
    protected MessageView initializeSequenceDiagram(Aspect fpcdmAspect, Operation sourceOperation) {
        // Get an aspect controller. This controller allows adding message view
        // for the delegating operation.
        AspectController aspectController = ControllerFactory.INSTANCE.getAspectController();

        // Add a messageView (empty SD) for the provided operation. This
        // implicitly initializes a corresponding metaclass lifeline.
        MessageView messageView = aspectController.createMessageView(fpcdmAspect, sourceOperation);

        return messageView;
    }

    /**
     * Helper method to obtain the base lifeline present in the messageview.
     * This is the lifeline of the base metaclass associated to the operation
     * whose behaviour is described by the message view.
     * 
     * @param messageView that shelters the base lifeline.
     * @return the base lifeline (the one associated to the messageview's
     * metaclass)
     */
    @SuppressWarnings("static-method")
    protected Lifeline extractBaseLifeline(MessageView messageView) {

        // The base lifeline is always the first listed lifeline in a
        // messageview's interaction (specification)
        Interaction messageviewSpecification = messageView.getSpecification();
        return messageviewSpecification.getLifelines().get(0);
    }

    /**
     * Adds a "getInstance" call to a provided controller method. Also stores
     * the obtained instance in a local variable and returns it.
     * 
     * @param messageView as the SD that must implement the getInstance call.
     * @param backendClass as the singleton class that must be called in the SD.
     * @return the typed element representing the obtained instance.
     *
     */
    private TypedElement addObtainInstanceSequence(MessageView messageView, Classifier backendClass) {

        // resolve the provided backend class to the "getInstance" method that
        // we want to use.
        Operation singletonAccessMethod = TrafoUtils.obtainSingletonAccessOperation(backendClass);

        // Creation of a lifeline for the backend metaclass requires wrapping
        // the class up as a reference (typed element)
        MessageViewController messageViewController = ControllerFactory.INSTANCE.getMessageViewController();
        Interaction messageviewSpecification = messageView.getSpecification();
        TypedElement classifierWrapper = createStaticReferenceAcceptedByLifelineControllerForLifelineCreationAndEnsureItsRegisteredInTheInteraction(
                messageviewSpecification, backendClass);

        // Now that we have the backend class wrapped up as a typed element
        // we can create a new lifeline for it and invoke its "getInstance"
        // method.
        messageViewController.createLifelineWithMessage(messageviewSpecification, classifierWrapper, 0, 0,
                extractBaseLifeline(messageView), messageviewSpecification, singletonAccessMethod, 1);

        // Finally the obtained "instance" must by stored, so it can be used for
        // subsequent calls.
        // This element is associated the the most recently created message.
        // Therefore we extract if from the corresponding lifeline message.
        Message getInstanceMessage = getMostRecentlyAddedLifelineMessage(messageviewSpecification);
        TypedElement instance = ControllerFactory.INSTANCE.getMessageController()
                .createTemporaryAssignment(getInstanceMessage);
        return instance;
    }

    /**
     * Analyzes the interaction (messageViewDescription) of a given sequence
     * diagram and determines the one message that represents the final
     * result/return message.
     * 
     * @param interaction as the messageViewSpecification to analyze
     * @return message that specifies the sequence diagrams result message.
     */
    @SuppressWarnings("static-method")
    private Message fiddleOutFinalReturnMessage(Interaction interaction) {
        for (Message message : interaction.getMessages()) {
            if (message.getMessageSort() == MessageSort.REPLY) {
                return message;
            }
        }
        throw new TransformerException("Unable to find SD return type in interaction. This is required to finalize the "
                + "delegation from REST controller to Business Logic");
    }

    /**
     * Helper method to fiddle out the most recently added message to another
     * lifeline, for a given messageViewSpecification.
     * 
     * @param messageViewSpecification specification that houses all messages of
     * a given messageview.
     * @return the message most recently added to the interaction
     */
    protected static Message getMostRecentlyAddedLifelineMessage(Interaction messageViewSpecification) {

        // get index of most recently added message
        int indexOfMostRecentMessage = messageViewSpecification.getMessages().size() - 1;

        // return the corresponding message
        return messageViewSpecification.getMessages().get(indexOfMostRecentMessage);
    }

    /**
     * Creates a sequence diagram that makes the auto generated new service
     * launcher invoke the selected rest technology. This method is abstract, as
     * its changes depending on the actual REST technology chosen.
     * 
     * @param fpcdm
     */
    public abstract void linkLauncherToRestFramework(COREExternalArtefact fpcdm);

    /**
     * Frustrating and ugly util method that is required because the lifeline
     * controller does not accept classifiers (for the creation of lifelines).
     * Calling this method embeds an artificial static reference in the
     * messageviewSpecificiation that presents the classifier as a typesElement
     * accepted by the controller. The returned reference can than be passed to
     * the lifeline controller instead of the actual classifier.
     * 
     * @param classifier that must be imitated by a static reference
     * @param interaction as messageviewSpecification that required an entry for
     * the static reference
     * @return Reference static reference representing the provided classifier
     * as typedElement
     */
    protected static Reference createStaticReferenceAcceptedByLifelineControllerForLifelineCreationAndEnsureItsRegisteredInTheInteraction(
            Interaction interaction, Classifier classifier) {

        Reference staticReference = RamFactory.eINSTANCE.createReference();
        staticReference.setType(classifier);
        staticReference.setStatic(true);
        interaction.getProperties().add(staticReference);
        return staticReference;
    }
}
