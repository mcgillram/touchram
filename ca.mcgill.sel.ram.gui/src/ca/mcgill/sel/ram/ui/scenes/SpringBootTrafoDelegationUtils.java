package ca.mcgill.sel.ram.ui.scenes;

import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.AssignmentStatement;
import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.ImplementationClass;
import ca.mcgill.sel.ram.Interaction;
import ca.mcgill.sel.ram.InteractionFragment;
import ca.mcgill.sel.ram.Lifeline;
import ca.mcgill.sel.ram.Message;
import ca.mcgill.sel.ram.MessageView;
import ca.mcgill.sel.ram.OpaqueExpression;
import ca.mcgill.sel.ram.Operation;
import ca.mcgill.sel.ram.Parameter;
import ca.mcgill.sel.ram.ParameterValue;
import ca.mcgill.sel.ram.ParameterValueMapping;
import ca.mcgill.sel.ram.TypedElement;
import ca.mcgill.sel.ram.ValueSpecification;
import ca.mcgill.sel.ram.controller.ControllerFactory;
import ca.mcgill.sel.ram.controller.MessageController;
import ca.mcgill.sel.ram.controller.MessageViewController;
import ca.mcgill.sel.ram.impl.RamFactoryImpl;
import ca.mcgill.sel.ram.impl.ValueSpecificationImpl;

/**
 * Spring Boot specific implementation of the Trafo Delegation Utils.
 * This wires the previously created launcher class to the spring provided run method.
 * 
 * @author Maximilian Schiedermeier
 *
 */
public class SpringBootTrafoDelegationUtils
        extends
            AbstractTrafoDelegationUtils {

    /**
     * Spring specific implementation that links the default launcher main
     * method to springs "run" method, so the framework is invoked.
     */
    @Override
    public void linkLauncherToRestFramework(COREExternalArtefact fpcdm) {

        // 0.) Extract aspect.
        Aspect fpcdmAspect = verifyAndExtractAspect(fpcdm);

        // 1.) Identify fpcdm launcher class (by name) and "main" operation.
        String springBootSpecificLauncherClassIdentifier = SpringBootFunctionalityPoorCdmFillUtil.LAUNCHER_NAME;
        Classifier springBootSpecificLauncherClass = extractClassifierByName(
                fpcdmAspect, springBootSpecificLauncherClassIdentifier);
        Operation mainOperation = springBootSpecificLauncherClass
                .getOperations().get(0);

        // 2.) Add Spring Boot implementation class and "run" operation.
        String springBootImplementationClassIdentifier = SpringBootFunctionalityPoorCdmFillUtil.FRAMEWORK_IMPLEMENTATION_CLASS_NAME;
        Classifier springBootImplementationClass = extractClassifierByName(
                fpcdmAspect, springBootImplementationClassIdentifier);
        Operation runSpring = springBootImplementationClass.getOperations()
                .get(0);

        // 3.) Add a new behaviour specification to the launcher's main
        // operation. (call spring run method with class and string[] args.)
        addCallToRun(fpcdm, mainOperation, springBootImplementationClass,
                runSpring);
        System.out.println("Launcher wiring completed.");
    }

    /**
     * Adds a message view to the launcher classes main method. The generated
     * code is: create a new "ClassResolver" (extends Object), call "getClass".
     * Then on the returned class object, all "getEnclosingClass" use the
     * respective returned class and the main args as input parameters for
     * springs run method.
     * 
     * @param fpcdm
     * @param mainOperation
     * @param springBootImplementationClass
     * @param runSpring
     */
    private void addCallToRun(COREExternalArtefact fpcdm,
            Operation mainOperation, Classifier springBootImplementationClass,
            Operation runSpring) {
        Aspect fpcdmAspect = verifyAndExtractAspect(fpcdm);
        MessageViewController messageViewController = ControllerFactory.INSTANCE
                .getMessageViewController();

        // 3.0) Prepare message view, lifeline types.
        MessageView runMessageView = initializeSequenceDiagram(fpcdmAspect,
                mainOperation);
        Lifeline baseLifeline = extractBaseLifeline(runMessageView);
        Interaction messageViewSpecification = runMessageView
                .getSpecification();

       

        // 3.2A) Add a new lifeline for the spring metaclass + delegation call
        TypedElement springBootImplementationClassWrapper = createStaticReferenceAcceptedByLifelineControllerForLifelineCreationAndEnsureItsRegisteredInTheInteraction(
                messageViewSpecification, springBootImplementationClass);
        messageViewController.createLifelineWithMessage(
                messageViewSpecification, springBootImplementationClassWrapper,
                0, 0, extractBaseLifeline(runMessageView),
                messageViewSpecification, runSpring, 1);

        // 3.2B) Add message parameters (RestServiceLauncher.class, String[]
        // args)
        // Resolve the parameter eObjects (by accessing the operation we want to call)
        Parameter launcherClass = runSpring.getParameters().get(0);
        Parameter stringArgs = runSpring.getParameters().get(1);
        
        // Create expressions that can be mapped on the operation parameters (fed as input)
        OpaqueExpression launcherClassValue = RamFactoryImpl.eINSTANCE.createOpaqueExpression();
        launcherClassValue.setBody("RestServiceLauncher.class");
        //OpaqueExpression stringArgsValue = RamFactoryImpl.eINSTANCE.createOpaqueExpression();
        ParameterValue stringArgsValue =  RamFactoryImpl.eINSTANCE.createParameterValue();
        stringArgsValue.setParameter(stringArgs);
        //stringArgsValue.setBody("args"); - not needed because stringArgsParameter is not opaque.
       
        // Link parameter eObjects and the values to be used when modeling a messageview call:
        ParameterValueMapping clazzMapping = RamFactoryImpl.eINSTANCE.createParameterValueMapping();
        clazzMapping.setParameter(launcherClass);
        clazzMapping.setValue(launcherClassValue);
        ParameterValueMapping argsMapping = RamFactoryImpl.eINSTANCE.createParameterValueMapping();
        argsMapping.setParameter(stringArgs);
        argsMapping.setValue(stringArgsValue);
        
        // Actually add theses parameter/value mappings to the message that represents the operation call:
        Message callSpringMessage = getMostRecentlyAddedLifelineMessage(
                messageViewSpecification);
        callSpringMessage.getArguments().add(clazzMapping);
        callSpringMessage.getArguments().add(argsMapping);
    }

    /**
     * Allows to retrieve a previously created lifeline. This exloits the fact
     * that lifelines are added incrementally. To get the most recent lifeline,
     * set the backwardcounter to 0, to get the secend youngest lifelne set it
     * to 1, etc.
     * 
     * @param messageViewSpecification
     * @param backwardcounter
     * @return
     */
    @SuppressWarnings("static-method")
    private Lifeline getLastLifeline(Interaction messageViewSpecification,
            int backwardcounter) {
        int indexLastLifeline = messageViewSpecification.getLifelines().size()
                - 1;
        return messageViewSpecification.getLifelines().get(indexLastLifeline - backwardcounter);
    }

    /**
     * Utility method to extract specific classifiers previously added by the
     * fpdcmFillUtils.
     * 
     * @param aspect
     *            as the model to analyze
     * @param classifierName
     *            as the identifier to search for
     * @return the first classifier found with matching name. A trafo exception
     *         is thrown if none of the existing classifiers matches.
     */
    @SuppressWarnings("static-method")
    private Classifier extractClassifierByName(Aspect aspect,
            String classifierName) {

        // Launcher is identified by name (final variable in
        // FunctionalityPoorCdmFillUtil)
        for (Classifier classifier : aspect.getStructuralView().getClasses()) {
            if (classifier.getName().equals(classifierName)) {
                return classifier;
            }
        }
        throw new TransformerException(
                "Launcher SD creation failed. Unable to locate class with identifier: "
                        + classifierName);

    }

}
