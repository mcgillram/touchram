package ca.mcgill.sel.ram.ui.scenes;

import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.Operation;
import ca.mcgill.sel.ram.OperationType;
import ca.mcgill.sel.ram.Parameter;
import ca.mcgill.sel.ram.RAMClassVisibilityType;
import ca.mcgill.sel.ram.RArray;
import ca.mcgill.sel.ram.RamFactory;
import ca.mcgill.sel.ram.StructuralView;
import ca.mcgill.sel.ram.Type;
import ca.mcgill.sel.ram.impl.AspectImpl;
import ca.mcgill.sel.ram.impl.OperationImpl;
import ca.mcgill.sel.ram.impl.RamFactoryImpl;
import ca.mcgill.sel.ram.util.StructuralViewUtil;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.management.RuntimeErrorException;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.ram.ImplementationClass;
import ca.mcgill.sel.ram.ObjectType;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREMapping;
import ca.mcgill.sel.core.COREModelElementComposition;
import ca.mcgill.sel.core.COREModelExtension;
import ca.mcgill.sel.ram.ArtifactSignature;
import ca.mcgill.sel.ram.Aspect;

/**
 * Utility class for all operations related to creating and filling a
 * FPCDM-Artifact with content. Content covers EObject structure (including
 * annotations) as well as mappings to the extended FrCdm Aspect. Note: This
 * class only takes care of structural element, not behavioural models. For SDs,
 * see "AbstractTrafoDelegationUtils" and framework-specific implementations.
 * 
 * @author Maximilian Schiedermeier
 *
 */
/**
 * @author gemini
 *
 */
public class SpringBootFunctionalityPoorCdmFillUtil
        extends
            AbstractFunctionalityPoorCdmFillUtil {

    /**
     * The launcher name.
     */
    public static final String LAUNCHER_NAME = "RestServiceLauncher";
    
    /**
     * The name of the Spring application.
     */
    public static final String FRAMEWORK_IMPLEMENTATION_CLASS_NAME = "SpringApplication";
    
    /**
     * The name of a class.
     */
    public static final String GENERIC_CLASS_NAME = "Class";
    
    /**
     * The name of an object.
     */
    public static final String OBJECT_CLASS_NAME = "Object";
    
//    public static final String CLASS_RESOLVER_NAME="ClassResolver";
    
    /**
     * The name of the application context.
     */
    public static final String APPLICATION_CONTEXT_CLASS_NAME = "ConfigurableApplicationContext";

    /**
     * Adds spring specific launcher and the required operations. (main,
     * getClass:Class<?>))
     */
    @Override
    public Classifier addRestApplicationLauncherClass(
            COREExternalArtefact fpcdmArtefact) {
        
        // Extract the actual aspect
        Aspect fpcdm = (Aspect) fpcdmArtefact.getRootModelElement();
        
        // Create reusable factory, used to create the required structural
        // elements:
        RamFactory factory = RamFactoryImpl.init();
        
        // first create all classes / types
        addAllLauncherRelatedClassifiers(fpcdm, factory);
        
        // then add the operations, for / using / depending-on these types
        enrichLauncherRelatedClassifiersByOperations(fpcdm, factory);
        
        // Finally return the main launcher class
        return resolveFpClassifierByName(LAUNCHER_NAME, fpcdm, false);
    }

    @Override
    protected Object getLauncherName() {
        return LAUNCHER_NAME;
    }
    
    private void addAllLauncherRelatedClassifiers(Aspect fpcdm, RamFactory factory) {
        // Extract the structural view where classifiers are added to:
        StructuralView structuralView = fpcdm.getStructuralView();
        
        // Object (only required as supertype for classresolver)
        ImplementationClass objectClass = factory.createImplementationClass();
        objectClass.setName(OBJECT_CLASS_NAME);
        objectClass.setVisibility(RAMClassVisibilityType.PUBLIC);
        objectClass.setInstanceClassName("java.lang.Object");
        structuralView.getClasses().add(objectClass);
        
        // ClassResolver => Obsolete, getClass() is called as opaque statement.
        // This one extends OBJECT, so it has an accessible "getClass()" operation.
//        Classifier classResolver = factory.createClass();
//        classResolver.setName(CLASS_RESOLVER_NAME);
//        classResolver.getSuperTypes().add(objectClass);
//        structuralView.getClasses().add(classResolver);
        
        // String[]
        addStringArrayType(fpcdm);
        
        // Class<?> (GENERIC_CLASS_NAME)
        ImplementationClass genericClass = factory.createImplementationClass();
        genericClass.setName(GENERIC_CLASS_NAME);
        genericClass.setVisibility(RAMClassVisibilityType.PUBLIC);
        genericClass.setInstanceClassName("java.lang.Class");
        structuralView.getClasses().add(genericClass);
        
        // SpringApplication (FRAMEWORK_IMPLEMENTATION_CLASS_NAME)
        ImplementationClass springApplication = RamFactoryImpl.init()
                .createImplementationClass();
        springApplication.setName(FRAMEWORK_IMPLEMENTATION_CLASS_NAME);
        springApplication.setVisibility(RAMClassVisibilityType.PUBLIC);
        springApplication.setInstanceClassName(
                "org.springframework.boot.SpringApplication");
        fpcdm.getStructuralView().getClasses().add(springApplication);
        
        // ConfigurableApplicationContext
        ImplementationClass configurableAppContext = factory.createImplementationClass();
        configurableAppContext.setName(APPLICATION_CONTEXT_CLASS_NAME);
        configurableAppContext.setInstanceClassName(
                "org.springframework.context.ConfigurableApplicationContext");
        configurableAppContext.setInterface(true);
        structuralView.getClasses().add(configurableAppContext);
        
        // RestLauncher (The actual launcher) 
        Classifier launcher = factory.createClass();
        launcher.setName(LAUNCHER_NAME);
        structuralView.getClasses().add(launcher);
    }
    
    private void enrichLauncherRelatedClassifiersByOperations(Aspect fpcdm, RamFactory factory) {
        // Object: add getClass():Class<?> operation
        Operation getClassOperation = factory.createOperation();
        getClassOperation.setName("getClass");
        getClassOperation.setStatic(false);
        Classifier genericClass = resolveFpClassifierByName(GENERIC_CLASS_NAME, fpcdm, false);
        getClassOperation.setReturnType(genericClass);
        Classifier objectClass = resolveFpClassifierByName(OBJECT_CLASS_NAME, fpcdm, false);
        objectClass.getOperations().add(getClassOperation);
        
        // ClassResolver: add default constructor = > Not needed. ClassResolver is obsolete.
//        Classifier classResolver = resolveFpClassifierByName(CLASS_RESOLVER_NAME, fpcdm, false);
//        Operation defaultConstructor = factory.createOperation();
//        defaultConstructor.setOperationType(OperationType.CONSTRUCTOR);
//        defaultConstructor.setName("create");
//        defaultConstructor.setStatic(false);
//        defaultConstructor.setReturnType(classResolver);
//        classResolver.getOperations().add(defaultConstructor);
        
        // Class<?> add getEnclosingClass:Class<?>
        Operation getEnclosingClassOperation = factory.createOperation();
        getEnclosingClassOperation.setName("getEnclosingClass");
        getEnclosingClassOperation.setStatic(false);
        getEnclosingClassOperation.setReturnType(genericClass);
        genericClass.getOperations().add(getEnclosingClassOperation);
        
        // SpringApplication: add run(arg0:Class<?>, arg1:String[]):ConfigurableApplicationContext operation (static)
        // create operation parameters
        Parameter runOperationClassParameter = factory.createParameter();
        runOperationClassParameter.setName("clazz");
        runOperationClassParameter.setType(genericClass);
        RArray stringArray = extractStringArray(fpcdm);
        Parameter runOperationStringArrayParameter = factory.createParameter();
        runOperationStringArrayParameter.setName("args");
        runOperationStringArrayParameter.setType(stringArray);
        // create operation, set return type
        Operation runOperation = RamFactoryImpl.init().createOperation();
        runOperation.setName("run");
        runOperation.setStatic(true);
        runOperation.setReturnType(resolveFpClassifierByName(APPLICATION_CONTEXT_CLASS_NAME, fpcdm, false));
        runOperation.getParameters().add(runOperationClassParameter);
        runOperation.getParameters().add(runOperationStringArrayParameter);
        // add operation to spring implementation class
        Classifier springApplication = resolveFpClassifierByName(FRAMEWORK_IMPLEMENTATION_CLASS_NAME, fpcdm, false);
        springApplication.getOperations().add(runOperation);
          
        // RestLauncher: add main(args:String[]):void operation (static)
        Parameter args = factory.createParameter();
        args.setType(stringArray);
        args.setName("args");
        Operation mainOperation = RamFactoryImpl.init().createOperation();
        mainOperation.setName("main");
        mainOperation.setStatic(true);
        mainOperation.setReturnType(extractPrimitiveType("void", fpcdm));
        mainOperation.getParameters().add(args);
        resolveFpClassifierByName(LAUNCHER_NAME, fpcdm, false).getOperations().add(mainOperation);
    }
    

}