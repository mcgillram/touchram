package ca.mcgill.sel.ram.ui.scenes.handler.impl;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import ca.mcgill.sel.commons.UniversalFileLoader;

/**
 * Singleton util class to handle import of xml filed from disk and output of document objects to xml files on dsik.
 * Based on this tutorial: https://mkyong.com/java/how-to-read-xml-file-in-java-dom-parser/
 * 
 * @author Bowen
 */
public final class XmlIOUtils {
    // static variable reference of XMLIOUtils
    private static XmlIOUtils xmlIOUtils;
    
    // used for input xml parsing
    private DocumentBuilderFactory documentBuilderFactory;
    private DocumentBuilder documentBuilder;
    
    // used for output xml file
    private TransformerFactory transformerFactory;
    private Transformer transformer;
    
    /**
     * Private constructor, create factory, builder and transformer objects and sets some properties.
     * 
     * @throws ParserConfigurationException
     * @throws TransformerConfigurationException
     */
    private XmlIOUtils() throws ParserConfigurationException, TransformerConfigurationException {
        documentBuilderFactory = DocumentBuilderFactory.newInstance();
        // optional, but recommended
        // process XML securely, avoid attacks like XML External Entities (XXE)
        documentBuilderFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
        
        // do not load external DTD from internet (causes TouchCORE to crash when no wifi if not set) 
        documentBuilderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);

        documentBuilder = documentBuilderFactory.newDocumentBuilder();
        
        transformerFactory = TransformerFactory.newInstance();
        transformer = transformerFactory.newTransformer();
        
        // configure transformer for pretty-printing by setting the OutputKeys.INDENT property on the transformer
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
    }
    
    /**
     * Returns the singleton instance of xml IO utils.
     * 
     * @return - instance of xml IO utils
     * @throws ParserConfigurationException
     * @throws TransformerException
     */
    public static XmlIOUtils getInstance() throws ParserConfigurationException, TransformerException {
        if (xmlIOUtils == null) {
            xmlIOUtils = new XmlIOUtils();
        }
        return xmlIOUtils;
    }
    
    /**
     * Helper method to load XML validation definition resource.
     * @param schemaResourceName
     * @return parsed {@link Document} object of the xml schema
     * @throws IOException
     * @throws SAXException
     */
    public Document parseXmlSchemaToDocument(String schemaResourceName) throws IOException, SAXException {
        
        File clonedResourceFile = UniversalFileLoader.getInstance().getFileForResource(schemaResourceName);
        return parseXmlFromDiskToDocument(clonedResourceFile);
    }
    
    /**
     * Helper method to parse a given xml file located in the disk into a document object.
     *
     * @param file - {@link File} object of the target xml file on disk.
     * @return - {@link Document} object representing the in-ram tree structure of the parsed file.
     */
    public Document parseXmlFromDiskToDocument(File file) throws 
            IOException, SAXException {
        // parse XML file
        Document document = documentBuilder.parse(file);

        // optional, but recommended
        // http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
        document.getDocumentElement().normalize();
        return document;
    }
    
    /**
     * Based on this tutorial: https://www.baeldung.com/java-write-xml-document-file.
     *
     * @param xmlDocument - the in-memory xml tree.
     * @param fileLocation - the file location on disk as string.
     */
    public void writeXmlDocumentToDisk(Document xmlDocument, String fileLocation) throws TransformerException, 
            IOException {
        // Specify input file
        DOMSource source = new DOMSource(xmlDocument);

        // Create an output stream the transformer can write to
        FileWriter writer = new FileWriter(new File(fileLocation));
        StreamResult result = new StreamResult(writer);

        // Actually convert the xml tree to text and write it into the file
        transformer.transform(source, result);
    }
    
    /**
     * Create a new empty document and retur it.
     * 
     * @return - new empty document
     */
    public Document createEmptyDocument() {
        return documentBuilder.newDocument();
    }
}
