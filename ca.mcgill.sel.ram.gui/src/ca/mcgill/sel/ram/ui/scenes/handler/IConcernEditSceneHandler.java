package ca.mcgill.sel.ram.ui.scenes.handler;

import java.util.List;

import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.ram.ui.scenes.DisplayConcernEditScene;

/**
 * Handler Interface for Concern Scene in edit mode.
 * @author Nishanth
 *
 */
public interface IConcernEditSceneHandler extends IRamAbstractSceneHandler {
    
    /**
     * Interface call when the Home button button is pressed.
     * @param scene - the scene
     */
    void switchToHome(DisplayConcernEditScene scene);
    
    /**
     * Interface call to create an impact model of the concern.
     * @param scene - the scene
     */
    void createImpactModel(DisplayConcernEditScene scene);
    
    /**
     * Interface call to create a new aspect from the current scene of the concern.
     * @param scene - the scene
     */
    void createAspect(DisplayConcernEditScene scene);
    
    /**
     * Interface call to delete a model from the current scene of the concern.
     * @param scene - the scene
     * @param model - the model to delete
     */
    void deleteModel(DisplayConcernEditScene scene, COREArtefact model);
    
    /**
     * Interface call to weave a the scenes of a given perspective of a set of features of the current concern.
     * @param scene - the scene
     * @param c - the concern
     * @param p - the perspective of interest
     * @param features - the list of features
     */
    void weaveSelectedFeatures(DisplayConcernEditScene scene, COREConcern c, COREPerspective p,
            List<COREFeature> features);
}
