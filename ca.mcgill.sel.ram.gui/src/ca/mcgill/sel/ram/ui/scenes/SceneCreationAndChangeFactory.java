package ca.mcgill.sel.ram.ui.scenes;

import ca.mcgill.sel.ram.ui.components.RamPopup.PopupType;
import ca.mcgill.sel.ram.ui.components.navigationbar.NavigationBar;
import ca.mcgill.sel.ram.ui.scenes.handler.IConcernEditSceneHandler;
import ca.mcgill.sel.ram.ui.scenes.handler.IConcernSelectSceneHandler;
import ca.mcgill.sel.ram.ui.scenes.handler.IRamAbstractSceneHandler;
import ca.mcgill.sel.ram.ui.scenes.handler.impl.ConcernSelectSceneHandler;
import ca.mcgill.sel.ram.ui.utils.GUIConstants;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.ImpactModelUtil;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.handler.HandlerFactory;
import ca.mcgill.sel.ram.ui.views.handler.IDisplaySceneHandler;
import ca.mcgill.sel.ram.ui.views.structural.StructuralDiagramView;
import ca.mcgill.sel.restif.RestIF;
import ca.mcgill.sel.restif.ui.scenes.DisplayRestTreeScene;
import ca.mcgill.sel.restif.ui.views.handler.RestTreeHandlerFactory;
import ca.mcgill.sel.usecases.UseCaseModel;
import ca.mcgill.sel.usecases.ui.scenes.DisplayUseCaseModelScene;
import ca.mcgill.sel.usecases.ui.utils.UseCaseModelHandlerFactory;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEMap.Entry;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.mt4j.components.MTComponent;
import org.mt4j.sceneManagement.Iscene;
import org.mt4j.sceneManagement.transition.SlideTransition;

import ca.mcgill.sel.classdiagram.ClassDiagram;
import ca.mcgill.sel.classdiagram.ui.scenes.DisplayClassDiagramScene;
import ca.mcgill.sel.classdiagram.ui.views.handler.ClassDiagramHandlerFactory;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.COREImpactModel;
import ca.mcgill.sel.core.COREImpactNode;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.core.COREScene;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.IntraLanguageMapping;
import ca.mcgill.sel.core.controller.COREControllerFactory;
import ca.mcgill.sel.core.controller.ImpactModelElementController;
import ca.mcgill.sel.core.evaluator.im.PropagationResult;
import ca.mcgill.sel.core.guinavigation.IntraLanguageMappingTree;
import ca.mcgill.sel.core.guinavigation.NavigationMappingHelper;
import ca.mcgill.sel.core.guinavigation.IntraLanguageMappingTree.ILMNode;
import ca.mcgill.sel.core.impl.LayoutContainerMapImpl;
import ca.mcgill.sel.core.perspective.COREPerspectiveUtil;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.environmentmodel.EnvironmentModel;
import ca.mcgill.sel.environmentmodel.ui.scenes.DisplayEnvironmentModelScene;
import ca.mcgill.sel.environmentmodel.ui.utils.EmHandlerFactory;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.ui.RamApp;

public class SceneCreationAndChangeFactory {
    
    private static SceneCreationAndChangeFactory instance = new SceneCreationAndChangeFactory();
    /**
     * This will refer to whether the current session is running in select mode or edit mode.
     * 
     * @author ian
     *
     */
    public enum CurrentMode {
        /**
         * Edit mode: in this we will be changing the model.
         */
        EDIT, 
        
        /**
         * Select mode: here we will be interacting with models.
         */
        SELECT,
        
        /**
         * Build application mode: only opened from home scene.
         */
        BUILD,
        
        /**
         * Used for when there is an association concern scene which is being created.
         */
        ASSOCIATION
    }
    
    private CurrentMode currentMode;
    private IRamAbstractSceneHandler inputHandler;
    private PropagationResult inputPropResult;
    private Map<Iscene, EObject> sceneToModel;
    private Map<COREScene, COREFeature> sceneToFeature;
    private COREArtefact currentArtefact;
    private NavigationBar navbar;
    private int numberTried;
    private EObject eObjectToCenter;
    private EObject currentModel;
    
    public SceneCreationAndChangeFactory() {
        inputHandler = null;
        inputPropResult = null;
        sceneToModel = new HashMap<Iscene, EObject>();
        sceneToFeature = new HashMap<COREScene, COREFeature>();
        currentArtefact = null;
        navbar = NavigationBar.getInstance();
        numberTried = 0;
    }
    
    public static SceneCreationAndChangeFactory getFactory() {
        return instance;
    }
    
    /**
     * Used when a specific handler is needed for use by a new scene. Can be set prior to navigation and will be
     * automatically recognized by the system upon calling navigationTo.
     * @param handler the handler to be explicitly specified for the new scene
     */
    public void setInputHandler(IRamAbstractSceneHandler handler) {
        inputHandler = handler;
    }
    
    public CurrentMode getCurrentMode() {
        return instance.currentMode;
    }
    
    /**
     * Should define whether the new scene should be an edit or select mode version of the given model.
     * @param mode
     */
    public void setCurrentMode(CurrentMode mode) {
        instance.currentMode = mode;
    }
    
    public PropagationResult getInputPropResult() {
        return instance.inputPropResult;
    }
    public void setInputPropResult(PropagationResult pr) {
        instance.inputPropResult = pr;
    }
    
    public EObject getModelFromScene(Iscene scene) {
        return sceneToModel.get(scene);
    }
    
    public COREArtefact getCurrentArtefact() {
        return currentArtefact;
    }
    
    public Iscene getSceneFromModel(EObject inputObj) {
        for (Map.Entry<Iscene, EObject> entry : sceneToModel.entrySet()) {
            if (inputObj.equals(entry.getValue())) {
                return entry.getKey();
            }
        }
        return null;
    }
    
    public void connectSceneToFeature(COREScene model, COREFeature feature) {
        sceneToFeature.put(model, feature);
    }
    
    public Map<COREScene, COREFeature> getSceneToFeature() {
        return sceneToFeature;
    }
    
    /**
     * The function loads the concern passed and registers the scene and changes it accordingly.
     *
     * @param concern - the concern to display
     */
    public void displayConcernFeatureModelEditScene(final COREConcern concern) {
        RamApp.getApplication().invokeLater(new Runnable() {
                @Override
                public void run() {
                    IConcernEditSceneHandler handler = HandlerFactory.INSTANCE.getDisplayConcernFMSceneHandler();
                    DisplayConcernEditScene scene = null;
                    scene = new DisplayConcernEditScene(RamApp.getApplication(), concern);
                    scene.setHandler(handler);
                    sceneToModel.put(scene, concern);
                    RamApp.getApplication().addScene(scene);
                    changeSceneAndUpdate(scene);
                }
        });
    }
    
    /**
     * This function loads the module of the Feature Model Select.
     *
     * @param concern - The concern which is being reused.
     * @param handler - The handler to use.
     * @param artefact - The artefact that is making the reuse
     * @param extendingReuse - If we are extending a reuse. null if not
     */
    public void  displayFeatureModelSelectScene(final COREConcern concern, 
            final IConcernSelectSceneHandler handler, final COREArtefact artefact, COREReuse extendingReuse) {
        RamApp.getApplication().invokeLater(new Runnable() {
            @Override
            public void run() {
                DisplayConcernSelectScene scene = new DisplayConcernSelectScene(RamApp.getApplication(), concern,
                        (COREExternalArtefact) artefact, extendingReuse);
                sceneToModel.put(scene, concern);
                scene.setHandler(handler);
                RamApp.getApplication().addScene(scene);
                changeSceneAndUpdate(scene);
            }
        });
    }
    
    /**
     * This function loads the module of the Feature Model Select and sets the associated reuse parameter.
     *
     * @param concern - The concern which is being reused.
     * @param handler - The handler to use.
     * @param artefact - The artefact that is making the reuse
     * @param associatedReuse - the associated reuse to the concern
     */
    public void displayFeatureModelSelectSceneAndSetReuse(final COREConcern concern, 
            final IConcernSelectSceneHandler handler, final COREArtefact artefact, COREReuse associatedReuse) {
        RamApp.getApplication().invokeLater(new Runnable() {
            @Override
            public void run() {
                DisplayConcernSelectScene scene = new DisplayConcernSelectScene(RamApp.getApplication(), concern,
                        (COREExternalArtefact) artefact, null, associatedReuse);
                sceneToModel.put(scene, concern);
                scene.setHandler(handler);
                RamApp.getApplication().addScene(scene);
                changeSceneAndUpdate(scene);
            }
        });
    }
    
    public void showImpactModel(COREImpactNode rootNode, COREImpactModel impactModel) {
        
        COREConcern concern = impactModel.getCoreConcern();
        final String name = concern.getName();
    
        LayoutContainerMapImpl mapLayout = EMFModelUtil.getEntryFromMap(impactModel.getLayouts(), rootNode);
        if (mapLayout == null) {
            ImpactModelElementController controller = COREControllerFactory.INSTANCE.getImpactModelElementController();
    
            if (rootNode == null) {
                controller.createRootImpactModelElement(
                        impactModel,
                        ImpactModelUtil.getUniqueGoalName(impactModel), RamApp.getApplication().getWidth() / 2,
                        GUIConstants.ROOT_GOAL_INITIAL_HEIGHT);
            } else {
                controller.createLayoutContainer(rootNode, RamApp.getApplication().getWidth() / 2, 
                        GUIConstants.ROOT_GOAL_INITIAL_HEIGHT);
            }
        } else {
            displayEditImpactModel(concern, impactModel, name, rootNode);
        }
    }
    
    public void displayEditImpactModel(COREConcern concern, COREImpactModel impactModel, String name,
            COREImpactNode rootNode) {
        DisplayImpactModelEditScene scene =
                new DisplayImpactModelEditScene(RamApp.getApplication(), name, concern, impactModel, rootNode);
    
        scene.setTransition(new SlideTransition(RamApp.getApplication(), 700, true));
        // TODO HandlerFactory
        scene.setHandler(HandlerFactory.INSTANCE.getDefaultRamSceneHandler());
        sceneToModel.put(scene, rootNode);
        RamApp.getApplication().addScene(scene);
        changeSceneAndUpdate(scene);
    }
    
    
    /**
     * Display the impact model. I am told that the propagation result field is not currently in use by the system and 
     * that i can therefore input null here for the time being.
     *
     * @param concern - The concern which is being reused.
     * @param rootNode the root node to display
     * @param propagationResult The result of the evaluation. It associates impact model elements with their weight
     */
    public void showImpactModelSelectMode(final COREConcern concern, COREImpactNode rootNode,
            final PropagationResult propagationResult) {

        final COREImpactModel impactModel = concern.getImpactModel();
        final String name = concern.getName();
    
        LayoutContainerMapImpl mapLayout = EMFModelUtil.getEntryFromMap(impactModel.getLayouts(), rootNode);
        if (mapLayout == null) {
            // Register a listener so that the goal can be displayed
            // when it was successfully added.
            impactModel.eAdapters().add(new EContentAdapter() {
                @Override
                public void notifyChanged(Notification notification) {
                    if (notification.getFeature() == CorePackage.Literals.CORE_IMPACT_MODEL__LAYOUTS) {
                        final LayoutContainerMapImpl containerMapImpl;
                        switch (notification.getEventType()) {
                            case Notification.ADD:
                                impactModel.eAdapters().remove(this);
                                containerMapImpl = (LayoutContainerMapImpl) notification.getNewValue();
    
                                containerMapImpl.eAdapters().add(new EContentAdapter() {
                                    @Override
                                    public void notifyChanged(Notification notif) {
                                        if (notif.getFeature() == CorePackage.Literals.LAYOUT_CONTAINER_MAP__VALUE) {
                                            switch (notif.getEventType()) {
                                                case Notification.ADD:
                                                    containerMapImpl.eAdapters().remove(this);
                                                    Entry<?, ?> entry = (Entry<?, ?>) notif.getNewValue();
    
                                                    displaySelectImpactModel(concern, impactModel, name,
                                                            (COREImpactNode) entry.getKey(), propagationResult);
                                            }
                                        }
                                    }
                                });
                        }
                    }
                }
            });
    
            COREControllerFactory.INSTANCE.getImpactModelElementController().createLayoutContainer(rootNode, 
                    RamApp.getApplication().getWidth() / 2, GUIConstants.ROOT_GOAL_INITIAL_HEIGHT);
        } else {
            displaySelectImpactModel(concern, impactModel, name, rootNode, propagationResult);
        }
    }
    
    
    /**
     * Operation called to display the Impact Model.
     *
     * @param name - The name of the scene.
     * @param concern - The concern name.
     * @param impactModel - The Impact Model.
     * @param rootNode the root node of this scene
     * @param propagationResult The result of the evaluation. It associates impact model elements with their weight
     */
    public void displaySelectImpactModel(COREConcern concern, COREImpactModel impactModel, String name,
            COREImpactNode rootNode, PropagationResult propagationResult) {
        DisplayImpactModelSelectScene scene =
                new DisplayImpactModelSelectScene(RamApp.getApplication(), name, impactModel, rootNode,
                        propagationResult);
        sceneToModel.put(scene, rootNode);
        scene.setTransition(new SlideTransition(RamApp.getApplication(), 700, true));
    
        RamApp.getApplication().addScene(scene);
        changeSceneAndUpdate(scene);
    }
    
    /**
     * This function gets the scene of an aspect from loaded aspects but if that aspect has not been loaded before it
     * creates one scene with that aspect. If there is an error during initialization, null is returned, which the
     * caller should check for.
     *
     * @param model the model to load a scene for
     * @param pushToNavBar should be set to true if a nav bar section should be added for the new scene
     * @return the loaded {@link DisplayAspectScene}, null if there was an error during initialization
     */
    public RamAbstractScene<IDisplaySceneHandler> getExistingOrCreateModelScene(EObject model, boolean pushToNavBar) {
        RamApp.getApplication().registerModel(model);
        String modelSceneName = getSceneName(model);
        RamAbstractScene<IDisplaySceneHandler> scene;
        
        scene = (RamAbstractScene<IDisplaySceneHandler>) RamApp.getApplication().getScene(modelSceneName);
        if (scene != null) {
            return scene;
        }
        return getExistingOrCreateModelScene(COREArtefactUtil.getReferencingExternalArtefact(model), model, 
                pushToNavBar);
    }
    
    /**
     * This function gets the scene of an aspect from loaded aspects but if that aspect has not been loaded before it
     * creates one scene with that aspect. If there is an error during initialization, null is returned, which the
     * caller should check for.
     *
     * @param artefact the artefact related to the aspect 
     * @param model the aspect to load a scene for
     * @param pushToNavBar should be set to true if a nav bar section should be added for the new scene
     * @return the loaded {@link DisplayAspectScene}, null if there was an error during initialization
     */
    public RamAbstractScene<IDisplaySceneHandler> getExistingOrCreateModelScene(COREExternalArtefact artefact,
            EObject model, boolean pushToNavBar) {
        synchronized (RamApp.getApplication().getLoadedModels()) {
            RamApp.getApplication().registerModel(model);
            String modelSceneName = getSceneName(model);
            RamAbstractScene<IDisplaySceneHandler> scene = null;
    
            if (scene == null) {
                IDisplaySceneHandler handler;
                try {
    
                    if (model instanceof Aspect) {
                        handler = HandlerFactory.INSTANCE.getDisplayAspectSceneHandler();
                        scene = new DisplayAspectScene(RamApp.getApplication(), artefact, (Aspect) model, 
                                modelSceneName);
                        sceneToModel.put(scene, model);
                        ((DisplayAspectScene) scene).setHandler(handler);
                        if (pushToNavBar) {
                            ((DisplayAspectScene) scene).repushSections();
                        }
                    } else if (model instanceof ClassDiagram) {
                        handler = ClassDiagramHandlerFactory.INSTANCE.getDisplayClassDiagramSceneHandler();
                        scene = new DisplayClassDiagramScene(RamApp.getApplication(), artefact, 
                                (ClassDiagram) model, modelSceneName);
                        sceneToModel.put(scene, model);
                        if (pushToNavBar) {
                            ((DisplayClassDiagramScene) scene).repushSections();
                        }
                    } else if (model instanceof UseCaseModel) {
                        handler = UseCaseModelHandlerFactory.INSTANCE.getUseCaseDiagramDisplaySceneHandler();
                        scene = new DisplayUseCaseModelScene(RamApp.getApplication(), artefact,
                          (UseCaseModel) model, modelSceneName);
                        sceneToModel.put(scene, model);
                        if (pushToNavBar) {
                            ((DisplayUseCaseModelScene) scene).repushSections();
                        }
                    } else if (model instanceof RestIF) {
                        handler = RestTreeHandlerFactory.INSTANCE.getDisplayRestTreeSceneHandler();
                        scene = new DisplayRestTreeScene(RamApp.getApplication(), artefact, (RestIF) model,
                                modelSceneName);
                        sceneToModel.put(scene, model);
                        if (pushToNavBar) {
                            ((DisplayRestTreeScene) scene).repushSections();
                        }
                    } else if (model instanceof EnvironmentModel) {
                        handler = EmHandlerFactory.INSTANCE.getCommunicationDiagramDisplaySceneHandler();
                        scene = new DisplayEnvironmentModelScene(RamApp.getApplication(), artefact,
                                (EnvironmentModel) model, modelSceneName);
                        sceneToModel.put(scene, model);
                        if (pushToNavBar) {
                            ((DisplayEnvironmentModelScene) scene).repushSections();
                        }
                    }
                    RamApp.getApplication().addScene(scene);
    
                    // CHECKSTYLE:IGNORE IllegalCatch: There could be many different exceptions during initialization.
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
    
            return scene;
        }
    }
    
    /**
     * Loads the scene of a given model. If a scene for the given model does not exist yet it
     * will be created.
     *
     * @param artefact the artefact of the model to be loaded
     * @param model the model to display
     */
    public void loadScene(COREExternalArtefact artefact, final EObject model) {
        loadScene(artefact, model, null);
    }
    
    /**
     * Loads the scene of a given model. If a scene for the given model does not exist yet it
     * will be created.
     *
     * @param artefact the artefact of the model to be loaded
     * @param model - the aspect to display
     * @param handler - the handler to set for the scene. If null, handler of the scene will not be modified.
     */
    public void loadScene(final COREExternalArtefact artefact, final EObject model,
            final IDisplaySceneHandler handler) {
    
        RamApp.getApplication().invokeLater(new Runnable() {
            @Override
            public void run() {
                RamAbstractScene<IDisplaySceneHandler> scene = getExistingOrCreateModelScene(artefact, model, true);
                sceneToModel.put(scene, model);
                if (handler != null) {
                    scene.setHandler(handler);
                }
                if (scene != null) {
                    SceneCreationAndChangeFactory.getFactory().changeSceneAndUpdate(scene);
                } else {
                    RamApp.getApplication().getActiveScene().displayPopup(Strings.popupAspectMalformed(model
                            .toString()), PopupType.ERROR);
                }
            }
        });
    }
    
    
    /**
     * Creates a unique name for a scene which displays an aspect.
     *
     * @param model the aspect that the scene should display
     * @return the unique name of the aspect scene
     */
    private static String getSceneName(EObject model) {
        String modelString = model.toString();
        // The string contains the name of the aspect. Since an aspect could be renamed,
        // the scene name has to be independent of the name of an aspect.
        // Just strip that part of and use the identify of the object.
        modelString = modelString.substring(0, modelString.indexOf(' '));
    
        return modelString;
    }
    
    /**
     * The goal of this is to create a single point of entry where the current scene can be changed so as to allow 
     * various other methods easy access to information concerning what scene is currently active.
     * @param sceneToChange scene which will be changed to.
     */
    public void changeSceneAndUpdate(Iscene sceneToChange) {
        MTComponent [] sceneChildren = sceneToChange.getCanvas().getChildren();
        boolean sceneHasNavBar = Arrays.asList(sceneChildren).contains(navbar);
        if (!sceneHasNavBar) {
            sceneToChange.getCanvas().addChild(navbar);
        }
        RamApp.getApplication().changeScene(sceneToChange);
        if (sceneToModel.containsKey(sceneToChange)) {
            EObject sceneBaseObject = sceneToModel.get(sceneToChange);
            navbar.setCurrentModel(sceneBaseObject);
            currentArtefact = COREArtefactUtil.getReferencingExternalArtefact(sceneBaseObject);
            if (currentArtefact != null) {
                COREScene coreScene = currentArtefact.getScene();
                navbar.setCurrentScene(coreScene);
                if (coreScene != null) {
                    COREPerspective p = COREPerspectiveUtil.INSTANCE.getPerspective(coreScene.getPerspectiveName());
                    if (navbar.getCurrentPerspective() == null || !navbar.getCurrentPerspective().equals(p)) {
                        navbar.setCurrentPerspective(p);
                    }  
                }
            }
            if (!navbar.checkForSectionWithObject(sceneBaseObject)) {
                if (sceneToChange instanceof DisplayConcernEditScene) {
                    navbar.pushSection(((COREConcern) sceneBaseObject).getName(), navbar.getGenericNamerBase(), 
                            (COREConcern) sceneBaseObject);
                } else if (sceneToChange instanceof DisplayImpactModelEditScene) {
                    COREImpactNode rootNode = (COREImpactNode) sceneBaseObject;
                    COREImpactModel im = (COREImpactModel) rootNode.eContainer();
                    navbar.pushSectionImpact(Icons.ICON_MENU_NEW_ROOT_GOAL, im.getName(), navbar.getGenericNamerBase(), 
                            im);
                } else if (sceneToChange instanceof DisplayAspectScene) {
                    DisplayAspectScene aspectSceneToChange = (DisplayAspectScene) sceneToChange;
                    if (!(aspectSceneToChange.getCurrentView() instanceof StructuralDiagramView)) {
                        aspectSceneToChange.switchToPreviousView();
                    }
                    aspectSceneToChange.repushSections();
                } else if (sceneToChange instanceof DisplayClassDiagramScene) {
                    DisplayClassDiagramScene cdSceneToChange = (DisplayClassDiagramScene) sceneToChange;
                    cdSceneToChange.repushSections();
                } else if (sceneToChange instanceof DisplayRestTreeScene) {
                    DisplayRestTreeScene restSceneToChange = (DisplayRestTreeScene) sceneToChange;
                    restSceneToChange.repushSections();
                } else if (sceneToChange instanceof DisplayUseCaseModelScene) {
                    DisplayUseCaseModelScene ucdSceneToChange = (DisplayUseCaseModelScene) sceneToChange;
                    ucdSceneToChange.repushSections();
                    ucdSceneToChange.switchToView(ucdSceneToChange.getUseCaseDiagramView(), false);
                }
            } else {
                if (sceneToChange instanceof DisplayUseCaseModelScene) {
                    DisplayUseCaseModelScene ucdSceneToChange = (DisplayUseCaseModelScene) sceneToChange;
                    ucdSceneToChange.switchToView(ucdSceneToChange.getUseCaseDiagramView(), false);
                }
            }
        }
        navbar.resetCurrentlySelected();
        if (eObjectToCenter != null) {
            navbar.toggleGenericSelection(eObjectToCenter, (RamAbstractScene) sceneToChange);
            eObjectToCenter = null;
        }
    }
    
    /**
     * This will be the catch all method which allows movement from one scene to the next. Send in an EObject which will
     * represent a model which we which to navigate to. In addition, a second EObject will allow the user to provide
     * extra information thus making navigation easier. Finally, using the setter methods above allows for the 
     * programmer to specify any handlers, aspects, etc. that the model will be using but default values have been set 
     * as well.
     * 
     * @param model the main model which is being navigated to
     * @param modelHelper second eobject used to provide necessary information on model. 
     * @param inputCurrentMode the mode (either edit or scene) which you will be moving into
     */
    public void navigateToModel(EObject model, EObject modelHelper, CurrentMode inputCurrentMode) {
        if (model instanceof COREConcern) {
            currentMode = CurrentMode.SELECT;
            IConcernSelectSceneHandler currentHandler = (IConcernSelectSceneHandler) inputHandler;
            displayFeatureModelSelectScene((COREConcern) model, currentHandler, currentArtefact, null);
        } else if (modelHelper instanceof COREExternalArtefact) {
            loadScene((COREExternalArtefact) modelHelper, model, null);
        }
    }
    
    /**
     * This is used in order to navigate to a scene for the model object which is input into the method. In the case of 
     * a model, the scene is based around that model. In case of a model object, a scene is found based on the model 
     * which encompasses that model object. Different modes are used for separate versions of the scene which can appear
     * (build application, select mode, etc.)
     * @param model the model object around which the scene is found/created
     * @param inputCurrentMode the mode of the scene
     */
    public void navigateToModel(EObject model, CurrentMode inputCurrentMode) {
        if (model instanceof COREConcern) {
            if (inputCurrentMode == CurrentMode.EDIT) {
                currentMode = CurrentMode.EDIT;
                displayConcernFeatureModelEditScene((COREConcern) model);
                navbar.pushSection(((COREConcern) model).getName(), navbar.getGenericNamerBase(), (COREConcern) model);
            } else if (inputCurrentMode == CurrentMode.BUILD) {
                currentMode = CurrentMode.BUILD;
                IConcernSelectSceneHandler handler = HandlerFactory.INSTANCE.getBuildApplicationSceneHandler();
                displayFeatureModelSelectScene((COREConcern) model, handler, null, null);
            } else if (inputCurrentMode == CurrentMode.SELECT) {
                currentMode = CurrentMode.SELECT;
                IConcernSelectSceneHandler handler = HandlerFactory.INSTANCE.getDisplayFMSceneSelectHandler();
                COREArtefact artefact = currentArtefact;
                displayFeatureModelSelectScene((COREConcern) model, handler, artefact, null);
            } else {
                currentMode = CurrentMode.ASSOCIATION;
                
            }
            numberTried = 0;
        } else if (model instanceof COREImpactNode) {
            if (inputCurrentMode == CurrentMode.EDIT) {
                COREImpactNode rootNode = (COREImpactNode) model;
                COREImpactModel impactModel = (COREImpactModel) rootNode.eContainer();
                showImpactModel(rootNode, impactModel);
                navbar.pushSectionImpact(Icons.ICON_MENU_NEW_ROOT_GOAL, impactModel.getName(), navbar
                        .getGenericNamerBase(), impactModel);
            } else if (inputCurrentMode == CurrentMode.SELECT) {
                COREImpactNode rootNode = (COREImpactNode) model;
                COREImpactModel impactModel = (COREImpactModel) rootNode.eContainer();
                COREConcern concern = impactModel.getCoreConcern();
                showImpactModelSelectMode(concern, rootNode, inputPropResult);
            }
            numberTried = 0;
        } else if (model instanceof Aspect) {
            if (inputCurrentMode == CurrentMode.BUILD) {
                currentMode = CurrentMode.BUILD;
                IDisplaySceneHandler handler = HandlerFactory.INSTANCE.getDisplayWovenAspectSceneHandler();
                
                COREExternalArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(model);
                currentArtefact = artefact;
                
                loadScene(artefact, model, handler);
            } else {
                currentMode = CurrentMode.EDIT;
                COREExternalArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(model);
                currentArtefact = artefact;
                
                loadScene(artefact, model, null);
            }
            numberTried = 0;
        } else if (model instanceof COREReuse) {
            if (inputCurrentMode == CurrentMode.SELECT) {
                currentMode = CurrentMode.SELECT;
                ConcernSelectSceneHandler selectHandler = new ConcernSelectSceneHandler();
                COREConcern concern = ((COREReuse) model).getReusedConcern();
                displayFeatureModelSelectScene(concern, selectHandler, currentArtefact, (COREReuse) model);
            }
            numberTried = 0;
        } else if (model instanceof UseCaseModel) {
            COREExternalArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(model);
            loadScene(artefact, model);
            numberTried = 0;
        } else if (model instanceof ClassDiagram) {
            COREExternalArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(model);
            loadScene(artefact, model);
            numberTried = 0;
        } else if (model instanceof RestIF) {
            COREExternalArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(model);
            loadScene(artefact, model);
            numberTried = 0;
        } else if (model instanceof EnvironmentModel) {
            COREExternalArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(model);
            loadScene(artefact, model);
            numberTried = 0;
        } else {
            COREExternalArtefact extArtefact = COREArtefactUtil.getReferencingExternalArtefact(model);
            EObject rootObject = extArtefact.getRootModelElement();
            if (numberTried > 1) {
                eObjectToCenter = null;
                return;
            }
            numberTried++;
            eObjectToCenter = model;
            navigateToModel(rootObject, inputCurrentMode);
        }
    }
    
    /**
     * This is used to navigate to the {@link DisplayConcernSelectScene} and set the {@link COREReuse} as a parameter.
     * This method is used when the user wants to reselect the configuration for a reuse.
     * 
     * @author Bowen
     * 
     * @param model the core concern model which the scene is created
     * @param reuse
     */
    public void navigateToConcernSelectSceneAndSetReuse(COREConcern model, COREReuse reuse) {
        IConcernSelectSceneHandler handler = HandlerFactory.INSTANCE.getDisplayFMSceneSelectHandler();
        COREArtefact artefact = currentArtefact;
        displayFeatureModelSelectSceneAndSetReuse(model, handler, artefact, reuse);
        
        numberTried = 0;
    }
    
    /**
     * This method should be used for the situation in which a single language contains multiple views which can be 
     * switched between when working in the lanugage. What I like to call multi-model intra-language navigation.
     * Basically, this should check to see if the object sent into the method is the basis of a given view in the 
     * model. If so, it should switch to that view and then check to see if a navbar section should be removed before
     * populating a new navbar section.
     * @param object the object which we will check to see if it is the basis of a view
     * @author ian 
     */
    public void switchViewAndUpdateNavbar(EObject object) {
        RamAbstractScene curScene = RamApp.getActiveScene();
        
        Map<MTComponent, EObject> viewToObject = curScene.getViewToObject();
        
        MTComponent newView = null;
        
        for (Map.Entry<MTComponent, EObject> entry : viewToObject.entrySet()) {
            if (object.equals(entry.getValue())) {
                newView = entry.getKey();
            }
        }
        
        if (newView == null) {
            return;
        }        
       
        if (curScene.getCurrentView().equals(newView)) {
            return;
        }
        
        if (curScene instanceof DisplayAspectScene) {
            ((DisplayAspectScene) curScene).switchToView(newView);
        } else if (curScene instanceof DisplayUseCaseModelScene) {
            ((DisplayUseCaseModelScene) curScene).switchToView(newView, true);
        }
        
        boolean increaseDepth = false;
        
        IntraLanguageMappingTree tree = NavigationMappingHelper.getInstance().getTreeForObject(sceneToModel.get(
                curScene));
        if (tree != null) {
            List<ILMNode> nodes = tree.getAllNodes();
            for (ILMNode node : nodes) {
                if (node.getNodeModelElement().equals(object)) {
                    IntraLanguageMapping ilm = (IntraLanguageMapping) node.getILM();
                    if (ilm.isIncreaseDepth()) {
                        String modelName = EMFModelUtil.getNameAttribute(object);
                        if (modelName == null) {
                            modelName = object.toString();
                        }
                        navbar.pushSection(Icons.ICON_NAVIGATION_ASPECT, modelName, navbar.getGenericNamerBase(),
                                object);
                        increaseDepth = true;
                    }
                }
            }
        }
        
        if (!increaseDepth) {
            navbar.popSection();
            String modelName = EMFModelUtil.getNameAttribute(object);
            if (modelName == null) {
                modelName = object.toString();
            }
            navbar.pushSection(Icons.ICON_NAVIGATION_ASPECT, modelName, navbar.getGenericNamerBase(), object);
        }
    }
    
}
