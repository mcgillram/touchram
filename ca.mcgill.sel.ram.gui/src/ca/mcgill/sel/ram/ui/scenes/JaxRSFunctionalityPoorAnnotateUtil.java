package ca.mcgill.sel.ram.ui.scenes;

import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREModelElementMapping;
import ca.mcgill.sel.ram.Annotation;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.Operation;
import ca.mcgill.sel.ram.Parameter;
import ca.mcgill.sel.ram.StructuralView;
import ca.mcgill.sel.ram.impl.AspectImpl;
import ca.mcgill.sel.ram.impl.OperationImpl;
import ca.mcgill.sel.ram.impl.ParameterImpl;
import ca.mcgill.sel.ram.impl.RamFactoryImpl;
import ca.mcgill.sel.restif.AccessMethod;
import ca.mcgill.sel.restif.DynamicFragment;
import ca.mcgill.sel.restif.MethodType;
import ca.mcgill.sel.restif.PathFragment;
import ca.mcgill.sel.restif.Resource;
import ca.mcgill.sel.restif.RestIF;
import ca.mcgill.sel.restif.impl.DynamicFragmentImpl;
import ca.mcgill.sel.restif.impl.StaticFragmentImpl;

public class JaxRSFunctionalityPoorAnnotateUtil extends AbstractFunctionalityPoorAnnotateUtil {
    
    /**
     * Jax-RS import stub.
     */
    private static final String JAX_RS_IMPORT_STUB = "javax.ws.rs";
    /**
     * Jax-RS import name.
     */
    private static final String JAX_RS_IMPORT_NAME = "javax ws rs";
    
    /**
     * Jax-RS core import stub.
     */
    private static final String JAX_RS_CORE_IMPORT_STUB = "javax.ws.rs.core";
    /**
     * Jax-RS core import name.
     */
    private static final String JAX_RS_CORE_IMPORT_NAME = "javax ws rs core";
    
    @Override
    public void addAnnotationSupport(COREExternalArtefact fpcdm) {
        // Extract the structural view that houses all artifacts and imports.
        Aspect fpcdmAspect = (AspectImpl) fpcdm.getRootModelElement();
        StructuralView fpcdmStrcuturalView = fpcdmAspect.getStructuralView();
        
        // Add all required annotation imports to Ram-Model: (annotations may share import-stubs)
        // Support for "@Get", "@Put", "@Post", "@Delete", "@Path", "ApplicationPath", "@Produces", "@Consumes"
        addImport(null, JAX_RS_IMPORT_STUB, JAX_RS_IMPORT_NAME, fpcdmStrcuturalView);
        
        // Support for Application imports
        addImport(null, JAX_RS_CORE_IMPORT_STUB, JAX_RS_CORE_IMPORT_NAME, fpcdmStrcuturalView);

        // TODO: verify if further import-stubs are required for parameter-
        // mappings / bodies / payload-encodings
    }
        
    /**
     * Adds spring specific annotations to a fpcdm, based on a provided rif-ram.
     * mapping
     * 
     * @param fpcdm RAM model to decorate with annotations
     * @param rifRamConcern concern containing rif and ram model concerns
     */
    @Override
    public void addAnnotations(COREExternalArtefact fpcdm, COREConcern rifRamConcern) {
        
        // Decorate application class

        // 1: Create ApplicationPath annotation
        Annotation jaxRSApplicationAnnotation = RamFactoryImpl.init().createAnnotation();
        jaxRSApplicationAnnotation.setImport(identifyImportByName(fpcdm, JAX_RS_IMPORT_NAME));
        jaxRSApplicationAnnotation.setMatter("ApplicationPath");
        jaxRSApplicationAnnotation.getParameter().add("\"/\"");

        //TODO:
        // 2: Decorate launcher-class with this annotation:

        // Decorate all controller classes (not yet including operations)
        decorateJaxRSControllers(fpcdm);

        // Decorate all exposed controller methods with the _correct_ mapping
        // type and URL as annotation parameter
        decorateExposedControllerOperations(fpcdm, rifRamConcern);

        return;
    }
    
    /**
     * Adds the static Path annotation to all dedicated FPCDM controllers.
     * 
     * @param fpcdm as the RAM model to decorate.
     */
    public void decorateJaxRSControllers(COREExternalArtefact fpcdm) {

        // Extract the structural view that houses all controllers.
        Aspect fpcdmAspect = (AspectImpl) fpcdm.getRootModelElement();
        StructuralView fpcdmStrcuturalView = fpcdmAspect.getStructuralView();

        // Iterate over all classifiers. Decorate if the name contains the
        // keyword "Controller"
        for (Classifier classifier : fpcdmStrcuturalView.getClasses()) {

            if (classifier.getName().toLowerCase().contains("controller")) {

                // Add a new Path annotation
                Annotation restControllerAnnotation = RamFactoryImpl.init().createAnnotation();
                restControllerAnnotation.setImport(identifyImportByName(fpcdm, JAX_RS_IMPORT_NAME));
                restControllerAnnotation.setMatter("Path");
                restControllerAnnotation.getParameter().add("\"/\"");
                classifier.getAnnotation().add(restControllerAnnotation);
            }
        }
    }
    
    /**
     * Iterates over all RIF_RAM mappings and annotates the corresponding
     * controller operation with a corresponding annotations (including
     * parameters).
     * 
     * @param fpcdm as the fpcdm housing the controller operations to be
     * decorated.
     * @param rifRamConcern as the general concern, listing the RIF-RAM
     * mappings.
     */
    public void decorateExposedControllerOperations(COREExternalArtefact fpcdm, COREConcern rifRamConcern) {

        // Iterate over all RIF-RAM mappings, add the corresponding annotations
        // for signatures AND signature parameters.
        for (COREModelElementMapping mapping : TrafoUtils.getRifRamLanguageElementMappings(rifRamConcern)) {

            if (TrafoUtils.isOperationMapping(mapping)) {

                // This is an operation mapping. We must add the corresponding
                // annotation to the FP-controller class. (In front of the
                // signature in question)
                createOperationMappingAnnotation(mapping, fpcdm, rifRamConcern);
                
            } else {
                
                // This is a parameter mapping. We must add the corresponding
                // annotation to the FP-controller classes signature. (In front
                // of the parameter in question)
                createParameterMappingAnnotation(mapping, fpcdm);
            }
        }
        
        // iterate over all RIF-RAM mappings again, with all of the operation and parameter mappings complete,
        // check if there are any parameters that are not annotated, if there are, add a @Consumes(...) annotation.
        for (COREModelElementMapping mapping : TrafoUtils.getRifRamLanguageElementMappings(rifRamConcern)) {

            if (TrafoUtils.isOperationMapping(mapping)) {
                
                // This is a operation mapping. We will add a Consumes annotation
                // (in front of the operation in question)
                // if the operation in the fpcdm has any unannotated parameters.
                createOperationMappingConsumesAnnotation(mapping, fpcdm);
            }
        }
    }
    
    /**
     * Create appropriate annotations and decorate them on the associated operation
     * in the fpcdm model.
     * 
     * @param mapping operation rif-ram mapping
     * @param fpcdm as the RAM model that stores the required imports (support
     * for the annotation)
     * @param rifRamConcern concern containing rif and ram model concerns
     */
    private static void createOperationMappingAnnotation(COREModelElementMapping mapping, COREExternalArtefact fpcdm,
            COREConcern rifRamConcern) {
        // Use the RIF end to identify the REST method, use the RAM end
        // to
        // identify the BL operation whose FPCDM counterpart must be
        // decorated by an
        // annotation.
        OperationImpl mappedRamOperation = TrafoUtils.getRamOperationMappingEnd(mapping);
        AccessMethod restAccessMethod = TrafoUtils.getRestAccessMethodMappingEnd(mapping);

        // Construct appropriate annotations: method annotation, path annotation, and produce annotation
        RestIF rif = (RestIF) TrafoUtils.extractCoreArtefactByKey(rifRamConcern, TrafoUtils.RIF_KEY)
                .getRootModelElement();
        Annotation jaxRSMethodTypeAnnotation = createMethodTypeAnnotationFromAccessMethod(fpcdm, restAccessMethod);
        Annotation jaxRSPathAnnotation = createPathAnnotationFromAccessMethod(fpcdm, restAccessMethod, rif);
        Annotation jaxRSProducesAnnotation = createProducesAnnotationFromOperation(fpcdm, mappedRamOperation);
        
        // Resolve the ram operation to it's controller counterpart
        // (only controllers are decorated with annotations.)
        Operation controllerOperation = AbstractFunctionalityPoorCdmFillUtil
                .resolveToControllerOperation(mappedRamOperation, fpcdm);
        
        // Append all annotations (that exist) to the operation.
        controllerOperation.getAnnotation().add(jaxRSMethodTypeAnnotation);
        controllerOperation.getAnnotation().add(jaxRSPathAnnotation);
        if (jaxRSProducesAnnotation != null) {
            controllerOperation.getAnnotation().add(jaxRSProducesAnnotation);
        }
    }
    
    /**
     * Create appropriate annotations and decorate them on the associated parameters
     * in the fpcdm model.
     * 
     * @param mapping operation rif-ram mapping
     * @param fpcdm as the RAM model that stores the required imports (support
     * for the annotation)
     */
    private static void createParameterMappingAnnotation(COREModelElementMapping mapping, COREExternalArtefact fpcdm) {
        // Use the RIF end to identify the REST dynamic fragment, 
        // use the RAM end to identify the BL parameter whose FPCDM 
        // counterpart must be decorated by an annotation.
        DynamicFragment restDynamicFragment = TrafoUtils.getRestDynamicFragmentMappingEnd(mapping);
        ParameterImpl mappedRamParameter = TrafoUtils.getRamParameterMappingEnd(mapping);
        
        // Construct path param annotations
        Annotation jaxRSPathParamAnnotation = createPathParamAnnotationFromDynamicFragment(fpcdm, restDynamicFragment);
        
        // Resolve the ram parameter to it's controller counterpart
        // (only controllers are decorated with annotations.)
        Parameter controllerParameter = AbstractFunctionalityPoorCdmFillUtil
                .resolveToControllerParameter(mappedRamParameter, fpcdm);
        
        // Append annotation to the parameter.
        controllerParameter.getAnnotation().add(jaxRSPathParamAnnotation);
    }
    
    /**
     * Create consumes annotation and add it to the mapped RAM operation in the fpcdm if the operation has any 
     * parameters without annotations.
     * 
     * @param mapping operation rif-ram mapping
     * @param fpcdm as the RAM model that stores the required imports (support
     * for the annotation)
     */
    private static void createOperationMappingConsumesAnnotation(COREModelElementMapping mapping, 
            COREExternalArtefact fpcdm) {
        // Use the RIF end to identify the REST method, use the RAM end
        // to
        // identify the BL operation whose FPCDM counterpart must be
        // decorated by an
        // annotation.
        OperationImpl mappedRamOperation = TrafoUtils.getRamOperationMappingEnd(mapping);
        
        // Resolve the ram operation to it's controller counterpart
        // (only controllers are decorated with annotations.)
        Operation controllerOperation = AbstractFunctionalityPoorCdmFillUtil
                .resolveToControllerOperation(mappedRamOperation, fpcdm);
        
        // Check if there are any unmapped parameter from the operation (returns the first one)
        Parameter unmappedParameter = getUnmappedParameterFromOperation(controllerOperation);
        
        // Create and append Consumes annotation to the parameter if it is not null.
        if (unmappedParameter != null) {
            Annotation consumesAnnotation = createConsumesAnnotationFromOperation(fpcdm);
            
            controllerOperation.getAnnotation().add(consumesAnnotation);
        }
    }
    
    /**
     * Creates PathParam annotation and sets the formatted place holder of the
     * dynamic fragment as annotation parameter.
     * 
     * @param fpcdm as the RAM model that stores the required imports (support
     * for the annotation)
     * @param restDynamicFragment as the RIF dynamic fragment to be interpreted.
     * @return the Jax-RS annotation ready as is to insert into fpcdm (decorates
     * controller operation)
     */
    private static Annotation createPathParamAnnotationFromDynamicFragment(COREExternalArtefact fpcdm,
            DynamicFragment restDynamicFragment) {
        Annotation result = RamFactoryImpl.init().createAnnotation();
        result.setImport(identifyImportByName(fpcdm, JAX_RS_IMPORT_NAME));
        
        result.setMatter("PathParam");
        
        String mappedElement = extractedFormattedNameFromDynamicFragment(restDynamicFragment);
                
        result.getParameter().add(mappedElement);

        return result;
    }

    /**
     * Inspects the internal "type" attribute of the accessMethod. Creates
     * appropriate annotation based on whether it is a get, put, post or delete
     * method.
     * 
     * @param fpcdm as the RAM model that stores the required imports (support
     * for the annotation)
     * @param accessMethod as the RIF access method to be interpreted.
     * @return the Jax-RS annotation ready as is to insert into fpcdm (decorates
     * controller operation)
     */
    private static Annotation createMethodTypeAnnotationFromAccessMethod(COREExternalArtefact fpcdm, 
            AccessMethod accessMethod) {
        
        Annotation result = RamFactoryImpl.init().createAnnotation();
        result.setImport(identifyImportByName(fpcdm, JAX_RS_IMPORT_NAME));
        
        // Set the Annotation MATTER (the annotation itself) according
        // to access method type (G/Pu/Po/D)
        // Check against all four supported types.
        if (accessMethod.getType().equals(MethodType.GET)) {
            result.setMatter("GET");
            return result;
        }
        if (accessMethod.getType().equals(MethodType.PUT)) {
            result.setMatter("PUT");
            return result;
        }
        if (accessMethod.getType().equals(MethodType.POST)) {
            result.setMatter("POST");
            return result;
        }
        if (accessMethod.getType().equals(MethodType.DELETE)) {
            result.setMatter("DELETE");
            return result;
        }
        
        throw new TransformerException(
                "Provided access method could not be transformed to an annotation - unsupported type attribute.");
    }
    
    /**
     * Creates Path annotation and sets the correct absolute fragment path as
     * annotation parameter.
     * 
     * @param fpcdm as the RAM model that stores the required imports (support
     * for the annotation)
     * @param accessMethod as the RIF access method to be interpreted
     * @param rif as the RIF model that provides the full fragment tree
     * (including the fragment referenced by the accessMethods parent resource)
     * @return the Jax-RS annotation ready as is to insert into fpcdm (decorates
     * controller operation)
     */
    private static Annotation createPathAnnotationFromAccessMethod(COREExternalArtefact fpcdm, 
            AccessMethod accessMethod, RestIF rif) {

        Annotation result = RamFactoryImpl.init().createAnnotation();
        result.setImport(identifyImportByName(fpcdm, JAX_RS_IMPORT_NAME));
        result.setMatter("Path");

        // In either case the annotation parameter is the full fragment path of
        // the accessMethod.
        PathFragment targetFragement = ((Resource) accessMethod.eContainer()).getEndpoint();
        PathFragment root = rif.getRoot();
        String resourcePath = constructRestPath(root, targetFragement);

        // Add quotation marks around resource path
        String annotationPathParameter = QUOTATION_MARK_SYMBOL + resourcePath + QUOTATION_MARK_SYMBOL;

        // Store the fully qualified resource path as annotation parameter.
        result.getParameter().add(annotationPathParameter);
        
        return result;
    }
    
    /**
     * Inspects the return type of the operation. Creates a Produces annotation
     * if the return type is not void, otherwise return null.
     * 
     * @param fpcdm as the RAM model that stores the required imports (support
     * for the annotation)
     * @param operation the original operation that contains a rif-ram mapping
     * @return the Jax-RS annotation ready as is to insert into fpcdm (decorates
     * controller operation)
     */
    private static Annotation createProducesAnnotationFromOperation(COREExternalArtefact fpcdm, Operation operation) {
        
        if (!operation.getReturnType().getName().equals("void")) {
            Annotation result = RamFactoryImpl.init().createAnnotation();
            result.setImport(identifyImportByName(fpcdm, JAX_RS_IMPORT_NAME));
            result.setMatter("Produces");
            result.getParameter().add("\"application/json\"");
            
            return result;
        } else {
            return null;
        }
    }
    
    /**
     * Creates a Consumes annotation.
     * 
     * @param fpcdm as the RAM model that stores the required imports (support
     * for the annotation)
     * @return the Jax-RS annotation ready as is to insert into fpcdm (decorates
     * controller operation)
     */
    private static Annotation createConsumesAnnotationFromOperation(COREExternalArtefact fpcdm) {
        Annotation result = RamFactoryImpl.init().createAnnotation();
        result.setImport(identifyImportByName(fpcdm, JAX_RS_IMPORT_NAME));
        result.setMatter("Consumes");
        result.getParameter().add("\"application/json\"");
                    
        return result;
    }
    
    /**
     * Walks upward the model element hierarchy, until the root resource is
     * reached. Constructs absolute resource path while walking upward by
     * appending parent resource names in front of the current path string. This
     * method is recursive. It delegates construction of the parent's absolute
     * path until the root resource is reached.
     *
     * @param root as the top node in the resource tree.
     * @param currentFragment as the resource fragment for which the absolute
     * path must be determined.
     * 
     * @return absolute rest path of a target fragment, using root as origin.
     */
    private static String constructRestPath(PathFragment root, PathFragment currentFragment) {

        // recursion end criteria: targetFragment is identical to root fragment
        if (currentFragment == root) {
            return getIsolatedFragmentLocator(root);
        }

        // recursion step: concatenate own fragment locator to result of
        // recursive call to parent.
        PathFragment parent = (PathFragment) currentFragment.eContainer();
        return constructRestPath(root, parent) + getIsolatedFragmentLocator(currentFragment);
    }

    /**
     * Resolves a resource to the associated fragment locator (isolated),
     * including leading "/" and enclosing "{}" (the latter in case of a dynamic
     * fragment).
     * 
     * @param fragment that must be converted into an annotation-parameter alike
     * string representation.
     * @return isolated fragment string representation.
     */
    private static String getIsolatedFragmentLocator(PathFragment fragment) {

        // Dynamic fragments have enclosing "{...}" and a leading "/"
        if (fragment.getClass().equals(DynamicFragmentImpl.class)) {
            DynamicFragmentImpl dynamicFragment = (DynamicFragmentImpl) fragment;
            return dynamicFragment.getPlaceholder();
        }

        // Static fragments have only a leading "/"
        if (fragment.getClass().equals(StaticFragmentImpl.class)) {
            StaticFragmentImpl staticFragment = (StaticFragmentImpl) fragment;
            return staticFragment.getInternalname();
        } else {
            throw new TransformerException(
                    "The provided resource does not reference a valid dynamic or static fragemnt.");
        }
    }

    @Override
    public Classifier extractLauncherClass(StructuralView structuralView) {
        // TODO Auto-generated method stub
        return null;
    }
}