package ca.mcgill.sel.ram.ui.scenes;

import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.Operation;
import ca.mcgill.sel.ram.Parameter;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREModelElementMapping;
import ca.mcgill.sel.ram.Annotation;
import ca.mcgill.sel.ram.ArtifactSignature;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.StructuralView;
import ca.mcgill.sel.ram.impl.AspectImpl;
import ca.mcgill.sel.ram.impl.OperationImpl;
import ca.mcgill.sel.ram.impl.ParameterImpl;
import ca.mcgill.sel.ram.impl.RamFactoryImpl;
import ca.mcgill.sel.restif.AccessMethod;
import ca.mcgill.sel.restif.DynamicFragment;
import ca.mcgill.sel.restif.MethodType;
import ca.mcgill.sel.restif.PathFragment;
import ca.mcgill.sel.restif.Resource;
import ca.mcgill.sel.restif.RestIF;
import ca.mcgill.sel.restif.impl.DynamicFragmentImpl;
import ca.mcgill.sel.restif.impl.StaticFragmentImpl;

/**
 * Utility class for all operations related decorating an FPCDM-Artifact with
 * spring boot specific annotations.
 * 
 * @author Maximilian Schiedermeier
 *
 */
public class SpringBootFunctionalityPoorAnnotateUtil extends AbstractFunctionalityPoorAnnotateUtil {

    /**
     * Just a default ctr...
     */
    public SpringBootFunctionalityPoorAnnotateUtil() {
    }

    /**
     * Prepares structural EObjects required for spring boot specific
     * annotations and adds them to the fpcdm's structural view. The added
     * EObjects are specifically: Imports satisfied by the artifact, that 
     * are required for spring-boot specific annotations.
     * 
     * @param fpcdm
     */
    @Override
    public void addAnnotationSupport(COREExternalArtefact fpcdm) {

        // Extract the structural view that houses all artifacts and imports.
        Aspect fpcdmAspect = (AspectImpl) fpcdm.getRootModelElement();
        StructuralView fpcdmStrcuturalView = fpcdmAspect.getStructuralView();

        // Add all required annotation imports to Ram-Model: (annotations may
        // share
        // import-stubs)
        // Support for "@Get/@Put/@Post/@Delete-Mapping", "@RestController"
        addImport(null, "org.springframework.web.bind.annotation",
                "org springframework web bind annotation", fpcdmStrcuturalView); // ,

        // Support for "@SpringBootApplication"
        addImport(null, "org.springframework.boot.autoconfigure",
                "org springframework boot autoconfigure", fpcdmStrcuturalView);

        // TODO: verify if further import-stubs are required for parameter-
        // mappings / bodies / payload-encodings
    }

    /**
     * Adds spring specific annotations to a fpcdm, based on a provided rif-ram
     * mapping
     * 
     * @param fpcdm as the RAM model to decorate with annotations
     * @param Concern
     */
    @Override
    public void addAnnotations(COREExternalArtefact fpcdm, COREConcern rifRamConcern) {

        // commented out since we are using generating the launcher class as a static class with Acceleo now
        
//        // Decorate launcher class
//
//        // 1: Create correctly wired "SpringBootApplication" annotation
//        Annotation springBootApplicationAnnotation = RamFactoryImpl.init().createAnnotation();
//        springBootApplicationAnnotation
//                .setImport(identifyImportByName(fpcdm, "org springframework boot autoconfigure"));
//        springBootApplicationAnnotation.setMatter("SpringBootApplication");
//
//        // 2: Decorate launcher-class with this annotation:
//        Aspect aspect = (Aspect) fpcdm.getRootModelElement();
//        Classifier launcher = extractLauncherClass(aspect.getStructuralView());
//        launcher.getAnnotation().add(springBootApplicationAnnotation);

        // Decorate all controller classes (not yet including operations)
        decorateSpringControllers(fpcdm);

        // Decorate all exposed controller methods with the _correct_ mapping
        // type and URL as annotation parameter
        decorateExposedControllerOperations(fpcdm, rifRamConcern);

        return;
    }

    /**
     * Adds the static "@RestController" annotation to all dedicated FPCDM
     * controllers.
     * 
     * @param fpcdm as the RAM model to decorate.
     */
    private static void decorateSpringControllers(COREExternalArtefact fpcdm) {

        // Extract the structural view that houses all controllers.
        Aspect fpcdmAspect = (AspectImpl) fpcdm.getRootModelElement();
        StructuralView fpcdmStrcuturalView = fpcdmAspect.getStructuralView();

        // Iterate over all classifiers. Decorate if the name contains the
        // keyword "Controller"
        for (Classifier classifier : fpcdmStrcuturalView.getClasses()) {

            if (classifier.getName().toLowerCase().contains("controller")) {

                // Add a new "@RestController" annotation
                Annotation restControllerAnnotation = RamFactoryImpl.init().createAnnotation();
                restControllerAnnotation
                        .setImport(identifyImportByName(fpcdm, "org springframework web bind annotation"));
                restControllerAnnotation.setMatter("RestController");
                classifier.getAnnotation().add(restControllerAnnotation);
                
                // Add a new @CrossOrigin annotation (supports API calls from local HTTP/JS sample app)
                Annotation corsAnnotation = RamFactoryImpl.init().createAnnotation();
                corsAnnotation
                        .setImport(identifyImportByName(fpcdm, "org springframework web bind annotation"));
                corsAnnotation.setMatter("CrossOrigin");
                classifier.getAnnotation().add(corsAnnotation);
            }
        }
    }

    /**
     * Iterates over all RIF_RAM mappings and annotates the corresponding
     * controller operation with a corresponding annotations (including
     * parameters).
     * 
     * @param fpcdm as the fpcdm housing the controller operations to be
     * decorated.
     * @param rifRamConcern as the general concern, listing the RIF-RAM
     * mappings.
     */
    private static void decorateExposedControllerOperations(COREExternalArtefact fpcdm, COREConcern rifRamConcern) {

        // Iterate over all RIF-RAM mappings, add the corresponding annotations
        // for signatures AND signature parameters.
        for (COREModelElementMapping mapping : TrafoUtils.getRifRamLanguageElementMappings(rifRamConcern)) {

            if (TrafoUtils.isOperationMapping(mapping)) {

                // This is an operation mapping. We must add the corresponding
                // annotation to the FP-controller class. (In front of the
                // signature in question)
                createOperationMappingAnnotation(mapping, fpcdm, rifRamConcern);
            } else {
                // This is a parameter mapping. We must add the corresponding
                // annotation to the FP-controller classes signature. (In front
                // of the parameter in question)
                createPathVariableParameterMappingAnnotation(mapping, fpcdm);
            }
        }
        
        // iterate over all RIF-RAM mappings again, with all of the operation and parameter mappings complete,
        // check if there are any parameters that are not annotated, if there are, add a @RequestBody(...) annotation.
        for (COREModelElementMapping mapping : TrafoUtils.getRifRamLanguageElementMappings(rifRamConcern)) {

            if (TrafoUtils.isOperationMapping(mapping)) {
                
                // This is a parameter mapping. We will add a RequestBody annotation
                // (in front of the parameter in question)
                // if the operation in the fpcdm has any unannotated parameters.
                createRequestBodyParameterMappingAnnotation(mapping, fpcdm);
            }
        }
    }

    /**
     * Create appropriate annotations and decorate them on the associated operation
     * in the fpcdm model.
     * 
     * @param mapping operation rif-ram mapping
     * @param fpcdm as the RAM model that stores the required imports (support
     * for the annotation)
     * @param rifRamConcern concern containing rif and ram model concerns
     */
    private static void createOperationMappingAnnotation(COREModelElementMapping mapping, COREExternalArtefact fpcdm,
            COREConcern rifRamConcern) {
        // Use the RIF end to identify the REST method, use the RAM end
        // to
        // identify the BL operation whose FPCDM counterpart must be
        // decorated by an
        // annotation.
        OperationImpl mappedRamOperation = TrafoUtils.getRamOperationMappingEnd(mapping);
        AccessMethod restAccessMethod = TrafoUtils.getRestAccessMethodMappingEnd(mapping);

        // Construct the full annotation: Matching annotation type +
        // full
        // path to target fragment.
        RestIF rif = (RestIF) TrafoUtils.extractCoreArtefactByKey(rifRamConcern, TrafoUtils.RIF_KEY)
                .getRootModelElement();
        Annotation springExposureAnnotation = resolveAccessMethodToAnnotation(fpcdm, restAccessMethod, rif);

        // Resolve the ram operation to it's controller counterpart
        // (only controllers are decorated with annotations.)
        Operation controllerOperation = AbstractFunctionalityPoorCdmFillUtil
                .resolveToControllerOperation(mappedRamOperation, fpcdm);
        controllerOperation.getAnnotation().add(springExposureAnnotation);
    }
    
    /**
     * Create appropriate annotations and decorate them on the associated parameters
     * in the fpcdm model.
     * 
     * @param mapping operation rif-ram mapping
     * @param fpcdm as the RAM model that stores the required imports (support
     * for the annotation)
     */
    private static void createPathVariableParameterMappingAnnotation(COREModelElementMapping mapping, 
            COREExternalArtefact fpcdm) {
        // Use the RIF end to identify the REST dynamic fragment, 
        // use the RAM end to identify the BL parameter whose FPCDM 
        // counterpart must be decorated by an annotation.
        DynamicFragment restDynamicFragment = TrafoUtils.getRestDynamicFragmentMappingEnd(mapping);
        ParameterImpl mappedRamParameter = TrafoUtils.getRamParameterMappingEnd(mapping);
        
        // Construct path variable annotations
        Annotation jaxRSPathParamAnnotation = createPathVariableAnnotationFromDynamicFragment(fpcdm, 
                restDynamicFragment);
        
        // Resolve the ram parameter to it's controller counterpart
        // (only controllers are decorated with annotations.)
        Parameter controllerParameter = AbstractFunctionalityPoorCdmFillUtil
                .resolveToControllerParameter(mappedRamParameter, fpcdm);
        
        // Append annotation to the parameter.
        controllerParameter.getAnnotation().add(jaxRSPathParamAnnotation);
    }
    
    /**
     * Creates PathVariable annotation and sets the formatted place holder of the
     * dynamic fragment as annotation parameter.
     * 
     * @param fpcdm as the RAM model that stores the required imports (support
     * for the annotation)
     * @param restDynamicFragment as the RIF dynamic fragment to be interpreted.
     * @return the Spring Boot annotation ready as is to insert into fpcdm (decorates
     * parameter operation)
     */
    private static Annotation createPathVariableAnnotationFromDynamicFragment(COREExternalArtefact fpcdm,
            DynamicFragment restDynamicFragment) {
        Annotation result = RamFactoryImpl.init().createAnnotation();
        result.setImport(identifyImportByName(fpcdm, "org springframework web bind annotation"));
        
        result.setMatter("PathVariable");
        
        String mappedElement = extractedFormattedNameFromDynamicFragment(restDynamicFragment);
                
        result.getParameter().add(mappedElement);

        return result;
    }
    
    /**
     * Create RequestBody annotation and add it to the mapped RAM operation in the fpcdm if the operation has any 
     * parameters without annotations.
     * 
     * @param mapping operation rif-ram mapping
     * @param fpcdm as the RAM model that stores the required imports (support
     * for the annotation)
     */
    private static void createRequestBodyParameterMappingAnnotation(COREModelElementMapping mapping, 
            COREExternalArtefact fpcdm) {
        // Use the RIF end to identify the REST method, use the RAM end
        // to
        // identify the BL operation whose FPCDM counterpart must be
        // decorated by an
        // annotation.
        OperationImpl mappedRamOperation = TrafoUtils.getRamOperationMappingEnd(mapping);
        
        // Resolve the ram operation to it's controller counterpart
        // (only controllers are decorated with annotations.)
        Operation controllerOperation = AbstractFunctionalityPoorCdmFillUtil
                .resolveToControllerOperation(mappedRamOperation, fpcdm);
        
        // Check if there are any unmapped parameter from the operation (returns the first one)
        Parameter unmappedParameter = getUnmappedParameterFromOperation(controllerOperation);
        
        // Create and append RequestBody annotation to the parameter if it is not null.
        if (unmappedParameter != null) {
            Annotation requestBodyAnnotation = createRequestBodyAnnotation(fpcdm);
            
            unmappedParameter.getAnnotation().add(requestBodyAnnotation);
        }
    }

    /**
     * Creates a RequestBody annotation.
     * 
     * @param fpcdm as the RAM model that stores the required imports (support
     * for the annotation)
     * @return the Spring Boot annotation ready as is to insert into fpcdm (decorates
     * parameter parameter)
     */
    private static Annotation createRequestBodyAnnotation(COREExternalArtefact fpcdm) {
        Annotation result = RamFactoryImpl.init().createAnnotation();
        result.setImport(identifyImportByName(fpcdm, "org springframework web bind annotation"));
        
        result.setMatter("RequestBody");

        return result;
    }

    /**
     * Inspects the internal "type" attribute of the accessMethod. Creates
     * appropriate annotation based on whether it is a get, put, post or del
     * method. Also sets the correct absolute fragment path as annotation
     * parameter.
     * 
     * @param fpcdm as the RAM model that stores the required imports (support
     * for the annotation)
     * @param accessMethod as the RIF access method to be interpreted.
     * @param rif as the RIF model that provides the full fragment tree
     * (including the fragment referenced by the accessMethods parent resource)
     * @return the spring annotation ready as is to insert into fpcdm (decorates
     * controller operation)
     */
    private static Annotation resolveAccessMethodToAnnotation(COREExternalArtefact fpcdm, AccessMethod accessMethod,
            RestIF rif) {
        // In either case the annotation depends on the "org springframework web
        // bind annotation" import.
        Annotation result = RamFactoryImpl.init().createAnnotation();
        result.setImport(identifyImportByName(fpcdm, "org springframework web bind annotation"));

        // In either case the annotation parameter is the full fragment path of
        // the accessMethod.
        PathFragment targetFragement = ((Resource) accessMethod.eContainer()).getEndpoint();
        PathFragment root = rif.getRoot();
        String resourcePath = constructRestPath(root, targetFragement);

        // Spring only allows raw (without key) path parameters, if no further
        // annotation parameters are around. A strict usage of keyed path
        // arguments is therefore safer.
        String annotationPathParameter = convertToAnnotationParameter("path", resourcePath);

        // Store the fully qualified resource path as annotation parameter.
        result.getParameter().add(annotationPathParameter);

        // Finally, set the Annotation MATTER (the annotation itself) according
        // to access method type (G/Pu/Po/D)
        // Check against all four supported types.
        if (accessMethod.getType().equals(MethodType.GET)) {
            result.setMatter("GetMapping");
            return result;
        }
        if (accessMethod.getType().equals(MethodType.PUT)) {
            result.setMatter("PutMapping");
            return result;
        }
        if (accessMethod.getType().equals(MethodType.POST)) {
            result.setMatter("PostMapping");
            return result;
        }
        if (accessMethod.getType().equals(MethodType.DELETE)) {
            result.setMatter("DeleteMapping");
            return result;
        }

        throw new TransformerException(
                "Provided access method could not be transformed to an annotation - unsupported type attribute.");
    }

    /**
     * Universal annotation key-value enricher. Can e.g. be used to declare a
     * mapping path for a @GetMapping annotation. Spring Boot allows multiple
     * ways to declare a mapping path. If no other annotation parameters are
     * used, only specifying the path in quotes is sufficient. If further
     * parameters are present, each parameter must be flagged with a key. This
     * implementation therefore uses the second, more universal option.
     * 
     * @param key as the annotation parameter's key field.
     * @param value as the annotation parameter's value field.
     * 
     * @return fused string with universal annotation syntax for key value
     * pairs. Ready for use in a ny annotation that accepts key-value based
     * arguments.
     */
    private static String convertToAnnotationParameter(String key, String value) {
        return key + "=\"" + value + "\"";
    }

    /**
     * Walks upward the model element hierarchy, until the root resource is
     * reached. Constructs absolute resource path while walking upward by
     * appending parent resource names in front of the current path string. This
     * method is recursive. It delegates construction of the parent's absolute
     * path until the root resource is reached.
     *
     * @param root as the top node in the resource tree.
     * @param currentFragment as the resource fragment for which the absolute
     * path must be determined.
     * 
     * @return absolute rest path of a target fragment, using root as origin.
     */
    private static String constructRestPath(PathFragment root, PathFragment currentFragment) {

        // recursion end criteria: targetFragment is identical to root fragment
        if (currentFragment == root) {
            return getIsolatedFragmentLocator(root);
        }

        // recursion step: concatenate own fragment locator to result of
        // recursive call to parent.
        PathFragment parent = (PathFragment) currentFragment.eContainer();
        return constructRestPath(root, parent) + getIsolatedFragmentLocator(currentFragment);
    }

    /**
     * Resolves a resource to the associated fragment locator (isolated),
     * including leading "/" and enclosing "{}" (the latter in case of a dynamic
     * fragment).
     * 
     * @param fragment that must be converted into an annotation-parameter alike
     * string representation.
     * @return isolated fragment string representation.
     */
    private static String getIsolatedFragmentLocator(PathFragment fragment) {

        // Dynamic fragments have enclosing "{...}" and a leading "/"
        if (fragment.getClass().equals(DynamicFragmentImpl.class)) {
            DynamicFragmentImpl dynamicFragment = (DynamicFragmentImpl) fragment;
            return dynamicFragment.getPlaceholder();
        }

        // Static fragments have only a leading "/"
        if (fragment.getClass().equals(StaticFragmentImpl.class)) {
            StaticFragmentImpl staticFragment = (StaticFragmentImpl) fragment;
            return staticFragment.getInternalname();
        } else {
            throw new TransformerException(
                    "The provided resource does not reference a valid dynamic or static fragemnt.");
        }
    }

    @Override
    public Classifier extractLauncherClass(StructuralView structuralView) {
        for (Classifier classifier : structuralView.getClasses()) {
            if (classifier.getName().equals(SpringBootFunctionalityPoorCdmFillUtil.LAUNCHER_NAME)) {
                return classifier;
            }
        }
        throw new TransformerException(
                "Service launcher can not be extracted. None of the FPCDM classes matched by name.");
    }
}
