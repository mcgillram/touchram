package ca.mcgill.sel.ram.ui.scenes.handler.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import ca.mcgill.sel.ram.ui.scenes.handler.impl.PomXmlSchemaValidator.MergeNodesResponse;

/**
 * This class is used to as an implementation of merge sort for {@link Node} objects.
 * It will sort and merge {@link Node} objects based on Xml tags.
 * There is no public constructor as every method is static.
 * 
 * @author Bowen
 */
public final class PomXmlMergeSort {
    /**
     * Empty private constructor as this class only contains static methods.
     */
    private PomXmlMergeSort() {
    }
    
    /**
     * This method is used to split an array of element nodes into two and merge them together with {@link merge}.
     * 
     * @param elementNodes - input array list of nodes
     * @return - sorted version of input array list of nodes based on xml tag priority
     */
    public static ArrayList<Node> mergeSort(ArrayList<Node> elementNodes) {
        if (elementNodes.size() == 1) {
            return elementNodes;
        }
        
        ArrayList<Node> elementNodesOne = new ArrayList<>();
        ArrayList<Node> elementNodesTwo = new ArrayList<>();
        
        int splitIndex = elementNodes.size() / 2;
        
        for (int i = 0; i < elementNodes.size(); i++) {
            if (i < splitIndex) {
                elementNodesOne.add(elementNodes.get(i));
            } else {
                elementNodesTwo.add(elementNodes.get(i));
            }
        }
        
        elementNodesOne = mergeSort(elementNodesOne);
        elementNodesTwo = mergeSort(elementNodesTwo);
        
        return merge(elementNodesOne, elementNodesTwo, false);
    }
    
    /**
     * This method is used to merge two input sorted array lists. This method will output a single sorted array based on
     * xml tag priority. When the tags from both array lists are the same, the nodes will be merged as one based on
     * rules defined in {@link mergeElementNodes}.
     * 
     * @param elementNodesOne - input sorted array list
     * @param elementNodesTwo - input sorted array list
     * @param mergeNodes - whether or not to merge nodes that have the same node name
     * @return - sorted and merged array list of both input array lists
     */
    public static ArrayList<Node> merge(ArrayList<Node> elementNodesOne, ArrayList<Node> elementNodesTwo, 
            boolean mergeNodes) {
        
        // indices to keep track of current index in two arrays
        int indexArrayOne = 0;
        int indexArrayTwo = 0;
        
        ArrayList<Node> mergedElementNodes = new ArrayList<>();
        
        while (indexArrayOne < elementNodesOne.size() && indexArrayTwo < elementNodesTwo.size()) {
            Node nodeOne = elementNodesOne.get(indexArrayOne);
            Node nodeTwo = elementNodesTwo.get(indexArrayTwo);
                        
            int priorityOne = getPriorityValueOf(nodeOne);
            int priorityTwo = getPriorityValueOf(nodeTwo);
            
            if (priorityOne < priorityTwo) {
                // if node one has a lower priority, add it to merged node list, increment index
                mergedElementNodes.add(nodeOne);
                indexArrayOne++;
            } else {
                if (priorityOne > priorityTwo) {
                    // if node two has a lower priority, add it to merged node list, increment index
                    mergedElementNodes.add(nodeTwo);
                    indexArrayTwo++;
                } else {
                    if (mergeNodes) {
                        // if the two nodes have the same priorities and mergeNodes is true,
                        // merge the two nodes with {@link mergeElementNodes} and add the merged node to merged node 
                        // list, increment indices
                        Node mergedElementNode = mergeElementNodes(nodeOne, nodeTwo);
                        mergedElementNodes.add(mergedElementNode);
                            
                        indexArrayOne++;
                        indexArrayTwo++;
                    } else {
                        // if the two nodes have the same priorities and mergeNodes is false, 
                        // add node one to merged node list, and increment its index
                        mergedElementNodes.add(nodeOne);
                        indexArrayOne++;
                    }
                }
            }  
        }
        // if first input node list is not fully explored, add the remaining nodes in the merged node list
        while (indexArrayOne < elementNodesOne.size()) {            
            mergedElementNodes.add(elementNodesOne.get(indexArrayOne));
            indexArrayOne++;
        }
            
        // if second input node list is not fully explored, add the remaining nodes in the merged node list
        while (indexArrayTwo < elementNodesTwo.size()) {            
            mergedElementNodes.add(elementNodesTwo.get(indexArrayTwo));
            indexArrayTwo++;
        }
            
        return mergedElementNodes;
    }
    
    /**
     * This method merges two nodes that have the same node name.
     * This method handles two cases, one in which both nodes each contain one child and it is a text node, another that
     * both nodes each contain element nodes as children.
     * 
     * @param elementNodeOne - first input node
     * @param elementNodeTwo - second input node
     * @return - merged node that is merged depending on the input nodes' types
     */
    private static Node mergeElementNodes(Node elementNodeOne, Node elementNodeTwo) {
        try {
            /**
             * If the definition of the node name is a primitive type and its maxOccurs attribute is 1 in the pom.xml
             * schema, we prioritize the second node.
             * 
             * elementNodeOne is instance of <tag1>text1</tag1>
             * elementNodeTwo is instance of <tag1>text2</tag1>
             * return element node two, as text2 is prioritized
             */
            if (PomXmlSchemaValidator.getInstance().isNodeNamePrimitiveTypeAndMaxOccursIsOne(
                    elementNodeOne.getNodeName())) {
                return elementNodeTwo;
            } else {
                /**
                 * Otherwise
                 * 
                 * elementNodeOne is instance of <tag*> <tag1> elementNode 11 elementNode 12 </tag1></tag*>
                 * elementNodeTwo is instance of <tag*> <tag1> elementNode 21 elementNode 22 </tag1></tag*>
                 * 
                 * <tag*> means 0 or more instances of tags that have a type attribute in the pom.xml schema that links
                 * to a complex type and has a maxOccurs attribute of 1.
                 * <tag1> is a tag that does not have a type attribute in the pom.xml schema, an inner node definition 
                 * that has a maxOccurs attribute of "unbounded" in the pom.xml schema, and the node we merge in.
                 * 
                 * return <tag*> <tag1> elementNode 11 elementNode 12 elementNode 21 elementNode 22 </tag1></tag*>
                 */
                Node[] commonSplitNodes = findCommonSplitNodes(elementNodeOne, elementNodeTwo, new ArrayList<String>());
                
                // if the common split nodes are not null, merge the nodes
                if (commonSplitNodes != null) {
                    Node mergedNode = elementNodeOne;
                    
                    NodeList nodeListTwo = commonSplitNodes[1].getChildNodes();
                    
                    for (int i = 0; i < nodeListTwo.getLength(); i++) {
    
                        Node node = nodeListTwo.item(i);
                        if (node.getNodeType() == Node.ELEMENT_NODE) {
                            // if node in elementNodeTwo does not exist already in elementNodeOne, append it
                            if (!containsNode(commonSplitNodes[0], node)) {
                                commonSplitNodes[0].appendChild(node);
                            }
                        }
                    }
                    
                    return mergedNode;
                }
            }
        } catch (ParserConfigurationException | TransformerException | IOException | SAXException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        // by default, return element node two
        return elementNodeTwo;
    }
    
    /**
     * This method returns the priority value of a node based on its node name.
     * This method looks into the pom.xml schema and find the accepted tags in order.
     * 
     * @param node - input node
     * @return - int value of the priority of the given node (lower value means higher priority)
     */
    private static int getPriorityValueOf(Node node) {
        String nodeName = node.getNodeName();
        
        List<String> acceptedTagsOrder = new ArrayList<>();
        
        try {
            acceptedTagsOrder = PomXmlSchemaValidator.getInstance().getAcceptedTagsOrder();
        } catch (ParserConfigurationException | TransformerException | IOException | SAXException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        return acceptedTagsOrder.indexOf(nodeName);
    }
    
    /**
     * This method finds the common split nodes for two input nodes.
     * 
     * @param nodeOne - input node one
     * @param nodeTwo - input node two
     * @param nodeTagsList - list of all previously recursed and current node tags, used for pom.xml schema to check
     * if the node's type attribute inks to a complex type
     * @return - Node list of the common split nodes of nodeOne and nodeTwo, null if it does not exist.
     */
    private static Node[] findCommonSplitNodes(Node nodeOne, Node nodeTwo, ArrayList<String> nodeTagsList) {
        String nameNodeOne = nodeOne.getNodeName();
        String nameNodeTwo = nodeTwo.getNodeName();
        MergeNodesResponse mergeNodesResponse;
        
        // add node name one to the node tag list to keep track of previously recursed and current node tags
        nodeTagsList.add(nameNodeOne);
        
        // check whether the current node's type attribute in the pom.xml schema file links to a complex type
        try {
            mergeNodesResponse = PomXmlSchemaValidator.getInstance().areInputNodesMergeNodes(
                    nodeOne, nodeTwo, nodeTagsList);
            
            // if we need to continue recursing, both nodes have only one child, and the names of the two nodes are 
            // equal, recurse to their children
            if (mergeNodesResponse == MergeNodesResponse.CONTINUE_RECURSING) {
                if (getNumOfElementNodeChildren(nodeOne) == 1) {
                    if (getNumOfElementNodeChildren(nodeTwo) == 1) {
                        if (nameNodeOne.equals(nameNodeTwo)) {
                            Node newNodeOne = getFirstElementNodeChild(nodeOne);
                            Node newNodeTwo = getFirstElementNodeChild(nodeTwo);
                            return findCommonSplitNodes(newNodeOne, newNodeTwo, nodeTagsList);
                        }
                    }
                }
            } else if (mergeNodesResponse == MergeNodesResponse.FOUND_MERGE_NODES) {
                // the current nodes are the merge nodes, return them
                Node[] nodeList = {nodeOne, nodeTwo};
                return nodeList;
            }
        } catch (ParserConfigurationException | TransformerException | IOException | SAXException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        // return null if there does not exist a merge node
        return null;
    }
    
    /**
     * This method returns the total number of children element nodes the input node has.
     * 
     * @param node - input node
     * @return - total number of children element nodes
     */
    private static int getNumOfElementNodeChildren(Node node) {
        int total = 0;
        
        NodeList nodeList = node.getChildNodes();
        
        for (int i = 0; i < nodeList.getLength(); i++) {
            if (nodeList.item(i).getNodeType() == Node.ELEMENT_NODE) {
                total++;
            }
        }
        
        return total;
    }
    
    /**
     * This method returns the first child that is an element node, returns null if there are none.
     * 
     * @param node - input node
     * @return - the first element node child of input node
     */
    private static Node getFirstElementNodeChild(Node node) {
        NodeList nodeList = node.getChildNodes();
        
        for (int i = 0; i < nodeList.getLength(); i++) {
            if (nodeList.item(i).getNodeType() == Node.ELEMENT_NODE) {
                return nodeList.item(i);
            }
        }
        
        return null;
    }
    
    /**
     * This method returns whether nodeONe already contains nodeTwo as a child node.
     * 
     * @param nodeOne - input node one
     * @param nodeTwo -input node two
     * @return - whether nodeONe already contains nodeTwo as a child node.
     */
    private static boolean containsNode(Node nodeOne, Node nodeTwo) {
        NodeList nodeListOne = nodeOne.getChildNodes();
        
        for (int i = 0; i < nodeListOne.getLength(); i++) {
            if (checkNodeEquality(nodeListOne.item(i), nodeTwo)) {
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * This method checks if two nodes' contents are equal.
     * 
     * @param nodeOne - input node one
     * @param nodeTwo - input node two
     * @return - whether the two nodes are equal
     */
    public static boolean checkNodeEquality(Node nodeOne, Node nodeTwo) {
        return nodeOne.isEqualNode(nodeTwo);
    }
}
