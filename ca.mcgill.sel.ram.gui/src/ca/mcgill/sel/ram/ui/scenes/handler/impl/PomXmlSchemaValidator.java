package ca.mcgill.sel.ram.ui.scenes.handler.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


/**
 * This singular class parses the pom.xml schema file and provides parsing and validation functionality.
 * 
 * The pom.xml schema file is from the following url...
 * https://maven.apache.org/xsd/maven-4.0.0.xsd
 *  
 * @author Bowen
 */
public final class PomXmlSchemaValidator {
    /**
     * Private enum class to denote the three possible categories of the type attribute of an element node.
     */
    private enum TypeAttributeCategory {
        /**
         * Type attribute starts with "xs:" and ends with a primitive type such as string, boolean etc.
         */
        PRIMITIVE_TYPE,
        /**
         * Does not contain a type attribute.
         */
        NONE,
        /**
         * Type attribute links to other complex type in the pom.xml schema file.
         */
        LINK_TO_OTHER_COMPLEX_TYPE
    }
    
    /**
     * Public enum class to denote the three possible different response from checking if the input nodes are common
     * merge nodes.
     */
    public enum MergeNodesResponse {
        /**
         * Input nodes are the nodes to merge at.
         */
        FOUND_MERGE_NODES,
        /**
         * Input nodes are not the nodes to merge at, continue recursing.
         */
        CONTINUE_RECURSING,
        /**
         * Error occurred during pom xml parsing and the merge nodes do not exist.
         */
        MERGE_NODES_DNE
    }
    
    // static variable reference of PomXmlSchemaParser
    private static PomXmlSchemaValidator pomXmlSchemaParser;
    
    // the pom.xml schema file is currently located in the ca/mcgill/sel/ram/gui/resources folder
    // TODO: this file does not belong there, find correct location and update this path variable
    private static final String POM_XML_SCHEMA_PATH = "maven-4.0.0.xsd";
        
    private XmlIOUtils xmlIOUtils;
    private Document pomXmlSchemaDocument;
    
    // node in pom.xml schema that contains all of the accepted tags as children
    private Node acceptedTagsNode;
    // unmodifiable list of accepted tags (order taken from pom.xml schema file)
    private final List<String> acceptedTagsOrder;

    /**
     * Private constructor, parse pom.xml schema file, find the node containing accepted tags as children, and get
     * the list of name attributes of the children of that node.
     * 
     * @throws ParserConfigurationException
     * @throws TransformerException
     * @throws IOException
     * @throws SAXException
     */
    private PomXmlSchemaValidator() throws ParserConfigurationException, TransformerException, IOException, SAXException {
        
        xmlIOUtils = XmlIOUtils.getInstance();
        // parse pom.xml schema file
        pomXmlSchemaDocument = xmlIOUtils.parseXmlSchemaToDocument(POM_XML_SCHEMA_PATH);
        
        // find the node that contains accepted tags as children
        // this node is found by the document root node -> name = "Model" node -> tag = "xs:all" node
        acceptedTagsNode = findXsAllComplexTypeNodeByAttributeName("Model");
        // get the name attributes from all of the children of acceptedTagsNode
        acceptedTagsOrder = getNameAttributeOfChildrenOf(acceptedTagsNode);
    }

    /**
     * Returns the singleton instance of pom xml schema parser.
     * 
     * @return - instance of pom xml schema parser
     * @throws ParserConfigurationException
     * @throws TransformerException
     */
    public static PomXmlSchemaValidator getInstance() throws ParserConfigurationException, TransformerException, 
        IOException, SAXException {
        
        if (pomXmlSchemaParser == null) {
            pomXmlSchemaParser = new PomXmlSchemaValidator();
        }
        return pomXmlSchemaParser;
    }
    
    /**
     * Returns unmodifiable list of name attributes of children of input node.
     * 
     * @param node - input node object
     * @return - unmodifiable list of name attributes of children of input node
     */
    private static List<String> getNameAttributeOfChildrenOf(Node node) {
        ArrayList<String> names = new ArrayList<>();
        
        // get node list children of node
        NodeList nodeList = node.getChildNodes();
        
        // loop through each child of the node, if it is an element node, add its "name" attribute to the list 
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node child = nodeList.item(i);
            
            if (child.getNodeType() == Node.ELEMENT_NODE) {
                Element childElement = (Element) child;
                
                names.add(childElement.getAttribute("name"));
            }
        }
        
        // return unmodifiable version of the list
        return Collections.unmodifiableList(names);
    }
    
    /**
     * Returns list of node names of children of input node.
     * 
     * @param node - input node object
     * @return - list of node names of children of input node
     */
    private static List<String> getNodeNamesOfChildrenOf(Node node) {
        ArrayList<String> names = new ArrayList<>();
        
        // get node list children of node
        NodeList nodeList = node.getChildNodes();
        
        // loop through each child of the node, if it is an element node, add its node name to the list
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node child = nodeList.item(i);
            
            if (child.getNodeType() == Node.ELEMENT_NODE) {                
                names.add(child.getNodeName());
            }
        }
        
        // return unmodifiable version of the list
        return names;
    }
    
    /**
     * This method examines the children of the input node and returns the child that has the value of the input
     * attribute name equal to the input attribute value if it exists.
     * 
     * @param node - input node
     * @param attributeName - name of attribute to check
     * @param attributeValue - value of attribute under attribute name
     * @return - child that has the value of the input attribute name equal to the input attribute value if it exists,
     * null otherwise
     */
    private static Node getChildOfElementNodeByAttribute(Node node, String attributeName, String attributeValue) {
        // get node list children of node
        NodeList nodeList = node.getChildNodes();
        
        // loop through each child of the node, if it is an element node, check if the value of the input attribute name
        // is equal to the input attribute value, if it is, return that child, if it is not, continue iterating
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node child = nodeList.item(i);
            
            if (child.getNodeType() == Node.ELEMENT_NODE) {
                Element elementChild = (Element) child;
                
                if (elementChild.getAttribute(attributeName).equals(attributeValue)) {
                    return child;
                }
            }
        }
        
        return null;
    }
    
    /**
     * This method examines the children of the input node and returns the child that contains a tag name of the input
     * tag name if it exists.
     * 
     * @param node - input node
     * @param tagName - tag name of an element node
     * @return - child node that contains an tag name of the input tag name if it exists, null otherwise
     */
    private static Node getChildOfElementNodeByTagName(Node node, String tagName) {
        // get node list children of node
        NodeList nodeList = node.getChildNodes();
        
        // loop through each child of the node, if it is an element node, check if the tag name is equal to the input 
        // tag name, if it is, return that child, if it is not, continue iterating
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node child = nodeList.item(i);
            
            if (child.getNodeType() == Node.ELEMENT_NODE) {
                Element elementChild = (Element) child;
                
                if (elementChild.getTagName().equals(tagName)) {
                    return child;
                }
            }
        }
        
        return null;
    }
    
    /**
     * Public getter for unmodifiable list of accepted tags (order taken from pom.xml schema file).
     * 
     * @return - unmodifiable list of accepted tags (order taken from pom.xml schema file)
     */
    public List<String> getAcceptedTagsOrder() {
        return acceptedTagsOrder;
    }
    
    /**
     * This method checks if the input node name's type tag is primitive (starts with "xs:") and the maxOccurs attribute
     * is one. 
     * 
     * @param nodeName - input node name
     * @return - whether or not the input node name's type tag is primitive (starts with "xs:") and the maxOccurs 
     * attribute is one. 
     */
    public boolean isNodeNamePrimitiveTypeAndMaxOccursIsOne(String nodeName) {
        Node acceptedTagsNodeChild = getChildOfElementNodeByAttribute(acceptedTagsNode, "name", nodeName);
        
        if (acceptedTagsNodeChild != null) {
            if (acceptedTagsNodeChild.getNodeType() == Node.ELEMENT_NODE) {
                Element acceptedTagsNodeChildElement = (Element) acceptedTagsNodeChild;
                
                // get the type attribute and find the category of it
                String tagElementTypeAttribute = getAttributeOfElementNode(acceptedTagsNodeChildElement, "type");
                TypeAttributeCategory tagTypeAttributeCategory = findTypeAttributeCategoryOf(tagElementTypeAttribute);
                
                // if the cateogry of the type attribute is primitive type
                if (tagTypeAttributeCategory.equals(TypeAttributeCategory.PRIMITIVE_TYPE)) {
                    String maxOccursAttribute = getAttributeOfElementNode(acceptedTagsNodeChildElement, "maxOccurs");
                    
                    // if the maxOccurs attribute DNE (return empty string), the default value is 1.
                    if (maxOccursAttribute.isEmpty() || "1".equals(maxOccursAttribute)) {
                        return true;
                    }
                }
            }
        }
        
        return false;
    }
    
    /**
     * This method checks if the input nodes can be merged. Input nodes can be merged if the input nodes links to other
     * complex types in the type attribute, and the common tag name of the input nodes' children have a maxOccurs
     * attribute of unbounded in the pom.xml schema.
     * 
     * @param inputNodeOne - input node one
     * @param inputNodeTwo - input node two
     * @param nodeTagsList - list of all previous and current node tags, e.g. {"build", "plugins"}
     * @return - the appropriate action response to whether the input nodes are the merge nodes (are merge nodes, 
     * aren't merge nodes keep recursing, aren't merge nodes stop recursing)
     */
    public MergeNodesResponse areInputNodesMergeNodes(Node inputNodeOne, Node inputNodeTwo, ArrayList<String> 
        nodeTagsList) {
        
        // object to represent current node iterated on
        Node currentNode = null;
        
        // iterate the current node through each tag in the nodeTagsList
        for (int i = 0; i < nodeTagsList.size(); i++) {
            String nodeTag = nodeTagsList.get(i);
            
            if (i == 0) {
                // if it is the first tag in the node tag list, get the child of the root node of the pom.xml schema
                // with the name attribute of the nodeTag
                currentNode = getChildOfElementNodeByAttribute(acceptedTagsNode, "name", nodeTag);
            } else {
                // if it is not the first tag in the node tag list, if current node tag's type attribute links to 
                // another complex type in the pom.xml schema get the child of the root node of the pom.xml schema with 
                // the name attribute of the nodeTag
                if (currentNode != null) {
                    if (currentNode.getNodeType() == Node.ELEMENT_NODE) {
                        String typeAttributeOfCurrentNode = getAttributeOfElementNode((Element) currentNode, "type");
                        TypeAttributeCategory typeAttributeCategory = findTypeAttributeCategoryOf(
                                typeAttributeOfCurrentNode);
                        
                        if (typeAttributeCategory.equals(TypeAttributeCategory.LINK_TO_OTHER_COMPLEX_TYPE)) {
                            currentNode = findXsAllComplexTypeNodeByAttributeName(typeAttributeOfCurrentNode);
                            
                            if (currentNode != null) {
                                // find the child of the xs:all node with the name of the current nodeTag
                                currentNode = getChildOfElementNodeByAttribute(currentNode, "name", nodeTag);
                            }
                        } else {
                            return MergeNodesResponse.MERGE_NODES_DNE;
                        }
                    }
                }
            }
        }
        
        // if current node is not null after iterating through the nodeTagsList
        if (currentNode != null) {
            // get the type attribute category of the current node
            String typeAttribute = getAttributeOfElementNode((Element) currentNode, "type");
            TypeAttributeCategory typeAttributeCategory = findTypeAttributeCategoryOf(typeAttribute);
            
            // if the node does not have a type attribute, this means that it contains a definition of its child under
            // this node
            if (typeAttributeCategory.equals(TypeAttributeCategory.NONE)) {
                // find the common child tag name of input nodes
                String commonTagName = findCommonChildTagNameOf(inputNodeOne, inputNodeTwo);
                                
                if (!commonTagName.isEmpty()) {
                    // find inner node definition with the common tag name 
                    Node innerNodeDefinition = getInnerNodeDefintionOf(currentNode, commonTagName);
                    
                    if (innerNodeDefinition != null) {
                        if (innerNodeDefinition.getNodeType() == Node.ELEMENT_NODE) {
                            // find maxOccurs attribute of inner node definition
                            String maxOccursAttribute = getAttributeOfElementNode((Element) innerNodeDefinition, 
                                    "maxOccurs");
                            
                            // if attribute of inner node definition is unbounded this means that the input nodes are
                            // the merge nodes, return that we found the merge nodes
                            if ("unbounded".equals(maxOccursAttribute)) {
                                return MergeNodesResponse.FOUND_MERGE_NODES;
                            }               
                        }
                    }
                } else {
                    // if common tag name does not exist (empty string), check if there is an inner node definition of
                    // the current node that has the tag name of "xs:any" (meaning only node)
                    Node innerNodeDefinition = getOnlyInnerNodeDefintionOf(currentNode);
                    
                    // if the innerNodeDefinition exists, check if the value of the process contents attribute is skip
                    // if it is, it means that this is the merge node
                    if (innerNodeDefinition != null) {
                        String processContentsAttribute = getAttributeOfElementNode((Element) innerNodeDefinition, 
                                "processContents");
    
                        if ("skip".equals(processContentsAttribute)) {
                            return MergeNodesResponse.FOUND_MERGE_NODES;
                        }
                    }
                }
                
            } else if (typeAttributeCategory.equals(TypeAttributeCategory.LINK_TO_OTHER_COMPLEX_TYPE)) {
                // if the type attribute of the current node is a link to another complex type in the pom.xml schema,
                // and the maxOccurs attribute of the current node is 1, return that we need to continue recursing
                String maxOccursAttribute = getAttributeOfElementNode((Element) currentNode, "maxOccurs");
                
                if (maxOccursAttribute.isEmpty() || "1".equals(maxOccursAttribute)) {
                    return MergeNodesResponse.CONTINUE_RECURSING;
                }
            }
        }
        
        return MergeNodesResponse.MERGE_NODES_DNE;
    }
    
    /**
     * This method finds the common tag name of the children of the two input nodes and returns the empty string if it
     * does not exist.
     * 
     * @param inputNodeOne - input node one
     * @param inputNodeTwo - input node two
     * @return - common tag name of the children of the two input nodes if it exists, empty string if not
     */
    private static String findCommonChildTagNameOf(Node inputNodeOne, Node inputNodeTwo) {
        // get node names of the children of nodes one and two
        List<String> namesOfChildrenOfNodeOne = getNodeNamesOfChildrenOf(inputNodeOne);
        List<String> namesOfChildrenOfNodeTwo = getNodeNamesOfChildrenOf(inputNodeTwo);
        
        // if the lists are distinct
        if (namesOfChildrenOfNodeOne.stream().distinct().count() <= 1) {
            if (namesOfChildrenOfNodeTwo.stream().distinct().count() <= 1) {
                
                // if both of the lists are nonempty, return the value if they are equal
                // if one of the lists are nonempty, return the nonempty value
                if (namesOfChildrenOfNodeOne.size() > 0) {
                    if (namesOfChildrenOfNodeTwo.size() > 0) {
                        if (namesOfChildrenOfNodeOne.get(0).equals(namesOfChildrenOfNodeTwo.get(0))) {
                            return namesOfChildrenOfNodeOne.get(0);
                        }
                    } else {
                        return namesOfChildrenOfNodeOne.get(0);
                    }
                } else {
                    if (namesOfChildrenOfNodeTwo.size() > 0) {
                        return namesOfChildrenOfNodeTwo.get(0);
                    }
                }
            }
        }
        
        return "";
    }
    
    /**
     * This method returns the attribute of an input element node.
     * 
     * @param elementNode - input element node
     * @param attributeName - input attribute name
     * @return - type attribute of input element node
     */
    private static String getAttributeOfElementNode(Element elementNode, String attributeName) {
        return elementNode.getAttribute(attributeName);
    }
    
    /**
     * This method returns the type attribute category of an input type attribute string.
     * 
     * @param typeAttribute - input type attribute string
     * @return - type attribute category of input trype attribute string
     */
    private static TypeAttributeCategory findTypeAttributeCategoryOf(String typeAttribute) {        
        if (typeAttribute.startsWith("xs:")) {
            return TypeAttributeCategory.PRIMITIVE_TYPE;
        } else if (typeAttribute.isEmpty()) {
            return TypeAttributeCategory.NONE;
        } else {
            return TypeAttributeCategory.LINK_TO_OTHER_COMPLEX_TYPE;
        }
    }
    
    /**
     * This method takes the root node of the pom.xml schema document and finds the node that has a name attribute of
     * the input attribute value. This method then finds the child of the previous node that contains the tag name 
     * "xs:all".
     * 
     * @param attributeValue - input attribute value string
     * @return - the comeplex type node with the name attribute of attribute value's child with "xs:all" tag name
     */
    private Node findXsAllComplexTypeNodeByAttributeName(String attributeValue) {
        Node rootNode = pomXmlSchemaDocument.getDocumentElement();
        
        if (rootNode != null) {
            Node complexTypeNode = getChildOfElementNodeByAttribute(rootNode, "name", attributeValue);
            
            if (complexTypeNode != null) {
                Node xsAllComplexTypeNode = getChildOfElementNodeByTagName(complexTypeNode, "xs:all");
                
                return xsAllComplexTypeNode;
            }
        }
        
        return null;
    }
    
    /**
     * This method takes in an input node from the pom.xml schema, that is an element node and not have a type 
     * attribute and finds the inner node definition with the input name. It does this by first looking for the child
     * with "xs:complexType" tag name, the child with the "xs:sequence" tag name, and the child with the name attribute
     * of the input string.
     * 
     * @param node - input node, must be an element node and not have a type attribute
     * @param innerNodeName - input string of the name of the node we are looking for
     * @return - inner node definition with a name of the input string if it exists.
     */
    private static Node getInnerNodeDefintionOf(Node node, String innerNodeName) {
        Node complexTypeNode = getChildOfElementNodeByTagName(node, "xs:complexType");
        
        if (complexTypeNode != null) {
            Node sequenceNode = getChildOfElementNodeByTagName(complexTypeNode, "xs:sequence");
            
            if (sequenceNode != null) {
                return getChildOfElementNodeByAttribute(sequenceNode, "name", innerNodeName);
            }
        }
                
        return null;
    }
    
    /**
     * This method takes in an input node from the pom.xml schema, that is an element node and not have a type 
     * attribute and finds the inner node definition with the "xs:any" tag name (only child). It does this by first 
     * looking for the child with "xs:complexType" tag name, the child with the "xs:sequence" tag name, and the child 
     * with the "xs:any" tag name.
     * 
     * @param node - input node, must be an element node and not have a type attribute
     * @return - inner node definition with a tag name of xs:all if it exists.
     */
    private static Node getOnlyInnerNodeDefintionOf(Node node) {
        Node complexTypeNode = getChildOfElementNodeByTagName(node, "xs:complexType");
        
        if (complexTypeNode != null) {
            Node sequenceNode = getChildOfElementNodeByTagName(complexTypeNode, "xs:sequence");
            
            if (sequenceNode != null) {
                return getChildOfElementNodeByTagName(sequenceNode, "xs:any");
            }
        }
                
        return null;
    }
}