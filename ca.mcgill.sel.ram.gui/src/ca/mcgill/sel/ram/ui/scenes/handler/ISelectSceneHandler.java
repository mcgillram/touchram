package ca.mcgill.sel.ram.ui.scenes.handler;

import ca.mcgill.sel.ram.ui.scenes.TouchCOREStartupScene;

/**
 * This interface is implemented by something that can handle events for a {@link TouchCOREStartupScene}.
 * 
 * @author mschoettle
 */
public interface ISelectSceneHandler extends IRamAbstractSceneHandler {
    
    /**
     * Handles creating a new concern {@link Concern}.
     * @param scene
     *            the affected {@link TouchCOREStartupScene}
     */
    void createConcernDisplay(TouchCOREStartupScene scene);
    
    /**
     * Handles loading an existing concern to edit it. {@link Concern}
     * @param scene
     *            the affected {@link TouchCOREStartupScene}.
     */
    void loadConcernDisplay(TouchCOREStartupScene scene);
    
    /**
     * Handles loading an existing concern to make selection for it. {@link Concern}
     * @param scene
     *            the affected {@link TouchCOREStartupScene}.
     */
    void buildApplicationConcernDisplay(TouchCOREStartupScene scene);
    
}
