package ca.mcgill.sel.ram.ui.components.navigationbar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.mt4j.components.MTComponent;
import org.mt4j.components.StateChange;
import org.mt4j.components.StateChangeEvent;
import org.mt4j.components.StateChangeListener;
import org.mt4j.components.TransformSpace;
import org.mt4j.input.inputProcessors.componentProcessors.AbstractComponentProcessor;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldProcessor;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.CORELanguageElementMapping;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.CORENamedElement;
import ca.mcgill.sel.core.COREScene;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.InterLanguageMapping;
import ca.mcgill.sel.core.IntraLanguageMapping;
import ca.mcgill.sel.core.NavigationMapping;
import ca.mcgill.sel.core.guinavigation.IntraLanguageMappingTree.ILMNode;
import ca.mcgill.sel.core.impl.ArtefactMapImpl;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.AspectMessageView;
import ca.mcgill.sel.ram.MessageView;
import ca.mcgill.sel.ram.Operation;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.controller.ControllerFactory;
import ca.mcgill.sel.ram.impl.NamedElementImpl;
import ca.mcgill.sel.ram.provider.util.RAMEditUtil;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.ConfirmPopup;
import ca.mcgill.sel.ram.ui.components.RamButton;
import ca.mcgill.sel.ram.ui.components.RamEMFListComponent;
import ca.mcgill.sel.ram.ui.components.RamExpendableComponent;
import ca.mcgill.sel.ram.ui.components.RamImageComponent;
import ca.mcgill.sel.ram.ui.components.RamListComponent;
import ca.mcgill.sel.ram.ui.components.RamListComponent.Filter;
import ca.mcgill.sel.ram.ui.components.RamListComponent.Namer;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent.Cardinal;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamSpacerComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.ram.ui.components.listeners.RamListListener;
import ca.mcgill.sel.ram.ui.components.navigationbar.NavigationBarMenu.NavigationBarNamerGeneric;
import ca.mcgill.sel.ram.ui.components.navigationbar.NavigationBarMenu.NavigatonBarNamer;
import ca.mcgill.sel.ram.ui.events.listeners.ActionListener;
import ca.mcgill.sel.ram.ui.layouts.HorizontalLayout;
import ca.mcgill.sel.ram.ui.layouts.VerticalLayout;
import ca.mcgill.sel.ram.ui.scenes.DisplayAspectScene;
import ca.mcgill.sel.ram.ui.scenes.DisplayConcernEditScene;
import ca.mcgill.sel.ram.ui.scenes.DisplayImpactModelEditScene;
import ca.mcgill.sel.ram.ui.scenes.SceneCreationAndChangeFactory;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.GUIConstants;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.impl.TextViewHandlerNavBar;
import ca.mcgill.sel.ram.ui.views.structural.StructuralDiagramView;
import ca.mcgill.sel.usecases.NamedElement;
import ca.mcgill.sel.usecases.UcPackage;

/**
 * An element in a {@link NavigationBarMenu}. This component contains a list of {T} elements.
 * The list can be headed by a label. When it's the case, the list will be contained in an
 * {@link RamExpendableComponent} if the list contains more than one element. Otherwise, it will show it in line.
 * When an element of the list is tapped it can either shows a sub-menu if a {@link NavigatonBarNamer} is given,
 * otherwise it will do the actions of the given listener.
 *
 * @param <T> - type of elements in the list.
 * @author Andrea
 */
public class NavigationBarMenuElement<T> extends RamRectangleComponent implements RamListListener<T> {
    
    
    /**
     * Text view which should be used within this class which should be called when the text of the model element is 
     * updated to ensure that the list within the current nav bar element remains sorted in alphabetical order at all
     * times.
     * @author ian
     *
     */
    public class TextViewNavBarElement extends TextView {

        public TextViewNavBarElement(EObject data, EStructuralFeature feature) {
            super(data, feature);
        }
        
        @Override
        public void updateText() {                        
            super.updateText();
            
            sortTopLevel();
        }
        
        
        @Override
        public String getModelText() {
            return super.getModelText();
        }
        
        public void registerTapAndHoldProcessor(TextViewHandlerNavBar handler) {
            AbstractComponentProcessor tapAndHoldProcessor = new TapAndHoldProcessor(RamApp.getApplication(),
                    GUIConstants.TAP_AND_HOLD_DURATION);
            registerInputProcessor(tapAndHoldProcessor);
            tapAndHoldProcessor.setBubbledEventsEnabled(true);
            addGestureListener(TapAndHoldProcessor.class, handler);
        }
        
    }
    

    private RamListComponent<T> list;
    private NavigatonBarNamer<T> namer;
    private NavigationBarMenu menu;
    private Map<RamExpendableComponent, List<String>> sortingListMap;
    private ILMNode node;
    private NavigationMapping ilmInput;
    private String nameElem;

    /**
     * Constructs an element in a {@link NavigationBarMenu} in function of the parameters given. It's a default
     * constructor. "menu" parameter must not be null. Then if you want a {@link RamEMFListComponent} you have to give
     * "data" and "feature", otherwise give "list" parameter. If you want a sub-menu you have to give "namer" otherwise
     * "listener" parameter. "label" and "type" and "filter" are optional.
     * 
     * @param menu - menu containing the element.
     * @param label - label heading the list (Optional)
     * @param data - {@link EObject} for {@link RamEMFListComponent}
     * @param feature - {@link EStructuralFeature} for {@link RamEMFListComponent}
     * @param type - {@link EClassifier} for {@link RamEMFListComponent} (Optional)
     * @param list - list of {T} elements
     * @param namer - {@link NavigatonBarNamer} to initialize sub-menu when item is tapped.
     * @param listener - listener called when item is tapped.
     * @param filter - filter to remove elements in the list (Optional)
     */
    // CHECKSTYLE:IGNORE ParameterNumberCheck: general constructor which is only called by the addMenuElement methods
    NavigationBarMenuElement(NavigationBarMenu menu, String label, EObject data, EStructuralFeature feature,
            EClassifier type, List<T> list, NavigatonBarNamer<T> namer, RamListListener<T> listener, Filter<T> filter) {
        super();
        this.menu = menu;
        this.nameElem = label;
        
        if (list != null) {
            this.list = new RamListComponent<T>(list, false);
        } else {
            this.list = new RamEMFListComponent<T>(data, feature, false);
        }
        this.list.setFilter(filter);
        this.list.setNoFill(true);
        this.list.setNoStroke(true);
        this.list.setNamer(new Namer<T>() {
            @Override
            public RamRectangleComponent getDisplayComponent(T element) {
                return customizeNamer(element);
            }
           
            @Override
            public String getSortingName(T element) {
                return sortElementsForMenus(element);   
            }
            @Override
            public String getSearchingName(T element) {
                return null;
            }
        });
        this.namer = namer;

        if (listener != null) {
            this.list.registerListener(listener);
        } else {
            this.list.registerListener(this);
        }

        if (label != null) {
            RamTextComponent labelComponent = new RamTextComponent(label + ": ");
            
            if (this.list.getElementMap().size() > 1) {
                initElementExpendable(labelComponent, label);
            } else {
                initElementOneLine(labelComponent);
            }
        } else {
            initElementSimple();
        }
    }

    
    /**
     * Generic implementation of creating the navigation bar element for a specific model. This should be called for 
     * each model when it is opened to populate the nav bar with information. The code will run recursively down the 
     * intralanguage mappings and will populate each element within those mappings to the navigation bar.
     * All down tree connections will be created within a separate member of the list.
     * @param menu the menu to which the element will be place
     * @param label the name of the current element
     * @param node the starting node of the implementation - should be root of the tree when called initially
     * @param namer the sub-namer for all menu elements which will be included
     * @param listener optional listener (to be implemented)
     * @param mapping the specific mapping to be checked in this instance
     * @param expandable used to tell if the element is expandable (2 for expandable with object as title, 1 for 
     * expandable with simple title, 0 for simple element)
     */
    NavigationBarMenuElement(NavigationBarMenu menu, String label, ILMNode node, NavigatonBarNamer<T> namer, 
            RamListListener<T> listener, NavigationMapping mapping, int expandable) {
        super();
        this.menu = menu;
        this.namer = namer;
        this.node = node;
        this.ilmInput = mapping;
        this.nameElem = label;
        sortingListMap = new HashMap<RamExpendableComponent, List<String>>();
        
        List<T> emptyList = new ArrayList<T>();
        this.list = new RamListComponent<T>(emptyList);
        
        RamExpendableComponent recurseThroughComponent = recurseThroughTree(label, node, mapping);
        
        this.setLayout(new VerticalLayout());
        StateChangeListener listenerComp = new InternalStateChangeListener();
        this.list.addStateChangeListener(StateChange.REMOVED_FROM_PARENT, listenerComp);
        this.list.addStateChangeListener(StateChange.ADDED_TO_PARENT, listenerComp);
        recurseThroughComponent.getFirstLine().setNoFill(true);
        recurseThroughComponent.getFirstLine().setNoStroke(true);
        recurseThroughComponent.setNoFill(true);
        recurseThroughComponent.setNoStroke(true);
        recurseThroughComponent.setName(label);
        this.addChild(recurseThroughComponent);
        
        node.getNodeComponents().add(recurseThroughComponent);
        node.getIntraLanguageMappingTree().getNavigationBarMenuElements().add(this);
        
    }
    /**
     * Recursive method to create all of the parts of the generic nav bar menu element.
     * @param label the name of the current element
     * @param n the current origin node
     * @param mapping the mapping to be used in this iteration - used to filter only the pertinent elements 
     * submenu
     * @return the created component for the current submenu
     */
    private RamExpendableComponent recurseThroughTree(String label, ILMNode n, NavigationMapping mapping) {
        List<ILMNode> simpleElements = new ArrayList<ILMNode>();
        List<ILMNode> reuseNodes = new ArrayList<ILMNode>();
        
        List<RamExpendableComponent> expendableComponents = new ArrayList<RamExpendableComponent>();
        
        for (ILMNode curNode : n.getChildren()) {
            if (curNode.getILM() instanceof IntraLanguageMapping) {
                if (curNode.getILM().equals(mapping)) {
                    if (curNode.getChildren().isEmpty()) {
                        simpleElements.add(curNode);
                        if (curNode.getILM() instanceof IntraLanguageMapping) {
                            IntraLanguageMapping curILM = (IntraLanguageMapping) curNode.getILM();
                            if (curILM.isReuse()) {
                                reuseNodes.add(curNode);
                            }
                        }
                    } else {
                        String modelString = EMFModelUtil.getNameAttribute(curNode.getNodeModelElement());
                        
                        if (modelString == null) {
                            modelString = label;
                        }
                        Map<NavigationMapping, String> typesOfChildren = new HashMap<NavigationMapping, String>();
                        List<RamExpendableComponent> listOfExpandableElements = new ArrayList<RamExpendableComponent>();
                        for (ILMNode child : curNode.getChildren()) {
                            if (!typesOfChildren.containsKey(child.getILM())) {
                                if (child.getILM() instanceof IntraLanguageMapping) {
                                    IntraLanguageMapping childILM = (IntraLanguageMapping) child.getILM();
                                    typesOfChildren.put(childILM, childILM.getName());
                                } else {
                                    typesOfChildren.put(child.getILM(), child.getInterMappingName());
                                }
                            }
                        }
                        for (NavigationMapping map : typesOfChildren.keySet()) {
                            String expElementName = typesOfChildren.get(map);
                            RamExpendableComponent innerElement = recurseThroughTree(expElementName, curNode, map);
                            innerElement.setName(expElementName);
                            innerElement.getFirstLine().setNoFill(true);
                            innerElement.getFirstLine().setNoStroke(true);
                            innerElement.setNoFill(true);
                            innerElement.setNoStroke(true);
                            listOfExpandableElements.add(innerElement);
                        }
                        List<T> emptyListInner = new ArrayList<T>();
                        RamListComponent<T> nodeComponentList = new RamListComponent<T>(emptyListInner);
                        
                        List<String> innerListOfNames = new ArrayList<String>();
                        
                        for (RamExpendableComponent elem : listOfExpandableElements) {                        
                            String elemName = elem.getName();
                            
                            innerListOfNames.add(elemName.toUpperCase());
                            
                            java.util.Collections.sort(innerListOfNames);
                            
                            int elementPosition = innerListOfNames.indexOf(elemName.toUpperCase());
                            
                            nodeComponentList.addChild(elementPosition, elem);
                        }
                        
                        RamExpendableComponent nodeExpComp = initElementExpendableWithTitleElement(
                                curNode.getNodeModelElement(), nodeComponentList, curNode.getParentNode()
                                .getNodeModelElement(), false);
                        
                        nodeExpComp.setName(modelString);
                        nodeExpComp.setNoStroke(true);
                        
                        sortingListMap.put(nodeExpComp, innerListOfNames);
                        
                        curNode.getNodeComponents().add(nodeExpComp);
                        
                        expendableComponents.add(nodeExpComp);
                    }
                }
            } else {
                NavigationMapping curNodeILM = curNode.getILM();
                if (curNodeILM.equals(mapping)) {
                  
                    simpleElements.add(curNode);
                }
            }
        }
        
        List<String> sortNameList = new ArrayList<String>();
        List<T> emptyList = new ArrayList<T>();
        RamListComponent<T> outterElementList = new RamListComponent<T>(emptyList);
        
        for (ILMNode eleNode : simpleElements) {
            EObject ele = eleNode.getNodeModelElement();
            
            if (ele instanceof ArtefactMapImpl) {
                ArtefactMapImpl nodeMap = (ArtefactMapImpl) ele;
                ele = nodeMap.getValue().get(0);
            }
            TextViewNavBarElement addedElement;
            if (reuseNodes.contains(eleNode)) {
                addedElement = createTextViewNavBarElement(ele, eleNode.getParentNode().getNodeModelElement(), true);  
            } else {
                addedElement = createTextViewNavBarElement(ele, eleNode.getParentNode().getNodeModelElement(), false);  
            }
            
            String eleName = EMFModelUtil.getNameAttribute(ele);
            
            if (eleName == null) {
                eleName = ele.toString();
            }
            sortNameList.add(eleName.toUpperCase());
            
            java.util.Collections.sort(sortNameList);
            
            int elementPosition = sortNameList.indexOf(eleName.toUpperCase());
            
            addedElement.setName(eleName);

            outterElementList.addChild(elementPosition, addedElement);
            
            eleNode.getNodeComponents().add(addedElement);
        }
        
        for (RamExpendableComponent eComp : expendableComponents) {
            String eleName = eComp.getName();
            sortNameList.add(eleName.toUpperCase());
            java.util.Collections.sort(sortNameList);
            int elementPosition = sortNameList.indexOf(eleName.toUpperCase());
            outterElementList.addChild(elementPosition, eComp);
        }
        RamTextComponent labelComponent = new RamTextComponent(label);
        labelComponent.setName(label);
        labelComponent.setBufferSize(Cardinal.NORTH, -1.0f);
        RamExpendableComponent mainComponent = new RamExpendableComponent(labelComponent, outterElementList);
        sortingListMap.put(mainComponent, sortNameList);
        return mainComponent;
    }
    
    /**Returns according to the type of element that has been treated the relative TextComponent for the menu element.
     * 
     * @param element element to examine
     * @return the RamTextComponent with the corresponding element name customized accordingly
     */
    private RamRectangleComponent customizeNamer(T element) {
        String label = sortElementsForMenus(element);
        
        if (element instanceof DisplayAspectScene) {
            label = String.format("Aspect: %s", label);
        } else if (element instanceof DisplayConcernEditScene) {
            label = String.format("Concern: %s", label);
        } else if (element instanceof DisplayImpactModelEditScene) {
            label = String.format("Goal: %s", label);
        }
        
        return new RamTextComponent(label);
    }

    /**
     * Sorts the elements appearing in every menu of a section in alphabetical order.
     * @param element to sort
     * @return String of the element name
     */
    protected String sortElementsForMenus(T element) {
        String result = null;
        
        if (element instanceof Operation) {
            Operation operation = (Operation) element;
            AdapterFactory adapterFactory = EMFEditUtil.getAdapterFactory(operation);
            
            result = EMFEditUtil.stripTypeName(operation, 
                    RAMEditUtil.getOperationSignature(adapterFactory, operation, false, true));
        } else if (element instanceof MessageView) {
            MessageView messageView = (MessageView) element;
            AdapterFactory adapterFactory = EMFEditUtil.getAdapterFactory(messageView);
            Operation operation = messageView.getSpecifies();
            
            result = EMFEditUtil.stripTypeName(operation, 
                    RAMEditUtil.getOperationSignature(adapterFactory, operation, true, false));
        } else if (element instanceof COREModelComposition) {
            result = ((COREModelComposition) element).getSource().getName();
        } else if (element instanceof DisplayAspectScene) {
            result = ((DisplayAspectScene) element).getAspect().getName();
        } else if (element instanceof DisplayConcernEditScene) {
            result = ((DisplayConcernEditScene) element).getConcern().getName();
        } else if (element instanceof DisplayImpactModelEditScene) {
            result = ((DisplayImpactModelEditScene) element).getGoal().getName();
        } else if (element instanceof EObject) {
            result = EMFModelUtil.getNameAttribute((EObject) element);
        } 
        
        if (result == null) {
            result = element.toString();
        }
        
        return result;
    }
    
    public void callNamer(T element, EObject parentEObject, boolean reuse) {
        NavigationBarMenu subMenu = new NavigationBarMenu();
        if (parentEObject instanceof COREFeature) {
            if (element instanceof EObject) {
                COREExternalArtefact modelArtefact = COREArtefactUtil.getReferencingExternalArtefact((EObject) element);
                COREScene scene = modelArtefact.getScene();
                SceneCreationAndChangeFactory.getFactory().connectSceneToFeature((COREScene) scene, (COREFeature) 
                        parentEObject);
            }
        }
        if (namer instanceof NavigationBarNamerGeneric) {
            ((NavigationBarNamerGeneric) namer).setReuse(reuse);
        }
        namer.initMenu(subMenu, element);
    }
    
    /**
     * Creates a new sub-menu related to the given element and call the sub-menu manager in {@link NavigationBar}.
     * 
     * @param element - the element related to the sub-menu.
     * @param elementView - view related to the given element.
     */
    public void createSubMenu(T element, RamRectangleComponent elementView) {
        NavigationBarMenu subMenu = new NavigationBarMenu();
        namer.initMenu(subMenu, element);
        
        if (subMenu.containsElements()) {
            Vector3D positionGlobal = elementView.getPosition(TransformSpace.GLOBAL);
            positionGlobal.y += elementView.getHeightXY(TransformSpace.GLOBAL) / 2;
            getParentOfType(NavigationBar.class).openSubMenu(menu, subMenu, positionGlobal);
        } else {
            subMenu.destroy();
        }
    }

    /**
     * Initialize the element to show in one line. Used when there is just one element in the list and a label.
     * 
     * @param label - label before the list
     */
    private void initElementOneLine(RamTextComponent label) {
        this.setLayout(new HorizontalLayout());
        this.addChild(label);
        this.addChild(this.list);
    }

    /**
     * Initialize the element to show just the list, whitout label.
     */
    private void initElementSimple() {
        this.setLayout(new VerticalLayout());
        this.addChild(this.list);
    }

    /**
     * Initialize the element to show an {@link RamExpendableComponent} which contains the list. So it must have a label
     * as header of the {@link RamExpendableComponent}.
     * 
     * @param label - header of the {@link RamExpendableComponent}
     * @param labelName the name for the expandable element 
     */
    private void initElementExpendable(RamTextComponent label, String labelName) {
        this.setLayout(new VerticalLayout());
        StateChangeListener listener = new InternalStateChangeListener();
        this.list.addStateChangeListener(StateChange.REMOVED_FROM_PARENT, listener);
        this.list.addStateChangeListener(StateChange.ADDED_TO_PARENT, listener);
        RamExpendableComponent expendable = new RamExpendableComponent(label, this.list);
        expendable.getFirstLine().setNoFill(true);
        expendable.getFirstLine().setNoStroke(true);
        expendable.setNoFill(true);
        expendable.setNoStroke(true);
        expendable.setName(labelName);
        this.addChild(expendable);
    }
    
    private RamExpendableComponent initElementExpendableWithTitleElement(EObject baseElement, 
            RamListComponent<T> inputList, EObject parentEObect, boolean reuse) {
        StateChangeListener listener = new InternalStateChangeListener();
        inputList.addStateChangeListener(StateChange.REMOVED_FROM_PARENT, listener);
        inputList.addStateChangeListener(StateChange.ADDED_TO_PARENT, listener);
        
        TextViewNavBarElement titleElementComp = createTextViewNavBarElement(baseElement, parentEObect, reuse);
        RamExpendableComponent expendable = new RamExpendableComponent(titleElementComp, inputList);
        expendable.getFirstLine().setNoFill(true);
        expendable.getFirstLine().setNoStroke(true);
        expendable.setNoFill(true);
        expendable.setNoStroke(true);
        return expendable;
    }

    
    public void sortTopLevel() {
        if (this.getChildCount() == 0) {
            return;
        }
        MTComponent expChild = this.getChildByIndex(0);
        if (expChild instanceof RamExpendableComponent) {
            sortChildren((RamExpendableComponent) expChild);
        }
    }
    
    public void sortChildren(RamExpendableComponent comp) {
        RamRectangleComponent hideableComponent = comp.getHidableComponent();
        
        if (hideableComponent instanceof RamListComponent<?>) {
            RamListComponent<T> componentList = (RamListComponent<T>) hideableComponent;
            
            MTComponent [] children = componentList.getChildren();
            
            List<MTComponent> childrenSortList = new ArrayList<MTComponent>();
            
            List<String> nameSort = new ArrayList<String>();
            
            for (MTComponent child : children) {
                String childName = new String();
                if (child instanceof NavigationBarMenuElement.TextViewNavBarElement) {
                    childName = ((NavigationBarMenuElement.TextViewNavBarElement) child).getModelText();
                    child.setName(childName);
                } else if (child instanceof RamExpendableComponent) {
                    sortChildren((RamExpendableComponent) child);
                    RamRectangleComponent title = ((RamExpendableComponent) child).getTitle();
                    if (title  instanceof NavigationBarMenuElement.TextViewNavBarElement) {
                        childName = ((NavigationBarMenuElement.TextViewNavBarElement) title).getModelText();
                        child.setName(childName);
                    } else {
                        childName = title.getName();
                    }
                } else {
                    childName = child.getName();
                }
                nameSort.add(childName.toUpperCase());
                
                java.util.Collections.sort(nameSort);
                
                int elementPosition = nameSort.indexOf(childName.toUpperCase());
                
                childrenSortList.add(elementPosition, child);
            }
            
            componentList.removeAllChildren();
            
            for (int i = 0; i < childrenSortList.size(); i++) {
                componentList.addChild(childrenSortList.get(i));
            }
            
            if (sortingListMap.containsKey(comp)) {
                sortingListMap.replace(comp, nameSort);
            } else {
                sortingListMap.put(comp, nameSort);
            }
        }
    }
    
    public TextViewNavBarElement createTextViewNavBarElement(EObject element, EObject parentEObject, boolean reuse) {
        TextViewNavBarElement addedElement;
        if (element instanceof NamedElementImpl) {
            addedElement = new TextViewNavBarElement(element, 
                    RamPackage.Literals.NAMED_ELEMENT__NAME);
        } else if (element instanceof NamedElement) {
            addedElement = new TextViewNavBarElement(element, 
                    UcPackage.Literals.NAMED_ELEMENT__NAME);
        } else {
            addedElement = new TextViewNavBarElement(element, 
                    CorePackage.Literals.CORE_NAMED_ELEMENT__NAME);
        }
        TextViewHandlerNavBar elementHandler = new TextViewHandlerNavBar((NavigationBarMenuElement<EObject>) this, 
                parentEObject, reuse);
        addedElement.registerTapProcessor(elementHandler);  
        addedElement.registerTapAndHoldProcessor(elementHandler);
        return addedElement;
    }

    @Override
    public void elementSelected(RamListComponent<T> listComponent, T element) {
        createSubMenu(element, listComponent.getDisplayComponent(element));
    }

    @Override
    public void elementDoubleClicked(RamListComponent<T> listComponent, T element) {
    }

    @Override
    public void elementHeld(RamListComponent<T> listComponent, T element) {
    }

    /**
     * Returns true if the list contains at least one element.
     * 
     * @return true if the list contains at least one element. False otherwise.
     */
    public boolean containsElements() {
        return this.list.size() > 0;
    }
    
    protected boolean checkForDuplicateElement(ILMNode inputNode, NavigationMapping inputILM) {
        return inputNode.equals(this.node) && inputILM.equals(this.ilmInput);
    }
    
    protected String getNameElem() {
        return nameElem;
    }

    /**
     * Stage change listener which close the sub-menu linked to the element when the list is removed of its parent. Used
     * in the case of expandable components.
     * 
     * @author g.Nicolas
     *
     */
    private class InternalStateChangeListener implements StateChangeListener {
        @Override
        public void stateChanged(StateChangeEvent evt) {
            getParentOfType(NavigationBar.class).closeSubMenu(menu);
        }
    }

}
