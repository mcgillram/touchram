package ca.mcgill.sel.ram.ui.components.navigationbar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.mt4j.components.MTComponent;
import org.mt4j.components.TransformSpace;
import org.mt4j.components.visibleComponents.shapes.AbstractShape;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldProcessor;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapProcessor;
import org.mt4j.sceneManagement.Iscene;
import org.mt4j.sceneManagement.transition.FadeTransition;
import org.mt4j.sceneManagement.transition.SlideTransition;
import org.mt4j.util.MTColor;
import org.mt4j.util.math.Vector3D;

import ca.mcgill.sel.classdiagram.ui.scenes.DisplayClassDiagramScene;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.COREFeatureImpactNode;
import ca.mcgill.sel.core.COREFeatureModel;
import ca.mcgill.sel.core.COREImpactNode;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.COREScene;
import ca.mcgill.sel.core.IntraLanguageMapping;
import ca.mcgill.sel.core.NavigationMapping;
import ca.mcgill.sel.core.guinavigation.NavigationMappingHelper;
import ca.mcgill.sel.core.guinavigation.IntraLanguageMappingTree.ILMNode;
import ca.mcgill.sel.core.impl.COREConcernImpl;
import ca.mcgill.sel.core.impl.COREFeatureImpl;
import ca.mcgill.sel.core.impl.COREImpactNodeImpl;
import ca.mcgill.sel.core.navigation.CORENavigationContext;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.AspectMessageView;
import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.MessageView;
import ca.mcgill.sel.ram.impl.AspectImpl;
import ca.mcgill.sel.ram.impl.AspectMessageViewImpl;
import ca.mcgill.sel.ram.impl.MessageViewImpl;
import ca.mcgill.sel.ram.impl.MessageViewReferenceImpl;
import ca.mcgill.sel.ram.impl.OperationImpl;
import ca.mcgill.sel.ram.impl.StateViewImpl;
import ca.mcgill.sel.ram.impl.StructuralViewImpl;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamButton;
import ca.mcgill.sel.ram.ui.components.RamImageComponent;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamRoundedRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamScrollComponent;
import ca.mcgill.sel.ram.ui.components.navigationbar.NavigationBarMenu.NavigationBarNamerFeatureSwitch;
import ca.mcgill.sel.ram.ui.components.navigationbar.NavigationBarMenu.NavigationBarNamerGeneric;
import ca.mcgill.sel.ram.ui.components.navigationbar.NavigationBarMenu.NavigatonBarNamer;
import ca.mcgill.sel.ram.ui.events.listeners.ActionListener;
import ca.mcgill.sel.ram.ui.events.listeners.ITapListener;
import ca.mcgill.sel.ram.ui.layouts.DefaultLayout;
import ca.mcgill.sel.ram.ui.layouts.HorizontalLayoutVerticallyCentered;
import ca.mcgill.sel.ram.ui.layouts.VerticalLayout;
import ca.mcgill.sel.ram.ui.scenes.AbstractConcernScene;
import ca.mcgill.sel.ram.ui.scenes.AbstractImpactScene;
import ca.mcgill.sel.ram.ui.scenes.DisplayAspectScene;
import ca.mcgill.sel.ram.ui.scenes.DisplayConcernEditScene;
import ca.mcgill.sel.ram.ui.scenes.DisplayConcernSelectScene;
import ca.mcgill.sel.ram.ui.scenes.DisplayImpactModelEditScene;
import ca.mcgill.sel.ram.ui.scenes.DisplayImpactModelSelectScene;
import ca.mcgill.sel.ram.ui.scenes.RamAbstractScene;
import ca.mcgill.sel.ram.ui.scenes.SceneCreationAndChangeFactory;
import ca.mcgill.sel.ram.ui.scenes.SceneCreationAndChangeFactory.CurrentMode;
import ca.mcgill.sel.ram.ui.scenes.handler.impl.ConcernEditSceneHandler;
import ca.mcgill.sel.ram.ui.scenes.handler.impl.ConcernSelectSceneHandler;
import ca.mcgill.sel.ram.ui.scenes.handler.impl.DisplayAspectSceneHandler;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.GUIConstants;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.views.AbstractView;
import ca.mcgill.sel.ram.ui.views.GenericSplitView;
import ca.mcgill.sel.ram.ui.views.handler.BaseHandler;
import ca.mcgill.sel.ram.ui.views.impact.FeatureImpactView;
import ca.mcgill.sel.ram.ui.views.message.MessageViewView;
import ca.mcgill.sel.ram.ui.views.state.StateDiagramView;
import ca.mcgill.sel.ram.ui.views.structural.ClassifierView;
import ca.mcgill.sel.ram.ui.views.structural.CompositionSplitEditingView;
import ca.mcgill.sel.ram.ui.views.structural.StructuralDiagramView;
import ca.mcgill.sel.restif.ui.scenes.DisplayRestTreeScene;
import ca.mcgill.sel.usecases.UseCase;
import ca.mcgill.sel.usecases.UseCaseModel;
import ca.mcgill.sel.usecases.ui.scenes.DisplayUseCaseModelScene;
import ca.mcgill.sel.usecases.ui.scenes.handler.impl.DisplayUseCaseModelSceneHandler;
import ca.mcgill.sel.usecases.ui.views.UseCaseDetailView;
import processing.core.PImage;

/**
 * A navigation bar showing the location of the user in the application and helps him to navigate into the app.
 * A back button is added to go back to the previous scene which is saved in a stack.
 * It's composed of a {@link NavigationBarSection} stack.
 *
 * @author Andrea
 */
public final class NavigationBar extends RamRectangleComponent {
    
    /**
     * Handler of the navigation bar attached to the back menu.
     * handles the single tap to pop sections and also the hold down to hide the bar
     * 
     * @author andrea
     */
    private final class BackMenuHandler extends BaseHandler implements ITapListener {
    
        /**
         * Constructs the menu handler.
         */
        private BackMenuHandler() {
        }
    
        @Override
        public boolean processTapEvent(TapEvent tapEvent) {
            if (tapEvent.isTapped() && isEnabled()) {
                closeMenu();
                if (!previousScenesForReuse.isEmpty() && RamApp.getApplication().getActiveScene().getPreviousScene()
                        .equals(previousScenesForReuse.lastElement())) {
                    previousScenesForReuse.pop();
                    List<NavigationBarSection> sectionsToAdd = sectionsForReuse.pop();
                    while (sections.size() > 1) {
                        NavigationBarSection removeChild = sections.pop();
                        topContainer.removeChild(removeChild);
                    }
                    for (NavigationBarSection sec : sectionsToAdd) {
                        sections.add(sec);
                        topContainer.addChild(sec);
                    }
                    if (sectionsForReuse.size() == 0) {
                        inReuse = false;
                        NavigationBarSection reuseSection = sections.get(0);
                        sections.remove(reuseSection);
                        topContainer.removeChild(reuseSection);
                    }
                    reuseObjects.pop();
                } else if (RamApp.getApplication().getActiveScene().getPreviousScene().equals(
                        previousSceneForExtends)) {
                    topContainer.removeChild(expandPreviousConcern);
                }
                COREConcern currentConcern = CORENavigationContext.getCurrentConcern();
                if (RamApp.getApplication().getCurrentScene() instanceof DisplayConcernSelectScene) {
                    concernSelectMode();
                    ConcernSelectSceneHandler handler = (ConcernSelectSceneHandler) ((DisplayConcernSelectScene) RamApp
                            .getApplication().getCurrentScene()).getHandler();
                    handler.switchToPreviousScene((DisplayConcernSelectScene) RamApp.getApplication()
                            .getCurrentScene());
                }
                if (RamApp.getApplication().getCurrentScene() instanceof DisplayImpactModelSelectScene) {
                    DisplayImpactModelSelectScene scene = (DisplayImpactModelSelectScene) RamApp.getApplication()
                            .getCurrentScene();
                    scene.switchToPreviousScene();
                }
                if (sections.size() > 0) {
                    if (RamApp.getActiveScene() instanceof DisplayConcernEditScene) {
                        ConcernEditSceneHandler a = (ConcernEditSceneHandler) RamApp.getActiveScene().getHandler();
                        a.switchToHome((DisplayConcernEditScene) RamApp.getActiveScene());
                    } else if (RamApp.getActiveScene() instanceof DisplayClassDiagramScene) {
                        DisplayClassDiagramScene scene = (DisplayClassDiagramScene) RamApp.getActiveScene();
                        popSection();
                        
                        if (scene.getCurrentView() instanceof GenericSplitView) {  
                            scene.getCurrentView().destroy();
                        }
                        
                        scene.getHandler().switchToConcern(scene);
                        return true;
                    } else if (RamApp.getActiveScene() instanceof DisplayRestTreeScene) {
                        DisplayRestTreeScene scene = (DisplayRestTreeScene) RamApp.getActiveScene();
                        popSection();

                        if (scene.getCurrentView() instanceof GenericSplitView) {  
                            scene.getCurrentView().destroy();
                        }
                        
                        scene.getHandler().switchToConcern(scene);
                        
                        return true;                            
                    } else if (RamApp.getActiveScene() instanceof DisplayUseCaseModelScene) {
                        DisplayUseCaseModelScene scene = (DisplayUseCaseModelScene) RamApp.getActiveScene();
                        popSection();
                        DisplayUseCaseModelSceneHandler handler =
                                (DisplayUseCaseModelSceneHandler) RamApp.getActiveScene().getHandler();
                        if (scene.getCurrentView() instanceof UseCaseDetailView) {
                            scene.switchToPreviousView();
                        } else if (scene.getCurrentView() instanceof GenericSplitView) {
                            scene.getCurrentView().destroy();
                            scene.getHandler().switchToConcern(scene); 
                        } else {
                            if (scene.getPreviousScene() instanceof DisplayUseCaseModelScene) {
                                DisplayUseCaseModelScene previousScene = 
                                        (DisplayUseCaseModelScene) scene.getPreviousScene();
                                UseCaseModel previousModel = previousScene.getUseCaseDiagram();
                                if (topContainer.containsChild(returnToConcernButton)) {
                                    topContainer.removeAllChildren();

                                    handleSections();
                                    
                                    pushSection(currentConcern.getName(), getGenericNamerBase(), 
                                            currentConcern);
                                    handler.switchBackTo(RamApp.getActiveScene());

                                    if (previousModel.getName().contains(WOVEN_DELIMITER)) {
                                        weavingInterfaceMode();
                                    }
                                } else {
                                    if (previousModel.getName().contains(WOVEN_DELIMITER) && !inReuse) {
                                        weavingInterfaceMode();
                                    }
                                    handler.switchToConcern(RamApp.getActiveScene());
                                }
                            } else {
                                if (scene.getPreviousScene() instanceof DisplayConcernSelectScene) {
                                    scene.setTransition(
                                            new SlideTransition(RamApp.getApplication(), 700, false));
                                    handler.switchToConcern(scene);
                                } else {
                                    handler.switchToConcern(RamApp.getActiveScene());
                                }
                            }
                        }

                        return true;
                    } else if (RamApp.getActiveScene() instanceof DisplayAspectScene) {
                        selected = null;
                         
                        DisplayAspectScene scene = (DisplayAspectScene) RamApp.getActiveScene();
                        DisplayAspectSceneHandler handler =
                                (DisplayAspectSceneHandler) RamApp.getActiveScene().getHandler();
                        if (scene.getCurrentView() instanceof StructuralDiagramView) {
                            if (scene.getPreviousScene() instanceof DisplayImpactModelEditScene) {
                                popSection();
                                SceneCreationAndChangeFactory.getFactory().changeSceneAndUpdate(
                                        scene.getPreviousScene());
                                RamApp.getApplication().closeAspectScene(scene);
                            } else {
                                if (scene.getPreviousScene() instanceof DisplayAspectScene) {
                                    DisplayAspectScene previousScene = 
                                            (DisplayAspectScene) scene.getPreviousScene();
                                    Aspect previousAspect = previousScene.getAspect();
                                    if (topContainer.containsChild(returnToConcernButton)) {
                                        topContainer.removeAllChildren();

                                        handleSections();

                                        pushSection(currentConcern.getName(), getGenericNamerBase(), 
                                                currentConcern);
                                        handler.switchBackTo((DisplayAspectScene) RamApp.getActiveScene());

                                        if (previousAspect.getName().contains(WOVEN_DELIMITER)) {
                                            weavingInterfaceMode(previousAspect);
                                        }
                                    } else {
                                        if (previousAspect.getName().contains(WOVEN_DELIMITER) && !inReuse) {
                                            weavingInterfaceMode(previousAspect);
                                        }
                                        handler.fastSwitchToConcern((DisplayAspectScene) RamApp.getActiveScene());
                                    }
                                } else {
                                    if (scene.getPreviousScene() instanceof DisplayConcernSelectScene) {
                                        scene.setTransition(
                                                new SlideTransition(RamApp.getApplication(), 700, false));
                                        handler.switchToConcern(scene);
                                    } else {
                                        handler.fastSwitchToConcern((DisplayAspectScene) RamApp.getActiveScene());
                                    }
                                }
                            }
                        } else if (scene.getCurrentView() instanceof MessageViewView) {
                            if (topContainer.containsChild(returnToConcernButton)) {
                                scene.switchToPreviousView();
                            } else {
                                scene.switchToPreviousView();
                            }
                        } else if (scene.getCurrentView() instanceof StateDiagramView) {
                            scene.switchToPreviousView();
                        } else if (scene.getCurrentView() instanceof CompositionSplitEditingView) {
                            handler.closeSplitView(scene);
                        } else if (scene.getCurrentView() instanceof GenericSplitView) {
                            scene.getCurrentView().destroy();
                            scene.getHandler().switchToConcern(scene); 
                        }
                    } else if (RamApp.getActiveScene() instanceof DisplayImpactModelEditScene) {
                        AbstractImpactScene currentScene = (AbstractImpactScene) RamApp.getActiveScene();
                        currentScene.switchToPreviousScene();
                        if (currentScene.getPreviousScene() instanceof DisplayConcernEditScene) {
                            popSection();
                        }
                    }
                }
            }
            return true;
        }
    }

    /**
     * Name delimiter for woven models.
     */
    private static final String WOVEN_DELIMITER = "_";

    private static NavigationBar nav = new NavigationBar();

    private static final float BACK_BUTTON_SIZE = 55;
    private static final String MENU_ARROW_KEY = "arrow";

    private Stack<NavigationBarSection> sections;
    private RamRectangleComponent topContainer;
    private RamRectangleComponent menuContainer;
    private RamRectangleComponent rootContainer;
    private RamRoundedRectangleComponent backButton;
    private RamImageComponent image;
    private RamRectangleComponent acrossNotationContainerF;
    private RamRectangleComponent acrossNotationContainerRM;
    private RamRectangleComponent withinNotationContainer;
    private NavigationBarSection uniqueMenuSection;
    private NavigationBarMenu uniqueMenu;
    private Map<NavigationBarMenu, NavigationBarMenu> subMenus;
    private Deque<RamRectangleComponent> listOfWithin = new LinkedList<>();
    private Classifier selected;
    private boolean containsWeaveCrumb;
    private RamButton returnToConcernButton;

    private COREScene sceneForConflictRes;
    
    private String currentLanguageRole;
    /**
     * Object in current scene which is currently lit up.
     */
    private AbstractShape currentlySelected;
    /**
     * The previous colour of the currently selected object on screen. Used when we unselect that object.
     */
    private MTColor previousColor;
    
    private Stack<Iscene> previousScenesForReuse;
    
    private Iscene previousSceneForExtends;
    
    private Stack<List<NavigationBarSection>> sectionsForReuse;
    
    private RamImageComponent expandPreviousConcern;
    
    private Stack<EObject> reuseObjects;
    private boolean inReuse;
    /**
     * Constructs a navigation bar and initialize it with no sections.
     */
    private NavigationBar() {
        super(new VerticalLayout());
        
        // CORENavigationContext.setCurrentModel(null);
        // CORENavigationContext.setCurrentScene(null);

        setPickable(false);
        sections = new Stack<NavigationBarSection>();
        this.subMenus = new HashMap<NavigationBarMenu, NavigationBarMenu>();
        rootContainer = new RamRectangleComponent(new HorizontalLayoutVerticallyCentered(5f));
        // Ensure that the bar always appears the same way (horizontally centered) independent of the contents.
        rootContainer.setMinimumHeight(74f);
        rootContainer.setPickable(false);
        topContainer = new RamRectangleComponent(new HorizontalLayoutVerticallyCentered(5f));
        topContainer.setPickable(false);
        menuContainer = new RamRectangleComponent(new DefaultLayout());
        menuContainer.setPickable(false);
        RamScrollComponent scrollContainer = new RamScrollComponent(menuContainer);
        scrollContainer.setPickable(false);

        scrollContainer.setLayout(new DefaultLayout(0, RamApp.getApplication().getHeight() - BACK_BUTTON_SIZE));
        
        image = new RamImageComponent(Icons.ICON_MENU_BACK, MTColor.WHITE);

        backButton = new RamRoundedRectangleComponent(BACK_BUTTON_SIZE * 2, BACK_BUTTON_SIZE, BACK_BUTTON_SIZE / 2);
        backButton.addChild(image);
        backButton.setAutoMaximizes(false);
        backButton.setAutoMinimizes(false);
        backButton.setNoStroke(true);
        backButton.setNoFill(false);
        backButton.setFillColor(Colors.MENU_BACKGROUND_COLOR);
        backButton.registerInputProcessor(new TapProcessor(RamApp.getApplication()));
        backButton.registerInputProcessor(new TapAndHoldProcessor(RamApp.getApplication(),
                GUIConstants.TAP_AND_HOLD_DURATION));
        BackMenuHandler menuHandler = new BackMenuHandler();
        backButton.addGestureListener(TapProcessor.class, menuHandler);
        backButton.addGestureListener(TapAndHoldProcessor.class, menuHandler);
        
        image.setPickable(false);
        image.setSizeLocal(BACK_BUTTON_SIZE - 20, BACK_BUTTON_SIZE - 20);
        float posX = (float) (0.75 * (backButton.getWidth() - image.getWidth()));
        float posY = (float) (0.5 * (backButton.getHeight() - image.getHeight()));
        image.setPositionRelativeToParent(new Vector3D(posX, posY));

        Vector3D navigationBarPosition = getPosition(TransformSpace.GLOBAL);
        navigationBarPosition.x -= BACK_BUTTON_SIZE;
        navigationBarPosition.y -= 5;
        setPositionGlobal(navigationBarPosition);

        rootContainer.addChild(backButton);
        rootContainer.addChild(topContainer);
        addChild(rootContainer);
        addChild(scrollContainer);
        
        sectionsForReuse = new Stack<List<NavigationBarSection>>();
        previousScenesForReuse = new Stack<Iscene>();
        reuseObjects = new Stack<EObject>();
        inReuse = false;
    }

    /**
     * Used to retrieve the navigation bar singleton.
     * 
     * @return the actual Navigation Bar
     */
    public static NavigationBar getInstance() {
        return nav;
    }
  
    /**
     * To push a section removing similar sections from the navigation bar.
     * 
     * @param icon icon of the section (not used)
     * @param label name of the section
     * @param namer used to create the actual section
     * @param eObject content of the section
     * @param <T> type of the {@link NavigationBarSection} related eObject
     */
    public <T> void pushSectionJumpGeneric(PImage icon, String label, NavigatonBarNamer<T> namer, T eObject) {
        if (eObject instanceof COREFeature) {
            while (!sections.lastElement().retrieveTypeOfSection().equals(COREConcernImpl.class)) {
                popSection();
            }
        }
        pushSection(icon, label, namer, eObject);
    }
    
    /**
     * Should return whether there is section with a given object as its base object already present in the navbar.
     * @param model the object to check for
     * @return true if there is a section false if not.
     */
    public boolean checkForSectionWithObject(EObject model) {
        for (NavigationBarSection section : sections) {
            if (section.getSectionEObject().equals(model)) {
                return true;
            }
//            while (!sections.lastElement().retrieveTypeOfSection().equals(MessageImpl.class)) {
//                popSection();
//            }
        }
        return false;
    }

    /**
     * Add a new {@link NavigationBarSection} with an icon and a menu, in the section stack.
     * It closes opened menu.
     * 
     * @param icon - the icon in the {@link NavigationBarSection}
     * @param label - label of the {@link NavigationBarSection}
     * @param namer - namer of the {@link NavigationBarSection} for the menu creation.
     * @param eObject - eObject related to the {@link NavigationBarSection}
     * @param <T> - type of the {@link NavigationBarSection} related eObject.
     */
    public <T> void pushSection(PImage icon, String label, NavigatonBarNamer<T> namer, T eObject) {
        NavigationBarSection section = new NavigationBarSection(icon, label, namer, eObject);
        
        if (namer == null) {
            section.hideArrows();
        }
        sections.push(section);
        topContainer.addChild(section);
        closeMenu();

        if (eObject.getClass().equals(COREFeatureImpl.class)) {
            acrossNotationContainerF = new RamRectangleComponent();
            acrossNotationContainerF.setLayout(new VerticalLayout());
            RamImageComponent acrossModelNotation = new RamImageComponent(Icons.ICON_COLON_ACROSS_MODEL, MTColor.WHITE);
            acrossModelNotation.setSizeLocal(8, 24);
            acrossModelNotation.setAutoMaximizes(false);
            acrossModelNotation.setAutoMinimizes(false);
            acrossModelNotation.setFillColor(MTColor.RED);
            acrossNotationContainerF.addChild(acrossModelNotation);
            topContainer.addChild(acrossNotationContainerF);

        }
    }
    
    //place holder for pushing aspect section
    public <T> void pushSectionAspect(PImage icon, String label, NavigatonBarNamer<T> namer, T eObject) {
        pushSection(icon, label, namer, eObject);
    }
    
  //place holder for pushing feature section
    public <T> void pushSectionFeature(PImage icon, String label, NavigatonBarNamer<T> namer, T eObject) {
        pushSection(icon, label, namer, eObject);
    }
    
  //place holder for pushing impact model section
    public <T> void pushSectionImpact(PImage icon, String label, NavigatonBarNamer<T> namer, T eObject) {
        while (!sections.lastElement().retrieveTypeOfSection().equals(COREConcernImpl.class)) {
            popSection();
        }
        
        pushSection(icon, label, namer, eObject);
    }
  //place holder for pushing structural view section
    public <T> void pushSectionStructView(PImage icon, String label, NavigatonBarNamer<T> namer, T eObject) {
        pushSection(icon, label, namer, eObject);
    }
  //place holder for pushing message view section
    public <T> void pushSectionMsgView(EObject eObject) {
        String messageViewName = "";
        if (eObject instanceof MessageView) {
            MessageView messageView = (MessageView) eObject;
            messageViewName = messageView.getSpecifies().getName();
        } else if (eObject instanceof AspectMessageView) {
            AspectMessageView messageView = (AspectMessageView) eObject;
            messageViewName = messageView.getName();
        }
        pushSection(Icons.ICON_MENU_MESSAGE_VIEW, messageViewName, getGenericNamerBase(), eObject);
    }
    
    public void pushSectionUseCaseDetail(UseCase useCase) {
        pushSection(Icons.ICON_MENU_MESSAGE_VIEW, useCase.getName(), null, useCase);
    }
    
    public <T> void pushSectionClassDiagram(PImage icon, String label, NavigatonBarNamer<T> namer, T eObject) {
        pushSection(icon, label, namer, eObject);
    }
    
    public <T> void pushSectionEnvironmentModel(PImage icon, String label, NavigatonBarNamer<T> namer, T eObject) {
        pushSection(icon, label, namer, eObject);
    }
    
    public <T> void pushSectionReuse(NavigatonBarNamer<T> namer) {
        EObject reuseObject = reuseObjects.get(0);
        NavigationBarSection section = new NavigationBarSection(namer, reuseObject);
        
        sections.push(section);
        topContainer.addChild(section);
        closeMenu();
    }

    /**
     * Add a new {@link NavigationBarSection} without icon but with a menu, in the section stack.
     * It closes opened menu.
     * 
     * @param label - label of the {@link NavigationBarSection}
     * @param namer - namer of the {@link NavigationBarSection} for the menu creation.
     * @param eObject - eObject related to the {@link NavigationBarSection}
     * @param <T> - type of the {@link NavigationBarSection} related eObject.
     */
    public <T> void pushSection(String label, NavigatonBarNamer<T> namer, T eObject) {
        pushSection(null, label, namer, eObject);
        // VARIANT FOR NO ICON, meaning the Concern section, the FIRST in the navigation bar
    }

    /**
     * Remove the lately added section of the stack and close shown menus.
     * BASICALLY WHEN YOU CLICK ON BACK BUTTON
     */
    public void popSection() {
        if (sections.size() > 0) {
            NavigationBarSection section = sections.pop();
            if (section.retrieveTypeOfSection().equals(COREFeatureImpl.class)) {
                acrossNotationContainerF.removeAllChildren();
                topContainer.removeChild(acrossNotationContainerF);

            } else if (section.retrieveTypeOfSection().equals(StructuralViewImpl.class)
                    || section.retrieveTypeOfSection().equals(MessageViewImpl.class)
                    || section.retrieveTypeOfSection().equals(StateViewImpl.class)
                    || section.retrieveTypeOfSection().equals(AspectMessageViewImpl.class)
                    || section.retrieveTypeOfSection().equals(OperationImpl.class)
                    || section.retrieveTypeOfSection().equals(MessageViewReferenceImpl.class)
                    || section.retrieveTypeOfSection().equals(COREImpactNodeImpl.class)) {
                if (listOfWithin.size() > 1) {
                    RamRectangleComponent t = (RamRectangleComponent) listOfWithin.pop();
                    t.removeAllChildren();
                } else {
                    if (!listOfWithin.isEmpty()) {
                        RamRectangleComponent t = (RamRectangleComponent) listOfWithin.pop();
                        t.removeAllChildren();
                        topContainer.removeChild(withinNotationContainer);
                    }
                }
            } else if (section.retrieveTypeOfSection().equals(AspectImpl.class)) {
                // second check enabled only when peculiar situation of Realization Model with NO Feature happens
                if (topContainer.containsChild(acrossNotationContainerF)) {
                    acrossNotationContainerF.removeAllChildren();
                    topContainer.removeChild(acrossNotationContainerF);
                }
            }
            if (sections.size() > 0) {
                if (sections.lastElement().retrieveTypeOfSection().equals(COREFeatureImpl.class)) {
                    popSection();
                }
                sections.peek().removeUnusedExpandButton();
            }
            topContainer.removeChild(section);
            

            closeMenu();
        }
    }

    /**
     * Pop to a section when the LABEL is tapped.
     * Implementing the "Jump" action, used by a user by tapping on a section to which he wants to go.
     * 
     * @param wantedSectionObj the object representing the section we wish to jump to
     */
    public void popSection(EObject wantedSectionObj) {
        while (sections.size() > 1) {
            if (sections.lastElement().getSectionEObject().equals(wantedSectionObj)) {
                break;
            }
            while (!sections.lastElement().getSectionEObject().equals(wantedSectionObj)) {
                NavigationBarSection section = sections.pop();

                if (section.retrieveTypeOfSection().equals(COREFeatureImpl.class)) {

                    acrossNotationContainerF.removeAllChildren();
                    topContainer.removeChild(acrossNotationContainerF);

                } else if (section.retrieveTypeOfSection().equals(StructuralViewImpl.class)
                        || section.retrieveTypeOfSection().equals(MessageViewImpl.class)
                        || section.retrieveTypeOfSection().equals(StateViewImpl.class)
                        || section.retrieveTypeOfSection().equals(AspectMessageViewImpl.class)
                        || section.retrieveTypeOfSection().equals(OperationImpl.class)
                        || section.retrieveTypeOfSection().equals(MessageViewReferenceImpl.class)) {
                    if (listOfWithin.size() > 1) {
                        RamRectangleComponent t = (RamRectangleComponent) listOfWithin.pop();
                        t.removeAllChildren();
                    } else {
                        if (!listOfWithin.isEmpty()) {
                            RamRectangleComponent t = (RamRectangleComponent) listOfWithin.pop();
                            t.removeAllChildren();
                            topContainer.removeChild(withinNotationContainer);
                        } else {
                            // means we're in RM : SD so the ":" must be removed
                            acrossNotationContainerRM.removeAllChildren();
                            topContainer.removeChild(acrossNotationContainerRM);
                        }
                    }

                    RamApp.getActiveAspectScene().switchToView(RamApp.getActiveAspectScene()
                            .getStructuralDiagramView());
                } else if (section.retrieveTypeOfSection().equals(AspectImpl.class)
                        && acrossNotationContainerRM != null) {
                    acrossNotationContainerRM.removeAllChildren();
                    topContainer.removeChild(acrossNotationContainerRM);
                } else if (section.retrieveTypeOfSection().equals(COREImpactNodeImpl.class)) {
                    acrossNotationContainerRM.removeAllChildren();
                    topContainer.removeChild(acrossNotationContainerRM);
                    AbstractImpactScene aI = (AbstractImpactScene) RamApp.getActiveScene();
                    aI.switchToPreviousScene();
                }

                topContainer.removeChild(section);

                closeMenu();
            }
        }
    }

    /**
     * Method to specifically return back to the Concern scene removing every other scene that could be present.
     * 
     * @param wantedSection COREConcern class
     * @param obj COREConcern EObject
     */
    public void popSection(Object wantedSection, EObject obj) {
        if (!sections.lastElement().retrieveTypeOfSection().equals(wantedSection)) {

            if (acrossNotationContainerF != null) {
                acrossNotationContainerF.removeAllChildren();
                topContainer.removeChild(acrossNotationContainerF);
            }
            if (acrossNotationContainerRM != null) {
                acrossNotationContainerRM.removeAllChildren();
                topContainer.removeChild(acrossNotationContainerRM);
            }
            while (sections.size() > 1) {
                topContainer.removeChild(sections.pop());
            }

            // CHECKSTYLE:IGNORE AnonInnerLength: Okay here.
            RamApp.getApplication().invokeLater(new Runnable() {
                @Override
                public void run() {
                    // TODO: Here getCurrentScene could be used instead of for loop?
                    for (Iscene scene : RamApp.getApplication().getScenes()) {
                        if (scene instanceof DisplayAspectScene) {
                            DisplayAspectScene aspectScene = (DisplayAspectScene) scene;
                            if (aspectScene.getPreviousScene() instanceof DisplayConcernEditScene) {
                                DisplayAspectSceneHandler handler = 
                                        (DisplayAspectSceneHandler) aspectScene.getHandler();
                                ((RamAbstractScene<?>) RamApp.getApplication().getCurrentScene())
                                        .setTransition(new SlideTransition(RamApp.getApplication(), 700, false));
                                handler.fastSwitchToConcern((DisplayAspectScene) scene);
                            }
                        } else if (scene instanceof DisplayImpactModelEditScene) {
                            DisplayImpactModelEditScene impactScene = (DisplayImpactModelEditScene) scene;
                            if (impactScene.getPreviousScene() instanceof DisplayConcernEditScene) {
                                impactScene.switchToPreviousScene();
                            }
                        }
                    }
                    
                    // TODO: Can this be within the above for loop?
                    for (Iscene scene : RamApp.getApplication().getScenes()) {
                        if (scene instanceof DisplayAspectScene) {
                            if (RamApp.getApplication().getCurrentScene().equals(scene)) {
                                RamApp.getApplication().destroySceneAfterTransition(scene);
                            } else {
                                RamApp.getApplication().closeAspectScene((DisplayAspectScene) scene);
                            }
                        }
                    }
                }
            });
        }
    }

    /**
     * Opens the given menu for the given section at the given position. If there is already a menu, it will be
     * replaced (removed).
     * 
     * @param section - related section of the given menu.
     * @param menu - menu to show.
     * @param position - position of the menu.
     */
    public void openMenu(final NavigationBarSection section, final NavigationBarMenu menu, final Vector3D position) {
        closeMenu();
        
        RamApp.getApplication().invokeLater(new Runnable() {

            @Override
            public void run() {
                uniqueMenuSection = section;
                uniqueMenu = menu;
                uniqueMenuSection.updateExpandButton(true);
                menuContainer.addChild(menu, true);
                position.y += topContainer.getHeightXY(TransformSpace.GLOBAL) / 2.4;
                position.x = uniqueMenuSection.getPosition(TransformSpace.GLOBAL).x;
                uniqueMenu.setPositionGlobal(position);
                menuContainer.updateLayout();
            }
        });
    }

    /**
     * Close the currently shown menu and its sub-menus.
     */
    public void closeMenu() {
        RamApp.getApplication().invokeLater(new Runnable() {

            @Override
            public void run() {
                if (uniqueMenuSection != null && uniqueMenu != null) {
                    closeSubMenu(uniqueMenu);
                    uniqueMenuSection.updateExpandButton(false);
                    uniqueMenuSection.updateUpArrowSwitch(false);
                    menuContainer.removeChild(uniqueMenu);
                    uniqueMenuSection = null;
                    uniqueMenu = null;
                    if (selected != null) {

                        ClassifierView<?> w = RamApp.getActiveAspectScene()
                                .getStructuralDiagramView().getClassViewOf(selected);
                        w.setFillColor(Colors.DEFAULT_ELEMENT_FILL_COLOR);
                        selected = null;
                    }
                }
            }
        });

        closeMenuFanOut();
    }

    /**
     * Used to open the secondary menu for the Filtered Switch action.
     * 
     * @param section of the bar to which the filtered switch menu should appear
     * @param menu the actual menu we are in
     * @param position position for the Menu to appear
     */
    public void openMenuFanOut(final NavigationBarSection section,
            final NavigationBarMenu menu,
            final Vector3D position) {
        closeMenu();
        closeMenuFanOut();
        if (selected != null) {
            ClassifierView<?> w = RamApp.getActiveAspectScene()
                    .getStructuralDiagramView().getClassViewOf(selected);
            w.setFillColor(Colors.DEFAULT_ELEMENT_FILL_COLOR);
            selected = null;
        }

        RamApp.getApplication().invokeLater(new Runnable() {

            @Override
            public void run() {
                uniqueMenuSection = section;
                uniqueMenu = menu;
                uniqueMenuSection.updateUpArrowSwitch(true);
                menuContainer.addChild(menu, true);
                position.y += topContainer.getHeightXY(TransformSpace.GLOBAL) / 2.4;
                position.x = uniqueMenuSection.getPosition(TransformSpace.GLOBAL).x;
                uniqueMenu.setPositionGlobal(position);
                menuContainer.updateLayout();
            }
        });
    }

    /**
     * Method for closing the Filtered Switch menu created previously.
     */
    public void closeMenuFanOut() {
        RamApp.getApplication().invokeLater(new Runnable() {

            @Override
            public void run() {
                if (uniqueMenuSection != null && uniqueMenu != null) {
                    closeSubMenu(uniqueMenu);
                    uniqueMenuSection.updateExpandButton(false);
                    uniqueMenuSection.updateUpArrowSwitch(false);
                    menuContainer.removeChild(uniqueMenu);
                    uniqueMenuSection = null;
                    uniqueMenu = null;
                }
            }
        });
    }

    /**
     * Opens the given sub-menu related to the given menu at the given position. If a sub-menu is already shown for the
     * given menu, it will be replaced (removed).
     * 
     * @param menu - related menu of the given sub-menu.
     * @param subMenu - the sub-menu to show.
     * @param position - global position of the sub-menu.
     */
    public void openSubMenu(NavigationBarMenu menu, NavigationBarMenu subMenu, Vector3D position) {
        closeSubMenu(menu);
        menuContainer.addChild(subMenu, true);
        position.x = menu.getPosition(TransformSpace.GLOBAL).x + menu.getWidthXY(TransformSpace.GLOBAL);
        subMenu.setPositionGlobal(position);
        // --- Arrow
        RamImageComponent arrow = new RamImageComponent(Icons.ICON_NAVIGATION_COLLAPSE, MTColor.WHITE);
        arrow.setFillColor(Colors.MENU_BACKGROUND_COLOR);
        arrow.setSizeLocal(35, 35);
        menuContainer.addChild(arrow);
        Vector3D arrowPosition = position;
        arrowPosition.y -= arrow.getHeightXY(TransformSpace.GLOBAL) / 2;
        arrowPosition.x -= 2 * arrow.getWidth() / 3 - 2;
        arrow.setPositionGlobal(position);
        // ----
        menu.setUserData(MENU_ARROW_KEY, arrow);
        menuContainer.updateLayout();
        subMenus.put(menu, subMenu);
    }

    /**
     * Closes the shown sub-menu of the given menu if there is one. In this case it will replace it (remove) and do the
     * same for its sub-menus.
     * 
     * @param menu - related menu of the sub-menu to close.
     */
    public void closeSubMenu(final NavigationBarMenu menu) {
        NavigationBarMenu subMenu = subMenus.get(menu);
        if (subMenu != null) {
            closeSubMenu(subMenu);
            menuContainer.removeChild((RamImageComponent) menu.getUserData(MENU_ARROW_KEY));
            menuContainer.removeChild(subMenu, true);
            subMenus.remove(menu);
        }

    }
    
    public NavigatonBarNamer<EObject> getGenericNamerBase() {
        NavigatonBarNamer<EObject> currentGenericNamer = new NavigationBarMenu.NavigatonBarNamer<EObject>() {
                        
            @Override
            public void initMenu(NavigationBarMenu menu, final EObject object) {
                
                ILMNode rootNode = NavigationMappingHelper.getInstance()
                        .getMappingsForModel(object).getRootNode();
                
                rootNode.getIntraLanguageMappingTree().setNavigationBarMenu(menu);
                
                Map<NavigationMapping, String> typesOfChildrenMap = new HashMap<NavigationMapping, String>();
                for (ILMNode child : rootNode.getChildren()) {
                    if (child.getILM() instanceof IntraLanguageMapping) {
                        List<EReference> newHops = ((IntraLanguageMapping) child.getILM()).getHops();
                        String curName = newHops.get(newHops.size() - 1).getName();
                        if (!typesOfChildrenMap.containsKey(child.getILM())) {
                            IntraLanguageMapping mapping = (IntraLanguageMapping) child.getILM();
                            if (mapping.getName() != null) {
                                typesOfChildrenMap.put(mapping, mapping.getName());
                            } else {
                                typesOfChildrenMap.put(mapping, curName);
                            }
                        }
                    } else {
                        if (child.getInterMappingName() != null) {
                            typesOfChildrenMap.put(child.getILM(), child.getInterMappingName());
                        }
                    }
                }
                for (NavigationMapping map : typesOfChildrenMap.keySet()) {
                    System.out.println(typesOfChildrenMap.get(map));
                    menu.addMenuElementGeneric(typesOfChildrenMap.get(map), rootNode, getGenericNamerInner(), 
                            null, map);
                }
            }    
        };
        return currentGenericNamer;
    }
    
    public NavigationBarNamerGeneric<EObject> getGenericNamerInner() {
        NavigationBarNamerGeneric<EObject> currentGenericNamer = new NavigationBarMenu.NavigationBarNamerGeneric<
                EObject>() {
            
            private boolean reuse;
            @Override
            public void initMenu(NavigationBarMenu menu, final EObject object) {
                if (reuse) {
                    handleReuseNamerActions(object);
                } else if (RamApp.getActiveScene() instanceof AbstractConcernScene || object instanceof COREFeature) {
                    handleFeatureModelNamerActions(object);
                } else if (RamApp.getActiveScene() instanceof AbstractImpactScene) { 
                    handleImpactModelNamerActions(object);
                } else if (!(object instanceof COREFeatureImpactNode)) {
                    if (!toggleGenericSelection(object, null)) {
                        COREExternalArtefact extArtefact = COREArtefactUtil.getReferencingExternalArtefact(object);
                        EObject rootObj = extArtefact.getRootModelElement();
                        if (rootObj.equals(RamApp.getActiveScene().getSceneRootModelObject())) {
                            SceneCreationAndChangeFactory.getFactory().switchViewAndUpdateNavbar(object);
                            
                        } else {
                            RamAbstractScene<?> currentScene = RamApp.getActiveScene(); 
                            CurrentMode mode = SceneCreationAndChangeFactory.getFactory().getCurrentMode();
                            currentScene.setTransition(new SlideUpDownTransition(RamApp.getApplication(), 500, false));
                            SceneCreationAndChangeFactory.getFactory().navigateToModel(object, mode);
                        }
                    }                
                }
            }
            
            @Override
            public void setReuse(boolean reuse) {
                this.reuse = reuse;
            }
        };
        
        return currentGenericNamer;
    }
    
    public void handleReuseNamerActions(EObject object) {
        if (object instanceof COREExternalArtefact) {
            COREConcern reuseConcern = ((COREExternalArtefact) object).getCoreConcern();
            COREConcern currentConcern = CORENavigationContext.getCurrentConcern();
            if (reuseConcern.equals(currentConcern)) {
                RamAbstractScene<?> currentRAMScene = RamApp.getActiveScene(); 
                CurrentMode mode = SceneCreationAndChangeFactory.getFactory().getCurrentMode();
                currentRAMScene.setTransition(new SlideUpDownTransition(RamApp.getApplication(), 500, false));
                SceneCreationAndChangeFactory.getFactory().navigateToModel(object, mode);
                previousSceneForExtends = RamApp.getActiveScene();

            } else {
                List<NavigationBarSection> sectionsForReturn = new ArrayList<NavigationBarSection>();
                if (inReuse) {
                    for (int i = 1; i < sections.size(); i++) {
                        sectionsForReturn.add(sections.get(i));
                    }
                    
                    sectionsForReuse.add(sectionsForReturn);
                } else {
                    for (int i = 0; i < sections.size(); i++) {
                        sectionsForReturn.add(sections.get(i));
                    }
                    
                    sectionsForReuse.add(sectionsForReturn);
                }
                inReuse = true;
                while (!(sections.isEmpty())) {
                    popSection();
                }
                EObject currentObject = SceneCreationAndChangeFactory.getFactory()
                        .getModelFromScene(RamApp.getActiveScene());
                reuseObjects.add(currentObject);
                pushSectionReuse(getReuseNamer());
                
                previousScenesForReuse.push(RamApp.getActiveScene());
                
                pushSection(reuseConcern.getName(), getGenericNamerBase(), reuseConcern);
                
                RamAbstractScene<?> currentScene = RamApp.getActiveScene(); 
                CurrentMode mode = SceneCreationAndChangeFactory.getFactory().getCurrentMode();
                currentScene.setTransition(new SlideUpDownTransition(RamApp.getApplication(), 500, false));
                SceneCreationAndChangeFactory.getFactory().navigateToModel(object, mode);
            }
        } else if (object instanceof COREConcern) {
            closeMenu();
            RamAbstractScene<?> currentScene = RamApp.getActiveScene(); 
            if (!(currentScene instanceof DisplayConcernEditScene)) {
                CurrentMode mode = CurrentMode.EDIT;
                currentScene.setTransition(new SlideUpDownTransition(RamApp.getApplication(), 500, false));
                SceneCreationAndChangeFactory.getFactory().navigateToModel(getCurrentConcern(), mode);
                popSection();
            }
        }
    }
    
    public NavigatonBarNamer<EObject> getReuseNamer() {
        NavigatonBarNamer<EObject> reuseNamer = new NavigationBarMenu.NavigatonBarNamer<EObject>() {
            
            @Override
            public void initMenu(NavigationBarMenu menu, EObject element) {
                List<EObject> reuses = new ArrayList<EObject>();
                for (EObject obj : reuseObjects) {
                    reuses.add(obj);
                }
                menu.addMenuElement("Previous Reuses", reuses, getReuseNamerInner()); 
                
            }
        };
        return reuseNamer; 
    }
    
    public NavigatonBarNamer<EObject> getReuseNamerInner() {
        NavigatonBarNamer<EObject> reuseNamerInner = new NavigationBarMenu.NavigatonBarNamer<EObject>() {

            @Override
            public void initMenu(NavigationBarMenu menu, EObject element) {
                Iscene sceneForModel = SceneCreationAndChangeFactory.getFactory().getSceneFromModel(element);
                while (!RamApp.getActiveScene().equals(sceneForModel)) {
                    Iscene previousScene = RamApp.getActiveScene().getPreviousScene();
                    SceneCreationAndChangeFactory.getFactory().changeSceneAndUpdate(previousScene);
                    if (previousScene.equals(sceneForModel)) {
                        break;
                    }
                    reuseObjects.pop();
                    previousScenesForReuse.pop();
                    sectionsForReuse.pop();
                }
                List<NavigationBarSection> sectionsToAdd = sectionsForReuse.pop();
                reuseObjects.pop();
                previousScenesForReuse.pop();
                while (sections.size() > 1) {
                    NavigationBarSection removeChild = sections.pop();
                    topContainer.removeChild(removeChild);
                }
                for (NavigationBarSection sec : sectionsToAdd) {
                    sections.add(sec);
                    topContainer.addChild(sec);
                }
                if (reuseObjects.size() == 0) {
                    inReuse = false;
                    NavigationBarSection reuseSection = sections.get(0);
                    sections.remove(reuseSection);
                    topContainer.removeChild(reuseSection);
                }
            }
            
        };
        return reuseNamerInner;
        
    }
    
    /**
     * Functionality for handling a navbar click within a feature model. This is a specific case which will be handled 
     * slightly differently than others within the navbar.
     * @param object
     */
    public void handleFeatureModelNamerActions(EObject object) {
        if (!(object instanceof COREFeature) && !(object instanceof COREFeatureImpactNode)) {
            RamAbstractScene<?> currentScene = RamApp.getActiveScene(); 
            CurrentMode mode = SceneCreationAndChangeFactory.getFactory().getCurrentMode();
            currentScene.setTransition(new SlideTransition(RamApp.getApplication(), 500, true));
            SceneCreationAndChangeFactory.getFactory().navigateToModel(object, mode);
        } else if (object instanceof COREFeature) {
            if (!toggleGenericSelection(object, null)) { 
                COREFeatureModel featureModel = (COREFeatureModel) object.eContainer();
                COREConcern concern = featureModel.getCoreConcern();
                popSection(concern);
                popSection();
                RamAbstractScene<?> currentScene = RamApp.getActiveScene(); 
                CurrentMode mode = SceneCreationAndChangeFactory.getFactory().getCurrentMode();
                currentScene.setTransition(new SlideTransition(RamApp.getApplication(), 500, false));
                SceneCreationAndChangeFactory.getFactory().navigateToModel(concern, mode);
                toggleGenericSelection(object, null);
            }
            
        }
    }
    
    public void handleImpactModelNamerActions(EObject object) {
        if (object instanceof COREImpactNode) {
            if (!(object instanceof COREFeatureImpactNode)) {
                RamAbstractScene<?> currentScene = RamApp.getActiveScene(); 
                CurrentMode mode = SceneCreationAndChangeFactory.getFactory().getCurrentMode();
                currentScene.setTransition(new FadeTransition(RamApp.getApplication(), 600));
                SceneCreationAndChangeFactory.getFactory().navigateToModel(object, mode);
            } else {
                RamAbstractScene<?> currentScene = RamApp.getActiveScene();
                MTComponent currentView =  currentScene.getCurrentView();
                
                if (currentView instanceof AbstractView) {
                    MTComponent wantedComponent = ((AbstractView<?>) currentView).getChildByEObject(object);
                    if (wantedComponent != null && wantedComponent instanceof AbstractShape) {
                        if (currentlySelected != null && currentlySelected instanceof FeatureImpactView) {
                            ((FeatureImpactView) currentlySelected).changeColorToDefault();
                        }
                        previousColor = ((AbstractShape) wantedComponent).getFillColor();
                        ((FeatureImpactView) wantedComponent).changeColor(Colors.FEATURE_ASSIGNEMENT_FILL_COLOR);
                        currentlySelected = (AbstractShape) wantedComponent;
                    }
                }
            }
        }
    }
    
    public List<COREScene> sortScenes(EList<COREScene> scenes) {
        List<String> sceneNames = new ArrayList<String>();
        for (COREScene scene : scenes) {
            sceneNames.add(scene.getName());
        }
        Collections.sort(sceneNames);
        List<COREScene> sortedScenes = new ArrayList<COREScene>();
        for (String sceneName : sceneNames) {
            for (COREScene scene : scenes) {
                if (sceneName.equals(scene.getName())) {
                    sortedScenes.add(scene);
                    break;
                }
            }
        }
        return sortedScenes;
    }
    
    public void addConflictResolutionFeatureNamer(COREScene scene, COREFeature feature) {
        sceneForConflictRes = scene;
        pushSectionJumpGeneric(Icons.ICON_NAVIGATION_FEATURE, feature.getName(), getCoreFeaturePerspective(), feature);
    }
    
    private NavigatonBarNamer<COREFeature> getCoreFeaturePerspective() {
        NavigatonBarNamer<COREFeature> namerFeature = new NavigatonBarNamer<COREFeature>() {

            @Override
            public void initMenu(NavigationBarMenu menu, COREFeature element) {
                List<COREFeature> features = sceneForConflictRes.getRealizes();
                List<COREFeature> featuresForRealization = new ArrayList<COREFeature>();
                featuresForRealization.addAll(features);
                featuresForRealization.remove(element);
                
                for (COREFeature feature : featuresForRealization) {
                    for (COREScene scene : feature.getRealizedBy()) {
                        if (scene.equals(sceneForConflictRes)) {
                            EMap<String, EList<COREArtefact>> artefactsMap = scene.getArtefacts();
                            List<COREArtefact> artefacts = new ArrayList<COREArtefact>();
                            for (EList<COREArtefact> curArtefacts : artefactsMap.values()) {
                                artefacts.addAll(curArtefacts);
                            }
                            NavigationBarNamerFeatureSwitch<COREArtefact> namer = getCoreArtefactNamer();
                            namer.setFeature(feature);
                            menu.addMenuElement(feature.getName(), artefacts, namer); 
                        }            
                    }
                }
            }
        };
        
        return namerFeature;
    }
    
    public NavigationBarNamerFeatureSwitch<COREArtefact> getCoreArtefactNamer() {
        NavigationBarNamerFeatureSwitch<COREArtefact> namerScene = new NavigationBarNamerFeatureSwitch<COREArtefact>() {
            
            private COREFeature featureForSwitch;

            @Override
            public void initMenu(NavigationBarMenu menu, COREArtefact element) {
                RamAbstractScene<?> currentScene = RamApp.getActiveScene(); 
                EObject rootModelElement = ((COREExternalArtefact) element).getRootModelElement();
                CurrentMode mode = SceneCreationAndChangeFactory.getFactory().getCurrentMode();
                currentScene.setTransition(new FadeTransition(RamApp.getApplication(), 600));
                SceneCreationAndChangeFactory.getFactory().connectSceneToFeature(element.getScene(), featureForSwitch);
                SceneCreationAndChangeFactory.getFactory().navigateToModel(rootModelElement, mode);
            }

            @Override
            public void setFeature(COREFeature feature) {
                featureForSwitch = feature;
                
            }
            
        };
        return namerScene;
    }

    /**
     * Function to load a new aspect when residing inside an aspect already.
     * Called also by the confirm popup after checking if the current aspect needs saving.
     * 
     * @param currentAspect aspect SCENE to transition from
     * @param element Aspect element to go to
     */
    public void loadNewAspect(DisplayAspectScene currentAspect, Aspect element) {
        
        EObject model = EcoreUtil.getRootContainer(element);

        currentAspect.setTransition(new SlideTransition(RamApp.getApplication(), 700, true));
        SceneCreationAndChangeFactory.getFactory().navigateToModel(model, null);
    }

    @Override
    public void destroy() {
        // TODO: Really?
        RamApp.getApplication().getCanvas().addChild(this);
    }


    /**
     * Method to toggle the selection of a class when needs to be highlighted or de-selected.
     * 
     * @param classifier the classifier that potentially will be highlighted
     */
    public void toggleClassSelection(Classifier classifier) {
        if (selected == null) {
            selected = classifier;
            ClassifierView<?> v = RamApp.getActiveAspectScene().getStructuralDiagramView().getClassViewOf(selected);
            v.setFillColor(Colors.FEATURE_ASSIGNEMENT_FILL_COLOR);
        } else {
            ClassifierView<?> w = RamApp.getActiveAspectScene().getStructuralDiagramView().getClassViewOf(selected);
            w.setFillColor(Colors.DEFAULT_ELEMENT_FILL_COLOR);
            selected = classifier;
            ClassifierView<?> v = RamApp.getActiveAspectScene().getStructuralDiagramView().getClassViewOf(selected);
            v.setFillColor(Colors.FEATURE_ASSIGNEMENT_FILL_COLOR);
        }
    }
    
    public boolean toggleGenericSelection(EObject obj, RamAbstractScene sceneWanted) {
        RamAbstractScene<?> currentScene;
        MTComponent currentView;
        if (sceneWanted == null) {
            currentScene = RamApp.getActiveScene();
            currentView = currentScene.getCurrentView();
        } else {
            currentScene = sceneWanted;
            currentView = currentScene.getCurrentView();
        }
        
        if (currentView instanceof AbstractView) {
            MTComponent wantedComponent = ((AbstractView<?>) currentView).getChildByEObject(obj);
            if (wantedComponent != null && wantedComponent instanceof AbstractShape) {
                if (currentlySelected != null) {
                    currentlySelected.setFillColor(previousColor);
                }
                previousColor = ((AbstractShape) wantedComponent).getFillColor();
                ((AbstractShape) wantedComponent).setFillColor(Colors.FEATURE_ASSIGNEMENT_FILL_COLOR);
                currentlySelected = (AbstractShape) wantedComponent;
                
                return true;
            }
        }
        return false;
    }


    /**
     * Connecting method to create the NavigationBarSection for the building mode.
     * 
     * @param icon the icon for the section
     * @param label the name of the section
     * @param namer the content
     * @param eObject the type of content
     * @param <T> actual type
     */
    public <T> void pushSectionBuildingRM(PImage icon, String label, NavigatonBarNamer<T> namer, T eObject) {
        NavigationBarSection section = new NavigationBarSection(icon, label, namer, eObject);

        sections.push(section);
        topContainer.addChild(section);
        closeMenu();
    }

    /**
     * Triggered when a Concern Reuse is being queried.
     *
     * @param rootModel Root model to be collapsed
     */
    public void collapseStartingEnvironment(EObject rootModel) {
        RamImageComponent expPreviousConcern = new RamImageComponent(Icons.ICON_REUSE_COLLAPSE, MTColor.WHITE);
        expPreviousConcern.setMinimumSize(80, 80);
        expPreviousConcern.setMaximumSize(80, 80);
        expPreviousConcern.setBufferSize(Cardinal.WEST, 10);
        expPreviousConcern.setBufferSize(Cardinal.EAST, 10);

        returnToConcernButton = new RamButton(expPreviousConcern);

        COREFeature feature = COREArtefactUtil.getReferencingExternalArtefact(rootModel)
                .getScene().getRealizes().get(0);

        final COREConcern concern = (COREConcern) feature.eContainer().eContainer();
        CORENavigationContext.setCurrentConcern(concern);
        topContainer.removeAllChildren();
        topContainer.addChild(returnToConcernButton);

        returnToConcernButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                DisplayAspectScene reusedAspect = (DisplayAspectScene) RamApp.getActiveScene();
                if (reusedAspect.getCurrentView() instanceof MessageViewView) {
                    reusedAspect.switchToPreviousView();
                }

                topContainer.removeAllChildren();
                handleSections();

                pushSection(concern.getName(), getGenericNamerBase(), concern);
                DisplayAspectSceneHandler d =
                        (DisplayAspectSceneHandler) RamApp.getActiveScene().getHandler();
                d.switchBackTo((DisplayAspectScene) RamApp.getActiveScene());
            }
        });
    }

    /**
     * Method used by the closeAspectScene(lowerLevelAspectScene) when it comes to going back from a split view.
     * It restores the navigation bar with the information of the UPPER aspect
     */
    public void returnNormalViewFromSplit() {
        DisplayAspectScene reusedAspect;
        
        if (RamApp.getActiveScene().getCurrentView() instanceof GenericSplitView) {
            GenericSplitView<?, ?, ?> genericSplitView = (GenericSplitView<?, ?, ?>) RamApp.getActiveScene()
                    .getCurrentView();
            
            if (genericSplitView.getDisplaySceneInnerLevel() instanceof DisplayAspectScene) {
                reusedAspect = (DisplayAspectScene) genericSplitView.getDisplaySceneInnerLevel();
            } else {
                reusedAspect = null;
            }
        } else {
            reusedAspect = (DisplayAspectScene) RamApp.getActiveScene();
        }
        
        if (reusedAspect.getCurrentView() instanceof MessageViewView) {
            reusedAspect.switchToPreviousView();
        }

        topContainer.removeAllChildren();
        handleSections();
        COREConcern currentConcern = CORENavigationContext.getCurrentConcern();

        pushSection(currentConcern.getName(), getGenericNamerBase(), currentConcern);
        if (reusedAspect != null) {
            reusedAspect.repushSections();
        }
    }

    /**
     * Method used to clear all the sections of the bar when it comes to handling RM with no Feature.
     * This will be also called when user wants to check out a reused design model.
     */
    public void handleSections() {
        sections.clear();
    }

    /**
     * Retrieves the size of the navigation bar.
     * Used to understand whether we're in a weaving mode inside a full concern OR we're actually
     * in a build Application mode where the navigation bar is empty and must be populated.
     * 
     * @return the size of navbar
     */
    public int getSectionSize() {
        return sections.size();
    }


    /**
     * Modality used to temporarily hide the bar when it comes to an occurrence of DisplayConcernSelectScene comes up.
     * The bar will be hidden so that the user can focus on the Feature Model of the Concern that wants to be reused.
     */
    public void concernSelectMode() {
        if (topContainer.isVisible()) {
            topContainer.setVisible(false);
        } else {
            topContainer.setVisible(true);
        }
    }

    /**
     * Method called by ConcernEditScene handler to check if the Feature Model structure has been modified.
     * If it needs to be saved a popup confirm will be displayed and then this method will be called.
     */
    public void wipeNavigationBar() {
        selected = null;
        sections.clear();
        topContainer.removeAllChildren();
    }

    /**
     * Method that returns the COREScene that the currently displayed model is part of.
     * @return the current COREScene
     */
    public COREScene getCurrentScene() {
        return CORENavigationContext.getCurrentScene();
    }
    
    /**
     * Method that returns the root EObject of the currently displayed model.
     * @return the root of the current model
     */
    public EObject getCurrentModel() {
        return CORENavigationContext.getCurrentModel();
    }
    
    /**
     * Method that returns the COREArtefact of the currently displayed model.
     * @return the COREARtefact of the current model
     */
    public COREArtefact getCurrentArtefact() {
        return CORENavigationContext.getCurrentArtefact();
    }
    
    /**
     * Returns the Concern on which the user is working on.
     * 
     * @return the COREConcern the user is working inside of
     */
    public COREConcern getCurrentConcern() {
        return CORENavigationContext.getCurrentConcern();
    }

    /**
     * Sets the current model.
     * 
     * @param model the current model
     */
    public void setCurrentModel(EObject model) {
        CORENavigationContext.setCurrentModel(model);
    }

    /**
     * Sets the current COREScene.
     * 
     * @param scene the current scene
     */
    public void setCurrentScene(COREScene scene) {
        CORENavigationContext.setCurrentScene(scene);
    }

    /**
     * Sets the current concern.
     * 
     * @param concern the current concern
     */
    public void setCurrentConcern(COREConcern concern) {
        CORENavigationContext.setCurrentConcern(concern);
    }

    /**
     * Returns the perspective that the user is working in.
     * 
     * @return the COREPerspective the user is working inside of
     */
    public COREPerspective getCurrentPerspective() {
        return CORENavigationContext.getCurrentPerspective();
    }

    /**
     * Sets the current perspective.
     * 
     * @param perspective the current perspective
     */
    public void setCurrentPerspective(COREPerspective perspective) {
        CORENavigationContext.setCurrentPerspective(perspective);
    }
    
    /**
     * Returns the language role that the user is working in.
     * 
     * @return the language role the user is working inside of
     */
    public String getCurrentLanguageRole() {
        return currentLanguageRole;
    }

    /**
     * Sets the current language role.
     * 
     * @param currentLanguageRole the current language role
     */
    public void setCurrentLanguageRole(String currentLanguageRole) {
        this.currentLanguageRole = currentLanguageRole;
    }
    
    public void resetCurrentlySelected() {
        if (currentlySelected != null) {
            currentlySelected.setFillColor(previousColor);
            if (currentlySelected instanceof FeatureImpactView) {
                ((FeatureImpactView) currentlySelected).changeColorToDefault();
            }
            
            currentlySelected = null;
            previousColor = null;
        }
    }

    /**
     * Enables the specialized navigation bar after a model has been woven.
     * 
     * @param weavingAspect the aspect for which to enable the weaving result mode for
     */
    public void weavingInterfaceMode(Aspect weavingAspect) {
        containsWeaveCrumb = true;

        RamImageComponent expPreviousConcern = new RamImageComponent(Icons.ICON_WEAVE_COLLAPSE, MTColor.WHITE);
        expPreviousConcern.setMinimumSize(80, 80);
        expPreviousConcern.setMaximumSize(80, 80);
        expPreviousConcern.setBufferSize(Cardinal.WEST, 10);
        expPreviousConcern.setBufferSize(Cardinal.EAST, 10);

        returnToConcernButton = new RamButton(expPreviousConcern);
        
        COREArtefact externalArtefact = COREArtefactUtil.getReferencingExternalArtefact(weavingAspect);
        

        final COREConcern concern = (COREConcern) externalArtefact.getCoreConcern() == null
                ? externalArtefact.getTemporaryConcern() : (COREConcern) externalArtefact.getCoreConcern();

        CORENavigationContext.setCurrentConcern(concern);
        topContainer.removeAllChildren();
        topContainer.addChild(returnToConcernButton);

        returnToConcernButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                DisplayAspectScene reusedAspectScene = (DisplayAspectScene) RamApp.getActiveAspectScene();
                DisplayAspectSceneHandler handler = (DisplayAspectSceneHandler) reusedAspectScene.getHandler();
                
                if (reusedAspectScene.getCurrentView() instanceof MessageViewView) {
                    reusedAspectScene.switchToPreviousView();
                }

                DisplayAspectScene previousScene = (DisplayAspectScene) reusedAspectScene.getPreviousScene();
                if (previousScene.getAspect().getName().contains(WOVEN_DELIMITER)) {
                    topContainer.removeAllChildren();
                    handleSections();
                    COREConcern currentConcern = CORENavigationContext.getCurrentConcern();
                    pushSection(currentConcern.getName(), getGenericNamerBase(), currentConcern);
                    handler.switchBackTo(reusedAspectScene);
                    weavingInterfaceMode(reusedAspectScene.getAspect());
                } else {
                    topContainer.removeAllChildren();
                    handleSections();
                    containsWeaveCrumb = false;
                    pushSection(concern.getName(), getGenericNamerBase(), concern);

                    handler.switchBackTo(reusedAspectScene);
                }
            }
        });
    }
    /**
     * Enables the specialized navigation bar after a model has been woven for any kind of model.
     */
    public void weavingInterfaceMode() {
        containsWeaveCrumb = true;

        RamImageComponent expPreviousConcern = new RamImageComponent(Icons.ICON_WEAVE_COLLAPSE, MTColor.WHITE);
        expPreviousConcern.setMinimumSize(80, 80);
        expPreviousConcern.setMaximumSize(80, 80);
        expPreviousConcern.setBufferSize(Cardinal.WEST, 10);
        expPreviousConcern.setBufferSize(Cardinal.EAST, 10);

        returnToConcernButton = new RamButton(expPreviousConcern);
                
//        COREFeature feature = externalArtefact.getScene().getRealizes().get(0);
//
//        final COREConcern concern = (COREConcern) feature.eContainer().eContainer();
        // COREFeature feature = externalArtefact.getScene().getRealizes().get(0);

//        final COREConcern concern = (COREConcern) wovenArtefact.getCoreConcern() == null
//                ? wovenArtefact.getTemporaryConcern() : (COREConcern) wovenArtefact.getCoreConcern();

//        currentConcern = concern;
        topContainer.removeAllChildren();
        topContainer.addChild(returnToConcernButton);
    }

    /**
     * Returns whether the navigation bar contains a weave crumb.
     * 
     * @return true, if it contains a weave crumb, false otherwise
     */
    public boolean containsWeaveCrumb() {
        return containsWeaveCrumb;
    }

}
