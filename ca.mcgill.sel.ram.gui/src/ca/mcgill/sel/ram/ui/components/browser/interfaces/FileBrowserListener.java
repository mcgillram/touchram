package ca.mcgill.sel.ram.ui.components.browser.interfaces;

import java.io.File;

import org.eclipse.emf.ecore.EObject;

/**
 * Interface to handle aspect file browser events.
 * 
 * @author tdimeco
 */
public interface FileBrowserListener {
    
    /**
     * Called when an aspect is loaded on an aspect file browser.
     * 
     * @param model The loaded aspect
     */
    void modelLoaded(EObject model);
    
    /**
     * Called when an aspect is saved on an aspect file browser.
     * 
     * @param file The saved aspect file
     */
    void modelSaved(File file);
}
