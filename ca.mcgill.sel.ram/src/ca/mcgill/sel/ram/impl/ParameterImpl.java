/**
 */
package ca.mcgill.sel.ram.impl;

import ca.mcgill.sel.ram.Annotatable;
import ca.mcgill.sel.ram.Annotation;
import ca.mcgill.sel.ram.MappableElement;
import ca.mcgill.sel.ram.Parameter;
import ca.mcgill.sel.ram.RAMPartialityType;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.Type;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Parameter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.ram.impl.ParameterImpl#getPartiality <em>Partiality</em>}</li>
 *   <li>{@link ca.mcgill.sel.ram.impl.ParameterImpl#getAnnotation <em>Annotation</em>}</li>
 *   <li>{@link ca.mcgill.sel.ram.impl.ParameterImpl#getType <em>Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ParameterImpl extends TypedElementImpl implements Parameter {
	/**
     * The default value of the '{@link #getPartiality() <em>Partiality</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getPartiality()
     * @generated
     * @ordered
     */
	protected static final RAMPartialityType PARTIALITY_EDEFAULT = RAMPartialityType.NONE;

	/**
     * The cached value of the '{@link #getPartiality() <em>Partiality</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getPartiality()
     * @generated
     * @ordered
     */
	protected RAMPartialityType partiality = PARTIALITY_EDEFAULT;

	/**
     * The cached value of the '{@link #getAnnotation() <em>Annotation</em>}' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getAnnotation()
     * @generated
     * @ordered
     */
	protected EList<Annotation> annotation;

	/**
     * The cached value of the '{@link #getType() <em>Type</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getType()
     * @generated
     * @ordered
     */
	protected Type type;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected ParameterImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return RamPackage.Literals.PARAMETER;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public RAMPartialityType getPartiality() {
        return partiality;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setPartiality(RAMPartialityType newPartiality) {
        RAMPartialityType oldPartiality = partiality;
        partiality = newPartiality == null ? PARTIALITY_EDEFAULT : newPartiality;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, RamPackage.PARAMETER__PARTIALITY, oldPartiality, partiality));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<Annotation> getAnnotation() {
        if (annotation == null) {
            annotation = new EObjectContainmentEList<Annotation>(Annotation.class, this, RamPackage.PARAMETER__ANNOTATION);
        }
        return annotation;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Type getType() {
        if (type != null && type.eIsProxy()) {
            InternalEObject oldType = (InternalEObject)type;
            type = (Type)eResolveProxy(oldType);
            if (type != oldType) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, RamPackage.PARAMETER__TYPE, oldType, type));
            }
        }
        return type;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public Type basicGetType() {
        return type;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setType(Type newType) {
        Type oldType = type;
        type = newType;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, RamPackage.PARAMETER__TYPE, oldType, type));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case RamPackage.PARAMETER__ANNOTATION:
                return ((InternalEList<?>)getAnnotation()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case RamPackage.PARAMETER__PARTIALITY:
                return getPartiality();
            case RamPackage.PARAMETER__ANNOTATION:
                return getAnnotation();
            case RamPackage.PARAMETER__TYPE:
                if (resolve) return getType();
                return basicGetType();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case RamPackage.PARAMETER__PARTIALITY:
                setPartiality((RAMPartialityType)newValue);
                return;
            case RamPackage.PARAMETER__ANNOTATION:
                getAnnotation().clear();
                getAnnotation().addAll((Collection<? extends Annotation>)newValue);
                return;
            case RamPackage.PARAMETER__TYPE:
                setType((Type)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID) {
            case RamPackage.PARAMETER__PARTIALITY:
                setPartiality(PARTIALITY_EDEFAULT);
                return;
            case RamPackage.PARAMETER__ANNOTATION:
                getAnnotation().clear();
                return;
            case RamPackage.PARAMETER__TYPE:
                setType((Type)null);
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID) {
            case RamPackage.PARAMETER__PARTIALITY:
                return partiality != PARTIALITY_EDEFAULT;
            case RamPackage.PARAMETER__ANNOTATION:
                return annotation != null && !annotation.isEmpty();
            case RamPackage.PARAMETER__TYPE:
                return type != null;
        }
        return super.eIsSet(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
        if (baseClass == MappableElement.class) {
            switch (derivedFeatureID) {
                case RamPackage.PARAMETER__PARTIALITY: return RamPackage.MAPPABLE_ELEMENT__PARTIALITY;
                default: return -1;
            }
        }
        if (baseClass == Annotatable.class) {
            switch (derivedFeatureID) {
                case RamPackage.PARAMETER__ANNOTATION: return RamPackage.ANNOTATABLE__ANNOTATION;
                default: return -1;
            }
        }
        return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
        if (baseClass == MappableElement.class) {
            switch (baseFeatureID) {
                case RamPackage.MAPPABLE_ELEMENT__PARTIALITY: return RamPackage.PARAMETER__PARTIALITY;
                default: return -1;
            }
        }
        if (baseClass == Annotatable.class) {
            switch (baseFeatureID) {
                case RamPackage.ANNOTATABLE__ANNOTATION: return RamPackage.PARAMETER__ANNOTATION;
                default: return -1;
            }
        }
        return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (partiality: ");
        result.append(partiality);
        result.append(')');
        return result.toString();
    }

} //ParameterImpl
