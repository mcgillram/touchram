/**
 */
package ca.mcgill.sel.ram.impl;

import ca.mcgill.sel.ram.MappableElement;
import ca.mcgill.sel.ram.ObjectType;
import ca.mcgill.sel.ram.RAMPartialityType;
import ca.mcgill.sel.ram.RamPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Object Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.ram.impl.ObjectTypeImpl#getPartiality <em>Partiality</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class ObjectTypeImpl extends TypeImpl implements ObjectType {
	/**
     * The default value of the '{@link #getPartiality() <em>Partiality</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getPartiality()
     * @generated
     * @ordered
     */
	protected static final RAMPartialityType PARTIALITY_EDEFAULT = RAMPartialityType.NONE;

	/**
     * The cached value of the '{@link #getPartiality() <em>Partiality</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getPartiality()
     * @generated
     * @ordered
     */
	protected RAMPartialityType partiality = PARTIALITY_EDEFAULT;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected ObjectTypeImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return RamPackage.Literals.OBJECT_TYPE;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public RAMPartialityType getPartiality() {
        return partiality;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setPartiality(RAMPartialityType newPartiality) {
        RAMPartialityType oldPartiality = partiality;
        partiality = newPartiality == null ? PARTIALITY_EDEFAULT : newPartiality;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, RamPackage.OBJECT_TYPE__PARTIALITY, oldPartiality, partiality));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case RamPackage.OBJECT_TYPE__PARTIALITY:
                return getPartiality();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case RamPackage.OBJECT_TYPE__PARTIALITY:
                setPartiality((RAMPartialityType)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID) {
            case RamPackage.OBJECT_TYPE__PARTIALITY:
                setPartiality(PARTIALITY_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID) {
            case RamPackage.OBJECT_TYPE__PARTIALITY:
                return partiality != PARTIALITY_EDEFAULT;
        }
        return super.eIsSet(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
        if (baseClass == MappableElement.class) {
            switch (derivedFeatureID) {
                case RamPackage.OBJECT_TYPE__PARTIALITY: return RamPackage.MAPPABLE_ELEMENT__PARTIALITY;
                default: return -1;
            }
        }
        return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
        if (baseClass == MappableElement.class) {
            switch (baseFeatureID) {
                case RamPackage.MAPPABLE_ELEMENT__PARTIALITY: return RamPackage.OBJECT_TYPE__PARTIALITY;
                default: return -1;
            }
        }
        return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (partiality: ");
        result.append(partiality);
        result.append(')');
        return result.toString();
    }

} //ObjectTypeImpl
