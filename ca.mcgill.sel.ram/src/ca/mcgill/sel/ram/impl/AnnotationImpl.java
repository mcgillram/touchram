/**
 */
package ca.mcgill.sel.ram.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

import ca.mcgill.sel.ram.Annotation;
import ca.mcgill.sel.ram.Import;
import ca.mcgill.sel.ram.RamPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Annotation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.ram.impl.AnnotationImpl#getParameter <em>Parameter</em>}</li>
 *   <li>{@link ca.mcgill.sel.ram.impl.AnnotationImpl#getMatter <em>Matter</em>}</li>
 *   <li>{@link ca.mcgill.sel.ram.impl.AnnotationImpl#getImport <em>Import</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AnnotationImpl extends EObjectImpl implements Annotation {
	/**
     * The cached value of the '{@link #getParameter() <em>Parameter</em>}' attribute list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getParameter()
     * @generated
     * @ordered
     */
	protected EList<String> parameter;

	/**
     * The default value of the '{@link #getMatter() <em>Matter</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getMatter()
     * @generated
     * @ordered
     */
	protected static final String MATTER_EDEFAULT = null;

	/**
     * The cached value of the '{@link #getMatter() <em>Matter</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getMatter()
     * @generated
     * @ordered
     */
	protected String matter = MATTER_EDEFAULT;

	/**
     * The cached value of the '{@link #getImport() <em>Import</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getImport()
     * @generated
     * @ordered
     */
	protected Import import_;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected AnnotationImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return RamPackage.Literals.ANNOTATION;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<String> getParameter() {
        if (parameter == null) {
            parameter = new EDataTypeUniqueEList<String>(String.class, this, RamPackage.ANNOTATION__PARAMETER);
        }
        return parameter;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String getMatter() {
        return matter;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setMatter(String newMatter) {
        String oldMatter = matter;
        matter = newMatter;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, RamPackage.ANNOTATION__MATTER, oldMatter, matter));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Import getImport() {
        if (import_ != null && import_.eIsProxy()) {
            InternalEObject oldImport = (InternalEObject)import_;
            import_ = (Import)eResolveProxy(oldImport);
            if (import_ != oldImport) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, RamPackage.ANNOTATION__IMPORT, oldImport, import_));
            }
        }
        return import_;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public Import basicGetImport() {
        return import_;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setImport(Import newImport) {
        Import oldImport = import_;
        import_ = newImport;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, RamPackage.ANNOTATION__IMPORT, oldImport, import_));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case RamPackage.ANNOTATION__PARAMETER:
                return getParameter();
            case RamPackage.ANNOTATION__MATTER:
                return getMatter();
            case RamPackage.ANNOTATION__IMPORT:
                if (resolve) return getImport();
                return basicGetImport();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case RamPackage.ANNOTATION__PARAMETER:
                getParameter().clear();
                getParameter().addAll((Collection<? extends String>)newValue);
                return;
            case RamPackage.ANNOTATION__MATTER:
                setMatter((String)newValue);
                return;
            case RamPackage.ANNOTATION__IMPORT:
                setImport((Import)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID) {
            case RamPackage.ANNOTATION__PARAMETER:
                getParameter().clear();
                return;
            case RamPackage.ANNOTATION__MATTER:
                setMatter(MATTER_EDEFAULT);
                return;
            case RamPackage.ANNOTATION__IMPORT:
                setImport((Import)null);
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID) {
            case RamPackage.ANNOTATION__PARAMETER:
                return parameter != null && !parameter.isEmpty();
            case RamPackage.ANNOTATION__MATTER:
                return MATTER_EDEFAULT == null ? matter != null : !MATTER_EDEFAULT.equals(matter);
            case RamPackage.ANNOTATION__IMPORT:
                return import_ != null;
        }
        return super.eIsSet(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (parameter: ");
        result.append(parameter);
        result.append(", matter: ");
        result.append(matter);
        result.append(')');
        return result.toString();
    }

} //AnnotationImpl
