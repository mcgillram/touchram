/**
 */
package ca.mcgill.sel.ram.impl;

import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.impl.COREMappingImpl;

import ca.mcgill.sel.ram.Operation;
import ca.mcgill.sel.ram.OperationMapping;
import ca.mcgill.sel.ram.ParameterMapping;
import ca.mcgill.sel.ram.RamPackage;

import java.util.Collection;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operation Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class OperationMappingImpl extends COREMappingImpl<Operation> implements OperationMapping {
	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected OperationMappingImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return RamPackage.Literals.OPERATION_MAPPING;
    }

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
    @Override
    public EList<ParameterMapping> getParameterMappings() {
        Collection<ParameterMapping> collection = EMFModelUtil.collectElementsOfType(this, 
                CorePackage.Literals.CORE_MAPPING__MAPPINGS, RamPackage.Literals.PARAMETER_MAPPING);
         
        return new BasicEList<ParameterMapping>(collection);
    }

} //OperationMappingImpl
