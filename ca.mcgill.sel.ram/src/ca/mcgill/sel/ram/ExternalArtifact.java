/**
 */
package ca.mcgill.sel.ram;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>External Artifact</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.ram.ExternalArtifact#getGroupId <em>Group Id</em>}</li>
 *   <li>{@link ca.mcgill.sel.ram.ExternalArtifact#getAtrifactId <em>Atrifact Id</em>}</li>
 *   <li>{@link ca.mcgill.sel.ram.ExternalArtifact#getVersion <em>Version</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.ram.RamPackage#getExternalArtifact()
 * @model
 * @generated
 */
public interface ExternalArtifact extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Group Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group Id</em>' attribute.
	 * @see #setGroupId(String)
	 * @see ca.mcgill.sel.ram.RamPackage#getExternalArtifact_GroupId()
	 * @model
	 * @generated
	 */
	String getGroupId();

	/**
	 * Sets the value of the '{@link ca.mcgill.sel.ram.ExternalArtifact#getGroupId <em>Group Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Group Id</em>' attribute.
	 * @see #getGroupId()
	 * @generated
	 */
	void setGroupId(String value);

	/**
	 * Returns the value of the '<em><b>Atrifact Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Atrifact Id</em>' attribute.
	 * @see #setAtrifactId(String)
	 * @see ca.mcgill.sel.ram.RamPackage#getExternalArtifact_AtrifactId()
	 * @model
	 * @generated
	 */
	String getAtrifactId();

	/**
	 * Sets the value of the '{@link ca.mcgill.sel.ram.ExternalArtifact#getAtrifactId <em>Atrifact Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Atrifact Id</em>' attribute.
	 * @see #getAtrifactId()
	 * @generated
	 */
	void setAtrifactId(String value);

	/**
	 * Returns the value of the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Version</em>' attribute.
	 * @see #setVersion(String)
	 * @see ca.mcgill.sel.ram.RamPackage#getExternalArtifact_Version()
	 * @model
	 * @generated
	 */
	String getVersion();

	/**
	 * Sets the value of the '{@link ca.mcgill.sel.ram.ExternalArtifact#getVersion <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Version</em>' attribute.
	 * @see #getVersion()
	 * @generated
	 */
	void setVersion(String value);

} // ExternalArtifact
