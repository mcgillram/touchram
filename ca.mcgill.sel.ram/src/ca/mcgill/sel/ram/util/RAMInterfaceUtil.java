package ca.mcgill.sel.ram.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.core.util.COREInterfaceUtil;
import ca.mcgill.sel.core.util.COREModelUtil;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.Lifeline;
import ca.mcgill.sel.ram.Message;
import ca.mcgill.sel.ram.ObjectType;
import ca.mcgill.sel.ram.PrimitiveType;
import ca.mcgill.sel.ram.RArray;
import ca.mcgill.sel.ram.REnum;
import ca.mcgill.sel.ram.REnumLiteral;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.StructuralFeature;
import ca.mcgill.sel.ram.StructuralView;
import ca.mcgill.sel.ram.TemporaryProperty;
import ca.mcgill.sel.ram.TypedElement;

/**
 * Utility class with helper methods to retrieve available properties of a given model.
 * This reflects the available properties as per usage and customization defined by CORE.
 * 
 * @author mschoettle
 */
public final class RAMInterfaceUtil {

    /**
     * Creates a new instance.
     */
    private RAMInterfaceUtil() {

    }

    /**
     * Returns all types that are available to be used as a type for attributes.
     * This includes data types, enums and primitive types.
     * 
     * @param currentObject the object contained in a model
     * @return the collection of types
     */
    public static Collection<ObjectType> getAvailableAttributeTypes(EObject currentObject) {
        Collection<ObjectType> result = new HashSet<>();

        result.addAll(getAvailableDataTypes(currentObject));
        result.addAll(getAvailablePrimitiveTypes(currentObject));
        result.addAll(getAvailableExternalEnums(currentObject));

        return result;
    }
    
    /**
     * Returns all types that are available to be used as a return type for operations.
     * This includes primitive types, enums, classes and data types.
     * Void and Any are added manually.
     * 
     * @param currentObject an object contained in a model
     * @return the collection of types
     */
    public static Collection<EObject> getOperationReturnTypes(EObject currentObject) {
        
        Collection<EObject> result = new ArrayList<>();
        
        result.addAll(getAvailablePrimitiveTypes(currentObject));
        result.addAll(getAvailableExternalEnums(currentObject));
        result.addAll(getAvailableClasses(currentObject)); 
        
        StructuralView structuralView = 
                EMFModelUtil.getRootContainerOfType(currentObject, RamPackage.Literals.STRUCTURAL_VIEW);
        
        result.add(RAMModelUtil.getAnyType(structuralView));
        result.add(RAMModelUtil.getVoidType(structuralView));
        
        return result;
    }
    
    /**
     * Returns all types that are available to be used as a type for parameters.
     * This includes primitive types, enums, classes and data types.
     * 
     * @param currentObject an object contained in a model
     * @return the collection of types
     */
    public static Collection<EObject> getAvailableParameterTypes(EObject currentObject) {
        
        Collection<EObject> result = new ArrayList<>();
        
        result.addAll(getAvailablePrimitiveTypes(currentObject));
        result.addAll(getAvailableExternalEnums(currentObject));
        result.addAll(getAvailableClasses(currentObject)); 
                
        return result;
    }

    /**
     * Returns all primitive types of the model the object is contained in.
     * This includes enums and arrays of primitive types.
     * 
     * @param currentObject the object contained in a model
     * @return the collection of primitive types of the model
     */
    public static Collection<PrimitiveType> getAvailablePrimitiveTypes(EObject currentObject) {
        Collection<PrimitiveType> result = new ArrayList<>();
        Aspect aspect = getAspect(currentObject);

        // Collect all primitive types, except arrays of non-primitive types.
        Collection<PrimitiveType> primitiveTypes =
                EcoreUtil.getObjectsByType(aspect.getStructuralView().getTypes(), RamPackage.Literals.PRIMITIVE_TYPE);

        for (PrimitiveType type : primitiveTypes) {
            // Ignore arrays with a non-primitive type.
            // Add enums through respective method below to include those from extended models.
            if (type instanceof RArray
                    && !(((RArray) type).getType() instanceof PrimitiveType)
                    || type instanceof REnum) {
                continue;
            }

            result.add(type);
        }

        result.addAll(getAvailableEnums(aspect));

        return result;
    }

    /**
     * Returns all enums of the model the object is contained in as well as extended models.
     * 
     * @param currentObject the object contained in a model
     * @return the collection of enums
     */
    public static Collection<REnum> getAvailableEnums(EObject currentObject) {
        Collection<REnum> result = getAvailableExternalEnums(currentObject);

        // add the enums of the current model
        COREExternalArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(currentObject);
        
        Aspect aspect = (Aspect) artefact.getRootModelElement();
        Collection<REnum> enums = EcoreUtil.getObjectsByType(aspect.getStructuralView().getTypes(),
                RamPackage.Literals.RENUM);
        result.addAll(enums);

        return result;
    }

    /**
     * Returns all enums of extended and reused models.
     * 
     * @param currentObject the object contained in a model
     * @return the collection of enums
     */
    public static Collection<REnum> getAvailableExternalEnums(EObject currentObject) {
        // Get the COREExternalArtefact that potentially contains extensions and reuses, and then get
        // all the COREExternalArtefacts of the extended models
        COREExternalArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(currentObject);
        Collection<COREExternalArtefact> extendedArtefacts = COREModelUtil.getExtendedArtefacts(artefact);
        
        Collection<REnum> result = new ArrayList<>();
        
        // From every extended artefact, gather the Enums 
        for (COREExternalArtefact externalArtefact : extendedArtefacts) {
            Aspect aspect = (Aspect) externalArtefact.getRootModelElement();

            Collection<REnum> enums =
                    EcoreUtil.getObjectsByType(aspect.getStructuralView().getTypes(), RamPackage.Literals.RENUM);
            result.addAll(enums);
        }

        // Get all the COREExternalArtefacts that are reused
        Collection<COREExternalArtefact> reusedArtefacts = COREModelUtil.getReusedArtefacts(artefact);

        // From every reused artefact, gather the Enums if they are public
        for (COREExternalArtefact externalArtefact : reusedArtefacts) {
            Aspect aspect = (Aspect) externalArtefact.getRootModelElement();
            Collection<REnum> enums =
                    EcoreUtil.getObjectsByType(aspect.getStructuralView().getTypes(), RamPackage.Literals.RENUM);
            for (REnum e : enums) {
                if (COREInterfaceUtil.isPublic(externalArtefact, e)) {
                    result.add(e);
                }
            }
        }
        
        Set<COREModelComposition> allCompositions = COREModelUtil.collectAllModelCompositions(artefact);        
        COREModelUtil.filterMappedElements(result, allCompositions);
        
        return result;
    }

    /**
     * Returns all classes of the model the object is contained in as well as extended models.
     * 
     * @param currentObject the object contained in a model
     * @return the collection of classes
     */
    public static Collection<Classifier> getAvailableClasses(EObject currentObject) {
        Collection<Classifier> result = getAvailableExternalClasses(currentObject);

        // add the classes of the current model
        COREExternalArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(currentObject);
        
        Aspect aspect = (Aspect) artefact.getRootModelElement();
        result.addAll(aspect.getStructuralView().getClasses());

        return result;
    }

    /**
     * Returns all super types for the given classifier.
     * 
     * @param classifier the classifier
     * @return the super types for the given classifier, an empty collection if the classifier has none
     */
    public static Collection<Classifier> getAvailableSuperTypes(Classifier classifier) {
        Collection<Classifier> result = new HashSet<>();

        Aspect aspect = getAspect(classifier);
        Set<Classifier> classifiers = RAMModelUtil.collectClassifiersFor(aspect, classifier, false);

        for (Classifier currentClassifier : classifiers) {
            result.addAll(currentClassifier.getSuperTypes());
        }

        Set<COREModelComposition> allCompositions = COREModelUtil.collectAllModelCompositions(aspect);
        COREModelUtil.filterMappedElements(result, allCompositions);

        return result;
    }

    /**
     * Returns all classes of extended and reused models.
     * 
     * @param currentObject the object contained in a model
     * @return the collection of classes
     */
    public static Collection<Classifier> getAvailableExternalClasses(EObject currentObject) {
        return getAvailableExternalClassifiers(currentObject, false);
    }

    /**
     * Returns all datatypes of the model the object is contained in as well as extended models that
     * are identified as data types.
     * 
     * @param currentObject the object contained in a model
     * @return the collection of classes
     */
    public static Collection<Classifier> getAvailableDataTypes(EObject currentObject) {
        Collection<Classifier> result = new HashSet<>();

        Set<Aspect> extendedAspects = getExtendedAspects(currentObject);

        for (Aspect aspect : extendedAspects) {
            for (Classifier classifier : aspect.getStructuralView().getClasses()) {
                // TODO: Make sure this is not needed anymore because we are now using references
                // Classifier resolvedClassifier = RAMModelUtil.resolveClassifier(currentAspect, classifier);
                if (classifier.isDataType()) {
                    result.add(classifier);
                }
            }
        }

        return result;
    }

    /**
     * Returns all data types of extended and reused models.
     * 
     * @param currentObject the object contained in a model
     * @return the collection of classes
     */
    public static Collection<Classifier> getAvailableExternalDatatypes(EObject currentObject) {
        return getAvailableExternalClassifiers(currentObject, true);
    }

    /**
     * Returns all available classes or datatypes of extended and reused models.
     * 
     * @param currentObject an object contained in a RAM model
     * @param dataType <code>true</code> for data types, <code>false</code> to get all classes
     * @return the collection of classes
     */
    private static Collection<Classifier> getAvailableExternalClassifiers(EObject currentObject, boolean dataType) {
        // Get the COREExternalArtefact that potentially contains extensions and reuses, and then get
        // all the COREExternalArtefacts of the extended models
        COREExternalArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(currentObject);
        Collection<COREExternalArtefact> extendedArtefacts = COREModelUtil.getExtendedArtefacts(artefact);
        
        // From every extended artefacts, gather the Classifiers
        Collection<Classifier> classifiersResult = new ArrayList<>();
        for (COREExternalArtefact externalArtefact : extendedArtefacts) {
            Aspect aspect = (Aspect) externalArtefact.getRootModelElement();
            classifiersResult.addAll(aspect.getStructuralView().getClasses());
        }
        
        // Get all the COREExternalArtefacts that are reused
        Collection<COREExternalArtefact> reusedArtefacts = COREModelUtil.getReusedArtefacts(artefact);
        
        // From every reused artefact, gather the Classifiers if they are public
        for (COREExternalArtefact externalArtefact : reusedArtefacts) {
            Aspect aspect = (Aspect) externalArtefact.getRootModelElement();
            for (Classifier c : aspect.getStructuralView().getClasses()) {
                if (COREInterfaceUtil.isPublic(externalArtefact, c)) {
                    classifiersResult.add(c);
                }
            }
        }
        
        Set<COREModelComposition> allCompositions = COREModelUtil.collectAllModelCompositions(artefact);        
        COREModelUtil.filterMappedElements(classifiersResult, allCompositions);

        return classifiersResult;
    }

    /**
     * Returns all enum literals of the available enums.
     * If the given object is an enum, only the enum literals of the given enum are returned.
     * 
     * @param currentObject the object contained in a model, an {@link REnum} if only its literals should be retrieved
     * @return the collection of enum literals
     */
    public static Collection<REnumLiteral> getAvailableLiterals(EObject currentObject) {
        Collection<REnumLiteral> literals = new HashSet<>();

        Collection<REnum> enums = getAvailableEnums(currentObject);

        if (currentObject instanceof REnum) {
            REnum specificEnum = (REnum) currentObject;
            literals.addAll(specificEnum.getLiterals());
        } else {
            for (REnum currentEnum : enums) {
                literals.addAll(currentEnum.getLiterals());
            }
        }

        return literals;
    }

    /**
     * Collects all possible structural features.
     * Valid choices must be a {@link StructuralFeature} and be a structural feature of the
     * classifier that is represented by the lifeline (or any classifier that is the same classifier,
     * e.g., through inheritance, mapping etc.) a fragment is located on.
     * Also, local properties of the initial message are valid as well.
     * 
     * @param aspect the {@link Aspect} containing the given objects
     * @param lifeline the lifeline for whose represented classifier the structural features are of interest
     * @param initialMessage the message that is defined, which contains the local properties
     * @return the filtered list of choices
     */
    public static Collection<StructuralFeature> getStructuralFeatures(Aspect aspect, Lifeline lifeline,
            Message initialMessage) {
        Collection<StructuralFeature> result = new UniqueEList<>();

        if (lifeline == null || initialMessage == null) {
            return result;
        }

        /**
         * Build a list of structural features of all represented classes.
         */
        TypedElement representedType = lifeline.getRepresents();

        if (representedType != null && representedType.getType() != null
                && representedType.getType() instanceof Classifier) {
            Classifier type = (Classifier) representedType.getType();

            Set<Classifier> classifiers = RAMModelUtil.collectClassifiersFor(aspect, type);

            for (Classifier classifier : classifiers) {
                TreeIterator<EObject> contents = EcoreUtil.getAllProperContents(classifier, true);

                while (contents.hasNext()) {
                    Object next = contents.next();

                    if (next instanceof StructuralFeature) {
                        result.add((StructuralFeature) next);
                    }
                }

            }
        }

        for (TemporaryProperty property : initialMessage.getLocalProperties()) {
            result.add((StructuralFeature) property);
        }

        Set<COREModelComposition> modelExtensions = COREModelUtil.collectAllModelCompositions(aspect);
        COREModelUtil.filterMappedElements(result, modelExtensions);

        return result;
    }

    /**
     * Returns the aspect the given object is contained in.
     * If the object is the aspect itself, the object is returned.
     * 
     * @param currentObject the object contained in a model
     * @return the aspect the given object is contained in, null if none found
     */
    private static Aspect getAspect(EObject currentObject) {
        Aspect aspect = EMFModelUtil.getRootContainerOfType(currentObject, RamPackage.Literals.ASPECT);

        if (aspect == null && currentObject instanceof Aspect) {
            aspect = (Aspect) currentObject;
        }

        return aspect;
    }

    /**
     * Returns the set of extended aspects within whose hierarchy the given object is contain in.
     * The set contains the aspect the given object is contained in.
     * 
     * @param currentObject the object contained in a model
     * @return the set of extended aspects, including the aspect of the object
     */
    private static Set<Aspect> getExtendedAspects(EObject currentObject) {
        Aspect aspect = getAspect(currentObject);
        COREArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(aspect);

        // Create a set of aspects where elements are allowed from.
        // Besides the current aspect, add extended aspects
        Set<COREArtefact> includedCOREArtefact = COREModelUtil.collectExtendedModels(artefact);
        Set<Aspect> includedAspects = new HashSet<>();
        
        for (COREArtefact includedArtefact : includedCOREArtefact) {
            COREExternalArtefact extArtefact = (COREExternalArtefact) includedArtefact;
            includedAspects.add((Aspect) extArtefact.getRootModelElement());
        }
        
        includedAspects.add(aspect);

        return includedAspects;
    }

}
