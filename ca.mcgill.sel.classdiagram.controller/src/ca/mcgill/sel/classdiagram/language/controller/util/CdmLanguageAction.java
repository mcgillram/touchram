package ca.mcgill.sel.classdiagram.language.controller.util;

import java.util.ArrayList;
import java.util.List;

/**
 * Enum of Class Diagram Language Actions with their string names and if they're available in domain models
 */
public enum CdmLanguageAction {
    ADD_CLASS("ClassDiagram.Class.add", true),
    EDIT_CLASS("ClassDiagram.Class.edit", true),
    DELETE_CLASS("ClassDiagram.Class.delete", true),
    
    ADD_NOTE("ClassDiagram.Note.add", true),
    EDIT_NOTE("ClassDiagram.Note.edit", true),
    DELETE_NOTE("ClassDiagram.Note.delete", true),
    
    ADD_ASSOCIATION("ClassDiagram.Association.add", true),
    EDIT_ASSOCIATION("ClassDiagram.Association.edit", true),
    DELETE_ASSOCIATION("ClassDiagram.Association.delete", true),
    
    ADD_NARY_ASSOCIATION("ClassDiagram.NaryAssociation.add", true),
    DELETE_NARY_ASSOCIATION("ClassDiagram.NaryAssociation.delete", true),
    
    ADD_ATTRIBUTE("ClassDiagram.Classifier.Attribute.add", true),
    EDIT_ATTRIBUTE("ClassDiagram.Classifier.Attribute.edit", true),
    DELETE_ATTRIBUTE("ClassDiagram.Classifier.Attribute.delete", true),
    
    ADD_ENUM("ClassDiagram.CDEnum.add", true),
    EDIT_ENUM("ClassDiagram.CDEnum.edit", true),
    DELETE_ENUM("ClassDiagram.CDEnum.delete", true),
    
    EDIT_STATIC("ClassDiagram.StructuralFeature.static.edit", true),
    
    ADD_IMPLEMENTATION_CLASS("ClassDiagram.ImplementationClass.add", false),
    EDIT_IMPLEMENTATION_CLASS("ClassDiagram.ImplementationClass.edit", false),
    DELETE_IMPLEMENTATION_CLASS("ClassDiagram.ImplementationClass.delete", false),
    
    ADD_OPERATION("ClassDiagram.Classifier.Operation.add", false),
    EDIT_OPERATION("ClassDiagram.Classifier.Operation.edit", false),
    REMOVE_OPERATION("ClassDiagram.Classifier.Operation.delete", false),
    
    ADD_OPERATION_PARAMETER("ClassDiagram.Classifier.Operation.Parameter.add", false),
    EDIT_OPERATION_PARAMETER("ClassDiagram.Classifier.Operation.Parameter.edit", false),
    REMOVE_OPERATION_PARAMETER("ClassDiagram.Classifier.Operation.Parameter.delete", false),
    
    EDIT_VISIBILITY("ClassDiagram.VisibilityType.edit", false);
    
    /**
     * name is long form unique identifier for the language action built using the ClassDiagram metamodel
     */
    private String name;
    /**
     * boolean for availability of the action in Domain Model (a restricted version of class diagram) 
     */
    private boolean allowedInDomainModel;
    
    public String getName() {
        return name;
    }

    public boolean isAllowedInDomainModel() {
        return allowedInDomainModel;
    }

    CdmLanguageAction(String name, boolean allowedInDomainModel) {
        this.name = name;
        this.allowedInDomainModel = allowedInDomainModel;
    }
    
    public static List<String> getDomainModelActions() {
        List<String> actionCodes = new ArrayList<String>();
        for (CdmLanguageAction action : CdmLanguageAction.values()) {
            if (action.allowedInDomainModel) {
                actionCodes.add(action.getName());
            }
        }
        
        return actionCodes;
    }
    
    public static List<String> getClassDiagramActions() {
        List<String> actionStrings = new ArrayList<String>();
        for (CdmLanguageAction action : CdmLanguageAction.values()) {
            actionStrings.add(action.getName());
        }
        
        return actionStrings;
    }
}
