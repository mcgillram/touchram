/**
 */
package ca.mcgill.sel.classdiagram;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Comment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.classdiagram.Comment#getNamedElement <em>Named Element</em>}</li>
 *   <li>{@link ca.mcgill.sel.classdiagram.Comment#getContent <em>Content</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.classdiagram.CdmPackage#getComment()
 * @model
 * @generated
 */
public interface Comment extends EObject {
    /**
     * Returns the value of the '<em><b>Named Element</b></em>' reference list.
     * The list contents are of type {@link ca.mcgill.sel.classdiagram.NamedElement}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Named Element</em>' reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Named Element</em>' reference list.
     * @see ca.mcgill.sel.classdiagram.CdmPackage#getComment_NamedElement()
     * @model
     * @generated
     */
    EList<NamedElement> getNamedElement();

    /**
     * Returns the value of the '<em><b>Content</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Content</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Content</em>' attribute.
     * @see #setContent(String)
     * @see ca.mcgill.sel.classdiagram.CdmPackage#getComment_Content()
     * @model
     * @generated
     */
    String getContent();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.classdiagram.Comment#getContent <em>Content</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Content</em>' attribute.
     * @see #getContent()
     * @generated
     */
    void setContent(String value);

} // Comment
