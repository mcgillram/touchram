/**
 */
package ca.mcgill.sel.classdiagram;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CD Mappable Element</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.mcgill.sel.classdiagram.CdmPackage#getCDMappableElement()
 * @model
 * @generated
 */
public interface CDMappableElement extends NamedElement {
} // CDMappableElement
