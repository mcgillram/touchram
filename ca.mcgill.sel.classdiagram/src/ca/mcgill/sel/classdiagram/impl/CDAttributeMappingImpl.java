/**
 */
package ca.mcgill.sel.classdiagram.impl;

import ca.mcgill.sel.classdiagram.Attribute;
import ca.mcgill.sel.classdiagram.CDAttributeMapping;
import ca.mcgill.sel.classdiagram.CdmPackage;

import ca.mcgill.sel.core.impl.COREMappingImpl;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CD Attribute Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CDAttributeMappingImpl extends COREMappingImpl<Attribute> implements CDAttributeMapping {
    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    protected CDAttributeMappingImpl() {
		super();
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    protected EClass eStaticClass() {
		return CdmPackage.Literals.CD_ATTRIBUTE_MAPPING;
	}

} //CDAttributeMappingImpl
