/**
 */
package ca.mcgill.sel.classdiagram.impl;

import ca.mcgill.sel.classdiagram.CDEnum;
import ca.mcgill.sel.classdiagram.CDEnumMapping;
import ca.mcgill.sel.classdiagram.CdmPackage;

import ca.mcgill.sel.core.impl.COREMappingImpl;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CD Enum Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CDEnumMappingImpl extends COREMappingImpl<CDEnum> implements CDEnumMapping {
    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    protected CDEnumMappingImpl() {
		super();
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    protected EClass eStaticClass() {
		return CdmPackage.Literals.CD_ENUM_MAPPING;
	}

} //CDEnumMappingImpl
