/**
 */
package ca.mcgill.sel.classdiagram.impl;

import java.util.Collection;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

import ca.mcgill.sel.classdiagram.CDOperationMapping;
import ca.mcgill.sel.classdiagram.CDParameterMapping;
import ca.mcgill.sel.classdiagram.CdmPackage;
import ca.mcgill.sel.classdiagram.Operation;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.impl.COREMappingImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CD Operation Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CDOperationMappingImpl extends COREMappingImpl<Operation> implements CDOperationMapping {
    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    protected CDOperationMappingImpl() {
		super();
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    protected EClass eStaticClass() {
		return CdmPackage.Literals.CD_OPERATION_MAPPING;
	}

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated NOT
     */
    @Override
    public EList<CDParameterMapping> getParameterMappings() {
        Collection<CDParameterMapping> collection = EMFModelUtil.collectElementsOfType(this, 
                CorePackage.Literals.CORE_MAPPING__MAPPINGS, CdmPackage.Literals.CD_PARAMETER_MAPPING);
         
        return new BasicEList<CDParameterMapping>(collection);
    }

} //CDOperationMappingImpl
