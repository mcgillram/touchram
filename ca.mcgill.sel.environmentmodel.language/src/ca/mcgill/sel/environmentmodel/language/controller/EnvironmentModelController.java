package ca.mcgill.sel.environmentmodel.language.controller;

import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.environmentmodel.*;


/**
 * The controller for {@link EnvironmentModel}
 * @author hyacinthali
 *
 */
public class EnvironmentModelController extends BaseController{

    /**
     * Creates a new instance of {@link EnvironmentModelController}.
     */
    protected EnvironmentModelController() {
        // Prevent anyone outside this package to instantiate.
    }

    /**
     * Creates new actor in environment model.
     * @param owner - the container {@link EnvironmentModel} of the actor
     * @param name - name of the actor
     * @param lowerBound - the lowest multiplicity of the actor
     * @param upperBound - the maximum multiplicity of the actor
     * @param communicationLowerBound - the maximum number of the corresponding message(s)
     * @param communicationUpperBound - the lowest number of the corresponding message(s)
     */
    public void createActor(EnvironmentModel owner, String name, int lowerBound, int upperBound, 
            int communicationLowerBound, int communicationUpperBound) {

        Actor newActor = EmFactory.eINSTANCE.createActor();
        newActor.setName(name);
        newActor.setActorLowerBound(lowerBound);
        newActor.setActorUpperBound(upperBound);

        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);

        // Create commands.
        Command addActorCommand = AddCommand.create(editingDomain, owner, EmPackage.Literals.ENVIRONMENT_MODEL__ACTORS,
                newActor);

        // Create compound command.
        CompoundCommand compoundCommand = new CompoundCommand();
        compoundCommand.append(addActorCommand);

        doExecute(editingDomain, compoundCommand);

    }

    /**
     * Creates a new actor as well as its layout element.
     *
     * @param owner the {@link CommunicationDiagram} the actor should be added to
     * @param name the name of the actor
     * @param actorName 
     * @param x the x position of the class
     * @param y the y position of the class 
     */
    public void createActor(EnvironmentModel owner, String actorTypeName, String actorName, float x, float y) {
        Actor newActor = EmFactory.eINSTANCE.createActor();
        newActor.setName(actorName);
        //        ActorType newActorType = EmFactory.eINSTANCE.createActorType();
        //        newActorType.setName(actorTypeName);
        //        newActor.setActorType(newActorType);
        //        owner.getActorTypes().add(newActorType);

        if (actorTypeName == null) {
            if (owner.getActorTypes().size() == 0) {
                newActor.setActorType(createActorType(owner, null));
            }
            else {
                newActor.setActorType(owner.getActorTypes().get(0));
            }
        } else {
            newActor.setActorType(createActorType(owner, actorTypeName));
        }


        LayoutElement layoutElement = EmFactory.eINSTANCE.createLayoutElement();
        layoutElement.setX(x);
        layoutElement.setY(y);

        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);

        // Create commands.
        Command addClassCommand = AddCommand.create(editingDomain, owner, EmPackage.Literals.ENVIRONMENT_MODEL__ACTORS,
                newActor);
        Command createElementMapCommand =
                createAddLayoutElementCommand(editingDomain, owner, newActor, layoutElement);

        // Create compound command.
        CompoundCommand compoundCommand = new CompoundCommand();

        // If the layout element is added first, when the structural view gets notified (about a new class)
        // access to the layout element is already possible.
        compoundCommand.append(addClassCommand);
        compoundCommand.append(createElementMapCommand);
        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Creates a new note.
     *
     * @param owner the {@link EnvironmentModel} the note should be added to
     * @param name the name of the note
     * @param x the x position of the note
     * @param y the y position of the note
     */
    public void createNote(EnvironmentModel owner, float x, float y) {
        //Create note
        Note newNote = EmFactory.eINSTANCE.createNote();

        LayoutElement layoutElement = EmFactory.eINSTANCE.createLayoutElement();
        layoutElement.setX(x);
        layoutElement.setY(y);

        //Get editing domain
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);

        //Create Commands
        Command addNoteCommand = AddCommand.create(editingDomain, owner, EmPackage.Literals.ENVIRONMENT_MODEL__NOTES,
                newNote);
        Command createElementMapCommand =
                createAddLayoutElementCommand(editingDomain, owner, newNote, layoutElement);

        // Create compound command.
        CompoundCommand compoundCommand = new CompoundCommand();
        compoundCommand.append(addNoteCommand);
        compoundCommand.append(createElementMapCommand);

        doExecute(editingDomain, compoundCommand);

    }

    public ActorType createActorType(EnvironmentModel owner, String actorTypeName) {

        ActorType newActorType = EmFactory.eINSTANCE.createActorType();
        newActorType.setName(actorTypeName);

        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);

        // Create commands.
        Command addTypeCommand = AddCommand.create(editingDomain, owner, EmPackage.Literals.ENVIRONMENT_MODEL__ACTOR_TYPES,
                newActorType);
        // Create compound command.
        CompoundCommand compoundCommand = new CompoundCommand();

        // If the layout element is added first, when the structural view gets notified (about a new class)
        // access to the layout element is already possible.
        compoundCommand.append(addTypeCommand);
        doExecute(editingDomain, compoundCommand);

        return newActorType;

    }

    public void createTimeTriggeredEvent(EnvironmentModel em, int index, String name) {

        TimeTriggeredEvent event = EmFactory.eINSTANCE.createTimeTriggeredEvent();
        event.setName(name);

        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(em);

        Command addEventCommand = AddCommand.create(editingDomain, em, 
                EmPackage.Literals.TIME_TRIGGERED_EVENT, event);       

        doExecute(editingDomain, addEventCommand);  

    }

    public void createSystemBox(EnvironmentModel owner, LayoutElement layoutElement) {     

        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);

        Command createElementMapCommand =
                createAddLayoutElementCommand(editingDomain, owner, owner, layoutElement);


        doExecute(editingDomain, createElementMapCommand);

    }


    /**
     * Creates either input or output message in an environment model.
     * @param em - the environment model
     * @param actor - the actor that contains the message
     * @param name - name of the message
     * @param messageDirection - either input message or output message.
     */
    public void createMessage(EnvironmentModel em, Actor actor, String name, MessageDirection messageDirection) {

        // create message
        Message message = EmFactory.eINSTANCE.createMessage();
        message.setMessageDirection(messageDirection);

        MessageType type;
        if (name == null) {
            if (em.getMessageTypes().size() == 0) {
                type = createMessageType(em, "UntypedMessage");
            } else {
                type = em.getMessageTypes().get(0);
            }
        } else {        
            // check if the type of the message exist
            type = messageTypeExist(em, name);
            if (type == null) {
                // create new message type
                type = createMessageType(em, name);
            }
        }

        message.setMessageType(type);

        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(actor);

        // Create commands.
        Command addMessageCommand = AddCommand.create(editingDomain, actor, 
                EmPackage.Literals.ACTOR__MESSAGES, message);

        // Create compound command.
        CompoundCommand compoundCommand = new CompoundCommand();
        compoundCommand.append(addMessageCommand);

        doExecute(editingDomain, compoundCommand);

    }

    /**
     * Create new message type.
     * @param em - the environment model
     * @param name - name of the message type
     * @return - the message type.
     */
    public MessageType createMessageType(EnvironmentModel em, String name) {


        // create communication
        MessageType type = EmFactory.eINSTANCE.createMessageType();
        type.setName(name);

        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(em);

        // Create commands.
        Command addMessageTypeCommand = AddCommand.create(editingDomain, em, 
                EmPackage.Literals.ENVIRONMENT_MODEL__MESSAGE_TYPES, type);

        // Create compound command.
        CompoundCommand compoundCommand = new CompoundCommand();
        compoundCommand.append(addMessageTypeCommand);

        // execute commands
        doExecute(editingDomain, compoundCommand);

        return type;

    }

    /**
     * Return an existing message type.
     * @param em - environment model
     * @param name - name of the message type
     * @return the message type
     */
    private boolean actorActorCommunicationExist(EnvironmentModel em, Actor sender, Actor receiver) {

        for (ActorActorCommunication c: em.getCommunications()) {
            if((c.getParticipants().get(0).equals(sender) && c.getParticipants().get(1).equals(receiver))
                    || (c.getParticipants().get(1).equals(sender) && c.getParticipants().get(0).equals(receiver)))
                return true;
        }
        return false;

    }

    /**
     * Return an existing actor-actor communication.
     * @param em - environment model
     * @param sender - sender of the communication
     * @param sender - receiver of the communication
     * @return the message type
     */
    private MessageType messageTypeExist(EnvironmentModel em, String name) {
        MessageType type = null;

        for (MessageType t : em.getMessageTypes()) {
            if (t.getName().equals(name)) {
                type = t;
            }
        }
        return type;
    }

    /**
     * Moves a set of named elements in a diagram.
     * @param uem The use case diagram.
     * @param positionMap The position map to use.
     */
    public void moveNamedElements(
            EnvironmentModel em, Map<NamedElement, LayoutElement> positionMap) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(em);

        if (positionMap.size() > 0) {
            // Create compound command.
            CompoundCommand moveClassifiersCommand = new CompoundCommand();

            for (Entry<NamedElement, LayoutElement> entry : positionMap.entrySet()) {
                // get all required values
                NamedElement namedElement = entry.getKey();
                float x = entry.getValue().getX();
                float y = entry.getValue().getY();

                Command classifierMoveCommand = createMoveCommand(editingDomain, em, namedElement, x, y);
                moveClassifiersCommand.append(classifierMoveCommand);
            }

            doExecute(editingDomain, moveClassifiersCommand);
        }

    }

    /**
     * Moves a set of non-named elements in a diagram.
     * @param uem The use case diagram.
     * @param nonClassifierPositionMap The position map to use.
     */
    public void moveNonNamedElements(
            EnvironmentModel em, Map<EObject, LayoutElement> nonClassifierPositionMap) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(em);

        if (nonClassifierPositionMap.size() > 0) {
            // Create compound command.
            CompoundCommand moveClassifiersCommand = new CompoundCommand();

            for (Entry<EObject, LayoutElement> entry : nonClassifierPositionMap.entrySet()) {
                // get all required values
                EObject object = entry.getKey();
                float x = entry.getValue().getX();
                float y = entry.getValue().getY();

                Command classifierMoveCommand = createMoveCommand(editingDomain, em, object, x, y);
                moveClassifiersCommand.append(classifierMoveCommand);
            }

            doExecute(editingDomain, moveClassifiersCommand);
        }

    }



    /**
     * Removes the given actor and its associated layout element.
     *
     * @param actor the actor that should be removed
     */
    public void removeActor(Actor actor) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(actor);
        EnvironmentModel em = (EnvironmentModel) actor.eContainer();

        CompoundCommand compoundCommand = new CompoundCommand();

        // Create command for removing ElementMap (includes the layout element).
        Command removeElementMapCommand = createRemoveLayoutElementCommand(editingDomain, em, actor);
        compoundCommand.append(removeElementMapCommand);

        // Create command for removing actor.
        Command removeActorCommand = RemoveCommand.create(editingDomain, actor);
        compoundCommand.append(removeActorCommand);        

        // Manage the communications associated to this actor
        for (ActorActorCommunication communication : em.getCommunications()) {
            if(communication.getParticipants().get(0)==actor || communication.getParticipants().get(1)==actor) {
                compoundCommand.append(RemoveCommand.create(editingDomain, communication));
            }
        }


        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Creates either input or output message in an environment model.
     * @param em - the environment model
     * @param sender - the sender of the communication
     * @param receiver - the receiver of the communication
     * @param name - name of the communication
     */
    public void createActorActorCommunication(EnvironmentModel em, Actor sender, Actor receiver, String name) {

        if (!actorActorCommunicationExist(em, sender, receiver)) {
            // create actor-actor communication
            ActorActorCommunication communication=EmFactory.eINSTANCE.createActorActorCommunication();
            communication.setName(name);
            communication.getParticipants().add(0, sender);
            communication.getParticipants().add(1, receiver);

            EditingDomain editingDomain = EMFEditUtil.getEditingDomain(em);

            // Create commands.        
            Command addCommunicationCommand = AddCommand.create(editingDomain, em, 
                    EmPackage.Literals.ENVIRONMENT_MODEL__COMMUNICATIONS, communication);

            doExecute(editingDomain, addCommunicationCommand);

        }           
    }

    public void removeActorActorCommunication(Actor actor, Actor element) {
        EnvironmentModel em = (EnvironmentModel) actor.eContainer();

        for (ActorActorCommunication communication : em.getCommunications()) {          
            if(communication.getParticipants().get(0) == actor || communication.getParticipants().get(1) == actor) {
                removeActorActorCommunication(communication);
                break;
            }
        }        
    }


    private void removeActorActorCommunication(ActorActorCommunication communication) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(communication);

        Command command = RemoveCommand.create(editingDomain, communication);

        doExecute(editingDomain, command);

    }

    public void removeTimeTriggeredEvent(TimeTriggeredEvent event) {

        // Get the editing domain
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(event);

        // Execute remove command for this event
        Command removeCommand = RemoveCommand.create(editingDomain, event);
        doExecute(editingDomain, removeCommand);

    }

    /**
     * Creates a new parameter. In case the affected operation is used in message views, all messages are updated by
     * adding a corresponding parameter value mapping.
     *
     * @param owner the operation the parameter should be added to
     * @param index the index the parameter should be added at
     * @param name the name of the parameter
     * @param type the type of the parameter
     */
    public void createParameter(MessageType owner, int index, String name,  ParameterType type) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);
        CompoundCommand compoundCommand = new CompoundCommand();

        Parameter parameter = EmFactory.eINSTANCE.createParameter();
        parameter.setName(name);
        parameter.setType(type);

        compoundCommand.append(AddCommand.create(editingDomain, owner, EmPackage.Literals.MESSAGE_TYPE__PARAMETERS,
                parameter, index));

        doExecute(editingDomain, compoundCommand);
    }

    public Parameter createParameter(MessageType owner, int index, String name) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);
        CompoundCommand compoundCommand = new CompoundCommand();

        Parameter parameter = EmFactory.eINSTANCE.createParameter();
        parameter.setName(name);

        compoundCommand.append(AddCommand.create(editingDomain, owner, EmPackage.Literals.MESSAGE_TYPE__PARAMETERS,
                parameter, index));

        doExecute(editingDomain, compoundCommand);

        return parameter;
    }

    public ParameterType createParameterType(EnvironmentModel owner, int index, String typeString) {

        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);
        CompoundCommand compoundCommand = new CompoundCommand();

        ParameterType type = EmFactory.eINSTANCE.createParameterType();
        type.setName(typeString);

        compoundCommand.append(AddCommand.create(editingDomain, owner, EmPackage.Literals.ENVIRONMENT_MODEL__PARAMETERTYPE,
                type, index));

        doExecute(editingDomain, compoundCommand);

        return type;
    }

    /**
     * Removes the given parameter. In case the affected operation is used in message views, all messages are updated by
     * removing the corresponding parameter value mapping.
     *
     * @param parameter the parameter to be removed
     */
    public void removeParameter(Parameter parameter) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(parameter.eContainer());
        CompoundCommand compoundCommand = new CompoundCommand();

        compoundCommand.append(RemoveCommand.create(editingDomain, parameter));

        doExecute(editingDomain, compoundCommand);

    }

    public void resetMessageType(Message message, MessageType type) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(message.eContainer());
        CompoundCommand compoundCommand = new CompoundCommand();

        compoundCommand.append(SetCommand.create(editingDomain, message, EmPackage.Literals.MESSAGE__MESSAGE_TYPE, type));

        doExecute(editingDomain, compoundCommand);
    }



    /**
     * Removes the given note and its associated layout element
     *
     * @param note the note that should be removed
     */
    public void removeNote(Note note) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(note);
        EnvironmentModel em = (EnvironmentModel) note.eContainer();

        CompoundCommand compoundCommand = new CompoundCommand();

        // Create command for removing ElementMap (includes the layout element).
        Command removeElementMapCommand = createRemoveLayoutElementCommand(editingDomain, em, note);
        compoundCommand.append(removeElementMapCommand);

        // Create command for removing note.
        Command removeNoteCommand = RemoveCommand.create(editingDomain, note);
        compoundCommand.append(removeNoteCommand);

        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Creates a note link between the note and the given named element.
     *
     * @param owner the {@link EnvironmentModel} the association should be added to
     * @param note the {@link Note} linked from
     * @param annotatedElement the annotated {@link NamedElement}
     */
    public void createAnnotation(EnvironmentModel owner, Note note, NamedElement annotatedElement) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);

        if(!note.getAnnotatedElements().contains(annotatedElement)) {
            Command addNotedElementCommand = AddCommand.create(editingDomain, note,
                    EmPackage.Literals.NOTE__ANNOTATED_ELEMENTS, annotatedElement);
            // execute command
            doExecute(editingDomain, addNotedElementCommand);
        }
        java.lang.System.out.println("!!! command excuted "+ note.getAnnotatedElements().toString());
    }

    /**
     * Remove a note link between the note and the given class.
     *
     * @param note the {@link Note} linked from
     * @param annotatedElement the annotated {@link NamedElement}
     */
    public void removeAnnotation(Note note, NamedElement annotatedElement) {
        EnvironmentModel em= (EnvironmentModel) note.eContainer();
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(em);

        Command removeNotedElementCommand = RemoveCommand.create(editingDomain, note,
                EmPackage.Literals.NOTE__ANNOTATED_ELEMENTS, annotatedElement);

        // execute command
        doExecute(editingDomain, removeNotedElementCommand);
    }

    /**
     * Remove all the annotations of the given note
     *
     * @param note the {@link Note} linked from
     */
    public void removeAnnotations(Note note) {
        EnvironmentModel cd = (EnvironmentModel) note.eContainer();
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(cd);

        CompoundCommand compoundCommand = new CompoundCommand();

        if (note.getAnnotatedElements().size() > 0) {
            for (NamedElement element : note.getAnnotatedElements()) {
                Command removeNotedElementCommand = RemoveCommand.create(editingDomain, note,
                        EmPackage.Literals.NOTE__ANNOTATED_ELEMENTS, element);
                compoundCommand.append(removeNotedElementCommand);
            }

            // execute command
            doExecute(editingDomain, compoundCommand);
        }
    }
}
