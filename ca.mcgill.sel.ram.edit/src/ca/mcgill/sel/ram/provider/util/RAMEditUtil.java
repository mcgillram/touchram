package ca.mcgill.sel.ram.provider.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREMapping;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.COREModelExtension;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.core.util.COREModelUtil;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.ClassifierMapping;
import ca.mcgill.sel.ram.Lifeline;
import ca.mcgill.sel.ram.Message;
import ca.mcgill.sel.ram.NamedElement;
import ca.mcgill.sel.ram.Operation;
import ca.mcgill.sel.ram.OperationMapping;
import ca.mcgill.sel.ram.Parameter;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.StructuralFeature;
import ca.mcgill.sel.ram.TemporaryProperty;
import ca.mcgill.sel.ram.TypedElement;
import ca.mcgill.sel.ram.impl.ClassImpl;
import ca.mcgill.sel.ram.provider.RAMEditPlugin;
import ca.mcgill.sel.ram.util.RAMModelUtil;

/**
 * Helper class with convenient static methods for working with EMF objects.
 * 
 * @author mschoettle
 */
public final class RAMEditUtil {

    /**
     * Creates a new instance of {@link RAMEditUtil}.
     */
    private RAMEditUtil() {
        // suppress default constructor
    }

    /**
     * Returns the complete signature of the given {@link Operation} including parameters.
     * 
     * @param adapterFactory the {@link AdapterFactory} to use
     * @param operation the {@link Operation} a signature should be returned for
     * @param includeClassName whether the class name should be included in the signature (at the beginning)
     * @param includeReturnType whether the return type of the operation should be appended
     * @return the complete signature of the given {@link Operation}
     */
    public static String getOperationSignature(AdapterFactory adapterFactory, Operation operation,
            boolean includeClassName, boolean includeReturnType) {
        StringBuffer stringBuffer = new StringBuffer();

        stringBuffer.append(RAMEditPlugin.INSTANCE.getString("_UI_Operation_type"));
        stringBuffer.append(" ");

        if (includeClassName) {
            stringBuffer.append(EMFEditUtil.getTextFor(adapterFactory, operation.eContainer()));
            stringBuffer.append(".");
        }

        String operationName = EMFEditUtil.getTextFor(adapterFactory, operation);
        stringBuffer.append(operationName);
        stringBuffer.append("(");

        for (Parameter parameter : operation.getParameters()) {
            // if it is not the first one, add a ","
            if (operation.getParameters().indexOf(parameter) > 0) {
                stringBuffer.append(", ");
            }

            stringBuffer.append(EMFEditUtil.getTextFor(adapterFactory, parameter.getType()));
            stringBuffer.append(" ");
            stringBuffer.append(EMFEditUtil.getTextFor(adapterFactory, parameter));
        }

        stringBuffer.append(")");

        if (includeReturnType) {
            stringBuffer.append(" : ");
            stringBuffer.append(EMFEditUtil.getTextFor(adapterFactory, operation.getReturnType()));
        }

        return stringBuffer.toString();
    }

    /**
     * Recursively gather all COREExternalArtefacts associate in
     * the COREModelExtension of a given {@link COREExternalArtefact} (Including himself).
     * 
     * @param currentArtefact - The given artefact
     * @return a collection of all the {@link COREExternalArtefact} gathered
     */
    public static Collection<COREExternalArtefact> rGetArtefactsFromModelExtension(
            COREExternalArtefact currentArtefact) {

        Collection<COREExternalArtefact> externalArtefactsResult = new ArrayList<>();

        // Add the actual CEA
        externalArtefactsResult.add(currentArtefact);

        // Add all the CEA in the COREModelExtension associate with the CEA
        if (currentArtefact.getModelExtensions().size() > 0) {
            EList<COREModelExtension> modelExtensions = currentArtefact.getModelExtensions();
            for (COREModelExtension modelExtension : modelExtensions) {
                COREExternalArtefact artefact = (COREExternalArtefact) modelExtension.getSource();
                externalArtefactsResult.addAll(rGetArtefactsFromModelExtension(artefact));
            }
        }

        return externalArtefactsResult;
    }
    
    /**
     * Recursively gather all ClassImpl that are super classes of a given {@link ClassImpl} (Including himself).
     * 
     * @param classifier - The given classifier
     * @return a collection of all the {@link ClassImpl} gathered
     */
    public static Collection<ClassImpl> rGetSuperClassesFromClassifier(ClassImpl classifier) {
        Collection<ClassImpl> classifiersResult = new ArrayList<>();

        // Add the actual classifier
        classifiersResult.add(classifier);
        
        // Add all the classifiers that are super classes of this classifier
        if (classifier.getSuperTypes().size() > 0) {
            EList<Classifier> superClasses = classifier.getSuperTypes();
            for (Classifier superClass : superClasses) {
                ClassImpl superClassImpl = (ClassImpl) superClass;
                classifiersResult.addAll(rGetSuperClassesFromClassifier(superClassImpl));
            }
        }
        
        return classifiersResult;
    }

    /**
     * Filters the list of all operations to a list of callable operations on the given receiver.
     * The given collection is directly modified and all operations that cannot be called on the receiver are
     * removed from it.
     * 
     * @param operations the list of all operations found
     * @param aspect the current aspect
     * @param receiver the receiver the operation is called on
     */
    public static void filterCallableOperations(Collection<?> operations, Aspect aspect, Classifier receiver) {
        Set<Classifier> callReceivers = Collections.emptySet();

        if (receiver != null) {
            callReceivers = RAMModelUtil.collectClassifiersFor(aspect, receiver);
        }

        Set<COREModelComposition> modelCompositions = new HashSet<>();
        modelCompositions.addAll(COREModelUtil.collectModelExtensions(aspect));
        modelCompositions.addAll(COREModelUtil.collectAllModelReuses(aspect));
        Set<Operation> mappedFromOperations = new HashSet<Operation>();

        for (COREModelComposition modelComposition : modelCompositions) {
            Collection<ClassifierMapping> ccm = EcoreUtil.getObjectsByType(modelComposition.getCompositions(),
                    RamPackage.Literals.CLASSIFIER_MAPPING);
            for (ClassifierMapping mapping : ccm) {
                if (callReceivers.contains(mapping.getFrom())) {
                    for (OperationMapping operationMapping : mapping.getOperationMappings()) {
                        mappedFromOperations.add(operationMapping.getFrom());
                    }
                }
            }
        }

        // Filter out all operations that are not part of the classifiers in the callReceivers list.
        // The call receivers are the given receiver itself, super types, mapped classes and extended classes.
        for (Iterator<?> iterator = operations.iterator(); iterator.hasNext(); ) {
            Operation value = (Operation) iterator.next();

            // null is also contained in the list
            if (value != null) {
                if (!callReceivers.contains(value.eContainer())
                        || mappedFromOperations.contains(value)) {
                    iterator.remove();
                }
            }
        }
    }

    /**
     * Collects all possible structural features.
     * Valid choices must be a {@link StructuralFeature} and be a structural feature of the
     * classifier that is represented by the lifeline (or any classifier that is the same classifier,
     * e.g., through inheritance, mapping etc.) a fragment is located on.
     * Also, local properties of the initial message are valid as well.
     * 
     * @param aspect the {@link Aspect} containing the given objects
     * @param lifeline the lifeline for whose represented classifier the structural features are of interest
     * @param initialMessage the message that is defined, which contains the local properties
     * @param currentValue the current structural feature that is set
     * @return the filtered list of choices
     */
    public static Collection<?> collectStructuralFeatures(Aspect aspect, Lifeline lifeline,
            Message initialMessage, StructuralFeature currentValue) {
        Collection<Object> result = new UniqueEList<Object>();
        result.add(null);

        if (lifeline == null || initialMessage == null) {
            return result;
        }

        /**
         * Build a list of structural features of all represented classes.
         */
        TypedElement representedType = lifeline.getRepresents();

        if (representedType != null && representedType.getType() != null
                && representedType.getType() instanceof Classifier) {
            Classifier type = (Classifier) representedType.getType();

            Set<Classifier> classifiers = RAMModelUtil.collectClassifiersFor(aspect, type);

            for (Classifier classifier : classifiers) {
                TreeIterator<EObject> contents = EcoreUtil.getAllProperContents(classifier, true);

                while (contents.hasNext()) {
                    Object next = contents.next();

                    if (next instanceof StructuralFeature) {
                        result.add(next);
                    }
                }

            }
        }

        for (TemporaryProperty property : initialMessage.getLocalProperties()) {
            result.add(property);
        }

        Set<COREModelExtension> modelExtensions = COREModelUtil.collectModelExtensions(aspect);
        COREModelUtil.filterMappedElements(result, modelExtensions);

        return result;
    }
    
    /**
     * Return the expected text if the mapping have referenced mappings or the next expected one to display.
     * Will return a String of this form &lt;A,B&gt;
     * If there is no referenced mappings, just return an empty string
     * 
     * @param mapping The mapping we're looking at
     * @param element The element that will be displayed
     * @return A string containing the referenced mappings
     */
    public static String getReferencedMappingsText(COREMapping<? extends EObject> mapping, EObject element) {
        StringBuilder result = new StringBuilder();
        if (mapping.getReferencedMappings().size() >= 1 
                && (mapping.getFrom() == null || mapping.getFrom() == element)) {
            result.append("<");
            for (COREMapping<?> refMapping : mapping.getReferencedMappings()) {
                if (refMapping.getTo() != null) {
                    NamedElement namedElement = (NamedElement) refMapping.getTo();
                    result.append(namedElement.getName());
                    result.append(",");
                }
            }
            result.deleteCharAt(result.length() - 1);
            result.append(">");
        } else {
            Collection<COREMapping<? extends EObject>> mappings 
                = COREArtefactUtil.getNextReferencedMappings(element);
            
            if (mappings != null) {
                result.append("<");
                for (COREMapping<? extends EObject> refMapping : mappings) {
                    NamedElement namedElement = (NamedElement) refMapping.getTo();
                    result.append(namedElement.getName());
                    result.append(",");
                }
                result.deleteCharAt(result.length() - 1);
                result.append(">");
            }
        }
        return result.toString();
    }

}
