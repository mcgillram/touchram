package ca.mcgill.sel.ram.util;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Helper class for convenient test methods.
 * 
 * @author mschoettle
 */
public final class TestUtil {

    /**
     * Creates a new instance.
     */
    private TestUtil() {

    }

    /**
     * Asserts that the actual value contains only the expected values and in the order given in expected.
     * 
     * @param actual the actual value
     * @param expected the expected value
     * @param <T> the type of the elements
     */
    public static <T> void assertContainsOnlyElementsExactlyOf(Iterable<T> actual, Iterable<T> expected) {
        assertThat(actual).containsOnlyElementsOf(expected).containsExactlyElementsOf(expected);
    }

}
