package ca.mcgill.sel.ram.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import org.eclipse.emf.ecore.EObject;
import org.junit.Rule;
import org.junit.Test;

import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.AssociationEnd;
import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.util.MessageViewTestUtil;
import ca.mcgill.sel.ram.util.Model;
import ca.mcgill.sel.ram.util.ModelResource;

public class AssociationControllerTest {
    @Rule
    public final ModelResource modelResource = new ModelResource("tests/models/undefined.ram") {

        @Override
        public void modelLoaded(EObject model) {
            aspect = (Aspect) model;
        }
        
    };
    
    private static AssociationController controller = ControllerFactory.INSTANCE.getAssociationController();
    
    private Aspect aspect;
    
    /**
     * (A ---> B) becomes (A --- B)
     */
    @Test
    @Model(fileName = "tests/models/AssociationTest/AssociationTest.ram")
    public void setNavigable_Bidirectional() {
        Classifier a = MessageViewTestUtil.getClassifierByName(aspect, "A");
        Classifier b = MessageViewTestUtil.getClassifierByName(aspect, "B");
        AssociationEnd assocEnd = a.getAssociationEnds().get(0);
        AssociationEnd oppositeEnd = b.getAssociationEnds().get(0);

        assertThat(assocEnd).hasFieldOrPropertyWithValue("navigable", true);
        assertThat(oppositeEnd).hasFieldOrPropertyWithValue("navigable", false);
        
        controller.setNavigable(aspect, assocEnd, true, oppositeEnd, true);
        
        assertThat(assocEnd).hasFieldOrPropertyWithValue("navigable", true);
        assertThat(oppositeEnd).hasFieldOrPropertyWithValue("navigable", true);
    }
    
    /**
     * (A ---> B) becomes (A <--- B)
     */
    @Test
    @Model(fileName = "tests/models/AssociationTest/AssociationTest.ram")
    public void setNavigable_Reverse() {
        Classifier a = MessageViewTestUtil.getClassifierByName(aspect, "A");
        Classifier b = MessageViewTestUtil.getClassifierByName(aspect, "B");
        AssociationEnd assocEnd = a.getAssociationEnds().get(0);
        AssociationEnd oppositeEnd = b.getAssociationEnds().get(0);

        assertThat(assocEnd).hasFieldOrPropertyWithValue("navigable", true);
        assertThat(oppositeEnd).hasFieldOrPropertyWithValue("navigable", false);
        
        controller.setNavigable(aspect, assocEnd, false, oppositeEnd, true);
        
        assertThat(assocEnd).hasFieldOrPropertyWithValue("navigable", false);
        assertThat(oppositeEnd).hasFieldOrPropertyWithValue("navigable", true);
    }
    
    /**
     * (C --- D) becomes (C <--- D)
     */
    @Test
    @Model(fileName = "tests/models/AssociationTest/AssociationTest.ram")
    public void setNavigable_Unidirectional() {
        Classifier c = MessageViewTestUtil.getClassifierByName(aspect, "C");
        Classifier d = MessageViewTestUtil.getClassifierByName(aspect, "D");
        AssociationEnd assocEnd = c.getAssociationEnds().get(0);
        AssociationEnd oppositeEnd = d.getAssociationEnds().get(0);

        assertThat(assocEnd).hasFieldOrPropertyWithValue("navigable", true);
        assertThat(oppositeEnd).hasFieldOrPropertyWithValue("navigable", true);
        
        controller.setNavigable(aspect, assocEnd, false, oppositeEnd, true);
        
        assertThat(assocEnd).hasFieldOrPropertyWithValue("navigable", false);
        assertThat(oppositeEnd).hasFieldOrPropertyWithValue("navigable", true);
    }
    
    /**
     * (A ---> B) stays (A ---> B)
     */
    @Test
    @Model(fileName = "tests/models/AssociationTest/AssociationTest.ram")
    public void setNavigable_StaysUnidirectional() {
        Classifier a = MessageViewTestUtil.getClassifierByName(aspect, "A");
        Classifier b = MessageViewTestUtil.getClassifierByName(aspect, "B");
        AssociationEnd assocEnd = a.getAssociationEnds().get(0);
        AssociationEnd oppositeEnd = b.getAssociationEnds().get(0);

        assertThat(assocEnd).hasFieldOrPropertyWithValue("navigable", true);
        assertThat(oppositeEnd).hasFieldOrPropertyWithValue("navigable", false);
        
        controller.setNavigable(aspect, assocEnd, true, oppositeEnd, false);
        
        assertThat(assocEnd).hasFieldOrPropertyWithValue("navigable", true);
        assertThat(oppositeEnd).hasFieldOrPropertyWithValue("navigable", false);
    }

    /**
     * (C --- D) stays (C --- D)
     */
    @Test
    @Model(fileName = "tests/models/AssociationTest/AssociationTest.ram")
    public void setNavigable_StaysBidirectional() {
        Classifier c = MessageViewTestUtil.getClassifierByName(aspect, "C");
        Classifier d = MessageViewTestUtil.getClassifierByName(aspect, "D");
        AssociationEnd assocEnd = c.getAssociationEnds().get(0);
        AssociationEnd oppositeEnd = d.getAssociationEnds().get(0);

        assertThat(assocEnd).hasFieldOrPropertyWithValue("navigable", true);
        assertThat(oppositeEnd).hasFieldOrPropertyWithValue("navigable", true);
        
        controller.setNavigable(aspect, assocEnd, true, oppositeEnd, true);
        
        assertThat(assocEnd).hasFieldOrPropertyWithValue("navigable", true);
        assertThat(oppositeEnd).hasFieldOrPropertyWithValue("navigable", true);
    }
    
    /**
     * (A ---> B) becomes (A none B)
     */
    @Test
    @Model(fileName = "tests/models/AssociationTest/AssociationTest.ram")
    public void setNavigableto_Bad1() {
        Classifier a = MessageViewTestUtil.getClassifierByName(aspect, "A");
        Classifier b = MessageViewTestUtil.getClassifierByName(aspect, "B");
        AssociationEnd assocEnd = a.getAssociationEnds().get(0);
        AssociationEnd oppositeEnd = b.getAssociationEnds().get(0);

        assertThat(assocEnd).hasFieldOrPropertyWithValue("navigable", true);
        assertThat(oppositeEnd).hasFieldOrPropertyWithValue("navigable", false);
                
        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> {
            controller.setNavigable(aspect, assocEnd, false, oppositeEnd, false);
        }).withNoCause()
          .withMessageContaining("The Association has to be Navigable");
        
    }
    
    /**
     * (C --- D) becomes (C none D)
     */
    @Test
    @Model(fileName = "tests/models/AssociationTest/AssociationTest.ram")
    public void setNavigableto_Bad2() {
        Classifier c = MessageViewTestUtil.getClassifierByName(aspect, "C");
        Classifier d = MessageViewTestUtil.getClassifierByName(aspect, "D");
        AssociationEnd assocEnd = c.getAssociationEnds().get(0);
        AssociationEnd oppositeEnd = d.getAssociationEnds().get(0);

        assertThat(assocEnd).hasFieldOrPropertyWithValue("navigable", true);
        assertThat(oppositeEnd).hasFieldOrPropertyWithValue("navigable", true);
                
        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> {
            controller.setNavigable(aspect, assocEnd, false, oppositeEnd, false);
        }).withNoCause()
          .withMessageContaining("The Association has to be Navigable");
        
    }
}
