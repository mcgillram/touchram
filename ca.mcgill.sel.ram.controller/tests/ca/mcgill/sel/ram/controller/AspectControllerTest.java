package ca.mcgill.sel.ram.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.junit.Rule;
import org.junit.Test;

import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.CORECIElement;
import ca.mcgill.sel.core.COREPartialityType;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.ram.AbstractMessageView;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.AspectMessageView;
import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.InteractionFragment;
import ca.mcgill.sel.ram.Lifeline;
import ca.mcgill.sel.ram.MessageView;
import ca.mcgill.sel.ram.MessageViewReference;
import ca.mcgill.sel.ram.Operation;
import ca.mcgill.sel.ram.OperationMapping;
import ca.mcgill.sel.ram.OriginalBehaviorExecution;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.util.MessageViewTestUtil;
import ca.mcgill.sel.ram.util.MessageViewUtil;
import ca.mcgill.sel.ram.util.Model;
import ca.mcgill.sel.ram.util.ModelResource;

public class AspectControllerTest {
    
    @Rule
    public final ModelResource modelResource = new ModelResource("tests/models/undefined.ram") {

        @Override
        public void modelLoaded(EObject model) {
            aspect = (Aspect) model;
        }
        
    };
    
    private static AspectController controller = ControllerFactory.INSTANCE.getAspectController();
    
    private Aspect aspect;
    
    /**
     * Test AMV creation for operation with message view.
     */
    @Test
    @Model(fileName = "tests/models/MessageViews/MappedOperations/Extendee.ram")
    public void testCreateAspectMessageView_OperationWithMessageView_AMVCreated() {
        Classifier classifier = MessageViewTestUtil.getClassifierByName(aspect, "TheB");
        Operation operation = MessageViewTestUtil.getOperationByName(classifier, "hasMessageView");
        MessageView messageView = MessageViewUtil.getMessageViewFor(aspect, operation);
        assertThat(messageView.getSpecification()).isNotNull();
        
        List<AbstractMessageView> messageViews = new ArrayList<>(aspect.getMessageViews());
        List<AspectMessageView> affectedBys = new ArrayList<>(messageView.getAffectedBy());
        
        controller.createAspectMessageView(aspect, operation);
        
        assertAspectMessageViewExists(operation, messageView, messageViews, affectedBys);
    }
    
    /**
     * Test AMV creation for non-partial operation with no message view.
     */
    @Test
    @Model(fileName = "tests/models/MessageViews/MappedOperations/Extendee.ram")
    public void testCreateAspectMessageView_OperationWithNoMessageView_MVAndAMVCreated() {
        Classifier classifier = MessageViewTestUtil.getClassifierByName(aspect, "TheB");
        Operation operation = MessageViewTestUtil.getOperationByName(classifier, "hasNoMessageViewButAdviceMe");
        MessageView messageView = MessageViewUtil.getMessageViewFor(aspect, operation);
        
        assertThat(messageView).isNull();
        assertThat(operation.getPartiality()).isEqualTo(COREPartialityType.NONE);
        
        List<AbstractMessageView> messageViews = new ArrayList<>(aspect.getMessageViews());
        
        controller.createAspectMessageView(aspect, operation);
        
        messageView = MessageViewUtil.getMessageViewFor(aspect, operation);
        assertMessageViewCreated(messageView);
        messageViews.add(messageView);
        
        assertAspectMessageViewExists(operation, messageView, messageViews, new ArrayList<AspectMessageView>());
    }
    
    /**
     * Test AMV creation for partial operation with no message view.
     */
    @Test
    @Model(fileName = "tests/models/MessageViews/MappedOperations/Extendee.ram")
    public void testCreateAspectMessageView_PartialOperationWithNoMessageView_EmptyMVAndAMVCreated() {
        Classifier classifier = MessageViewTestUtil.getClassifierByName(aspect, "TheB");
        Operation operation = MessageViewTestUtil.getOperationByName(classifier, "isPartial");
        MessageView messageView = MessageViewUtil.getMessageViewFor(aspect, operation);
        
        assertThat(messageView).isNull();
        assertThat(operation.getPartiality()).isEqualTo(COREPartialityType.PUBLIC);
        
        List<AbstractMessageView> messageViews = new ArrayList<>(aspect.getMessageViews());
        
        controller.createAspectMessageView(aspect, operation);
        
        messageView = MessageViewUtil.getMessageViewFor(aspect, operation);
        assertMessageViewCreated(messageView);
        messageViews.add(messageView);
        
        assertAspectMessageViewExists(operation, messageView, messageViews, new ArrayList<AspectMessageView>());
    }
    
    /**
     * Test AMV creation for concern partial operation with no message view.
     */
    @Test
    @Model(fileName = "tests/models/MessageViews/MappedOperations/Extendee.ram")
    public void testCreateAspectMessageView_ConcernPartialMethodWithNoMessageView() {                
        Classifier classifier = MessageViewTestUtil.getClassifierByName(aspect, "TheB");
        Operation operation = MessageViewTestUtil.getOperationByName(classifier, "isConcernPartial");
        MessageView messageView = MessageViewUtil.getMessageViewFor(aspect, operation);
        
        assertThat(messageView).isNull();
        assertThat(operation.getPartiality()).isEqualTo(COREPartialityType.CONCERN);
        
        List<AbstractMessageView> messageViews = new ArrayList<>(aspect.getMessageViews());
        
        controller.createAspectMessageView(aspect, operation);
        
        messageView = MessageViewUtil.getMessageViewFor(aspect, operation);
        assertMessageViewCreated(messageView);
        messageViews.add(messageView);
        
        assertAspectMessageViewExists(operation, messageView, messageViews, new ArrayList<AspectMessageView>());
    }
    
    /**
     * Test AMV creation for mapped operation (extended) with no message view.
     * 
     * @issue #396
     */
    @Test
    @Model(fileName = "tests/models/MessageViews/MappedOperations/Extendee.ram")
    public void testCreateAspectMessageView_ExtendedOperationMappedNoMV_NoMVCreated() {
        Classifier classifier = MessageViewTestUtil.getClassifierByName(aspect, "TheB");
        Operation operation = MessageViewTestUtil.getOperationByName(classifier, "hasNoName");
        
        MessageView messageView = MessageViewUtil.getMessageViewFor(aspect, operation);
        assertThat(messageView).isNull();
        
        Operation mappedFrom = getMappedFromOperation(aspect, operation);
        MessageView externalMessageView = getExternalMessageView(mappedFrom);
        assertThat(externalMessageView).isNull();
        
        // TODO: if Java 8 is supported change everywhere to:
        // Throwable thrown = catchThrowable(() -> { controller.createAspectMessageView(aspect, operation); });
        // 
        // assertThat(thrown).isInstanceOf(IllegalArgumentException.class)
        //                   .hasMessageContaining("no message view");
        
        try {
            controller.createAspectMessageView(aspect, operation);
            
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageContaining("No message view");
        }
    }
    
    /**
     * Test AMV creation for mapped operation (extended) with message view.
     * 
     * @issue #396
     */
    @Test
    @Model(fileName = "tests/models/MessageViews/MappedOperations/Extendee.ram")
    public void testCreateAspectMessageView_ExtendedOperationMapped_NoMVCreated() {
        Classifier classifier = MessageViewTestUtil.getClassifierByName(aspect, "TheA");
        Operation operation = MessageViewTestUtil.getOperationByName(classifier, "hasBetterName");
        
        MessageView messageView = MessageViewUtil.getMessageViewFor(aspect, operation);
        assertThat(messageView).isNull();
        
        Operation mappedFrom = getMappedFromOperation(aspect, operation);
        MessageView externalMessageView = getExternalMessageView(mappedFrom);
        assertThat(externalMessageView).isNotNull();
        
        List<AbstractMessageView> messageViews = new ArrayList<>(aspect.getMessageViews());
        
        controller.createAspectMessageView(aspect, operation);
        
        assertMessageViewReferenceExists(externalMessageView);
        
        MessageViewReference reference = getMessageViewReference(externalMessageView);
        messageViews.add(reference);
        
        assertAspectMessageViewExists(operation, reference, messageViews, new ArrayList<AspectMessageView>());
    }
    
    /**
     * Test AMV creation for mapped operation (extended) with message view and already existing message view reference.
     * 
     * @issue #396
     */
    @Test
    @Model(fileName = "tests/models/MessageViews/MappedOperations/Extendee.ram")
    public void testCreateAspectMessageView_ExtendedOperationMapped_ExistingReference() {
        Classifier classifier = MessageViewTestUtil.getClassifierByName(aspect, "TheA");
        Operation operation = MessageViewTestUtil.getOperationByName(classifier, "hasReference");
        
        MessageView messageView = MessageViewUtil.getMessageViewFor(aspect, operation);
        assertThat(messageView).isNull();
        
        Operation mappedFrom = getMappedFromOperation(aspect, operation);
        MessageView externalMessageView = getExternalMessageView(mappedFrom);
        assertThat(externalMessageView).isNotNull();        
        
        MessageViewReference reference = getMessageViewReference(externalMessageView);
        assertThat(reference.getAffectedBy()).hasSize(1);
        
        List<AspectMessageView> affectedBys = new ArrayList<>(reference.getAffectedBy());
        List<AbstractMessageView> messageViews = new ArrayList<>(aspect.getMessageViews());
        AspectMessageView existingAMV = getAspectMessageView(operation);
        
        controller.createAspectMessageView(aspect, operation);
        
        List<AspectMessageView> amvs = MessageViewUtil.getMessageViewsOfType(aspect, AspectMessageView.class);
        assertThat(amvs).hasSize(2);
        
        amvs.remove(existingAMV);
        AspectMessageView amv = amvs.get(0);
        assertThat(amv.getPointcut()).isEqualTo(operation);
        
        messageViews.add(amv);
        affectedBys.add(amv);

        assertThat(aspect.getMessageViews()).containsOnlyElementsOf(messageViews);
        assertThat(reference.getAffectedBy()).containsOnlyElementsOf(affectedBys)
                                             .containsExactlyElementsOf(affectedBys);
    }

    /**
     * Test AMV creation for mapped operation (reused) with no message view.
     * 
     * @issue #396
     */
    @Test
    @Model(fileName = "tests/models/MessageViews/MappedOperations/Extendee.ram")
    public void testCreateAspectMessageView_ReusedOperationMappedNoMV_NoMVCreated() {
        Classifier classifier = MessageViewTestUtil.getClassifierByName(aspect, "TheA");
        Operation operation = MessageViewTestUtil.getOperationByName(classifier, "mappedAndNoMV");
        
        MessageView messageView = MessageViewUtil.getMessageViewFor(aspect, operation);
        assertThat(messageView).isNull();
        
        Operation mappedFrom = getMappedFromOperation(aspect, operation);
        MessageView externalMessageView = getExternalMessageView(mappedFrom);
        assertThat(externalMessageView).isNull();
        
        try {
            controller.createAspectMessageView(aspect, operation);
            
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageContaining("No message view");
        }
    }
    
    /**
     * Test AMV creation for mapped partial operation (reused) with a message view.
     * 
     * @issue #396
     */
    @Test
    @Model(fileName = "tests/models/MessageViews/MappedOperations/Extendee.ram")
    public void testCreateAspectMessageView_ReusedPartialOperationMapped_MVCreated() {
        Classifier classifier = MessageViewTestUtil.getClassifierByName(aspect, "TheA");
        Operation operation = MessageViewTestUtil.getOperationByName(classifier, "modifyTheB");
        
        MessageView messageView = MessageViewUtil.getMessageViewFor(aspect, operation);
        assertThat(messageView).isNull();
        
        Operation mappedFrom = getMappedFromOperation(aspect, operation);
        assertThat(mappedFrom.getPartiality()).isEqualTo(COREPartialityType.PUBLIC);
        
        MessageView externalMessageView = getExternalMessageView(mappedFrom);
        assertThat(externalMessageView).isNotNull();
        
        List<AbstractMessageView> messageViews = new ArrayList<>(aspect.getMessageViews());
        
        controller.createAspectMessageView(aspect, operation);
        
        messageView = MessageViewUtil.getMessageViewFor(aspect, operation);
        assertMessageViewCreated(messageView);
        messageViews.add(messageView);
        
        assertAspectMessageViewExists(operation, messageView, messageViews, new ArrayList<AspectMessageView>());
    }
    
    /**
     * Test AMV creation for mapped partial operation (reused) with a message view.
     * 
     * @issue #396
     */
    @Test
    @Model(fileName = "tests/models/MessageViews/MappedOperations/Extendee.ram")
    public void testCreateAspectMessageView_ReusedPartialOperationMappedNoMV_MVCreated() {
        Classifier classifier = MessageViewTestUtil.getClassifierByName(aspect, "TheA");
        Operation operation = MessageViewTestUtil.getOperationByName(classifier, "mappedFromPartialAndNoMV");
        
        MessageView messageView = MessageViewUtil.getMessageViewFor(aspect, operation);
        assertThat(messageView).isNull();
        
        Operation mappedFrom = getMappedFromOperation(aspect, operation);
        assertThat(mappedFrom.getPartiality()).isEqualTo(COREPartialityType.PUBLIC);
        
        MessageView externalMessageView = getExternalMessageView(mappedFrom);
        assertThat(externalMessageView).isNull();
        
        List<AbstractMessageView> messageViews = new ArrayList<>(aspect.getMessageViews());
        
        controller.createAspectMessageView(aspect, operation);
        
        messageView = MessageViewUtil.getMessageViewFor(aspect, operation);
        assertMessageViewCreated(messageView);
        messageViews.add(messageView);
        
        assertAspectMessageViewExists(operation, messageView, messageViews, new ArrayList<AspectMessageView>());
    }

    private void assertMessageViewCreated(MessageView messageView) {
        assertThat(messageView).isNotNull();
        
        CORECIElement ci = COREArtefactUtil.getCIElementFor(messageView.getSpecifies());
        if (ci != null && ci.getPartiality() != COREPartialityType.NONE) {
            assertThat(messageView.getSpecification()).isNull();
        } else {
            assertThat(messageView.getSpecification()).isNotNull();
        }
    }

    private void assertAspectMessageViewExists(Operation operation,
                    AbstractMessageView messageView,
                    List<AbstractMessageView> previousMessageViews, 
                    List<AspectMessageView> previousAffectedBys) {
        AspectMessageView amv = getAspectMessageView(operation);
        
        previousMessageViews.add(amv);
        previousAffectedBys.add(amv);
        
        assertThat(aspect.getMessageViews()).containsOnlyElementsOf(previousMessageViews);
        assertThat(messageView.getAffectedBy()).containsOnlyElementsOf(previousAffectedBys)
                                               .containsExactlyElementsOf(previousAffectedBys);
        
        assertThat(amv.getAdvice()).isNotNull();
        InteractionFragment fragment = amv.getAdvice().getFragments().get(1);
        Lifeline targetLifeline = MessageViewTestUtil.getLifelineByName(amv.getAdvice(), "target");
        assertThat(targetLifeline.getCoveredBy()).contains(fragment);
        assertThat(fragment).isInstanceOf(OriginalBehaviorExecution.class);
    }
    
    private void assertMessageViewReferenceExists(MessageView referenced) {
        MessageViewReference reference = getMessageViewReference(referenced);
        assertThat(reference.getReferences()).isEqualTo(referenced);
    }

    private MessageView getExternalMessageView(Operation externalOperation) {
        Aspect externalAspect = EMFModelUtil.getRootContainerOfType(externalOperation, RamPackage.Literals.ASPECT);
        MessageView externalMessageView = MessageViewUtil.getMessageViewFor(externalAspect, externalOperation);
        
        return externalMessageView;
    }
    

    private AspectMessageView getAspectMessageView(Operation operation) {
        List<AspectMessageView> aspectMessageViews = MessageViewUtil.getMessageViewsOfType(aspect, AspectMessageView.class);
        assertThat(aspectMessageViews).as("No AspectMessageView found")
                                      .isNotEmpty();
        
        assertThat(aspectMessageViews).extracting("pointcut")
                                      .as("More than one AspectMessageView for the same Operation.")
                                      .containsOnlyOnce(operation);
        
        for (AspectMessageView amv : aspectMessageViews) {
            if (amv.getPointcut() == operation) {
                return amv;
            }
        }
        
        fail("No AspectMessageView found.");
        
        return null;
    }
    
    private MessageViewReference getMessageViewReference(MessageView referenced) {
        List<MessageViewReference> references = MessageViewUtil.getMessageViewsOfType(aspect, MessageViewReference.class);
        assertThat(references).as("No MessageViewReference found")
                              .isNotEmpty();
        
        assertThat(references).extracting("references")
                              .as("More than one MessageViewReference for the same MessageView.")
                              .containsOnlyOnce(referenced);
        
        for (MessageViewReference reference : references) {
            if (reference.getReferences() == referenced) {
                return reference;
            }
        }
        
        fail("No MessageViewReference found.");
        
        return null;
    }
    
    private Operation getMappedFromOperation(Aspect current, Operation mapped) {
        Collection<OperationMapping> mappings = EMFModelUtil.findCrossReferencesOfType(mapped, 
                CorePackage.Literals.CORE_LINK__TO, RamPackage.Literals.OPERATION_MAPPING);
        assertThat(mappings).as("No mapping exists").hasSize(1);
        
        OperationMapping mapping = mappings.iterator().next();
        assertThat(mapping.getTo()).isEqualTo(mapped);
        assertThat(mapping.getFrom()).isNotNull();
        assertThat(mapping.getFrom().eIsProxy()).isFalse();
        
        return mapping.getFrom();
    }
    
    @Test
    @Model(fileName = "tests/models/MessageViews/MappedOperations/MappedOperations.ram")
    public void testRemoveAspectMessageView_SingleOperationAffected_AMVRemoved() {
        Classifier classifier = MessageViewTestUtil.getClassifierByName(aspect, "B");
        Operation operation = MessageViewTestUtil.getOperationByName(classifier, "isPartial");
        MessageView messageView = MessageViewUtil.getMessageViewFor(aspect, operation);
        assertThat(messageView).isNotNull();
        
        AspectMessageView existingAMV = MessageViewTestUtil.getAspectMessageView(aspect, operation);
        
        controller.removeAspectMessageView(existingAMV);
        
        assertThat(existingAMV.eContainer()).isNull();
        assertThat(aspect.getLayout().getContainers().keySet()).doesNotContain(existingAMV);
        assertThat(messageView.getAffectedBy()).doesNotContain(existingAMV);
    }
    
    @Test
    @Model(fileName = "tests/models/MessageViews/MappedOperations/MappedOperations.ram")
    public void testRemoveAspectMessageView_SeveralOperationsAffected_AffectedByRemoved() {
        Classifier classifier = MessageViewTestUtil.getClassifierByName(aspect, "B");
        Operation operation = MessageViewTestUtil.getOperationByName(classifier, "isPartial");
        MessageView messageView = MessageViewUtil.getMessageViewFor(aspect, operation);
        assertThat(messageView).isNotNull();
        
        AspectMessageView existingAMV = MessageViewTestUtil.getAspectMessageView(aspect, operation);
        
        Classifier otherClassifier = MessageViewTestUtil.getClassifierByName(aspect, "A");
        Operation otherOperation = MessageViewTestUtil.getOperationByName(otherClassifier, "hasMessageView");
        MessageView otherMessageView = MessageViewUtil.getMessageViewFor(aspect, otherOperation);
        assertThat(otherMessageView).isNotNull();
        
        otherMessageView.getAffectedBy().add(existingAMV);
        
        controller.removeAspectMessageView(existingAMV);
        
        assertThat(existingAMV.eContainer()).isNull();
        assertThat(messageView.getAffectedBy()).doesNotContain(existingAMV);
        assertThat(otherMessageView.getAffectedBy()).doesNotContain(existingAMV);
    }
    
}
