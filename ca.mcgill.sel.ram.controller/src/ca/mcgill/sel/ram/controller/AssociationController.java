package ca.mcgill.sel.ram.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.DeleteCommand;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.CORECIElement;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREConfiguration;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.COREFeatureModel;
import ca.mcgill.sel.core.COREMapping;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.COREModelElementComposition;
import ca.mcgill.sel.core.COREModelReuse;
import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.core.CoreFactory;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.core.util.COREModelUtil;
import ca.mcgill.sel.core.language.weaver.COREWeaver;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.Association;
import ca.mcgill.sel.ram.AssociationEnd;
import ca.mcgill.sel.ram.AssociationEndMapping;
import ca.mcgill.sel.ram.Class;
import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.ClassifierMapping;
import ca.mcgill.sel.ram.LayoutElement;
import ca.mcgill.sel.ram.Operation;
import ca.mcgill.sel.ram.OperationMapping;
import ca.mcgill.sel.ram.Parameter;
import ca.mcgill.sel.ram.RamFactory;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.ReferenceType;
import ca.mcgill.sel.ram.Type;
import ca.mcgill.sel.ram.controller.util.RAMReferenceUtil;
import ca.mcgill.sel.ram.util.Constants;
import ca.mcgill.sel.ram.util.RAMModelUtil;

/**
 * The controller for {@link Association}s and {@link AssociationEnd}s.
 *
 * @author mschoettle
 * @author cbensoussan
 */
public class AssociationController extends BaseController {

    private static final String AUTOMATIC_SELECTION = "Automatic selection";

    /**
     * Creates a new instance of {@link AssociationController}.
     */
    protected AssociationController() {
        // Prevent anyone outside this package to instantiate.
    }

    /**
     * Sets the selected features of the given {@link AssociationEnd} according to the given set of features
     * and executes the command.
     *
     * @param aspect The current aspect
     * @param associationEnd the {@link AssociationEnd} the selected features should be changed for
     * @param reuseConfiguration The reuse configuration when the user reused the concern
     */
    public void setFeatureSelection(Aspect aspect, AssociationEnd associationEnd,
            COREConfiguration reuseConfiguration) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(associationEnd);
        AssociationEnd oppositeEnd = associationEnd.getOppositeEnd();
        CompoundCommand compoundCommand;
        if (oppositeEnd.isNavigable()) {
            compoundCommand = new BidirectionalCompoundCommand(aspect, associationEnd, oppositeEnd, reuseConfiguration);
        } else {
            compoundCommand = new UnidirectionalCompoundCommand(aspect, associationEnd, oppositeEnd,
                    reuseConfiguration);
            compoundCommand.append(deleteFeatureSelection(compoundCommand, aspect, associationEnd));
        }
        
        // Dirty fix to not have an empty compound command when executing
        compoundCommand.append(SetCommand.create(editingDomain, reuseConfiguration, 
                CorePackage.Literals.CORE_NAMED_ELEMENT__NAME, reuseConfiguration.getName()));
        
        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Change the selected features of the given {@link AssociationEnd} according to the given set of features
     * and executes the command.
     *
     * @param aspect The current aspect
     * @param associationEnd the {@link AssociationEnd} the selected features should be changed for
     * @param newConfiguration The new configuration that should be used
     */
    public void changeFeatureSelection(Aspect aspect, AssociationEnd associationEnd,
            COREConfiguration newConfiguration) {
        AssociationEnd oppositeEnd = associationEnd.getOppositeEnd();

        if (oppositeEnd.isNavigable()) {
            modifyAssociation(aspect, associationEnd, true, associationEnd.getLowerBound(),
                    associationEnd.getUpperBound(), newConfiguration, oppositeEnd, true,
                    oppositeEnd.getFeatureSelection().getConfiguration());
        } else {
            modifyAssociation(aspect, associationEnd, true, associationEnd.getLowerBound(),
                    associationEnd.getUpperBound(), newConfiguration, oppositeEnd, false, null);           
        }
    }

    /**
     * Creates the feature selection, helper when called without selectedFeatures.
     * @param aspect The current aspect
     * @param concern The current concern
     * @param associationEnd The association end
     * @param oppositeEnd The opposite end
     * @return The compound command
     */
    public CompoundCommand createFeatureSelection(Aspect aspect, COREConcern concern, AssociationEnd associationEnd,
            AssociationEnd oppositeEnd) {
        return createFeatureSelection(aspect, concern, associationEnd, oppositeEnd, null, null);
    }

    /**
     * Creates the feature selection including a model reuse.
     * 
     * @param aspect The current aspect
     * @param concern The current concern
     * @param associationEnd The association end
     * @param oppositeEnd The opposite end
     * @param userSelectedConfiguration The selected features
     * @param oppositeSelectedConfiguration The selected configuration of the opposite end
     * @return The compound command
     */
    private CompoundCommand createFeatureSelection(Aspect aspect, COREConcern concern, AssociationEnd associationEnd,
            AssociationEnd oppositeEnd, COREConfiguration userSelectedConfiguration,
            COREConfiguration oppositeSelectedConfiguration) {
        
        CompoundCommand compoundCommand = new CompoundCommand();
        COREExternalArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(aspect);

        COREConcern associationConcern = null;
        try {
            associationConcern = COREModelUtil.getLocalConcern(Constants.ASSOCIATION_CONCERN_LOCATION,
                    concern.eResource().getURI());
        } catch (IOException e) {
            return null;
        }

        HashSet<COREFeature> automaticSelectedFeatures = new HashSet<COREFeature>();
        if (userSelectedConfiguration != null) {
            automaticSelectedFeatures.addAll(userSelectedConfiguration.getSelected());
        }

        determineAssociationFeatures(associationEnd, oppositeEnd, associationConcern,
                automaticSelectedFeatures, oppositeSelectedConfiguration);
        
        // In case the features cannot be found, make sure that there is no invalid reuse created.
        if (!automaticSelectedFeatures.isEmpty()) {
            COREModelReuse modelReuse = createModelReuse(aspect, associationEnd, oppositeEnd, associationConcern,
                    userSelectedConfiguration, automaticSelectedFeatures);
    
            EditingDomain editingDomain = EMFEditUtil.getEditingDomain(concern);

            // Add the model reuse to the artefact that refers to the aspect with the association
            compoundCommand.append(AddCommand.create(editingDomain, artefact,
                    CorePackage.Literals.CORE_ARTEFACT__MODEL_REUSES, modelReuse));
    
            // Add the reuse to the concern, if needed
            if (modelReuse.getReuse().eContainer() == null) {
                compoundCommand.append(AddCommand.create(editingDomain, concern,
                        CorePackage.Literals.CORE_CONCERN__REUSES, modelReuse.getReuse()));
            }
    
            // add the model reuse also to the corresponding association end 
            compoundCommand.append(SetCommand.create(editingDomain, associationEnd,
                    RamPackage.Literals.ASSOCIATION_END__FEATURE_SELECTION, modelReuse));

            // now lets weave the association concern
            COREExternalArtefact newArtefact = COREWeaver.getInstance().weaveReuse(artefact,
                    associationConcern, modelReuse.getConfiguration());
            modelReuse.setSource(newArtefact);
            
            // now lets create the mappings for the Source and Destination classes
            ClassifierMapping cmSource = RamFactory.eINSTANCE.createClassifierMapping();
            
            // now lets create the source and target mappings
            Aspect wovenAspect = (Aspect) newArtefact.getRootModelElement();
            Classifier source = null;
            for (Classifier c : wovenAspect.getStructuralView().getClasses()) {
                if (c.getName().equals(Constants.ASSOCIATION_SOURCE_CLASS_NAME)) {
                    source = c;
                    break;
                }
            }
            cmSource.setFrom(source);
            cmSource.setTo(associationEnd.getClassifier());
            compoundCommand.append(AddCommand.create(editingDomain, modelReuse,
                    CorePackage.Literals.CORE_MODEL_COMPOSITION__COMPOSITIONS, cmSource));

            ClassifierMapping cmTarget = RamFactory.eINSTANCE.createClassifierMapping();
            Classifier target = null;
            for (Classifier c : wovenAspect.getStructuralView().getClasses()) {
                if (c.getName().equals(Constants.ASSOCIATION_TARGET_CLASS_NAME)) {
                    target = c;
                    break;
                }
            }
            cmTarget.setFrom(target);
            cmTarget.setTo(oppositeEnd.getClassifier());
            compoundCommand.append(AddCommand.create(editingDomain, modelReuse,
                    CorePackage.Literals.CORE_MODEL_COMPOSITION__COMPOSITIONS, cmTarget));

            // now we need to create a mapping for the association end myTarget
            AssociationEndMapping aeMappingTarget = RamFactory.eINSTANCE.createAssociationEndMapping();
            // there is only one association end in the Source class, namely myTarget
            aeMappingTarget.setFrom(source.getAssociationEnds().get(0)); 
            aeMappingTarget.setTo(associationEnd);
            compoundCommand.append(AddCommand.create(editingDomain, cmSource,
                    CorePackage.Literals.CORE_MAPPING__MAPPINGS, aeMappingTarget));

            // now we need to create a mapping for the association end mySource
            AssociationEndMapping aeMappingSource = RamFactory.eINSTANCE.createAssociationEndMapping();
            // there is only one association end in the Target class, namely mySource
            for (AssociationEnd e : target.getAssociationEnds()) {
                if (e.getName().equals("mySource")) {
                    aeMappingSource.setFrom(e); 
                }
            }
            aeMappingSource.setTo(oppositeEnd);
            compoundCommand.append(AddCommand.create(editingDomain, cmTarget,
                    CorePackage.Literals.CORE_MAPPING__MAPPINGS, aeMappingSource));

            // create ghosts for all the operations of the source class 
            // and rename them to use the 
            // name of the association end that points to the target class 
            // For example, if an association is created from A to B, and the association
            // end is named myB, then getMyTarget should be renamed getMyB
            for (Operation op : source.getOperations()) {
                Operation newOp = (Operation) RAMReferenceUtil.createLocalElementWithComposition(
                        editingDomain, compoundCommand, artefact, associationEnd.getClassifier(),
                        op, modelReuse);
                
                // now we must update all references contained in the operation, which still point
                // to the woven aspect, to point to the current one
                RAMReferenceUtil.updateLocalElementPropertiesWithComposition(editingDomain, compoundCommand,
                        aspect, newOp, modelReuse);
                String opName = newOp.getName();
                String associationEndName = associationEnd.getName();
                // CHECKSTYLE:IGNORE MultipleStringLiterals FOR 10 LINES: Okay here
                compoundCommand.append(SetCommand.create(editingDomain, newOp, RamPackage.Literals.NAMED_ELEMENT__NAME,
                        opName.replaceAll("Target", associationEndName.substring(0, 1).toUpperCase()
                                + associationEndName.substring(1))));
            }
        }

        return compoundCommand;
    }

    /**
     * Changes the feature selection of an association.
     * 
     * @param aspect The current aspect
     * @param concern The current concern
     * @param associationEnd The association end
     * @param oppositeEnd The opposite end
     * @param userSelectedConfiguration The selected features
     * @param oppositeSelectedConfiguration The selected configuration of the opposite end
     * @return The compound command
     */
    private CompoundCommand changeFeatureSelection(Aspect aspect, COREConcern concern, AssociationEnd associationEnd,
            AssociationEnd oppositeEnd, COREConfiguration userSelectedConfiguration,
            COREConfiguration oppositeSelectedConfiguration) {
        
        CompoundCommand compoundCommand = new CompoundCommand();
        //COREExternalArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(aspect);

        COREConcern associationConcern = null;
        try {
            associationConcern = COREModelUtil.getLocalConcern(Constants.ASSOCIATION_CONCERN_LOCATION,
                    concern.eResource().getURI());
        } catch (IOException e) {
            return null;
        }

        HashSet<COREFeature> automaticSelectedFeatures = new HashSet<COREFeature>();
        if (userSelectedConfiguration != null) {
            automaticSelectedFeatures.addAll(userSelectedConfiguration.getSelected());
        }

        determineAssociationFeatures(associationEnd, oppositeEnd, associationConcern,
                automaticSelectedFeatures, oppositeSelectedConfiguration);
        
        // In case the features cannot be found, make sure that there is no invalid reuse created.
        if (!automaticSelectedFeatures.isEmpty()) {
            COREModelReuse modelReuse = associationEnd.getFeatureSelection();
    
            //TODO: Here we must weave the aspect
            System.out.println("Here we must weave the association concern");
//            Aspect wovenAspect = COREWeaverUtil.createWovenAspect(modelReuse.getReuse().getSelectedConfiguration(),
//                    concern, true, true);
            EditingDomain editingDomain = EMFEditUtil.getEditingDomain(concern);

            // Create a new configuration for the selected features and add it
            COREConfiguration automaticConfiguration = CoreFactory.eINSTANCE.createCOREConfiguration();
            automaticConfiguration.setName(AUTOMATIC_SELECTION);
            automaticConfiguration.getSelected().addAll(automaticSelectedFeatures);
            automaticConfiguration.setSource(concern.getFeatureModel());
            
            compoundCommand.append(SetCommand.create(editingDomain, modelReuse,
                    CorePackage.Literals.CORE_MODEL_REUSE__CONFIGURATION, automaticConfiguration));
                    
            //TODO: create ghosts for all the operations
            //createReuseInstantiation(aspect, wovenAspect, associationEnd,
            //        modelReuse, compoundCommand);
    

        }

        return compoundCommand;
    }
    
    /**
     * Method that removes from configuration c any features that were selected among the "opposite"
     * features.
     * 
     * @param c the Configuration
     */
    private void removeOppositeSelection(COREConfiguration c) {
        if (c.getSelected().size() > 0) {
            Iterator<COREFeature> i = c.getSelected().iterator();
            while (i.hasNext()) {
                COREFeature f = i.next();
                if (f.getName().equalsIgnoreCase("Bidirectional")
                        || f.getName().equalsIgnoreCase("OneOpposite")
                        || f.getName().equalsIgnoreCase("PlainOpposite")
                        || f.getName().equalsIgnoreCase("OrderedOpposite")
                        || f.getName().equalsIgnoreCase("ManyOpposite")
                        || f.getName().equalsIgnoreCase("KeyIndexedOpposite")) {
                    i.remove();
                }
            }
        }        
    }

    /**
     * Creates a model reuse with a reuse and configurations given selected features.
     *
     * @param aspect The current aspect
     * @param associationEnd The association end
     * @param oppositeEnd The opposite end
     * @param concern The association concern
     * @param userSelectedConfiguration The configuration manually selected by the user
     * @param automaticallySelectedFeatures The already selected features
     *
     * @return Returns the created {@link COREModelReuse}
     */
    private COREModelReuse createModelReuse(Aspect aspect, AssociationEnd associationEnd, AssociationEnd oppositeEnd,
            COREConcern concern, COREConfiguration userSelectedConfiguration,
            Set<COREFeature> automaticallySelectedFeatures) {

        // A configuration for the complete selection
        COREConfiguration automaticConfiguration = CoreFactory.eINSTANCE.createCOREConfiguration();
        automaticConfiguration.setName(AUTOMATIC_SELECTION);
        automaticConfiguration.getSelected().addAll(automaticallySelectedFeatures);
        automaticConfiguration.setSource(concern.getFeatureModel());

        COREExternalArtefact currentArtefact = COREArtefactUtil.getReferencingExternalArtefact(aspect);
        COREReuse reuse = COREModelUtil.createReuse(currentArtefact.getCoreConcern(), concern);

        reuse.setName(getReuseName(aspect, concern.getName(), associationEnd.getClassifier().getName(),
                oppositeEnd.getClassifier().getName()));

        COREModelReuse modelReuse = CoreFactory.eINSTANCE.createCOREModelReuse();
        //modelReuse.setSource(source);
        modelReuse.setReuse(reuse);
        modelReuse.setConfiguration(automaticConfiguration);        
        return modelReuse;
    }

    /**
     * Determines what features of the Association concern need to be selected.
     * 
     * @param associationEnd The association end
     * @param oppositeEnd The opposite end
     * @param concern The association concern
     * @param selectedFeatures The already selected features, if any
     * @param oppositeSelectedConfiguration The selected configuration of the opposite end
     */
    private void determineAssociationFeatures(AssociationEnd associationEnd, AssociationEnd oppositeEnd,
            COREConcern concern, Set<COREFeature> selectedFeatures, COREConfiguration oppositeSelectedConfiguration) {
        if (associationEnd.getUpperBound() == 1) {
            selectedFeatures.addAll(getSelectedFeatures(concern.getFeatureModel(),
                    Constants.FEATURE_NAME_ONE));
        } else {
            selectedFeatures.addAll(getSelectedFeatures(concern.getFeatureModel(),
                    Constants.FEATURE_NAME_MANY));
            selectedFeatures.addAll(getSelectedFeatures(concern.getFeatureModel(),
                    Constants.FEATURE_NAME_UNIQUE));            
            if (associationEnd.getLowerBound() > 0 && !RAMModelUtil.hasKeyIndexedSelection(selectedFeatures)) {
                selectedFeatures.addAll(getSelectedFeatures(concern.getFeatureModel(),
                        Constants.FEATURE_NAME_MINIMUM));
            }
            if (associationEnd.getUpperBound() > 1) {
                selectedFeatures.addAll(getSelectedFeatures(concern.getFeatureModel(),
                        Constants.FEATURE_NAME_MAXIMUM));
            }
        }

        if (oppositeEnd.isNavigable()) {
            if (oppositeEnd.getUpperBound() == 1) {
                selectedFeatures.addAll(getSelectedFeatures(concern.getFeatureModel(),
                        Constants.FEATURE_NAME_ONE_OPPOSITE));
            } else {
                if (oppositeSelectedConfiguration != null) {
                    // Opposite side updated its selected features and its reuse was already performed.
                    if (getFeatureByName(oppositeSelectedConfiguration.getSelected(),
                            Constants.FEATURE_NAME_ORDERED) != null) {
                        selectedFeatures.addAll(getSelectedFeatures(concern.getFeatureModel(),
                                Constants.FEATURE_NAME_ORDERED_OPPOSITE));
                    } else if (getFeatureByName(oppositeSelectedConfiguration.getSelected(),
                            Constants.FEATURE_NAME_PLAIN) != null) {
                        selectedFeatures.addAll(getSelectedFeatures(concern.getFeatureModel(),
                                Constants.FEATURE_NAME_PLAIN_OPPOSITE));
                    } else {
                        selectedFeatures.addAll(getSelectedFeatures(concern.getFeatureModel(),
                                Constants.FEATURE_NAME_MANY_OPPOSITE));
                    }
                } else {
                    selectedFeatures.addAll(getSelectedFeatures(concern.getFeatureModel(),
                            Constants.FEATURE_NAME_MANY_OPPOSITE));
                }
            }
        }
    }

//    /**
//     * Given a {@link ClassifierMapping}, it gets the operations of the from classifier and creates
//     * a copy in the to classifier if it does not exist, otherwise it finds the existing one to map it.
//     *
//     * @param aspect The current aspect
//     * @param associationEnd The association end for which a selection was done
//     * @param classifierMapping The {@link ClassifierMapping} for which we need to create operation mappings
//     * @param compoundCommand The {@link CompoundCommand}
//     */
//    private void createOperationsMappings(Aspect aspect, AssociationEnd associationEnd,
//            ClassifierMapping classifierMapping, CompoundCommand compoundCommand) {
//        //TODO: Figure out what to do
//        System.out.println("Maybe need to create mappings here.");
//
        //        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(classifierMapping.getTo());
//        for (Operation operation : classifierMapping.getFrom().getOperations()) {
//            OperationMapping operationMapping = RamFactory.eINSTANCE.createOperationMapping();
//            classifierMapping.getOperationMappings().add(operationMapping);
//
//            operationMapping.setFrom(operation);
//            Operation mapped = ControllerFactory.INSTANCE.getClassController().createOperationCopyWithoutCommand(
//                    operationMapping, getOperationName(operation.getName(), associationEnd.getName(),
//                            operation.getParameters()),
//                    aspect.getStructuralView());
//
//            if (COREVisibilityType.PUBLIC.equals(mapped.getVisibility())) {
//                mapped.setVisibility(COREVisibilityType.CONCERN);
//                mapped.setExtendedVisibility(RAMVisibilityType.PACKAGE);
//            }
//
//            if (RAMModelUtil.isUniqueOperation(classifierMapping.getTo(), 
//                    mapped.getName(), mapped.getReturnType(), mapped.getParameters())) {
//                if (mapped.getOperationType() == OperationType.CONSTRUCTOR) {
//                    mapped.setVisibility(COREVisibilityType.PUBLIC);
//                    compoundCommand.append(AddCommand.create(editingDomain, classifierMapping.getTo(),
//                            RamPackage.Literals.CLASSIFIER__OPERATIONS, mapped, 0));
//                } else {
//                    compoundCommand.append(AddCommand.create(editingDomain, classifierMapping.getTo(),
//                        RamPackage.Literals.CLASSIFIER__OPERATIONS, mapped));
//                }
//            } else {
//                for (Operation existingOperation : classifierMapping.getTo().getOperations()) {
//                    if (RAMModelUtil.hasSameSignature(mapped, existingOperation)) {
//                        mapped = existingOperation;
//                    }
//                }
//            }
//            
//            if (associationEnd.isStatic()) {
//                compoundCommand.append(SetCommand.create(editingDomain, mapped, 
//                        RamPackage.Literals.OPERATION__STATIC, true));
//            }
//            operationMapping.setTo(mapped);
//        }
//    }

    /**
     * Get the unique name of the reuse in the format Association_FromClass_ToClass[0-9]* .
     * @param aspect The current aspect
     * @param concernName The name of the reused concern
     * @param fromClassName The name of the FROM classifier
     * @param toClassName The name of the TO classifier
     * @return the unique name of the reuse
     */
    private String getReuseName(Aspect aspect, String concernName, String fromClassName, String toClassName) {
        // Get the list of other reuses (used to generate a unique name for the reuse)
        Set<COREReuse> reuses = new HashSet<COREReuse>();

        //TODO: Make sure the name is unique
//        COREExternalArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(aspect);
//        for (COREFeature feature : artefact.getScene().getRealizes()) {
//            reuses.addAll(feature.getReuses());
//        }
//
        String name = concernName + "_" + fromClassName + "_" + toClassName;
        return COREModelUtil.createUniqueNameFromElements(name, reuses);
    }

//    /**
//     * Gets the name of the mapped operation.
//     * Given an association with a role name myObservers, the operation are generated as follow:
//     * get --> getMyObserver (same for add, remove, contains and getKey)
//     * size --> sizeOfMyObservers (same for keySet and values)
//     * getAssociated --> getMyObservers
//     *
//     * @param operationName the name of the operation
//     * @param associationEndName the name of the association end
//     * @param parameters the parameters of the operation
//     *
//     * @return the name of the mapped operation
//     */
//    private String getOperationName(String operationName, String associationEndName, EList<Parameter> parameters) {
//        String associationName = associationEndName.substring(0, 1).toUpperCase() + associationEndName.substring(1);
//        String associationNameSingular = associationName;
//
//        String mappedName = "";
//
//        if ("getAssociated".equals(operationName)) {
//            mappedName = "get" + associationName;
//        } else if (operationName.matches("(get|add|remove|contains|put|getKey)")) {
//            mappedName = operationName + associationNameSingular;
//            if (parameters.size() > 0 && parameters.get(0).getType() instanceof RInt) {
//                mappedName += "At";
//            }
//        } else if (operationName.matches("(size|keySet|values)")) {
//            mappedName = operationName + "Of" + associationName;
//        } else if ("create".equals(operationName)) {
//            mappedName = operationName;
//        } else {
//            mappedName = operationName + associationName;
//        }
//
//        return mappedName;
//    }

    /**
     * Returns a command that deletes the feature selection of the given {@link AssociationEnd},
     * deletes the mapped operations and the feature reuses from the features that realize the aspect.
     * Returns <code>null</code> if there is no feature selection associated with the {@link AssociationEnd}.
     *
     * @param command the overall {@link CompoundCommand} containing all to be executed commands
     * @param aspect The current aspect
     * @param associationEnd the {@link AssociationEnd} the selected features should be changed for
     *
     * @return the compound command, null if there is no feature selection associated with the association end
     */
    private CompoundCommand deleteFeatureSelection(CompoundCommand command, Aspect aspect,
                AssociationEnd associationEnd) {
        if (associationEnd.getFeatureSelection() != null) {
            EditingDomain editingDomain = EMFEditUtil.getEditingDomain(associationEnd);
            CompoundCommand compoundCommand = new CompoundCommand();

            compoundCommand.append(SetCommand.create(editingDomain, associationEnd,
                    RamPackage.Literals.ASSOCIATION_END__FEATURE_SELECTION, SetCommand.UNSET_VALUE));

//            COREModelReuse modelReuse = associationEnd.getFeatureSelection();
//            compoundCommand.append(deleteMappedOperations(command, aspect, modelReuse,
//                    associationEnd.getClassifier(), editingDomain));
//            if (associationEnd.getClassifier() != associationEnd.getType()) {
//                compoundCommand.append(deleteMappedOperations(command, aspect, modelReuse,
//                        (Classifier) associationEnd.getType(), editingDomain));
//            }
//
//            compoundCommand.append(RemoveCommand.create(editingDomain,
//                    associationEnd.getFeatureSelection().getReuse()));
//
//            compoundCommand.append(RemoveCommand.create(editingDomain, associationEnd.getFeatureSelection()));

            return compoundCommand;
        }

        return null;
    }

//    /**
//     * Delete the mapped operations.
//     *
//     * @param command the overall {@link CompoundCommand} containing all to be executed commands
//     * @param aspect The current aspect
//     * @param modelReuse the modelReuse to look into for operations to delete
//     * @param classifier The classifier for which to delete operations
//     * @param editingDomain the {@link EditingDomain} to use for executing commands
//     *
//     * @return the created {@link CompoundCommand}
//     */
//    private CompoundCommand deleteMappedOperations(CompoundCommand command, Aspect aspect, COREModelReuse modelReuse,
//            Classifier classifier, EditingDomain editingDomain) {
//        CompoundCommand compoundCommand = new CompoundCommand();
//        
//        //TODO: Figure out what to do
//        System.out.println("Maybe need to delete mappings here.");
        
//        if (modelReuse != null) {
//            for (COREModelElementComposition<?> composition : modelReuse.getCompositions()) {
//                COREMapping<?> mapping = (COREMapping<?>) composition;
//                
//                if (classifier.equals(mapping.getTo())) {
//                    ClassifierMapping classifierMapping = (ClassifierMapping) mapping;
//                    for (OperationMapping operationMapping : classifierMapping.getOperationMappings()) {
//                        Operation to = operationMapping.getTo();
//                        
//                        if (to.getOperationType() != OperationType.CONSTRUCTOR) {
//                            MessageView messageView = MessageViewUtil.getMessageViewFor(aspect, to);
//                            
//                            if (messageView != null) {
//                                compoundCommand.append(AspectController.createRemoveMessageViewCommand(messageView));
//                            }
//                        
//                            /**
//                             * Ensure that the operation is not mapped by any other operation mappings.
//                             * Only if its only mapped by the current one (which will be deleted), 
//                             * also delete the operation.
//                             */
//                            List<EObject> referencingObjects =
//                                    EMFModelUtil.findCrossReferences(to, CorePackage.Literals.CORE_LINK__TO);
//                            referencingObjects.remove(operationMapping);
//    
//                            Collection<COREMapping<COREModelElement>> mappings =
//                                    EcoreUtil.getObjectsByType(command.getResult(),
//                                        CorePackage.Literals.CORE_MAPPING);
//                            referencingObjects.removeAll(mappings);
//                            
//                            if (referencingObjects.isEmpty()) {
//                                compoundCommand.append(RemoveCommand.create(editingDomain, to));
//                            }
//                            
//                            compoundCommand.append(RemoveCommand.create(editingDomain, operationMapping));
//                        }
//                    }
//                }
//            }
//        }
        
//        return (compoundCommand.isEmpty()) ? null : compoundCommand;
//    }

    /**
     * Created the selected features set with the feature given its name and all its ancestors until the root.
     * @param featureModel The feature model to look into
     * @param featureName The name of feature to look for
     * @return The set of selected features including the feature and all its ancestors
     */
    private HashSet<COREFeature> getSelectedFeatures(COREFeatureModel featureModel, String featureName) {
        HashSet<COREFeature> selectedFeatures = new HashSet<COREFeature>();
        COREFeature feature = getFeatureByName(featureModel.getFeatures(), featureName);
        
        if (feature != null) {
            selectedFeatures.add(feature);
            while (feature.getParent() != null) {
                feature = feature.getParent();
                selectedFeatures.add(feature);
            }
        }
        
        return selectedFeatures;
    }


    /**
     * Finds a {@link COREFeature} in a featureModel from the given name.
     * @param features The list of features to look into.
     * @param featureName The name of the feature to look for.
     * @return the core feature
     */
    private COREFeature getFeatureByName(EList<COREFeature> features, String featureName) {
        for (COREFeature feature : features) {
            if (featureName.equals(feature.getName())) {
                return feature;
            }
        }
        return null;
    }

    /**
     * Deletes the model reuse and reuse associated with an association end.
     *
     * @param c the {@link Concern} that contains the model that has the association
     * @param ae the {@link AssociationEnd} for which we want to delete the model reuse
     * @return a command that does the necessary
     */
    private CompoundCommand deleteModelReuseAndReuse(COREConcern c, AssociationEnd ae) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(ae);
        CompoundCommand compoundCommand = new CompoundCommand();
        EditingDomain concernEditingDomain = EMFEditUtil.getEditingDomain(c);

        // remove model reuses from artefact  
        COREReuse associationReuse = null;
        if (ae.getFeatureSelection() != null) {
            COREModelReuse modelReuse = ae.getFeatureSelection();
            associationReuse = modelReuse.getReuse();
            compoundCommand.append(RemoveCommand.create(concernEditingDomain, modelReuse));                
        }
        // remove the reference to the model reuse from the aspect
        compoundCommand.append(RemoveCommand.create(editingDomain, ae.getFeatureSelection()));
        
         // remove reuse from concern if no other model reuses are linked to it
        if (associationReuse != null && associationReuse.getModelReuses().size() == 1) {
            compoundCommand.append(RemoveCommand.create(concernEditingDomain, associationReuse));
        }
        return compoundCommand;
    }

    /**
     * Deletes the given {@link Association}.
     *
     * @param aspect the current {@link Aspect}
     * @param association the {@link Association} to delete
     */
    public void deleteAssociation(Aspect aspect, Association association) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(association);
        CompoundCommand compoundCommand = new CompoundCommand();

        compoundCommand.append(deleteAssociation(aspect, association, editingDomain));

        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Deletes the given {@link Association}.
     * Removes the model reuse and the reuse, if necessary
     *
     * @param aspect the current {@link Aspect}
     * @param association the {@link Association} to delete
     * @param editingDomain the {@link EditingDomain} to use for executing commands
     *
     * @return the created {@link CompoundCommand}
     */
    public CompoundCommand deleteAssociation(Aspect aspect, Association association, EditingDomain editingDomain) {
        CompoundCommand compoundCommand = new CompoundCommand();

        COREExternalArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(aspect);
        EditingDomain concernEditingDomain = EMFEditUtil.getEditingDomain(artefact);

        
        // remove model reuses from artefact        
        COREReuse associationReuse = null;
        for (AssociationEnd associationEnd : association.getEnds()) {
            if (associationEnd.getFeatureSelection() != null) {
                COREModelReuse modelReuse = associationEnd.getFeatureSelection();
                associationReuse = modelReuse.getReuse();
                
                // remove ghosts, if not referenced by anyone
                // keep track of which ones are being removed
                ArrayList<Operation> opsToBeDeleted = new ArrayList<Operation>();
                for (COREModelElementComposition<?> cm : modelReuse.getCompositions()) {
                    if (cm instanceof ClassifierMapping) {
                        ClassifierMapping cfm = (ClassifierMapping) cm;
                        if (cfm.getFrom().getName().equals(Constants.ASSOCIATION_SOURCE_CLASS_NAME)) {
                            for (OperationMapping om : cfm.getOperationMappings()) {
                                Operation o = om.getTo();
                                if (!EMFModelUtil.referencedInFeature(o, RamPackage.Literals.MESSAGE__SIGNATURE)) {
                                    // the operation is not called anywhere, so let's delete it
                                    compoundCommand.append(RemoveCommand.create(concernEditingDomain, o));
                                    opsToBeDeleted.add(o);
                                }
                            }
                        }
                    }
                }
                // loop again and find the collection class and remove it, if necessary
                Iterator<COREModelElementComposition<?>> i = modelReuse.getCompositions().iterator();
                while (i.hasNext()) {
                    COREModelElementComposition<?> cme = i.next();
                    if (cme instanceof ClassifierMapping) {
                        ClassifierMapping cfm = (ClassifierMapping) cme;
                        if (isCollectionClass(cfm.getFrom())) {
                            List<EObject> referencingOps = EMFModelUtil.findCrossReferences(cfm.getTo(),
                                    RamPackage.Literals.OPERATION__RETURN_TYPE);
                            referencingOps.addAll(EMFModelUtil.findCrossReferences(cfm.getTo(),
                                    RamPackage.Literals.PARAMETER__TYPE));                            
                            if (referencingOps.size() >= 0) {
                                int count = referencingOps.size();
                                // go through all referencing operations and check whether *all* are part of
                                // operations that are being deleted or not
                                // if yes, delete the classifier
                                for (EObject o : referencingOps) {
                                    boolean found = false;
                                    if (o instanceof Operation) {
                                        found = opsToBeDeleted.contains(o);
                                    } else if (o instanceof Parameter) {
                                        found = opsToBeDeleted.contains(o.eContainer());
                                    }
                                    if (found) {
                                        count--;
                                    }
                                }
                                if (count == 0) {
                                    // all references to the collection classifier are going to be deleted,
                                    // so we can delete the collection classifier as well
                                    CORECIElement ciEl = COREArtefactUtil.getCIElementFor(cfm.getTo());
                                    if (ciEl != null) {
                                        compoundCommand.append(deleteCORECIElementCommand(ciEl));
                                    }
                                    compoundCommand.append(createRemoveLayoutElementCommand(concernEditingDomain,
                                            aspect.getStructuralView(), cfm.getTo()));
                                    compoundCommand.append(RemoveCommand.create(concernEditingDomain, cfm.getTo()));
                                }
                            }
                        }
                    }
                }
                
                compoundCommand.append(RemoveCommand.create(concernEditingDomain, modelReuse)); 
                
                // remove reuse from concern if no other model reuses are linked to it
                if (associationReuse != null && associationReuse.getModelReuses().size() == 1) {
                    compoundCommand.append(RemoveCommand.create(concernEditingDomain, associationReuse));
                }

            }
        
        }


        // create remove command for association
        compoundCommand.append(RemoveCommand.create(editingDomain, association));

        // create remove commands for association ends
        for (AssociationEnd associationEnd : association.getEnds()) {
            if (associationEnd.getFeatureSelection() != null) {
                compoundCommand.append(deleteFeatureSelection(compoundCommand, aspect, associationEnd));
            }
            compoundCommand.append(RemoveCommand.create(editingDomain, associationEnd));
        }

        return compoundCommand;
    }

    /**
     * Sets the key mapping and updates all operations with the proper mapping.
     * @param keyMapping The mapping for which to set the key
     * @param value The value of the key
     * @param dataClassifier The data classifier to update its references to |Key
     */
    public void setKeySelection(ClassifierMapping keyMapping, Object value, Classifier dataClassifier) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(keyMapping);
        CompoundCommand compoundCommand = new CompoundCommand();
        Classifier keyClassifier = keyMapping.getFrom();
        Classifier newKeyType = (Classifier) value;
        COREModelComposition modelComposition = (COREModelComposition) keyMapping.eContainer();
        ClassifierMapping dataClassifierMapping = null;

        for (COREModelElementComposition<?> composition : modelComposition.getCompositions()) {
            COREMapping<?> mapping = (COREMapping<?>) composition;
                        
            if (dataClassifier.equals(mapping.getTo())) {
                dataClassifierMapping = (ClassifierMapping)  mapping;
            }
        }

        compoundCommand.append(SetCommand.create(editingDomain, keyMapping,
                CorePackage.Literals.CORE_LINK__TO, value));

        for (OperationMapping operationMapping : dataClassifierMapping.getOperationMappings()) {
            Operation operationFrom = operationMapping.getFrom();
            Operation operationTo = operationMapping.getTo();

            for (int i = 0; i < operationFrom.getParameters().size(); i++) {
                if (keyClassifier.equals(operationFrom.getParameters().get(i).getType())) {
                    compoundCommand.append(SetCommand.create(editingDomain, operationTo.getParameters().get(i),
                            RamPackage.Literals.PARAMETER__TYPE, newKeyType));
                }
            }

            if (keyClassifier.equals(operationFrom.getReturnType())) {
                compoundCommand.append(SetCommand.create(editingDomain, operationTo,
                      RamPackage.Literals.OPERATION__RETURN_TYPE, newKeyType));
            }
        }

        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Sets the multiplicity of the given {@link AssociationEnd} according to the given lower and upper bound.
     *
     * @param aspect the current aspect
     * @param associationEnd the {@link AssociationEnd} the multiplicity should be changed for
     * @param lowerBound the lower bound of the association end
     * @param upperBound the upper bound of the association end, -1 for many
     */
    public void setMultiplicity(Aspect aspect, AssociationEnd associationEnd, int lowerBound, int upperBound) {
        AssociationEnd oppositeEnd = associationEnd.getOppositeEnd();
        if (oppositeEnd.isNavigable()) {
            modifyAssociation(aspect, associationEnd, true, lowerBound,
                    upperBound, null, oppositeEnd, true, oppositeEnd.getFeatureSelection().getConfiguration());
        } else {
            modifyAssociation(aspect, associationEnd, true, lowerBound,
                    upperBound, null, oppositeEnd, false, null);           
        }
    }

    /**
     * Sets the role name of the given {@link AssociationEnd}.
     *
     * @param aspect the current aspect
     * @param associationEnd the {@link AssociationEnd} the role name should be changed for
     * @param roleName The new role name of the association end
     */
    public void setRoleName(Aspect aspect, AssociationEnd associationEnd, String roleName) {
        if (roleName.equals(associationEnd.getName())) {
            // do nothing if role name is unchanged
            return;
        }
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(associationEnd);
        CompoundCommand compoundCommand = new CompoundCommand();
        
        // Rename all operations from the association concern, if any
        if (associationEnd.getFeatureSelection() != null) {
            Classifier c = associationEnd.getClassifier();
            String oldEndName = associationEnd.getName();
            oldEndName = oldEndName.substring(0, 1).toUpperCase() + oldEndName.substring(1);
            
            for (Operation op : c.getOperations()) {
                if (op.getName().contains(oldEndName)) {
                    String opName = op.getName();
                    compoundCommand.append(SetCommand.create(editingDomain, op, RamPackage.Literals.NAMED_ELEMENT__NAME,
                            opName.replaceAll(oldEndName, roleName.substring(0, 1).toUpperCase()
                                    + roleName.substring(1))));
                }
            }
        }
        compoundCommand.append(SetCommand.create(editingDomain, associationEnd,
                RamPackage.Literals.NAMED_ELEMENT__NAME, roleName));
        doExecute(editingDomain, compoundCommand);
    }
    
    /**
     * Method that modifies an existing association. This method can handle all possible situations, i.e., change in
     * navigability, change in multiplicity, change in configuration.
     * 
     * @param aspect the aspect
     * @param associationEnd the association end that changes
     * @param endNavigable whether or not the association end that changes is navigable
     * @param newLowerBound the desired lower bound
     * @param newUpperBound the desired upper bound
     * @param newConfiguration the desired new configuration, can be null, in which case it selects the
     *        features automatically
     * @param oppositeEnd the opposite end
     * @param oppositeEndNavigable whether the opposite end is navigable
     * @param oppositeConfiguration the configuration of the opposite end
     */
    // CHECKSTYLE:IGNORE ParameterNumberCheck: We really need that many.
    public void modifyAssociation(Aspect aspect, AssociationEnd associationEnd, boolean endNavigable, int newLowerBound,
            int newUpperBound, COREConfiguration newConfiguration, AssociationEnd oppositeEnd,
            boolean oppositeEndNavigable, COREConfiguration oppositeConfiguration) {
        EditingDomain aspectEditingDomain = EMFEditUtil.getEditingDomain(associationEnd);
        CompoundCommand compoundCommand;

        if (!endNavigable && !oppositeEndNavigable) {
            // if the asked result gives a non-navigable Association, throw an error
            throw new IllegalArgumentException("The Association has to be Navigable.");
        } else {
            compoundCommand = new AssociationChangeCommand(aspect, associationEnd, endNavigable, newLowerBound,
                    newUpperBound, newConfiguration, oppositeEnd, oppositeEndNavigable,
                    oppositeConfiguration);
        }

        // set the new multiplicity
        compoundCommand.append(SetCommand.create(aspectEditingDomain, associationEnd,
                RamPackage.Literals.PROPERTY__LOWER_BOUND, newLowerBound));
        compoundCommand.append(SetCommand.create(aspectEditingDomain, associationEnd,
                RamPackage.Literals.PROPERTY__UPPER_BOUND, newUpperBound));


        compoundCommand.append(SetCommand.create(aspectEditingDomain, associationEnd,
                RamPackage.Literals.ASSOCIATION_END__NAVIGABLE, endNavigable));
        compoundCommand.append(SetCommand.create(aspectEditingDomain, oppositeEnd,
                RamPackage.Literals.ASSOCIATION_END__NAVIGABLE, oppositeEndNavigable));

        doExecute(aspectEditingDomain, compoundCommand);
    }

    /**
     * Sets the navigable property of the given association ends.
     * If both boolean are false, the function throws an error because a association cannot be non-navigable
     *
     * @param aspect the current {@link Aspect}
     * @param associationEnd the first {@link AssociationEnd}
     * @param endNavigable the navigable value of associationEnd
     * @param oppositeEnd the second {@link AssociationEnd}
     * @param oppositeEndNavigable the navigable value of oppositeEnd
     */
    public void setNavigable(Aspect aspect, AssociationEnd associationEnd, boolean endNavigable, 
            AssociationEnd oppositeEnd, boolean oppositeEndNavigable) {

        if (oppositeEnd.isNavigable()) {
            modifyAssociation(aspect, associationEnd, endNavigable, associationEnd.getLowerBound(),
                    associationEnd.getUpperBound(), associationEnd.getFeatureSelection().getConfiguration(),
                    oppositeEnd, oppositeEndNavigable, oppositeEnd.getFeatureSelection().getConfiguration());
        } else {
            modifyAssociation(aspect, associationEnd, endNavigable, associationEnd.getLowerBound(),
                    associationEnd.getUpperBound(), associationEnd.getFeatureSelection().getConfiguration(),
                    oppositeEnd, oppositeEndNavigable, null);           
        }
    }

    /**
     * Sets the reference type of the given association end to the new reference type.
     *
     * @param associationEnd the {@link AssociationEnd} the reference type should be changed of
     * @param referenceType the new {@link ReferenceType} to set
     */
    public void setReferenceType(AssociationEnd associationEnd, ReferenceType referenceType) {
        doSet(associationEnd, RamPackage.Literals.PROPERTY__REFERENCE_TYPE, referenceType);
    }

    /**
     * Switches the static property of the given association end.
     *
     * @param associationEnd the {@link AssociationEnd} the static property should be switched of
     */
    public void switchStatic(AssociationEnd associationEnd) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(associationEnd);
        CompoundCommand compoundCommand = new CompoundCommand();
        
        // Check if we should set the relevant methods to static 
        if (associationEnd.getFeatureSelection() != null) {
            List<COREModelElementComposition<?>> compositions =
                    associationEnd.getFeatureSelection().getCompositions();
            for (COREModelElementComposition<?> composition : compositions) {
                COREMapping<?> mapping = (COREMapping<?>) composition;

                if (associationEnd.getClassifier().equals(mapping.getTo())) {
                    ClassifierMapping classifierMapping = (ClassifierMapping) mapping;
                    for (OperationMapping operationMapping : classifierMapping.getOperationMappings()) {
                        Operation op = operationMapping.getTo();
                        compoundCommand.append(SetCommand.create(editingDomain, op, 
                                RamPackage.Literals.OPERATION__STATIC, !op.isStatic()));
                    }
                }
            }
        }
        compoundCommand.append(SetCommand.create(editingDomain, associationEnd, 
                RamPackage.Literals.STRUCTURAL_FEATURE__STATIC, !associationEnd.isStatic()));
        doExecute(editingDomain, compoundCommand);
    }

    /**
     * A specialized compound command for creating and deleting association feature selection.
     * @author celinebensoussan
     *
     */
    public class AssociationCompoundCommand extends CompoundCommand {

        /**
         * The current aspect.
         */
        protected Aspect aspect;

        /**
         * The association end.
         */
        protected AssociationEnd associationEnd;

        /**
         * The opposite end.
         */
        protected AssociationEnd oppositeEnd;

        /**
         * The selected features.
         * It will contain values if a use has made a selection, otherwise it will always be set empty.
         */
        protected COREConfiguration reuseConfiguration;

        /**
         * Constructor without selected features.
         * @param aspect The current aspect
         * @param associationEnd The association end
         * @param oppositeEnd The opposite end
         */
        public AssociationCompoundCommand(Aspect aspect, AssociationEnd associationEnd,
                AssociationEnd oppositeEnd) {
            this.aspect = aspect;
            this.associationEnd = associationEnd;
            this.oppositeEnd = oppositeEnd;
            //TODO: figure out what to do here
         // Get the configuration with the user selection which is different from the selected configuration
//            this.reuseConfiguration = COREConfigurationUtil.getConfigurationByName(
//                    associationEnd.getFeatureSelection().getReuse(), Constants.USER_SELECTED_CONFIGURATION_NAME);

            if (associationEnd.getFeatureSelection() == null || associationEnd.getUpperBound() <= 1) {
                this.reuseConfiguration = null;
            }
        }

        /**
         * Constructor with selected features.
         * @param aspect The current aspect
         * @param associationEnd The association end
         * @param oppositeEnd The opposite end
         * @param reuseConfiguration The selected features
         */
        public AssociationCompoundCommand(Aspect aspect, AssociationEnd associationEnd,
                AssociationEnd oppositeEnd, COREConfiguration reuseConfiguration) {
            this.aspect = aspect;
            this.associationEnd = associationEnd;
            this.oppositeEnd = oppositeEnd;
            this.reuseConfiguration = reuseConfiguration;
        }

    }

    /**
     * A compound command that extends the association compound command.
     * It is used to set the feature selection of a unidirectional association.
     * @author celinebensoussan
     *
     */
    public class UnidirectionalCompoundCommand extends AssociationCompoundCommand {

        /**
         * Constructor without selected features.
         * @param aspect The current aspect
         * @param associationEnd The association end
         * @param oppositeEnd The opposite end
         */
        public UnidirectionalCompoundCommand(Aspect aspect, AssociationEnd associationEnd,
                AssociationEnd oppositeEnd) {
            super(aspect, associationEnd, oppositeEnd);
        }

        /**
         * Constructor without selected features.
         * @param aspect The current aspect
         * @param associationEnd The association end
         * @param oppositeEnd The opposite end
         * @param reuseConfiguration The configuration manually selected by the user
         */
        public UnidirectionalCompoundCommand(Aspect aspect, AssociationEnd associationEnd,
                AssociationEnd oppositeEnd, COREConfiguration reuseConfiguration) {
            super(aspect, associationEnd, oppositeEnd, reuseConfiguration);
        }

        @Override
        public void execute() {
            super.execute();
            
            //TODO: Figure out what to do
            System.out.println("Figure out how to handle unidirectional commands");
            
            COREConcern concern = COREArtefactUtil.getReferencingExternalArtefact(aspect).getCoreConcern();

            appendAndExecute(createFeatureSelection(aspect, concern, associationEnd,
                    oppositeEnd, reuseConfiguration, null));
        }
    }

    /**
     * A compound command that extends the association compound command.
     * It is used to change the feature selection of a unidirectional association.
     * @author joerg
     *
     */
    public class UnidirectionalConfigurationChangeCompoundCommand extends AssociationCompoundCommand {

        /**
         * Constructor without selected features.
         * @param aspect The current aspect
         * @param associationEnd The association end
         * @param oppositeEnd The opposite end
         */
        public UnidirectionalConfigurationChangeCompoundCommand(Aspect aspect, AssociationEnd associationEnd,
                AssociationEnd oppositeEnd) {
            super(aspect, associationEnd, oppositeEnd);
        }

        /**
         * Constructor without selected features.
         * @param aspect The current aspect
         * @param associationEnd The association end
         * @param oppositeEnd The opposite end
         * @param reuseConfiguration The configuration manually selected by the user
         */
        public UnidirectionalConfigurationChangeCompoundCommand(Aspect aspect, AssociationEnd associationEnd,
                AssociationEnd oppositeEnd, COREConfiguration reuseConfiguration) {
            super(aspect, associationEnd, oppositeEnd, reuseConfiguration);
        }

        @Override
        public void execute() {
            super.execute();
            
            COREConcern concern = COREArtefactUtil.getReferencingExternalArtefact(aspect).getCoreConcern();
            if (oppositeEnd.isNavigable()) {
                append(deleteModelReuseAndReuse(concern, oppositeEnd));
            }
            appendAndExecute(changeFeatureSelection(aspect, concern, associationEnd, oppositeEnd,
                    reuseConfiguration, null));
        }
    }

   /**
     * A compound command that extends the association compound command.
     * It is used to set the feature selection of a bidirectional association.
     * @author celinebensoussan
     *
     */
    public class BidirectionalCompoundCommand extends AssociationCompoundCommand {

//        private COREConfiguration oppositeSelectedConfiguration;

        /**
         * Constructor without selected features.
         * @param aspect The current aspect
         * @param associationEnd The association end
         * @param oppositeEnd The opposite end
         */
        public BidirectionalCompoundCommand(Aspect aspect, AssociationEnd associationEnd,
                AssociationEnd oppositeEnd) {
            super(aspect, associationEnd, oppositeEnd);
        }

        /**
         * Constructor with selected features.
         * @param aspect The current aspect
         * @param associationEnd The association end
         * @param oppositeEnd The opposite end
         * @param reuseConfiguration The configuration manually selected by the user
         */
        public BidirectionalCompoundCommand(Aspect aspect, AssociationEnd associationEnd,
                AssociationEnd oppositeEnd, COREConfiguration reuseConfiguration) {
            super(aspect, associationEnd, oppositeEnd, reuseConfiguration);
        }

        @Override
        public void execute() {
            super.execute();
            COREConcern concern = COREArtefactUtil.getReferencingExternalArtefact(aspect).getCoreConcern();

            COREConfiguration oppositeConfiguration = null;
            if (oppositeEnd.getFeatureSelection() != null) {
                oppositeConfiguration = oppositeEnd.getFeatureSelection().getConfiguration();
                removeOppositeSelection(oppositeConfiguration);
                appendAndExecute(changeFeatureSelection(aspect, concern, oppositeEnd, associationEnd,
                        oppositeConfiguration, reuseConfiguration));
            } else {
                appendAndExecute(createFeatureSelection(aspect, concern, oppositeEnd, associationEnd,
                        oppositeConfiguration, reuseConfiguration));
            }
            appendAndExecute(changeFeatureSelection(aspect, concern, associationEnd, oppositeEnd,
                    reuseConfiguration, oppositeConfiguration));

        }
    }
    
    /**
     * A compound command that extends the association compound command.
     * It is used to set the feature selection of a bidirectional association.
     * @author celinebensoussan
     *
     */
    public class BidirectionalConfigurationChangeCommand extends AssociationCompoundCommand {

//        private COREConfiguration oppositeSelectedConfiguration;

        /**
         * Constructor without selected features.
         * @param aspect The current aspect
         * @param associationEnd The association end
         * @param oppositeEnd The opposite end
         */
        public BidirectionalConfigurationChangeCommand(Aspect aspect, AssociationEnd associationEnd,
                AssociationEnd oppositeEnd) {
            super(aspect, associationEnd, oppositeEnd);
        }

        /**
         * Constructor with selected features.
         * @param aspect The current aspect
         * @param associationEnd The association end
         * @param oppositeEnd The opposite end
         * @param reuseConfiguration The configuration manually selected by the user
         */
        public BidirectionalConfigurationChangeCommand(Aspect aspect, AssociationEnd associationEnd,
                AssociationEnd oppositeEnd, COREConfiguration reuseConfiguration) {
            super(aspect, associationEnd, oppositeEnd, reuseConfiguration);
        }

        @Override
        public void execute() {
            super.execute();
            COREConcern concern = COREArtefactUtil.getReferencingExternalArtefact(aspect).getCoreConcern();

            COREConfiguration oppositeConfiguration = null;
            if (oppositeEnd.getFeatureSelection() != null) {
                oppositeConfiguration = oppositeEnd.getFeatureSelection().getConfiguration();
                removeOppositeSelection(oppositeConfiguration);
                appendAndExecute(changeFeatureSelection(aspect, concern, associationEnd, oppositeEnd,
                        reuseConfiguration, oppositeConfiguration));
            } else {
                appendAndExecute(changeFeatureSelection(aspect, concern, oppositeEnd, associationEnd,
                        oppositeConfiguration, associationEnd.getFeatureSelection().getConfiguration()));
            }
            appendAndExecute(changeFeatureSelection(aspect, concern, oppositeEnd, associationEnd,
                    oppositeConfiguration, associationEnd.getFeatureSelection().getConfiguration()));

        }
    }

    /**
     * A compound command that can be used to make any changes to an association.
     * @author joerg
     */
    public class AssociationChangeCommand extends AssociationCompoundCommand {

        /**
         * New multiplicity lower bound.
         */
        protected int newLower;
        
        /**
         * New multiplicity upper bound.
         */
        protected int newUpper;
        
        private boolean navigable;
        private boolean oppositeNavigable;
        private COREConfiguration oppositeSelectedConfiguration;

        /**
         * Constructor with selected features.
         * @param aspect
         * @param associationEnd
         * @param endNavigable
         * @param newLowerBound
         * @param newUpperBound
         * @param newConfiguration
         * @param oppositeEnd
         * @param oppositeEndNavigable
         * @param oppositeConfiguration
         */
        // CHECKSTYLE:IGNORE ParameterNumberCheck: We really need that many.
        public AssociationChangeCommand(Aspect aspect, AssociationEnd associationEnd, boolean endNavigable,
                int newLowerBound, int newUpperBound, COREConfiguration newConfiguration, AssociationEnd oppositeEnd,
                boolean oppositeEndNavigable, COREConfiguration oppositeConfiguration) {
            super(aspect, associationEnd, oppositeEnd, newConfiguration);
            newLower = newLowerBound;
            newUpper = newUpperBound;
            navigable = endNavigable;
            oppositeNavigable = oppositeEndNavigable;
            oppositeSelectedConfiguration = oppositeConfiguration;
        }

        // CHECKSTYLE:IGNORE MethodLength: Okay here.
        @Override
        public void execute() {
            super.execute();
            COREExternalArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(aspect);
            COREConcern concern = artefact.getCoreConcern();
            EditingDomain artefactEditingDomain = EMFEditUtil.getEditingDomain(artefact);
            EditingDomain concernEditingDomain = EMFEditUtil.getEditingDomain(concern);
            // Auxiliary variables to keep track of created or deleted reuses
            COREReuse newReuse = null;
            COREReuse deletedReuse = null;

            // find the association concern
            COREConcern associationConcern = null;
            try {
                associationConcern = COREModelUtil.getLocalConcern(Constants.ASSOCIATION_CONCERN_LOCATION,
                        concern.eResource().getURI());
            } catch (IOException e) {
                return;
            }

            // process navigability of association end
            if (navigable && reuseConfiguration != null && oppositeSelectedConfiguration != null) {
                // if it is navigable, then get rid of the "opposite" selected features
                removeOppositeSelection(reuseConfiguration);
            } else if (!navigable && associationEnd.getFeatureSelection() != null) {
                append(deleteModelReuseAndReuse(concern, associationEnd)); 
            }
            
            // process navigability of opposite association end
            boolean oppositeAssociationCreated = false; 
            if (oppositeNavigable && oppositeSelectedConfiguration != null) {
                removeOppositeSelection(oppositeSelectedConfiguration);
            } else if (!oppositeNavigable && oppositeEnd.getFeatureSelection() != null) {                
                COREModelReuse modelReuse = oppositeEnd.getFeatureSelection();
                deletedReuse = modelReuse.getReuse();
                appendAndExecute(RemoveCommand.create(concernEditingDomain, deletedReuse,
                        CorePackage.Literals.CORE_CONCERN__REUSES, modelReuse));
                // remove the reference to the model reuse from the aspect
                appendAndExecute(SetCommand.create(artefactEditingDomain, oppositeEnd,
                        RamPackage.Literals.ASSOCIATION_END__FEATURE_SELECTION, null));
                // remove the reference to the model reuse from the artefact
                appendAndExecute(RemoveCommand.create(concernEditingDomain, artefact,
                        CorePackage.Literals.CORE_ARTEFACT__MODEL_REUSES, modelReuse));
            } else if (oppositeNavigable && oppositeEnd.getFeatureSelection() == null) {
                // the opposite end has no model reuse and reuse yet -> create it              
                HashSet<COREFeature> automaticSelectedFeatures = new HashSet<COREFeature>();
                determineAssociationFeatures(oppositeEnd, associationEnd, associationConcern,
                        automaticSelectedFeatures, reuseConfiguration);
                COREConfiguration oppositeConfiguration = CoreFactory.eINSTANCE.createCOREConfiguration();
                oppositeConfiguration.setName(AUTOMATIC_SELECTION);
                oppositeConfiguration.getSelected().addAll(automaticSelectedFeatures);
                oppositeConfiguration.setSource(concern.getFeatureModel());

                // create a reuse
                newReuse = COREModelUtil.createReuse(artefact.getCoreConcern(), associationConcern);
                newReuse.setName(getReuseName(aspect, associationConcern.getName(),
                        oppositeEnd.getClassifier().getName(), associationEnd.getClassifier().getName()));

                COREModelReuse modelReuse = COREModelUtil.createModelReuse(null, newReuse);
                modelReuse.setConfiguration(oppositeConfiguration);
                
                // Add the model reuse to the artefact that refers to the aspect with the association
                appendAndExecute(AddCommand.create(concernEditingDomain, artefact,
                        CorePackage.Literals.CORE_ARTEFACT__MODEL_REUSES, modelReuse));
        
                // add the model reuse also to the corresponding association end 
                appendAndExecute(SetCommand.create(concernEditingDomain, oppositeEnd,
                        RamPackage.Literals.ASSOCIATION_END__FEATURE_SELECTION, modelReuse));
                oppositeAssociationCreated = true;
                
                // now lets weave the association concern
                COREExternalArtefact newArtefact = COREWeaver.getInstance().weaveReuse(artefact,
                        associationConcern, oppositeConfiguration);
                modelReuse.setSource(newArtefact);

                // create the mappings for source and target classes
                appendAndExecute(createClassifierMappings(concernEditingDomain, newArtefact, modelReuse, oppositeEnd,
                        associationEnd));                
            }
            
            COREConfiguration  automaticConfiguration = null;
            if (navigable) {
                // determine which features need to be selected
                // use the chosen ones and add any automatic ones (for instance, those that take care of the opposite)
                HashSet<COREFeature> automaticSelectedFeatures = new HashSet<COREFeature>();
                if (reuseConfiguration != null) {
                    automaticSelectedFeatures.addAll(reuseConfiguration.getSelected());
                }
                determineAssociationFeatures(associationEnd, oppositeEnd, associationConcern,
                        automaticSelectedFeatures, oppositeSelectedConfiguration);
                
                COREModelReuse modelReuse = associationEnd.getFeatureSelection();
            
                // Create a new configuration for the selected features and add it
                automaticConfiguration = CoreFactory.eINSTANCE.createCOREConfiguration();
                automaticConfiguration.setName(AUTOMATIC_SELECTION);
                automaticConfiguration.getSelected().addAll(automaticSelectedFeatures);
                automaticConfiguration.setSource(concern.getFeatureModel());
                    
                appendAndExecute(SetCommand.create(concernEditingDomain, modelReuse,
                            CorePackage.Literals.CORE_MODEL_REUSE__CONFIGURATION, automaticConfiguration));

                // now lets weave the association concern
                COREExternalArtefact newArtefact = COREWeaver.getInstance().weaveReuse(artefact,
                        associationConcern, automaticConfiguration);

                updateReuse(concernEditingDomain, newArtefact, modelReuse, associationEnd,
                        oppositeEnd);                
            }
            
            if (oppositeNavigable && !oppositeAssociationCreated) {
                // determine which features need to be selected
                // use the chosen ones and add any automatic ones (for instance, those that take care of the opposite)
                HashSet<COREFeature> automaticSelectedFeatures = new HashSet<COREFeature>();
                if (oppositeSelectedConfiguration != null) {
                    automaticSelectedFeatures.addAll(oppositeSelectedConfiguration.getSelected());
                }
                determineAssociationFeatures(oppositeEnd, associationEnd, associationConcern,
                        automaticSelectedFeatures, automaticConfiguration);
                
                COREModelReuse modelReuse = oppositeEnd.getFeatureSelection();
                
                // Create a new configuration for the selected features and add it
                COREConfiguration automaticOppositeConfiguration = CoreFactory.eINSTANCE.createCOREConfiguration();
                automaticOppositeConfiguration.setName(AUTOMATIC_SELECTION);
                automaticOppositeConfiguration.getSelected().addAll(automaticSelectedFeatures);
                automaticOppositeConfiguration.setSource(concern.getFeatureModel());
                    
                appendAndExecute(SetCommand.create(concernEditingDomain, modelReuse,
                            CorePackage.Literals.CORE_MODEL_REUSE__CONFIGURATION, automaticOppositeConfiguration));
                
                // now lets weave the association concern
                COREExternalArtefact newArtefact = COREWeaver.getInstance().weaveReuse(artefact,
                        associationConcern, automaticOppositeConfiguration);

                // delete old classifier mappings
                appendAndExecute(RemoveCommand.create(concernEditingDomain, modelReuse,
                        CorePackage.Literals.CORE_MODEL_COMPOSITION__COMPOSITIONS, modelReuse.getCompositions()));

                modelReuse.setSource(newArtefact);

                // create the mappings for source and target classes
                createClassifierMappings(concernEditingDomain, newArtefact, modelReuse, oppositeEnd,
                        associationEnd);                    
            }
            if (newReuse != null) {
                // If we created a new reuse during this operation, add it to the concern.
                // We need to do this last, because it triggers notifications in the DisplayConcernEditScene
                if (newReuse.eContainer() == null) {
                    appendAndExecute(AddCommand.create(concernEditingDomain, concern,
                            CorePackage.Literals.CORE_CONCERN__REUSES, newReuse));
                }
                        
            }
            if (deletedReuse != null && deletedReuse.getModelReuses().size() == 1) {
                appendAndExecute(RemoveCommand.create(concernEditingDomain, deletedReuse));
            }
            
            
    //TODO: create ghosts for all the operations
    //createReuseInstantiation(aspect, wovenAspect, associationEnd,
    //        modelReuse, compoundCommand);

        }
        
        /**
         * Auxiliary method for creating classifier mappings for Source and Target classes.
         * Also updates the operation mappings, if needed.
         * 
         * @param concernEditingDomain the domain
         * @param newArtefact the newly woven association artefact
         * @param modelReuse the model reuse
         * @param associationEnd the association end designating the target
         * @param oppositeEnd the association end designating the source
         * @return a compound command that deals with the classifier mappings
         */
        private CompoundCommand createClassifierMappings(EditingDomain concernEditingDomain,
                COREExternalArtefact newArtefact, COREModelReuse modelReuse,
                AssociationEnd associationEnd, AssociationEnd oppositeEnd) {

            CompoundCommand result = new CompoundCommand();
            
            // now lets create the mappings for the Source and Destination classes
            ClassifierMapping cmSource = RamFactory.eINSTANCE.createClassifierMapping();
            
            // now lets create the source and target mappings
            Aspect wovenAspect = (Aspect) newArtefact.getRootModelElement();
            Classifier source = null;
            for (Classifier c : wovenAspect.getStructuralView().getClasses()) {
                if (c.getName().equals(Constants.ASSOCIATION_SOURCE_CLASS_NAME)) {
                    source = c;
                    break;
                }
            }
            cmSource.setFrom(source);
            cmSource.setTo(associationEnd.getClassifier());
            result.append(AddCommand.create(concernEditingDomain, modelReuse,
                    CorePackage.Literals.CORE_MODEL_COMPOSITION__COMPOSITIONS, cmSource));
            
            ClassifierMapping cmTarget = RamFactory.eINSTANCE.createClassifierMapping();
            Classifier target = null;
            for (Classifier c : wovenAspect.getStructuralView().getClasses()) {
                if (c.getName().equals(Constants.ASSOCIATION_TARGET_CLASS_NAME)) {
                    target = c;
                    break;
                }
            }
            cmTarget.setFrom(target);
            cmTarget.setTo(oppositeEnd.getClassifier());
            result.append(AddCommand.create(concernEditingDomain, modelReuse,
                    CorePackage.Literals.CORE_MODEL_COMPOSITION__COMPOSITIONS, cmTarget));
            
            // create ghosts for all the operations of the source class 
            // and rename them to use the 
            // name of the association end that points to the target class 
            // For example, if an association is created from A to B, and the association
            // end is named myB, then getMyTarget should be renamed getMyB
            COREArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(aspect);
            for (Operation op : source.getOperations()) {
                Operation newOp = (Operation) RAMReferenceUtil.createLocalElementWithComposition(
                        concernEditingDomain, result, artefact, associationEnd.getClassifier(),
                        op, modelReuse);
                
                // now we must update all references contained in the operation, which still point
                // to the woven aspect, to point to the current one
                RAMReferenceUtil.updateLocalElementPropertiesWithComposition(concernEditingDomain, result,
                        aspect, newOp, modelReuse);
                String opName = newOp.getName();
                String associationEndName = associationEnd.getName();
                result.append(SetCommand.create(concernEditingDomain, newOp, RamPackage.Literals.NAMED_ELEMENT__NAME,
                        opName.replaceAll("Target", associationEndName.substring(0, 1).toUpperCase()
                                + associationEndName.substring(1))));
            }
            return result;
        }
        
        /**
         * Auxiliary method for updating an association reuse.
         * 
         * @param concernEditingDomain the domain
         * @param newArtefact the newly woven association artefact
         * @param modelReuse the model reuse
         * @param associationEnd the association end designating the target
         * @param oppositeEnd the association end designating the source
         */
        // CHECKSTYLE:IGNORE MethodLength: Okay here.
        private void updateReuse(EditingDomain concernEditingDomain,
                COREExternalArtefact newArtefact, COREModelReuse modelReuse,
                AssociationEnd associationEnd, AssociationEnd oppositeEnd) {
    
            Aspect wovenAspect = (Aspect) newArtefact.getRootModelElement();
            Aspect currentAspect = (Aspect) EcoreUtil.getRootContainer(associationEnd);
                
            // Go through the old mappings
            ArrayList<COREModelElementComposition<?>> comps =
                    new ArrayList<COREModelElementComposition<?>>(modelReuse.getCompositions());
            for (COREModelElementComposition<?> comp : comps) {
                if (comp instanceof ClassifierMapping) {
                    ClassifierMapping cm = (ClassifierMapping) comp;
                    Classifier originalFrom = cm.getFrom();

                    // lets find the "source" mapping, and update it
                    if (originalFrom.getName().contentEquals(Constants.ASSOCIATION_SOURCE_CLASS_NAME)) {
                        
                        Classifier newFrom = null;
                        
                        for (Classifier c : wovenAspect.getStructuralView().getClasses()) {
                            if (c.getName().equals(Constants.ASSOCIATION_SOURCE_CLASS_NAME)) {
                                newFrom = c;
                                break;
                            }
                        }
                        appendAndExecute(SetCommand.create(concernEditingDomain, cm,
                                CorePackage.Literals.CORE_LINK__FROM, newFrom));
    
                        // now go through all operations from the old source, and
                        // check whether they also exist in the new source
                        ArrayList<Operation> mappedOps = new ArrayList<Operation>();
                        ArrayList<OperationMapping> oldOpMappingsToDelete = new ArrayList<OperationMapping>();
                        for (OperationMapping om : cm.getOperationMappings()) {
                            Operation oldOpFrom = om.getFrom();
                            Operation newOp = null;
                            for (Operation o : newFrom.getOperations()) {
                                if (RAMModelUtil.hasSameSignature(o, oldOpFrom)) {
                                    newOp = o;
                                    break;
                                }
                            }
                            if (newOp != null) {
                                // the operation exists! so we update the mapping
                                appendAndExecute(SetCommand.create(concernEditingDomain, om,
                                        CorePackage.Literals.CORE_LINK__FROM, newOp));
                                // let's remember that we used this operation so we don't add it again later
                                mappedOps.add(newOp);
                            } else {
                                // the old operation does not exist anymore so we need to
                                // delete it
                                appendAndExecute(DeleteCommand.create(concernEditingDomain, om.getTo()));
                                oldOpMappingsToDelete.add(om);
                            }
                        }
                        // Now we have to delete the old mappings
                        for (OperationMapping omToDelete : oldOpMappingsToDelete) {
                            appendAndExecute(DeleteCommand.create(concernEditingDomain, omToDelete));                 
                        }
                        
                        // Now to finish we have to add operations and mappings for all operations that
                        // have not been used when updating the mappings
                        for (Operation o : newFrom.getOperations()) {
                            if (!mappedOps.contains(o)) {
                                // Deep copy the operation (including the parameters)
                                Operation newO = EcoreUtil.copy(o);
                                // Rename it if needed
                                String opName = newO.getName();
                                Classifier target = null;
                                for (COREModelElementComposition<?> cme : modelReuse.getCompositions()) {
                                    if (cme instanceof ClassifierMapping) {
                                        ClassifierMapping cfm = (ClassifierMapping) cme;
                                        if (cfm.getFrom().getName().equals(Constants.ASSOCIATION_TARGET_CLASS_NAME)) {
                                            target = cfm.getTo();
                                            break;
                                        }
                                    }
                                }
                                
                                // Make sure the references are updated to point to the current model
                                Type returnType = newO.getReturnType();
                                if (RamPackage.Literals.PRIMITIVE_TYPE.isInstance(returnType)) {
                                    newO.setReturnType(RAMModelUtil.getPrimitiveTypeByName(
                                            currentAspect.getStructuralView(), returnType.getName()));
                                } else if (RamPackage.Literals.RVOID.isInstance(returnType)) {
                                    newO.setReturnType(RAMModelUtil.getVoidType(currentAspect.getStructuralView()));
                                } else if (returnType.getName()
                                        .contentEquals(Constants.ASSOCIATION_TARGET_CLASS_NAME)) {
                                    newO.setReturnType(target);
                                } else if (returnType.getName()
                                        .contentEquals(Constants.ASSOCIATION_SOURCE_CLASS_NAME)) {
                                    newO.setReturnType(cm.getTo());
                                } else {
                                    // it must be a collection or an implementation class, so we need to find
                                    // it in the current model if it exists
                                    Classifier c = findCollectionClass(modelReuse);
                                    
                                    if (c != null) {
                                        newO.setReturnType(c);
                                    } else {
                                        Classifier newC = localizeClassifier(concernEditingDomain,
                                                modelReuse, currentAspect, (Classifier) returnType);
                                        newO.setReturnType(newC);
                                    }
                                }
                                for (Parameter p : newO.getParameters()) {
                                    if (RamPackage.Literals.PRIMITIVE_TYPE.isInstance(p.getType())) {
                                        p.setType(RAMModelUtil.getPrimitiveTypeByName(
                                            currentAspect.getStructuralView(), p.getType().getName()));
                                    } else if (RamPackage.Literals.RVOID.isInstance(p.getType())) {
                                        p.setType(RAMModelUtil.getVoidType(currentAspect.getStructuralView()));
                                    } else if (p.getType().getName()
                                            .contentEquals(Constants.ASSOCIATION_TARGET_CLASS_NAME)) {
                                        p.setType(target);
                                    } else {
                                        // it could be a key or a collection
                                        if (p.getType().getName().contentEquals("Key")) {
                                            Classifier newC = localizeClassifier(concernEditingDomain,
                                                    modelReuse, currentAspect, (Classifier) p.getType());
                                            p.setType(newC);
                                        } else {
                                            // it must be a collection or an implementation class, so we need to find
                                            // it in the current model if it exists
                                            Classifier c = findCollectionClass(modelReuse);
                                            
                                            if (c != null) {
                                                p.setType(c);
                                            } else {
                                                Classifier newC = localizeClassifier(concernEditingDomain,
                                                        modelReuse, currentAspect, (Classifier) p.getType());
                                                p.setType(newC);
                                            }
                                        }
                                    }
                                    
                                }
                                String associationEndName = associationEnd.getName();
                                newO.setName(opName.replaceAll("Target",
                                        associationEndName.substring(0, 1).toUpperCase()
                                        + associationEndName.substring(1)));
                                
                                appendAndExecute(AddCommand.create(concernEditingDomain, cm.getTo(),
                                        RamPackage.Literals.CLASSIFIER__OPERATIONS, newO));
                                
                                OperationMapping m = RamFactory.eINSTANCE.createOperationMapping();
                                m.setFrom(o);
                                m.setTo(newO);
                                appendAndExecute(AddCommand.create(concernEditingDomain, cm,
                                        CorePackage.Literals.CORE_MAPPING__MAPPINGS, m));

                            }
                        }
                        
                    } else if (originalFrom.getName().contentEquals(Constants.ASSOCIATION_TARGET_CLASS_NAME)) {
                        Classifier newFrom = null;
                        
                        for (Classifier c : wovenAspect.getStructuralView().getClasses()) {
                            if (c.getName().equals(Constants.ASSOCIATION_TARGET_CLASS_NAME)) {
                                newFrom = c;
                                break;
                            }
                        }
                        appendAndExecute(SetCommand.create(concernEditingDomain, cm,
                                CorePackage.Literals.CORE_LINK__FROM, newFrom));
                        
                    } else { 
                        Classifier newFrom = null;

                        if (isCollectionClass(originalFrom)) {
                            
                            // check whether the new woven aspect has a collection
                            for (Classifier c : wovenAspect.getStructuralView().getClasses()) {
                                if (isCollectionClass(c)) {
                                    newFrom = c;
                                    break;
                                }
                            }
                            // If we found a collection, then update the mapping and update the name of the ghost
                            if (newFrom != null) {
                                appendAndExecute(SetCommand.create(concernEditingDomain, cm,
                                        CorePackage.Literals.CORE_LINK__FROM, newFrom));
                                appendAndExecute(SetCommand.create(concernEditingDomain, cm.getTo(),
                                        RamPackage.Literals.NAMED_ELEMENT__NAME, newFrom.getName()));
                            } else {
                                // There is no more collection class in the new association woven aspect
                                // (because the user used a 0..1 association), so we need to delete the
                                // collection ghost if it is not used by anything anymore
                                System.out.println("jojojo");
                            }
                            
                        }
                    }
                }
            }
            // go through the old mappings one more time and eliminate those that refer to "unused" collections
            ArrayList<ClassifierMapping> toDelete = new ArrayList<ClassifierMapping>();
            for (COREModelElementComposition<?> comp : modelReuse.getCompositions()) {
                if (comp instanceof ClassifierMapping) {
                    ClassifierMapping cm = (ClassifierMapping) comp;
                    Classifier originalFrom = cm.getFrom();
                    
                    if (originalFrom.eContainer().eContainer() != wovenAspect) {
                        toDelete.add((ClassifierMapping) comp);
                    }
                }
            }
            for (ClassifierMapping cm : toDelete) {
                appendAndExecute(RemoveCommand.create(concernEditingDomain, cm.getFrom()));
                appendAndExecute(RemoveCommand.create(concernEditingDomain, cm));
            }
            
            appendAndExecute(SetCommand.create(concernEditingDomain, modelReuse,
                    CorePackage.Literals.CORE_MODEL_COMPOSITION__SOURCE, newArtefact));            
        } 
        
        /**
         * Operation that localizes the classifier c (creating also a mapping).
         * 
         * @param e the editing domain
         * @param modelReuse the model reuse of the association concern
         * @param currentAspect the current aspect model
         * @param c the classifier to localize
         * @return the new local copy (ghost)
         */
        private Classifier localizeClassifier(EditingDomain e, COREModelReuse modelReuse, 
                Aspect currentAspect, Classifier c) {
            Classifier newC = (Classifier) EcoreUtil.copy(c);
            
            newC.getOperations().clear();
            newC.getAssociationEnds().clear();
            newC.getSuperTypes().clear();
                
            if (RamPackage.Literals.CLASS.isInstance(newC)) {
                Class clazz = (Class) newC;                                      
                clazz.getAttributes().clear();
            }
            
            appendAndExecute(AddCommand.create(e,
                    currentAspect.getStructuralView(), 
                    RamPackage.Literals.STRUCTURAL_VIEW__CLASSES, newC));
            
            LayoutElement layoutElement = RamFactory.eINSTANCE.createLayoutElement();
            
//            ContainerMapImpl containerMap = EMFModelUtil.getEntryFromMap(
//                    currentAspect.getLayout().getContainers(),
//                    currentAspect.getStructuralView());
            
            
            Command layoutCommand = createAddLayoutElementCommand(e,
                    currentAspect.getStructuralView(), newC,
                    layoutElement);

            appendAndExecute(layoutCommand);
                        
            // and finally create a mapping
            @SuppressWarnings("unchecked")
            COREMapping<Classifier> mapping =
                    (COREMapping<Classifier>) RamFactory.eINSTANCE.create(
                    RamPackage.Literals.CLASSIFIER_MAPPING);
            
            mapping.setFrom((Classifier) c);
            mapping.setTo(newC);
            
            appendAndExecute(AddCommand.create(e, modelReuse,
                    CorePackage.Literals.CORE_MODEL_COMPOSITION__COMPOSITIONS,
                    mapping));    
            return newC;

        }
        
    }
    
    /**
     * Operation that determines whether the classifier c plays the role of the collection in the
     * association concern.
     * 
     * @param c the classifier
     * @return whether c plays the role of a collection
     */
    private boolean isCollectionClass(Classifier c) {
        String s = c.getName();
        return s.contentEquals("Collection")
                || s.contentEquals("List")
                || s.contentEquals("Map")
                || s.contentEquals("Set")
                || s.contentEquals("ArrayList")
                || s.contentEquals("LinkedList")
                || s.contentEquals("Stack")
                || s.contentEquals("HashSet") 
                || s.contentEquals("TreeSet")
                || s.contentEquals("HashMap")
                || s.contentEquals("TreeMap");
    }

    /**
     * Operation that returns the classifier in the aspect that made the model reuse of the association
     * concern that plays the role of the collection.
     * 
     * @param modelReuse the model reuse of the association
     * @return the classifier
     */
    private Classifier findCollectionClass(COREModelReuse modelReuse) {
        Classifier result = null;
        for (COREModelElementComposition<?> mec : modelReuse.getCompositions()) {
            if (mec instanceof ClassifierMapping) {
                ClassifierMapping cm = (ClassifierMapping) mec;
                if (isCollectionClass(cm.getFrom())) {
                    return cm.getTo();
                }
            }
        }
        
        return result;
    }

}