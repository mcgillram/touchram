package ca.mcgill.sel.ram.expressions.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.testing.InjectWith;
import org.eclipse.xtext.testing.XtextRunner;
import org.eclipse.xtext.testing.util.ParseHelper;
import org.eclipse.xtext.testing.validation.ValidationTestHelper;
import org.eclipse.xtext.xbase.lib.Extension;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.xtext.example.mydsl.tests.ExpressionsDslInjectorProvider;

import com.google.inject.Inject;

import ca.mcgill.sel.commons.emf.util.ResourceManager;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.AssignmentStatement;
import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.Interaction;
import ca.mcgill.sel.ram.InteractionFragment;
import ca.mcgill.sel.ram.MessageView;
import ca.mcgill.sel.ram.Operation;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.Type;
import ca.mcgill.sel.ram.ValueSpecification;
import ca.mcgill.sel.ram.expressions.ExpressionDslStandaloneSetup;
import ca.mcgill.sel.ram.expressions.ParserResult;
import ca.mcgill.sel.ram.expressions.ParserUtil;
import ca.mcgill.sel.ram.expressions.ParserUtil.TypeContainer;
import ca.mcgill.sel.ram.expressions.RamStandaloneSetup;
import ca.mcgill.sel.ram.util.MessageViewTestUtil;
import ca.mcgill.sel.ram.util.MessageViewUtil;
import ca.mcgill.sel.ram.util.RamResourceFactoryImpl;

/**
 * Load model then modify its parameters and see if ExpressionsAreHandledCorrectly.
 * 
 * @author maksim
 *
 */
@RunWith(XtextRunner.class)
@InjectWith(ExpressionsDslInjectorProvider.class)
@SuppressWarnings("all")
public class ExpressionParserWithModelTest {
    private static List<Aspect> aspects = new ArrayList<Aspect>();
    private static String testFolder = "resources/ExpressionTests/";
    
    @Inject
    @Extension
    private ParseHelper<ValueSpecification> parseHelper;

    @Inject
    @Extension
    private ValidationTestHelper validationTestHelper;

    /**
     * It's utility enum to keep tests cleaner.
     * 
     * @author maksim
     *
     */
    enum ExpectedType {
        /**
         * Structural value.
         */
        StructuralValue,
        /**
         * Parameter value.
         */
        ParameterValue,
        /**
         * Opaque expression.
         */
        OpaqueExpression,
        /**
         * Literal integer.
         */
        LiteralInteger
    }

    /**
     * Sets up once for/before this class.
     */
    @BeforeClass
    public static void setUpBeforeClass() {
        ResourceManager.initialize();

        // Initialize packages.
        RamPackage.eINSTANCE.eClass();
        CorePackage.eINSTANCE.eClass();

        // Register resource factories.
        ResourceManager.registerExtensionFactory("ram", new RamResourceFactoryImpl());

        ExpressionDslStandaloneSetup.doSetup();
        RamStandaloneSetup.doSetup();

        String [] ramFiles = new File(testFolder).list((dir, name) -> name.toLowerCase().endsWith(".ram"));
        
        for (String name : ramFiles) {
            String path = testFolder + name;
            Aspect aspect = (Aspect) ResourceManager.loadModel(path);
            aspects.add(aspect);
        }
    }

    /**
     * Test if replacement of opaqueNodes with
     * StructuralFeatures, ParameterValues, and local variables is done properly.
     * It loops over the aspects. And reevaluates assignment statements. 
     * If the assignment statement is reevaluated then the test passes. 
     */
    @Test
    public void testReplacementOfOpaqueNodesByStructuralValue() {
        TestUtils utils = new TestUtils();
        for (Aspect aspect : aspects) {
            
            Classifier classifier = MessageViewTestUtil.getClassifierByName(aspect, "TestClass");

            Operation operation = MessageViewTestUtil.getOperationByName(classifier, "main");

            MessageView messageView = MessageViewUtil.getMessageViewFor(aspect, operation);

            Interaction interaction = messageView.getSpecification();

            EList<InteractionFragment> fragments = interaction.getFragments();

            for (InteractionFragment fragment : fragments) {
                if (fragment instanceof AssignmentStatement) {
                    AssignmentStatement assignment = (AssignmentStatement) fragment;
                    
                    Type expectedType = assignment.getAssignTo().getType();
                    TypeContainer container = new TypeContainer(expectedType);

                    String currentText = utils.stringRepr(assignment.getValue());
                    
                    ValueSpecification value = assignment.getValue();
                    ValueSpecification result =
                            parse(assignment, currentText, container);
                    
                    assertNotEquals(result, null);
                    assertEquals(currentText, utils.stringRepr(result));
                }
            }
        }
    }
    
    /**
     * Used to check if expression parsed and is consistent with expectedType.
     * 
     * @param textViewData
     *            This object is a reference to currentTextView.
     * @param textViewText
     *            This is a string that contains expression that we want to parse.
     * @param typeContainer
     *            It's used to contain different types that type checking is using.
     * @return
     *         boolean flag if expression was parsed successfully
     */
    private static ValueSpecification parse(EObject textViewData, String textViewText,
            TypeContainer typeContainer) {
        ParserUtil parserUtil = new ParserUtil();
        ParserResult result = parserUtil.parseAndTypeCheckExpression(textViewText, textViewData, typeContainer);

        if (result != null) {
            if (result.getErrors().isEmpty()) {
                return result.getModel();
            }
        }
        return null;
    }
}
