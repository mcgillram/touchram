package ca.mcgill.sel.ram.expressions.tests

import ca.mcgill.sel.ram.And
import ca.mcgill.sel.ram.Attribute
import ca.mcgill.sel.ram.Comparison
import ca.mcgill.sel.ram.Conditional
import ca.mcgill.sel.ram.Equality
import ca.mcgill.sel.ram.LiteralBoolean
import ca.mcgill.sel.ram.LiteralByte
import ca.mcgill.sel.ram.LiteralDouble
import ca.mcgill.sel.ram.LiteralFloat
import ca.mcgill.sel.ram.LiteralInteger
import ca.mcgill.sel.ram.LiteralLong
import ca.mcgill.sel.ram.LiteralNull
import ca.mcgill.sel.ram.LiteralString
import ca.mcgill.sel.ram.LogicalOperator
import ca.mcgill.sel.ram.Minus
import ca.mcgill.sel.ram.MulDivMod
import ca.mcgill.sel.ram.Not
import ca.mcgill.sel.ram.OpaqueExpression
import ca.mcgill.sel.ram.Or
import ca.mcgill.sel.ram.Plus
import ca.mcgill.sel.ram.PostIncrementOrDecrement
import ca.mcgill.sel.ram.PreIncrementOrDecrement
import ca.mcgill.sel.ram.Shift
import ca.mcgill.sel.ram.StructuralFeatureValue
import ca.mcgill.sel.ram.TypedElement
import ca.mcgill.sel.ram.ValueSpecification
import org.eclipse.emf.common.util.EList
import org.eclipse.xtext.util.EmfFormatter

class TestUtils {
    def printObject(Object input) {
        println(EmfFormatter.objToStr(input))
    }

    def String listToString(EList<ValueSpecification> list) {
        var str = new StringBuilder();
        for (ValueSpecification e : list) {
            str.append('''[«e.stringRepr»]''')
        }
        str.toString()
    }
    
    def String getText(TypedElement t) {
        switch (t) {
            Attribute: '''«t.name»'''
        }
    }

    def String stringRepr(ValueSpecification e) {
        switch (e) {
            PostIncrementOrDecrement: '''«e.expression.stringRepr»«e.op»'''
            PreIncrementOrDecrement: '''«e.op»«e.expression.stringRepr»'''
            Conditional: '''(«e.condition.stringRepr» ? «e.expressionT.stringRepr» : «e.expressionF.stringRepr»)'''
            And: '''(«e.left.stringRepr» && «e.right.stringRepr»)'''
            Or: '''(«e.left.stringRepr» || «e.right.stringRepr»)'''
            Not: '''(!«e.expression.stringRepr»)'''
            Shift: '''(«e.left.stringRepr» «e.op» «e.right.stringRepr»)'''
            Equality: '''(«e.left.stringRepr» «e.op» «e.right.stringRepr»)'''
            Comparison: '''(«e.left.stringRepr» «e.op» «e.right.stringRepr»)'''
            Plus: '''(«e.left.stringRepr» + «e.right.stringRepr»)'''
            Minus: '''(«e.left.stringRepr» - «e.right.stringRepr»)'''
            MulDivMod: '''(«e.left.stringRepr» «e.op» «e.right.stringRepr»)'''
            LogicalOperator: '''(«e.left.stringRepr» «e.op» «e.right.stringRepr»)'''
            LiteralInteger: '''«e.value»'''
            LiteralBoolean: '''«e.value»'''
            LiteralFloat: '''«e.value»'''
            LiteralLong: '''«e.value.toString»'''
            LiteralDouble: '''«e.value»'''
            LiteralByte: '''«e.value»'''
            OpaqueExpression: '''«e.body»'''
            LiteralString: e.value
            LiteralNull: 'null'
            StructuralFeatureValue: e.value.getText
        }.toString
    }
}
