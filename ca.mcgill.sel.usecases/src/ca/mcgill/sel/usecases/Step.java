/**
 */
package ca.mcgill.sel.usecases;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Step</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.usecases.Step#getStepText <em>Step Text</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.Step#getStepDescription <em>Step Description</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.usecases.UcPackage#getStep()
 * @model abstract="true"
 * @generated
 */
public interface Step extends MappableElement {

    /**
     * Returns the value of the '<em><b>Step Text</b></em>' attribute.
     * The default value is <code>""</code>.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Step Text</em>' attribute.
     * @see #setStepText(String)
     * @see ca.mcgill.sel.usecases.UcPackage#getStep_StepText()
     * @model default=""
     * @generated
     */
    String getStepText();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.usecases.Step#getStepText <em>Step Text</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Step Text</em>' attribute.
     * @see #getStepText()
     * @generated
     */
    void setStepText(String value);

    /**
     * Returns the value of the '<em><b>Step Description</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Step Description</em>' containment reference.
     * @see #setStepDescription(ActorReferenceText)
     * @see ca.mcgill.sel.usecases.UcPackage#getStep_StepDescription()
     * @model containment="true" required="true"
     * @generated
     */
    ActorReferenceText getStepDescription();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.usecases.Step#getStepDescription <em>Step Description</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Step Description</em>' containment reference.
     * @see #getStepDescription()
     * @generated
     */
    void setStepDescription(ActorReferenceText value);
} // Step
