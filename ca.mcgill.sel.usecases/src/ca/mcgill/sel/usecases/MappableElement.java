/**
 */
package ca.mcgill.sel.usecases;

import ca.mcgill.sel.core.COREPartialityType;
import ca.mcgill.sel.core.COREVisibilityType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mappable Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.usecases.MappableElement#getPartiality <em>Partiality</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.MappableElement#getVisibility <em>Visibility</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.usecases.UcPackage#getMappableElement()
 * @model
 * @generated
 */
public interface MappableElement extends NamedElement {
	/**
     * Returns the value of the '<em><b>Partiality</b></em>' attribute.
     * The literals are from the enumeration {@link ca.mcgill.sel.core.COREPartialityType}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Partiality</em>' attribute.
     * @see ca.mcgill.sel.core.COREPartialityType
     * @see #setPartiality(COREPartialityType)
     * @see ca.mcgill.sel.usecases.UcPackage#getMappableElement_Partiality()
     * @model
     * @generated
     */
	COREPartialityType getPartiality();

	/**
     * Sets the value of the '{@link ca.mcgill.sel.usecases.MappableElement#getPartiality <em>Partiality</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Partiality</em>' attribute.
     * @see ca.mcgill.sel.core.COREPartialityType
     * @see #getPartiality()
     * @generated
     */
	void setPartiality(COREPartialityType value);

    /**
     * Returns the value of the '<em><b>Visibility</b></em>' attribute.
     * The literals are from the enumeration {@link ca.mcgill.sel.core.COREVisibilityType}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Visibility</em>' attribute.
     * @see ca.mcgill.sel.core.COREVisibilityType
     * @see #setVisibility(COREVisibilityType)
     * @see ca.mcgill.sel.usecases.UcPackage#getMappableElement_Visibility()
     * @model
     * @generated
     */
    COREVisibilityType getVisibility();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.usecases.MappableElement#getVisibility <em>Visibility</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Visibility</em>' attribute.
     * @see ca.mcgill.sel.core.COREVisibilityType
     * @see #getVisibility()
     * @generated
     */
    void setVisibility(COREVisibilityType value);

} // MappableElement
