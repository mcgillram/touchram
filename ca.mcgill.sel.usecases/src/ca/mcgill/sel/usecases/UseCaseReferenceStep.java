/**
 */
package ca.mcgill.sel.usecases;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Use Case Reference Step</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.usecases.UseCaseReferenceStep#getUsecase <em>Usecase</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.usecases.UcPackage#getUseCaseReferenceStep()
 * @model
 * @generated
 */
public interface UseCaseReferenceStep extends Step {
    /**
     * Returns the value of the '<em><b>Usecase</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Usecase</em>' reference.
     * @see #setUsecase(UseCase)
     * @see ca.mcgill.sel.usecases.UcPackage#getUseCaseReferenceStep_Usecase()
     * @model required="true"
     * @generated
     */
    UseCase getUsecase();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.usecases.UseCaseReferenceStep#getUsecase <em>Usecase</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Usecase</em>' reference.
     * @see #getUsecase()
     * @generated
     */
    void setUsecase(UseCase value);

} // UseCaseReferenceStep
