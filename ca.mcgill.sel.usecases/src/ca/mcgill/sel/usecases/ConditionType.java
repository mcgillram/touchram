/**
 */
package ca.mcgill.sel.usecases;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Condition Type</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see ca.mcgill.sel.usecases.UcPackage#getConditionType()
 * @model
 * @generated
 */
public enum ConditionType implements Enumerator {
    /**
     * The '<em><b>System State Condition</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #SYSTEM_STATE_CONDITION_VALUE
     * @generated
     * @ordered
     */
    SYSTEM_STATE_CONDITION(1, "System_State_Condition", "System_State_Condition"),

    /**
     * The '<em><b>Environment Input</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #ENVIRONMENT_INPUT_VALUE
     * @generated
     * @ordered
     */
    ENVIRONMENT_INPUT(2, "Environment_Input", "Environment_Input");

    /**
     * The '<em><b>System State Condition</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #SYSTEM_STATE_CONDITION
     * @model name="System_State_Condition"
     * @generated
     * @ordered
     */
    public static final int SYSTEM_STATE_CONDITION_VALUE = 1;

    /**
     * The '<em><b>Environment Input</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #ENVIRONMENT_INPUT
     * @model name="Environment_Input"
     * @generated
     * @ordered
     */
    public static final int ENVIRONMENT_INPUT_VALUE = 2;

    /**
     * An array of all the '<em><b>Condition Type</b></em>' enumerators.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private static final ConditionType[] VALUES_ARRAY =
        new ConditionType[] {
            SYSTEM_STATE_CONDITION,
            ENVIRONMENT_INPUT,
        };

    /**
     * A public read-only list of all the '<em><b>Condition Type</b></em>' enumerators.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public static final List<ConditionType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

    /**
     * Returns the '<em><b>Condition Type</b></em>' literal with the specified literal value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param literal the literal.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static ConditionType get(String literal) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            ConditionType result = VALUES_ARRAY[i];
            if (result.toString().equals(literal)) {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>Condition Type</b></em>' literal with the specified name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param name the name.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static ConditionType getByName(String name) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            ConditionType result = VALUES_ARRAY[i];
            if (result.getName().equals(name)) {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>Condition Type</b></em>' literal with the specified integer value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the integer value.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static ConditionType get(int value) {
        switch (value) {
            case SYSTEM_STATE_CONDITION_VALUE: return SYSTEM_STATE_CONDITION;
            case ENVIRONMENT_INPUT_VALUE: return ENVIRONMENT_INPUT;
        }
        return null;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private final int value;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private final String name;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private final String literal;

    /**
     * Only this class can construct instances.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private ConditionType(int value, String name, String literal) {
        this.value = value;
        this.name = name;
        this.literal = literal;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public int getValue() {
      return value;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String getName() {
      return name;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String getLiteral() {
      return literal;
    }

    /**
     * Returns the literal value of the enumerator, which is its string representation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String toString() {
        return literal;
    }
    
} //ConditionType
