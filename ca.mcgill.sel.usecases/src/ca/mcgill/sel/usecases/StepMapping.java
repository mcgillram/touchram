/**
 */
package ca.mcgill.sel.usecases;

import ca.mcgill.sel.core.COREMapping;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Step Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.mcgill.sel.usecases.UcPackage#getStepMapping()
 * @model
 * @generated
 */
public interface StepMapping extends COREMapping<Step> {
} // StepMapping
