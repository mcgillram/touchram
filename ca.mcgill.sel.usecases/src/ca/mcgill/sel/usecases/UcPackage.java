/**
 */
package ca.mcgill.sel.usecases;

import ca.mcgill.sel.core.CorePackage;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see ca.mcgill.sel.usecases.UcFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/OCL/Import"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore invocationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot' settingDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot' validationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot'"
 * @generated
 */
public interface UcPackage extends EPackage {
    /**
     * The package name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    String eNAME = "usecases";

    /**
     * The package namespace URI.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    String eNS_URI = "http://cs.mcgill.ca/sel/uc/1.0";

    /**
     * The package namespace name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    String eNS_PREFIX = "usecases";

    /**
     * The singleton instance of the package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    UcPackage eINSTANCE = ca.mcgill.sel.usecases.impl.UcPackageImpl.init();

    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.impl.NamedElementImpl <em>Named Element</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.impl.NamedElementImpl
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getNamedElement()
     * @generated
     */
    int NAMED_ELEMENT = 1;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NAMED_ELEMENT__NAME = 0;

    /**
     * The number of structural features of the '<em>Named Element</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NAMED_ELEMENT_FEATURE_COUNT = 1;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.impl.MappableElementImpl <em>Mappable Element</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.impl.MappableElementImpl
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getMappableElement()
     * @generated
     */
    int MAPPABLE_ELEMENT = 20;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int MAPPABLE_ELEMENT__NAME = NAMED_ELEMENT__NAME;

    /**
     * The feature id for the '<em><b>Partiality</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int MAPPABLE_ELEMENT__PARTIALITY = NAMED_ELEMENT_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Visibility</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int MAPPABLE_ELEMENT__VISIBILITY = NAMED_ELEMENT_FEATURE_COUNT + 1;

    /**
     * The number of structural features of the '<em>Mappable Element</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int MAPPABLE_ELEMENT_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 2;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.impl.ActorImpl <em>Actor</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.impl.ActorImpl
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getActor()
     * @generated
     */
    int ACTOR = 0;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ACTOR__NAME = MAPPABLE_ELEMENT__NAME;

    /**
     * The feature id for the '<em><b>Partiality</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ACTOR__PARTIALITY = MAPPABLE_ELEMENT__PARTIALITY;

    /**
     * The feature id for the '<em><b>Visibility</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ACTOR__VISIBILITY = MAPPABLE_ELEMENT__VISIBILITY;

    /**
     * The feature id for the '<em><b>Lower Bound</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ACTOR__LOWER_BOUND = MAPPABLE_ELEMENT_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Upper Bound</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ACTOR__UPPER_BOUND = MAPPABLE_ELEMENT_FEATURE_COUNT + 1;

    /**
     * The feature id for the '<em><b>Generalization</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ACTOR__GENERALIZATION = MAPPABLE_ELEMENT_FEATURE_COUNT + 2;

    /**
     * The feature id for the '<em><b>Abstract</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ACTOR__ABSTRACT = MAPPABLE_ELEMENT_FEATURE_COUNT + 3;

    /**
     * The number of structural features of the '<em>Actor</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ACTOR_FEATURE_COUNT = MAPPABLE_ELEMENT_FEATURE_COUNT + 4;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.impl.UseCaseImpl <em>Use Case</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.impl.UseCaseImpl
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getUseCase()
     * @generated
     */
    int USE_CASE = 2;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE__NAME = MAPPABLE_ELEMENT__NAME;

    /**
     * The feature id for the '<em><b>Partiality</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE__PARTIALITY = MAPPABLE_ELEMENT__PARTIALITY;

    /**
     * The feature id for the '<em><b>Visibility</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE__VISIBILITY = MAPPABLE_ELEMENT__VISIBILITY;

    /**
     * The feature id for the '<em><b>Primary Actors</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE__PRIMARY_ACTORS = MAPPABLE_ELEMENT_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Secondary Actors</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE__SECONDARY_ACTORS = MAPPABLE_ELEMENT_FEATURE_COUNT + 1;

    /**
     * The feature id for the '<em><b>Intention</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE__INTENTION = MAPPABLE_ELEMENT_FEATURE_COUNT + 2;

    /**
     * The feature id for the '<em><b>Multiplicity</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE__MULTIPLICITY = MAPPABLE_ELEMENT_FEATURE_COUNT + 3;

    /**
     * The feature id for the '<em><b>Level</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE__LEVEL = MAPPABLE_ELEMENT_FEATURE_COUNT + 4;

    /**
     * The feature id for the '<em><b>Abstract</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE__ABSTRACT = MAPPABLE_ELEMENT_FEATURE_COUNT + 5;

    /**
     * The feature id for the '<em><b>Generalization</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE__GENERALIZATION = MAPPABLE_ELEMENT_FEATURE_COUNT + 6;

    /**
     * The feature id for the '<em><b>Included Use Cases</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE__INCLUDED_USE_CASES = MAPPABLE_ELEMENT_FEATURE_COUNT + 7;

    /**
     * The feature id for the '<em><b>Extended Use Case</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE__EXTENDED_USE_CASE = MAPPABLE_ELEMENT_FEATURE_COUNT + 8;

    /**
     * The feature id for the '<em><b>Main Success Scenario</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE__MAIN_SUCCESS_SCENARIO = MAPPABLE_ELEMENT_FEATURE_COUNT + 9;

    /**
     * The feature id for the '<em><b>Selection Condition</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE__SELECTION_CONDITION = MAPPABLE_ELEMENT_FEATURE_COUNT + 10;

    /**
     * The feature id for the '<em><b>Use Case Intention</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE__USE_CASE_INTENTION = MAPPABLE_ELEMENT_FEATURE_COUNT + 11;

    /**
     * The feature id for the '<em><b>Use Case Multiplicity</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE__USE_CASE_MULTIPLICITY = MAPPABLE_ELEMENT_FEATURE_COUNT + 12;

    /**
     * The feature id for the '<em><b>Facilitator Actors</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE__FACILITATOR_ACTORS = MAPPABLE_ELEMENT_FEATURE_COUNT + 13;

    /**
     * The number of structural features of the '<em>Use Case</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE_FEATURE_COUNT = MAPPABLE_ELEMENT_FEATURE_COUNT + 14;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.impl.LayoutImpl <em>Layout</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.impl.LayoutImpl
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getLayout()
     * @generated
     */
    int LAYOUT = 3;

    /**
     * The feature id for the '<em><b>Containers</b></em>' map.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int LAYOUT__CONTAINERS = 0;

    /**
     * The number of structural features of the '<em>Layout</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int LAYOUT_FEATURE_COUNT = 1;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.impl.ContainerMapImpl <em>Container Map</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.impl.ContainerMapImpl
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getContainerMap()
     * @generated
     */
    int CONTAINER_MAP = 4;

    /**
     * The feature id for the '<em><b>Key</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CONTAINER_MAP__KEY = 0;

    /**
     * The feature id for the '<em><b>Value</b></em>' map.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CONTAINER_MAP__VALUE = 1;

    /**
     * The number of structural features of the '<em>Container Map</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CONTAINER_MAP_FEATURE_COUNT = 2;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.impl.ElementMapImpl <em>Element Map</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.impl.ElementMapImpl
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getElementMap()
     * @generated
     */
    int ELEMENT_MAP = 5;

    /**
     * The feature id for the '<em><b>Key</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ELEMENT_MAP__KEY = 0;

    /**
     * The feature id for the '<em><b>Value</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ELEMENT_MAP__VALUE = 1;

    /**
     * The number of structural features of the '<em>Element Map</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ELEMENT_MAP_FEATURE_COUNT = 2;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.impl.LayoutElementImpl <em>Layout Element</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.impl.LayoutElementImpl
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getLayoutElement()
     * @generated
     */
    int LAYOUT_ELEMENT = 6;

    /**
     * The feature id for the '<em><b>X</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int LAYOUT_ELEMENT__X = 0;

    /**
     * The feature id for the '<em><b>Y</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int LAYOUT_ELEMENT__Y = 1;

    /**
     * The number of structural features of the '<em>Layout Element</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int LAYOUT_ELEMENT_FEATURE_COUNT = 2;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.impl.UseCaseModelImpl <em>Use Case Model</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.impl.UseCaseModelImpl
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getUseCaseModel()
     * @generated
     */
    int USE_CASE_MODEL = 7;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE_MODEL__NAME = NAMED_ELEMENT__NAME;

    /**
     * The feature id for the '<em><b>Layout</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE_MODEL__LAYOUT = NAMED_ELEMENT_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Actors</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE_MODEL__ACTORS = NAMED_ELEMENT_FEATURE_COUNT + 1;

    /**
     * The feature id for the '<em><b>Use Cases</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE_MODEL__USE_CASES = NAMED_ELEMENT_FEATURE_COUNT + 2;

    /**
     * The feature id for the '<em><b>Notes</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE_MODEL__NOTES = NAMED_ELEMENT_FEATURE_COUNT + 3;

    /**
     * The feature id for the '<em><b>Conditions</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE_MODEL__CONDITIONS = NAMED_ELEMENT_FEATURE_COUNT + 4;

    /**
     * The feature id for the '<em><b>System Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE_MODEL__SYSTEM_NAME = NAMED_ELEMENT_FEATURE_COUNT + 5;

    /**
     * The number of structural features of the '<em>Use Case Model</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE_MODEL_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 6;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.impl.NoteImpl <em>Note</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.impl.NoteImpl
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getNote()
     * @generated
     */
    int NOTE = 8;

    /**
     * The feature id for the '<em><b>Noted Element</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NOTE__NOTED_ELEMENT = 0;

    /**
     * The feature id for the '<em><b>Content</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NOTE__CONTENT = 1;

    /**
     * The number of structural features of the '<em>Note</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NOTE_FEATURE_COUNT = 2;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.impl.StepImpl <em>Step</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.impl.StepImpl
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getStep()
     * @generated
     */
    int STEP = 9;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int STEP__NAME = MAPPABLE_ELEMENT__NAME;

    /**
     * The feature id for the '<em><b>Partiality</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int STEP__PARTIALITY = MAPPABLE_ELEMENT__PARTIALITY;

    /**
     * The feature id for the '<em><b>Visibility</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int STEP__VISIBILITY = MAPPABLE_ELEMENT__VISIBILITY;

    /**
     * The feature id for the '<em><b>Step Text</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int STEP__STEP_TEXT = MAPPABLE_ELEMENT_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Step Description</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int STEP__STEP_DESCRIPTION = MAPPABLE_ELEMENT_FEATURE_COUNT + 1;

    /**
     * The number of structural features of the '<em>Step</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int STEP_FEATURE_COUNT = MAPPABLE_ELEMENT_FEATURE_COUNT + 2;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.impl.FlowImpl <em>Flow</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.impl.FlowImpl
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getFlow()
     * @generated
     */
    int FLOW = 10;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int FLOW__NAME = NAMED_ELEMENT__NAME;

    /**
     * The feature id for the '<em><b>Steps</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int FLOW__STEPS = NAMED_ELEMENT_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Alternate Flows</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int FLOW__ALTERNATE_FLOWS = NAMED_ELEMENT_FEATURE_COUNT + 1;

    /**
     * The feature id for the '<em><b>Conclusion Type</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int FLOW__CONCLUSION_TYPE = NAMED_ELEMENT_FEATURE_COUNT + 2;

    /**
     * The feature id for the '<em><b>Conclusion Step</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int FLOW__CONCLUSION_STEP = NAMED_ELEMENT_FEATURE_COUNT + 3;

    /**
     * The feature id for the '<em><b>Post Condition</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int FLOW__POST_CONDITION = NAMED_ELEMENT_FEATURE_COUNT + 4;

    /**
     * The feature id for the '<em><b>Referenced Steps</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int FLOW__REFERENCED_STEPS = NAMED_ELEMENT_FEATURE_COUNT + 5;

    /**
     * The feature id for the '<em><b>Precondition Step</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int FLOW__PRECONDITION_STEP = NAMED_ELEMENT_FEATURE_COUNT + 6;

    /**
     * The number of structural features of the '<em>Flow</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int FLOW_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 7;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.impl.CommunicationStepImpl <em>Communication Step</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.impl.CommunicationStepImpl
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getCommunicationStep()
     * @generated
     */
    int COMMUNICATION_STEP = 11;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int COMMUNICATION_STEP__NAME = STEP__NAME;

    /**
     * The feature id for the '<em><b>Partiality</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int COMMUNICATION_STEP__PARTIALITY = STEP__PARTIALITY;

    /**
     * The feature id for the '<em><b>Visibility</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int COMMUNICATION_STEP__VISIBILITY = STEP__VISIBILITY;

    /**
     * The feature id for the '<em><b>Step Text</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int COMMUNICATION_STEP__STEP_TEXT = STEP__STEP_TEXT;

    /**
     * The feature id for the '<em><b>Step Description</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int COMMUNICATION_STEP__STEP_DESCRIPTION = STEP__STEP_DESCRIPTION;

    /**
     * The feature id for the '<em><b>Direction</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int COMMUNICATION_STEP__DIRECTION = STEP_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>Communication Step</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int COMMUNICATION_STEP_FEATURE_COUNT = STEP_FEATURE_COUNT + 1;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.impl.UseCaseReferenceStepImpl <em>Use Case Reference Step</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.impl.UseCaseReferenceStepImpl
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getUseCaseReferenceStep()
     * @generated
     */
    int USE_CASE_REFERENCE_STEP = 12;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE_REFERENCE_STEP__NAME = STEP__NAME;

    /**
     * The feature id for the '<em><b>Partiality</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE_REFERENCE_STEP__PARTIALITY = STEP__PARTIALITY;

    /**
     * The feature id for the '<em><b>Visibility</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE_REFERENCE_STEP__VISIBILITY = STEP__VISIBILITY;

    /**
     * The feature id for the '<em><b>Step Text</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE_REFERENCE_STEP__STEP_TEXT = STEP__STEP_TEXT;

    /**
     * The feature id for the '<em><b>Step Description</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE_REFERENCE_STEP__STEP_DESCRIPTION = STEP__STEP_DESCRIPTION;

    /**
     * The feature id for the '<em><b>Usecase</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE_REFERENCE_STEP__USECASE = STEP_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>Use Case Reference Step</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE_REFERENCE_STEP_FEATURE_COUNT = STEP_FEATURE_COUNT + 1;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.impl.ContextStepImpl <em>Context Step</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.impl.ContextStepImpl
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getContextStep()
     * @generated
     */
    int CONTEXT_STEP = 13;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CONTEXT_STEP__NAME = STEP__NAME;

    /**
     * The feature id for the '<em><b>Partiality</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CONTEXT_STEP__PARTIALITY = STEP__PARTIALITY;

    /**
     * The feature id for the '<em><b>Visibility</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CONTEXT_STEP__VISIBILITY = STEP__VISIBILITY;

    /**
     * The feature id for the '<em><b>Step Text</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CONTEXT_STEP__STEP_TEXT = STEP__STEP_TEXT;

    /**
     * The feature id for the '<em><b>Step Description</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CONTEXT_STEP__STEP_DESCRIPTION = STEP__STEP_DESCRIPTION;

    /**
     * The feature id for the '<em><b>Type</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CONTEXT_STEP__TYPE = STEP_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>Context Step</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CONTEXT_STEP_FEATURE_COUNT = STEP_FEATURE_COUNT + 1;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.impl.ConditionImpl <em>Condition</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.impl.ConditionImpl
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getCondition()
     * @generated
     */
    int CONDITION = 14;

    /**
     * The feature id for the '<em><b>Text</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CONDITION__TEXT = 0;

    /**
     * The feature id for the '<em><b>Condition Text</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CONDITION__CONDITION_TEXT = 1;

    /**
     * The feature id for the '<em><b>Type</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CONDITION__TYPE = 2;

    /**
     * The number of structural features of the '<em>Condition</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CONDITION_FEATURE_COUNT = 3;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.impl.ExtensionPointImpl <em>Extension Point</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.impl.ExtensionPointImpl
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getExtensionPoint()
     * @generated
     */
    int EXTENSION_POINT = 15;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EXTENSION_POINT__NAME = STEP__NAME;

    /**
     * The feature id for the '<em><b>Partiality</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EXTENSION_POINT__PARTIALITY = STEP__PARTIALITY;

    /**
     * The feature id for the '<em><b>Visibility</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EXTENSION_POINT__VISIBILITY = STEP__VISIBILITY;

    /**
     * The feature id for the '<em><b>Step Text</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EXTENSION_POINT__STEP_TEXT = STEP__STEP_TEXT;

    /**
     * The feature id for the '<em><b>Step Description</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EXTENSION_POINT__STEP_DESCRIPTION = STEP__STEP_DESCRIPTION;

    /**
     * The number of structural features of the '<em>Extension Point</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EXTENSION_POINT_FEATURE_COUNT = STEP_FEATURE_COUNT + 0;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.impl.ActorMappingImpl <em>Actor Mapping</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.impl.ActorMappingImpl
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getActorMapping()
     * @generated
     */
    int ACTOR_MAPPING = 16;

    /**
     * The feature id for the '<em><b>To</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ACTOR_MAPPING__TO = CorePackage.CORE_MAPPING__TO;

    /**
     * The feature id for the '<em><b>From</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ACTOR_MAPPING__FROM = CorePackage.CORE_MAPPING__FROM;

    /**
     * The feature id for the '<em><b>Mappings</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ACTOR_MAPPING__MAPPINGS = CorePackage.CORE_MAPPING__MAPPINGS;

    /**
     * The feature id for the '<em><b>Referenced Mappings</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ACTOR_MAPPING__REFERENCED_MAPPINGS = CorePackage.CORE_MAPPING__REFERENCED_MAPPINGS;

    /**
     * The number of structural features of the '<em>Actor Mapping</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ACTOR_MAPPING_FEATURE_COUNT = CorePackage.CORE_MAPPING_FEATURE_COUNT + 0;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.impl.UseCaseMappingImpl <em>Use Case Mapping</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.impl.UseCaseMappingImpl
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getUseCaseMapping()
     * @generated
     */
    int USE_CASE_MAPPING = 17;

    /**
     * The feature id for the '<em><b>To</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE_MAPPING__TO = CorePackage.CORE_MAPPING__TO;

    /**
     * The feature id for the '<em><b>From</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE_MAPPING__FROM = CorePackage.CORE_MAPPING__FROM;

    /**
     * The feature id for the '<em><b>Mappings</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE_MAPPING__MAPPINGS = CorePackage.CORE_MAPPING__MAPPINGS;

    /**
     * The feature id for the '<em><b>Referenced Mappings</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE_MAPPING__REFERENCED_MAPPINGS = CorePackage.CORE_MAPPING__REFERENCED_MAPPINGS;

    /**
     * The number of structural features of the '<em>Use Case Mapping</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int USE_CASE_MAPPING_FEATURE_COUNT = CorePackage.CORE_MAPPING_FEATURE_COUNT + 0;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.impl.StepMappingImpl <em>Step Mapping</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.impl.StepMappingImpl
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getStepMapping()
     * @generated
     */
    int STEP_MAPPING = 18;

    /**
     * The feature id for the '<em><b>To</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int STEP_MAPPING__TO = CorePackage.CORE_MAPPING__TO;

    /**
     * The feature id for the '<em><b>From</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int STEP_MAPPING__FROM = CorePackage.CORE_MAPPING__FROM;

    /**
     * The feature id for the '<em><b>Mappings</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int STEP_MAPPING__MAPPINGS = CorePackage.CORE_MAPPING__MAPPINGS;

    /**
     * The feature id for the '<em><b>Referenced Mappings</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int STEP_MAPPING__REFERENCED_MAPPINGS = CorePackage.CORE_MAPPING__REFERENCED_MAPPINGS;

    /**
     * The number of structural features of the '<em>Step Mapping</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int STEP_MAPPING_FEATURE_COUNT = CorePackage.CORE_MAPPING_FEATURE_COUNT + 0;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.impl.AnythingStepImpl <em>Anything Step</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.impl.AnythingStepImpl
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getAnythingStep()
     * @generated
     */
    int ANYTHING_STEP = 19;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ANYTHING_STEP__NAME = STEP__NAME;

    /**
     * The feature id for the '<em><b>Partiality</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ANYTHING_STEP__PARTIALITY = STEP__PARTIALITY;

    /**
     * The feature id for the '<em><b>Visibility</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ANYTHING_STEP__VISIBILITY = STEP__VISIBILITY;

    /**
     * The feature id for the '<em><b>Step Text</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ANYTHING_STEP__STEP_TEXT = STEP__STEP_TEXT;

    /**
     * The feature id for the '<em><b>Step Description</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ANYTHING_STEP__STEP_DESCRIPTION = STEP__STEP_DESCRIPTION;

    /**
     * The number of structural features of the '<em>Anything Step</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ANYTHING_STEP_FEATURE_COUNT = STEP_FEATURE_COUNT + 0;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.impl.FlowMappingImpl <em>Flow Mapping</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.impl.FlowMappingImpl
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getFlowMapping()
     * @generated
     */
    int FLOW_MAPPING = 21;

    /**
     * The feature id for the '<em><b>To</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int FLOW_MAPPING__TO = CorePackage.CORE_MAPPING__TO;

    /**
     * The feature id for the '<em><b>From</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int FLOW_MAPPING__FROM = CorePackage.CORE_MAPPING__FROM;

    /**
     * The feature id for the '<em><b>Mappings</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int FLOW_MAPPING__MAPPINGS = CorePackage.CORE_MAPPING__MAPPINGS;

    /**
     * The feature id for the '<em><b>Referenced Mappings</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int FLOW_MAPPING__REFERENCED_MAPPINGS = CorePackage.CORE_MAPPING__REFERENCED_MAPPINGS;

    /**
     * The number of structural features of the '<em>Flow Mapping</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int FLOW_MAPPING_FEATURE_COUNT = CorePackage.CORE_MAPPING_FEATURE_COUNT + 0;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.impl.ActorReferenceTextImpl <em>Actor Reference Text</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.impl.ActorReferenceTextImpl
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getActorReferenceText()
     * @generated
     */
    int ACTOR_REFERENCE_TEXT = 22;

    /**
     * The feature id for the '<em><b>Text</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ACTOR_REFERENCE_TEXT__TEXT = 0;

    /**
     * The feature id for the '<em><b>Actor References</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ACTOR_REFERENCE_TEXT__ACTOR_REFERENCES = 1;

    /**
     * The number of structural features of the '<em>Actor Reference Text</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ACTOR_REFERENCE_TEXT_FEATURE_COUNT = 2;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.Level <em>Level</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.Level
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getLevel()
     * @generated
     */
    int LEVEL = 23;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.Direction <em>Direction</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.Direction
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getDirection()
     * @generated
     */
    int DIRECTION = 24;

    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.ContextType <em>Context Type</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.ContextType
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getContextType()
     * @generated
     */
    int CONTEXT_TYPE = 25;


    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.ConclusionType <em>Conclusion Type</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.ConclusionType
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getConclusionType()
     * @generated
     */
    int CONCLUSION_TYPE = 26;


    /**
     * The meta object id for the '{@link ca.mcgill.sel.usecases.ConditionType <em>Condition Type</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ca.mcgill.sel.usecases.ConditionType
     * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getConditionType()
     * @generated
     */
    int CONDITION_TYPE = 27;

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.usecases.Actor <em>Actor</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Actor</em>'.
     * @see ca.mcgill.sel.usecases.Actor
     * @generated
     */
    EClass getActor();

    /**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.usecases.Actor#getLowerBound <em>Lower Bound</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Lower Bound</em>'.
     * @see ca.mcgill.sel.usecases.Actor#getLowerBound()
     * @see #getActor()
     * @generated
     */
    EAttribute getActor_LowerBound();

    /**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.usecases.Actor#getUpperBound <em>Upper Bound</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Upper Bound</em>'.
     * @see ca.mcgill.sel.usecases.Actor#getUpperBound()
     * @see #getActor()
     * @generated
     */
    EAttribute getActor_UpperBound();

    /**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.usecases.Actor#getGeneralization <em>Generalization</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Generalization</em>'.
     * @see ca.mcgill.sel.usecases.Actor#getGeneralization()
     * @see #getActor()
     * @generated
     */
    EReference getActor_Generalization();

    /**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.usecases.Actor#isAbstract <em>Abstract</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Abstract</em>'.
     * @see ca.mcgill.sel.usecases.Actor#isAbstract()
     * @see #getActor()
     * @generated
     */
    EAttribute getActor_Abstract();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.usecases.NamedElement <em>Named Element</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Named Element</em>'.
     * @see ca.mcgill.sel.usecases.NamedElement
     * @generated
     */
    EClass getNamedElement();

    /**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.usecases.NamedElement#getName <em>Name</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see ca.mcgill.sel.usecases.NamedElement#getName()
     * @see #getNamedElement()
     * @generated
     */
    EAttribute getNamedElement_Name();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.usecases.UseCase <em>Use Case</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Use Case</em>'.
     * @see ca.mcgill.sel.usecases.UseCase
     * @generated
     */
    EClass getUseCase();

    /**
     * Returns the meta object for the reference list '{@link ca.mcgill.sel.usecases.UseCase#getPrimaryActors <em>Primary Actors</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Primary Actors</em>'.
     * @see ca.mcgill.sel.usecases.UseCase#getPrimaryActors()
     * @see #getUseCase()
     * @generated
     */
    EReference getUseCase_PrimaryActors();

    /**
     * Returns the meta object for the reference list '{@link ca.mcgill.sel.usecases.UseCase#getSecondaryActors <em>Secondary Actors</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Secondary Actors</em>'.
     * @see ca.mcgill.sel.usecases.UseCase#getSecondaryActors()
     * @see #getUseCase()
     * @generated
     */
    EReference getUseCase_SecondaryActors();

    /**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.usecases.UseCase#getIntention <em>Intention</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Intention</em>'.
     * @see ca.mcgill.sel.usecases.UseCase#getIntention()
     * @see #getUseCase()
     * @generated
     */
    EAttribute getUseCase_Intention();

    /**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.usecases.UseCase#getMultiplicity <em>Multiplicity</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Multiplicity</em>'.
     * @see ca.mcgill.sel.usecases.UseCase#getMultiplicity()
     * @see #getUseCase()
     * @generated
     */
    EAttribute getUseCase_Multiplicity();

    /**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.usecases.UseCase#getLevel <em>Level</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Level</em>'.
     * @see ca.mcgill.sel.usecases.UseCase#getLevel()
     * @see #getUseCase()
     * @generated
     */
    EAttribute getUseCase_Level();

    /**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.usecases.UseCase#isAbstract <em>Abstract</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Abstract</em>'.
     * @see ca.mcgill.sel.usecases.UseCase#isAbstract()
     * @see #getUseCase()
     * @generated
     */
    EAttribute getUseCase_Abstract();

    /**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.usecases.UseCase#getGeneralization <em>Generalization</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Generalization</em>'.
     * @see ca.mcgill.sel.usecases.UseCase#getGeneralization()
     * @see #getUseCase()
     * @generated
     */
    EReference getUseCase_Generalization();

    /**
     * Returns the meta object for the reference list '{@link ca.mcgill.sel.usecases.UseCase#getIncludedUseCases <em>Included Use Cases</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Included Use Cases</em>'.
     * @see ca.mcgill.sel.usecases.UseCase#getIncludedUseCases()
     * @see #getUseCase()
     * @generated
     */
    EReference getUseCase_IncludedUseCases();

    /**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.usecases.UseCase#getExtendedUseCase <em>Extended Use Case</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Extended Use Case</em>'.
     * @see ca.mcgill.sel.usecases.UseCase#getExtendedUseCase()
     * @see #getUseCase()
     * @generated
     */
    EReference getUseCase_ExtendedUseCase();

    /**
     * Returns the meta object for the containment reference '{@link ca.mcgill.sel.usecases.UseCase#getMainSuccessScenario <em>Main Success Scenario</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference '<em>Main Success Scenario</em>'.
     * @see ca.mcgill.sel.usecases.UseCase#getMainSuccessScenario()
     * @see #getUseCase()
     * @generated
     */
    EReference getUseCase_MainSuccessScenario();

    /**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.usecases.UseCase#getSelectionCondition <em>Selection Condition</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Selection Condition</em>'.
     * @see ca.mcgill.sel.usecases.UseCase#getSelectionCondition()
     * @see #getUseCase()
     * @generated
     */
    EReference getUseCase_SelectionCondition();

    /**
     * Returns the meta object for the containment reference '{@link ca.mcgill.sel.usecases.UseCase#getUseCaseIntention <em>Use Case Intention</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference '<em>Use Case Intention</em>'.
     * @see ca.mcgill.sel.usecases.UseCase#getUseCaseIntention()
     * @see #getUseCase()
     * @generated
     */
    EReference getUseCase_UseCaseIntention();

    /**
     * Returns the meta object for the containment reference '{@link ca.mcgill.sel.usecases.UseCase#getUseCaseMultiplicity <em>Use Case Multiplicity</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference '<em>Use Case Multiplicity</em>'.
     * @see ca.mcgill.sel.usecases.UseCase#getUseCaseMultiplicity()
     * @see #getUseCase()
     * @generated
     */
    EReference getUseCase_UseCaseMultiplicity();

    /**
     * Returns the meta object for the reference list '{@link ca.mcgill.sel.usecases.UseCase#getFacilitatorActors <em>Facilitator Actors</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Facilitator Actors</em>'.
     * @see ca.mcgill.sel.usecases.UseCase#getFacilitatorActors()
     * @see #getUseCase()
     * @generated
     */
    EReference getUseCase_FacilitatorActors();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.usecases.Layout <em>Layout</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Layout</em>'.
     * @see ca.mcgill.sel.usecases.Layout
     * @generated
     */
    EClass getLayout();

    /**
     * Returns the meta object for the map '{@link ca.mcgill.sel.usecases.Layout#getContainers <em>Containers</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the map '<em>Containers</em>'.
     * @see ca.mcgill.sel.usecases.Layout#getContainers()
     * @see #getLayout()
     * @generated
     */
    EReference getLayout_Containers();

    /**
     * Returns the meta object for class '{@link java.util.Map.Entry <em>Container Map</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Container Map</em>'.
     * @see java.util.Map.Entry
     * @model keyType="org.eclipse.emf.ecore.EObject" keyRequired="true"
     *        valueMapType="ca.mcgill.sel.usecases.ElementMap&lt;org.eclipse.emf.ecore.EObject, ca.mcgill.sel.usecases.LayoutElement&gt;"
     * @generated
     */
    EClass getContainerMap();

    /**
     * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Key</em>'.
     * @see java.util.Map.Entry
     * @see #getContainerMap()
     * @generated
     */
    EReference getContainerMap_Key();

    /**
     * Returns the meta object for the map '{@link java.util.Map.Entry <em>Value</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the map '<em>Value</em>'.
     * @see java.util.Map.Entry
     * @see #getContainerMap()
     * @generated
     */
    EReference getContainerMap_Value();

    /**
     * Returns the meta object for class '{@link java.util.Map.Entry <em>Element Map</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Element Map</em>'.
     * @see java.util.Map.Entry
     * @model keyType="org.eclipse.emf.ecore.EObject" keyRequired="true"
     *        valueType="ca.mcgill.sel.usecases.LayoutElement" valueContainment="true" valueRequired="true"
     * @generated
     */
    EClass getElementMap();

    /**
     * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Key</em>'.
     * @see java.util.Map.Entry
     * @see #getElementMap()
     * @generated
     */
    EReference getElementMap_Key();

    /**
     * Returns the meta object for the containment reference '{@link java.util.Map.Entry <em>Value</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference '<em>Value</em>'.
     * @see java.util.Map.Entry
     * @see #getElementMap()
     * @generated
     */
    EReference getElementMap_Value();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.usecases.LayoutElement <em>Layout Element</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Layout Element</em>'.
     * @see ca.mcgill.sel.usecases.LayoutElement
     * @generated
     */
    EClass getLayoutElement();

    /**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.usecases.LayoutElement#getX <em>X</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>X</em>'.
     * @see ca.mcgill.sel.usecases.LayoutElement#getX()
     * @see #getLayoutElement()
     * @generated
     */
    EAttribute getLayoutElement_X();

    /**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.usecases.LayoutElement#getY <em>Y</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Y</em>'.
     * @see ca.mcgill.sel.usecases.LayoutElement#getY()
     * @see #getLayoutElement()
     * @generated
     */
    EAttribute getLayoutElement_Y();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.usecases.UseCaseModel <em>Use Case Model</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Use Case Model</em>'.
     * @see ca.mcgill.sel.usecases.UseCaseModel
     * @generated
     */
    EClass getUseCaseModel();

    /**
     * Returns the meta object for the containment reference '{@link ca.mcgill.sel.usecases.UseCaseModel#getLayout <em>Layout</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference '<em>Layout</em>'.
     * @see ca.mcgill.sel.usecases.UseCaseModel#getLayout()
     * @see #getUseCaseModel()
     * @generated
     */
    EReference getUseCaseModel_Layout();

    /**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.usecases.UseCaseModel#getActors <em>Actors</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Actors</em>'.
     * @see ca.mcgill.sel.usecases.UseCaseModel#getActors()
     * @see #getUseCaseModel()
     * @generated
     */
    EReference getUseCaseModel_Actors();

    /**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.usecases.UseCaseModel#getUseCases <em>Use Cases</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Use Cases</em>'.
     * @see ca.mcgill.sel.usecases.UseCaseModel#getUseCases()
     * @see #getUseCaseModel()
     * @generated
     */
    EReference getUseCaseModel_UseCases();

    /**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.usecases.UseCaseModel#getNotes <em>Notes</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Notes</em>'.
     * @see ca.mcgill.sel.usecases.UseCaseModel#getNotes()
     * @see #getUseCaseModel()
     * @generated
     */
    EReference getUseCaseModel_Notes();

    /**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.usecases.UseCaseModel#getConditions <em>Conditions</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Conditions</em>'.
     * @see ca.mcgill.sel.usecases.UseCaseModel#getConditions()
     * @see #getUseCaseModel()
     * @generated
     */
    EReference getUseCaseModel_Conditions();

    /**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.usecases.UseCaseModel#getSystemName <em>System Name</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>System Name</em>'.
     * @see ca.mcgill.sel.usecases.UseCaseModel#getSystemName()
     * @see #getUseCaseModel()
     * @generated
     */
    EAttribute getUseCaseModel_SystemName();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.usecases.Note <em>Note</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Note</em>'.
     * @see ca.mcgill.sel.usecases.Note
     * @generated
     */
    EClass getNote();

    /**
     * Returns the meta object for the reference list '{@link ca.mcgill.sel.usecases.Note#getNotedElement <em>Noted Element</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Noted Element</em>'.
     * @see ca.mcgill.sel.usecases.Note#getNotedElement()
     * @see #getNote()
     * @generated
     */
    EReference getNote_NotedElement();

    /**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.usecases.Note#getContent <em>Content</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Content</em>'.
     * @see ca.mcgill.sel.usecases.Note#getContent()
     * @see #getNote()
     * @generated
     */
    EAttribute getNote_Content();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.usecases.Step <em>Step</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Step</em>'.
     * @see ca.mcgill.sel.usecases.Step
     * @generated
     */
    EClass getStep();

    /**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.usecases.Step#getStepText <em>Step Text</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Step Text</em>'.
     * @see ca.mcgill.sel.usecases.Step#getStepText()
     * @see #getStep()
     * @generated
     */
    EAttribute getStep_StepText();

    /**
     * Returns the meta object for the containment reference '{@link ca.mcgill.sel.usecases.Step#getStepDescription <em>Step Description</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference '<em>Step Description</em>'.
     * @see ca.mcgill.sel.usecases.Step#getStepDescription()
     * @see #getStep()
     * @generated
     */
    EReference getStep_StepDescription();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.usecases.Flow <em>Flow</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Flow</em>'.
     * @see ca.mcgill.sel.usecases.Flow
     * @generated
     */
    EClass getFlow();

    /**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.usecases.Flow#getSteps <em>Steps</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Steps</em>'.
     * @see ca.mcgill.sel.usecases.Flow#getSteps()
     * @see #getFlow()
     * @generated
     */
    EReference getFlow_Steps();

    /**
     * Returns the meta object for the containment reference list '{@link ca.mcgill.sel.usecases.Flow#getAlternateFlows <em>Alternate Flows</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Alternate Flows</em>'.
     * @see ca.mcgill.sel.usecases.Flow#getAlternateFlows()
     * @see #getFlow()
     * @generated
     */
    EReference getFlow_AlternateFlows();

    /**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.usecases.Flow#getConclusionType <em>Conclusion Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Conclusion Type</em>'.
     * @see ca.mcgill.sel.usecases.Flow#getConclusionType()
     * @see #getFlow()
     * @generated
     */
    EAttribute getFlow_ConclusionType();

    /**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.usecases.Flow#getConclusionStep <em>Conclusion Step</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Conclusion Step</em>'.
     * @see ca.mcgill.sel.usecases.Flow#getConclusionStep()
     * @see #getFlow()
     * @generated
     */
    EReference getFlow_ConclusionStep();

    /**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.usecases.Flow#getPostCondition <em>Post Condition</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Post Condition</em>'.
     * @see ca.mcgill.sel.usecases.Flow#getPostCondition()
     * @see #getFlow()
     * @generated
     */
    EReference getFlow_PostCondition();

    /**
     * Returns the meta object for the reference list '{@link ca.mcgill.sel.usecases.Flow#getReferencedSteps <em>Referenced Steps</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Referenced Steps</em>'.
     * @see ca.mcgill.sel.usecases.Flow#getReferencedSteps()
     * @see #getFlow()
     * @generated
     */
    EReference getFlow_ReferencedSteps();

    /**
     * Returns the meta object for the containment reference '{@link ca.mcgill.sel.usecases.Flow#getPreconditionStep <em>Precondition Step</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference '<em>Precondition Step</em>'.
     * @see ca.mcgill.sel.usecases.Flow#getPreconditionStep()
     * @see #getFlow()
     * @generated
     */
    EReference getFlow_PreconditionStep();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.usecases.CommunicationStep <em>Communication Step</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Communication Step</em>'.
     * @see ca.mcgill.sel.usecases.CommunicationStep
     * @generated
     */
    EClass getCommunicationStep();

    /**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.usecases.CommunicationStep#getDirection <em>Direction</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Direction</em>'.
     * @see ca.mcgill.sel.usecases.CommunicationStep#getDirection()
     * @see #getCommunicationStep()
     * @generated
     */
    EAttribute getCommunicationStep_Direction();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.usecases.UseCaseReferenceStep <em>Use Case Reference Step</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Use Case Reference Step</em>'.
     * @see ca.mcgill.sel.usecases.UseCaseReferenceStep
     * @generated
     */
    EClass getUseCaseReferenceStep();

    /**
     * Returns the meta object for the reference '{@link ca.mcgill.sel.usecases.UseCaseReferenceStep#getUsecase <em>Usecase</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Usecase</em>'.
     * @see ca.mcgill.sel.usecases.UseCaseReferenceStep#getUsecase()
     * @see #getUseCaseReferenceStep()
     * @generated
     */
    EReference getUseCaseReferenceStep_Usecase();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.usecases.ContextStep <em>Context Step</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Context Step</em>'.
     * @see ca.mcgill.sel.usecases.ContextStep
     * @generated
     */
    EClass getContextStep();

    /**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.usecases.ContextStep#getType <em>Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Type</em>'.
     * @see ca.mcgill.sel.usecases.ContextStep#getType()
     * @see #getContextStep()
     * @generated
     */
    EAttribute getContextStep_Type();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.usecases.Condition <em>Condition</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Condition</em>'.
     * @see ca.mcgill.sel.usecases.Condition
     * @generated
     */
    EClass getCondition();

    /**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.usecases.Condition#getText <em>Text</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Text</em>'.
     * @see ca.mcgill.sel.usecases.Condition#getText()
     * @see #getCondition()
     * @generated
     */
    EAttribute getCondition_Text();

    /**
     * Returns the meta object for the containment reference '{@link ca.mcgill.sel.usecases.Condition#getConditionText <em>Condition Text</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference '<em>Condition Text</em>'.
     * @see ca.mcgill.sel.usecases.Condition#getConditionText()
     * @see #getCondition()
     * @generated
     */
    EReference getCondition_ConditionText();

    /**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.usecases.Condition#getType <em>Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Type</em>'.
     * @see ca.mcgill.sel.usecases.Condition#getType()
     * @see #getCondition()
     * @generated
     */
    EAttribute getCondition_Type();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.usecases.ExtensionPoint <em>Extension Point</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Extension Point</em>'.
     * @see ca.mcgill.sel.usecases.ExtensionPoint
     * @generated
     */
    EClass getExtensionPoint();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.usecases.ActorMapping <em>Actor Mapping</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Actor Mapping</em>'.
     * @see ca.mcgill.sel.usecases.ActorMapping
     * @generated
     */
    EClass getActorMapping();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.usecases.UseCaseMapping <em>Use Case Mapping</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Use Case Mapping</em>'.
     * @see ca.mcgill.sel.usecases.UseCaseMapping
     * @generated
     */
    EClass getUseCaseMapping();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.usecases.StepMapping <em>Step Mapping</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Step Mapping</em>'.
     * @see ca.mcgill.sel.usecases.StepMapping
     * @generated
     */
    EClass getStepMapping();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.usecases.AnythingStep <em>Anything Step</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Anything Step</em>'.
     * @see ca.mcgill.sel.usecases.AnythingStep
     * @generated
     */
    EClass getAnythingStep();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.usecases.MappableElement <em>Mappable Element</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Mappable Element</em>'.
     * @see ca.mcgill.sel.usecases.MappableElement
     * @generated
     */
    EClass getMappableElement();

    /**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.usecases.MappableElement#getPartiality <em>Partiality</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Partiality</em>'.
     * @see ca.mcgill.sel.usecases.MappableElement#getPartiality()
     * @see #getMappableElement()
     * @generated
     */
    EAttribute getMappableElement_Partiality();

    /**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.usecases.MappableElement#getVisibility <em>Visibility</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Visibility</em>'.
     * @see ca.mcgill.sel.usecases.MappableElement#getVisibility()
     * @see #getMappableElement()
     * @generated
     */
    EAttribute getMappableElement_Visibility();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.usecases.FlowMapping <em>Flow Mapping</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Flow Mapping</em>'.
     * @see ca.mcgill.sel.usecases.FlowMapping
     * @generated
     */
    EClass getFlowMapping();

    /**
     * Returns the meta object for class '{@link ca.mcgill.sel.usecases.ActorReferenceText <em>Actor Reference Text</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Actor Reference Text</em>'.
     * @see ca.mcgill.sel.usecases.ActorReferenceText
     * @generated
     */
    EClass getActorReferenceText();

    /**
     * Returns the meta object for the attribute '{@link ca.mcgill.sel.usecases.ActorReferenceText#getText <em>Text</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Text</em>'.
     * @see ca.mcgill.sel.usecases.ActorReferenceText#getText()
     * @see #getActorReferenceText()
     * @generated
     */
    EAttribute getActorReferenceText_Text();

    /**
     * Returns the meta object for the reference list '{@link ca.mcgill.sel.usecases.ActorReferenceText#getActorReferences <em>Actor References</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Actor References</em>'.
     * @see ca.mcgill.sel.usecases.ActorReferenceText#getActorReferences()
     * @see #getActorReferenceText()
     * @generated
     */
    EReference getActorReferenceText_ActorReferences();

    /**
     * Returns the meta object for enum '{@link ca.mcgill.sel.usecases.Level <em>Level</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for enum '<em>Level</em>'.
     * @see ca.mcgill.sel.usecases.Level
     * @generated
     */
    EEnum getLevel();

    /**
     * Returns the meta object for enum '{@link ca.mcgill.sel.usecases.Direction <em>Direction</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for enum '<em>Direction</em>'.
     * @see ca.mcgill.sel.usecases.Direction
     * @generated
     */
    EEnum getDirection();

    /**
     * Returns the meta object for enum '{@link ca.mcgill.sel.usecases.ContextType <em>Context Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for enum '<em>Context Type</em>'.
     * @see ca.mcgill.sel.usecases.ContextType
     * @generated
     */
    EEnum getContextType();

    /**
     * Returns the meta object for enum '{@link ca.mcgill.sel.usecases.ConclusionType <em>Conclusion Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for enum '<em>Conclusion Type</em>'.
     * @see ca.mcgill.sel.usecases.ConclusionType
     * @generated
     */
    EEnum getConclusionType();

    /**
     * Returns the meta object for enum '{@link ca.mcgill.sel.usecases.ConditionType <em>Condition Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for enum '<em>Condition Type</em>'.
     * @see ca.mcgill.sel.usecases.ConditionType
     * @generated
     */
    EEnum getConditionType();

    /**
     * Returns the factory that creates the instances of the model.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the factory that creates the instances of the model.
     * @generated
     */
    UcFactory getUcFactory();

    /**
     * <!-- begin-user-doc -->
     * Defines literals for the meta objects that represent
     * <ul>
     *   <li>each class,</li>
     *   <li>each feature of each class,</li>
     *   <li>each operation of each class,</li>
     *   <li>each enum,</li>
     *   <li>and each data type</li>
     * </ul>
     * <!-- end-user-doc -->
     * @generated
     */
    interface Literals {
        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.impl.ActorImpl <em>Actor</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.impl.ActorImpl
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getActor()
         * @generated
         */
        EClass ACTOR = eINSTANCE.getActor();

        /**
         * The meta object literal for the '<em><b>Lower Bound</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute ACTOR__LOWER_BOUND = eINSTANCE.getActor_LowerBound();

        /**
         * The meta object literal for the '<em><b>Upper Bound</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute ACTOR__UPPER_BOUND = eINSTANCE.getActor_UpperBound();

        /**
         * The meta object literal for the '<em><b>Generalization</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference ACTOR__GENERALIZATION = eINSTANCE.getActor_Generalization();

        /**
         * The meta object literal for the '<em><b>Abstract</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute ACTOR__ABSTRACT = eINSTANCE.getActor_Abstract();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.impl.NamedElementImpl <em>Named Element</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.impl.NamedElementImpl
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getNamedElement()
         * @generated
         */
        EClass NAMED_ELEMENT = eINSTANCE.getNamedElement();

        /**
         * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute NAMED_ELEMENT__NAME = eINSTANCE.getNamedElement_Name();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.impl.UseCaseImpl <em>Use Case</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.impl.UseCaseImpl
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getUseCase()
         * @generated
         */
        EClass USE_CASE = eINSTANCE.getUseCase();

        /**
         * The meta object literal for the '<em><b>Primary Actors</b></em>' reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference USE_CASE__PRIMARY_ACTORS = eINSTANCE.getUseCase_PrimaryActors();

        /**
         * The meta object literal for the '<em><b>Secondary Actors</b></em>' reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference USE_CASE__SECONDARY_ACTORS = eINSTANCE.getUseCase_SecondaryActors();

        /**
         * The meta object literal for the '<em><b>Intention</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute USE_CASE__INTENTION = eINSTANCE.getUseCase_Intention();

        /**
         * The meta object literal for the '<em><b>Multiplicity</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute USE_CASE__MULTIPLICITY = eINSTANCE.getUseCase_Multiplicity();

        /**
         * The meta object literal for the '<em><b>Level</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute USE_CASE__LEVEL = eINSTANCE.getUseCase_Level();

        /**
         * The meta object literal for the '<em><b>Abstract</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute USE_CASE__ABSTRACT = eINSTANCE.getUseCase_Abstract();

        /**
         * The meta object literal for the '<em><b>Generalization</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference USE_CASE__GENERALIZATION = eINSTANCE.getUseCase_Generalization();

        /**
         * The meta object literal for the '<em><b>Included Use Cases</b></em>' reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference USE_CASE__INCLUDED_USE_CASES = eINSTANCE.getUseCase_IncludedUseCases();

        /**
         * The meta object literal for the '<em><b>Extended Use Case</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference USE_CASE__EXTENDED_USE_CASE = eINSTANCE.getUseCase_ExtendedUseCase();

        /**
         * The meta object literal for the '<em><b>Main Success Scenario</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference USE_CASE__MAIN_SUCCESS_SCENARIO = eINSTANCE.getUseCase_MainSuccessScenario();

        /**
         * The meta object literal for the '<em><b>Selection Condition</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference USE_CASE__SELECTION_CONDITION = eINSTANCE.getUseCase_SelectionCondition();

        /**
         * The meta object literal for the '<em><b>Use Case Intention</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference USE_CASE__USE_CASE_INTENTION = eINSTANCE.getUseCase_UseCaseIntention();

        /**
         * The meta object literal for the '<em><b>Use Case Multiplicity</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference USE_CASE__USE_CASE_MULTIPLICITY = eINSTANCE.getUseCase_UseCaseMultiplicity();

        /**
         * The meta object literal for the '<em><b>Facilitator Actors</b></em>' reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference USE_CASE__FACILITATOR_ACTORS = eINSTANCE.getUseCase_FacilitatorActors();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.impl.LayoutImpl <em>Layout</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.impl.LayoutImpl
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getLayout()
         * @generated
         */
        EClass LAYOUT = eINSTANCE.getLayout();

        /**
         * The meta object literal for the '<em><b>Containers</b></em>' map feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference LAYOUT__CONTAINERS = eINSTANCE.getLayout_Containers();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.impl.ContainerMapImpl <em>Container Map</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.impl.ContainerMapImpl
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getContainerMap()
         * @generated
         */
        EClass CONTAINER_MAP = eINSTANCE.getContainerMap();

        /**
         * The meta object literal for the '<em><b>Key</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference CONTAINER_MAP__KEY = eINSTANCE.getContainerMap_Key();

        /**
         * The meta object literal for the '<em><b>Value</b></em>' map feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference CONTAINER_MAP__VALUE = eINSTANCE.getContainerMap_Value();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.impl.ElementMapImpl <em>Element Map</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.impl.ElementMapImpl
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getElementMap()
         * @generated
         */
        EClass ELEMENT_MAP = eINSTANCE.getElementMap();

        /**
         * The meta object literal for the '<em><b>Key</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference ELEMENT_MAP__KEY = eINSTANCE.getElementMap_Key();

        /**
         * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference ELEMENT_MAP__VALUE = eINSTANCE.getElementMap_Value();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.impl.LayoutElementImpl <em>Layout Element</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.impl.LayoutElementImpl
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getLayoutElement()
         * @generated
         */
        EClass LAYOUT_ELEMENT = eINSTANCE.getLayoutElement();

        /**
         * The meta object literal for the '<em><b>X</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute LAYOUT_ELEMENT__X = eINSTANCE.getLayoutElement_X();

        /**
         * The meta object literal for the '<em><b>Y</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute LAYOUT_ELEMENT__Y = eINSTANCE.getLayoutElement_Y();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.impl.UseCaseModelImpl <em>Use Case Model</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.impl.UseCaseModelImpl
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getUseCaseModel()
         * @generated
         */
        EClass USE_CASE_MODEL = eINSTANCE.getUseCaseModel();

        /**
         * The meta object literal for the '<em><b>Layout</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference USE_CASE_MODEL__LAYOUT = eINSTANCE.getUseCaseModel_Layout();

        /**
         * The meta object literal for the '<em><b>Actors</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference USE_CASE_MODEL__ACTORS = eINSTANCE.getUseCaseModel_Actors();

        /**
         * The meta object literal for the '<em><b>Use Cases</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference USE_CASE_MODEL__USE_CASES = eINSTANCE.getUseCaseModel_UseCases();

        /**
         * The meta object literal for the '<em><b>Notes</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference USE_CASE_MODEL__NOTES = eINSTANCE.getUseCaseModel_Notes();

        /**
         * The meta object literal for the '<em><b>Conditions</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference USE_CASE_MODEL__CONDITIONS = eINSTANCE.getUseCaseModel_Conditions();

        /**
         * The meta object literal for the '<em><b>System Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute USE_CASE_MODEL__SYSTEM_NAME = eINSTANCE.getUseCaseModel_SystemName();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.impl.NoteImpl <em>Note</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.impl.NoteImpl
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getNote()
         * @generated
         */
        EClass NOTE = eINSTANCE.getNote();

        /**
         * The meta object literal for the '<em><b>Noted Element</b></em>' reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference NOTE__NOTED_ELEMENT = eINSTANCE.getNote_NotedElement();

        /**
         * The meta object literal for the '<em><b>Content</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute NOTE__CONTENT = eINSTANCE.getNote_Content();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.impl.StepImpl <em>Step</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.impl.StepImpl
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getStep()
         * @generated
         */
        EClass STEP = eINSTANCE.getStep();

        /**
         * The meta object literal for the '<em><b>Step Text</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute STEP__STEP_TEXT = eINSTANCE.getStep_StepText();

        /**
         * The meta object literal for the '<em><b>Step Description</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference STEP__STEP_DESCRIPTION = eINSTANCE.getStep_StepDescription();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.impl.FlowImpl <em>Flow</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.impl.FlowImpl
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getFlow()
         * @generated
         */
        EClass FLOW = eINSTANCE.getFlow();

        /**
         * The meta object literal for the '<em><b>Steps</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference FLOW__STEPS = eINSTANCE.getFlow_Steps();

        /**
         * The meta object literal for the '<em><b>Alternate Flows</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference FLOW__ALTERNATE_FLOWS = eINSTANCE.getFlow_AlternateFlows();

        /**
         * The meta object literal for the '<em><b>Conclusion Type</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute FLOW__CONCLUSION_TYPE = eINSTANCE.getFlow_ConclusionType();

        /**
         * The meta object literal for the '<em><b>Conclusion Step</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference FLOW__CONCLUSION_STEP = eINSTANCE.getFlow_ConclusionStep();

        /**
         * The meta object literal for the '<em><b>Post Condition</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference FLOW__POST_CONDITION = eINSTANCE.getFlow_PostCondition();

        /**
         * The meta object literal for the '<em><b>Referenced Steps</b></em>' reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference FLOW__REFERENCED_STEPS = eINSTANCE.getFlow_ReferencedSteps();

        /**
         * The meta object literal for the '<em><b>Precondition Step</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference FLOW__PRECONDITION_STEP = eINSTANCE.getFlow_PreconditionStep();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.impl.CommunicationStepImpl <em>Communication Step</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.impl.CommunicationStepImpl
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getCommunicationStep()
         * @generated
         */
        EClass COMMUNICATION_STEP = eINSTANCE.getCommunicationStep();

        /**
         * The meta object literal for the '<em><b>Direction</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute COMMUNICATION_STEP__DIRECTION = eINSTANCE.getCommunicationStep_Direction();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.impl.UseCaseReferenceStepImpl <em>Use Case Reference Step</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.impl.UseCaseReferenceStepImpl
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getUseCaseReferenceStep()
         * @generated
         */
        EClass USE_CASE_REFERENCE_STEP = eINSTANCE.getUseCaseReferenceStep();

        /**
         * The meta object literal for the '<em><b>Usecase</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference USE_CASE_REFERENCE_STEP__USECASE = eINSTANCE.getUseCaseReferenceStep_Usecase();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.impl.ContextStepImpl <em>Context Step</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.impl.ContextStepImpl
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getContextStep()
         * @generated
         */
        EClass CONTEXT_STEP = eINSTANCE.getContextStep();

        /**
         * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute CONTEXT_STEP__TYPE = eINSTANCE.getContextStep_Type();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.impl.ConditionImpl <em>Condition</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.impl.ConditionImpl
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getCondition()
         * @generated
         */
        EClass CONDITION = eINSTANCE.getCondition();

        /**
         * The meta object literal for the '<em><b>Text</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute CONDITION__TEXT = eINSTANCE.getCondition_Text();

        /**
         * The meta object literal for the '<em><b>Condition Text</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference CONDITION__CONDITION_TEXT = eINSTANCE.getCondition_ConditionText();

        /**
         * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute CONDITION__TYPE = eINSTANCE.getCondition_Type();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.impl.ExtensionPointImpl <em>Extension Point</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.impl.ExtensionPointImpl
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getExtensionPoint()
         * @generated
         */
        EClass EXTENSION_POINT = eINSTANCE.getExtensionPoint();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.impl.ActorMappingImpl <em>Actor Mapping</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.impl.ActorMappingImpl
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getActorMapping()
         * @generated
         */
        EClass ACTOR_MAPPING = eINSTANCE.getActorMapping();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.impl.UseCaseMappingImpl <em>Use Case Mapping</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.impl.UseCaseMappingImpl
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getUseCaseMapping()
         * @generated
         */
        EClass USE_CASE_MAPPING = eINSTANCE.getUseCaseMapping();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.impl.StepMappingImpl <em>Step Mapping</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.impl.StepMappingImpl
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getStepMapping()
         * @generated
         */
        EClass STEP_MAPPING = eINSTANCE.getStepMapping();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.impl.AnythingStepImpl <em>Anything Step</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.impl.AnythingStepImpl
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getAnythingStep()
         * @generated
         */
        EClass ANYTHING_STEP = eINSTANCE.getAnythingStep();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.impl.MappableElementImpl <em>Mappable Element</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.impl.MappableElementImpl
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getMappableElement()
         * @generated
         */
        EClass MAPPABLE_ELEMENT = eINSTANCE.getMappableElement();

        /**
         * The meta object literal for the '<em><b>Partiality</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute MAPPABLE_ELEMENT__PARTIALITY = eINSTANCE.getMappableElement_Partiality();

        /**
         * The meta object literal for the '<em><b>Visibility</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute MAPPABLE_ELEMENT__VISIBILITY = eINSTANCE.getMappableElement_Visibility();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.impl.FlowMappingImpl <em>Flow Mapping</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.impl.FlowMappingImpl
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getFlowMapping()
         * @generated
         */
        EClass FLOW_MAPPING = eINSTANCE.getFlowMapping();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.impl.ActorReferenceTextImpl <em>Actor Reference Text</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.impl.ActorReferenceTextImpl
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getActorReferenceText()
         * @generated
         */
        EClass ACTOR_REFERENCE_TEXT = eINSTANCE.getActorReferenceText();

        /**
         * The meta object literal for the '<em><b>Text</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute ACTOR_REFERENCE_TEXT__TEXT = eINSTANCE.getActorReferenceText_Text();

        /**
         * The meta object literal for the '<em><b>Actor References</b></em>' reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference ACTOR_REFERENCE_TEXT__ACTOR_REFERENCES = eINSTANCE.getActorReferenceText_ActorReferences();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.Level <em>Level</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.Level
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getLevel()
         * @generated
         */
        EEnum LEVEL = eINSTANCE.getLevel();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.Direction <em>Direction</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.Direction
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getDirection()
         * @generated
         */
        EEnum DIRECTION = eINSTANCE.getDirection();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.ContextType <em>Context Type</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.ContextType
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getContextType()
         * @generated
         */
        EEnum CONTEXT_TYPE = eINSTANCE.getContextType();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.ConclusionType <em>Conclusion Type</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.ConclusionType
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getConclusionType()
         * @generated
         */
        EEnum CONCLUSION_TYPE = eINSTANCE.getConclusionType();

        /**
         * The meta object literal for the '{@link ca.mcgill.sel.usecases.ConditionType <em>Condition Type</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see ca.mcgill.sel.usecases.ConditionType
         * @see ca.mcgill.sel.usecases.impl.UcPackageImpl#getConditionType()
         * @generated
         */
        EEnum CONDITION_TYPE = eINSTANCE.getConditionType();

    }

} //UcPackage
