package ca.mcgill.sel.usecases.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.Stack;
import java.util.stream.Collectors;

import org.eclipse.emf.common.util.BasicEMap;
import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.core.COREMapping;
import ca.mcgill.sel.core.controller.ModelUtil;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.core.util.COREModelUtil;
import ca.mcgill.sel.usecases.Actor;
import ca.mcgill.sel.usecases.CommunicationStep;
import ca.mcgill.sel.usecases.ContextStep;
import ca.mcgill.sel.usecases.ContextType;
import ca.mcgill.sel.usecases.Flow;
import ca.mcgill.sel.usecases.Layout;
import ca.mcgill.sel.usecases.LayoutElement;
import ca.mcgill.sel.usecases.Level;
import ca.mcgill.sel.usecases.NamedElement;
import ca.mcgill.sel.usecases.Step;
import ca.mcgill.sel.usecases.UcFactory;
import ca.mcgill.sel.usecases.UseCase;
import ca.mcgill.sel.usecases.UseCaseModel;
import ca.mcgill.sel.usecases.UseCaseReferenceStep;

public final class UcModelUtil implements ModelUtil {   
    
    /**
     * Creates a new instance of {@link UcdmModelUtil}.
     */
    public UcModelUtil() {
    }    

    /**
     * Creates a new use case diagram and associated entities
     * @param name The name of the diagram
     * @return The new diagram
     */
    public static UseCaseModel createUseCaseDiagram(String name) {
        UseCaseModel ucd = UcFactory.eINSTANCE.createUseCaseModel();       

        ucd.setName(name);
        ucd.setSystemName(name);

        // create an empty layout
        createLayout(ucd);       

        return ucd;
    }
    
    /**
     * Gets the set of use cases included by a use case.
     * @param useCase The use case
     * @return the included use cases
     */
    public static List<UseCase> getIncludedUseCases(UseCase useCase) {
        List<UseCase> includedUseCases = new ArrayList<UseCase>();
        includedUseCases.addAll(useCase.getIncludedUseCases());
        
        if (useCase.getMainSuccessScenario() == null) {
            return includedUseCases;
        }
        
        List<Flow> flows = new ArrayList<Flow>();
        flows.add(useCase.getMainSuccessScenario());
        flows.addAll(useCase.getMainSuccessScenario().getAlternateFlows());
        
        for (Flow flow : flows) {
            for (Step step : flow.getSteps()) {
                if (step instanceof UseCaseReferenceStep) {
                    UseCaseReferenceStep useCaseStep = (UseCaseReferenceStep) step;
                    includedUseCases.add(useCaseStep.getUsecase());
                }
            }    
        }        
        
        return includedUseCases;
    }
    
    /**
     * Gets the set of use cases that include this use case.
     * @param useCase The use case.
     * @return The use cases including this use case.
     */
    public static List<UseCase> getUseCasesIncluding(UseCase useCase) {
        List<UseCase> useCases = new ArrayList<UseCase>();
        UseCaseModel ucm = (UseCaseModel) useCase.eContainer();
        
        for (UseCase uc : ucm.getUseCases()) {
            if (getIncludedUseCases(uc).contains(useCase)) {
                useCases.add(uc);
            }
        }
        
        return useCases;
    }
    
    /**
     * Gets all use cases generalized by this use case.
     * @param useCase The use case.
     * @return The use cases.
     */
    public static List<UseCase> getUseCasesGeneralizedBy(UseCase useCase) {
        List<UseCase> useCases = new ArrayList<UseCase>();
        UseCaseModel ucm = (UseCaseModel) useCase.eContainer();
        
        for (UseCase uc : ucm.getUseCases()) {
            if (uc.getGeneralization() == useCase) {
                useCases.add(uc);
            }
        }
        
        return useCases;
    }
    
    
    /**
     * Gets the list of flows in a use case that reference a particular step.
     * @param useCase The use case
     * @param step The step.
     * @return The flows
     */
    public static List<Flow> getFlowsReferencingStep(Flow flow, Step step) {
        List<Flow> flows = new ArrayList<Flow>();
        
        for (Flow f : flow.getAlternateFlows()) {
            if (f.getReferencedSteps().contains(step)) {
                flows.add(f);
            }
        }
        
        return flows;
    }
    
    /**
     * Gets the list of flows in a use case that reference a particular step in its conclusion.
     * @param useCase The use case
     * @param step The step.
     * @return The flows
     */
    public static List<Flow> getConclusionsReferencingStep(Flow flow, Step step) {
        List<Flow> flows = new ArrayList<Flow>();
        
        for (Flow f : flow.getAlternateFlows()) {            
            if (f.getConclusionStep() == step) {
                flows.add(f);
            }
        }
        
        return flows;
    }
    
    /**
     * Gets the index of a step within its parent flow.
     * @param step The step.
     * @return The index of the step in its flow.
     */
    public static int getStepIndex(Step step) {
        Flow parentFlow = (Flow) step.eContainer();
        return parentFlow.getSteps().indexOf(step);
    }
    
    /**
     * Checks if a link of a particular type exists between two use cases.
     * @param source The source use case.
     * @param dest The destination use case.
     * @param type The association type.
     * @return True if an association exists, false otherwise.
     */
    public static boolean useCaseLinkExists(UseCase source, UseCase dest, UseCaseAssociationType type) {
        List<UseCase> linkedUseCases = getIncludedUseCases(source);        
        return linkedUseCases.contains(dest);
    }
    
    /**
     * Gets all use cases in the model associated with an actor.
     * @param actor The actor.
     * @return The use cases.
     */
    public static List<UseCase> getUseCasesAssociatedToActor(Actor actor) {
        List<UseCase> useCases = new ArrayList<UseCase>();
        UseCaseModel ucm = (UseCaseModel) actor.eContainer();
        for (UseCase uc : ucm.getUseCases()) {
            if (uc.getPrimaryActors().contains(actor) || uc.getSecondaryActors().contains(actor)) {
                useCases.add(uc);
            }
        }
        
        return useCases;
    }
    
    /**
     * Gets all steps in a flow that can be extended
     * (either by an alternate flow or an extension use case).
     * @param flow The flow.
     * @return The extendable steps.
     */
    public static List<Step> getExtendableSteps(Flow flow) {
        return flow.getSteps().stream()
                .filter(s -> isExtendableStep(s))
                .collect(Collectors.toList());
    }
    
    /**
     * Gets the list of actors available for reference in a use case step.
     * @param useCase The use case.
     * @return The list of actors.
     */
    public static Set<Actor> getAvailableActors(UseCase useCase) {
        return getAvailableActors(useCase, new HashSet<UseCase>());
    }
    
    /**
     * Gets the list of flows that can be compared to the current flow.
     * This means they are at the same extension level, with the same step index.
     * @param flow The flow
     * @return The comparable flows.
     */
    public static List<Flow> getComparableFlows(Flow flow) {
        if (!(flow.eContainer() instanceof Flow)) {
            return new ArrayList<Flow>();
        }
        
        Flow parentFlow = (Flow) flow.eContainer();
        String flowStepIndex = UseCaseTextUtils.getFlowStepIndexText(flow, parentFlow);
        return parentFlow.getAlternateFlows()
                .stream()
                .filter(f -> UseCaseTextUtils.getFlowStepIndexText(f, parentFlow).equals(flowStepIndex))
                .collect(Collectors.toList());
    }
    
    /**
     * Gets all steps associated to all flows in a use case. Sorted by flow.
     * @param useCase The use case.
     * @return The steps.
     */
    public static List<Step> getSteps(UseCase useCase) {
        List<Step> steps = new ArrayList<Step>();
        
        for (Flow f : getFlows(useCase)) {
            steps.addAll(f.getSteps());
        }
        
        return steps;
    }
    
    /**
     * Gets all flows in a use case.
     * @param useCase The use case.
     * @return The flows.
     */
    public static List<Flow> getFlows(UseCase useCase) {
        List<Flow> flows = new ArrayList<Flow>();
        if (useCase.getMainSuccessScenario() == null) {
            return flows;
        }
        
        flows.add(useCase.getMainSuccessScenario());
        Stack<Flow> flowsToProcess = new Stack<Flow>();
        flowsToProcess.addAll(useCase.getMainSuccessScenario().getAlternateFlows());
        while (!flowsToProcess.empty()) {
            Flow f = flowsToProcess.pop();
            flows.add(f);
            flowsToProcess.addAll(f.getAlternateFlows());
        }
        
        return flows;
    }
    
    /**
     * Return the expected text if the mapping have referenced mappings or the next expected one to display.
     * Will return a String of this form &lt;A,B&gt;
     * If there is no referenced mappings, just return an empty string
     * 
     * @param mapping The mapping we're looking at
     * @param element The element that will be displayed
     * @return A string containing the referenced mappings
     */
    public static String getReferencedMappingsText(COREMapping<? extends EObject> mapping, EObject element) {
        StringBuilder result = new StringBuilder();
        if (mapping.getReferencedMappings().size() >= 1 
                && (mapping.getFrom() == null || mapping.getFrom() == element)) {
            result.append("<");
            for (COREMapping<?> refMapping : mapping.getReferencedMappings()) {
                if (refMapping.getTo() != null) {
                    NamedElement namedElement = (NamedElement) refMapping.getTo();
                    result.append(namedElement.getName());
                    result.append(",");
                }
            }
            result.deleteCharAt(result.length() - 1);
            result.append(">");
        } else {
            Collection<COREMapping<? extends EObject>> mappings 
                = COREArtefactUtil.getNextReferencedMappings(element);
            
            if (mappings != null) {
                result.append("<");
                for (COREMapping<? extends EObject> refMapping : mappings) {
                    NamedElement namedElement = (NamedElement) refMapping.getTo();
                    result.append(namedElement.getName());
                    result.append(",");
                }
                result.deleteCharAt(result.length() - 1);
                result.append(">");
            }
        }
        return result.toString();
    }
    
    /**
     * Gets the use cases that are sub use cases of a given use case.
     * @param parentUseCase The parent use case.
     * @return The child use cases.
     */
    public static List<UseCase> getSubUseCases(UseCase parentUseCase) {
        UseCaseModel ucm = (UseCaseModel) parentUseCase.eContainer();
        List<UseCase> useCases = new ArrayList<UseCase>();
        for (UseCase uc : ucm.getUseCases()) {
            if (parentUseCase.equals(uc.getGeneralization())) {
                useCases.add(uc);
            }
        }
        
        return useCases;
    }
    
    /**
     * Gets the steps between to steps in a range.
     * Throws an exception if the steps do not belong to the same flow.
     * @param parentFlow The parent flow.
     * @param rangeStart The step to start the range. 
     * @param rangeEnd The step to end the range.
     * @param includeRangeEnds Flag to include the range ends.
     * @return The range of steps.
     */
    public static List<Step> getStepsBetween(Flow parentFlow, Step rangeStart, Step rangeEnd, boolean includeRangeEnds) {
        if (rangeStart != null && rangeEnd != null && 
                !Objects.equals(rangeStart.eContainer(), rangeEnd.eContainer())) {
            throw new IllegalArgumentException();
        }
        
        List<Step> steps = new ArrayList<Step>();
        boolean inRange = rangeStart == null;
        
        for (Step s : parentFlow.getSteps()) {            
            if (Objects.equals(s, rangeStart)) {
                inRange = true;
                if (includeRangeEnds) {
                    steps.add(s);
                }
            } else if (Objects.equals(s, rangeEnd)) {
                if (includeRangeEnds) {
                    steps.add(s);
                }
                break;
            } else if (inRange) {
                steps.add(s);
            }
        }
        
        return steps;
    }

    /**
     * Finds a matching flow by name.
     * @param useCase The use case to search.
     * @param name The name.
     * @return
     */
    public static Flow getFlowByName(UseCase useCase, String name) {
        if (useCase.getMainSuccessScenario() != null && useCase.getMainSuccessScenario().getName().equals(name)) {
            return useCase.getMainSuccessScenario();
        }
        
        if (useCase.getMainSuccessScenario() != null) {
            for (Flow f : useCase.getMainSuccessScenario().getAlternateFlows()) {
                if (f.getName().equals(name)) {
                    return f;
                }
            }
        }        
        
        return null;
    }
    
    /**
     * Creates a new layout for a given {@link UseCaseDiagram}.
     * The layout is the {@link ca.mcgill.sel.ram.impl.ContainerMapImpl} specifically
     * that holds all {@link LayoutElement} for children of the given {@link UseCaseDiagram}.
     *
     * @param cd the {@link UseCaseDiagram} holding the {@link LayoutElement} for its children
     */
    private static void createLayout(UseCaseModel ucd) {
        Layout layout = UcFactory.eINSTANCE.createLayout();

        // workaround used here since creating the map, adding the values and then putting it doesn't work
        // EMF somehow does some magic with the passed map instance
        layout.getContainers().put(ucd, new BasicEMap<EObject, LayoutElement>());

        ucd.setLayout(layout);
    }
    
    /**
     * Determines if a step can be extended (i.e. if an alternate flow can reference it).
     * @param step The step.
     * @return True if the step can be extended.
     */
    private static boolean isExtendableStep(Step step) {
        if (step instanceof CommunicationStep || step instanceof UseCaseReferenceStep) {
            return true;
        } else if (step instanceof ContextStep) {
            ContextStep contextStep = (ContextStep) step;
            return contextStep.getType() == ContextType.INTERNAL;
        }
        
        return false;
    }
    
    /**
     * Used to limit the search depth (since currently no logic prevents infinite loops).
     * @param useCase The use case.
     * @param The previously searched use cases.
     * @return The actors.
     */
    @SuppressWarnings("unchecked")
    private static Set<Actor> getAvailableActors(UseCase useCase, HashSet<UseCase> seenUseCases) {
        // Don't search the same use case twice
        if (seenUseCases.contains(useCase)) {
            return new HashSet<Actor>();
        }
        
        seenUseCases.add(useCase);
        
        Set<Actor> actors = new HashSet<Actor>(useCase.getPrimaryActors());
        actors.addAll(useCase.getSecondaryActors());
        
        List<UseCase> useCasesToProcess = new ArrayList<UseCase>();
        
        // Add the generalization and extended use cases to be checked
        if (useCase.getGeneralization() != null) {
            useCasesToProcess.add(useCase.getGeneralization());
        }
        
        if (useCase.getExtendedUseCase() != null) {
            useCasesToProcess.add(useCase.getExtendedUseCase());
        }
        
        // If the use case is a subfunction or user-goal, we can also reference any actors in any use case
        // that INCLUDES this use case (to avoid having excessive links in the diagram).
        // TODO: review        
        if (useCase.getLevel() != Level.SUMMARY) {
            useCasesToProcess.addAll(getUseCasesIncluding(useCase));
        }
        
        for (EObject o : COREModelUtil.collectModelElementsFor(useCase)) {
            useCasesToProcess.add((UseCase) o);
        }
        
        for (UseCase uc : useCasesToProcess) {
            actors.addAll(getAvailableActors(uc, seenUseCases));
        }
        
        return actors;
    }

    @Override
    public EObject createNewEmptyModel(String name) {
        return createUseCaseDiagram(name);
    }
}
