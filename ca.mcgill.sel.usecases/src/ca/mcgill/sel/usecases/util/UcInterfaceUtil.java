package ca.mcgill.sel.usecases.util;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.COREVisibilityType;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.core.util.COREModelUtil;
import ca.mcgill.sel.usecases.Actor;
import ca.mcgill.sel.usecases.Step;
import ca.mcgill.sel.usecases.UseCase;
import ca.mcgill.sel.usecases.UseCaseModel;

/**
 * Utility class with helper methods to retrieve available properties of a given model.
 * This reflects the available properties as per usage and customization defined by CORE.
 * @author rlanguay
 *
 */
public final class UcInterfaceUtil {
    private UcInterfaceUtil() {
        
    }
    
    /**
     * Gets the actors available for selection in the current artefact and all external artefacts.
     * @param currentObject The current artefact to check.
     * @return The set of available actors.
     */
    public static Collection<Actor> getAvailableActors(EObject currentObject) {
        Collection<Actor> result = getAvailableExternalActors(currentObject);

        //add the classes of the current model
        COREExternalArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(currentObject);
        
        UseCaseModel ucm = (UseCaseModel) artefact.getRootModelElement();
        result.addAll(ucm.getActors());

        return result;
    }
    
    /**
     * Gets the actors available for selection in all external artefacts.
     * @param currentObject The current artefact to check.
     * @return The set of available actors.
     */
    public static Collection<Actor> getAvailableExternalActors(EObject currentObject) {
     // Get the COREExternalArtefact that potentially contains extensions and reuses, and then get
        // all the COREExternalArtefacts of the extended models
        COREExternalArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(currentObject);
        Collection<COREExternalArtefact> extendedArtefacts = COREModelUtil.getExtendedArtefacts(artefact);
        
        // From every extended artefacts, gather the actors
        Collection<Actor> actorsResult = new HashSet<>();
        for (COREExternalArtefact externalArtefact : extendedArtefacts) {
            UseCaseModel ucm = (UseCaseModel) externalArtefact.getRootModelElement();
            actorsResult.addAll(ucm.getActors());
        }
        
        // Get all the COREExternalArtefacts that are reused
        Collection<COREExternalArtefact> reusedArtefacts = COREModelUtil.getReusedArtefacts(artefact);
        

        // From every reused artefact, gather the actors if they are public
        for (COREExternalArtefact externalArtefact : reusedArtefacts) {
            UseCaseModel ucm = (UseCaseModel) externalArtefact.getRootModelElement();
            for (Actor a : ucm.getActors()) {
                if (a.getVisibility() == COREVisibilityType.PUBLIC) {
                    actorsResult.add(a);    
                }
            }
        }
        
        Set<COREModelComposition> allCompositions = COREModelUtil.collectAllModelCompositions(artefact);        
        COREModelUtil.filterMappedElements(actorsResult, allCompositions);

        return actorsResult;
    }
    
    /**
     * Gets the use cases available for selection in the current artefact and all external artefacts.
     * @param currentObject The current artefact to check.
     * @return The set of available use cases.
     */
    public static Collection<UseCase> getAvailableUseCases(EObject currentObject) {
        Collection<UseCase> result = getAvailableExternalUseCases(currentObject);

        //add the classes of the current model
        COREExternalArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(currentObject);
        
        UseCaseModel ucm = (UseCaseModel) artefact.getRootModelElement();
        result.addAll(ucm.getUseCases());

        return result;
    }
    
    /**
     * Gets the use cases available for selection in all external artefacts.
     * @param currentObject The current artefact to check.
     * @return The set of available use cases.
     */
    public static Collection<UseCase> getAvailableExternalUseCases(EObject currentObject) {
     // Get the COREExternalArtefact that potentially contains extensions and reuses, and then get
        // all the COREExternalArtefacts of the extended models
        COREExternalArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(currentObject);
        Collection<COREExternalArtefact> extendedArtefacts = COREModelUtil.getExtendedArtefacts(artefact);
        
        // From every extended artefacts, gather the actors
        Collection<UseCase> useCasesResult = new HashSet<>();
        for (COREExternalArtefact externalArtefact : extendedArtefacts) {
            UseCaseModel ucm = (UseCaseModel) externalArtefact.getRootModelElement();
            useCasesResult.addAll(ucm.getUseCases());
        }
        
        // Get all the COREExternalArtefacts that are reused
        Collection<COREExternalArtefact> reusedArtefacts = COREModelUtil.getReusedArtefacts(artefact);
        

        // From every reused artefact, gather the actors if they are public
        for (COREExternalArtefact externalArtefact : reusedArtefacts) {
            UseCaseModel ucm = (UseCaseModel) externalArtefact.getRootModelElement();
            for (UseCase u : ucm.getUseCases()) {
                if (u.getVisibility() == COREVisibilityType.PUBLIC) {
                    useCasesResult.add(u);    
                }
                
            }
        }
        
        Set<COREModelComposition> allCompositions = COREModelUtil.collectAllModelCompositions(artefact);        
        COREModelUtil.filterMappedElements(useCasesResult, allCompositions);

        return useCasesResult;
    }
    
    /**
     * Gets the steps available for selection in the current artefact.
     * If the artefact is a use case, returns the steps associated to this use case.
     * Otherwise, returns all steps in all available use cases.
     * @param currentObject The current artefact to check.
     * @return The set of available steps.
     */
    public static Collection<Step> getAvailableSteps(EObject currentObject) {
        Collection<Step> steps = new HashSet<>();
        
        if (currentObject instanceof UseCase) {
            UseCase specificUseCase = (UseCase) currentObject;
            steps.addAll(UcModelUtil.getSteps(specificUseCase));
        } else {
            Collection<UseCase> useCases = getAvailableUseCases(currentObject);
            for (UseCase currentUseCase : useCases) {
                steps.addAll(UcModelUtil.getSteps(currentUseCase));
            }            
        }
        
        return steps;
    }
}
