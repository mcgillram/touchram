package ca.mcgill.sel.usecases.util;

/**
 * The possible types of use case preconditions.
 * @author rlanguay
 *
 */
public enum UseCasePreconditionType {
    /**
     * A validation of internal system state.
     * Maps to an internal context step.
     */
    SYSTEM_STATE_VALIDATION,
    
    /**
     * An input from an actor.
     * Maps to an input communication step.
     */
    INPUT,
    
    /**
     * A timeout.
     * Maps to a timeout context step.
     */
    TIMEOUT
}
