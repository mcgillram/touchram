package ca.mcgill.sel.usecases.util;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import ca.mcgill.sel.usecases.Actor;
import ca.mcgill.sel.usecases.ActorReferenceText;
import ca.mcgill.sel.usecases.AnythingStep;
import ca.mcgill.sel.usecases.ConclusionType;
import ca.mcgill.sel.usecases.ContextStep;
import ca.mcgill.sel.usecases.ContextType;
import ca.mcgill.sel.usecases.ExtensionPoint;
import ca.mcgill.sel.usecases.Flow;
import ca.mcgill.sel.usecases.Step;
import ca.mcgill.sel.usecases.UseCase;
import ca.mcgill.sel.usecases.UseCaseModel;

/**
 * Utility methods for getting the texts associated with use case artifacts.
 * @author rlanguay
 *
 */
public class UseCaseTextUtils {    
    private static final String FAILURE_TEXT = "Use case ends in failure.";
    private static final String SUCCESS_TEXT = "Use case ends in success.";
    private static final String ABANDON_TEXT = "Use case ends in abandon.";
    private static final String INTERRUPT_TEXT = "Use case continues where it was interrupted.";
    private static final String STEP_TEXT = "Use case continues at step %s";
    private static final String STEP_NUMBER_SUFFIX = ".";
    public static final String ACTOR_TOKEN = "__%d__";
    public static final String ELLIPSES = "...";
    /**
     * Gets the display index of a step, based on its position in the flow.
     * @param flow The flow.
     * @param step The step.
     * @return The step number to display.
     */
    public static String getStepDisplayIndex(Flow flow, Step step) {
        if (isUnindexed(step, flow)) {
            return "";
        }
        
        int index = 1;
        for (Step s : flow.getSteps()) {
            if (s == step) {
                return Integer.toString(index);
            } else if (isUnindexed(s, flow)) {
                continue;
            } else {
                index++;
            }
        }
        
        throw new IndexOutOfBoundsException();
    }
    
    /**
     * Determines whether a step should be completely unnumbered (i.e. no display text at all).
     * * Examples where this is not wanted:
     * - a step is of type external context, control flow, or extension point
     * @param step The step.
     * @return True is the step should not display any text.
     */
    private static boolean isUnnumbered(Step step) {
        return step instanceof ContextStep && ((ContextStep) step).getType() != ContextType.INTERNAL
                || step instanceof ExtensionPoint || step instanceof AnythingStep;
    }

    /**
     * Helper to determine if a step should be assigned a number based on its position.
     * Examples where this is not wanted:
     * - the step is unnumbered
     * - The step is the first step in an alternate flow
     * @param step The step
     * @param flow The containing flow
     * @return True if the step should not be assigned a number.
     */
    private static boolean isUnindexed(Step step, Flow flow) {
        return isUnnumbered(step)
                || Objects.equals(step, flow.getPreconditionStep());
    }

    /**
     * Gets the text to display for a particular step, depending on the type.
     * @param flow The flow.
     * @param step The step.
     * @return The text to display in the step's view.
     */
    public static String getStepText(Flow flow, Step step) {
        String stepIndex = getStepDisplayIndex(flow, step);
        String result = replaceActorTokens(step.getStepDescription());
        
        return String.format("%s. %s", stepIndex, result);
    }
    
    /**
     * Gets the text used to sort steps in a flow.
     * This essentially prepends chars to ensure that numerical ordering is respected.
     * The text itself is never seen.
     * @param flow The flow.
     * @param step The step.
     * @return The sorting text.
     */
    public static String getSortingText(Flow flow, Step step) {
        int stepIndex = flow.getSteps().indexOf(step) + 1;
        String stepIndexString = Integer.toString(stepIndex);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < stepIndexString.length(); i++) {
            sb.append("z");
        }
        
        sb.append(stepIndexString);
        
        return sb.toString();
    }

    /**
     * Gets the text to display to show the steps associated to an extension flow.
     * @param flow The extension flow.
     * @return The step index text to display.
     */
    public static String getFlowStepNumberText(Flow flow) {
        if (flow.eContainer() instanceof Flow) {
            Flow parentFlow = (Flow) flow.eContainer();
            String flowText = String.format("%s%s", 
                    getFlowStepIndexText(flow, parentFlow), getExtensionFlowLetter(flow, parentFlow));
            if (!"".equals(flowText)) {
                flowText += STEP_NUMBER_SUFFIX;
            }
            
            return String.format("%s%s", getFlowStepNumberText((Flow) flow.eContainer()), flowText);
        } else {
            return "";
        } 
    }
    
    /**
     * Gets the text to display to show the steps associated to an extension flow.
     * Use this overload when the parent is NOT KNOWN (i.e. a flow hasn't been added yet).
     * @param flow The extension flow.
     * @return The step index text to display.
     */
    public static String getFlowStepNumberText(Flow flow, Flow parentFlow) {
        String flowText = String.format("%s%s", 
                getFlowStepIndexText(flow, parentFlow), getExtensionFlowLetter(flow, parentFlow));
        if (!"".equals(flowText)) {
            flowText += STEP_NUMBER_SUFFIX;
        }
        
        return String.format("%s%s", getFlowStepNumberText(parentFlow), flowText);
    }
    
    /**
     * Gets the complete step number for a step.
     * @param step The step.
     * @return The step number.
     */
    public static String getStepNumber(Step step) {
        Flow containingFlow = (Flow) step.eContainer();
        String flowNumber = getFlowStepNumberText(containingFlow);
        String stepNumber = getStepDisplayIndex(containingFlow, step); 
        if (!"".equals(stepNumber)) {
            stepNumber += STEP_NUMBER_SUFFIX;
        }
        
        // If the step doesn't have a display index, don't show anything
        if (isUnnumbered(step)) {
            return "";
        } else {
            return flowNumber + stepNumber;
        }
    }
    
    /**
     * Gets the short display for a step.
     * @param step The step.
     * @return A short string representing the step.
     */
    public static String getStepShortDisplay(Step step) {
        Flow containingFlow = (Flow) step.eContainer();
        String flowNumber = getFlowStepNumberText(containingFlow);
        String stepNumber = getStepDisplayIndex(containingFlow, step);
        
        if ("".equals(stepNumber)) {
            stepNumber = "[" + Integer.toString(containingFlow.getSteps().indexOf(step)) + "]";
        }
        
        return flowNumber + stepNumber;
    }
    
    /**
     * Get the text that appears before an alternate flow. This is a function of the referenced steps of the flow.
     * @param flow The flow to check.
     * @param parentFlow The flow's parent flow.
     * @return The text.
     */
    public static String getFlowStepIndexText(Flow flow, Flow parentFlow) {
        List<Step> referencedSteps = flow.getReferencedSteps();
        if (referencedSteps.size() == 1) {
            return getStepDisplayIndex(parentFlow, referencedSteps.get(0));
        } else if (referencedSteps.size() > 1) {
            Flow f = parentFlow;
            List<Integer> stepIndexes = referencedSteps
                    .stream()
                    .map(step -> Integer.parseInt(getStepDisplayIndex(f, step)))
                    .sorted()
                    .collect(Collectors.toList());
            
            List<String> results = new ArrayList<String>();
            int i = 0;
            int rangeStart = 0;
            int rangeEnd = 0;
            
            while (i < stepIndexes.size()) {
                int currentStepIndex = stepIndexes.get(i);
                if (rangeStart == 0) {
                    // Starting a new potential range
                    rangeStart = currentStepIndex;
                } else if ((currentStepIndex == rangeStart + 1 && rangeEnd == 0)
                        || currentStepIndex == rangeEnd + 1) {
                    // Continuing an existing range
                    rangeEnd = currentStepIndex;
                } else {
                    // Current range has ended
                    if (rangeEnd == 0) {
                        results.add(Integer.toString(rangeStart));
                    } else {
                        results.add(String.format("%d-%d", rangeStart, rangeEnd));
                    }
                    
                    rangeStart = currentStepIndex;
                    rangeEnd = 0;
                }
                
                i++;
            }
            
            if (rangeStart != 0) {
                if (rangeEnd == 0) {
                    results.add(Integer.toString(rangeStart));
                } else {
                    results.add(String.format("%d-%d", rangeStart, rangeEnd));
                }
            }
            
            return String.format("(%s)", String.join(", ", results));
        } else {
            // Must be a global flow; put an artificial range of all steps
            String firstDisplayIndex = null;          
            String lastDisplayIndex = null;
            for (Step step : parentFlow.getSteps()) {
                String stepDisplayIndex = getStepDisplayIndex(parentFlow, step);
                if (firstDisplayIndex == null && stepDisplayIndex != "") {
                    firstDisplayIndex = stepDisplayIndex;
                }
                
                if (stepDisplayIndex != "") {
                    lastDisplayIndex = stepDisplayIndex;
                }
            }
            
            if (firstDisplayIndex == null && lastDisplayIndex == null) {
                return "";
            } else if (firstDisplayIndex == lastDisplayIndex) {
                return String.format("(%s)", firstDisplayIndex);
            } else {
                return String.format("(%s-%s)", firstDisplayIndex, lastDisplayIndex);    
            }            
        }
    }
    
    private static String getExtensionFlowLetter(Flow flow, Flow parentFlow) {
        Map<Flow, String> flowToStepExpressionMap = parentFlow.getAlternateFlows()
                .stream()
                .collect(Collectors.toMap(f -> f, 
                    f -> getFlowStepIndexText(f, parentFlow)));
        
        // SPECIAL CASE: this can be called when a flow is not yet part of the alternates
        // In this case, we should return the next letter in the alphabet
        if (!parentFlow.getAlternateFlows().contains(flow)) {
            String newFlowStepExpression = getFlowStepIndexText(flow, parentFlow);
            long numMatches = flowToStepExpressionMap.values()
                    .stream()
                    .filter(c -> c.equals(newFlowStepExpression))
                    .count();
            return getLetterForInteger((int)numMatches);
        }
        
        String currentFlowExpression = flowToStepExpressionMap.get(flow);
        int letter = 1;
        for (Flow f : parentFlow.getAlternateFlows()) {
            if (f == flow) {
                break;
            } else if (flowToStepExpressionMap.get(f).equals(currentFlowExpression)) {
                letter++;
            }
        }
        
        return getLetterForInteger(letter);
    }

    private static String getLetterForInteger(int letter) {
        if (letter > 0 && letter < 27) {
            return String.valueOf((char) (letter + 96));
        } else if (letter >= 27) {
            // In this case we should be doubling up
            int repetitions = letter / 26;
            int singleLetter = (letter % 26) + 1;
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < repetitions; i++) {
                sb.append(getLetterForInteger(singleLetter));
            }
            
            return sb.toString();
        } else {
            return "";
        }
    }
    
    /**
     * Gets the text to display for the different conclusion types.
     * Used in the conclusion selector.
     * @param conclusionType The conclusion type.
     * @return The text.
     */
    public static String getConclusionTypeText(ConclusionType conclusionType) {
        String result = "";
        switch (conclusionType) {
            case FAILURE:
                result = FAILURE_TEXT;
                break;
            case STEP:
                result = String.format(STEP_TEXT, ELLIPSES);
                break;
            case SUCCESS:
                result = SUCCESS_TEXT;
                break;
            case ABANDON:
                result = ABANDON_TEXT;
                break;
            case WHERE_INTERRUPTED:
                result = INTERRUPT_TEXT;
                break;
        }
        
        return result;
    }

    /**
     * Gets the text to display for the conclusion of a flow.
     * Used in the actual conclusion view.
     * @param flow The flow.
     * @return The text.
     */
    public static String getConclusionTypeText(Flow flow) {
        String result = "";
        switch (flow.getConclusionType()) {
            case FAILURE:
                result = FAILURE_TEXT;
                break;
            case STEP:
                if (flow.getConclusionStep() != null) {
                    result = String.format(STEP_TEXT, getStepNumber(flow.getConclusionStep()));
                } else {
                    result = String.format(STEP_TEXT, ELLIPSES);
                }
                break;                
            case SUCCESS:
                result = SUCCESS_TEXT;
                break;
            case ABANDON:
                result = ABANDON_TEXT;
                break;
            case WHERE_INTERRUPTED:
                result = INTERRUPT_TEXT;
                break;
        }
        
        return result;
    }
    
    /**
     * Parses actor names from a string, and returns the tokenized string.
     * @param text The text to parse.
     * @param useCase The use case that contains this text.
     * @param referencedActors The list of referenced actors (will be populated).
     * @return The tokenized string (and the populated list of actors).
     */
    public static String parseActorNames(String text, UseCase useCase, List<Actor> referencedActors) {
        UseCaseModel localRoot = (UseCaseModel) useCase.eContainer();
        
        List<Actor> actors = new ArrayList<Actor>(UcModelUtil.getAvailableActors(useCase));
        
        // Order actors by name length, to find longer matches first
        // But also, favour local actors over non-local ones
        actors.sort(new Comparator<Actor>() {
            @Override
            public int compare(Actor a, Actor b) {
                if (a.eContainer() != b.eContainer()) {
                    if (a.eContainer() == localRoot) {
                        return -1;
                    } else if (b.eContainer() == localRoot) {
                        return 1;
                    }
                }
                
                return Integer.compare(b.getName().length(), a.getName().length());
            }
        });
        
        int currentIndex = 0;
        for (Actor actor : actors) {
            String textLower = text.toLowerCase();
            if (actor.getName().contains(" ")) {
                // Multi-word actor name; can be 
                if (textLower.contains(actor.getName().toLowerCase())) {
                    Pattern pattern = Pattern.compile(actor.getName(), Pattern.CASE_INSENSITIVE);
                    text = pattern.matcher(text).replaceAll(String.format(ACTOR_TOKEN, currentIndex));
                    referencedActors.add(actor);
                    currentIndex++;
                }
            } else {
                Pattern pattern = Pattern.compile("\\b" + actor.getName() + "\\b", Pattern.CASE_INSENSITIVE);
                Matcher matcher = pattern.matcher(text);
                if (matcher.find()) {
                    text = pattern.matcher(text).replaceAll(String.format(ACTOR_TOKEN, currentIndex));
                    referencedActors.add(actor);
                    currentIndex++;
                }                
            }            
        }
        
        return text;
    }
    
    /**
     * Replaces the actor tokens in an actor reference text.
     * @param text The actor reference text object.
     * @return The display-friendly text.
     */
    public static String replaceActorTokens(ActorReferenceText text) {
        if (text == null) {
            return "";
        }
        
        int i = 0;
        String currentText = text.getText();
        for (Actor actor : text.getActorReferences()) {
            currentText = currentText.replace(String.format(ACTOR_TOKEN, i), actor.getName());
            i++;
        }        
        
        return currentText;
    }
}
