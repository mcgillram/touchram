/**
 */
package ca.mcgill.sel.usecases;

import ca.mcgill.sel.core.COREMapping;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Use Case Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.mcgill.sel.usecases.UcPackage#getUseCaseMapping()
 * @model
 * @generated
 */
public interface UseCaseMapping extends COREMapping<UseCase> {
	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @model kind="operation"
     * @generated
     */
	EList<FlowMapping> getFlowMappings();

} // UseCaseMapping
