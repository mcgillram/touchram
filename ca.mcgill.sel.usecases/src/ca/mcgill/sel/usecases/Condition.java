/**
 */
package ca.mcgill.sel.usecases;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Condition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.usecases.Condition#getText <em>Text</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.Condition#getConditionText <em>Condition Text</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.Condition#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.usecases.UcPackage#getCondition()
 * @model
 * @generated
 */
public interface Condition extends EObject {
    /**
     * Returns the value of the '<em><b>Text</b></em>' attribute.
     * The default value is <code>""</code>.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Text</em>' attribute.
     * @see #setText(String)
     * @see ca.mcgill.sel.usecases.UcPackage#getCondition_Text()
     * @model default=""
     * @generated
     */
    String getText();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.usecases.Condition#getText <em>Text</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Text</em>' attribute.
     * @see #getText()
     * @generated
     */
    void setText(String value);

    /**
     * Returns the value of the '<em><b>Condition Text</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Condition Text</em>' containment reference.
     * @see #setConditionText(ActorReferenceText)
     * @see ca.mcgill.sel.usecases.UcPackage#getCondition_ConditionText()
     * @model containment="true" required="true"
     * @generated
     */
    ActorReferenceText getConditionText();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.usecases.Condition#getConditionText <em>Condition Text</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Condition Text</em>' containment reference.
     * @see #getConditionText()
     * @generated
     */
    void setConditionText(ActorReferenceText value);

    /**
     * Returns the value of the '<em><b>Type</b></em>' attribute.
     * The literals are from the enumeration {@link ca.mcgill.sel.usecases.ConditionType}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Type</em>' attribute.
     * @see ca.mcgill.sel.usecases.ConditionType
     * @see #setType(ConditionType)
     * @see ca.mcgill.sel.usecases.UcPackage#getCondition_Type()
     * @model
     * @generated
     */
    ConditionType getType();

    /**
     * Sets the value of the '{@link ca.mcgill.sel.usecases.Condition#getType <em>Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Type</em>' attribute.
     * @see ca.mcgill.sel.usecases.ConditionType
     * @see #getType()
     * @generated
     */
    void setType(ConditionType value);

} // Condition
