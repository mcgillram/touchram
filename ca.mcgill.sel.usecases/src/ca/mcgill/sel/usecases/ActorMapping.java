/**
 */
package ca.mcgill.sel.usecases;

import ca.mcgill.sel.core.COREMapping;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Actor Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.mcgill.sel.usecases.UcPackage#getActorMapping()
 * @model
 * @generated
 */
public interface ActorMapping extends COREMapping<Actor> {
} // ActorMapping
