/**
 */
package ca.mcgill.sel.usecases.impl;

import ca.mcgill.sel.core.impl.COREMappingImpl;

import ca.mcgill.sel.usecases.FlowMapping;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.UseCase;
import ca.mcgill.sel.usecases.UseCaseMapping;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Use Case Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class UseCaseMappingImpl extends COREMappingImpl<UseCase> implements UseCaseMapping {
	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected UseCaseMappingImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return UcPackage.Literals.USE_CASE_MAPPING;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<FlowMapping> getFlowMappings() {
        // TODO: implement this method
        // Ensure that you remove @generated or mark it @generated NOT
        throw new UnsupportedOperationException();
    }

} //UseCaseMappingImpl
