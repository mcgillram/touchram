/**
 */
package ca.mcgill.sel.usecases.impl;

import ca.mcgill.sel.usecases.ExtensionPoint;
import ca.mcgill.sel.usecases.UcPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Extension Point</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ExtensionPointImpl extends StepImpl implements ExtensionPoint {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected ExtensionPointImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return UcPackage.Literals.EXTENSION_POINT;
    }

} //ExtensionPointImpl
