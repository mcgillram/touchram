/**
 */
package ca.mcgill.sel.usecases.impl;

import ca.mcgill.sel.usecases.ActorReferenceText;
import ca.mcgill.sel.usecases.Step;
import ca.mcgill.sel.usecases.UcPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Step</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.usecases.impl.StepImpl#getStepText <em>Step Text</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.impl.StepImpl#getStepDescription <em>Step Description</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class StepImpl extends MappableElementImpl implements Step {
    /**
     * The default value of the '{@link #getStepText() <em>Step Text</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getStepText()
     * @generated
     * @ordered
     */
    protected static final String STEP_TEXT_EDEFAULT = "";
    /**
     * The cached value of the '{@link #getStepText() <em>Step Text</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getStepText()
     * @generated
     * @ordered
     */
    protected String stepText = STEP_TEXT_EDEFAULT;

    /**
     * The cached value of the '{@link #getStepDescription() <em>Step Description</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getStepDescription()
     * @generated
     * @ordered
     */
    protected ActorReferenceText stepDescription;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected StepImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return UcPackage.Literals.STEP;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String getStepText() {
        return stepText;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setStepText(String newStepText) {
        String oldStepText = stepText;
        stepText = newStepText;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, UcPackage.STEP__STEP_TEXT, oldStepText, stepText));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ActorReferenceText getStepDescription() {
        return stepDescription;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public NotificationChain basicSetStepDescription(ActorReferenceText newStepDescription, NotificationChain msgs) {
        ActorReferenceText oldStepDescription = stepDescription;
        stepDescription = newStepDescription;
        if (eNotificationRequired()) {
            ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, UcPackage.STEP__STEP_DESCRIPTION, oldStepDescription, newStepDescription);
            if (msgs == null) msgs = notification; else msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setStepDescription(ActorReferenceText newStepDescription) {
        if (newStepDescription != stepDescription) {
            NotificationChain msgs = null;
            if (stepDescription != null)
                msgs = ((InternalEObject)stepDescription).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - UcPackage.STEP__STEP_DESCRIPTION, null, msgs);
            if (newStepDescription != null)
                msgs = ((InternalEObject)newStepDescription).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - UcPackage.STEP__STEP_DESCRIPTION, null, msgs);
            msgs = basicSetStepDescription(newStepDescription, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, UcPackage.STEP__STEP_DESCRIPTION, newStepDescription, newStepDescription));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case UcPackage.STEP__STEP_DESCRIPTION:
                return basicSetStepDescription(null, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case UcPackage.STEP__STEP_TEXT:
                return getStepText();
            case UcPackage.STEP__STEP_DESCRIPTION:
                return getStepDescription();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case UcPackage.STEP__STEP_TEXT:
                setStepText((String)newValue);
                return;
            case UcPackage.STEP__STEP_DESCRIPTION:
                setStepDescription((ActorReferenceText)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case UcPackage.STEP__STEP_TEXT:
                setStepText(STEP_TEXT_EDEFAULT);
                return;
            case UcPackage.STEP__STEP_DESCRIPTION:
                setStepDescription((ActorReferenceText)null);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case UcPackage.STEP__STEP_TEXT:
                return STEP_TEXT_EDEFAULT == null ? stepText != null : !STEP_TEXT_EDEFAULT.equals(stepText);
            case UcPackage.STEP__STEP_DESCRIPTION:
                return stepDescription != null;
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (stepText: ");
        result.append(stepText);
        result.append(')');
        return result.toString();
    }

} //StepImpl
