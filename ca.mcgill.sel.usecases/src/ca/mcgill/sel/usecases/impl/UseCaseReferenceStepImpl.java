/**
 */
package ca.mcgill.sel.usecases.impl;

import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.UseCase;
import ca.mcgill.sel.usecases.UseCaseReferenceStep;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Use Case Reference Step</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.usecases.impl.UseCaseReferenceStepImpl#getUsecase <em>Usecase</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UseCaseReferenceStepImpl extends StepImpl implements UseCaseReferenceStep {
    /**
     * The cached value of the '{@link #getUsecase() <em>Usecase</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getUsecase()
     * @generated
     * @ordered
     */
    protected UseCase usecase;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected UseCaseReferenceStepImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return UcPackage.Literals.USE_CASE_REFERENCE_STEP;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public UseCase getUsecase() {
        if (usecase != null && usecase.eIsProxy()) {
            InternalEObject oldUsecase = (InternalEObject)usecase;
            usecase = (UseCase)eResolveProxy(oldUsecase);
            if (usecase != oldUsecase) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, UcPackage.USE_CASE_REFERENCE_STEP__USECASE, oldUsecase, usecase));
            }
        }
        return usecase;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public UseCase basicGetUsecase() {
        return usecase;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setUsecase(UseCase newUsecase) {
        UseCase oldUsecase = usecase;
        usecase = newUsecase;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, UcPackage.USE_CASE_REFERENCE_STEP__USECASE, oldUsecase, usecase));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case UcPackage.USE_CASE_REFERENCE_STEP__USECASE:
                if (resolve) return getUsecase();
                return basicGetUsecase();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case UcPackage.USE_CASE_REFERENCE_STEP__USECASE:
                setUsecase((UseCase)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case UcPackage.USE_CASE_REFERENCE_STEP__USECASE:
                setUsecase((UseCase)null);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case UcPackage.USE_CASE_REFERENCE_STEP__USECASE:
                return usecase != null;
        }
        return super.eIsSet(featureID);
    }

} //UseCaseReferenceStepImpl
