/**
 */
package ca.mcgill.sel.usecases.impl;

import ca.mcgill.sel.usecases.Actor;
import ca.mcgill.sel.usecases.ActorReferenceText;
import ca.mcgill.sel.usecases.UcPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Actor Reference Text</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.usecases.impl.ActorReferenceTextImpl#getText <em>Text</em>}</li>
 *   <li>{@link ca.mcgill.sel.usecases.impl.ActorReferenceTextImpl#getActorReferences <em>Actor References</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ActorReferenceTextImpl extends MinimalEObjectImpl.Container implements ActorReferenceText {
	/**
     * The default value of the '{@link #getText() <em>Text</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getText()
     * @generated
     * @ordered
     */
	protected static final String TEXT_EDEFAULT = null;

	/**
     * The cached value of the '{@link #getText() <em>Text</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getText()
     * @generated
     * @ordered
     */
	protected String text = TEXT_EDEFAULT;

	/**
     * The cached value of the '{@link #getActorReferences() <em>Actor References</em>}' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getActorReferences()
     * @generated
     * @ordered
     */
	protected EList<Actor> actorReferences;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected ActorReferenceTextImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return UcPackage.Literals.ACTOR_REFERENCE_TEXT;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String getText() {
        return text;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setText(String newText) {
        String oldText = text;
        text = newText;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, UcPackage.ACTOR_REFERENCE_TEXT__TEXT, oldText, text));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<Actor> getActorReferences() {
        if (actorReferences == null) {
            actorReferences = new EObjectResolvingEList<Actor>(Actor.class, this, UcPackage.ACTOR_REFERENCE_TEXT__ACTOR_REFERENCES);
        }
        return actorReferences;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case UcPackage.ACTOR_REFERENCE_TEXT__TEXT:
                return getText();
            case UcPackage.ACTOR_REFERENCE_TEXT__ACTOR_REFERENCES:
                return getActorReferences();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case UcPackage.ACTOR_REFERENCE_TEXT__TEXT:
                setText((String)newValue);
                return;
            case UcPackage.ACTOR_REFERENCE_TEXT__ACTOR_REFERENCES:
                getActorReferences().clear();
                getActorReferences().addAll((Collection<? extends Actor>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID) {
            case UcPackage.ACTOR_REFERENCE_TEXT__TEXT:
                setText(TEXT_EDEFAULT);
                return;
            case UcPackage.ACTOR_REFERENCE_TEXT__ACTOR_REFERENCES:
                getActorReferences().clear();
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID) {
            case UcPackage.ACTOR_REFERENCE_TEXT__TEXT:
                return TEXT_EDEFAULT == null ? text != null : !TEXT_EDEFAULT.equals(text);
            case UcPackage.ACTOR_REFERENCE_TEXT__ACTOR_REFERENCES:
                return actorReferences != null && !actorReferences.isEmpty();
        }
        return super.eIsSet(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (text: ");
        result.append(text);
        result.append(')');
        return result.toString();
    }

} //ActorReferenceTextImpl
