/**
 */
package ca.mcgill.sel.usecases.impl;

import ca.mcgill.sel.core.impl.COREMappingImpl;

import ca.mcgill.sel.usecases.Step;
import ca.mcgill.sel.usecases.StepMapping;
import ca.mcgill.sel.usecases.UcPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Step Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class StepMappingImpl extends COREMappingImpl<Step> implements StepMapping {
	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected StepMappingImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return UcPackage.Literals.STEP_MAPPING;
    }

} //StepMappingImpl
