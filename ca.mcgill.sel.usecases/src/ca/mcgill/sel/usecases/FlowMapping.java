/**
 */
package ca.mcgill.sel.usecases;

import ca.mcgill.sel.core.COREMapping;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Flow Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.mcgill.sel.usecases.UcPackage#getFlowMapping()
 * @model
 * @generated
 */
public interface FlowMapping extends COREMapping<Flow> {
	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @model kind="operation"
     * @generated
     */
	EList<StepMapping> getStepMappings();

} // FlowMapping
