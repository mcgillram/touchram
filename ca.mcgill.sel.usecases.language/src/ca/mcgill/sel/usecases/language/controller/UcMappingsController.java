package ca.mcgill.sel.usecases.language.controller;

import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.edit.command.DeleteCommand;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.COREMapping;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.usecases.ActorMapping;
import ca.mcgill.sel.usecases.FlowMapping;
import ca.mcgill.sel.usecases.StepMapping;
import ca.mcgill.sel.usecases.UcFactory;
import ca.mcgill.sel.usecases.UseCaseMapping;

/**
 * Controller for mappings in use case models.
 * @author rlanguay
 *
 */
public class UcMappingsController extends BaseController {
    /**
     * Creates a new instance of {@link UcMappingsController}.
     */
    protected UcMappingsController() {
        // Prevent anyone outside this package to instantiate.
    }
    
    /**
     * Creates a new mapping between actors.
     * 
     * @param modelComposition the {@link COREModelComposition} the mapping should be added to
     * @return {@link ActorMapping}
     */
    public ActorMapping createActorMapping(COREModelComposition modelComposition) {
        // create mapping
        ActorMapping mapping = UcFactory.eINSTANCE.createActorMapping();
        
        doAdd(modelComposition, CorePackage.Literals.CORE_MODEL_COMPOSITION__COMPOSITIONS, mapping);
        
        return mapping;
    }
    
    /**
     * Creates a new mapping between use cases.
     * 
     * @param modelComposition the {@link COREModelComposition} the mapping should be added to
     * @return {@link UseCaseMapping}
     */
    public UseCaseMapping createUseCaseMapping(COREModelComposition modelComposition) {
        // create mapping
        UseCaseMapping mapping = UcFactory.eINSTANCE.createUseCaseMapping();
        
        doAdd(modelComposition, CorePackage.Literals.CORE_MODEL_COMPOSITION__COMPOSITIONS, mapping);
        
        return mapping;
    }
    
    /**
     * Creates a new mapping between flows of mapped use cases.
     * 
     * @param useCaseMapping the {@link UseCaseMapping} the mapping should be added to
     * @return {@link StepMapping}
     */
    public FlowMapping createFlowMapping(UseCaseMapping useCaseMapping) {
        // create mapping
        FlowMapping mapping = UcFactory.eINSTANCE.createFlowMapping();
        
        doAdd(useCaseMapping, CorePackage.Literals.CORE_MAPPING__MAPPINGS, mapping);
        
        return mapping;
    }
    
    /**
     * Creates a new mapping between steps of mapped use cases.
     * 
     * @param flowMapping the {@link FlowMapping} the mapping should be added to
     * @return {@link StepMapping}
     */
    public StepMapping createStepMapping(FlowMapping flowMapping) {
        // create mapping
        StepMapping mapping = UcFactory.eINSTANCE.createStepMapping();
        
        doAdd(flowMapping, CorePackage.Literals.CORE_MAPPING__MAPPINGS, mapping);
        
        return mapping;
    }
    
    /**
     * Deletes the given mapping.
     * 
     * @param mapping the {@link Mapping} that should be deleted
     */
    public void deleteMapping(COREMapping<?> mapping) {
        EditingDomain domain = EMFEditUtil.getEditingDomain(mapping);
        CompoundCommand command = new CompoundCommand();
        
        // Unset the to element specifically to force a notification for the derived property.
        command.append(SetCommand.create(domain, mapping, CorePackage.Literals.CORE_LINK__TO, SetCommand.UNSET_VALUE));
        command.append(DeleteCommand.create(domain, mapping));
        
        doExecute(domain, command);
    }    
}
