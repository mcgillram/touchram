package ca.mcgill.sel.usecases.language.controller;

import java.util.List;

import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.controller.util.COREReferenceUtil;
import ca.mcgill.sel.usecases.Actor;
import ca.mcgill.sel.usecases.ActorReferenceText;
import ca.mcgill.sel.usecases.UcPackage;

public class ActorReferenceTextController extends BaseController {
    
    /**
     * Updates the actors and text for an actor reference text.
     * @param textObject The text object.
     * @param newText The new text string (with placeholders).
     * @param actorReferences The actor references.
     */
    public void updateActorReferenceText(ActorReferenceText textObject, String newText, List<Actor> actorReferences) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(textObject);
        CompoundCommand command = new CompoundCommand();
        
        textObject.getActorReferences().clear();
        
        for (Actor actor : actorReferences) {
            Actor localActor = COREReferenceUtil.localizeElement(editingDomain, command, textObject, actor);
            command.append(AddCommand.create(editingDomain, textObject, 
                    UcPackage.Literals.ACTOR_REFERENCE_TEXT__ACTOR_REFERENCES, localActor));
        }
        
        command.append(SetCommand.create(editingDomain, textObject,
                UcPackage.Literals.ACTOR_REFERENCE_TEXT__TEXT, newText));
        
        doExecute(editingDomain, command);
    }
}
