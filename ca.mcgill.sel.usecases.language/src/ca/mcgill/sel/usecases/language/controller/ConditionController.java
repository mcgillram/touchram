package ca.mcgill.sel.usecases.language.controller;

import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.usecases.ActorReferenceText;
import ca.mcgill.sel.usecases.Condition;
import ca.mcgill.sel.usecases.ConditionType;
import ca.mcgill.sel.usecases.UcFactory;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.UseCaseModel;

public class ConditionController extends BaseController {
    /**
     * Creates a new condition and associates it to a feature.
     * @param relatedObject The related object.
     * @param relatedFeature The condition text, if any.
     * @return The new condition.
     */
    public Condition createCondition(EObject relatedObject, String text, ConditionType type) {      
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(relatedObject);
        
        CompoundCommand command = new CompoundCommand();
        
        UseCaseModel useCaseModel = EMFModelUtil.getRootContainerOfType(
                relatedObject, UcPackage.Literals.USE_CASE_MODEL);
        
        Condition condition = UcFactory.eINSTANCE.createCondition();
        condition.setText(text);
        condition.setType(type);
        
        ActorReferenceText textObject = UcFactory.eINSTANCE.createActorReferenceText();
        textObject.setText(text);
        condition.setConditionText(textObject);
        
        command.append(AddCommand.create(editingDomain, useCaseModel, 
                UcPackage.Literals.USE_CASE_MODEL__CONDITIONS, condition));
        
        doExecute(editingDomain, command);
        
        return condition;
    }
    
    /**
     * Clears a condition from a feature.
     * @param relatedObject The related object.
     * @param relatedFeature The related feature.
     */
    public void updateCondition(EObject relatedObject, EStructuralFeature relatedFeature, Condition value) {
        doSet(relatedObject, relatedFeature, value);
    }
}
