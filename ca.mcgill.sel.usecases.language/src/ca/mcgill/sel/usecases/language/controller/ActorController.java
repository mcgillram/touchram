package ca.mcgill.sel.usecases.language.controller;

import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.COREPartialityType;
import ca.mcgill.sel.usecases.Actor;
import ca.mcgill.sel.usecases.MappableElement;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.UseCaseModel;
import ca.mcgill.sel.usecases.util.Multiplicity;

/**
 * Controller defining actions on use case actors
 * 
 * @author rlanguay
 *
 */
public class ActorController extends BaseController {

    /**
     * Sets the multiplicity of the given {@link Actor} according to the given lower and upper bound.
     *
     * @param ucd the current {@link UseCaseDiagram}
     * @param actor the actor for which the multiplicity is going to be set
     * @param multiplicity the new multiplicity
     */
    public void setMultiplicity(UseCaseModel ucd, Actor actor, Multiplicity multiplicity) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(actor);
        CompoundCommand compoundCommand;
        
        compoundCommand = new CompoundCommand();
        compoundCommand.append(SetCommand.create(editingDomain, actor,
                UcPackage.Literals.ACTOR__LOWER_BOUND, multiplicity.getLowerBound()));
        compoundCommand.append(SetCommand.create(editingDomain, actor,
                UcPackage.Literals.ACTOR__UPPER_BOUND, multiplicity.getUpperBound()));
        doExecute(editingDomain, compoundCommand);
    }
    
    /**
     * Set the generalization of an actor.
     * @param actor The actor.
     * @param parentActor The parent actor.
     */
    public void setGeneralization(Actor actor, Actor parentActor) {
        doSet(actor, UcPackage.Literals.ACTOR__GENERALIZATION, parentActor);
    }
    
    /**
     * Switch the abstract flag on an actor.
     * @param useCase The actor.
     */
    public void switchAbstract(Actor actor) {
        doSwitch(actor, UcPackage.Literals.ACTOR__ABSTRACT);
    }
    
    /**
     * Changes the partiality of a {@link MappableElement}.
     * 
     * @param owner - The actor we want the partiality to be changed
     * @param corePartiality - The new value of the classifier partiality
     */
    public void changeActorPartiality(Actor owner, COREPartialityType corePartiality) {
        changeMappableElementPartiality(owner, corePartiality);
    }
}
