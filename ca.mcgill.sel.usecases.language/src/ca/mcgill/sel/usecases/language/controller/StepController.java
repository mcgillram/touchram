package ca.mcgill.sel.usecases.language.controller;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.COREPartialityType;
import ca.mcgill.sel.core.controller.util.COREReferenceUtil;
import ca.mcgill.sel.usecases.LayoutElement;
import ca.mcgill.sel.usecases.MappableElement;
import ca.mcgill.sel.usecases.Step;
import ca.mcgill.sel.usecases.UcFactory;
import ca.mcgill.sel.usecases.UcPackage;
import ca.mcgill.sel.usecases.UseCase;
import ca.mcgill.sel.usecases.UseCaseModel;
import ca.mcgill.sel.usecases.UseCaseReferenceStep;

/**
 * Controller for operations on steps.
 * @author rlanguay
 *
 */
public class StepController extends BaseController {
    
    /**
     * Updates the use case references by a step.
     * @param step The step. 
     * @param useCase The use case.
     * @return The localized use case that is set.
     */
    public UseCase setUseCase(UseCaseReferenceStep step, UseCase useCase) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(step);
        
        CompoundCommand command = new CompoundCommand();
        
        // If this use case is in the "included" list, remove it
        UseCase containingUseCase = EMFModelUtil.getRootContainerOfType(step, UcPackage.Literals.USE_CASE);
        if (containingUseCase.getIncludedUseCases() != null && containingUseCase.getIncludedUseCases().contains(useCase)) {
            command.append(
                    RemoveCommand.create(editingDomain, containingUseCase, UcPackage.Literals.USE_CASE__INCLUDED_USE_CASES, useCase));
        }
        
        UseCase localValue = COREReferenceUtil.localizeElement(editingDomain, command, step, useCase);                
        if (localValue.getMainSuccessScenario() == null) {
            // Add the layout element as well
            LayoutElement layoutElement = UcFactory.eINSTANCE.createLayoutElement();
            UseCaseModel owner = EMFModelUtil.getRootContainerOfType(step, UcPackage.Literals.USE_CASE_MODEL);
            Command createElementMapCommand =
                    createAddLayoutElementCommand(editingDomain, owner, localValue, layoutElement);
            command.append(createElementMapCommand);
        }
        
        command.append(SetCommand.create(
                editingDomain, step, UcPackage.Literals.USE_CASE_REFERENCE_STEP__USECASE, localValue));       
        
        doExecute(editingDomain, command);
        
        return localValue;
    }
    
    /**
     * Changes the partiality of a {@link MappableElement}.
     * 
     * @param owner - The step we want the partiality to be changed
     * @param corePartiality - The new value of the classifier partiality
     */
    public void changeStepPartiality(Step owner, COREPartialityType corePartiality) {
        changeMappableElementPartiality(owner, corePartiality);
    }
}
