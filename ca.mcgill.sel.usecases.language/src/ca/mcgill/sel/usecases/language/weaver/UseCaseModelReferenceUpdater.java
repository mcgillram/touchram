package ca.mcgill.sel.usecases.language.weaver;

import java.util.List;

import org.eclipse.emf.ecore.util.Switch;

import ca.mcgill.sel.core.language.weaver.util.COREReferenceUpdater;
import ca.mcgill.sel.usecases.Actor;
import ca.mcgill.sel.usecases.ActorReferenceText;
import ca.mcgill.sel.usecases.Condition;
import ca.mcgill.sel.usecases.Flow;
import ca.mcgill.sel.usecases.Step;
import ca.mcgill.sel.usecases.UseCase;
import ca.mcgill.sel.usecases.UseCaseReferenceStep;
import ca.mcgill.sel.usecases.util.UcSwitch;

public class UseCaseModelReferenceUpdater extends COREReferenceUpdater {
    private class Switcher extends UcSwitch<Object> {        
        @Override
        public Object caseActor(Actor actor) {
            Actor generalization = actor.getGeneralization();
            if (currentWeavingInformation.wasWoven(generalization)) {
                actor.setGeneralization(currentWeavingInformation.getFirstToElement(generalization));
            }
            
            return Boolean.TRUE;            
        }
        
        @Override
        public Object caseUseCase(UseCase useCase) {
            // Actor references
            List<Actor> primaryActors = useCase.getPrimaryActors();
            for (int index = 0; index < primaryActors.size(); index++) {
                Actor primaryActor = primaryActors.get(index);
                if (currentWeavingInformation.wasWoven(primaryActor)) {
                    Actor newActor = currentWeavingInformation.getFirstToElement(primaryActor);
                    primaryActors.remove(index);
                    if (!primaryActors.contains(newActor)) {
                        primaryActors.add(index, newActor);
                    }
                }
            }
            
            List<Actor> secondaryActors = useCase.getSecondaryActors();
            for (int index = 0; index < secondaryActors.size(); index++) {
                Actor secondaryActor = secondaryActors.get(index);
                if (currentWeavingInformation.wasWoven(secondaryActor)) {
                    Actor newActor = currentWeavingInformation.getFirstToElement(secondaryActor);
                    secondaryActors.remove(index);
                    if (!secondaryActors.contains(newActor)) {
                        secondaryActors.add(index, newActor);
                    }
                }
            }
            
            List<Actor> facilitatorActors = useCase.getFacilitatorActors();
            for (int index = 0; index < facilitatorActors.size(); index++) {
                Actor facilitatorActor = facilitatorActors.get(index);
                if (currentWeavingInformation.wasWoven(facilitatorActor)) {
                    Actor newActor = currentWeavingInformation.getFirstToElement(facilitatorActor);
                    facilitatorActors.remove(index);
                    if (!facilitatorActors.contains(newActor)) {
                        facilitatorActors.add(index, newActor);
                    }
                }
            }
            
            // Use case references
            UseCase generalization = useCase.getGeneralization();
            if (currentWeavingInformation.wasWoven(generalization)) {
                useCase.setGeneralization(currentWeavingInformation.getFirstToElement(generalization));
            }
            
            List<UseCase> includedUseCases = useCase.getIncludedUseCases();
            for (int index = 0; index < includedUseCases.size(); index++) {
                UseCase includedUseCase = includedUseCases.get(index);
                if (currentWeavingInformation.wasWoven(includedUseCase)) {
                    UseCase newIncludedUseCase = currentWeavingInformation.getFirstToElement(includedUseCase);
                    includedUseCases.remove(index);
                    if (!includedUseCases.contains(newIncludedUseCase)) {
                        includedUseCases.add(index, newIncludedUseCase);
                    }
                }
            }
            
            // Conditions
            Condition selectionCondition = useCase.getSelectionCondition();
            if (currentWeavingInformation.wasWoven(selectionCondition)) {
                useCase.setSelectionCondition(currentWeavingInformation.getFirstToElement(selectionCondition));
            }
            
            return Boolean.TRUE;
        }
        
        @Override
        public Object caseFlow(Flow flow) {
            Step conclusionStep = flow.getConclusionStep();
            if (currentWeavingInformation.wasWoven(conclusionStep)) {
                flow.setConclusionStep(currentWeavingInformation.getFirstToElement(conclusionStep));
            }
            
            List<Step> referencedSteps = flow.getReferencedSteps();
            for (int index = 0; index < referencedSteps.size(); index++) {
                Step referencedStep = referencedSteps.get(index);
                if (currentWeavingInformation.wasWoven(referencedStep)) {
                    List<Step> newReferencedSteps = currentWeavingInformation.getToElements(referencedStep);
                    referencedSteps.remove(index);
                    for (Step newReferencedStep : newReferencedSteps) {
                        if (!referencedSteps.contains(newReferencedStep)) {
                            referencedSteps.add(index, newReferencedStep);
                        }   
                    }
                }
            }
            
            // Conditions
            Condition postCondition = flow.getPostCondition();
            if (currentWeavingInformation.wasWoven(postCondition)) {
                flow.setPostCondition(currentWeavingInformation.getFirstToElement(postCondition));
            }
            
            return Boolean.TRUE;
        }
        
        @Override
        public Object caseUseCaseReferenceStep(UseCaseReferenceStep step) {
            UseCase referencedUseCase = step.getUsecase();
            if (currentWeavingInformation.wasWoven(referencedUseCase)) {
                step.setUsecase(currentWeavingInformation.getFirstToElement(referencedUseCase));
            }
            
            return Boolean.TRUE;
        }
        
        @Override
        public Object caseActorReferenceText(ActorReferenceText text) {
            List<Actor> referencedActors = text.getActorReferences();
            for (int index = 0; index < referencedActors.size(); index++) {
                Actor referencedActor = referencedActors.get(index);
                if (currentWeavingInformation.wasWoven(referencedActor)) {
                    Actor newActor = currentWeavingInformation.getFirstToElement(referencedActor);
                    referencedActors.remove(index);
                    if (!referencedActors.contains(newActor)) {
                        referencedActors.add(index, newActor);
                    }
                }
            }
            
            return Boolean.TRUE;
        }
    }
    
    private static UseCaseModelReferenceUpdater instance;
        
    @Override
    protected Switch<?> createSwitcher() {
        return new Switcher();
    }
    
    public static UseCaseModelReferenceUpdater getInstance() {
        if (instance == null) {
            instance = new UseCaseModelReferenceUpdater();
        }
        
        return instance;
    }

}
