package ca.mcgill.sel.usecases.language.weaver;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.COREPartialityType;
import ca.mcgill.sel.core.language.weaver.COREModelWeaver;
import ca.mcgill.sel.core.language.weaver.COREWeaver;
import ca.mcgill.sel.core.language.weaver.WeaverException;
import ca.mcgill.sel.core.language.weaver.util.WeavingInformation;
import ca.mcgill.sel.usecases.Actor;
import ca.mcgill.sel.usecases.AnythingStep;
import ca.mcgill.sel.usecases.Condition;
import ca.mcgill.sel.usecases.Flow;
import ca.mcgill.sel.usecases.Step;
import ca.mcgill.sel.usecases.UseCase;
import ca.mcgill.sel.usecases.UseCaseModel;
import ca.mcgill.sel.usecases.util.UcModelUtil;
import ca.mcgill.sel.usecases.util.UseCaseTextUtils;

/**
 * Takes care of updating the references of elements of any {@link org.eclipse.emf.ecore.EObject} of use case models.
 * based on the weaving information provided by the use case model weaver.
 * @author rlanguay
 *
 */
public class UseCaseModelWeaver extends COREModelWeaver<UseCaseModel> {
    private static final String WEAVER_ANYTHINGSTEP_EXCEPTION = 
            "Error in weaving flow %s from use case %s: steps %s appear between mapped steps without an anything step.";
    @Override
    public UseCaseModel createEmptyModel() {
        UseCaseModel ucm = UcModelUtil.createUseCaseDiagram("newUseCaseModel");
        return ucm;
    }
    
    @Override
    public UseCaseModel weaveModel(COREExternalArtefact targetArtefact, UseCaseModel target,
            COREModelComposition modelComposition, WeavingInformation weavingInfo, boolean topdown) throws WeaverException {
        // Set the name of the woven model to the name of the target artefact
        COREExternalArtefact sourceArtefact = (COREExternalArtefact) modelComposition.getSource();
        target.setName(targetArtefact.getName());
        target.setSystemName(sourceArtefact.getCoreConcern().getName());
        
        UseCaseModel sourceUCM = (UseCaseModel) sourceArtefact.getRootModelElement();
        
        // First iterate over the use cases in the source and either merge or copy them
        for (Iterator<UseCase> iterator = sourceUCM.getUseCases().iterator(); iterator.hasNext(); ) {
            UseCase useCaseFromSource = iterator.next();
            
            System.out.print("Handling " + useCaseFromSource.getName() + ", which is ");
            if (weavingInfo.wasWoven(useCaseFromSource)) {
                // The use case was mapped, so we need to merge
                List<UseCase> mappedToTargetUseCases = weavingInfo.getToElements(useCaseFromSource);
                System.out.println(" mapped to: " + mappedToTargetUseCases);
                for (UseCase targetUseCase : mappedToTargetUseCases) {
                    if (topdown && useCaseFromSource.getPartiality() == COREPartialityType.NONE) {
                        targetUseCase.setName(useCaseFromSource.getName());
                        
                        if (useCaseFromSource.getUseCaseIntention() != null) {
                            targetUseCase.setUseCaseIntention(EcoreUtil.copy(useCaseFromSource.getUseCaseIntention()));
                        }
                        
                        if (useCaseFromSource.getUseCaseMultiplicity() != null) {
                            targetUseCase.setUseCaseMultiplicity(EcoreUtil.copy(useCaseFromSource.getUseCaseMultiplicity()));
                        }
                    }
                    
                    if ((targetUseCase.getUseCaseIntention() == null || targetUseCase.getUseCaseIntention().getText() == null) 
                            && useCaseFromSource.getUseCaseIntention() != null) {
                        targetUseCase.setUseCaseIntention(EcoreUtil.copy(useCaseFromSource.getUseCaseIntention()));
                    }
                    
                    if ((targetUseCase.getUseCaseMultiplicity() == null || targetUseCase.getUseCaseMultiplicity().getText() == null) 
                            && useCaseFromSource.getUseCaseMultiplicity() != null) {
                        targetUseCase.setUseCaseMultiplicity(EcoreUtil.copy(useCaseFromSource.getUseCaseMultiplicity()));
                    }
                    
                    if (targetUseCase.getMainSuccessScenario() == null) {
                        // This use case is a reference; we can just copy it as-is into the target.
                        targetUseCase.setUseCaseIntention(EcoreUtil.copy(useCaseFromSource.getUseCaseIntention()));
                        targetUseCase.setUseCaseMultiplicity(EcoreUtil.copy(useCaseFromSource.getUseCaseMultiplicity()));
                    }
                    
                    weaveFlows(targetUseCase, useCaseFromSource, weavingInfo, modelComposition, topdown);                      
                    
                    // Weave inheritance
                    // Although this code will set the supertype reference to point to the super use case in
                    // the source model, the reference updater will take care of changing the reference to
                    // the use case in the target model eventually
                    if (targetUseCase.getGeneralization() == null && useCaseFromSource.getGeneralization() != null) {
                        targetUseCase.setGeneralization(useCaseFromSource.getGeneralization());
                    }
                    
                    // Weave actor relationships
                    for (Actor a : useCaseFromSource.getPrimaryActors()) {
                        if (!weavingInfo.wasWoven(a) && !targetUseCase.getPrimaryActors().contains(a)) {
                            targetUseCase.getPrimaryActors().add(a);
                        }
                    }
                    
                    for (Actor a : useCaseFromSource.getSecondaryActors()) {
                        if (!weavingInfo.wasWoven(a) && !targetUseCase.getSecondaryActors().contains(a)) {
                            targetUseCase.getSecondaryActors().add(a);
                        }
                    }
                    
                    for (Actor a : useCaseFromSource.getFacilitatorActors()) {
                        if (!weavingInfo.wasWoven(a) && !targetUseCase.getFacilitatorActors().contains(a)) {
                            targetUseCase.getFacilitatorActors().add(a);
                        }
                    }
                }
            } else {
                // The use case was not mapped, so we can just copy it over
                System.out.println("not mapped.");
                UseCase modelElementCopy = COREWeaver.copyModelElement(useCaseFromSource, targetArtefact, 
                        weavingInfo, modelComposition);
                target.getUseCases().add(modelElementCopy);
            }
        }
        
        // Then do the same for the actors
        for (Iterator<Actor> iterator = sourceUCM.getActors().iterator(); iterator.hasNext(); ) {
            Actor actorFromSource = iterator.next();
            
            System.out.print("Handling " + actorFromSource.getName() + ", which is ");
            if (!weavingInfo.wasWoven(actorFromSource)) {
                // Copy the actor from the source to the target
                System.out.println("not mapped.");
                Actor modelElementCopy = COREWeaver.copyModelElement(actorFromSource, targetArtefact,
                        weavingInfo, modelComposition);
                target.getActors().add(modelElementCopy);
            } else {
                List<Actor> mappedToTargetActors = weavingInfo.getToElements(actorFromSource);
                System.out.println(" mapped to: " + mappedToTargetActors);
                
                for (Actor targetActor : mappedToTargetActors) {
                    if (targetActor.getGeneralization() == null && actorFromSource.getGeneralization() != null) {
                        targetActor.setGeneralization(actorFromSource.getGeneralization());
                    }
                }
            }
        }
        
        // Also add all conditions from the source
        for (Iterator<Condition> iterator = sourceUCM.getConditions().iterator(); iterator.hasNext(); ) {
            Condition conditionFromSource = iterator.next();
            
            System.out.print("Handling " + conditionFromSource.toString() + ", which is ");
            if (!weavingInfo.wasWoven(conditionFromSource)) {
                // Copy the condition from the source to the target
                System.out.println("not mapped.");
                Condition modelElementCopy = COREWeaver.copyModelElement(conditionFromSource, targetArtefact,
                        weavingInfo, modelComposition);
                target.getConditions().add(modelElementCopy);
            } else {
                List<Condition> mappedToTargetConditions = weavingInfo.getToElements(conditionFromSource);
                System.out.println(" mapped to: " + mappedToTargetConditions);
                for (Condition targetCondition : mappedToTargetConditions) {
                    if (topdown) {
                        targetCondition.setConditionText(EcoreUtil.copy(conditionFromSource.getConditionText()));
                    }
                }
            }
        }
        
        // now update any references that still refer to the external source model
        UseCaseModelReferenceUpdater updater = UseCaseModelReferenceUpdater.getInstance();
        updater.update(target, weavingInfo);
        
        return target;
    }

    /**
     * This is the main method to weave flows in use cases.
     * @param target The target use case.
     * @param source The source use case.
     * @param weavingInfo The weaving information.
     * @param modelComposition The model composition.
     * @param topdown Flag for top-down or bottom-up weaving.
     * @throws WeaverException if a flow's mapping structure is not properly defined.
     */
    public static void weaveFlows(final UseCase target, final UseCase source,
            final WeavingInformation weavingInfo, COREModelComposition modelComposition, final boolean topdown) throws WeaverException {
        if (target.getMainSuccessScenario() == null) {
            // Copy all the flows from the source to the target
            for (Flow sourceFlow : UcModelUtil.getFlows(source)) {
                copySourceFlowToTarget(sourceFlow, target, weavingInfo);
            }
            
            return;
        }
        
        HashSet<Step> processedSourceSteps = new HashSet<Step>();
        Set<Step> excludedTargetSteps = new HashSet<Step>();
        boolean anythingStepInRange = false;
        
        for (Flow sourceFlow : UcModelUtil.getFlows(source)) {
            if (weavingInfo.wasWoven(sourceFlow)) {
                // This source flow was mapped in the composition
                Flow targetFlow = weavingInfo.getFirstToElement(sourceFlow);
                
                Step prevMappedStep = null;
                List<Step> stepsBetweenMapped = new ArrayList<>();
                
                int currentIndex = 0;
                
                // Merge references steps from both flows
                if (!targetFlow.isMainSuccessScenario() ) {
                    for (Step refStep : sourceFlow.getReferencedSteps()) {
                        List<Step> refTargetSteps = weavingInfo.getToElements(refStep);
                        if (refTargetSteps == null) {
                            targetFlow.getReferencedSteps().add(refStep);
                        } else {
                            for (Step refTargetStep : refTargetSteps) {
                                if (!targetFlow.getReferencedSteps().contains(refTargetStep)) {
                                    targetFlow.getReferencedSteps().add(refTargetStep);
                                }
                            }
                        }
                    }  
                }            
                
                if (topdown) {
                    targetFlow.setConclusionType(sourceFlow.getConclusionType());
                    targetFlow.setConclusionStep(sourceFlow.getConclusionStep());
                    
                    // In top-down weaving, we want to iterate through the source flow
                    // since this is where the ordering has been defined
                    int numIterations = sourceFlow.getSteps().size();
                    for (int i = 0; i < numIterations; i++) {
                        Step sourceStep = sourceFlow.getSteps().get(i);
                        
                        // If the source step is an anything step, we skip forward through the target
                        // until we reach an unmapped step
                        if (sourceStep instanceof AnythingStep) {
                            Step currentTargetStep = targetFlow.getSteps().get(currentIndex);
                            Step nextMappedStep = getNextMappedStep(weavingInfo, targetFlow, currentTargetStep);
                            
                            while (currentTargetStep != nextMappedStep && 
                                    currentIndex < targetFlow.getSteps().size()) {
                                currentIndex++;
                                if (currentIndex != targetFlow.getSteps().size()) {
                                    currentTargetStep = targetFlow.getSteps().get(currentIndex);    
                                } 
                            }
                            
                            anythingStepInRange = true;
                            stepsBetweenMapped.add(sourceStep);
                            continue;
                        }
                        
                        if (weavingInfo.wasWoven(sourceStep)) {                            
                            Step mappedTargetStep = weavingInfo.getFirstToElement(sourceStep);
                            List<Step> targetSteps = UcModelUtil.getStepsBetween(targetFlow, prevMappedStep, 
                                    mappedTargetStep, false);
                            
                            if (targetSteps.size() > 0 && stepsBetweenMapped.size() > 0 && !anythingStepInRange) {
                                // Invalid; an anything step is required.
                                List<String> stepNumbers = stepsBetweenMapped.stream()
                                        .map(s -> UseCaseTextUtils.getStepShortDisplay(s))
                                        .collect(Collectors.toList());
                                String exceptionMessage = String.format(
                                        WEAVER_ANYTHINGSTEP_EXCEPTION,
                                        sourceFlow.getName(),
                                        source.getName(),
                                        String.join(", ", stepNumbers));
                                throw new WeaverException(exceptionMessage);
                            }              
                            
                            if (sourceStep.getPartiality() != COREPartialityType.NONE) {
                                // In this case, we just mark it as processed and move on
                                processedSourceSteps.add(sourceStep);
                                for (Step s : weavingInfo.getToElements(sourceStep)) {
                                    int mappedIndex = targetFlow.getSteps().indexOf(s);
                                    if (currentIndex < mappedIndex) {
                                        currentIndex = mappedIndex;
                                    }
                                }
                                continue;
                             }
                            
                            // This step was mapped, which means we need to replace the version in the target
                            // with the one from the source                            
                            currentIndex = targetFlow.getSteps().indexOf(mappedTargetStep);                            
                            Step newTargetStep = addStepToTarget(weavingInfo, targetFlow, sourceStep, currentIndex);
                            replaceStepReferences(mappedTargetStep, newTargetStep);
                            
                            weavingInfo.getToElements(sourceStep).add(0, newTargetStep);                            
                            weavingInfo.getToElements(sourceStep).remove(mappedTargetStep);
                            
                            prevMappedStep = mappedTargetStep;
                            stepsBetweenMapped.clear();
                            anythingStepInRange = false;
                            excludedTargetSteps.add(mappedTargetStep);
                        } else {
                            // This step was not mapped. We just add it to the target at our current position
                            Step newTargetStep = addStepToTarget(weavingInfo, targetFlow, sourceStep, currentIndex);
                            weavingInfo.getToElements(sourceStep).add(0, newTargetStep);
                            stepsBetweenMapped.add(sourceStep);
                        }
                        
                        processedSourceSteps.add(sourceStep);
                        currentIndex++;
                    }
                    
                    // Now, we need to remove any steps in the target that would have been overriden by things in the source
                    // This is only steps that were mapped and therefore replaced!
                    for (Step s : excludedTargetSteps) {
                        Step newStep = weavingInfo.getFirstToElement(s);
                        removeStep(s, newStep);
                        targetFlow.getSteps().remove(s);                        
                    }
                } else {
                    // Bottom-up flow weaving
                    
                    // SPECIAL CASE: if all of the steps in the flow are partial and mapped,
                    // we just want to map the alternate flows
                    boolean allStepsPartial = true;
                    for (Step s : targetFlow.getSteps()) {
                        if (s.getPartiality() == COREPartialityType.NONE 
                                || weavingInfo.getFromElements(s).size() == 0) {
                            allStepsPartial = false;
                            break;
                        }
                    }
                    
                    if (allStepsPartial) {
                        Map<Step, Step> stepCopyMap = new HashMap<Step, Step>();
                        
                        List<Step> targetSteps = new ArrayList<Step>(targetFlow.getSteps());
                        
                        processedSourceSteps.addAll(sourceFlow.getSteps());
                        for (Step s : sourceFlow.getSteps()) {
                            Step newStep = addStepToTarget(weavingInfo, targetFlow, s, currentIndex);
                            stepCopyMap.put(s, newStep);
                            currentIndex++;
                        }
                        
                        for (Step s : targetSteps) {
                            List<Step> fromElements = weavingInfo.getFromElements(s);
                            List<Step> newSteps = new ArrayList<Step>();
                            for (Step fromElement : fromElements) {
                                newSteps.add(stepCopyMap.get(fromElement));
                            }
                            
                            replaceStepReferences(s, newSteps);
                            targetFlow.getSteps().remove(s);
                        }                        
                        
                        continue;
                    }
                    
                    
                    int initialTargetSize = targetFlow.getSteps().size();
                    for (int i = 0; i < initialTargetSize; i++) {
                        
                        Step targetStep = targetFlow.getSteps().get(currentIndex);
                        
                        if (targetStep instanceof AnythingStep) {
                            // This pattern matches all of the steps in the source
                            // between the mappings
                            Step nextMappedStep = getNextMappedStep(weavingInfo, targetFlow, targetStep);
                            Step prevMappedSourceStep = prevMappedStep == null ? null : 
                                weavingInfo.getFromElements(prevMappedStep).get(0);
                            Step nextMappedSourceStep = nextMappedStep == null ? null : 
                                weavingInfo.getFromElements(nextMappedStep).get(0);
                            
                            List<Step> sourceSteps = UcModelUtil.getStepsBetween(sourceFlow, prevMappedSourceStep, 
                                    nextMappedSourceStep, false);
                            
                            for (Step s : sourceSteps) {
                                addStepToTarget(weavingInfo, targetFlow, s, currentIndex);
                                processedSourceSteps.add(s);
                                currentIndex++;
                            }
                            
                            targetFlow.getSteps().remove(targetStep);
                            anythingStepInRange = true;
                            stepsBetweenMapped.add(targetStep);
                            continue;
                        }
                        
                        List<Step> fromElements = weavingInfo.getFromElements(targetStep);
                        if (fromElements.size() > 0) {
                            // Step was mapped
                            Step mappedSourceStep = fromElements.get(0);
                            Step prevMappedSourceStep = prevMappedStep == null ? null : 
                                weavingInfo.getFromElements(prevMappedStep).get(0);
                            List<Step> sourceSteps = UcModelUtil.getStepsBetween(sourceFlow, prevMappedSourceStep, 
                                    mappedSourceStep, false);
                            
                            if (sourceSteps.size() > 0 && stepsBetweenMapped.size() > 0 && !anythingStepInRange) {
                                // Invalid; an anything step is required.
                                List<String> stepNumbers = stepsBetweenMapped.stream()
                                        .map(s -> UseCaseTextUtils.getStepShortDisplay(s))
                                        .collect(Collectors.toList());
                                String exceptionMessage = String.format(
                                        WEAVER_ANYTHINGSTEP_EXCEPTION,
                                        targetFlow.getName(),
                                        target.getName(),
                                        String.join(", ", stepNumbers));
                                throw new WeaverException(exceptionMessage);
                            }              
                            
                            processedSourceSteps.addAll(fromElements);
                            
                            if (targetStep.getPartiality() != COREPartialityType.NONE) {
                                // The step in the target is mapped
                                // We want to replace it with the steps from the source that it was mapped to.
                                List<Step> newSteps = new ArrayList<Step>();
                                for (Step s : fromElements) {
                                    Step newStep = addStepToTarget(weavingInfo, targetFlow, s, currentIndex);
                                    newSteps.add(newStep);
                                    currentIndex++;
                                }
                                                                
                                replaceStepReferences(targetStep, newSteps);
                                targetFlow.getSteps().remove(targetStep);
                                continue;
                            }
                            
                            if (stepsBetweenMapped.size() == 0) {
                                // If no steps between in the target, we add from the source
                                // If there were, an Anything step is required to inject from the source                                
                                for (Step s : sourceSteps) {
                                    addStepToTarget(weavingInfo, targetFlow, s, currentIndex);
                                    processedSourceSteps.add(s);
                                    currentIndex++;
                                }
                            }
                            
                            // Set the target step as previously mapped, and clear the steps between
                            prevMappedStep = targetStep;
                            anythingStepInRange = false;
                            stepsBetweenMapped.clear();
                        } else {
                            // Step was not mapped; add it to the set of steps between mapped steps
                            stepsBetweenMapped.add(targetStep);
                        }
                        
                        currentIndex++;
                    }
                }                
            } else {
                // Unmapped source flows: if a referenced step has been previously processed,
                // then we need to add this flow to the target as well
                boolean shouldBeCopied = false;
                for (Step refStep : sourceFlow.getReferencedSteps()) {
                    if (processedSourceSteps.contains(refStep)) {
                        shouldBeCopied = true;
                        break;
                    }
                }
                
                if (shouldBeCopied) {              
                    copySourceFlowToTarget(sourceFlow, target, weavingInfo);
                }
            }
        }        
    }

    /**
     * Adds a specific step from a source flow to a target flow.
     * @param weavingInfo The weaving info (will be updated to include the new copy).
     * @param targetFlow The target flow.
     * @param s The step to insert.
     * @param index The position to insert at.
     * @return The copy of the step. This is what has been added to the target flow.
     */
    private static Step addStepToTarget(WeavingInformation weavingInfo, Flow targetFlow, Step s, int index) {
        Step copy = EcoreUtil.copy(s);
        if (index >= 0 || index >= targetFlow.getSteps().size()) {
            targetFlow.getSteps().add(index, copy);   
        } else {
            targetFlow.getSteps().add(copy);
        }
        
        if (!weavingInfo.wasWoven(s)) {
            weavingInfo.add(s, copy);    
        }
        
        return copy;
    }
    
    /**
     * Gets the next mapped step in a target flow.
     * @param weavingInfo The weaving information.
     * @param targetFlow The flow to scan.
     * @param currentStep The current step being examined.
     * @return The next mapped step in the flow, or null if there is none.
     */
    private static Step getNextMappedStep(WeavingInformation weavingInfo, Flow targetFlow, Step currentStep) {
        int index = targetFlow.getSteps().indexOf(currentStep);
        index++;
        for (; index < targetFlow.getSteps().size(); index++) {
            Step s = targetFlow.getSteps().get(index);
            List<Step> from = weavingInfo.getFromElements(s);
            if (from.size() > 0) {
               return s;
            }
        }
        
        return null;
    }
    
    /**
     * Copy an entire flow from the source to the target.
     * @param sourceFlow The source flow to copy.
     * @param targetUseCase The target use case.
     * @param weavingInfo The weaving information (will be updated to include the new copies).
     */
    private static void copySourceFlowToTarget(Flow sourceFlow, UseCase targetUseCase,
            WeavingInformation weavingInfo) {
        Flow copy = EcoreUtil.copy(sourceFlow);
        
        if (sourceFlow.isMainSuccessScenario()) {
            targetUseCase.setMainSuccessScenario(copy);
        } else {
            Flow sourceParentFlow = (Flow) sourceFlow.eContainer();
            Flow originalTargetParentFlow = weavingInfo.getFirstToElement(sourceParentFlow);
            Flow targetParentFlow = UcModelUtil.getFlowByName(targetUseCase, originalTargetParentFlow.getName());
            targetParentFlow.getAlternateFlows().add(copy);
        }
                
        weavingInfo.add(sourceFlow, copy);
        
        for (int i = 0; i < sourceFlow.getSteps().size(); i++) {
            weavingInfo.add(sourceFlow.getSteps().get(i), copy.getSteps().get(i));
        }
    }
    
    /**
     * Replace a step with another. Used in top-down weaving.
     * @param oldStep The old step.
     * @param newStep The new step.
     */
    private static void replaceStepReferences(Step oldStep, Step newStep) {
        List<EObject> referencedObjects = EMFModelUtil.findObjectsThatCrossReference(oldStep);
        
        for (EObject refObject : referencedObjects) {
            // This has to be a flow, according to the definition of the metamodel
            // But just in case...
            if (refObject instanceof Flow) {
                Flow refFlow = (Flow) refObject;
                
                // Check any of the possible links (excluding containment, as this cannot apply)
                if (Objects.equals(oldStep, refFlow.getConclusionStep())) {
                    refFlow.setConclusionStep(newStep);
                }
                
                List<Step> referencedSteps = refFlow.getReferencedSteps();
                for (int index = 0; index < referencedSteps.size(); index++) {
                    Step referencedStep = referencedSteps.get(index);
                    if (Objects.equals(oldStep, referencedStep)) {
                        referencedSteps.remove(index);
                        if (!referencedSteps.contains(newStep)) {
                            referencedSteps.add(index, newStep);
                        }
                    }
                }
            }
        }
    }
    
    /**
     * Replace a step with another. Used in top-down weaving.
     * @param oldStep The old step.
     * @param newSteps The new steps.
     */
    private static void replaceStepReferences(Step oldStep, List<Step> newSteps) {
        List<EObject> referencedObjects = EMFModelUtil.findObjectsThatCrossReference(oldStep);
        
        for (EObject refObject : referencedObjects) {
            // This has to be a flow, according to the definition of the metamodel
            // But just in case...
            if (refObject instanceof Flow) {
                Flow refFlow = (Flow) refObject;
                
                List<Step> referencedSteps = refFlow.getReferencedSteps();
                for (int index = 0; index < referencedSteps.size(); index++) {
                    Step referencedStep = referencedSteps.get(index);
                    if (Objects.equals(oldStep, referencedStep)) {
                        referencedSteps.remove(index);
                        referencedSteps.addAll(newSteps);
                    }
                }
            }
        }
    }
    
    /**
     * Remove a step. Used in top-down weaving.
     * @param oldStep The old step.
     */
    private static void removeStep(Step oldStep, Step newStep) {
        List<EObject> referencedObjects = EMFModelUtil.findObjectsThatCrossReference(oldStep);
        
        for (EObject refObject : referencedObjects) {
            // This has to be a flow, according to the definition of the metamodel
            // But just in case...
            if (refObject instanceof Flow) {
                Flow refFlow = (Flow) refObject;
                
                if (Objects.equals(oldStep, refFlow.getConclusionStep())) {
                    refFlow.setConclusionStep(newStep);
                }
                
                List<Step> referencedSteps = refFlow.getReferencedSteps();                
                
                if (referencedSteps.contains(oldStep)) {
                    int index = referencedSteps.indexOf(oldStep);
                    referencedSteps.remove(oldStep);
                    referencedSteps.add(index, newStep);
                }
                
                refFlow.getSteps().remove(oldStep);
            }
        }
    }
}
