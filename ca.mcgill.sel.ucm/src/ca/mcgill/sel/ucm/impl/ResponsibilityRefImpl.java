/**
 */
package ca.mcgill.sel.ucm.impl;

import ca.mcgill.sel.ucm.Responsibility;
import ca.mcgill.sel.ucm.ResponsibilityRef;
import ca.mcgill.sel.ucm.UCMPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Responsibility Ref</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link ca.mcgill.sel.ucm.impl.ResponsibilityRefImpl#getRespDef <em>Resp Def</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ResponsibilityRefImpl extends PathNodeImpl implements ResponsibilityRef {
    /**
     * The cached value of the '{@link #getRespDef() <em>Resp Def</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getRespDef()
     * @generated
     * @ordered
     */
    protected Responsibility respDef;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected ResponsibilityRefImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return UCMPackage.Literals.RESPONSIBILITY_REF;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public Responsibility getRespDef() {
        if (respDef != null && respDef.eIsProxy()) {
            InternalEObject oldRespDef = (InternalEObject)respDef;
            respDef = (Responsibility)eResolveProxy(oldRespDef);
            if (respDef != oldRespDef) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, UCMPackage.RESPONSIBILITY_REF__RESP_DEF, oldRespDef, respDef));
            }
        }
        return respDef;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public Responsibility basicGetRespDef() {
        return respDef;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public NotificationChain basicSetRespDef(Responsibility newRespDef, NotificationChain msgs) {
        Responsibility oldRespDef = respDef;
        respDef = newRespDef;
        if (eNotificationRequired()) {
            ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, UCMPackage.RESPONSIBILITY_REF__RESP_DEF, oldRespDef, newRespDef);
            if (msgs == null) msgs = notification; else msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setRespDef(Responsibility newRespDef) {
        if (newRespDef != respDef) {
            NotificationChain msgs = null;
            if (respDef != null)
                msgs = ((InternalEObject)respDef).eInverseRemove(this, UCMPackage.RESPONSIBILITY__RESPS_REFS, Responsibility.class, msgs);
            if (newRespDef != null)
                msgs = ((InternalEObject)newRespDef).eInverseAdd(this, UCMPackage.RESPONSIBILITY__RESPS_REFS, Responsibility.class, msgs);
            msgs = basicSetRespDef(newRespDef, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, UCMPackage.RESPONSIBILITY_REF__RESP_DEF, newRespDef, newRespDef));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case UCMPackage.RESPONSIBILITY_REF__RESP_DEF:
                if (respDef != null)
                    msgs = ((InternalEObject)respDef).eInverseRemove(this, UCMPackage.RESPONSIBILITY__RESPS_REFS, Responsibility.class, msgs);
                return basicSetRespDef((Responsibility)otherEnd, msgs);
        }
        return super.eInverseAdd(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case UCMPackage.RESPONSIBILITY_REF__RESP_DEF:
                return basicSetRespDef(null, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case UCMPackage.RESPONSIBILITY_REF__RESP_DEF:
                if (resolve) return getRespDef();
                return basicGetRespDef();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case UCMPackage.RESPONSIBILITY_REF__RESP_DEF:
                setRespDef((Responsibility)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case UCMPackage.RESPONSIBILITY_REF__RESP_DEF:
                setRespDef((Responsibility)null);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case UCMPackage.RESPONSIBILITY_REF__RESP_DEF:
                return respDef != null;
        }
        return super.eIsSet(featureID);
    }

} //ResponsibilityRefImpl
