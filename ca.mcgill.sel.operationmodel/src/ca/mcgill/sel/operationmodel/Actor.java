/**
 */
package ca.mcgill.sel.operationmodel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Actor</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.operationmodel.Actor#getOutputMessages <em>Output Messages</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.operationmodel.OmPackage#getActor()
 * @model
 * @generated
 */
public interface Actor extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Output Messages</b></em>' reference list.
	 * The list contents are of type {@link ca.mcgill.sel.operationmodel.Message}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output Messages</em>' reference list.
	 * @see ca.mcgill.sel.operationmodel.OmPackage#getActor_OutputMessages()
	 * @model
	 * @generated
	 */
	EList<Message> getOutputMessages();

} // Actor
