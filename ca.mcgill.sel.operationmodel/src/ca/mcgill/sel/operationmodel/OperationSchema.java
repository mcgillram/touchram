/**
 */
package ca.mcgill.sel.operationmodel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operation Schema</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.operationmodel.OperationSchema#getScope <em>Scope</em>}</li>
 *   <li>{@link ca.mcgill.sel.operationmodel.OperationSchema#getInputMessage <em>Input Message</em>}</li>
 *   <li>{@link ca.mcgill.sel.operationmodel.OperationSchema#getMessages <em>Messages</em>}</li>
 *   <li>{@link ca.mcgill.sel.operationmodel.OperationSchema#getSystemName <em>System Name</em>}</li>
 *   <li>{@link ca.mcgill.sel.operationmodel.OperationSchema#getActors <em>Actors</em>}</li>
 *   <li>{@link ca.mcgill.sel.operationmodel.OperationSchema#getParameterTypes <em>Parameter Types</em>}</li>
 *   <li>{@link ca.mcgill.sel.operationmodel.OperationSchema#getDescription <em>Description</em>}</li>
 *   <li>{@link ca.mcgill.sel.operationmodel.OperationSchema#getPreCondition <em>Pre Condition</em>}</li>
 *   <li>{@link ca.mcgill.sel.operationmodel.OperationSchema#getPostCondition <em>Post Condition</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.operationmodel.OmPackage#getOperationSchema()
 * @model
 * @generated
 */
public interface OperationSchema extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Scope</b></em>' containment reference list.
	 * The list contents are of type {@link ca.mcgill.sel.operationmodel.Classifier}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scope</em>' containment reference list.
	 * @see ca.mcgill.sel.operationmodel.OmPackage#getOperationSchema_Scope()
	 * @model containment="true"
	 * @generated
	 */
	EList<Classifier> getScope();

	/**
	 * Returns the value of the '<em><b>Input Message</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input Message</em>' reference.
	 * @see #setInputMessage(Message)
	 * @see ca.mcgill.sel.operationmodel.OmPackage#getOperationSchema_InputMessage()
	 * @model
	 * @generated
	 */
	Message getInputMessage();

	/**
	 * Sets the value of the '{@link ca.mcgill.sel.operationmodel.OperationSchema#getInputMessage <em>Input Message</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Input Message</em>' reference.
	 * @see #getInputMessage()
	 * @generated
	 */
	void setInputMessage(Message value);

	/**
	 * Returns the value of the '<em><b>Messages</b></em>' containment reference list.
	 * The list contents are of type {@link ca.mcgill.sel.operationmodel.Message}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Messages</em>' containment reference list.
	 * @see ca.mcgill.sel.operationmodel.OmPackage#getOperationSchema_Messages()
	 * @model containment="true"
	 * @generated
	 */
	EList<Message> getMessages();

	/**
	 * Returns the value of the '<em><b>System Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>System Name</em>' attribute.
	 * @see #setSystemName(String)
	 * @see ca.mcgill.sel.operationmodel.OmPackage#getOperationSchema_SystemName()
	 * @model
	 * @generated
	 */
	String getSystemName();

	/**
	 * Sets the value of the '{@link ca.mcgill.sel.operationmodel.OperationSchema#getSystemName <em>System Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>System Name</em>' attribute.
	 * @see #getSystemName()
	 * @generated
	 */
	void setSystemName(String value);

	/**
	 * Returns the value of the '<em><b>Actors</b></em>' containment reference list.
	 * The list contents are of type {@link ca.mcgill.sel.operationmodel.Actor}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Actors</em>' containment reference list.
	 * @see ca.mcgill.sel.operationmodel.OmPackage#getOperationSchema_Actors()
	 * @model containment="true"
	 * @generated
	 */
	EList<Actor> getActors();

	/**
	 * Returns the value of the '<em><b>Parameter Types</b></em>' containment reference list.
	 * The list contents are of type {@link ca.mcgill.sel.operationmodel.ParameterType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter Types</em>' containment reference list.
	 * @see ca.mcgill.sel.operationmodel.OmPackage#getOperationSchema_ParameterTypes()
	 * @model containment="true"
	 * @generated
	 */
	EList<ParameterType> getParameterTypes();

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see ca.mcgill.sel.operationmodel.OmPackage#getOperationSchema_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link ca.mcgill.sel.operationmodel.OperationSchema#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Pre Condition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pre Condition</em>' attribute.
	 * @see #setPreCondition(String)
	 * @see ca.mcgill.sel.operationmodel.OmPackage#getOperationSchema_PreCondition()
	 * @model
	 * @generated
	 */
	String getPreCondition();

	/**
	 * Sets the value of the '{@link ca.mcgill.sel.operationmodel.OperationSchema#getPreCondition <em>Pre Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pre Condition</em>' attribute.
	 * @see #getPreCondition()
	 * @generated
	 */
	void setPreCondition(String value);

	/**
	 * Returns the value of the '<em><b>Post Condition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Post Condition</em>' attribute.
	 * @see #setPostCondition(String)
	 * @see ca.mcgill.sel.operationmodel.OmPackage#getOperationSchema_PostCondition()
	 * @model
	 * @generated
	 */
	String getPostCondition();

	/**
	 * Sets the value of the '{@link ca.mcgill.sel.operationmodel.OperationSchema#getPostCondition <em>Post Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Post Condition</em>' attribute.
	 * @see #getPostCondition()
	 * @generated
	 */
	void setPostCondition(String value);

} // OperationSchema
