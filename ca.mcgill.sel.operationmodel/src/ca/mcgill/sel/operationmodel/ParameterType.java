/**
 */
package ca.mcgill.sel.operationmodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parameter Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.mcgill.sel.operationmodel.OmPackage#getParameterType()
 * @model
 * @generated
 */
public interface ParameterType extends NamedElement {
} // ParameterType
