/**
 */
package ca.mcgill.sel.operationmodel.impl;

import ca.mcgill.sel.operationmodel.Classifier;
import ca.mcgill.sel.operationmodel.OmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Classifier</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ClassifierImpl extends NamedElementImpl implements Classifier {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClassifierImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OmPackage.Literals.CLASSIFIER;
	}

} //ClassifierImpl
