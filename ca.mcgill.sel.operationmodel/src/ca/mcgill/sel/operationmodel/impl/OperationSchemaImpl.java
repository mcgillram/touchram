/**
 */
package ca.mcgill.sel.operationmodel.impl;

import ca.mcgill.sel.operationmodel.Actor;
import ca.mcgill.sel.operationmodel.Classifier;
import ca.mcgill.sel.operationmodel.Message;
import ca.mcgill.sel.operationmodel.OmPackage;
import ca.mcgill.sel.operationmodel.OperationSchema;
import ca.mcgill.sel.operationmodel.ParameterType;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operation Schema</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.operationmodel.impl.OperationSchemaImpl#getScope <em>Scope</em>}</li>
 *   <li>{@link ca.mcgill.sel.operationmodel.impl.OperationSchemaImpl#getInputMessage <em>Input Message</em>}</li>
 *   <li>{@link ca.mcgill.sel.operationmodel.impl.OperationSchemaImpl#getMessages <em>Messages</em>}</li>
 *   <li>{@link ca.mcgill.sel.operationmodel.impl.OperationSchemaImpl#getSystemName <em>System Name</em>}</li>
 *   <li>{@link ca.mcgill.sel.operationmodel.impl.OperationSchemaImpl#getActors <em>Actors</em>}</li>
 *   <li>{@link ca.mcgill.sel.operationmodel.impl.OperationSchemaImpl#getParameterTypes <em>Parameter Types</em>}</li>
 *   <li>{@link ca.mcgill.sel.operationmodel.impl.OperationSchemaImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link ca.mcgill.sel.operationmodel.impl.OperationSchemaImpl#getPreCondition <em>Pre Condition</em>}</li>
 *   <li>{@link ca.mcgill.sel.operationmodel.impl.OperationSchemaImpl#getPostCondition <em>Post Condition</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OperationSchemaImpl extends NamedElementImpl implements OperationSchema {
	/**
	 * The cached value of the '{@link #getScope() <em>Scope</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScope()
	 * @generated
	 * @ordered
	 */
	protected EList<Classifier> scope;

	/**
	 * The cached value of the '{@link #getInputMessage() <em>Input Message</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInputMessage()
	 * @generated
	 * @ordered
	 */
	protected Message inputMessage;

	/**
	 * The cached value of the '{@link #getMessages() <em>Messages</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessages()
	 * @generated
	 * @ordered
	 */
	protected EList<Message> messages;

	/**
	 * The default value of the '{@link #getSystemName() <em>System Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSystemName()
	 * @generated
	 * @ordered
	 */
	protected static final String SYSTEM_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSystemName() <em>System Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSystemName()
	 * @generated
	 * @ordered
	 */
	protected String systemName = SYSTEM_NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getActors() <em>Actors</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActors()
	 * @generated
	 * @ordered
	 */
	protected EList<Actor> actors;

	/**
	 * The cached value of the '{@link #getParameterTypes() <em>Parameter Types</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameterTypes()
	 * @generated
	 * @ordered
	 */
	protected EList<ParameterType> parameterTypes;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getPreCondition() <em>Pre Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreCondition()
	 * @generated
	 * @ordered
	 */
	protected static final String PRE_CONDITION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPreCondition() <em>Pre Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreCondition()
	 * @generated
	 * @ordered
	 */
	protected String preCondition = PRE_CONDITION_EDEFAULT;

	/**
	 * The default value of the '{@link #getPostCondition() <em>Post Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPostCondition()
	 * @generated
	 * @ordered
	 */
	protected static final String POST_CONDITION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPostCondition() <em>Post Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPostCondition()
	 * @generated
	 * @ordered
	 */
	protected String postCondition = POST_CONDITION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OperationSchemaImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OmPackage.Literals.OPERATION_SCHEMA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Classifier> getScope() {
		if (scope == null) {
			scope = new EObjectContainmentEList<Classifier>(Classifier.class, this, OmPackage.OPERATION_SCHEMA__SCOPE);
		}
		return scope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Message getInputMessage() {
		if (inputMessage != null && inputMessage.eIsProxy()) {
			InternalEObject oldInputMessage = (InternalEObject)inputMessage;
			inputMessage = (Message)eResolveProxy(oldInputMessage);
			if (inputMessage != oldInputMessage) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OmPackage.OPERATION_SCHEMA__INPUT_MESSAGE, oldInputMessage, inputMessage));
			}
		}
		return inputMessage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Message basicGetInputMessage() {
		return inputMessage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInputMessage(Message newInputMessage) {
		Message oldInputMessage = inputMessage;
		inputMessage = newInputMessage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OmPackage.OPERATION_SCHEMA__INPUT_MESSAGE, oldInputMessage, inputMessage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Message> getMessages() {
		if (messages == null) {
			messages = new EObjectContainmentEList<Message>(Message.class, this, OmPackage.OPERATION_SCHEMA__MESSAGES);
		}
		return messages;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSystemName() {
		return systemName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSystemName(String newSystemName) {
		String oldSystemName = systemName;
		systemName = newSystemName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OmPackage.OPERATION_SCHEMA__SYSTEM_NAME, oldSystemName, systemName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Actor> getActors() {
		if (actors == null) {
			actors = new EObjectContainmentEList<Actor>(Actor.class, this, OmPackage.OPERATION_SCHEMA__ACTORS);
		}
		return actors;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ParameterType> getParameterTypes() {
		if (parameterTypes == null) {
			parameterTypes = new EObjectContainmentEList<ParameterType>(ParameterType.class, this, OmPackage.OPERATION_SCHEMA__PARAMETER_TYPES);
		}
		return parameterTypes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OmPackage.OPERATION_SCHEMA__DESCRIPTION, oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPreCondition() {
		return preCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPreCondition(String newPreCondition) {
		String oldPreCondition = preCondition;
		preCondition = newPreCondition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OmPackage.OPERATION_SCHEMA__PRE_CONDITION, oldPreCondition, preCondition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPostCondition() {
		return postCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPostCondition(String newPostCondition) {
		String oldPostCondition = postCondition;
		postCondition = newPostCondition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OmPackage.OPERATION_SCHEMA__POST_CONDITION, oldPostCondition, postCondition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OmPackage.OPERATION_SCHEMA__SCOPE:
				return ((InternalEList<?>)getScope()).basicRemove(otherEnd, msgs);
			case OmPackage.OPERATION_SCHEMA__MESSAGES:
				return ((InternalEList<?>)getMessages()).basicRemove(otherEnd, msgs);
			case OmPackage.OPERATION_SCHEMA__ACTORS:
				return ((InternalEList<?>)getActors()).basicRemove(otherEnd, msgs);
			case OmPackage.OPERATION_SCHEMA__PARAMETER_TYPES:
				return ((InternalEList<?>)getParameterTypes()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OmPackage.OPERATION_SCHEMA__SCOPE:
				return getScope();
			case OmPackage.OPERATION_SCHEMA__INPUT_MESSAGE:
				if (resolve) return getInputMessage();
				return basicGetInputMessage();
			case OmPackage.OPERATION_SCHEMA__MESSAGES:
				return getMessages();
			case OmPackage.OPERATION_SCHEMA__SYSTEM_NAME:
				return getSystemName();
			case OmPackage.OPERATION_SCHEMA__ACTORS:
				return getActors();
			case OmPackage.OPERATION_SCHEMA__PARAMETER_TYPES:
				return getParameterTypes();
			case OmPackage.OPERATION_SCHEMA__DESCRIPTION:
				return getDescription();
			case OmPackage.OPERATION_SCHEMA__PRE_CONDITION:
				return getPreCondition();
			case OmPackage.OPERATION_SCHEMA__POST_CONDITION:
				return getPostCondition();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OmPackage.OPERATION_SCHEMA__SCOPE:
				getScope().clear();
				getScope().addAll((Collection<? extends Classifier>)newValue);
				return;
			case OmPackage.OPERATION_SCHEMA__INPUT_MESSAGE:
				setInputMessage((Message)newValue);
				return;
			case OmPackage.OPERATION_SCHEMA__MESSAGES:
				getMessages().clear();
				getMessages().addAll((Collection<? extends Message>)newValue);
				return;
			case OmPackage.OPERATION_SCHEMA__SYSTEM_NAME:
				setSystemName((String)newValue);
				return;
			case OmPackage.OPERATION_SCHEMA__ACTORS:
				getActors().clear();
				getActors().addAll((Collection<? extends Actor>)newValue);
				return;
			case OmPackage.OPERATION_SCHEMA__PARAMETER_TYPES:
				getParameterTypes().clear();
				getParameterTypes().addAll((Collection<? extends ParameterType>)newValue);
				return;
			case OmPackage.OPERATION_SCHEMA__DESCRIPTION:
				setDescription((String)newValue);
				return;
			case OmPackage.OPERATION_SCHEMA__PRE_CONDITION:
				setPreCondition((String)newValue);
				return;
			case OmPackage.OPERATION_SCHEMA__POST_CONDITION:
				setPostCondition((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OmPackage.OPERATION_SCHEMA__SCOPE:
				getScope().clear();
				return;
			case OmPackage.OPERATION_SCHEMA__INPUT_MESSAGE:
				setInputMessage((Message)null);
				return;
			case OmPackage.OPERATION_SCHEMA__MESSAGES:
				getMessages().clear();
				return;
			case OmPackage.OPERATION_SCHEMA__SYSTEM_NAME:
				setSystemName(SYSTEM_NAME_EDEFAULT);
				return;
			case OmPackage.OPERATION_SCHEMA__ACTORS:
				getActors().clear();
				return;
			case OmPackage.OPERATION_SCHEMA__PARAMETER_TYPES:
				getParameterTypes().clear();
				return;
			case OmPackage.OPERATION_SCHEMA__DESCRIPTION:
				setDescription(DESCRIPTION_EDEFAULT);
				return;
			case OmPackage.OPERATION_SCHEMA__PRE_CONDITION:
				setPreCondition(PRE_CONDITION_EDEFAULT);
				return;
			case OmPackage.OPERATION_SCHEMA__POST_CONDITION:
				setPostCondition(POST_CONDITION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OmPackage.OPERATION_SCHEMA__SCOPE:
				return scope != null && !scope.isEmpty();
			case OmPackage.OPERATION_SCHEMA__INPUT_MESSAGE:
				return inputMessage != null;
			case OmPackage.OPERATION_SCHEMA__MESSAGES:
				return messages != null && !messages.isEmpty();
			case OmPackage.OPERATION_SCHEMA__SYSTEM_NAME:
				return SYSTEM_NAME_EDEFAULT == null ? systemName != null : !SYSTEM_NAME_EDEFAULT.equals(systemName);
			case OmPackage.OPERATION_SCHEMA__ACTORS:
				return actors != null && !actors.isEmpty();
			case OmPackage.OPERATION_SCHEMA__PARAMETER_TYPES:
				return parameterTypes != null && !parameterTypes.isEmpty();
			case OmPackage.OPERATION_SCHEMA__DESCRIPTION:
				return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
			case OmPackage.OPERATION_SCHEMA__PRE_CONDITION:
				return PRE_CONDITION_EDEFAULT == null ? preCondition != null : !PRE_CONDITION_EDEFAULT.equals(preCondition);
			case OmPackage.OPERATION_SCHEMA__POST_CONDITION:
				return POST_CONDITION_EDEFAULT == null ? postCondition != null : !POST_CONDITION_EDEFAULT.equals(postCondition);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (systemName: ");
		result.append(systemName);
		result.append(", description: ");
		result.append(description);
		result.append(", preCondition: ");
		result.append(preCondition);
		result.append(", postCondition: ");
		result.append(postCondition);
		result.append(')');
		return result.toString();
	}

} //OperationSchemaImpl
