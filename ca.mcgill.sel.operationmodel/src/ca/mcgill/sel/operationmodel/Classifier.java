/**
 */
package ca.mcgill.sel.operationmodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Classifier</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.mcgill.sel.operationmodel.OmPackage#getClassifier()
 * @model
 * @generated
 */
public interface Classifier extends NamedElement {
} // Classifier
