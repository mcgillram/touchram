/**
 */
package ca.mcgill.sel.ucm.provider;


import ca.mcgill.sel.core.provider.COREModelItemProvider;

import ca.mcgill.sel.ucm.UCMFactory;
import ca.mcgill.sel.ucm.UCMMap;
import ca.mcgill.sel.ucm.UCMPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link ca.mcgill.sel.ucm.UCMMap} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class UCMMapItemProvider extends COREModelItemProvider {
    /**
     * This constructs an instance from a factory and a notifier.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public UCMMapItemProvider(AdapterFactory adapterFactory) {
        super(adapterFactory);
    }

    /**
     * This returns the property descriptors for the adapted class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
        if (itemPropertyDescriptors == null) {
            super.getPropertyDescriptors(object);

        }
        return itemPropertyDescriptors;
    }

    /**
     * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
     * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
     * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
        if (childrenFeatures == null) {
            super.getChildrenFeatures(object);
            childrenFeatures.add(UCMPackage.Literals.UCM_MAP__CONNECTIONS);
            childrenFeatures.add(UCMPackage.Literals.UCM_MAP__NODES);
            childrenFeatures.add(UCMPackage.Literals.UCM_MAP__RESPS);
            childrenFeatures.add(UCMPackage.Literals.UCM_MAP__COMPS);
            childrenFeatures.add(UCMPackage.Literals.UCM_MAP__COMP_REFS);
        }
        return childrenFeatures;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EStructuralFeature getChildFeature(Object object, Object child) {
        // Check the type of the specified child object and return the proper feature to use for
        // adding (see {@link AddCommand}) it as a child.

        return super.getChildFeature(object, child);
    }

    /**
     * This returns UCMMap.gif.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object getImage(Object object) {
        return overlayImage(object, getResourceLocator().getImage("full/obj16/UCMMap"));
    }

    /**
     * This returns the label text for the adapted class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String getText(Object object) {
        String label = ((UCMMap)object).getName();
        return label == null || label.length() == 0 ?
            getString("_UI_UCMMap_type") :
            getString("_UI_UCMMap_type") + " " + label;
    }
    

    /**
     * This handles model notifications by calling {@link #updateChildren} to update any cached
     * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void notifyChanged(Notification notification) {
        updateChildren(notification);

        switch (notification.getFeatureID(UCMMap.class)) {
            case UCMPackage.UCM_MAP__CONNECTIONS:
            case UCMPackage.UCM_MAP__NODES:
            case UCMPackage.UCM_MAP__RESPS:
            case UCMPackage.UCM_MAP__COMPS:
            case UCMPackage.UCM_MAP__COMP_REFS:
                fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
                return;
        }
        super.notifyChanged(notification);
    }

    /**
     * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
     * that can be created under this object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
        super.collectNewChildDescriptors(newChildDescriptors, object);

        newChildDescriptors.add
            (createChildParameter
                (UCMPackage.Literals.UCM_MAP__CONNECTIONS,
                 UCMFactory.eINSTANCE.createNodeConnection()));

        newChildDescriptors.add
            (createChildParameter
                (UCMPackage.Literals.UCM_MAP__NODES,
                 UCMFactory.eINSTANCE.createPathNode()));

        newChildDescriptors.add
            (createChildParameter
                (UCMPackage.Literals.UCM_MAP__NODES,
                 UCMFactory.eINSTANCE.createAndFork()));

        newChildDescriptors.add
            (createChildParameter
                (UCMPackage.Literals.UCM_MAP__NODES,
                 UCMFactory.eINSTANCE.createAndJoin()));

        newChildDescriptors.add
            (createChildParameter
                (UCMPackage.Literals.UCM_MAP__NODES,
                 UCMFactory.eINSTANCE.createOrFork()));

        newChildDescriptors.add
            (createChildParameter
                (UCMPackage.Literals.UCM_MAP__NODES,
                 UCMFactory.eINSTANCE.createOrJoin()));

        newChildDescriptors.add
            (createChildParameter
                (UCMPackage.Literals.UCM_MAP__NODES,
                 UCMFactory.eINSTANCE.createEmptyPoint()));

        newChildDescriptors.add
            (createChildParameter
                (UCMPackage.Literals.UCM_MAP__NODES,
                 UCMFactory.eINSTANCE.createResponsibilityRef()));

        newChildDescriptors.add
            (createChildParameter
                (UCMPackage.Literals.UCM_MAP__NODES,
                 UCMFactory.eINSTANCE.createStartPoint()));

        newChildDescriptors.add
            (createChildParameter
                (UCMPackage.Literals.UCM_MAP__NODES,
                 UCMFactory.eINSTANCE.createEndPoint()));

        newChildDescriptors.add
            (createChildParameter
                (UCMPackage.Literals.UCM_MAP__RESPS,
                 UCMFactory.eINSTANCE.createResponsibility()));

        newChildDescriptors.add
            (createChildParameter
                (UCMPackage.Literals.UCM_MAP__COMPS,
                 UCMFactory.eINSTANCE.createComponent()));

        newChildDescriptors.add
            (createChildParameter
                (UCMPackage.Literals.UCM_MAP__COMP_REFS,
                 UCMFactory.eINSTANCE.createComponentRef()));
    }

    /**
     * Return the resource locator for this item provider's resources.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ResourceLocator getResourceLocator() {
        return UCMEditPlugin.INSTANCE;
    }

}
