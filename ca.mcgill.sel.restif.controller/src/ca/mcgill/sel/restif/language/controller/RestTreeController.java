package ca.mcgill.sel.restif.language.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.MoveCommand;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.controller.CoreBaseController;
import ca.mcgill.sel.restif.AccessMethod;
import ca.mcgill.sel.restif.DynamicFragment;
import ca.mcgill.sel.restif.MethodType;
import ca.mcgill.sel.restif.PathFragment;
import ca.mcgill.sel.restif.Resource;
import ca.mcgill.sel.restif.RestIF;
import ca.mcgill.sel.restif.RestifFactory;
import ca.mcgill.sel.restif.RestifPackage;
import ca.mcgill.sel.restif.StaticFragment;

/**
 * The controller for {@link RestIF}.
 *
 * @author Bowen
 */
public class RestTreeController extends CoreBaseController {

    /**
     * Creates a new instance of {@link RestTreeController}.
     */
    protected RestTreeController() {
        // Prevent anyone outside this package to instantiate.
    }

    /**
     * Removes a {@link PathFragment} from a {@link RestIF} model. This method
     * removes the given {@link PathFragment} and its children then adds the
     * children to the parent of the given {@link PathFragment}. It also removes the
     * {@link Resource} associated with the removed {@link PathFragment} if it
     * exists.
     * 
     * @param parent              - the parent of the removed {@link PathFragment}
     * @param pathFragment        - the removed {@link PathFragment}
     * @param restIF              - the {@link RestIF} model
     * @param childrenToParentMap - a linked hash map containing keys of every child
     *                            of the removed {@link PathFragment} and its parent
     *                            as values
     */
    public void removePathFragment(PathFragment parent, PathFragment pathFragment, RestIF restIF,
            LinkedHashMap<PathFragment, PathFragment> childrenToParentMap) {

        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(restIF);
        CompoundCommand deleteCommands = new CompoundCommand();

        // Find index of removed path fragment.
        int indexOfRemovedPathFragment = parent.getChild().indexOf(pathFragment);
        
        // Reverse the list to remove from the children at the lowest levels first.
        List<PathFragment> keySet = new ArrayList<>(childrenToParentMap.keySet());
        Collections.reverse(keySet);
        Set<PathFragment> reversedKeySet = new LinkedHashSet<>(keySet);

        for (PathFragment child : reversedKeySet) {
            deleteCommands.append(RemoveCommand.create(editingDomain, childrenToParentMap.get(child),
                    RestifPackage.Literals.PATH_FRAGMENT__CHILD, child));
        }

        deleteCommands.append(
                RemoveCommand.create(editingDomain, parent, RestifPackage.Literals.PATH_FRAGMENT__CHILD, pathFragment));

        // remove the associated resource and access method of the removed path fragment if they exist
        for (Resource resource : restIF.getResource()) {
            if (resource.getEndpoint().equals(pathFragment)) {
                for (AccessMethod accessMethod : resource.getAccessmethod()) {
                    deleteCommands.append(RemoveCommand.create(editingDomain, accessMethod));
                }
                
                deleteCommands.append(RemoveCommand.create(editingDomain, restIF, RestifPackage.Literals.RESOURCE, 
                        resource));
            }
        }
        
        // Execute the compound delete command.
        doExecute(editingDomain, deleteCommands);
        
        ArrayList<PathFragment> childrenList = new ArrayList<>();
        childrenList.addAll(childrenToParentMap.keySet());
        
        CompoundCommand addCommands = new CompoundCommand();

        // Add every child back to the parent of the removed {@link PathFragment}.
        for (PathFragment child : childrenList) {
            // If the current child's parent is the removed path fragment, we add it to the
            // newly created path fragment.
            if (childrenToParentMap.get(child).equals(pathFragment)) {
                // execute each add command seperately to prevent EMF command is not executable error
                Command addCommand = AddCommand.create(editingDomain, parent, RestifPackage.PATH_FRAGMENT__CHILD, 
                        child, indexOfRemovedPathFragment);
                doExecute(editingDomain, addCommand);
                // increment indexOfRemovedPathFragment, so that the children of the removed node stay in the same order
                // when re-appended
                indexOfRemovedPathFragment++;
            } else {
                // Otherwise add the current child to its parent.
                addCommands.append(AddCommand.create(editingDomain, childrenToParentMap.get(child),
                        RestifPackage.Literals.PATH_FRAGMENT__CHILD, child));
            }
        }
        
        // Execute add commands if the command size is greater than 0.
        if (addCommands.getCommandList().size() > 0) {
            doExecute(editingDomain, addCommands);
        }
    }

    /**
     * Switches a {@link PathFragment} from {@link StaticFragment} to
     * {@link DynamicFragment} or vice versa. This method removes the given
     * {@link PathFragment} and its children then adds a new switched
     * {@link PathFragment} and then adds the children to the switched
     * {@link PathFragment}. It also removes the {@link Resource} associated with
     * the removed {@link PathFragment} if it exists and adds a new one with the
     * same {@link MethodType}s.
     * 
     * @param parent              - the parent of the switched {@link PathFragment}
     * @param pathFragment        - the switched {@link PathFragment}
     * @param restIF              - the {@link RestIF} model
     * @param childrenToParentMap - a linked hash map containing keys of every child
     *                            of the switched {@link PathFragment} and its
     *                            parent as values
     */
    public void switchPathFragment(PathFragment parent, PathFragment pathFragment, RestIF restIF,
            LinkedHashMap<PathFragment, PathFragment> childrenToParentMap) {

        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(restIF);
        CompoundCommand deleteCommands = new CompoundCommand();

        // Find index of removed path fragment.
        int indexOfRemovedPathFragment = parent.getChild().indexOf(pathFragment);
        
        // Reverse the list to remove from the child first.
        List<PathFragment> keySet = new ArrayList<>(childrenToParentMap.keySet());
        Collections.reverse(keySet);
        Set<PathFragment> reversedKeySet = new LinkedHashSet<>(keySet);

        for (PathFragment child : reversedKeySet) {
            deleteCommands.append(RemoveCommand.create(editingDomain, childrenToParentMap.get(child),
                    RestifPackage.Literals.PATH_FRAGMENT__CHILD, child));
        }

        // Execute the compound delete command.
        deleteCommands.append(
                RemoveCommand.create(editingDomain, parent, RestifPackage.Literals.PATH_FRAGMENT__CHILD, pathFragment));

        doExecute(editingDomain, deleteCommands);

        PathFragment newPathFragment = null;
        String newName;

        if (pathFragment instanceof StaticFragment) {
            newPathFragment = RestifFactory.eINSTANCE.createDynamicFragment();
            newName = "/{" + ((StaticFragment) pathFragment).getInternalname().substring(1) + "}";
            ((DynamicFragment) newPathFragment).setPlaceholder(newName);
        } else {
            newPathFragment = RestifFactory.eINSTANCE.createStaticFragment();
            newName = "/" + ((DynamicFragment) pathFragment).getPlaceholder().substring(2,
                    ((DynamicFragment) pathFragment).getPlaceholder().length() - 1);
            ((StaticFragment) newPathFragment).setInternalname(newName);
        }
        
        CompoundCommand addPFandSetResourceCommands = new CompoundCommand();

        // update the endpoint of the associated resource to the new path fragment
        for (Resource resource : restIF.getResource()) {
            if (resource.getEndpoint().equals(pathFragment)) {
                addPFandSetResourceCommands.append(SetCommand.create(editingDomain, resource, RestifPackage.Literals
                        .RESOURCE__ENDPOINT, newPathFragment));
                break;
            }
        }

        // Execute the add command to add the switched {@link PathFragment}.
        addPFandSetResourceCommands.append(AddCommand.create(editingDomain, parent, RestifPackage.PATH_FRAGMENT__CHILD, 
                newPathFragment, indexOfRemovedPathFragment));

        ArrayList<PathFragment> childrenList = new ArrayList<>();
        childrenList.addAll(childrenToParentMap.keySet());

        for (PathFragment child : childrenList) {
            // If the current child's parent is the switched path fragment, we add it to the
            // newly created path fragment.
            if (childrenToParentMap.get(child).equals(pathFragment)) {
                addPFandSetResourceCommands.append(AddCommand.create(editingDomain, newPathFragment, 
                        RestifPackage.PATH_FRAGMENT__CHILD, child));
            } else {
                // Otherwise add the current child to its parent.
                addPFandSetResourceCommands.append(AddCommand.create(editingDomain, childrenToParentMap.get(child),
                        RestifPackage.Literals.PATH_FRAGMENT__CHILD, child));
            }
        }
        
        doExecute(editingDomain, addPFandSetResourceCommands);
    }

    /**
     * Switches the root from {@link StaticFragment} to {@link DynamicFragment} or
     * vice versa. This method removes the given {@link PathFragment} and its
     * children then adds a new switched {@link PathFragment} and then adds the
     * children to the switched {@link PathFragment}. It also removes the
     * {@link Resource} associated with the removed root if it exists and adds a new
     * one with the same {@link MethodType}s.
     * 
     * @param restIF              - the {@link RestIF} model
     * @param childrenToParentMap - a linked hash map containing keys of every child
     *                            of the switched root and its parent as values
     */
    public void switchRootPathFragment(RestIF restIF, LinkedHashMap<PathFragment, PathFragment> childrenToParentMap) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(restIF);
        CompoundCommand deleteCommands = new CompoundCommand();
        PathFragment root = restIF.getRoot();

        // Reverse the list to remove from the child first.
        List<PathFragment> keySet = new ArrayList<>(childrenToParentMap.keySet());
        Collections.reverse(keySet);
        Set<PathFragment> reversedKeySet = new LinkedHashSet<>(keySet);

        for (PathFragment child : reversedKeySet) {
            deleteCommands.append(RemoveCommand.create(editingDomain, childrenToParentMap.get(child),
                    RestifPackage.Literals.PATH_FRAGMENT__CHILD, child));
        }

        // Execute the compound delete command.
        deleteCommands.append(RemoveCommand.create(editingDomain, root));

        doExecute(editingDomain, deleteCommands);

        PathFragment newRoot = null;
        String newName;

        if (root instanceof StaticFragment) {
            newRoot = RestifFactory.eINSTANCE.createDynamicFragment();
            newName = "/{" + ((StaticFragment) root).getInternalname().substring(1) + "}";
            ((DynamicFragment) newRoot).setPlaceholder(newName);
        } else {
            newRoot = RestifFactory.eINSTANCE.createStaticFragment();
            newName = "/" + ((DynamicFragment) root).getPlaceholder().substring(2,
                    ((DynamicFragment) root).getPlaceholder().length() - 1);
            ((StaticFragment) newRoot).setInternalname(newName);
        }
        
        CompoundCommand addPFandSetResourceCommands = new CompoundCommand();

        // update the endpoint of the associated resource to the new path fragment
        for (Resource resource : restIF.getResource()) {
            if (resource.getEndpoint().equals(root)) {
                addPFandSetResourceCommands.append(SetCommand.create(editingDomain, resource, RestifPackage.Literals
                        .RESOURCE__ENDPOINT, newRoot));
                break;
            }
        }

        // Execute the add command to add the switched root.
        addPFandSetResourceCommands.append(SetCommand.create(editingDomain, restIF, 
                RestifPackage.Literals.REST_IF__ROOT, newRoot));
        
        ArrayList<PathFragment> childrenList = new ArrayList<>();
        childrenList.addAll(childrenToParentMap.keySet());

        for (PathFragment child : childrenList) {
            if (childrenToParentMap.get(child).equals(root)) {
                childrenToParentMap.put(child, newRoot);
            }
        }

        for (PathFragment child : childrenList) {
            // If the current child's parent is the switched root, we add it to the newly
            // created root.
            if (childrenToParentMap.get(child).equals(root)) {
                addPFandSetResourceCommands.append(AddCommand.create(editingDomain, newRoot, 
                        RestifPackage.PATH_FRAGMENT__CHILD, child));
            } else {
                // Otherwise add the current child to its parent.
                addPFandSetResourceCommands.append(AddCommand.create(editingDomain, childrenToParentMap.get(child),
                        RestifPackage.Literals.PATH_FRAGMENT__CHILD, child));
            }
        }
        
        doExecute(editingDomain, addPFandSetResourceCommands);
    }

    /**
     * Add a {@link Resource} to the {@link RestIF} model.
     * 
     * @param owner    the {@link RestIF} model
     * @param resource the given {@link Resource}
     */
    public void addResource(RestIF owner, Resource resource) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);

        Command addCommand = AddCommand.create(editingDomain, owner, RestifPackage.Literals.RESOURCE, resource);
        doExecute(editingDomain, addCommand);
    }

    /**
     * Add a {@link MethodType} to a given {@link Resource}.
     * 
     * @param owner      - the {@link Resource} to add to
     * @param methodType - the {@link MethodType} to add
     */
    public void addAccessMethodToResource(Resource owner, MethodType methodType) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(owner);
        AccessMethod accessMethod = RestifFactory.eINSTANCE.createAccessMethod();
        accessMethod.setType(methodType);
        accessMethod.setName("[" + methodType.getLiteral() + "] " + getResourceName(owner));

        Command addCommand = AddCommand.create(editingDomain, owner, RestifPackage.Literals.ACCESS_METHOD,
                accessMethod);
        doExecute(editingDomain, addCommand);
    }

    private String getResourceName(Resource resource) {
        // Get list of all fragments to root.
        List<PathFragment> path = buildFragmentPath(resource.getEndpoint());

        // Concatenate names of all fragments to root.
        StringBuilder absolutePathBuilder = new StringBuilder("");
        for (PathFragment fragment : path) {
            // looks up the string field of the associated fragment. Can be either dynamic
            // or static.
            absolutePathBuilder.append(getFragmentName(fragment));
        }
        return absolutePathBuilder.toString();
    }

    /**
     * Private helper method to resolve a fragment to its absolute path, represented
     * as a list that reaches from the root fragment to the provided fragment. Works
     * recursively.
     * 
     * @return Ordered list reaching from the root PathFragment to the provided
     *         fragment.
     */
    private List<PathFragment> buildFragmentPath(PathFragment fragment) {

        // recursion end: the provided fragment IS the root fragment (parent is not a
        // fragment any more)
        if (!(fragment.eContainer() instanceof PathFragment)) {
            List<PathFragment> fragmentList = new LinkedList<PathFragment>();
            fragmentList.add(fragment);
            return fragmentList;
        }

        // recursion continue, this PathFragment is not yet the root.
        List<PathFragment> extendedPath = buildFragmentPath((PathFragment) fragment.eContainer());
        extendedPath.add(fragment);
        return extendedPath;
    }

    /**
     * Private helper method to resolve a fragment to its full qualified name, no
     * matter if it is a dynamic or static fragment.
     * 
     * @param fragment as the input for which the name must be looked up.
     * @return / + [{]? + fragmentname + [}]?
     */
    private String getFragmentName(PathFragment fragment) {
        if (fragment instanceof DynamicFragment) {
            return ((DynamicFragment) fragment).getPlaceholder();
        }
        return ((StaticFragment) fragment).getInternalname();
    }

    /**
     * Removes a {@link MethodType} from a given {@link Resource}.
     * 
     * @param resource   - the {@link Resource} to remove from
     * @param methodType - the {@link MethodType} to remove
     */
    public void removeAccessMethodFromResource(Resource resource, MethodType methodType) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(resource);
        CompoundCommand compoundCommand = new CompoundCommand();

        AccessMethod accessMethodToRemove = null;

        for (AccessMethod accessMethod : resource.getAccessmethod()) {
            if (accessMethod.getType() == methodType) {
                accessMethodToRemove = accessMethod;
            }
        }

        // Create remove Command.
        compoundCommand.append(RemoveCommand.create(editingDomain, accessMethodToRemove));

        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Add a new {@link StaticFragment} to the {@link RestIF} model.
     * 
     * @param restIF - the {@link RestIF} model
     * @param parent - the {@link PathFragment} to add a new child in
     * @param name   - the name of the new {@link StaticFragment}
     */
    public void addChildStaticFragment(RestIF restIF, PathFragment parent, String name) {
        StaticFragment child = RestifFactory.eINSTANCE.createStaticFragment();
        child.setInternalname(name);
        addGenericFragment(child, parent, name);
    }

    /**
     * Add a new {@link DynamicFragment} to the {@link RestIF} model.
     * 
     * @param restIF - the {@link RestIF} model
     * @param parent - the {@link PathFragment} to add a new child in
     * @param name   - the name of the new {@link DynamicFragment}
     */
    public void addChildDynamicFragment(RestIF restIF, PathFragment parent, String name) {
        DynamicFragment child = RestifFactory.eINSTANCE.createDynamicFragment();
        child.setPlaceholder(name);
        addGenericFragment(child, parent, name);
    }

    private void addGenericFragment(PathFragment child, PathFragment parent, String name) {
        //child.setName(name); // Possibly change this to absolute fragment path string (requires handling on
                             // tree layout change, dyn-stat flip etc.)

        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(parent);

        // Create commands.
        Command addChildCommand = AddCommand.create(editingDomain, parent, RestifPackage.Literals.PATH_FRAGMENT__CHILD,
                child);

        doExecute(editingDomain, addChildCommand);
    }

    /**
     * Sets a {@link PathFragment}'s name.
     * 
     * @param pathFragment - the given {@link PathFragment}
     * @param name         - the new name to change to
     */
    public void setPathFragmentName(PathFragment pathFragment, String name) {
        if (pathFragment instanceof StaticFragment) {
            name = "/" + name;
        } else if (pathFragment instanceof DynamicFragment) {
            name = "/{" + name + "}";
        }

        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(pathFragment);

        Command setCommand = null;

        if (pathFragment instanceof StaticFragment) {
            setCommand = SetCommand.create(editingDomain, pathFragment,
                    RestifPackage.Literals.STATIC_FRAGMENT__INTERNALNAME, name);
        } else {
            setCommand = SetCommand.create(editingDomain, pathFragment,
                    RestifPackage.Literals.DYNAMIC_FRAGMENT__PLACEHOLDER, name);
        }

        doExecute(editingDomain, setCommand);
    }

    /**
     * Switches the parent of a given {@link PathFragment} to another
     * {@link PathFragment}. This method removes the given {@link PathFragment} and
     * its children then adds them to the new parent.
     * 
     * @param pathFragment        - the {@link pathFragment} to move to the new
     *                            parent
     * @param parent              - the current parent of the {@link PathFragment}
     *                            to move
     * @param newParent           - the new parent of the {@link PathFragment} to
     *                            move
     * @param childrenToParentMap - a linked hash map containing keys of every child
     *                            of the switched root and its parent as values
     */
    public void switchParent(PathFragment pathFragment, PathFragment parent, PathFragment newParent,
            LinkedHashMap<PathFragment, PathFragment> childrenToParentMap) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(pathFragment);
        CompoundCommand deleteCommands = new CompoundCommand();

        // Reverse the list to remove from the child first.
        List<PathFragment> keySet = new ArrayList<>(childrenToParentMap.keySet());
        Collections.reverse(keySet);
        Set<PathFragment> reversedKeySet = new LinkedHashSet<>(keySet);

        for (PathFragment child : reversedKeySet) {
            deleteCommands.append(RemoveCommand.create(editingDomain, childrenToParentMap.get(child),
                    RestifPackage.Literals.PATH_FRAGMENT__CHILD, child));
        }

        // Execute the compound delete command.
        deleteCommands.append(
                RemoveCommand.create(editingDomain, parent, RestifPackage.Literals.PATH_FRAGMENT__CHILD, pathFragment));

        doExecute(editingDomain, deleteCommands);

        CompoundCommand addCommands = new CompoundCommand();
        addCommands.append(AddCommand.create(editingDomain, newParent, RestifPackage.Literals.PATH_FRAGMENT__CHILD,
                pathFragment));

        ArrayList<PathFragment> childrenList = new ArrayList<>();
        childrenList.addAll(childrenToParentMap.keySet());

        for (PathFragment child : childrenList) {
            // Adds each child.
            addCommands.append(AddCommand.create(editingDomain, childrenToParentMap.get(child),
                    RestifPackage.Literals.PATH_FRAGMENT__CHILD, child));
        }
        
        doExecute(editingDomain, addCommands);
    }

    /**
     * Sets the given {@link PathFragment} to a new position.
     * 
     * @param restIF       the {@link RestIF} model
     * @param pathFragment - the given {@link pathFragment} to reposition
     * @param parent       - the parent of the given {@link PathFragment}
     * @param newPosition  - the new position of the {@link PathFragment}
     */
    public void setPathFragmentPosition(RestIF restIF, PathFragment pathFragment, PathFragment parent,
            int newPosition) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(restIF);

        Command moveCommand = MoveCommand.create(editingDomain, parent, RestifPackage.Literals.PATH_FRAGMENT__CHILD,
                pathFragment, newPosition);
        doExecute(editingDomain, moveCommand);
    }
}
